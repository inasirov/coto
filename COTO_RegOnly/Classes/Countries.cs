﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Classes
{
    /// <summary>
    /// Summary description for Countries
    /// </summary>
    public class Countries
    {
        public static List<GenClass> ListOfCountries = new List<GenClass>();
        static Countries()
        {
            ListOfCountries.Add(new GenClass { Code = "", Description = "" });
            ListOfCountries.Add(new GenClass { Code = "Canada", Description = "Canada" });
            ListOfCountries.Add(new GenClass { Code = "USA", Description = "USA" });
            ListOfCountries.Add(new GenClass { Code = "UK", Description = "UK" });
            ListOfCountries.Add(new GenClass { Code = "India", Description = "India" });
            ListOfCountries.Add(new GenClass { Code = "Afghanistan", Description = "Afghanistan" });

            ListOfCountries.Add(new GenClass { Code = "Albania", Description = "Albania" });
            ListOfCountries.Add(new GenClass { Code = "Algeria", Description = "Algeria" });
            ListOfCountries.Add(new GenClass { Code = "Andorra", Description = "Andorra" });
            ListOfCountries.Add(new GenClass { Code = "Angola", Description = "Angola" });
            ListOfCountries.Add(new GenClass { Code = "Antigua", Description = "Antigua" });

            ListOfCountries.Add(new GenClass { Code = "Argentina", Description = "Argentina" });
            ListOfCountries.Add(new GenClass { Code = "Aruba", Description = "Aruba" });
            ListOfCountries.Add(new GenClass { Code = "Australia", Description = "Australia" });
            ListOfCountries.Add(new GenClass { Code = "Austria", Description = "Austria" });
            ListOfCountries.Add(new GenClass { Code = "Bahamas", Description = "Bahamas" });

            ListOfCountries.Add(new GenClass { Code = "Bahrain", Description = "Bahrain" });
            ListOfCountries.Add(new GenClass { Code = "Bangladesh", Description = "Bangladesh" });
            ListOfCountries.Add(new GenClass { Code = "Barbados", Description = "Barbados" });
            ListOfCountries.Add(new GenClass { Code = "Basutoland", Description = "Basutoland" });
            ListOfCountries.Add(new GenClass { Code = "Belgium", Description = "Belgium" });

            ListOfCountries.Add(new GenClass { Code = "Belize", Description = "Belize" });
            ListOfCountries.Add(new GenClass { Code = "Benin", Description = "Benin" });
            ListOfCountries.Add(new GenClass { Code = "Bermuda", Description = "Bermuda" });
            ListOfCountries.Add(new GenClass { Code = "Bhutan", Description = "Bhutan" });
            ListOfCountries.Add(new GenClass { Code = "Bolivia", Description = "Bolivia" });

            ListOfCountries.Add(new GenClass { Code = "Bophuthatswana", Description = "Bophuthatswana" });
            ListOfCountries.Add(new GenClass { Code = "Borneo", Description = "Borneo" });
            ListOfCountries.Add(new GenClass { Code = "Botswana", Description = "Botswana" });
            ListOfCountries.Add(new GenClass { Code = "Brazil", Description = "Brazil" });
            ListOfCountries.Add(new GenClass { Code = "Brunei", Description = "Brunei" });

            ListOfCountries.Add(new GenClass { Code = "Bulgaria", Description = "Bulgaria" });
            ListOfCountries.Add(new GenClass { Code = "Burma", Description = "Burma" });
            ListOfCountries.Add(new GenClass { Code = "Burundi", Description = "Burundi" });
            ListOfCountries.Add(new GenClass { Code = "Cambodia", Description = "Cambodia" });
            ListOfCountries.Add(new GenClass { Code = "Cameroon", Description = "Cameroon" });

            ListOfCountries.Add(new GenClass { Code = "Canal Zone", Description = "Canal Zone" });
            ListOfCountries.Add(new GenClass { Code = "Canary Islands", Description = "Canary Islands" });
            ListOfCountries.Add(new GenClass { Code = "Caymen Islands", Description = "Caymen Islands" });
            ListOfCountries.Add(new GenClass { Code = "Channel Island", Description = "Channel Island" });
            ListOfCountries.Add(new GenClass { Code = "Chile", Description = "Chile" });

            ListOfCountries.Add(new GenClass { Code = "Colombia", Description = "Colombia" });
            ListOfCountries.Add(new GenClass { Code = "Cook Island", Description = "Cook Island" });
            ListOfCountries.Add(new GenClass { Code = "Costa Rica", Description = "Costa Rica" });
            ListOfCountries.Add(new GenClass { Code = "Cuba", Description = "Cuba" });
            ListOfCountries.Add(new GenClass { Code = "Curacao", Description = "Curacao" });

            ListOfCountries.Add(new GenClass { Code = "Cwth. Ind. St.", Description = "Cwth. Ind. St." });
            ListOfCountries.Add(new GenClass { Code = "Cyprus", Description = "Cyprus" });
            ListOfCountries.Add(new GenClass { Code = "Czech Slovak", Description = "Czech Slovak" });
            ListOfCountries.Add(new GenClass { Code = "Dahomey", Description = "Dahomey" });
            ListOfCountries.Add(new GenClass { Code = "Denmark", Description = "Denmark" });

            ListOfCountries.Add(new GenClass { Code = "Dominican Republic", Description = "Dominican Republic" });
            ListOfCountries.Add(new GenClass { Code = "Ecuador", Description = "Ecuador" });
            ListOfCountries.Add(new GenClass { Code = "Egypt", Description = "Egypt" });
            ListOfCountries.Add(new GenClass { Code = "El Salvador", Description = "El Salvador" });
            ListOfCountries.Add(new GenClass { Code = "England", Description = "England" });

            ListOfCountries.Add(new GenClass { Code = "Ethiopia", Description = "Ethiopia" });
            ListOfCountries.Add(new GenClass { Code = "Falkland Islands", Description = "Falkland Islands" });
            ListOfCountries.Add(new GenClass { Code = "Finland", Description = "Finland" });
            ListOfCountries.Add(new GenClass { Code = "France", Description = "France" });
            ListOfCountries.Add(new GenClass { Code = "French Guiana", Description = "French Guiana" });

            ListOfCountries.Add(new GenClass { Code = "French Polynesia", Description = "French Polynesia" });
            ListOfCountries.Add(new GenClass { Code = "Gabon", Description = "Gabon" });
            ListOfCountries.Add(new GenClass { Code = "Gambia", Description = "Gambia" });
            ListOfCountries.Add(new GenClass { Code = "Germany", Description = "Germany" });
            ListOfCountries.Add(new GenClass { Code = "Ghana", Description = "Ghana" });

            ListOfCountries.Add(new GenClass { Code = "Gibralter", Description = "Gibralter" });
            ListOfCountries.Add(new GenClass { Code = "Granada", Description = "Granada" });
            ListOfCountries.Add(new GenClass { Code = "Greece", Description = "Greece" });
            ListOfCountries.Add(new GenClass { Code = "Greenland", Description = "Greenland" });
            ListOfCountries.Add(new GenClass { Code = "Guadeloupe", Description = "Guadeloupe" });

            ListOfCountries.Add(new GenClass { Code = "Guatemala", Description = "Guatemala" });
            ListOfCountries.Add(new GenClass { Code = "Guinea", Description = "Guinea" });
            ListOfCountries.Add(new GenClass { Code = "Guyana", Description = "Guyana" });
            ListOfCountries.Add(new GenClass { Code = "Haiti", Description = "Haiti" });
            ListOfCountries.Add(new GenClass { Code = "Honduras", Description = "Honduras" });

            ListOfCountries.Add(new GenClass { Code = "Hong Kong", Description = "Hong Kong" });
            ListOfCountries.Add(new GenClass { Code = "Hungary", Description = "Hungary" });
            ListOfCountries.Add(new GenClass { Code = "Iceland", Description = "Iceland" });
            ListOfCountries.Add(new GenClass { Code = "Indonesia", Description = "Indonesia" });
            ListOfCountries.Add(new GenClass { Code = "Iran", Description = "Iran" });

            ListOfCountries.Add(new GenClass { Code = "Iraq", Description = "Iraq" });
            ListOfCountries.Add(new GenClass { Code = "Ireland", Description = "Ireland" });
            ListOfCountries.Add(new GenClass { Code = "Israel", Description = "Israel" });
            ListOfCountries.Add(new GenClass { Code = "Italy", Description = "Italy" });
            ListOfCountries.Add(new GenClass { Code = "Ivory Coast", Description = "Ivory Coast" });

            ListOfCountries.Add(new GenClass { Code = "Jamaica", Description = "Jamaica" });
            ListOfCountries.Add(new GenClass { Code = "Japan", Description = "Japan" });
            ListOfCountries.Add(new GenClass { Code = "Jordan", Description = "Jordan" });
            ListOfCountries.Add(new GenClass { Code = "Kenya", Description = "Kenya" });
            ListOfCountries.Add(new GenClass { Code = "Korea", Description = "Korea" });

            ListOfCountries.Add(new GenClass { Code = "Kuwait", Description = "Kuwait" });
            ListOfCountries.Add(new GenClass { Code = "Laos", Description = "Laos" });
            ListOfCountries.Add(new GenClass { Code = "Lebanon", Description = "Lebanon" });
            ListOfCountries.Add(new GenClass { Code = "Lesotho", Description = "Lesotho" });
            ListOfCountries.Add(new GenClass { Code = "Liberia", Description = "Liberia" });

            ListOfCountries.Add(new GenClass { Code = "Libya", Description = "Libya" });
            ListOfCountries.Add(new GenClass { Code = "Liechtenstein", Description = "Liechtenstein" });
            ListOfCountries.Add(new GenClass { Code = "Luxembourg", Description = "Luxembourg" });
            ListOfCountries.Add(new GenClass { Code = "Madagascar", Description = "Madagascar" });
            ListOfCountries.Add(new GenClass { Code = "Malawi", Description = "Malawi" });

            ListOfCountries.Add(new GenClass { Code = "Malaysia", Description = "Malaysia" });
            ListOfCountries.Add(new GenClass { Code = "Mali", Description = "Mali" });
            ListOfCountries.Add(new GenClass { Code = "Malta", Description = "Malta" });
            ListOfCountries.Add(new GenClass { Code = "Martinique", Description = "Martinique" });
            ListOfCountries.Add(new GenClass { Code = "Mauritius", Description = "Mauritius" });

            ListOfCountries.Add(new GenClass { Code = "Malaysia", Description = "Mexico" });
            ListOfCountries.Add(new GenClass { Code = "Mali", Description = "Monace" });
            ListOfCountries.Add(new GenClass { Code = "Malta", Description = "Mongolia" });
            ListOfCountries.Add(new GenClass { Code = "Martinique", Description = "Morocco" });
            ListOfCountries.Add(new GenClass { Code = "Mauritius", Description = "Mozambique" });

            ListOfCountries.Add(new GenClass { Code = "Nepal", Description = "Nepal" });
            ListOfCountries.Add(new GenClass { Code = "Netherlands", Description = "Netherlands" });
            ListOfCountries.Add(new GenClass { Code = "New Caledonia", Description = "New Caledonia" });
            ListOfCountries.Add(new GenClass { Code = "New Guinea", Description = "New Guinea" });
            ListOfCountries.Add(new GenClass { Code = "New Zealand", Description = "New Zealand" });
            ListOfCountries.Add(new GenClass { Code = "Nicaragua", Description = "Nicaragua" });
            ListOfCountries.Add(new GenClass { Code = "Niger", Description = "Niger" });
            ListOfCountries.Add(new GenClass { Code = "Nigeria", Description = "Nigeria" });
            ListOfCountries.Add(new GenClass { Code = "North Ireland", Description = "North Ireland" });
            ListOfCountries.Add(new GenClass { Code = "Oman", Description = "Oman" });

            ListOfCountries.Add(new GenClass { Code = "Pakistan", Description = "Pakistan" });
            ListOfCountries.Add(new GenClass { Code = "Panama", Description = "Panama" });
            ListOfCountries.Add(new GenClass { Code = "Paraguay", Description = "Paraguay" });
            ListOfCountries.Add(new GenClass { Code = "Peoples Republic of China", Description = "Peoples Republic of China" });
            ListOfCountries.Add(new GenClass { Code = "Peoples Republic of Congo", Description = "Peoples Republic of Congo" });
            ListOfCountries.Add(new GenClass { Code = "Peru", Description = "Peru" });
            ListOfCountries.Add(new GenClass { Code = "Philippines", Description = "Philippines" });
            ListOfCountries.Add(new GenClass { Code = "Poland", Description = "Poland" });
            ListOfCountries.Add(new GenClass { Code = "Portugal", Description = "Portugal" });
            ListOfCountries.Add(new GenClass { Code = "Qatar", Description = "Qatar" });

            ListOfCountries.Add(new GenClass { Code = "Republic of South Africa", Description = "Republic of South Africa" });
            ListOfCountries.Add(new GenClass { Code = "Republic of Zaire", Description = "Republic of Zaire" });
            ListOfCountries.Add(new GenClass { Code = "Romania", Description = "Romania" });
            ListOfCountries.Add(new GenClass { Code = "Russia", Description = "Russia" });
            ListOfCountries.Add(new GenClass { Code = "Rwanda", Description = "Rwanda" });
            ListOfCountries.Add(new GenClass { Code = "Saint Vincent", Description = "Saint Vincent" });
            ListOfCountries.Add(new GenClass { Code = "Santa Lucia", Description = "Santa Lucia" });
            ListOfCountries.Add(new GenClass { Code = "Saudi Arabia", Description = "Saudi Arabia" });
            ListOfCountries.Add(new GenClass { Code = "Scotland", Description = "Scotland" });
            ListOfCountries.Add(new GenClass { Code = "Senegal", Description = "Senegal" });

            ListOfCountries.Add(new GenClass { Code = "Seychelles", Description = "Seychelles" });
            ListOfCountries.Add(new GenClass { Code = "Sierra Leone", Description = "Sierra Leone" });
            ListOfCountries.Add(new GenClass { Code = "Singapore", Description = "Singapore" });
            ListOfCountries.Add(new GenClass { Code = "Slovenia", Description = "Slovenia" });
            ListOfCountries.Add(new GenClass { Code = "Somali Republic", Description = "Somali Republic" });
            ListOfCountries.Add(new GenClass { Code = "Southwest Africa", Description = "Southwest Africa" });
            ListOfCountries.Add(new GenClass { Code = "Spain", Description = "Spain" });
            ListOfCountries.Add(new GenClass { Code = "Sri Lanka", Description = "Sri Lanka" });
            ListOfCountries.Add(new GenClass { Code = "Sudan", Description = "Sudan" });
            ListOfCountries.Add(new GenClass { Code = "Surinam", Description = "Surinam" });

            ListOfCountries.Add(new GenClass { Code = "Swaziland", Description = "Swaziland" });
            ListOfCountries.Add(new GenClass { Code = "Sweden", Description = "Sweden" });
            ListOfCountries.Add(new GenClass { Code = "Switzerland", Description = "Switzerland" });
            ListOfCountries.Add(new GenClass { Code = "Syria", Description = "Syria" });
            ListOfCountries.Add(new GenClass { Code = "Taiwan", Description = "Taiwan" });
            ListOfCountries.Add(new GenClass { Code = "Tanzania", Description = "Tanzania" });
            ListOfCountries.Add(new GenClass { Code = "Tasmania", Description = "Tasmania" });
            ListOfCountries.Add(new GenClass { Code = "Thailand", Description = "Thailand" });
            ListOfCountries.Add(new GenClass { Code = "Togo", Description = "Togo" });
            ListOfCountries.Add(new GenClass { Code = "Tonga", Description = "Tonga" });

            ListOfCountries.Add(new GenClass { Code = "Trinidad", Description = "Trinidad" });
            ListOfCountries.Add(new GenClass { Code = "Tunisia", Description = "Tunisia" });
            ListOfCountries.Add(new GenClass { Code = "Turkey", Description = "Turkey" });
            ListOfCountries.Add(new GenClass { Code = "UAE", Description = "UAE" });
            ListOfCountries.Add(new GenClass { Code = "Uganda", Description = "Uganda" });
            ListOfCountries.Add(new GenClass { Code = "Upper Volta", Description = "Upper Volta" });
            ListOfCountries.Add(new GenClass { Code = "Uruguay", Description = "Uruguay" });
            ListOfCountries.Add(new GenClass { Code = "Vatican City", Description = "Vatican City" });
            ListOfCountries.Add(new GenClass { Code = "Venezuela", Description = "Venezuela" });
            ListOfCountries.Add(new GenClass { Code = "Vietnam", Description = "Vietnam" });

            ListOfCountries.Add(new GenClass { Code = "Virgin Islands", Description = "Virgin Islands" });
            ListOfCountries.Add(new GenClass { Code = "Wales", Description = "Wales" });
            ListOfCountries.Add(new GenClass { Code = "Western Samoa", Description = "Western Samoa" });
            ListOfCountries.Add(new GenClass { Code = "Yemen", Description = "Yemen" });
            ListOfCountries.Add(new GenClass { Code = "Zambia", Description = "Zambia" });
            ListOfCountries.Add(new GenClass { Code = "Zimbabwe", Description = "Zimbabwe" });
        }

    }
}