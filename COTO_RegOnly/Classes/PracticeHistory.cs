﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    [Serializable]
    public class PracticeHistory
    {
        public string PracticedOutsideOntario { get; set; }
        
        public string FirstCountryPractice { get; set; }
        public string FirstProvinceStatePractice { get; set; }
        public string FirstProvincePractice { get; set; }
        public string FirstYearPractice { get; set; }
        public string FirstYearCanadianPractice { get; set; }

        public string LastCountryPractice { get; set; }
        public string LastProvincePractice { get; set; }
        public string LastProvinceStatePractice { get; set; }
        public string LastYearOutsideOntarioPractice { get; set; }
    }
}