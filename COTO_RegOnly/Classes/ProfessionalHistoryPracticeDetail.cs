﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    public class ProfessionalHistoryPracticeDetail
    {
        protected string _RegulatoryBody;
        protected string _RegulatoryBodyText;
        protected string _RegulatoryBodyOther;
        protected string _Province;
        protected string _Country;
        protected string _License;
        protected DateTime? _ExpiryDate;

        public string RegulatoryBody
        {
            get
            {
                return _RegulatoryBody;
            }
            set
            {
                _RegulatoryBody = value;
            }
        }

        public string RegulatoryBodyText
        {
            get
            {
                return _RegulatoryBodyText;
            }
            set
            {
                _RegulatoryBodyText = value;
            }
        }

        public string RegulatoryBodyOther
        {
            get
            {
                return _RegulatoryBodyOther;
            }
            set
            {
                _RegulatoryBodyOther = value;
            }
        }

        public string Province
        {
            get
            {
                return _Province;
            }
            set
            {
                _Province = value;
            }
        }

        public string Country
        {
            get
            {
                return _Country;
            }
            set
            {
                _Country = value;
            }
        }

        public string License
        {
            get
            {
                return _License;
            }
            set
            {
                _License = value;
            }
        }

        public DateTime? ExpiryDate
        {
            get
            {
                return _ExpiryDate;
            }
            set
            {
                _ExpiryDate = value;
            }
        }

    }
}