﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    /// <summary>
    /// Summary description for Employment Profile
    /// </summary>
    [Serializable]
    public class EmploymentProfile
    {
        public string PracticeStatus { get; set; }
        public string CollegeMailings { get; set; }
        public string CurrentWorkPreference { get; set; }
        public string StudentSupervisionLastYear { get; set; }
        public string StudentSupervisionComingYear { get; set; }
        public string HaveClinicalClients { get; set; }
        public int PracticingNumberWeeks { get; set; }
        public int PracticingNumberHoursPerWeek { get; set; }
        public string NaturePractice { get; set; }
        public int TimeSpentProfServices { get; set; }
        public int TimeSpentTeaching { get; set; }
        public int TimeSpentClinicalEducation { get; set; }
        public int TimeSpentResearch { get; set; }
        public int TimeSpentAdministration { get; set; }
        public int TimeSpentOtherActivities { get; set; }
    }
}