﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    [Serializable]
    public class ApplicationStatus
    {
        public DateTime ApplicationReceivedDate { get; set; }

        public DateTime ApplicationFeeReceivedDate { get; set; }

        public DateTime CurrencyRequrementMetDate { get; set; }

        public DateTime AddCurrencyDataSheetReceivedDate { get; set; }

        public DateTime PracticeSupervisorFormReceivedDate { get; set; }

        public DateTime InitialLearningContactDate { get; set; }

        public DateTime FinalLearningContactDate { get; set; }
        public DateTime VulnerableCheckDate { get; set; }
        public DateTime ConductRequirementMetDate { get; set; }

        public DateTime ConductReviewFeeReceivedDate { get; set; }

        public DateTime LanguageFluencyReceivedDate { get; set; }

        public DateTime WorkEligibilityLegalWorkDate { get; set; }

        public DateTime WESReportReceivedDate { get; set; }

        public DateTime AcademicEquivalencyReviewFeeReceivedDate { get; set; }

        public DateTime AcademicReviewToolReceivedDate { get; set; }

        public DateTime CirriculumReceivedDate { get; set; }

        public DateTime FinalEducationalTranscriptReceivedDate { get; set; }

        public DateTime ProgramApprovedCotoDate { get; set; }  // needs to be removed from all reports

        public DateTime SeasDispositionReport { get; set; }

        public DateTime LMSA_FormReceived { get; set; }

        public DateTime OTRegulatoryHistory1ReceivedDate { get; set; }

        public DateTime OTRegulatoryHistory2ReceivedDate { get; set; }

        public DateTime OTRegulatoryHistory3ReceivedDate { get; set; }

        public DateTime OTRegulatoryHistory4ReceivedDate { get; set; }

        public DateTime OTRegulatoryHistory5ReceivedDate { get; set; }

        public DateTime OtherProfessionRegistration1Date { get; set; }

        public DateTime OtherProfessionRegistration2Date { get; set; }

        public DateTime OtherProfessionRegistration3Date { get; set; }

        public DateTime ProfessionLiabilityInsuranceReceivedDate { get; set; }

        public DateTime AffidavitReceivedDate { get; set; }

        public DateTime EmployerAcknowledgeFormReceivedDate { get; set; }

        public DateTime ConfirmationRegistrationCAOTExamReceivedDate { get; set; }

        public DateTime CAOTExamResultsReceivedDate { get; set; }

        public DateTime RegistrationFeeReceivedDate { get; set; }

        public bool ReadyOnlineView { get; set; }

        public bool AddCurrencyDataSheetReceivedReqired { get; set; }

        public bool PracticeSupervisorFormReceivedRequired { get; set; }

        public bool InitialLearningContactRequired { get; set; }

        public bool FinalLearningContactRequired { get; set; }

        public bool ConductReviewFeeReceivedRequired { get; set; }

        public bool LanguageFluencyReceivedRequired { get; set; }

        public bool WESReportReceivedRequired { get; set; }

        public bool AcademicEquivalencyReviewFeeReceivedRequired { get; set; }

        public bool AcademicReviewToolReceivedRequired { get; set; }

        public bool CourseCirriculumReceivedRequired { get; set; }

        public bool FinalEducationalTranscriptReceivedRequired { get; set; }

        public bool SeasDispositionReportRequired { get; set; }

        public bool LMSA_FormReceivedRequired { get; set; }

        public bool OTRegulatoryHistory1ReceivedRequired { get; set; }

        public bool OTRegulatoryHistory2ReceivedRequired { get; set; }

        public bool OTRegulatoryHistory3ReceivedRequired { get; set; }

        public bool OTRegulatoryHistory4ReceivedRequired { get; set; }

        public bool OTRegulatoryHistory5ReceivedRequired { get; set; }

        public bool OtherProfessionRegistration1Required { get; set; }

        public bool OtherProfessionRegistration2Required { get; set; }

        public bool OtherProfessionRegistration3Required { get; set; }

        public bool AffidavitReceivedRequired { get; set; }

        public bool EmployerAcknowledgeFormReceivedRequired { get; set; }

        public bool ConfirmationRegistrationCAOTExamReceivedRequired { get; set; }

        public DateTime ConductSubmitted { get; set; }
    }
}