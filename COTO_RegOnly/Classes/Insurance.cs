﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    [Serializable]
    public class Insurance
    {
        public DateTime StartDate { get; set; }
        public int SEQN { get; set; }
        protected string _PlanHeld;
        protected string _OtherInsuranceProvider;
        protected DateTime _ExpiryDate;
        protected string _CertificateNumber;
        public string PlanHeldName { get; set; }
        public DateTime ReportedDate { get; set; }

        public string PlanHeld
        {
            get
            {
                return _PlanHeld;
            }
            set
            {
                _PlanHeld = value;
            }
        }

        public string OtherInsuranceProvider
        {
            get
            {
                return _OtherInsuranceProvider;
            }
            set
            {
                _OtherInsuranceProvider = value;
            }
        }

        public DateTime ExpiryDate
        {
            get
            {
                return _ExpiryDate;
            }
            set
            {
                _ExpiryDate = value;
            }
        }

        public string CertificateNumber
        {
            get
            {
                return _CertificateNumber;
            }
            set
            {
                _CertificateNumber = value;
            }
        }

    }
}