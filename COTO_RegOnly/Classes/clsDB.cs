﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace Classes
{
    public class clsDB
    {

        string _ConnString = WebConfigItems.GetConnectionString();
        // public static string = System.Configuration.ConfigurationManager.ConnectionStrings["sqlConn"].ConnectionString;

        public clsDB()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        //Execute the SQL
        public DataTable ExecuteReader(SqlCommand command)
        {
            DataTable dt = new DataTable();

            try
            {
                command.Connection.Open();
                SqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                dt.Load(dr);
                return dt;
            }
            //catch (SqlException ex)
            //{
            //    // handle error
            //}
            catch //(Exception ex)
            {
                // handle error
            }
            finally
            {
                command.Connection.Close();
            }
            return dt;
        }

        //Execute the SQL
        public bool ExecuteCommand(string strSQL)
        {
            string strConString = _ConnString;
            System.Data.SqlClient.SqlConnection conSQL = new System.Data.SqlClient.SqlConnection();
            conSQL.ConnectionString = strConString;
            SqlCommand cmdSQL = new SqlCommand();
            conSQL.Open();
            cmdSQL.Connection = conSQL;
            cmdSQL.CommandText = strSQL;
            cmdSQL.ExecuteNonQuery();
            conSQL.Close();
            conSQL.Dispose();
            return true;
        }


        //Get DataTable
        public DataTable GetData(string sql)
        {
            string strConString = _ConnString;
            DataTable dt = new DataTable();
            // try
            // {
            System.Data.SqlClient.SqlConnection conSQL = new System.Data.SqlClient.SqlConnection();
            conSQL.ConnectionString = strConString;
            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sql, conSQL);
            DataSet datads = new DataSet();
            da.Fill(datads);
            da.Dispose();
            dt = datads.Tables[0];
            conSQL.Close();
            return dt;
        }

        //Get DataTable
        public DataTable GetDataSecured(string sql)
        {
            string strConString = _ConnString;
            DataTable dt = new DataTable();
            try
            {
                System.Data.SqlClient.SqlConnection conSQL = new System.Data.SqlClient.SqlConnection();
                conSQL.ConnectionString = strConString;
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sql, conSQL);
                DataSet datads = new DataSet();
                da.Fill(datads);
                da.Dispose();
                dt = datads.Tables[0];
                conSQL.Close();
                return dt;

            }
            catch
            {
                return dt;
            }
        }

        //Get single value STRING
        public string getStringFromDB(string sql)
        {
            string thisValue = "";
            using (SqlConnection conn = new SqlConnection(_ConnString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.Add("@Name", SqlDbType.VarChar);
                cmd.Parameters["@name"].Value = "TEST";
                conn.Open();
                try
                {
                    thisValue = cmd.ExecuteScalar().ToString();
                }
                catch
                {
                    thisValue = "";
                }
                conn.Close();
            }
            return thisValue;
        }
    }
}
