﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Classes
{
    /// <summary>
    /// Summary description for SessionParameters
    /// </summary>
    public static class SessionParameters
    {
        #region Variables
        private const string SESSION_USER_ID = "ID";
        private const string SESSION_CURRENT_USER = "CurrentUser";
        private const string SESSION_CURRENT_EMPLOYMENT_STATUS = "CurrentEmploymentStatus";

        private const string SESSION_RETURN_URL = "ReturnUrl";
        private const string SESSION_PAGE_MESSAGES = "PageMessages";
        private const string SESSION_RENEWAL_STEP = "RenewalStep";
        private const string SESSION_APPLICATION_STEP = "ApplicationStep";
        private const string SESSION_SHOW_STEPS = "ShowSteps";

        private const string SESSION_APPLICATION_REGISTRATION_CATEGORY = "ApplicationRegCategory";
        private const string SESSION_APPLICATION_REGISTRATION_CITIZENSHIP = "ApplicationRegCitizenship";
        #endregion

        #region Methods
        private static object GetSession(string SessionName)
        {
            if (HttpContext.Current != null &&
                HttpContext.Current.Session != null &&
                HttpContext.Current.Session[SessionName] != null)
                return HttpContext.Current.Session[SessionName];
            return null;
        }

        private static void SetSession(string SessionName, Object Value)
        {
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Session != null)
                {
                    HttpContext.Current.Session[SessionName] = Value;
                }
            }
        }
        #endregion

        #region Parameters

        public static string CurrentUserId
        {
            get
            {
                if (GetSession(SESSION_USER_ID) == null)
                {
                    SetSession(SESSION_USER_ID, string.Empty);
                }
                return (string)GetSession(SESSION_USER_ID);
            }
            set
            {
                SetSession(SESSION_USER_ID, value);
            }
        }

        public static User CurrentUser
        {
            get
            {
                if (GetSession(SESSION_CURRENT_USER) != null)
                {
                    return (User)GetSession(SESSION_CURRENT_USER);
                }
                else
                    return null;
            }
            set
            {
                SetSession(SESSION_CURRENT_USER, value);
            }
        }

        public static string CurrentEmploymentStatus
        {
            get
            {
                if (GetSession(SESSION_CURRENT_EMPLOYMENT_STATUS) != null)
                {
                    return (string)GetSession(SESSION_CURRENT_EMPLOYMENT_STATUS);
                }
                else
                    return string.Empty;
            }
            set
            {
                SetSession(SESSION_CURRENT_EMPLOYMENT_STATUS, value);
            }
        }

        public static string ReturnUrl
        {
            get
            {
                if (GetSession(SESSION_RETURN_URL) != null)
                {
                    return (string)GetSession(SESSION_RETURN_URL);
                }
                else
                    return null;
            }
            set
            {
                SetSession(SESSION_RETURN_URL, value);
            }
        }

        public static PageMessages PageMessages
        {
            get
            {
                if (GetSession(SESSION_PAGE_MESSAGES) != null)
                {
                    return (PageMessages)GetSession(SESSION_PAGE_MESSAGES);
                }
                else
                {
                    var pageMessages = new PageMessages();
                    SetSession(SESSION_PAGE_MESSAGES, pageMessages);
                    return pageMessages;
                }

            }
            set
            {
                SetSession(SESSION_PAGE_MESSAGES, value);
            }
        }

        public static int RenewalStep
        {
            get
            {
                if (GetSession(SESSION_RENEWAL_STEP) != null)
                {
                    return (int)GetSession(SESSION_RENEWAL_STEP);
                }
                else
                    return 0;
            }
            set
            {
                SetSession(SESSION_RENEWAL_STEP, value);
            }
        }

        public static int ApplicationStep
        {
            get
            {
                if (GetSession(SESSION_APPLICATION_STEP) != null)
                {
                    return (int)GetSession(SESSION_APPLICATION_STEP);
                }
                else
                    return 0;
            }
            set
            {
                SetSession(SESSION_APPLICATION_STEP, value);
            }
        }

        public static bool ShowSteps
        {
            get
            {
                if (GetSession(SESSION_SHOW_STEPS) != null)
                {
                    return (bool)GetSession(SESSION_SHOW_STEPS);
                }
                else
                    return false;
            }
            set
            {
                SetSession(SESSION_SHOW_STEPS, value);
            }
        }

        public static string ApplicationRegistrationCategory
        {
            get
            {
                if (GetSession(SESSION_APPLICATION_REGISTRATION_CATEGORY) != null)
                {
                    return (string)GetSession(SESSION_APPLICATION_REGISTRATION_CATEGORY);
                }
                else
                    return null;
            }
            set
            {
                SetSession(SESSION_APPLICATION_REGISTRATION_CATEGORY, value);
            }
        }

        public static string ApplicationRegistrationCitizenship
        {
            get
            {
                if (GetSession(SESSION_APPLICATION_REGISTRATION_CITIZENSHIP) != null)
                {
                    return (string)GetSession(SESSION_APPLICATION_REGISTRATION_CITIZENSHIP);
                }
                else
                    return null;
            }
            set
            {
                SetSession(SESSION_APPLICATION_REGISTRATION_CITIZENSHIP, value);
            }
        }
        #endregion
    }
}