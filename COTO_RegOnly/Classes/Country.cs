﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Classes
{
    /// <summary>
    /// Summary description for Country
    /// </summary>
    [Serializable]
    public class Country
    {
        protected string _Name;
        protected string _FullName;

        public string FullName
        {
            get
            {
                return _FullName;
            }
            set
            {
                _FullName = value;
            }
        }

        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }
    }
}