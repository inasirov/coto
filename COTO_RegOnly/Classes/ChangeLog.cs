﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    public class ChangeLog
    {
        public DateTime UpdateDate { get; set; }
        public string LogType { get; set; }
        public string SubType { get; set; }
        public string User_Id { get; set; }
        public string ID { get; set; }
        public string Log_Text { get; set; }
    }
}