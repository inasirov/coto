﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    [Serializable]
    public class Conduct
    {
        protected string _Refused_OT_Other;
        protected string _Refused_OT_Other_Details;

        protected string _Finding_Other_Juris;
        protected string _Finding_Other_Juris_Details;

        protected string _Finding_Other_Prof;
        protected string _Finding_Other_Prof_Details;

        protected string _Guilty_Rel_OT;
        protected string _Guilty_Rel_OT_Details;

        protected string _Criminal_Guilt;
        protected string _Criminal_Guilt_Details;

        protected string _Lack_Know_Skill_Judg;
        protected string _Lack_Know_Skill_Judg_Details;

        protected string _Finding_Neg_Malpractice;
        protected string _Finding_Neg_Malpractice_Details;

        public string Facing_Proc { get; set; }
        public string Facing_Proc_Details { get; set; }
        public string Facing_Other_Prof { get; set; }
        public string Facing_Other_Prof_Details { get; set; }

        public string Refused_OT_Other
        {
            get
            {
                return _Refused_OT_Other;
            }
            set
            {
                _Refused_OT_Other = value;
            }
        }

        public string Refused_OT_Other_Details
        {
            get
            {
                return _Refused_OT_Other_Details;
            }
            set
            {
                _Refused_OT_Other_Details = value;
            }
        }

        public string Finding_Other_Juris
        {
            get
            {
                return _Finding_Other_Juris;
            }
            set
            {
                _Finding_Other_Juris = value;
            }
        }

        public string Finding_Other_Juris_Details
        {
            get
            {
                return _Finding_Other_Juris_Details;
            }
            set
            {
                _Finding_Other_Juris_Details = value;
            }
        }

        public string Finding_Other_Prof
        {
            get
            {
                return _Finding_Other_Prof;
            }
            set
            {
                _Finding_Other_Prof = value;
            }
        }

        public string Finding_Other_Prof_Details
        {
            get
            {
                return _Finding_Other_Prof_Details;
            }
            set
            {
                _Finding_Other_Prof_Details = value;
            }
        }

        public string Guilty_Rel_OT
        {
            get
            {
                return _Guilty_Rel_OT;
            }
            set
            {
                _Guilty_Rel_OT = value;
            }
        }

        public string Guilty_Rel_OT_Details
        {
            get
            {
                return _Guilty_Rel_OT_Details;
            }
            set
            {
                _Guilty_Rel_OT_Details = value;
            }
        }

        public string Criminal_Guilt
        {
            get
            {
                return _Criminal_Guilt;
            }
            set
            {
                _Criminal_Guilt = value;
            }
        }

        public string Criminal_Guilt_Details
        {
            get
            {
                return _Criminal_Guilt_Details;
            }
            set
            {
                _Criminal_Guilt_Details = value;
            }
        }

        public string Lack_Know_Skill_Judg
        {
            get
            {
                return _Lack_Know_Skill_Judg;
            }
            set
            {
                _Lack_Know_Skill_Judg = value;
            }
        }

        public string Lack_Know_Skill_Judg_Details
        {
            get
            {
                return _Lack_Know_Skill_Judg_Details;
            }
            set
            {
                _Lack_Know_Skill_Judg_Details = value;
            }
        }

        public string Finding_Neg_Malpractice
        {
            get
            {
                return _Finding_Neg_Malpractice;
            }
            set
            {
                _Finding_Neg_Malpractice = value;
            }
        }

        public string Finding_Neg_Malpractice_Details
        {
            get
            {
                return _Finding_Neg_Malpractice_Details;
            }
            set
            {
                _Finding_Neg_Malpractice_Details = value;
            }
        }

    }
}