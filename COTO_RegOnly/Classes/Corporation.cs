﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Classes;

namespace COTO_RegOnly.Classes
{
    public class Corporation
    {
        protected string _Id;
        protected string _FullName;
        protected string _Certificate;
        protected string _Status;
        protected DateTime? _StatusEffectiveDate;
        protected string _PracticeName;
        protected Address _BusinessAddress;
        protected string _Phone;
        protected string _Email;
        protected List<User> _Shareholders;

        public string Id
        {
            get
            {
                return _Id;
            }
            set
            {
                _Id = value;
            }
        }

        public string FullName
        {
            get
            {
                return _FullName;
            }
            set
            {
                _FullName = value;
            }
        }

        public string Certificate
        {
            get
            {
                return _Certificate;
            }
            set
            {
                _Certificate = value;
            }
        }

        public string Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }

        public DateTime? StatusEffectiveDate
        {
            get
            {
                return _StatusEffectiveDate;
            }
            set
            {
                _StatusEffectiveDate = value;
            }
        }

        public string PracticeName
        {
            get
            {
                return _PracticeName;
            }
            set
            {
                _PracticeName = value;
            }
        }

        public Address BusinessAddress
        {
            get
            {
                return _BusinessAddress;
            }
            set
            {
                _BusinessAddress = value;
            }
        }

        public string Phone
        {
            get
            {
                return _Phone;
            }
            set
            {
                _Phone = value;
            }
        }

        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                _Email = value;
            }
        }

        public List<User> Shareholders
        {
            get
            {
                return _Shareholders;
            }
            set
            {
                _Shareholders = value;
            }
        }
    }
}