﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    public static class HardcodedValues
    {
        public static string[] Steps = new string[]
        {
            //1"/ot/source/members/cMemberEdit.cfm>",
            //2"/ot/source/members/ud_displayedit.cfm?window_name=Name%2DE%5FREG%5FSection%5F1&table_name=CURRENCY&title=eReg%20Currency&isacompany=0&full_name=#URLEncodedFormat(Client.Full_Name)#",
            //3"/ot/source/members/UD_DisplayEditCustEducation.cfm?window_name=Name%2DE%5FREG%5FSection%5F3&table_name=EDUCATION%5FALL&title=eReg%20Education&isacompany=0&full_name=#URLEncodedFormat(Client.Full_Name)#",
            //4"/ot/source/members/custMemberOverallEmployment.cfm?&window_name=Name-OVERALL_EMPLOYMENT&table_name=EMPLOYMENT_ALL&title=Overall%20Employment&isacompany=0&full_name=#client.full_name#",
            //5"/ot/source/members/ud_displayedit.cfm?&window_name=Name%2DE%5FREG%5FSection%5F4&table_name=EMPLOYMENT%5FALL&title=eReg%20Employment&isacompany=0&full_name=#URLEncodedFormat(Client.Full_Name)#",
            //6"/ot/source/members/ud_displayedit.cfm?&window_name=Name%2DE%5FREG%5FSection%5F6&table_name=ANN%5FREG%5FADDL%5FINFO&title=eReg%20Lang/Conduct/Insurance&isacompany=0&full_name=#URLEncodedFormat(Client.Full_Name)#",
            //7"/ot/source/members/custMemberOTPractice.cfm?full_name=#URLEncodedFormat(Client.Full_Name)#",
            //8"/ot/source/members/custMemberProLiabilityIns.cfm?full_name=#URLEncodedFormat(Client.Full_Name)#",
            //9"/ot/source/Members/UD_DisplayEdit.cfm?WINDOW_NAME=Name%2DE%5FREG%5FSection%5FDECLARE&TABLE_NAME=DECLARATION&TITLE=eReg%20Declarations&FULL_NAME=#URLEncodedFormat(Client.Full_Name)#&ISACOMPANY=0",
            //10"../Dues/cDuesDisplay.cfm",
            //11"../dues/complete.cfm"
        };

        public static decimal AnnualRegistration_Fee_Total = 743.03M;
        public static decimal AnnualRegistration_Fee = 657.55M;
        public static decimal AnnualRegistration_Fee_HST = 85.48M;
        public static decimal AnnualRegistration_Late_Fee = 100.00M;
        public static decimal AnnualRegistration_Late_Fee_With_HST = 113.00M;
        public static decimal AnnualRegistration_NSF_Fee = 28.25M;

        public static decimal Application_NSF_Fee_Total = 28.25M;
        public static decimal Application_NSF_Fee = 25M;
        public static decimal Application_NSF_Fee_HST = 3.25M;

        public static string RenewalPeriod = "2021-2022";
        public static string PrevRenewalYear = "2020";
        public static string RenewalYear = "2021";
    }
}