﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;

namespace COTO_RegOnly.Classes
{
    public static class ImageUtil
    {

        #region :: Class Functions ::

        /// <summary>
        /// Creates a monochrome version of an image
        /// </summary>
        /// <param name="sourceImageFile">The path to the image to convert to monochrome. The source image is not altered in any way.</param>
        /// <returns>The monochrome image</returns>
        public static Image MakeMonochrome(string sourceImageFile)
        {
            return MakeMonochrome(Image.FromFile(sourceImageFile));
        }
        //--------------------------------------------------------------------------
        /// <summary>
        /// Creates a monochrome version of an image
        /// </summary>
        /// <param name="source">The image to convert to monochrome. The source image is not altered in any way.</param>
        /// <returns>The monochrome image</returns>
        public static Image MakeMonochrome(this Image source)
        {
            //Define the monochrome conversion matrix (luminance calculation values)
            var grayMatrix = new ColorMatrix(
                                    new float[][]{   
										new float[]{0.3f,0.3f,0.3f,0,0},
										new float[]{0.59f,0.59f,0.59f,0,0},
										new float[]{0.11f,0.11f,0.11f,0,0},
										new float[]{0,0,0,1,0,0},
										new float[]{0,0,0,0,1,0},
										new float[]{0,0,0,0,0,1}
									}
                                  );

            //Create the image attributes to apply
            var attribs = new ImageAttributes();
            attribs.SetColorMatrix(grayMatrix);

            //Create the blank canvas and the graphics object
            Bitmap canvas = new Bitmap(source.Width, source.Height);
            var g = Graphics.FromImage(canvas);

            //Draw the image using the matrix
            var rect = new Rectangle(0, 0, source.Width, source.Height);
            g.DrawImage(source, rect, 0, 0, source.Width, source.Height, GraphicsUnit.Pixel, attribs);

            return canvas;
        }

        #endregion

    }
}