﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    /// <summary>
    /// Summary description for Education
    /// </summary>
    [Serializable]
    public class Education
    {
        public string DiplomaName { get;set; }
        public string CanadianUniversity { get; set; }
        public string OtherUniversity { get;set; }
        public string OtherUniversityNotListed { get; set; }
        public string ProvinceState { get; set; }
        public string Country { get; set; }
        public string GraduationYear { get; set; }
        public string StudyField { get; set; }

    }
}