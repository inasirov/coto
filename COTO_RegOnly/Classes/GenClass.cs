﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Classes
{
    /// <summary>
    /// Summary description for GenClass
    /// </summary>
    [Serializable]
    public class GenClass
    {
        protected string _Code;
        protected string _Substitute;
        protected string _Description;

        public string Code
        {
            get
            {
                return _Code;
            }
            set
            {
                _Code = value;
            }
        }

        public string Substitute
        {
            get
            {
                return _Substitute;
            }
            set
            {
                _Substitute = value;
            }
        }

        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
            }
        }

    }
}