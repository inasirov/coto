﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    [Serializable]
    public class Declaration
    {
        protected string _RegistrationDeclaration;
        
        protected string _QADeclaration;

        protected bool _ApplicationDeclaration;
        protected DateTime? _ApplicationDeclarationDate;

        protected bool _ApplicationAuthorization;
        protected DateTime? _ApplicationAuthorizationDate;

        protected bool _PaymentApplicationFee;
        protected double _Satisfaction;
        protected string _SatisfactionNotes;

        public bool NewsletterUpdates { get; set; }

        public string RegistrationDeclaration
        {
            get
            {
                return _RegistrationDeclaration;
            }
            set
            {
                _RegistrationDeclaration = value;
            }
        }

        public string QADeclaration
        {
            get
            {
                return _QADeclaration;
            }
            set
            {
                _QADeclaration = value;
            }
        }

        public bool ApplicationAuthorization
        {
            get
            {
                return _ApplicationAuthorization;
            }
            set
            {
                _ApplicationAuthorization = value;
            }
        }

        public DateTime? ApplicationAuthorizationDate
        {
            get
            {
                return _ApplicationAuthorizationDate;
            }
            set
            {
                _ApplicationAuthorizationDate = value;
            }
        }

        public bool ApplicationDeclaration
        {
            get
            {
                return _ApplicationDeclaration;
            }
            set
            {
                _ApplicationDeclaration = value;
            }
        }

        public DateTime? ApplicationDeclarationDate
        {
            get
            {
                return _ApplicationDeclarationDate;
            }
            set
            {
                _ApplicationDeclarationDate = value;
            }
        }

        public bool PaymentApplicationFee
        {
            get
            {
                return _PaymentApplicationFee;
            }
            set
            {
                _PaymentApplicationFee = value;
            }
        }

        public double Satisfaction
        {
            get
            {
                return _Satisfaction;
            }
            set
            {
                _Satisfaction = value;
            }
        }

        public string SatisfactionNotes
        {
            get
            {
                return _SatisfactionNotes;
            }
            set
            {
                _SatisfactionNotes = value;
            }
        }
    }
}