﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    /// <summary>
    /// Class present user's Currency and Language Services
    /// </summary>
    [Serializable]
    public class CurrencyLanguage
    {
        protected string _Currency;
        protected string _Lang_Service1;
        protected string _Lang_Service2;
        protected string _Lang_Service3;
        protected string _Lang_Service4;
        protected string _Lang_Service5;
        protected string _First_Language;
        protected string _Lang_OT_Instr;
        protected string _Lang_Preferred_Doc;
        public string Currency
        {
            get
            {
                return _Currency;
            }
            set
            {
                _Currency = value;
            }
        }

        public string Lang_Service1
        {
            get
            {
                return _Lang_Service1;
            }
            set
            {
                _Lang_Service1 = value;
            }
        }

        public string Lang_Service2
        {
            get
            {
                return _Lang_Service2;
            }
            set
            {
                _Lang_Service2 = value;
            }
        }

        public string Lang_Service3
        {
            get
            {
                return _Lang_Service3;
            }
            set
            {
                _Lang_Service3 = value;
            }
        }

        public string Lang_Service4
        {
            get
            {
                return _Lang_Service4;
            }
            set
            {
                _Lang_Service4 = value;
            }
        }

        public string Lang_Service5
        {
            get
            {
                return _Lang_Service5;
            }
            set
            {
                _Lang_Service5 = value;
            }
        }

        public string First_Language
        {
            get
            {
                return _First_Language;
            }
            set
            {
                _First_Language = value;
            }
        }

        public string Lang_OT_Instr
        {
            get
            {
                return _Lang_OT_Instr;
            }
            set
            {
                _Lang_OT_Instr = value;
            }
        }

        public string Lang_Preferred_Doc
        {
            get
            {
                return _Lang_Preferred_Doc;
            }
            set
            {
                _Lang_Preferred_Doc = value;
            }
        }
    }
}