﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;


namespace Classes
{
    public class Tools
    {

        public bool SendConfirmationEmail(string CurrentId, string EmailMessage, string EmailSubject, string EmailAddressTo, string EmailAddressToCC)
        {
            string fromAddress = WebConfigItems.RegistrationContactEmail.ToLower();
            string toAddress = EmailAddressTo.ToLower();

            var repository = new Repository();
            //-----------SEND EMAIL---
            try
            {
                ////repository.AddErrorEntry("Message created before sending email confirmation",  CurrentId, DateTime.Now);
                //SmtpClient smtp = new SmtpClient(WebConfigItems.RegistrationHostService, WebConfigItems.RegistrationHostServicePort);
                //smtp.EnableSsl = WebConfigItems.RegistrationHostServiceEnableSSL;
                ////smtp.EnableSsl = true;
                //smtp.Credentials = new NetworkCredential(fromAddress, WebConfigItems.RegistrationContactEmailPwd);

                SmtpClient smtp = new SmtpClient();

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(fromAddress, "COTO Email System");
                //repository.AddErrorEntry(string.Format("Message of email confirmation:<br /> From: {0},<br /> To: {1},<br /> Subject: {2}, Email Message: {3}", fromAddress, toAddress, EmailSubject, EmailMessage), CurrentId, DateTime.Now);

                mail.To.Add(toAddress);
                if (!string.IsNullOrEmpty(EmailAddressToCC))
                {
                    mail.CC.Add(EmailAddressToCC);
                }
                mail.Subject = EmailSubject;
                mail.Body = EmailMessage;
                mail.IsBodyHtml = true;

                smtp.Send(mail);

                //repository.AddErrorEntry("Message created after sending email confirmation - email sent successfully!", CurrentId, DateTime.Now);
            }
            catch (Exception ex)
            {
                repository = new Repository();
                string message = string.Format("Message of email confirmation, error message from catch/catch block:<br /> Error Message: {0}, <br />", ex.Message);
                message += string.Format("Error Message Details: {0},<br />", ex.StackTrace);
                message += string.Format("From: {0},<br /> ", fromAddress);
                message += string.Format("To: {0},<br /> ", toAddress);
                message += string.Format("Subject: {0},", EmailSubject);
                message += string.Format("Email Message: {0}", EmailMessage);
                repository.AddErrorEntry(message, "0", DateTime.Now);
            }
            return true;
        }

        public bool SendConfirmationEmailAlternate(string CurrentId, string EmailMessageText, AlternateView EmailMessage, string EmailSubject, string EmailAddressTo, string EmailAddressToCC)
        {
            string fromAddress = WebConfigItems.RegistrationContactEmail.ToLower();
            string toAddress = EmailAddressTo.ToLower();

            var repository = new Repository();
            //-----------SEND EMAIL---
            try
            {
                //repository.AddErrorEntry("Message created before sending email confirmation", CurrentId, DateTime.Now);
                SmtpClient smtp = new SmtpClient();

                //SmtpClient smtp = new SmtpClient(WebConfigItems.RegistrationHostService, WebConfigItems.RegistrationHostServicePort);
                //smtp.EnableSsl = WebConfigItems.RegistrationHostServiceEnableSSL;
                //smtp.EnableSsl = true;
                //smtp.Credentials = new NetworkCredential(fromAddress, WebConfigItems.RegistrationContactEmailPwd);

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(fromAddress, "COTO Email System");
                //repository.AddErrorEntry(string.Format("Message of email confirmation:<br /> From: {0},<br /> To: {1},<br /> Subject: {2}, Email Message: {3}", fromAddress, toAddress, EmailSubject, EmailMessage), CurrentId, DateTime.Now);

                mail.To.Add(toAddress);
                if (!string.IsNullOrEmpty(EmailAddressToCC))
                {
                    mail.CC.Add(EmailAddressToCC);
                }
                mail.Subject = EmailSubject;
                //mail.Body = EmailMessage;
                mail.Body = EmailMessageText;
                mail.AlternateViews.Add(EmailMessage);
                mail.IsBodyHtml = true;

                smtp.Send(mail);

                //repository.AddErrorEntry("Message created after sending email confirmation - email sent successfully!", CurrentId, DateTime.Now);
            }
            catch (Exception ex)
            {
                //AddMessage(string.Format("Error message: {0}", ex.Message), PageMessageType.Critical, string.Empty);
                //return false;
                try
                {
                    //repository.AddErrorEntry(string.Format("Message of email confirmation, error message from catch block:<br /> Error Message: {4}, <br /> Error Message Details: {5}, <br /> From: {0},  To: {1},<br /> Subject: {2}, Email Message: {3}", fromAddress, toAddress, EmailSubject, EmailMessage, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : ex.StackTrace), "0", DateTime.Now);
                }
                catch (Exception ex2)
                {
                    repository = new Repository();
                    string message = string.Format("Message of email confirmation, error message from catch/catch block:<br /> Error Message: {0}, <br />", ex2.Message);
                    message += string.Format("Error Message Details: {0},<br />", ex2.StackTrace);
                    message += string.Format("From: {0},<br /> ", fromAddress);
                    message += string.Format("To: {0},<br /> ", toAddress);
                    message += string.Format("Subject: {0},", EmailSubject);
                    message += string.Format("Email Message: {0}", EmailMessage);
                    //repository.AddErrorEntry(message, "0", DateTime.Now);
                    //AddMessage(string.Format("Error message: {0}", ex.Message), PageMessageType.Critical, string.Empty);
                    //return false;
                }
            }
            return true;
        }

        /// <summary>
        ///   Allow developer adds custom errors, warning into Messages Collection.
        /// </summary>
        /// <param name="message">Message information.</param>
        /// <param name="messageType">Message type.</param>
        public void AddMessage(string message)
        {
            
            Messages.Add(new PageMessage(message, PageMessageType.Message));
        }

        /// <summary>
        ///  Messages collection. Contains all custom warning or errors which will be populated on the page.
        /// </summary>
        public PageMessages Messages
        {
            get
            {
                return (PageMessages)SessionParameters.PageMessages;
            }
            set
            {
                SessionParameters.PageMessages = value;
            }
        }
    }
}