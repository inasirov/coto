﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace Classes
{
    /// <summary>
    /// Summary description for PageMessages
    /// </summary>
    [Serializable]
    public class PageMessages : CollectionBase
    {
        #region Constructors
        public PageMessages()
            : base()
        {
        }
        #endregion

        #region Methods
        public void Add(PageMessage message)
        {
            List.Add(message);
        }

        public void Remove(PageMessage message)
        {
            List.Remove(message);
        }
        #endregion
    }
}