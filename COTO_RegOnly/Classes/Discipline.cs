﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    [Serializable]
    public class Discipline
    {
        public DateTime RefToDiscComm { get; set; }
        public string SummaryAllegation { get; set; }
        public string DiscHearingDate { get; set; }
        public string PenaltyHearingDate { get; set; }
        public string HearingPanelMembers { get; set; }
        
        public DateTime DecisionFinding { get; set; }
        public string PublicationBanTerms { get; set; }
        public string HearingOutcome { get; set; }
        public string HearingSummary { get; set; }

        protected DateTime _DecisionMade;
        //protected DateTime _DiscHearingDate;
        //protected DateTime _PenaltyHearingDate;
        protected string _HearAllegations;
        protected string _HearOutcomeSummary;
        protected string _AdditionalInfo;

        public DateTime DecisionMade
        {
            get
            {
                return _DecisionMade;
            }
            set
            {
                _DecisionMade = value;
            }
        }

        //public DateTime DiscHearingDate
        //{
        //    get
        //    {
        //        return _DiscHearingDate;
        //    }
        //    set
        //    {
        //        _DiscHearingDate = value;
        //    }
        //}

        //public DateTime PenaltyHearingDate
        //{
        //    get
        //    {
        //        return _PenaltyHearingDate;
        //    }
        //    set
        //    {
        //        _PenaltyHearingDate = value;
        //    }
        //}

        public string HearAllegations
        {
            get
            {
                return _HearAllegations;
            }
            set
            {
                _HearAllegations = value;
            }
        }

        public string HearOutcomeSummary
        {
            get
            {
                return _HearOutcomeSummary;
            }
            set
            {
                _HearOutcomeSummary = value;
            }
        }

        public string AdditionalInfo
        {
            get
            {
                return _AdditionalInfo;
            }
            set
            {
                _AdditionalInfo = value;
            }
        }
    }
}