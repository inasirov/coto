﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    /// <summary>
    /// Summary description for UserEducation
    /// </summary>
    [Serializable]
    public class UserEducation
    {
        public Education EntryDegree { get; set; }
        public Education Degree2 { get; set; }
        public Education Degree3 { get; set; }
        public Education OtherDegree1 { get; set; }
        public Education OtherDegree2 { get; set; }
    }
}