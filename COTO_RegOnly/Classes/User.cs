﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using COTO_RegOnly.Classes;

namespace Classes
{

    /// <summary>
    /// Summary description for User
    /// </summary>
    [Serializable]
    public class User
    {
        protected string _Id;
        protected string _CoId;
        protected long _Row_Number;
        protected string _FullName;
        protected string _FirstName;
        protected string _LastName;
        protected string _MiddleName;
        protected string _InformalName;

        protected string _LegalLastName;
        protected string _LegalFirstName;
        protected string _LegalMiddleName;

        protected string _PreviousLegalLastName;
        protected string _PreviousLegalFirstName;

        protected string _CommonlyUsedLastName;
        protected string _CommonlyUsedFirstName;
        protected string _CommonlyUsedMiddleName;

        protected DateTime _BirthDate;
        protected DateTime _RegisterBy;
        protected string _Gender;
        protected Boolean _Preferred_Mailing;
        protected Boolean _Preferred_Billing;

        protected string _Registration;
        protected string _Category;
        protected string _Citizenship;
        protected string _CurrentStatus;
        protected string _CurrentStatusCode;
        protected bool _EmploymentOffer;
        protected string _MemberType;
        protected string _NaturePractice;
        protected DateTime? _InitialRegistrationDate;
        protected DateTime _CertificateExpiry;
        protected string _Email;
        protected string _HomePhone;
        protected Address _HomeAddress;
        protected string _FullAddress;
        protected string _FullAddressDB;
        protected string _CurrentEmploymentStatus;
        protected string _CurrentWorkPreference;
        protected List<Employment> _EmploymentList;
        protected bool? _MoreThreePractSites;
        protected string _CollegeMailings;
        protected CurrencyLanguage _CurrencyLanguageServices;
        protected Conduct _ConductInfo;
        protected Insurance _InsuranceInfo;
        protected Examination _ExaminationInfo;
        protected Declaration _DeclarationInfo;
        protected ProfessionalHistory _ProfessionalHistoryInfo;
        protected UserEducation _Education;
        protected EmploymentProfile _EmploymentProfile;
        protected PracticeHistory _PracticeHistory;

        protected List<Activity> _PreviousLastNames;

        public string Id
        {
            get
            {
                return _Id;
            }
            set
            {
                _Id = value;
            }
        }

        public string CoId
        {
            get
            {
                return _CoId;
            }
            set
            {
                _CoId = value;
            }
        }

        public long Row_Number
        {
            get
            {
                return _Row_Number;
            }
            set
            {
                _Row_Number = value;
            }
        }

        public string FullName
        {
            get
            {
                return _FullName;
            }
            set
            {
                _FullName = value;
            }
        }

        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
            }
        }

        public string MiddleName
        {
            get
            {
                return _MiddleName;
            }
            set
            {
                _MiddleName = value;
            }
        }

        public string InformalName
        {
            get
            {
                return _InformalName;
            }
            set
            {
                _InformalName = value;
            }
        }

        public string LegalLastName
        {
            get
            {
                return _LegalLastName;
            }
            set
            {
                _LegalLastName = value;
            }
        }

        public string LegalFirstName
        {
            get
            {
                return _LegalFirstName;
            }
            set
            {
                _LegalFirstName = value;
            }
        }

        public string LegalMiddleName
        {
            get
            {
                return _LegalMiddleName;
            }
            set
            {
                _LegalMiddleName = value;
            }
        }

        public string PreviousLegalLastName
        {
            get
            {
                return _PreviousLegalLastName;
            }
            set
            {
                _PreviousLegalLastName = value;
            }
        }

        public string PreviousLegalFirstName
        {
            get
            {
                return _PreviousLegalFirstName;
            }
            set
            {
                _PreviousLegalFirstName = value;
            }
        }

        public string CommonlyUsedLastName
        {
            get
            {
                return _CommonlyUsedLastName;
            }
            set
            {
                _CommonlyUsedLastName = value;
            }
        }

        public string CommonlyUsedFirstName
        {
            get
            {
                return _CommonlyUsedFirstName;
            }
            set
            {
                _CommonlyUsedFirstName = value;
            }
        }

        public string CommonlyUsedMiddleName
        {
            get
            {
                return _CommonlyUsedMiddleName;
            }
            set
            {
                _CommonlyUsedMiddleName = value;
            }
        }

        public DateTime BirthDate
        {
            get
            {
                return _BirthDate;
            }
            set
            {
                _BirthDate = value;
            }
        }

        public DateTime RegisterBy
        {
            get
            {
                return _RegisterBy;
            }
            set
            {
                _RegisterBy = value;
            }
        }

        public string Gender
        {
            get
            {
                return _Gender;
            }
            set
            {
                _Gender = value;
            }
        }

        public string GenderSelfDescribe { get; set; }

        public Boolean Preferred_Mailing
        {
            get
            {
                return _Preferred_Mailing;
            }
            set
            {
                _Preferred_Mailing = value;
            }
        }

        public Boolean Preferred_Billing
        {
            get
            {
                return _Preferred_Billing;
            }
            set
            {
                _Preferred_Billing = value;
            }
        }

        public string Registration
        {
            get
            {
                return _Registration;
            }
            set
            {
                _Registration = value;
            }
        }

        public string Category
        {
            get
            {
                return _Category;
            }
            set
            {
                _Category = value;
            }
        }

        public string Citizenship
        {
            get
            {
                return _Citizenship;
            }
            set
            {
                _Citizenship = value;
            }
        }

        public string CurrentStatus
        {
            get
            {
                return _CurrentStatus;
            }
            set
            {
                _CurrentStatus = value;
            }
        }

        public string CurrentStatusCode
        {
            get
            {
                return _CurrentStatusCode;
            }
            set
            {
                _CurrentStatusCode = value;
            }
        }

        public bool EmploymentOffer
        {
            get
            {
                return _EmploymentOffer;
            }
            set
            {
                _EmploymentOffer = value;
            }
        }

        public string MemberType
        {
            get
            {
                return _MemberType;
            }
            set
            {
                _MemberType = value;
            }
        }

        public string CurrentEmploymentStatus
        {
            get
            {
                return _CurrentEmploymentStatus;
            }
            set
            {
                _CurrentEmploymentStatus = value;
            }
        }

        public string CurrentWorkPreference
        {
            get
            {
                return _CurrentWorkPreference;
            }
            set
            {
                _CurrentWorkPreference = value;
            }
        }

        public string NaturePractice
        {
            get
            {
                return _NaturePractice;
            }
            set
            {
                _NaturePractice = value;
            }
        }

        public DateTime? InitialRegistrationDate
        {
            get
            {
                return _InitialRegistrationDate;
            }
            set
            {
                _InitialRegistrationDate = value;
            }
        }

        public DateTime CertificateExpiry
        {
            get
            {
                return _CertificateExpiry;
            }
            set
            {
                _CertificateExpiry = value;
            }
        }

        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                _Email = value;
            }
        }

        public string HomePhone
        {
            get
            {
                return _HomePhone;
            }
            set
            {
                _HomePhone = value;
            }
        }

        public Address HomeAddress
        {
            get
            {
                return _HomeAddress;
            }
            set
            {
                _HomeAddress = value;
            }
        }

        public List<Employment> EmploymentList
        {
            get
            {
                return _EmploymentList;
            }
            set
            {
                _EmploymentList = value;
            }
        }

        public bool? MoreThreePractSites
        {
            get
            {
                return _MoreThreePractSites;
            }
            set
            {
                _MoreThreePractSites = value;
            }
        }

        public string CollegeMailings
        {
            get
            {
                return _CollegeMailings;
            }
            set
            {
                _CollegeMailings = value;
            }
        }

        public string FullAddress
        {
            get
            {
                if (string.IsNullOrEmpty(_FullAddress))
                {
                    string fullAddress = FullFormattedAddress;
                    return fullAddress;
                }
                else
                    return _FullAddress;
            }
            set
            {
                _FullAddress = value;
            }
        }

        public string FullAddressDB
        {
            get
            {
                if (string.IsNullOrEmpty(_FullAddressDB))
                {
                    string fullAddress = FullFormattedAddressDB;
                    return fullAddress;
                }
                else
                    return _FullAddressDB;
            }
            set
            {
                _FullAddressDB = value;
            }
        }

        public string FullFormattedAddress
        {
            get
            {
                string fullAddress = string.Empty;

                if (HomeAddress != null)
                {
                    if (!string.IsNullOrEmpty(HomeAddress.Address1))
                        fullAddress = HomeAddress.Address1 + "<br />";
                    if (!string.IsNullOrEmpty(HomeAddress.Address2))
                        fullAddress += HomeAddress.Address2 + "<br />";
                    if (!string.IsNullOrEmpty(HomeAddress.Address3))
                        fullAddress += HomeAddress.Address3 + "<br />";
                    string secondLine = string.Empty;
                    if (!string.IsNullOrEmpty(HomeAddress.City))
                        secondLine = HomeAddress.City;
                    if (!string.IsNullOrEmpty(HomeAddress.Province))
                    {
                        if (!string.IsNullOrEmpty(secondLine))
                        {
                            secondLine += ", " + HomeAddress.Province;
                        }
                        else
                        {
                            secondLine = HomeAddress.Province;
                        }
                    }

                    if (!string.IsNullOrEmpty(HomeAddress.PostalCode))
                    {
                        if (!string.IsNullOrEmpty(secondLine))
                        {
                            secondLine += " " + HomeAddress.PostalCode.ToUpper();
                        }
                        else
                        {
                            secondLine = HomeAddress.PostalCode.ToUpper();
                        }
                    }
                    if (!string.IsNullOrEmpty(secondLine))
                        fullAddress += secondLine + "<br />";
                    if (!string.IsNullOrEmpty(HomeAddress.Country))
                        fullAddress += HomeAddress.Country;

                }
                return fullAddress;
            }
        }

        public string FullFormattedAddressDB
        {
            get
            {
                string fullAddress = string.Empty;

                if (HomeAddress != null)
                {
                    if (!string.IsNullOrEmpty(HomeAddress.Address1))
                        fullAddress = HomeAddress.Address1;
                    if (!string.IsNullOrEmpty(HomeAddress.Address2))
                        if (!string.IsNullOrEmpty(fullAddress))
                        {
                            fullAddress += " " + HomeAddress.Address2;
                        }
                        else
                        {
                            fullAddress = HomeAddress.Address2;
                        }
                            
                    if (!string.IsNullOrEmpty(HomeAddress.Address3))
                        if (!string.IsNullOrEmpty(fullAddress))
                        {
                            fullAddress += " " + HomeAddress.Address3;
                        }
                        else
                        {
                            fullAddress = HomeAddress.Address3;
                        }
                        
                    string secondLine = string.Empty;
                    if (!string.IsNullOrEmpty(HomeAddress.City))
                        secondLine = HomeAddress.City;
                    if (!string.IsNullOrEmpty(HomeAddress.Province))
                    {
                        if (!string.IsNullOrEmpty(secondLine))
                        {
                            secondLine += ", " + HomeAddress.Province;
                        }
                        else
                        {
                            secondLine = HomeAddress.Province;
                        }
                    }

                    if (!string.IsNullOrEmpty(HomeAddress.PostalCode))
                    {
                        if (!string.IsNullOrEmpty(secondLine))
                        {
                            secondLine += " " + HomeAddress.PostalCode;
                        }
                        else
                        {
                            secondLine = HomeAddress.PostalCode;
                        }
                    }
                    if (!string.IsNullOrEmpty(secondLine))
                        fullAddress += " " + secondLine;
                    if (!string.IsNullOrEmpty(HomeAddress.Country))
                        fullAddress += " " + HomeAddress.Country;

                }
                return fullAddress;
            }
        }

        public CurrencyLanguage CurrencyLanguageServices
        {
            get
            {
                return _CurrencyLanguageServices;
            }
            set
            {
                _CurrencyLanguageServices = value;
            }
        }

        public Conduct ConductInfo
        {
            get
            {
                return _ConductInfo;
            }
            set
            {
                _ConductInfo = value;
            }
        }

        public Insurance InsuranceInfo
        {
            get
            {
                return _InsuranceInfo;
            }
            set
            {
                _InsuranceInfo = value;
            }
        }

        public Examination ExaminationInfo
        {
            get
            {
                return _ExaminationInfo;
            }
            set
            {
                _ExaminationInfo = value;
            }
        }

        public Declaration DeclarationInfo
        {
            get
            {
                return _DeclarationInfo;
            }
            set
            {
                _DeclarationInfo = value;
            }
        }

        public ProfessionalHistory ProfessionalHistoryInfo
        {
            get
            {
                return _ProfessionalHistoryInfo;
            }
            set
            {
                _ProfessionalHistoryInfo = value;
            }
        }

        public UserEducation Education
        {
            get
            {
                return _Education;
            }
            set
            {
                _Education = value;
            }
        }

        public EmploymentProfile EmploymentProfile
        {
            get
            {
                return _EmploymentProfile;
            }
            set
            {
                _EmploymentProfile = value;
            }
        }

        public PracticeHistory PracticeHistory
        {
            get
            {
                return _PracticeHistory;
            }
            set
            {
                _PracticeHistory = value;
            }
        }

        public List<Activity> PreviousLastNames
        {
            get
            {
                return _PreviousLastNames;
            }
            set
            {
                _PreviousLastNames = value;
            }
        }

        public string SelfIdentificationQuestion1
        {
            get;set;
        }

        public string SelfIdentificationQuestion2 { get; set; }
    }

    [Serializable]
    public class Address
    {
        protected string _Address1;
        protected string _Address2;
        protected string _Address3;
        protected string _City;
        protected string _PostalCode;
        protected string _Province;
        protected string _Country;

        public string Address1
        {
            get
            {
                return _Address1;
            }
            set
            {
                _Address1 = value;
            }
        }

        public string Address2
        {
            get
            {
                return _Address2;
            }
            set
            {
                _Address2 = value;
            }
        }

        public string Address3
        {
            get
            {
                return _Address3;
            }
            set
            {
                _Address3 = value;
            }
        }

        public string City
        {
            get
            {
                return _City;
            }
            set
            {
                _City = value;
            }
        }

        public string PostalCode
        {
            get
            {
                return _PostalCode;
            }
            set
            {
                _PostalCode = value;
            }
        }

        public string Province
        {
            get
            {
                return _Province;
            }
            set
            {
                _Province = value;
            }
        }

        public string Country
        {
            get
            {
                return _Country;
            }
            set
            {
                _Country = value;
            }
        }

    }
}