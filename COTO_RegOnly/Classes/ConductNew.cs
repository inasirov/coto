﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    [Serializable]
    public class ConductNew
    {
        public string UserID { get; set; }
        public int SEQN { get; set; }
        public string RenewalYear { get; set; }
        public DateTime ReportedDate { get; set; }
        public string ReportType { get; set; }
        public string RefusedByRegBody { get; set; }
        public string RefusedByRegBodyDetails { get; set; }
        public string FindMisconductIncomp { get; set; }
        public string FindMisconductIncompDetails { get; set; }
        public string FacingMisconduct { get; set; }
        public string FacingMisconductDetails { get; set; }
        public string FindNegMalpract {get;set;}
        public string FindNegMalpractDetails { get; set; }
        public string PrevConduct { get; set; }
        public string PrevConductDetails { get; set; }

        public string Guilty_Authority { get; set; }
        public string Guilty_Authority_Details { get; set; }
        public string CondRestrict { get; set; }
        public string CondRestrictDetails { get; set; }

        public string ChargedOffence { get; set; }
        public string ChargedOffenceDetails { get; set; }

        public string EventCircumstance { get; set; }
        public string EventCircumstanceDetails { get; set; }
    }
}