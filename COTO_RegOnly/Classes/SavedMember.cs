﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Classes
{
    [Serializable]
    public class SavedMember
    {
        public string UserID { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string FullName { get; set; }
        public string PreferredEmail { get; set; }
        public string AlternateEmail { get; set; }
        public string Website { get; set; }
        public string CompanyName { get; set; }
        public string JobTitle { get; set; }
        public string JobTitleValue { get; set; }
        public string IndustryType { get; set; }
        public string IndustryTypeValue { get; set; }

        public string BusinessBilling { get; set; }
        public string BusinessMailing { get; set; }
        public string BusinessAddress { get; set; }
        public string BusinessAddress1 { get; set; }
        public string BusinessAddress2 { get; set; }
        public string BusinessAddress3 { get; set; }
        public string BusinessCity { get; set; }
        public string BusinessCountry { get; set; }
        public string BusinessPostalCode { get; set; }
        public string BusinessProvince { get; set; }
        public string BusinessProvinceValue { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessFax { get; set; }

        public string HomeBilling { get; set; }
        public string HomeMailing { get; set; }
        public string HomeAddress { get; set; }
        public string HomeAddress1 { get; set; }
        public string HomeAddress2 { get; set; }
        public string HomeAddress3 { get; set; }
        public string HomeCity { get; set; }
        public string HomeCountry { get; set; }
        public string HomePostalCode { get; set; }
        public string HomeProvince { get; set; }
        public string HomeProvinceValue { get; set; }
        public string HomePhone { get; set; }
        public string HomeFax { get; set; }

        public string CanadianResident { get; set; }
        public string RegisteredHST { get; set; }

        public string CommSendNewsletters { get; set; }
        public string CommThirdParties { get; set; }
        public string CommProvideInfoLocalChapters { get; set; }

        public string LoginName { get; set; }
        public string Password { get; set; }
    }
}
