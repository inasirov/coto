﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Classes
{
    /// <summary>
    /// Summary description for Employment
    /// </summary>
    [Serializable]
    public class Employment
    {
        protected int _Index;
        protected string _Status;
        protected string _EmployerName;
        protected Address _Address;
        protected string _PostalCodeReflectPractice;
        protected string _Phone;
        protected string _Fax;
        protected DateTime? _StartDate;
        protected DateTime? _EndDate;
        protected string _EmploymentRelationship;
        protected string _CasualStatus;
        protected double _AverageWeeklyHours;
        protected string _PrimaryRole;
        protected string _PracticeSetting;
        protected string _MajorServices;
        protected string _ClientAgeRange;
        protected string _FundingSource;
        protected string _HealthCondition;

        public int Index
        {
            get
            {
                return _Index;
            }
            set
            {
                _Index = value;
            }
        }

        public string Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }

        public string EmployerName
        {
            get
            {
                return _EmployerName;
            }
            set
            {
                _EmployerName = value;
            }
        }

        public Address Address
        {
            get
            {
                return _Address;
            }
            set
            {
                _Address = value;
            }
        }

        public string PostalCodeReflectPractice
        {
            get
            {
                return _PostalCodeReflectPractice;
            }
            set
            {
                _PostalCodeReflectPractice = value;
            }
        }

        public string Phone
        {
            get
            {
                return _Phone;
            }
            set
            {
                _Phone = value;
            }
        }

        public string Fax
        {
            get
            {
                return _Fax;
            }
            set
            {
                _Fax = value;
            }
        }

        public DateTime? StartDate
        {
            get
            {
                return _StartDate;
            }
            set
            {
                _StartDate = value;
            }
        }

        public DateTime? EndDate
        {
            get
            {
                return _EndDate;
            }
            set
            {
                _EndDate = value;
            }
        }

        public string EmploymentRelationship
        {
            get
            {
                return _EmploymentRelationship;
            }
            set
            {
                _EmploymentRelationship = value;
            }
        }

        public string CasualStatus
        {
            get
            {
                return _CasualStatus;
            }
            set
            {
                _CasualStatus = value;
            }
        }

        public double AverageWeeklyHours
        {
            get
            {
                return _AverageWeeklyHours;
            }
            set
            {
                _AverageWeeklyHours = value;
            }
        }

        public string PrimaryRole
        {
            get
            {
                return _PrimaryRole;
            }
            set
            {
                _PrimaryRole = value;
            }
        }

        public string PracticeSetting
        {
            get
            {
                return _PracticeSetting;
            }
            set
            {
                _PracticeSetting = value;
            }
        }

        public string MajorServices
        {
            get
            {
                return _MajorServices;
            }
            set
            {
                _MajorServices = value;
            }
        }

        public string ClientAgeRange
        {
            get
            {
                return _ClientAgeRange;
            }
            set
            {
                _ClientAgeRange = value;
            }
        }

        public string FundingSource
        {
            get
            {
                return _FundingSource;
            }
            set
            {
                _FundingSource = value;
            }
        }

        public string HealthCondition
        {
            get
            {
                return _HealthCondition;
            }
            set
            {
                _HealthCondition = value;
            }
        }

    }
}