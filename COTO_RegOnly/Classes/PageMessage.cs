﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Classes
{
    /// <summary>
    /// Summary description for PageMessage
    /// </summary>
    [Serializable]
    public class PageMessage
    {
        #region Variables
        private string _strMessage;
        private PageMessageType _messageType;
        private Exception _exception;
        #endregion

        #region Constructors
        public PageMessage(string message, PageMessageType messageType)
        {
            _strMessage = message;
            _messageType = messageType;
        }

        public PageMessage(string message, PageMessageType messageType, Exception exception)
        {
            _strMessage = message;
            _messageType = messageType;
            _exception = exception;
        }

        #endregion

        #region Properties
        public string Message
        {
            get { return _strMessage; }
            set { _strMessage = value; }
        }

        public PageMessageType MessageType
        {
            get { return _messageType; }
            set { _messageType = value; }
        }

        public Exception Exception
        {
            get { return _exception; }
        }

        #endregion
    }

    public enum PageMessageType
    {
        Critical = 1,
        UserError,
        Message
    }
}

