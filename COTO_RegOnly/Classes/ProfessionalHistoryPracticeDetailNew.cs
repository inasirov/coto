﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    public class ProfessionalHistoryPracticeDetailNew
    {
        public int SEQN { get; set; }
        public int OrderNum { get; set; }
        public string RegType { get; set; }
        public string RegulatoryBody { get; set; }
        public string RegulatoryBodyDescription { get; set; }
        public string RegulatoryBodyOther { get; set; }

        public string OtherProfessionType { get; set; }
        public string OtherProfessionTypeDescription { get; set; }
        public string OtherProfessionTypeOther { get; set; }
        public string OtherRegulatoryBody { get; set; }
        public string OtherRegulatoryBodyDescription { get; set; }
        public string OtherRegulatoryBodyOther { get; set; }

        public string Province { get; set; }
        public string ProvinceDescription { get; set; }
        public string Country { get; set; }
        public string CountryDescription { get; set; }
        public string RegNumber { get; set; }
        public string RegStatus { get; set; }
        public string InitMonth { get; set; }
        public string InitYear { get; set; }
        public DateTime? ExpiryDate { get; set; }
    }
}