﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    [Serializable]
    public class Subscription
    {
        public string ID { get; set; }
        public string ProductCode { get; set; }
        public decimal Amount { get; set; }

    }
}