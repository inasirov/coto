﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    public class ControlledAct
    {
        public string Comm_Diagnosis_1 { get; set; }
        public string Below_Dermis_2 { get; set; }
        public string Setting_Casting_3 { get; set; }
        public string Spinal_Manipulation_4 { get; set; }
        public string Administer_Substance_5 { get; set; }
        public string Instr_Hand_Finger_6 { get; set; }
        public string Xrays_7 { get; set; }
        public string Drugs_8 { get; set; }
        public string Vision_9 { get; set; }
        public string Psyhotherapy_10 { get; set; }
        public string Acupuncture_11 { get; set; }
    }
}