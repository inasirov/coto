﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    [Serializable]
    public class Examination
    {
        protected string _ExaminationStatus;
        protected DateTime _CompletionDate;
        protected DateTime _ExaminationDate;
        protected string _ExaminationDateExt;

        public string ExaminationStatus
        {
            get
            {
                return _ExaminationStatus;
            }
            set
            {
                _ExaminationStatus = value;
            }
        }

        public DateTime CompletionDate
        {
            get
            {
                return _CompletionDate;
            }
            set
            {
                _CompletionDate = value;
            }
        }

        public DateTime ExaminationDate
        {
            get
            {
                return _ExaminationDate;
            }
            set
            {
                _ExaminationDate = value;
            }
        }

        public string ExaminationDateExt
        {
            get
            {
                return _ExaminationDateExt;
            }
            set
            {
                _ExaminationDateExt = value;
            }
        }

    }
}