﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Classes
{

    /// <summary>
    /// Summary description for WebConfigItems
    /// </summary>
    public class WebConfigItems
    {
        // hardcoded in one place only
        public static string DatabaseConnectionString = "SqlServerConnection";
        public static string GetConnectionString()
        {
            string connectionString = string.Empty;
            if (!string.IsNullOrEmpty(System.Web.Configuration.WebConfigurationManager.ConnectionStrings[DatabaseConnectionString].ToString()))
            {
                connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings[DatabaseConnectionString].ToString();
            }

            return connectionString;
        }

        protected static string GetConfigSetting(string ConfigName)
        {
            string result = "";
            if (ConfigurationManager.AppSettings[ConfigName] != null)
                result = ConfigurationManager.AppSettings[ConfigName];
            return result;
        }

        /// <summary>
        ///  Physical location of storing photo images
        /// </summary>
        public static string GetCOTOWelcomePageUrl
        {
            get
            {
                return GetConfigSetting("COTOWelcomePageUrl");
            }
        }

        public static bool DevMode
        {
            get
            {
                string parameter = GetConfigSetting("DevMode");
                if (!string.IsNullOrEmpty(parameter))
                {
                    return bool.Parse(parameter);
                }
                return false;
            }
        }

        public static int RenewalYear
        {
            get
            {
                string parameter = GetConfigSetting("RenewalYear");
                if (!string.IsNullOrEmpty(parameter))
                {
                    return int.Parse(parameter);
                }
                return DateTime.Now.Year;
            }
        }

        /// <summary>
        ///  Physical location of storing photo images
        /// </summary>
        public static string GetTestLabel
        {
            get
            {
                return GetConfigSetting("EmailTestHeader");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step0
        {
            get
            {
                return GetConfigSetting("Step0");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step0_ResignCertificate
        {
            get
            {
                return GetConfigSetting("Step0_ResignCertificate");
            }
        }
        /// <summary>
        ///  
        /// </summary>
        public static string Step1
        {
            get
            {
                return GetConfigSetting("Step1");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step1_EditName
        {
            get
            {
                return GetConfigSetting("Step1_EditName");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step2
        {
            get
            {
                return GetConfigSetting("Step2");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step3
        {
            get
            {
                return GetConfigSetting("Step3");
            }
        }
        
        /// <summary>
        ///  
        /// </summary>
        public static string Step3_UpdateEducationProfile
        {
            get
            {
                return GetConfigSetting("Step3_UpdateEducationProfile");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step4
        {
            get
            {
                return GetConfigSetting("Step4");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step5
        {
            get
            {
                return GetConfigSetting("Step5");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step6
        {
            get
            {
                return GetConfigSetting("Step6");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step7
        {
            get
            {
                return GetConfigSetting("Step7");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step8_UpdatePracticeHistoryProfile
        {
            get
            {
                return GetConfigSetting("Step8_UpdatePracticeHistoryProfile");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step8
        {
            get
            {
                return GetConfigSetting("Step8");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step9
        {
            get
            {
                return GetConfigSetting("Step9");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step10
        {
            get
            {
                return GetConfigSetting("Step10");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step11
        {
            get
            {
                return GetConfigSetting("Step11");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Step12
        {
            get
            {
                return GetConfigSetting("Step12");
            }
        }

        public static string Step12_PaymentConfirmation
        {
            get
            {
                return GetConfigSetting("Step12_PaymentConfirmation");
            }
        }

        public static string Step12_RegistrationSummary
        {
            get
            {
                return GetConfigSetting("Step12_RegistrationSummary");
            }
        }

        public static string Step12_OfflinePayment
        {
            get
            {
                return GetConfigSetting("Step12_OfflinePayment");
            }
        }

        public static string Step12_Feedback
        {
            get
            {
                return GetConfigSetting("Step12_Feedback");
            }
        }

        public static string Step12_PDF_Report
        {
            get
            {
                return GetConfigSetting("Step12_PDF_Report");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string RegistrationContactEmail
        {
            get
            {
                return GetConfigSetting("RegContact");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string RegistrationContactEmailPwd
        {
            get
            {
                return GetConfigSetting("RegContactPwd");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string RegistrationRegistrationContactEmail
        {
            get
            {
                return GetConfigSetting("RegRegistrationContact");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string RegistrationManagerContactEmail
        {
            get
            {
                return GetConfigSetting("RegManagerContact");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string RegistrationConductDeclaredEmail
        {
            get
            {
                return GetConfigSetting("RegistrationConductEmail");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string RegistrationSupportEmail
        {
            get
            {
                return GetConfigSetting("RegSupportContact");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string RegistrationQualityEmail
        {
            get
            {
                return GetConfigSetting("RegistrationQualityEmail");
            }
        }
        public static string RegistrationHostService
        {
            get
            {
                return GetConfigSetting("RegHostService");
            }
        }

        public static int RegistrationHostServicePort
        {
            get
            {
                int port = 25;
                string portString = GetConfigSetting("RegHostServicePort");
                if (!string.IsNullOrEmpty(portString))
                {
                    int.TryParse(portString, out port);
                }
                return port;
            }
        }

        public static bool RegistrationHostServiceEnableSSL
        {
            get
            {
                string parameter = GetConfigSetting("GegHostServiceEnableSSL");
                if (!string.IsNullOrEmpty(parameter))
                {
                    return bool.Parse(parameter);
                }
                return false;
            }
        }

        public static bool SaveErrorMessage
        {
            get
            {
                string parameter = GetConfigSetting("SaveErrorMessages");
                if (!string.IsNullOrEmpty(parameter))
                {
                    return bool.Parse(parameter);
                }
                return false;
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step0
        {
            get
            {
                return GetConfigSetting("Application_Step0");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step1
        {
            get
            {
                return GetConfigSetting("Application_Step1");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step2
        {
            get
            {
                return GetConfigSetting("Application_Step2");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step2_UpdatePersonalInfo
        {
            get
            {
                return GetConfigSetting("Application_Step2_UpdatePersonalInfo");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step2_EmployerForm
        {
            get
            {
                return GetConfigSetting("Application_Step2_EmployerForm");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step3
        {
            get
            {
                return GetConfigSetting("Application_Step3");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step3_LanguageRequirementLink
        {
            get
            {
                return GetConfigSetting("Application_Step3_LanguageRequirementLink");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step3_RHF_Link
        {
            get
            {
                return GetConfigSetting("Application_Step3_RHF_Link");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step3_LMSA_Link
        {
            get
            {
                return GetConfigSetting("Application_Step3_LMSA_Link");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step3_NotMeetCurrencyRequirementLink
        {
            get
            {
                return GetConfigSetting("Application_Step3_NotMeetCurrencyRequirementLink");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step4
        {
            get
            {
                return GetConfigSetting("Application_Step4");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step4_UpdateEducationInfo
        {
            get
            {
                return GetConfigSetting("Application_Step4_UpdateEducationInfo");
            }
        }
        
        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step5
        {
            get
            {
                return GetConfigSetting("Application_Step5");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step6
        {
            get
            {
                return GetConfigSetting("Application_Step6");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step6_RegulatoryForm
        {
            get
            {
                return GetConfigSetting("Application_Step6_RegulatoryForm");
            }
        }
        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step7
        {
            get
            {
                return GetConfigSetting("Application_Step7");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step7_UpdatePracticeHistory
        {
            get
            {
                return GetConfigSetting("Application_Step7_UpdatePracticeHistory");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step8
        {
            get
            {
                return GetConfigSetting("Application_Step8");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step9
        {
            get
            {
                return GetConfigSetting("Application_Step9");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step9_LabourMobilitSupportAgreementConfirmationForm
        {
            get
            {
                return GetConfigSetting("Application_Step3_LMSA_Link");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step9_RegulatoryHistoryForm
        {
            get
            {
                return GetConfigSetting("Application_Step9_RegulatoryHistoryForm");
            }
        }
        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step10
        {
            get
            {
                return GetConfigSetting("Application_Step10");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step11
        {
            get
            {
                return GetConfigSetting("Application_Step11");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step11_Offline_Payment
        {
            get
            {
                return GetConfigSetting("Application_Step11_Offline_Payment");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step11_PDF_Report
        {
            get
            {
                return GetConfigSetting("Application_Step11_PDF_Report");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step11_Feedback
        {
            get
            {
                return GetConfigSetting("Application_Step11_Feedback");
            }
        }

        /// <summary>
        ///  
        /// </summary>
        public static string Application_Step11_PaymentConfirmation
        {
            get
            {
                return GetConfigSetting("Application_Step11_PaymentConfirmation");
            }
        }
        
        /// <summary>
        ///  
        /// </summary>
        public static string ApplicationSupportEmail
        {
            get
            {
                return GetConfigSetting("ApplicationSupportContact");
            }
        }
    }
}