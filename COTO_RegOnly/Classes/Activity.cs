﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{   
    [Serializable]
    public class Activity
    {
        protected string _ID;
        protected string _ActivityType;
        protected string _UF_1;
        protected string _UF_2;
        protected string _UF_3;

        public string ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }

        public string ActivityType
        {
            get
            {
                return _ActivityType;
            }
            set
            {
                _ActivityType = value;
            }
        }

        public string UF_1
        {
            get
            {
                return _UF_1;
            }
            set
            {
                _UF_1 = value;
            }
        }

        public string UF_2
        {
            get
            {
                return _UF_2;
            }
            set
            {
                _UF_2 = value;
            }
        }

        public string UF_3
        {
            get
            {
                return _UF_3;
            }
            set
            {
                _UF_3 = value;
            }
        }

    }
}