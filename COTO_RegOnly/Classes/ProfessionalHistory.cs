﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTO_RegOnly.Classes
{
    [Serializable]
    public class ProfessionalHistory
    {
        protected string _Id;
        protected string _RegisteredPractiseInOtherProvince;
        protected string _RegisteredPractiseInOtherProfession;
        protected ProfessionalHistoryPracticeDetail _PracticeDetail1;
        protected ProfessionalHistoryPracticeDetail _PracticeDetail2;
        protected ProfessionalHistoryPracticeDetail _PracticeDetail3;
        protected ProfessionalHistoryPracticeDetail _PracticeDetail4;

        protected ProfessionalHistoryProfessionDetail _ProfessionDetail1;
        protected ProfessionalHistoryProfessionDetail _ProfessionDetail2;

        public List<ProfessionalHistoryPracticeDetailNew> PracticeDetails { get; set; }
        public List<ProfessionalHistoryPracticeDetailNew> PracticeOtherDetails { get; set; }

        public string Id
        {
            get
            {
                return _Id;
            }
            set
            {
                _Id = value;
            }
        }

        public string RegisteredPractiseInOtherProvince
        {
            get
            {
                return _RegisteredPractiseInOtherProvince;
            }
            set
            {
                _RegisteredPractiseInOtherProvince = value;
            }
        }

        public string RegisteredPractiseInOtherProfession
        {
            get
            {
                return _RegisteredPractiseInOtherProfession;
            }
            set
            {
                _RegisteredPractiseInOtherProfession = value;
            }
        }

        public ProfessionalHistoryPracticeDetail PracticeDetail1
        {
            get
            {
                return _PracticeDetail1;
            }
            set
            {
                _PracticeDetail1 = value;
            }
        }

        public ProfessionalHistoryPracticeDetail PracticeDetail2
        {
            get
            {
                return _PracticeDetail2;
            }
            set
            {
                _PracticeDetail2 = value;
            }
        }

        public ProfessionalHistoryPracticeDetail PracticeDetail3
        {
            get
            {
                return _PracticeDetail3;
            }
            set
            {
                _PracticeDetail3 = value;
            }
        }

        public ProfessionalHistoryPracticeDetail PracticeDetail4
        {
            get
            {
                return _PracticeDetail4;
            }
            set
            {
                _PracticeDetail4 = value;
            }
        }

        public ProfessionalHistoryProfessionDetail ProfessionDetail1
        {
            get
            {
                return _ProfessionDetail1;
            }
            set
            {
                _ProfessionDetail1 = value;
            }
        }

        public ProfessionalHistoryProfessionDetail ProfessionDetail2
        {
            get
            {
                return _ProfessionDetail2;
            }
            set
            {
                _ProfessionDetail2 = value;
            }
        }
    }
}