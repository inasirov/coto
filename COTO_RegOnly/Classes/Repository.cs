﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using COTO_RegOnly.Classes;
using System.Data.SqlClient;
//using Asi;
//using Asi.Web;
//using Asi.iBO;
//using Asi.iBO.ContactManagement;
//using Asi.iBO.SystemConfig;
//using Asi.iBO.Commerce;

namespace Classes
{
    /// <summary>
    /// Summary description for Repository
    /// </summary>
    public class Repository
    {
        public User GetUserInfo(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT Name.*, Curr.* FROM Name " +
                "  INNER JOIN COTO_REGDATA Curr ON Name.Id = Curr.id " +      // CURRENCY
                " WHERE Name.ID = '" + User_Id + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User
                {
                    FullName = (string)row["FULL_NAME"],
                    FullAddress = (string)row["FULL_ADDRESS"],

                    LegalLastName = (string)row["Legal_Last_Name"],
                    LegalFirstName = (string)row["Legal_First_Name"],
                    LegalMiddleName = (string)row["MIDDLE_NAME"],
                    PreviousLegalLastName = (string)row["LEGAL_PREVIOUS_LAST_NAME"],
                    PreviousLegalFirstName = (string)row["LEGAL_PREVIOUS_FIRST_NAME"],
                    CommonlyUsedLastName = (string)row["LAST_NAME"],
                    CommonlyUsedFirstName = (string)row["FIRST_NAME"],
                    CommonlyUsedMiddleName = (string)row["Middle_Name"],
                    //HomeAddress = new Address{
                    //    Address1 = (string)row["ADDRESS_1"],
                    //    Address2 = (string)row["ADDRESS_2"],
                    //    Address3 = (string)row["ADDRESS_3"],
                    //    City = (string)row["CITY"],
                    //    Province = (string)row["STATE_PROVINCE"],
                    //    PostalCode = (string)row["ZIP"],
                    //    Country = (string)row["COUNTRY"]
                    //},
                    FirstName = (string)row["FIRST_NAME"],
                    LastName = (string)row["LAST_NAME"],
                    Registration = (string)row["MAJOR_KEY"],
                    Email = (string)row["EMAIL"],
                    HomePhone = (string)row["HOME_PHONE"],
                    CurrentStatus = (string)row["STATUS"],
                };

                if (row["PAID_THRU"] != DBNull.Value)
                {
                    returnValue.CertificateExpiry = (DateTime)row["PAID_THRU"];
                }

                if (row["BIRTH_DATE_COTO"] != DBNull.Value)
                {
                    returnValue.BirthDate = (DateTime)row["BIRTH_DATE_COTO"];
                }
                else
                {
                    returnValue.BirthDate = DateTime.MinValue;
                }


                returnValue.Category = GetGeneralName("CATEGORY", (string)row["CATEGORY"]);
                returnValue.CurrentStatus = GetGeneralName("MEMBER_STATUS", (string)row["STATUS"]);
            }

            returnValue.HomeAddress = new Address();
            string sql_address = "SELECT * FROM Name_Address WHERE ID = '" + User_Id + "' AND PURPOSE='Home'";
            var table_address = db.GetData(sql_address);
            if (table_address != null && table_address.Rows.Count>0)
            {
                var row = table_address.Rows[0];
                var address = new Address
                    {
                        Address1 = (string)row["ADDRESS_1"],
                        Address2 = (string)row["ADDRESS_2"],
                        Address3 = (string)row["ADDRESS_3"],
                        City = (string)row["CITY"],
                        Province = (string)row["STATE_PROVINCE"],
                        PostalCode = (string)row["ZIP"],
                        Country = (string)row["COUNTRY"]
                    };
                returnValue.HomeAddress = address;
            }
            return returnValue;
        }

        public User GetUserGeneralInfo(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT n.ID," +
                        " n.FIRST_NAME," +
                        " n.FULL_NAME," +
                        " n.Middle_Name," +
                        " n.LAST_NAME," +
                        " n.COMPANY," +
                        " n.WORK_PHONE," +
                        " n.FAX," +
                        " n.EMAIL," +
                        " n.MEMBER_TYPE," +
                        " n.MAJOR_KEY," +
                        " n.CATEGORY," +
                        " n.STATUS," +
                        " n.co_id," +
                        " n.join_Date," +
                        " n.paid_thru," +
                        " ANN_REG_ADDL_INFO.LANG_OTHER," +
                        " EMPLOYMENT_ALL.CURRENT_EMPLOYMENT_STATUS," +
                        " EMPLOYMENT_ALL.EMP_1_NAME," +
                        " EMPLOYMENT_ALL.EMP_1_ADDR_1," +
                        " EMPLOYMENT_ALL.EMP_1_ADDR_2," +
                        " EMPLOYMENT_ALL.EMP_1_CITY, " +
                        " EMPLOYMENT_ALL.EMP_1_PROV," +
                        " EMPLOYMENT_ALL.EMP_1_POSTAL," +
                        " EMPLOYMENT_ALL.EMP_1_PHONE," +
                        " EMPLOYMENT_ALL.EMP_1_FUNDING_SOURCE," +
                        " EMPLOYMENT_ALL.EMP_1_AREA_PRACTICE," +
                        " EMPLOYMENT_ALL.EMP_1_CLNT_ARNGE," +
                        " EMPLOYMENT_ALL.EMP_1_HEALTH_CONDITION," +
                        " EMPLOYMENT_ALL.EMP_1_TYPE," +
                        " EMPLOYMENT_ALL.EMP_2_NAME," +
                        " EMPLOYMENT_ALL.EMP_2_ADDR_1," +
                        " EMPLOYMENT_ALL.EMP_2_ADDR_2," +
                        " EMPLOYMENT_ALL.EMP_2_CITY, " +
                        " EMPLOYMENT_ALL.EMP_2_PROV," +
                        " EMPLOYMENT_ALL.EMP_2_POSTAL," +
                        " EMPLOYMENT_ALL.EMP_2_PHONE," +
                        " EMPLOYMENT_ALL.EMP_2_FUNDING_SOURCE," +
                        " EMPLOYMENT_ALL.EMP_2_AREA_PRACTICE," +
                        " EMPLOYMENT_ALL.EMP_2_CLNT_ARNGE," +
                        " EMPLOYMENT_ALL.EMP_2_TYPE, " +
                        " EMPLOYMENT_ALL.EMP_2_HEALTH_CONDITION," +
                        " EMPLOYMENT_ALL.EMP_3_NAME," +
                        " EMPLOYMENT_ALL.EMP_3_ADDR_1," +
                        " EMPLOYMENT_ALL.EMP_3_ADDR_2," +
                        " EMPLOYMENT_ALL.EMP_3_CITY, " +
                        " EMPLOYMENT_ALL.EMP_3_PROV," +
                        " EMPLOYMENT_ALL.EMP_3_POSTAL," +
                        " EMPLOYMENT_ALL.EMP_3_PHONE," +
                        " EMPLOYMENT_ALL.EMP_3_FUNDING_SOURCE," +
                        " EMPLOYMENT_ALL.EMP_3_AREA_PRACTICE," +
                        " EMPLOYMENT_ALL.EMP_3_CLNT_ARNGE," +
                        " EMPLOYMENT_ALL.EMP_3_TYPE, " +
                        " EMPLOYMENT_ALL.EMP_3_HEALTH_CONDITION," +
                        " COTO_REGDATA.LEGAL_PREVIOUS_LAST_NAME," +
                        " COTO_REGDATA.LEGAL_PREVIOUS_FIRST_NAME," +
                        " COTO_REGDATA.LEGAL_LAST_NAME," +
                        " COTO_REGDATA.LEGAL_FIRST_NAME," +
                        " COTO_REGDATA.MIDDLE_NAME AS [LEGAL_MIDDLE_NAME]," +
                        " COTO_REGDATA.LANG_OF_SVCE_1," +
                        " COTO_REGDATA.LANG_OF_SVCE_2," +
                        " COTO_REGDATA.LANG_OF_SVCE_3," +
                        " COTO_REGDATA.LANG_OF_SVCE_4," +
                        " COTO_REGDATA.LANG_OF_SVCE_5," +
                        " COTO_REGDATA.prov_cert_expiry_date," +
                        " COTO_REGDATA.INITIAL_JOIN_DATE," +
                        " COTO_REGDATA.BIRTH_DATE_COTO" +
                        " FROM		Name as n," +
                        " ANN_REG_ADDL_INFO," +
                        " EMPLOYMENT_ALL, " +
                        " COTO_REGDATA " +
                        " WHERE n.id = ANN_REG_ADDL_INFO.ID" +
                        " AND n.id = EMPLOYMENT_ALL.ID" +
                        " AND n.id = COTO_REGDATA.id" +
                        " AND" +
                        " (" +
                            " (	" +
                                " n.STATUS not in ('P','D') and n.member_type='reg' " +
                            " ) " +
                            " OR " +
                            " ( " +
                                " n.STATUS  in ('AF','IF') and n.member_type='APP' " +
                            " ) " +
                        " ) " +
                        " AND n.ID = " + Escape(User_Id);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User
                {
                    Id = (string)row["ID"],
                    CoId = (string)row["CO_ID"],
                    FullName = (string)row["FULL_NAME"],
                    //FullAddress = (string)row["FULL_ADDRESS"],

                    LegalLastName = (string)row["Legal_Last_Name"],
                    LegalFirstName = (string)row["Legal_First_Name"],
                    LegalMiddleName = (string)row["LEGAL_MIDDLE_NAME"],
                    PreviousLegalLastName = (string)row["LEGAL_PREVIOUS_LAST_NAME"],
                    PreviousLegalFirstName = (string)row["LEGAL_PREVIOUS_FIRST_NAME"],
                    CommonlyUsedLastName = (string)row["LAST_NAME"],
                    CommonlyUsedFirstName = (string)row["FIRST_NAME"],
                    //HomeAddress = new Address{
                    //    Address1 = (string)row["ADDRESS_1"],
                    //    Address2 = (string)row["ADDRESS_2"],
                    //    Address3 = (string)row["ADDRESS_3"],
                    //    City = (string)row["CITY"],
                    //    Province = (string)row["STATE_PROVINCE"],
                    //    PostalCode = (string)row["ZIP"],
                    //    Country = (string)row["COUNTRY"]
                    //},
                    FirstName = (string)row["FIRST_NAME"],
                    LastName = (string)row["LAST_NAME"],
                    MiddleName = (string)row["MIDDLE_NAME"],

                    MemberType = (string)row["MEMBER_TYPE"],
                    Registration = (string)row["MAJOR_KEY"],
                    Email = (string)row["EMAIL"],
                    //HomePhone = (string)row["HOME_PHONE"],
                    //InitialRegistrationDate = (DateTime?)row["INITIAL_JOIN_DATE"],
                    //CertificateExpiry = (DateTime?)row["PAID_THRU"],
                    CurrentStatus = (string)row["STATUS"],
                    CurrentStatusCode = (string)row["STATUS"],
                };

                if (row["INITIAL_JOIN_DATE"] != DBNull.Value)
                {
                    returnValue.InitialRegistrationDate = (DateTime?)row["INITIAL_JOIN_DATE"];
                }

                if (row["PAID_THRU"] != DBNull.Value)
                {
                    returnValue.CertificateExpiry = (DateTime)row["PAID_THRU"];
                }

                if (row["BIRTH_DATE_COTO"] != DBNull.Value)
                {
                    returnValue.BirthDate = (DateTime)row["BIRTH_DATE_COTO"];
                }
                else
                {
                    returnValue.BirthDate = DateTime.MinValue;
                }


                returnValue.Category = GetGeneralName("CATEGORY", (string)row["CATEGORY"]);
                returnValue.CurrentStatus = GetGeneralName("MEMBER_STATUS", (string)row["STATUS"]);
                returnValue.CurrencyLanguageServices = new CurrencyLanguage();
                returnValue.CurrencyLanguageServices.Lang_Service1 = GetGeneralName("LANGUAGE_INST_AH", (string)row["LANG_OF_SVCE_1"]);
                returnValue.CurrencyLanguageServices.Lang_Service2 = GetGeneralName("LANGUAGE_INST_AH", (string)row["LANG_OF_SVCE_2"]);
                returnValue.CurrencyLanguageServices.Lang_Service3 = GetGeneralName("LANGUAGE_INST_AH", (string)row["LANG_OF_SVCE_3"]);
                returnValue.CurrencyLanguageServices.Lang_Service4 = GetGeneralName("LANGUAGE_INST_AH", (string)row["LANG_OF_SVCE_4"]);
                returnValue.CurrencyLanguageServices.Lang_Service5 = GetGeneralName("LANGUAGE_INST_AH", (string)row["LANG_OF_SVCE_5"]);

                string sql_address = "SELECT * FROM Name_Address WHERE ID = '" + User_Id + "' AND PURPOSE='Home'";
                var table_address = db.GetData(sql_address);
                if (table_address != null && table_address.Rows.Count > 0)
                {
                    var row1 = table_address.Rows[0];
                    var address = new Address
                    {
                        Address1 = (string)row1["ADDRESS_1"],
                        Address2 = (string)row1["ADDRESS_2"],
                        Address3 = (string)row1["ADDRESS_3"],
                        City = (string)row1["CITY"],
                        Province = (string)row1["STATE_PROVINCE"],
                        PostalCode = (string)row1["ZIP"],
                        Country = (string)row1["COUNTRY"]
                    };
                    returnValue.HomeAddress = address;
                }

                returnValue.CurrentEmploymentStatus = (string)row["CURRENT_EMPLOYMENT_STATUS"];
                returnValue.EmploymentList = new List<Employment>();
                if (!string.IsNullOrEmpty((string)row["EMP_1_NAME"]))
                {
                    var employment1 = new Employment();
                    employment1.Index = 1;
                    employment1.EmployerName = (string)row["EMP_1_NAME"];
                    employment1.Address = new Address();
                    employment1.Address.Address1 = (string)row["EMP_1_ADDR_1"];
                    employment1.Address.Address2 = (string)row["EMP_1_ADDR_2"];
                    employment1.Address.City = (string)row["EMP_1_CITY"];
                    employment1.Address.Province = (string)row["EMP_1_PROV"];
                    employment1.Address.PostalCode = (string)row["EMP_1_POSTAL"];
                    employment1.Phone = (string)row["EMP_1_PHONE"];
                    employment1.FundingSource = GetGeneralNameImprv("FUNDING", (string)row["EMP_1_FUNDING_SOURCE"]);
                    employment1.PracticeSetting = GetGeneralNameImprv("EMPLOYER_TYPE", (string)row["EMP_1_TYPE"]);
                    employment1.MajorServices = GetGeneralNameImprv("MAIN_AREA_PRACTICE", (string)row["EMP_1_AREA_PRACTICE"]);
                    employment1.ClientAgeRange = GetGeneralNameImprv("CLIENT_AGE_RANGE", (string)row["EMP_1_CLNT_ARNGE"]);
                    returnValue.EmploymentList.Add(employment1);
                }
                if (!string.IsNullOrEmpty((string)row["EMP_2_NAME"]))
                {
                    var employment2 = new Employment();
                    employment2.Index = 2;
                    employment2.EmployerName = (string)row["EMP_2_NAME"];
                    employment2.Address = new Address();
                    employment2.Address.Address1 = (string)row["EMP_2_ADDR_1"];
                    employment2.Address.Address2 = (string)row["EMP_2_ADDR_2"];
                    employment2.Address.City = (string)row["EMP_2_CITY"];
                    employment2.Address.Province = (string)row["EMP_2_PROV"];
                    employment2.Address.PostalCode = (string)row["EMP_2_POSTAL"];
                    employment2.Phone = (string)row["EMP_2_PHONE"];
                    employment2.FundingSource = GetGeneralNameImprv("FUNDING", (string)row["EMP_2_FUNDING_SOURCE"]);
                    employment2.PracticeSetting = GetGeneralNameImprv("EMPLOYER_TYPE", (string)row["EMP_2_TYPE"]);
                    employment2.MajorServices = GetGeneralNameImprv("MAIN_AREA_PRACTICE", (string)row["EMP_2_AREA_PRACTICE"]);
                    employment2.ClientAgeRange = GetGeneralNameImprv("CLIENT_AGE_RANGE", (string)row["EMP_2_CLNT_ARNGE"]);
                    returnValue.EmploymentList.Add(employment2);
                }
                if (!string.IsNullOrEmpty((string)row["EMP_1_NAME"]))
                {
                    var employment3 = new Employment();
                    employment3.Index = 3;
                    employment3.EmployerName = (string)row["EMP_3_NAME"];
                    employment3.Address = new Address();
                    employment3.Address.Address1 = (string)row["EMP_3_ADDR_1"];
                    employment3.Address.Address2 = (string)row["EMP_3_ADDR_2"];
                    employment3.Address.City = (string)row["EMP_3_CITY"];
                    employment3.Address.Province = (string)row["EMP_3_PROV"];
                    employment3.Address.PostalCode = (string)row["EMP_3_POSTAL"];
                    employment3.Phone = (string)row["EMP_3_PHONE"];
                    employment3.FundingSource = GetGeneralNameImprv("FUNDING", (string)row["EMP_3_FUNDING_SOURCE"]);
                    employment3.PracticeSetting = GetGeneralNameImprv("EMPLOYER_TYPE", (string)row["EMP_3_TYPE"]);
                    employment3.MajorServices = GetGeneralNameImprv("MAIN_AREA_PRACTICE", (string)row["EMP_3_AREA_PRACTICE"]);
                    employment3.ClientAgeRange = GetGeneralNameImprv("CLIENT_AGE_RANGE", (string)row["EMP_3_CLNT_ARNGE"]);
                    returnValue.EmploymentList.Add(employment3);
                }

            }
            return returnValue;
        }

        public Corporation GetUserCorporationInfo(string Corp_Id)
        {
            Corporation returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT n.ID, " +
                        " n.FIRST_NAME, " +
                        " n.FULL_NAME, " +
                        " n.LAST_NAME, " +
                        " n.COMPANY, " +
                        " n.WORK_PHONE, " +
                        " n.FAX, " +
                        " n.EMAIL, " +
                        " n.MEMBER_TYPE, " +
                        " n.MAJOR_KEY, " +
                        " n.CATEGORY," +
                        " n.STATUS, " +
                        " n.CO_ID, " +
                        " n.JOIN_DATE, " +
                        " Name_Address.PURPOSE, " +
                        " Name_Address.Address_1, " +
                        " Name_Address.Address_2, " +
                        " Name_Address.City, " +
                        " Name_Address.State_Province, " +
                        " Name_Address.ZIP, " +
                        " Name_Address.Phone " +
                        " FROM Name n INNER JOIN " +
                        " Name_Address ON n.ID = Name_Address.ID " +
                        " WHERE (n.STATUS NOT IN ('P','D','X')) " +
                        " AND (n.MEMBER_TYPE  IN ('REG', 'CORP')) " +
                        " AND (Name_Address.PURPOSE = 'Primary Employment') " +
                        " AND n.member_type IN ('CORP') " +
                        " AND n.ID = " + Escape(Corp_Id);

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new Corporation
                {
                    FullName = (string)row["COMPANY"],
                    Certificate = (string)row["MAJOR_KEY"],
                    Status = GetGeneralName("MEMBER_STATUS", (string)row["STATUS"]),

                    PracticeName = (string)row["COMPANY"],
                    BusinessAddress = new Address
                    {
                        Address1 = (string)row["Address_1"],
                        Address2 = (string)row["Address_2"],
                        //Address3 = (string)row["Address_3"],
                        City = (string)row["CITY"],
                        PostalCode = (string)row["Zip"],
                        Province = (string)row["State_Province"]
                    },
                    Phone = (string)row["PHONE"],
                    Email = (string)row["Email"]
                };
                if (row["Join_Date"] != DBNull.Value)
                {
                    returnValue.StatusEffectiveDate = (DateTime?)row["Join_Date"];
                }
                else
                {
                    returnValue.StatusEffectiveDate = null;
                }

                returnValue.Shareholders = GetCorporationShareholders(Corp_Id);
            }

            return returnValue;
        }

        public List<User> GetCorporationShareholders(string Corp_Id)
        {
            List<User> returnValue = new List<User>();
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT First_Name, Last_Name, Major_Key FROM Name WHERE CO_ID = " + Escape(Corp_Id) + " AND Functional_title like  'SHARE%'";

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    var user = new User();
                    user.FirstName = (string)row["First_Name"];
                    user.LastName = (string)row["Last_Name"];
                    user.Registration = (string)row["Major_Key"];

                    returnValue.Add(user);
                }
            }
            return returnValue;
        }

        public User GetUserEmployment(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM EMPLOYMENT_ALL " +
                " WHERE ID = '" + User_Id + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.CurrentEmploymentStatus = (string)row["CURRENT_EMPLOYMENT_STATUS"];
                returnValue.CurrentWorkPreference = (string)row["FULL_PT_STATUS_PREF"];
                if (!string.IsNullOrEmpty((string)row["MORE_THREE_PRACT_SITES"]))
                {
                    returnValue.MoreThreePractSites = (((string)row["MORE_THREE_PRACT_SITES"]).ToLower() == "0" || ((string)row["MORE_THREE_PRACT_SITES"]).ToLower() == "no") ? false : true;
                }
                else
                {
                    returnValue.MoreThreePractSites = null;
                }
                
                returnValue.NaturePractice = (string)row["NATURE_OF_PRACTICE"];
                returnValue.CollegeMailings = (string)row["PRIMARY_EMPL_PREFERRED"];

                returnValue.EmploymentList = new List<Employment>();

                var employment1 = new Employment();
                employment1.Index = 1;
                employment1.Status = (string)row["EMP_1_SOURCE"];
                employment1.EmployerName = (string)row["EMP_1_NAME"];
                var address1 = new Address();
                address1.Address1 = (string)row["EMP_1_ADDR_1"];
                address1.Address2 = (string)row["EMP_1_ADDR_2"];
                address1.City = (string)row["EMP_1_CITY"];
                address1.Province = (string)row["EMP_1_PROV"];
                address1.PostalCode = (string)row["EMP_1_POSTAL"];
                address1.Country = (string)row["EMP_1_COUNTRY"];
                employment1.Address = address1;
                employment1.Phone = (string)row["EMP_1_PHONE"];
                employment1.Fax = (string)row["EMP_1_FAX"];

                employment1.PostalCodeReflectPractice = (string)row["EMP_1_PC_REF_SP"];



                if (row["EMP_1_START_DATE"] != DBNull.Value)
                {
                    employment1.StartDate = (DateTime?)row["EMP_1_START_DATE"];
                }
                else
                {
                    employment1.StartDate = null;
                }

                if (row["CURRENT_EMPLOYMENT_END_DATE"] != DBNull.Value)
                {
                    employment1.EndDate = (DateTime?)row["CURRENT_EMPLOYMENT_END_DATE"];
                }
                else
                {
                    employment1.EndDate = null;
                }
                //employment1.EndDate = (DateTime?)row["CURRENT_EMPLOYMENT_END_DATE"];

                employment1.EmploymentRelationship = (string)row["EMP_1_CATEGORY"];
                employment1.CasualStatus = (string)row["EMP_1_FULL_PT_STATUS"];
                employment1.AverageWeeklyHours = (double)row["EMP_1_HOURS_WORKED_WEEK"];
                employment1.PrimaryRole = (string)row["EMP_1_POSITION"];
                employment1.PracticeSetting = (string)row["EMP_1_TYPE"];
                employment1.MajorServices = (string)row["EMP_1_AREA_PRACTICE"];
                employment1.ClientAgeRange = (string)row["EMP_1_CLNT_ARNGE"];
                employment1.FundingSource = (string)row["EMP_1_FUNDING_SOURCE"];
                employment1.HealthCondition = (string)row["EMP_1_HEALTH_CONDITION"];

                returnValue.EmploymentList.Add(employment1);

                var employment2 = new Employment();
                employment2.Index = 2;
                employment2.Status = (string)row["EMP_2_SOURCE"];
                employment2.EmployerName = (string)row["EMP_2_NAME"];
                var address2 = new Address();
                address2.Address1 = (string)row["EMP_2_ADDR_1"];
                address2.Address2 = (string)row["EMP_2_ADDR_2"];
                address2.City = (string)row["EMP_2_CITY"];
                address2.Province = (string)row["EMP_2_PROV"];
                address2.PostalCode = (string)row["EMP_2_POSTAL"];
                address2.Country = (string)row["EMP_2_COUNTRY"];
                employment2.Address = address2;
                employment2.Phone = (string)row["EMP_2_PHONE"];
                employment2.Fax = (string)row["EMP_2_FAX"];

                employment2.PostalCodeReflectPractice = (string)row["EMP_2_PC_REF_SP"];

                //employment2.StartDate = (DateTime?)row["EMP_2_START_DATE"];
                //employment2.EndDate = (DateTime?)row["EMP_2_END_DATE"];

                if (row["EMP_2_START_DATE"] != DBNull.Value)
                {
                    employment2.StartDate = (DateTime?)row["EMP_2_START_DATE"];
                }
                else
                {
                    employment2.StartDate = null;
                }

                if (row["EMP_2_END_DATE"] != DBNull.Value)
                {
                    employment2.EndDate = (DateTime?)row["EMP_2_END_DATE"];
                }
                else
                {
                    employment2.EndDate = null;
                }

                employment2.EmploymentRelationship = (string)row["EMP_2_CATEGORY"];
                employment2.CasualStatus = (string)row["EMP_2_FULL_PT_STATUS"];
                employment2.AverageWeeklyHours = (double)row["EMP_2_HOURS_WORKED_WEEK"];
                employment2.PrimaryRole = (string)row["EMP_2_POSITION"];
                employment2.PracticeSetting = (string)row["EMP_2_TYPE"];
                employment2.MajorServices = (string)row["EMP_2_AREA_PRACTICE"];
                employment2.ClientAgeRange = (string)row["EMP_2_CLNT_ARNGE"];
                employment2.FundingSource = (string)row["EMP_2_FUNDING_SOURCE"];
                employment2.HealthCondition = (string)row["EMP_2_HEALTH_CONDITION"];

                returnValue.EmploymentList.Add(employment2);

                var employment3 = new Employment();
                employment3.Index = 3;
                employment3.Status = (string)row["EMP_3_SOURCE"];
                employment3.EmployerName = (string)row["EMP_3_NAME"];
                var address3 = new Address();
                address3.Address1 = (string)row["EMP_3_ADDR_1"];
                address3.Address2 = (string)row["EMP_3_ADDR_2"];
                address3.City = (string)row["EMP_3_CITY"];
                address3.Province = (string)row["EMP_3_PROV"];
                address3.PostalCode = (string)row["EMP_3_POSTAL"];
                address3.Country = (string)row["EMP_3_COUNTRY"];
                employment3.Address = address3;
                employment3.Phone = (string)row["EMP_3_PHONE"];
                employment3.Fax = (string)row["EMP_3_FAX"];

                employment3.PostalCodeReflectPractice = (string)row["EMP_3_PC_REF_SP"];

                //employment3.StartDate = (DateTime?)row["EMP_3_START_DATE"];
                //employment3.EndDate = (DateTime?)row["EMP_3_END_DATE"];

                if (row["EMP_3_START_DATE"] != DBNull.Value)
                {
                    employment3.StartDate = (DateTime?)row["EMP_3_START_DATE"];
                }
                else
                {
                    employment3.StartDate = null;
                }

                if (row["EMP_3_END_DATE"] != DBNull.Value)
                {
                    employment3.EndDate = (DateTime?)row["EMP_3_END_DATE"];
                }
                else
                {
                    employment3.EndDate = null;
                }

                employment3.EmploymentRelationship = (string)row["EMP_3_CATEGORY"];
                employment3.CasualStatus = (string)row["EMP_3_FULL_PT_STATUS"];
                employment3.AverageWeeklyHours = (double)row["EMP_3_HOURS_WORKED_WEEK"];
                employment3.PrimaryRole = (string)row["EMP_3_POSITION"];
                employment3.PracticeSetting = (string)row["EMP_3_TYPE"];
                employment3.MajorServices = (string)row["EMP_3_AREA_PRACTICE"];
                employment3.ClientAgeRange = (string)row["EMP_3_CLNT_ARNGE"];
                employment3.FundingSource = (string)row["EMP_3_FUNDING_SOURCE"];
                employment3.HealthCondition = (string)row["EMP_3_HEALTH_CONDITION"];

                returnValue.EmploymentList.Add(employment3);
            }
            return returnValue;
        }

        public User GetUserEmploymentProfileInfo(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM EMPLOYMENT_ALL " +
                        " WHERE ID = " + Escape(User_Id);

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.EmploymentProfile = new EmploymentProfile
                {

                    PracticeStatus = (string)row["CURRENT_EMPLOYMENT_STATUS"],
                    //CollegeMailings = (string)row["PRIMARY_EMPL_PREFERRED"],
                    CurrentWorkPreference = (string)row["FULL_PT_STATUS_PREF"],
                    StudentSupervisionLastYear = (string)row["PROVIDE_STUDENT_SUPERVISION"],
                    StudentSupervisionComingYear = (string)row["ANTICIPATE_STUDENT_SUPER"],
                    HaveClinicalClients = (string)row["NO_CLINICAL_CLIENTS"],
                    PracticingNumberWeeks = (int)row["NUMBER_WEEKS_PRACTICE_YR"],
                    PracticingNumberHoursPerWeek = (int)row["AVG_HOURS_PRACTICE_YR"],
                    NaturePractice = (string)row["NATURE_OF_PRACTICE"],
                    TimeSpentProfServices = (int)row["WEEKLY_HRS_DIRECT_PROFESSION"],
                    TimeSpentTeaching = (int)row["WEEKLY_HRS_TEACH"],
                    TimeSpentClinicalEducation = (int)row["WEEKLY_HRS_CLINIC_EDUC"],
                    TimeSpentResearch = (int)row["WEEKLY_HRS_RESEARCH"],
                    TimeSpentAdministration = (int)row["WEEKLY_HRS_ADMIN"],
                    TimeSpentOtherActivities = (int)row["WEEKLY_HRS_OTHER"]
                };

            }

            return returnValue;
        }

        // not using anymore
        public User GetUserPracticeHistoryInfoOld(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM COTO_REGDATA " +
                        " WHERE ID = " + Escape(User_Id);

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.PracticeHistory = new PracticeHistory
                {
                    FirstCountryPractice = (string)row["Country_First_Practice_Prof"],
                    FirstProvinceStatePractice = (string)row["Prov_State_First_Practice"],
                    FirstProvincePractice = (string)row["First_Cdn_Prov_Practice"],
                    FirstYearCanadianPractice = (string)row["First_Year_CDN_Pract_Prof"],
                    FirstYearPractice = (string)row["First_Year_Practice_Prof"],

                    LastCountryPractice = (string)row["Most_Recent_Prev_Country"],
                    LastProvincePractice = (string)row["PREVIOUS_PROV"],
                    LastProvinceStatePractice = (string)row["Most_Recent_Prev_Prov"],
                    LastYearOutsideOntarioPractice = (string)row["MOST_RECENT_PREV_YEAR"],

                    //PracticedOutsideOntario = (string)row["WEEKLY_HRS_TEACH"],
                };

            }

            return returnValue;
        }

        public User GetUserPracticeHistoryInfo(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM COTO_REGDATA " +
                        " WHERE ID = " + Escape(User_Id);

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.PracticeHistory = new PracticeHistory
                {
                    FirstYearPractice = (string)row["First_Year_Practice_Prof"],
                    FirstCountryPractice = (string)row["Country_First_Practice_Prof"],
                    FirstProvinceStatePractice = (string)row["Prov_State_First_Practice"],
                    FirstYearCanadianPractice = (string)row["First_Year_CDN_Pract_Prof"],
                    FirstProvincePractice = (string)row["First_Cdn_Prov_Practice"],

                    LastYearOutsideOntarioPractice = (string)row["MOST_RECENT_PREV_YEAR"],
                    LastCountryPractice = (string)row["Most_Recent_Prev_Country"],
                    LastProvinceStatePractice = (string)row["Most_Recent_Prev_Prov"],
                    //LastProvincePractice = (string)row["PREVIOUS_PROV"],
                    //PracticedOutsideOntario = (string)row["WEEKLY_HRS_TEACH"],
                };

            }

            return returnValue;
        }

        public string UpdateUserInfo(User user)
        {
            string errorMessage = string.Empty;
            try
            {
                //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
                string dbconn = WebConfigItems.GetConnectionString();
                var db = new clsDB();

                string working_sql = string.Empty;
                bool pref_Mail = false;

                string sql_GetHomeAddress = " SELECT * FROM Name_Address WHERE  ID = " + Escape(user.Id) + " AND PURPOSE='Home'";
                //db.ExecuteCommand(sql_GetHomeAddress);

                var table = db.GetData(sql_GetHomeAddress);
                if (table != null && table.Rows.Count == 1)
                {
                    DataRow row = table.Rows[0];
                    int address_num = (int)row["ADDRESS_NUM"];
                    pref_Mail = (bool)row["PREFERRED_MAIL"];
                    string prev_full_address = (string)row["FULL_ADDRESS"];

                    string sql_UpdateUserAddress = "UPDATE Name_Address SET " +
                        " PHONE = " + Escape(user.HomePhone) + ", " +
                        " ADDRESS_1 = " + Escape(user.HomeAddress.Address1) + ", " +
                        " ADDRESS_2 = " + Escape(user.HomeAddress.Address2) + ", " +
                        " ADDRESS_3 = " + Escape(user.HomeAddress.Address3) + ", " +
                        " CITY = " + Escape(user.HomeAddress.City) + ", " +
                        " STATE_PROVINCE = " + Escape(user.HomeAddress.Province) + ", " +
                        " ZIP = " + Escape(user.HomeAddress.PostalCode) + ", " +
                        " COUNTRY = " + Escape(user.HomeAddress.Country) + ", " +
                        " FULL_ADDRESS = " + Escape(user.FullAddressDB) + ", " +
                        " LAST_UPDATED = Getdate() " +
                        " WHERE ID = " + Escape(user.Id) + " AND PURPOSE='Home'";
                    working_sql = sql_UpdateUserAddress;
                    db.ExecuteCommand(sql_UpdateUserAddress);

                    // this section saves old values of email and home phone if they are different than new values
                    // also saves old value of full_address if it preferred mail
                    string prev_Email = string.Empty;
                    string prev_HomePhone = string.Empty;
                    string full_address = string.Empty;

                    string sql_GetNameInfo = " SELECT * FROM Name WHERE  ID = " + Escape(user.Id);
                    working_sql = sql_GetNameInfo;
                    var table2 = db.GetData(sql_GetNameInfo);

                    if (table2 != null && table2.Rows.Count == 1)
                    {
                        DataRow row2 = table2.Rows[0];
                        prev_Email = (string)row2["EMAIL"];
                        prev_HomePhone = (string)row2["HOME_PHONE"];
                        full_address = (string)row2["FULL_ADDRESS"];
                    }

                    if (!user.Email.Equals(prev_Email))
                    {
                        string sql_InsertNameLogRecord = "INSERT INTO Name_Log (DATE_TIME, LOG_TYPE, SUB_TYPE, USER_ID, ID, LOG_TEXT)";
                        sql_InsertNameLogRecord += string.Format("Values({0},{1},{2},{3},{4},{5})", "GETDATE()", "'CHANGE'", "'CHANGE'", Escape(user.Id), Escape(user.Id), Escape("Name.EMAIL: " + prev_Email + " -> " + user.Email));
                        working_sql = sql_InsertNameLogRecord;
                        db.ExecuteCommand(sql_InsertNameLogRecord);
                    }

                    if (!user.HomePhone.Equals(prev_HomePhone))
                    {
                        string sql_InsertNameLogRecord = "INSERT INTO Name_Log (DATE_TIME, LOG_TYPE, SUB_TYPE, USER_ID, ID, LOG_TEXT)";
                        sql_InsertNameLogRecord += string.Format("Values({0},{1},{2},{3},{4},{5})", "GETDATE()", "'CHANGE'", "'CHANGE'", Escape(user.Id), Escape(user.Id), Escape("Name.HOME_PHONE: " + prev_HomePhone + " -> " + user.HomePhone));
                        working_sql = sql_InsertNameLogRecord;
                        db.ExecuteCommand(sql_InsertNameLogRecord);
                    }

                    if (pref_Mail && !user.FullAddressDB.Equals(prev_full_address))
                    {
                        string sql_InsertNameLogRecord = "INSERT INTO Name_Log (DATE_TIME, LOG_TYPE, SUB_TYPE, USER_ID, ID, LOG_TEXT)";
                        sql_InsertNameLogRecord += string.Format("Values({0},{1},{2},{3},{4},{5})", "GETDATE()", "'CHANGE'", "'CHANGE'", Escape(user.Id), Escape(user.Id), Escape("Name.FULL_ADDRESS: " + prev_full_address + " -> " + user.FullAddressDB));
                        working_sql = sql_InsertNameLogRecord;
                        db.ExecuteCommand(sql_InsertNameLogRecord);
                    }
                }
                else if (table != null && table.Rows.Count == 0)
                {
                    int newSEQN = GetCounter("Name_Address");
                    string sql_InsertNewUserAddress = "INSERT INTO Name_Address " +
                        " (ID, ADDRESS_NUM, PURPOSE, ADDRESS_1, ADDRESS_2, ADDRESS_3, CITY, STATE_PROVINCE, ZIP, COUNTRY, FULL_ADDRESS, LAST_UPDATED, PHONE) " +
                        string.Format(" VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10, {11}, {12})", Escape(user.Id), newSEQN, "'HOME'", Escape(user.HomeAddress.Address1),
                        Escape(user.HomeAddress.Address2), Escape(user.HomeAddress.Address3), Escape(user.HomeAddress.City), Escape(user.HomeAddress.Province), Escape(user.HomeAddress.PostalCode),
                        Escape(user.HomeAddress.Country), Escape(user.FullAddressDB), "Getdate()", Escape(user.HomePhone));
                    db.ExecuteCommand(sql_InsertNewUserAddress);
                }

                string sql_UpdateUser = "UPDATE Name SET " +
                        " EMAIL = " + Escape(user.Email) +
                        ", HOME_PHONE = " + Escape(user.HomePhone);

                if (pref_Mail)
                {
                    sql_UpdateUser += ", FULL_ADDRESS = " + Escape(user.FullAddressDB);
                    sql_UpdateUser += ", CITY = " + Escape(user.HomeAddress.City);
                    sql_UpdateUser += ", STATE_PROVINCE = " + Escape(user.HomeAddress.Province);
                    sql_UpdateUser += ", ZIP = " + Escape(user.HomeAddress.PostalCode);
                    sql_UpdateUser += ", COUNTRY = " + Escape(user.HomeAddress.Country);
                }
                //if (user.BirthDate != DateTime.MinValue)
                //{
                //    sql_UpdateUser += ", BIRTH_DATE = '" + user.BirthDate.ToString("yyyy-MM-dd") + "'";
                //}
                //else
                //{
                //    sql_UpdateUser += ", BIRTH_DATE = NULL";
                //}

                sql_UpdateUser += " WHERE ID = " + Escape(user.Id);
                working_sql = sql_UpdateUser;
                db.ExecuteCommand(sql_UpdateUser);

                string sql_UpdateUserBirthDate = "UPDATE COTO_REGDATA SET ";
                if (user.BirthDate != DateTime.MinValue)
                {
                    sql_UpdateUserBirthDate += " BIRTH_DATE_COTO = '" + user.BirthDate.ToString("yyyy-MM-dd") + "'";
                }
                else
                {
                    sql_UpdateUserBirthDate += " BIRTH_DATE_COTO = NULL";
                }

                sql_UpdateUserBirthDate += " WHERE ID = " + Escape(user.Id);
                db.ExecuteCommand(sql_UpdateUserBirthDate);
            }
            catch (Exception ex)
            {
                errorMessage = "Error message: " + ex.Message + "; Error Inner Exception: " + ex.InnerException; // +"SQL = " + working_sql;
            }
            return errorMessage;

        }

        public string UpdateUserInfoLogged(string UserID, User user)
        {
            string errorMessage = string.Empty;
            try
            {

                string prevEmail = string.Empty;
                string prevHomePhone = string.Empty;
                string prevFullAddress = string.Empty;

                string prevHomeAddress1 = string.Empty;
                string prevHomeAddress2 = string.Empty;
                string prevHomeAddress3 = string.Empty;

                string prevHomeCity = string.Empty;
                string prevHomeProvince = string.Empty;
                string prevHomeCountry = string.Empty;
                string prevHomePostalCode = string.Empty;

                //var db = new clsDB();
                string dbconn = WebConfigItems.GetConnectionString();
                var db = new clsDB();


                bool pref_Mail = false;

                string sql_GetHomeAddress = " SELECT * FROM Name_Address WHERE  ID = " + Escape(user.Id) + " AND PURPOSE='Home'";
                //db.ExecuteCommand(sql_GetHomeAddress);

                var table = db.GetData(sql_GetHomeAddress);
                if (table != null && table.Rows.Count == 1)
                {
                    DataRow row = table.Rows[0];
                    int address_num = (int)row["ADDRESS_NUM"];
                    pref_Mail = (bool)row["PREFERRED_MAIL"];

                    prevHomePhone = (string)row["PHONE"];
                    prevFullAddress = (string)row["FULL_ADDRESS"];

                    prevHomeAddress1 = (string)row["ADDRESS_1"];
                    prevHomeAddress2 = (string)row["ADDRESS_2"];
                    prevHomeAddress3 = (string)row["ADDRESS_3"];

                    prevHomeCity = (string)row["CITY"];
                    prevHomeProvince = (string)row["STATE_PROVINCE"];
                    prevHomeCountry = (string)row["COUNTRY"];
                    prevHomePostalCode = (string)row["ZIP"];

                    string sql_UpdateUserAddress = "UPDATE Name_Address SET " +
                        " PHONE = " + Escape(user.HomePhone) + ", " +
                        " ADDRESS_1 = " + Escape(user.HomeAddress.Address1) + ", " +
                        " ADDRESS_2 = " + Escape(user.HomeAddress.Address2) + ", " +
                        " ADDRESS_3 = " + Escape(user.HomeAddress.Address3) + ", " +
                        " CITY = " + Escape(user.HomeAddress.City) + ", " +
                        " STATE_PROVINCE = " + Escape(user.HomeAddress.Province) + ", " +
                        " ZIP = " + Escape(user.HomeAddress.PostalCode) + ", " +
                        " COUNTRY = " + Escape(user.HomeAddress.Country) + ", " +
                        " FULL_ADDRESS = " + Escape(user.FullAddressDB) + ", " +
                        " LAST_UPDATED = Getdate() " +
                        " WHERE ID = " + Escape(user.Id) + " AND PURPOSE='Home'";

                    db.ExecuteCommand(sql_UpdateUserAddress);

                    // save changes in name_address to name_log table

                    if (prevHomeAddress1 != user.HomeAddress.Address1)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_1 ( Addr_Num:" + address_num.ToString() + "): " + prevHomeAddress1 + " -> " + user.HomeAddress.Address1);
                    }

                    if (prevHomeAddress2 != user.HomeAddress.Address2)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_2 ( Addr_Num:" + address_num.ToString() + "): " + prevHomeAddress2 + " -> " + user.HomeAddress.Address2);
                    }

                    if (prevHomeAddress3 != user.HomeAddress.Address3)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_3 ( Addr_Num:" + address_num.ToString() + "): " + prevHomeAddress3 + " -> " + user.HomeAddress.Address3);
                    }

                    if (prevHomeCity != user.HomeAddress.City)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.CITY ( Addr_Num:" + address_num.ToString() + "): " + prevHomeCity + " -> " + user.HomeAddress.City);
                    }

                    if (prevHomeProvince != user.HomeAddress.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.STATE_PROVINCE ( Addr_Num:" + address_num.ToString() + "): " + prevHomeProvince + " -> " + user.HomeAddress.Province);
                    }

                    if (prevHomeCountry != user.HomeAddress.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.COUNTRY ( Addr_Num:" + address_num.ToString() + "): " + prevHomeCountry + " -> " + user.HomeAddress.Country);
                    }

                    if (prevHomePostalCode != user.HomeAddress.PostalCode)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.ZIP ( Addr_Num:" + address_num.ToString() + "): " + prevHomePostalCode + " -> " + user.HomeAddress.PostalCode);
                    }

                    if (prevFullAddress != user.FullAddressDB)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.FULL_ADDRESS ( Addr_Num:" + address_num.ToString() + "): " + prevFullAddress + " -> " + user.FullAddressDB);
                    }

                    if (prevHomePhone != user.HomePhone)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.PHONE ( Addr_Num:" + address_num.ToString() + "): " + prevHomePhone + " -> " + user.HomePhone);
                    }

                }
                else if (table != null && table.Rows.Count == 0)
                {
                    int newSEQN = GetCounter("Name_Address");
                    string sql_InsertNewUserAddress = "INSERT INTO Name_Address " +
                        " (ID, ADDRESS_NUM, PURPOSE, ADDRESS_1, ADDRESS_2, ADDRESS_3, CITY, STATE_PROVINCE, ZIP, COUNTRY, FULL_ADDRESS, LAST_UPDATED, PHONE) ";
                    sql_InsertNewUserAddress += string.Format(" VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12})", Escape(user.Id), newSEQN.ToString(), Escape("Home"), Escape(user.HomeAddress.Address1),
                         Escape(user.HomeAddress.Address2), Escape(user.HomeAddress.Address3), Escape(user.HomeAddress.City), Escape(user.HomeAddress.Province), Escape(user.HomeAddress.PostalCode),
                         Escape(user.HomeAddress.Country), Escape(user.FullAddressDB), "Getdate()", Escape(user.HomePhone));
                    db.ExecuteCommand(sql_InsertNewUserAddress);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_1 ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Address1);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_2 ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Address2);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_3 ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Address3);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.CITY ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.City);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.STATE_PROVINCE ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Province);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.COUNTRY ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Country);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.ZIP ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.PostalCode);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.FULL_ADDRESS ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.FullAddressDB);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.PHONE ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomePhone);

                }


                // this section saves old values of email and home phone if they are different than new values
                // also saves old value of full_address if it preferred mail
                string prev_Email = string.Empty;
                string prev_HomePhone = string.Empty;
                string full_address = string.Empty;
                string prevCommonlyUsedLastName = string.Empty;
                string prevCommonlyUsedFirstName = string.Empty;


                string sql_GetNameInfo = " SELECT * FROM Name WHERE  ID = " + Escape(user.Id);
                var table2 = db.GetData(sql_GetNameInfo);

                if (table2 != null && table2.Rows.Count == 1)
                {
                    DataRow row2 = table2.Rows[0];
                    prev_Email = (string)row2["EMAIL"];
                    prev_HomePhone = (string)row2["HOME_PHONE"];
                    full_address = (string)row2["FULL_ADDRESS"];
                    prevCommonlyUsedFirstName = (string)row2["FIRST_NAME"];
                    prevCommonlyUsedLastName = (string)row2["LAST_NAME"];
                }

                if (!user.Email.Equals(prev_Email))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name.EMAIL: " + prev_Email + " -> " + user.Email);
                }

                if (!user.HomePhone.Equals(prev_HomePhone))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name.HOME_PHONE: " + prev_HomePhone + " -> " + user.HomePhone);
                }

                if (pref_Mail && !user.FullAddressDB.Equals(full_address))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name.FULL_ADDRESS: " + full_address + " -> " + user.FullAddressDB);
                }

                if (!user.CommonlyUsedFirstName.Equals(prevCommonlyUsedFirstName))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name.FIRST_NAME: " + prevCommonlyUsedFirstName + " -> " + user.CommonlyUsedFirstName);
                }

                if (!user.CommonlyUsedLastName.Equals(prevCommonlyUsedLastName))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name.LAST_NAME: " + prevCommonlyUsedLastName + " -> " + user.CommonlyUsedLastName);
                }

                string sql_UpdateUser = "UPDATE Name SET " +
                        " EMAIL = " + Escape(user.Email) +
                        ", HOME_PHONE = " + Escape(user.HomePhone) +
                        ", LAST_NAME = " + Escape(user.CommonlyUsedLastName) +
                        ", FIRST_NAME = " + Escape(user.CommonlyUsedFirstName);

                if (pref_Mail)
                {
                    sql_UpdateUser += ", FULL_ADDRESS = " + Escape(user.FullAddressDB);
                    sql_UpdateUser += ", CITY = " + Escape(user.HomeAddress.City);
                    sql_UpdateUser += ", STATE_PROVINCE = " + Escape(user.HomeAddress.Province);
                    sql_UpdateUser += ", ZIP = " + Escape(user.HomeAddress.PostalCode);
                    sql_UpdateUser += ", COUNTRY = " + Escape(user.HomeAddress.Country);
                }

                sql_UpdateUser += " WHERE ID = " + Escape(user.Id);

                db.ExecuteCommand(sql_UpdateUser);

                //string sql_UpdateUserBirthDate = "UPDATE COTO_REGDATA SET ";
                //if (user.BirthDate != DateTime.MinValue)
                //{
                //    sql_UpdateUserBirthDate += " BIRTH_DATE_COTO = '" + user.BirthDate.ToString("yyyy-MM-dd") + "'";
                //}
                //else
                //{
                //    sql_UpdateUserBirthDate += " BIRTH_DATE_COTO = NULL";
                //}

                //sql_UpdateUserBirthDate += " WHERE ID = " + Escape(user.Id);
                //db.ExecuteCommand(sql_UpdateUserBirthDate);
                //errorMessage = sql_UpdateUser;
            }
            catch (Exception ex)
            {
                errorMessage = "Error message: " + ex.Message + "; Error Inner Exception: " + ex.InnerException + " Error stackTrace: " + ex.StackTrace; // +"SQL = " + working_sql;
            }
            return errorMessage;

        }

        public string UpdateUserInfoProfileLogged(string UserID, User user)
        {
            string errorMessage = string.Empty;
            try
            {

                string prevEmail = string.Empty;
                string prevHomePhone = string.Empty;
                string prevFullAddress = string.Empty;

                string prevHomeAddress1 = string.Empty;
                string prevHomeAddress2 = string.Empty;
                string prevHomeAddress3 = string.Empty;

                string prevHomeCity = string.Empty;
                string prevHomeProvince = string.Empty;
                string prevHomeCountry = string.Empty;
                string prevHomePostalCode = string.Empty;

                //var db = new clsDB();
                string dbconn = WebConfigItems.GetConnectionString();
                var db = new clsDB();


                bool pref_Mail = false;

                string sql_GetHomeAddress = " SELECT * FROM Name_Address WHERE  ID = " + Escape(user.Id) + " AND PURPOSE='Home'";
                //db.ExecuteCommand(sql_GetHomeAddress);

                var table = db.GetData(sql_GetHomeAddress);
                if (table != null && table.Rows.Count == 1)
                {
                    DataRow row = table.Rows[0];
                    int address_num = (int)row["ADDRESS_NUM"];
                    pref_Mail = (bool)row["PREFERRED_MAIL"];

                    prevHomePhone = (string)row["PHONE"];
                    prevFullAddress = (string)row["FULL_ADDRESS"];

                    prevHomeAddress1 = (string)row["ADDRESS_1"];
                    prevHomeAddress2 = (string)row["ADDRESS_2"];
                    prevHomeAddress3 = (string)row["ADDRESS_3"];

                    prevHomeCity = (string)row["CITY"];
                    prevHomeProvince = (string)row["STATE_PROVINCE"];
                    prevHomeCountry = (string)row["COUNTRY"];
                    prevHomePostalCode = (string)row["ZIP"];

                    string sql_UpdateUserAddress = "UPDATE Name_Address SET " +
                        " PHONE = " + Escape(user.HomePhone) + ", " +
                        " ADDRESS_1 = " + Escape(user.HomeAddress.Address1) + ", " +
                        " ADDRESS_2 = " + Escape(user.HomeAddress.Address2) + ", " +
                        " ADDRESS_3 = " + Escape(user.HomeAddress.Address3) + ", " +
                        " CITY = " + Escape(user.HomeAddress.City) + ", " +
                        " STATE_PROVINCE = " + Escape(user.HomeAddress.Province) + ", " +
                        " ZIP = " + Escape(user.HomeAddress.PostalCode) + ", " +
                        " COUNTRY = " + Escape(user.HomeAddress.Country) + ", " +
                        " FULL_ADDRESS = " + Escape(user.FullAddressDB) + ", " +
                        " LAST_UPDATED = Getdate() " +
                        " WHERE ID = " + Escape(user.Id) + " AND PURPOSE='Home'";

                    db.ExecuteCommand(sql_UpdateUserAddress);

                    // save changes in name_address to name_log table

                    if (prevHomeAddress1 != user.HomeAddress.Address1)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_1 ( Addr_Num:" + address_num.ToString() + "): " + prevHomeAddress1 + " -> " + user.HomeAddress.Address1);
                    }

                    if (prevHomeAddress2 != user.HomeAddress.Address2)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_2 ( Addr_Num:" + address_num.ToString() + "): " + prevHomeAddress2 + " -> " + user.HomeAddress.Address2);
                    }

                    if (prevHomeAddress3 != user.HomeAddress.Address3)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_3 ( Addr_Num:" + address_num.ToString() + "): " + prevHomeAddress3 + " -> " + user.HomeAddress.Address3);
                    }

                    if (prevHomeCity != user.HomeAddress.City)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.CITY ( Addr_Num:" + address_num.ToString() + "): " + prevHomeCity + " -> " + user.HomeAddress.City);
                    }

                    if (prevHomeProvince != user.HomeAddress.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.STATE_PROVINCE ( Addr_Num:" + address_num.ToString() + "): " + prevHomeProvince + " -> " + user.HomeAddress.Province);
                    }

                    if (prevHomeCountry != user.HomeAddress.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.COUNTRY ( Addr_Num:" + address_num.ToString() + "): " + prevHomeCountry + " -> " + user.HomeAddress.Country);
                    }

                    if (prevHomePostalCode != user.HomeAddress.PostalCode)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.ZIP ( Addr_Num:" + address_num.ToString() + "): " + prevHomePostalCode + " -> " + user.HomeAddress.PostalCode);
                    }

                    if (prevFullAddress != user.FullAddressDB)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.FULL_ADDRESS ( Addr_Num:" + address_num.ToString() + "): " + prevFullAddress + " -> " + user.FullAddressDB);
                    }

                    if (prevHomePhone != user.HomePhone)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.PHONE ( Addr_Num:" + address_num.ToString() + "): " + prevHomePhone + " -> " + user.HomePhone);
                    }

                }
                else if (table != null && table.Rows.Count == 0)
                {
                    int newSEQN = GetCounter("Name_Address");
                    string sql_InsertNewUserAddress = "INSERT INTO Name_Address " +
                        " (ID, ADDRESS_NUM, PURPOSE, ADDRESS_1, ADDRESS_2, ADDRESS_3, CITY, STATE_PROVINCE, ZIP, COUNTRY, FULL_ADDRESS, LAST_UPDATED, PHONE) ";
                    sql_InsertNewUserAddress += string.Format(" VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12})", Escape(user.Id), newSEQN.ToString(), Escape("Home"), Escape(user.HomeAddress.Address1),
                         Escape(user.HomeAddress.Address2), Escape(user.HomeAddress.Address3), Escape(user.HomeAddress.City), Escape(user.HomeAddress.Province), Escape(user.HomeAddress.PostalCode),
                         Escape(user.HomeAddress.Country), Escape(user.FullAddressDB), "Getdate()", Escape(user.HomePhone));
                    db.ExecuteCommand(sql_InsertNewUserAddress);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_1 ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Address1);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_2 ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Address2);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_3 ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Address3);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.CITY ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.City);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.STATE_PROVINCE ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Province);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.COUNTRY ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Country);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.ZIP ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.PostalCode);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.FULL_ADDRESS ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.FullAddressDB);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.PHONE ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomePhone);

                }


                // this section saves old values of email and home phone if they are different than new values
                // also saves old value of full_address if it preferred mail
                string prev_Email = string.Empty;
                string prev_HomePhone = string.Empty;
                string full_address = string.Empty;
                //string prevCommonlyUsedLastName = string.Empty;
                //string prevCommonlyUsedFirstName = string.Empty;


                string sql_GetNameInfo = " SELECT * FROM Name WHERE  ID = " + Escape(user.Id);
                var table2 = db.GetData(sql_GetNameInfo);

                if (table2 != null && table2.Rows.Count == 1)
                {
                    DataRow row2 = table2.Rows[0];
                    prev_Email = (string)row2["EMAIL"];
                    prev_HomePhone = (string)row2["HOME_PHONE"];
                    full_address = (string)row2["FULL_ADDRESS"];
                    //prevCommonlyUsedFirstName = (string)row2["FIRST_NAME"];
                    //prevCommonlyUsedLastName = (string)row2["LAST_NAME"];
                }

                if (!user.Email.Equals(prev_Email))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name.EMAIL: " + prev_Email + " -> " + user.Email);
                }

                if (!user.HomePhone.Equals(prev_HomePhone))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name.HOME_PHONE: " + prev_HomePhone + " -> " + user.HomePhone);
                }

                if (pref_Mail && !user.FullAddressDB.Equals(full_address))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name.FULL_ADDRESS: " + full_address + " -> " + user.FullAddressDB);
                }

                //if (!user.CommonlyUsedFirstName.Equals(prevCommonlyUsedFirstName))
                //{
                //    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name.FIRST_NAME: " + prevCommonlyUsedFirstName + " -> " + user.CommonlyUsedFirstName);
                //}

                //if (!user.CommonlyUsedLastName.Equals(prevCommonlyUsedLastName))
                //{
                //    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name.LAST_NAME: " + prevCommonlyUsedLastName + " -> " + user.CommonlyUsedLastName);
                //}

                string sql_UpdateUser = "UPDATE Name SET " +
                        " EMAIL = " + Escape(user.Email) +
                        ", HOME_PHONE = " + Escape(user.HomePhone); // +
                        //", LAST_NAME = " + Escape(user.CommonlyUsedLastName) +
                        //", FIRST_NAME = " + Escape(user.CommonlyUsedFirstName);

                if (pref_Mail)
                {
                    sql_UpdateUser += ", FULL_ADDRESS = " + Escape(user.FullAddressDB);
                    sql_UpdateUser += ", CITY = " + Escape(user.HomeAddress.City);
                    sql_UpdateUser += ", STATE_PROVINCE = " + Escape(user.HomeAddress.Province);
                    sql_UpdateUser += ", ZIP = " + Escape(user.HomeAddress.PostalCode);
                    sql_UpdateUser += ", COUNTRY = " + Escape(user.HomeAddress.Country);
                }

                sql_UpdateUser += " WHERE ID = " + Escape(user.Id);

                db.ExecuteCommand(sql_UpdateUser);

                //string sql_UpdateUserBirthDate = "UPDATE COTO_REGDATA SET ";
                //if (user.BirthDate != DateTime.MinValue)
                //{
                //    sql_UpdateUserBirthDate += " BIRTH_DATE_COTO = '" + user.BirthDate.ToString("yyyy-MM-dd") + "'";
                //}
                //else
                //{
                //    sql_UpdateUserBirthDate += " BIRTH_DATE_COTO = NULL";
                //}

                //sql_UpdateUserBirthDate += " WHERE ID = " + Escape(user.Id);
                //db.ExecuteCommand(sql_UpdateUserBirthDate);
                //errorMessage = sql_UpdateUser;
            }
            catch (Exception ex)
            {
                errorMessage = "Error message: " + ex.Message + "; Error Inner Exception: " + ex.InnerException + " Error stackTrace: " + ex.StackTrace; // +"SQL = " + working_sql;
            }
            return errorMessage;

        }

        public void UpdateUserEmployment(User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            var employment1 = user.EmploymentList.Find(E => E.Index == 1);
            var employment2 = user.EmploymentList.Find(E => E.Index == 2);
            var employment3 = user.EmploymentList.Find(E => E.Index == 3);
            if (employment1 != null && employment2 != null && employment3 != null)
            {

                string sql_UpdateEmp = "UPDATE EMPLOYMENT_ALL SET ";
                // update morethreepractsites and current employment status
                //sql_UpdateEmp += "CURRENT_EMPLOYMENT_STATUS = " + Escape(user.CurrentEmploymentStatus) + ", " +
                //" FULL_PT_STATUS_PREF = " + Escape(user.CurrentWorkPreference.ToString()) + ", " +
                //" NATURE_OF_PRACTICE = " + Escape(user.NaturePractice.ToString()) + ", " +
                string morethree = string.Empty;
                if (user.MoreThreePractSites != null)
                {
                    bool bln_MoreThree = (bool)user.MoreThreePractSites;
                    morethree = bln_MoreThree ? "1" : "0";
                }
                sql_UpdateEmp += " MORE_THREE_PRACT_SITES = " + Escape(morethree) + ", " +
                  " PRIMARY_EMPL_PREFERRED = " + Escape(user.CollegeMailings) + ", ";
                // first employment
                sql_UpdateEmp += " EMP_1_SOURCE = " + Escape(employment1.Status) + ", " +
                " EMP_1_NAME = " + Escape(employment1.EmployerName) + ", ";
                var address1 = employment1.Address;
                if (address1 != null)
                {
                    sql_UpdateEmp += "EMP_1_ADDR_1 = " + Escape(address1.Address1) + ", " +
                    "EMP_1_ADDR_2 = " + Escape(address1.Address2) + ", " +
                    "EMP_1_CITY = " + Escape(address1.City) + ", " +
                    "EMP_1_PROV = " + Escape(address1.Province) + ", " +
                    "EMP_1_POSTAL = " + Escape(address1.PostalCode) + ", " +
                    "EMP_1_COUNTRY = " + Escape(address1.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_1_PHONE = " + Escape(employment1.Phone) + ", " +
                "EMP_1_FAX = " + Escape(employment1.Fax) + ", " +
                "EMP_1_PC_REF_SP = " + Escape(employment1.PostalCodeReflectPractice) + ", ";

                if (employment1.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment1.StartDate;
                    sql_UpdateEmp += "EMP_1_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_1_START_DATE = NULL, ";
                }

                if (employment1.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment1.EndDate;
                    sql_UpdateEmp += "CURRENT_EMPLOYMENT_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "CURRENT_EMPLOYMENT_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_1_CATEGORY = " + Escape(employment1.EmploymentRelationship) + ", " +
                "EMP_1_FULL_PT_STATUS = " + Escape(employment1.CasualStatus) + ", " +
                "EMP_1_HOURS_WORKED_WEEK = " + employment1.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_1_POSITION = " + Escape(employment1.PrimaryRole) + ", " +
                "EMP_1_TYPE = " + Escape(employment1.PracticeSetting) + ", " +
                "EMP_1_AREA_PRACTICE = " + Escape(employment1.MajorServices) + ", " +
                "EMP_1_CLNT_ARNGE = " + Escape(employment1.ClientAgeRange) + ", " +
                "EMP_1_FUNDING_SOURCE = " + Escape(employment1.FundingSource) + ", " +
                "EMP_1_HEALTH_CONDITION = " + Escape(employment1.HealthCondition) + ", ";

                // second employment
                sql_UpdateEmp +=
                "EMP_2_SOURCE = " + Escape(employment2.Status) + ", " +
                "EMP_2_NAME = " + Escape(employment2.EmployerName) + ", ";
                var address2 = employment2.Address;
                if (address2 != null)
                {
                    sql_UpdateEmp += "EMP_2_ADDR_1 = " + Escape(address2.Address1) + ", " +
                    "EMP_2_ADDR_2 = " + Escape(address2.Address2) + ", " +
                    "EMP_2_CITY = " + Escape(address2.City) + ", " +
                    "EMP_2_PROV = " + Escape(address2.Province) + ", " +
                    "EMP_2_POSTAL = " + Escape(address2.PostalCode) + ", " +
                    "EMP_2_COUNTRY = " + Escape(address2.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_2_PHONE = " + Escape(employment2.Phone) + ", " +
                "EMP_2_FAX = " + Escape(employment2.Fax) + ", " +
                "EMP_2_PC_REF_SP = " + Escape(employment2.PostalCodeReflectPractice) + ", ";

                if (employment2.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment2.StartDate;
                    sql_UpdateEmp += "EMP_2_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_2_START_DATE = NULL, ";
                }

                if (employment2.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment2.EndDate;
                    sql_UpdateEmp += "EMP_2_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_2_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_2_CATEGORY = " + Escape(employment2.EmploymentRelationship) + ", " +
                "EMP_2_FULL_PT_STATUS = " + Escape(employment2.CasualStatus) + ", " +
                "EMP_2_HOURS_WORKED_WEEK = " + employment2.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_2_POSITION = " + Escape(employment2.PrimaryRole) + ", " +
                "EMP_2_TYPE = " + Escape(employment2.PracticeSetting) + ", " +
                "EMP_2_AREA_PRACTICE = " + Escape(employment2.MajorServices) + ", " +
                "EMP_2_CLNT_ARNGE = " + Escape(employment2.ClientAgeRange) + ", " +
                "EMP_2_FUNDING_SOURCE = " + Escape(employment2.FundingSource) + ", " +
                "EMP_2_HEALTH_CONDITION = " + Escape(employment2.HealthCondition) + ", ";

                // third employment
                sql_UpdateEmp +=
                "EMP_3_SOURCE = " + Escape(employment3.Status) + ", " +
                "EMP_3_NAME = " + Escape(employment3.EmployerName) + ", ";
                var address3 = employment3.Address;
                if (address2 != null)
                {
                    sql_UpdateEmp += "EMP_3_ADDR_1 = " + Escape(address3.Address1) + ", " +
                    "EMP_3_ADDR_2 = " + Escape(address3.Address2) + ", " +
                    "EMP_3_CITY = " + Escape(address3.City) + ", " +
                    "EMP_3_PROV = " + Escape(address3.Province) + ", " +
                    "EMP_3_POSTAL = " + Escape(address3.PostalCode) + ", " +
                    "EMP_3_COUNTRY = " + Escape(address3.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_3_PHONE = " + Escape(employment3.Phone) + ", " +
                "EMP_3_FAX = " + Escape(employment3.Fax) + ", " +
                "EMP_3_PC_REF_SP = " + Escape(employment3.PostalCodeReflectPractice) + ", ";

                if (employment3.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment3.StartDate;
                    sql_UpdateEmp += "EMP_3_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_3_START_DATE = NULL, ";
                }

                if (employment3.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment3.EndDate;
                    sql_UpdateEmp += "EMP_3_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_3_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_3_CATEGORY = " + Escape(employment3.EmploymentRelationship) + ", " +
                "EMP_3_FULL_PT_STATUS = " + Escape(employment3.CasualStatus) + ", " +
                "EMP_3_HOURS_WORKED_WEEK = " + employment3.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_3_POSITION = " + Escape(employment3.PrimaryRole) + ", " +
                "EMP_3_TYPE = " + Escape(employment3.PracticeSetting) + ", " +
                "EMP_3_AREA_PRACTICE = " + Escape(employment3.MajorServices) + ", " +
                "EMP_3_CLNT_ARNGE = " + Escape(employment3.ClientAgeRange) + ", " +
                "EMP_3_FUNDING_SOURCE = " + Escape(employment3.FundingSource) + ", " +
                "EMP_3_HEALTH_CONDITION = " + Escape(employment3.HealthCondition) +

                " WHERE ID = " + Escape(user.Id);

                db.ExecuteCommand(sql_UpdateEmp);
            }
        }

        public void UpdateUserEmploymentLogged(string UserID, User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            var employment1 = user.EmploymentList.Find(E => E.Index == 1);
            var employment2 = user.EmploymentList.Find(E => E.Index == 2);
            var employment3 = user.EmploymentList.Find(E => E.Index == 3);
            if (employment1 != null && employment2 != null && employment3 != null)
            {

                string prevMoreThreePract = string.Empty;
                string prevCollegeMailing = string.Empty;

                string prevEmp1Status = string.Empty;
                string prevEmp1EmployerName = string.Empty;
                string prevEmp1Address1 = string.Empty;
                string prevEmp1Address2 = string.Empty;
                string prevEmp1City = string.Empty;
                string prevEmp1Prov = string.Empty;
                string prevEmp1Postal = string.Empty;
                string prevEmp1Country = string.Empty;
                string prevEmp1Phone = string.Empty;
                string prevEmp1Fax = string.Empty;
                string prevEmp1PostalCodeReflectPractice = string.Empty;
                string prevEmp1StartDate = string.Empty;
                string prevEmp1EndDate = string.Empty;
                string prevEmp1Category = string.Empty;
                string prevEmp1CasualStatus = string.Empty;
                string prevEmp1AvgWeeklyHours = string.Empty;
                string prevEmp1PrimaryRole = string.Empty;
                string prevEmp1PracticeSetting = string.Empty;
                string prevEmp1MajorService = string.Empty;
                string prevEmp1ClientAgeRange = string.Empty;
                string prevEmp1FundingSource = string.Empty;
                string prevEmp1HealthCondition = string.Empty;

                string prevEmp2Status = string.Empty;
                string prevEmp2EmployerName = string.Empty;
                string prevEmp2Address1 = string.Empty;
                string prevEmp2Address2 = string.Empty;
                string prevEmp2City = string.Empty;
                string prevEmp2Prov = string.Empty;
                string prevEmp2Postal = string.Empty;
                string prevEmp2Country = string.Empty;
                string prevEmp2Phone = string.Empty;
                string prevEmp2Fax = string.Empty;
                string prevEmp2PostalCodeReflectPractice = string.Empty;
                string prevEmp2StartDate = string.Empty;
                string prevEmp2EndDate = string.Empty;
                string prevEmp2Category = string.Empty;
                string prevEmp2CasualStatus = string.Empty;
                string prevEmp2AvgWeeklyHours = string.Empty;
                string prevEmp2PrimaryRole = string.Empty;
                string prevEmp2PracticeSetting = string.Empty;
                string prevEmp2MajorService = string.Empty;
                string prevEmp2ClientAgeRange = string.Empty;
                string prevEmp2FundingSource = string.Empty;
                string prevEmp2HealthCondition = string.Empty;

                string prevEmp3Status = string.Empty;
                string prevEmp3EmployerName = string.Empty;
                string prevEmp3Address1 = string.Empty;
                string prevEmp3Address2 = string.Empty;
                string prevEmp3City = string.Empty;
                string prevEmp3Prov = string.Empty;
                string prevEmp3Postal = string.Empty;
                string prevEmp3Country = string.Empty;
                string prevEmp3Phone = string.Empty;
                string prevEmp3Fax = string.Empty;
                string prevEmp3PostalCodeReflectPractice = string.Empty;
                string prevEmp3StartDate = string.Empty;
                string prevEmp3EndDate = string.Empty;
                string prevEmp3Category = string.Empty;
                string prevEmp3CasualStatus = string.Empty;
                string prevEmp3AvgWeeklyHours = string.Empty;
                string prevEmp3PrimaryRole = string.Empty;
                string prevEmp3PracticeSetting = string.Empty;
                string prevEmp3MajorService = string.Empty;
                string prevEmp3ClientAgeRange = string.Empty;
                string prevEmp3FundingSource = string.Empty;
                string prevEmp3HealthCondition = string.Empty;

                string sql_GetCurrentEmployment = "SELECT * FROM EMPLOYMENT_ALL WHERE ID=" + Escape(UserID);
                var table = db.GetData(sql_GetCurrentEmployment);
                if (table != null && table.Rows.Count == 1)
                {
                    DataRow row = table.Rows[0];
                    prevMoreThreePract = (string)row["MORE_THREE_PRACT_SITES"];
                    prevCollegeMailing = (string)row["PRIMARY_EMPL_PREFERRED"];
                    #region mep1
                    prevEmp1Status = (string)row["EMP_1_SOURCE"];
                    prevEmp1EmployerName = (string)row["EMP_1_NAME"];
                    prevEmp1Address1 = (string)row["EMP_1_ADDR_1"];
                    prevEmp1Address2 = (string)row["EMP_1_ADDR_2"];
                    prevEmp1City = (string)row["EMP_1_CITY"];
                    prevEmp1Prov = (string)row["EMP_1_PROV"];
                    prevEmp1Postal = (string)row["EMP_1_POSTAL"];
                    prevEmp1Country = (string)row["EMP_1_COUNTRY"];
                    prevEmp1Phone = (string)row["EMP_1_PHONE"];
                    prevEmp1Fax = (string)row["EMP_1_FAX"];
                    prevEmp1PostalCodeReflectPractice = (string)row["EMP_1_PC_REF_SP"];
                    if (row["EMP_1_START_DATE"] != DBNull.Value)
                    {
                        prevEmp1StartDate = ((DateTime)row["EMP_1_START_DATE"]).ToString("dd/MM/yyyy");
                    }
                    if (row["CURRENT_EMPLOYMENT_END_DATE"] != DBNull.Value)
                    {
                        prevEmp1EndDate = ((DateTime)row["CURRENT_EMPLOYMENT_END_DATE"]).ToString("dd/MM/yyyy");
                    }

                    prevEmp1Category = (string)row["EMP_1_CATEGORY"];
                    prevEmp1CasualStatus = (string)row["EMP_1_FULL_PT_STATUS"];
                    prevEmp1AvgWeeklyHours = ((double)row["EMP_1_HOURS_WORKED_WEEK"]).ToString();
                    prevEmp1PrimaryRole = (string)row["EMP_1_POSITION"];
                    prevEmp1PracticeSetting = (string)row["EMP_1_TYPE"];
                    prevEmp1MajorService = (string)row["EMP_1_AREA_PRACTICE"];
                    prevEmp1ClientAgeRange = (string)row["EMP_1_CLNT_ARNGE"];
                    prevEmp1FundingSource = (string)row["EMP_1_FUNDING_SOURCE"];
                    prevEmp1HealthCondition = (string)row["EMP_1_HEALTH_CONDITION"];
                    #endregion
                    #region emp2
                    prevEmp2Status = (string)row["EMP_2_SOURCE"];
                    prevEmp2EmployerName = (string)row["EMP_2_NAME"];
                    prevEmp2Address1 = (string)row["EMP_2_ADDR_1"];
                    prevEmp2Address2 = (string)row["EMP_2_ADDR_2"];
                    prevEmp2City = (string)row["EMP_2_CITY"];
                    prevEmp2Prov = (string)row["EMP_2_PROV"];
                    prevEmp2Postal = (string)row["EMP_2_POSTAL"];
                    prevEmp2Country = (string)row["EMP_2_COUNTRY"];
                    prevEmp2Phone = (string)row["EMP_2_PHONE"];
                    prevEmp2Fax = (string)row["EMP_2_FAX"];
                    prevEmp2PostalCodeReflectPractice = (string)row["EMP_2_PC_REF_SP"];
                    if (row["EMP_2_START_DATE"] != DBNull.Value)
                    {
                        prevEmp2StartDate = ((DateTime)row["EMP_2_START_DATE"]).ToString("dd/MM/yyyy");
                    }
                    if (row["EMP_2_END_DATE"] != DBNull.Value)
                    {
                        prevEmp2EndDate = ((DateTime)row["EMP_2_END_DATE"]).ToString("dd/MM/yyyy");
                    }

                    prevEmp2Category = (string)row["EMP_2_CATEGORY"];
                    prevEmp2CasualStatus = (string)row["EMP_2_FULL_PT_STATUS"];
                    prevEmp2AvgWeeklyHours = ((double)row["EMP_2_HOURS_WORKED_WEEK"]).ToString();
                    prevEmp2PrimaryRole = (string)row["EMP_2_POSITION"];
                    prevEmp2PracticeSetting = (string)row["EMP_2_TYPE"];
                    prevEmp2MajorService = (string)row["EMP_2_AREA_PRACTICE"];
                    prevEmp2ClientAgeRange = (string)row["EMP_2_CLNT_ARNGE"];
                    prevEmp2FundingSource = (string)row["EMP_2_FUNDING_SOURCE"];
                    prevEmp2HealthCondition = (string)row["EMP_2_HEALTH_CONDITION"];
                    #endregion
                    #region emp3
                    prevEmp3Status = (string)row["EMP_3_SOURCE"];
                    prevEmp3EmployerName = (string)row["EMP_3_NAME"];
                    prevEmp3Address1 = (string)row["EMP_3_ADDR_1"];
                    prevEmp3Address2 = (string)row["EMP_3_ADDR_2"];
                    prevEmp3City = (string)row["EMP_3_CITY"];
                    prevEmp3Prov = (string)row["EMP_3_PROV"];
                    prevEmp3Postal = (string)row["EMP_3_POSTAL"];
                    prevEmp3Country = (string)row["EMP_3_COUNTRY"];
                    prevEmp3Phone = (string)row["EMP_3_PHONE"];
                    prevEmp3Fax = (string)row["EMP_3_FAX"];
                    prevEmp3PostalCodeReflectPractice = (string)row["EMP_3_PC_REF_SP"];
                    if (row["EMP_3_START_DATE"] != DBNull.Value)
                    {
                        prevEmp3StartDate = ((DateTime)row["EMP_3_START_DATE"]).ToString("dd/MM/yyyy");
                    }
                    if (row["EMP_3_END_DATE"] != DBNull.Value)
                    {
                        prevEmp3EndDate = ((DateTime)row["EMP_3_END_DATE"]).ToString("dd/MM/yyyy");
                    }

                    prevEmp3Category = (string)row["EMP_3_CATEGORY"];
                    prevEmp3CasualStatus = (string)row["EMP_3_FULL_PT_STATUS"];
                    prevEmp3AvgWeeklyHours = ((double)row["EMP_3_HOURS_WORKED_WEEK"]).ToString();
                    prevEmp3PrimaryRole = (string)row["EMP_3_POSITION"];
                    prevEmp3PracticeSetting = (string)row["EMP_3_TYPE"];
                    prevEmp3MajorService = (string)row["EMP_3_AREA_PRACTICE"];
                    prevEmp3ClientAgeRange = (string)row["EMP_3_CLNT_ARNGE"];
                    prevEmp3FundingSource = (string)row["EMP_3_FUNDING_SOURCE"];
                    prevEmp3HealthCondition = (string)row["EMP_3_HEALTH_CONDITION"];
                    #endregion
                }

                string sql_UpdateEmp = "UPDATE EMPLOYMENT_ALL SET ";
                // update morethreepractsites and current employment status
                //sql_UpdateEmp += "CURRENT_EMPLOYMENT_STATUS = " + Escape(user.CurrentEmploymentStatus) + ", " +
                //" FULL_PT_STATUS_PREF = " + Escape(user.CurrentWorkPreference.ToString()) + ", " +
                //" NATURE_OF_PRACTICE = " + Escape(user.NaturePractice.ToString()) + ", " +

                string morethree = string.Empty;
                if (user.MoreThreePractSites != null)
                {
                    bool bln_MoreThree = (bool)user.MoreThreePractSites;
                    morethree = bln_MoreThree ? "Yes" : "No";
                }

                sql_UpdateEmp += " MORE_THREE_PRACT_SITES = " + Escape(morethree) + ", " +
                  " PRIMARY_EMPL_PREFERRED = " + Escape(user.CollegeMailings) + ", ";
                // first employment
                sql_UpdateEmp += " EMP_1_SOURCE = " + Escape(employment1.Status) + ", " +
                " EMP_1_NAME = " + Escape(employment1.EmployerName) + ", ";
                var address1 = employment1.Address;
                if (address1 != null)
                {
                    sql_UpdateEmp += "EMP_1_ADDR_1 = " + Escape(address1.Address1) + ", " +
                    "EMP_1_ADDR_2 = " + Escape(address1.Address2) + ", " +
                    "EMP_1_CITY = " + Escape(address1.City) + ", " +
                    "EMP_1_PROV = " + Escape(address1.Province) + ", " +
                    "EMP_1_POSTAL = " + Escape(address1.PostalCode) + ", " +
                    "EMP_1_COUNTRY = " + Escape(address1.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_1_PHONE = " + Escape(employment1.Phone) + ", " +
                "EMP_1_FAX = " + Escape(employment1.Fax) + ", " +
                "EMP_1_PC_REF_SP = " + Escape(employment1.PostalCodeReflectPractice) + ", ";

                if (employment1.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment1.StartDate;
                    sql_UpdateEmp += "EMP_1_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_1_START_DATE = NULL, ";
                }

                if (employment1.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment1.EndDate;
                    sql_UpdateEmp += "CURRENT_EMPLOYMENT_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "CURRENT_EMPLOYMENT_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_1_CATEGORY = " + Escape(employment1.EmploymentRelationship) + ", " +
                "EMP_1_FULL_PT_STATUS = " + Escape(employment1.CasualStatus) + ", " +
                "EMP_1_HOURS_WORKED_WEEK = " + employment1.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_1_POSITION = " + Escape(employment1.PrimaryRole) + ", " +
                "EMP_1_TYPE = " + Escape(employment1.PracticeSetting) + ", " +
                "EMP_1_AREA_PRACTICE = " + Escape(employment1.MajorServices) + ", " +
                "EMP_1_CLNT_ARNGE = " + Escape(employment1.ClientAgeRange) + ", " +
                "EMP_1_FUNDING_SOURCE = " + Escape(employment1.FundingSource) + ", " +
                "EMP_1_HEALTH_CONDITION = " + Escape(employment1.HealthCondition) + ", ";

                // second employment
                sql_UpdateEmp +=
                "EMP_2_SOURCE = " + Escape(employment2.Status) + ", " +
                "EMP_2_NAME = " + Escape(employment2.EmployerName) + ", ";
                var address2 = employment2.Address;
                if (address2 != null)
                {
                    sql_UpdateEmp += "EMP_2_ADDR_1 = " + Escape(address2.Address1) + ", " +
                    "EMP_2_ADDR_2 = " + Escape(address2.Address2) + ", " +
                    "EMP_2_CITY = " + Escape(address2.City) + ", " +
                    "EMP_2_PROV = " + Escape(address2.Province) + ", " +
                    "EMP_2_POSTAL = " + Escape(address2.PostalCode) + ", " +
                    "EMP_2_COUNTRY = " + Escape(address2.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_2_PHONE = " + Escape(employment2.Phone) + ", " +
                "EMP_2_FAX = " + Escape(employment2.Fax) + ", " +
                "EMP_2_PC_REF_SP = " + Escape(employment2.PostalCodeReflectPractice) + ", ";

                if (employment2.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment2.StartDate;
                    sql_UpdateEmp += "EMP_2_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_2_START_DATE = NULL, ";
                }

                if (employment2.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment2.EndDate;
                    sql_UpdateEmp += "EMP_2_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_2_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_2_CATEGORY = " + Escape(employment2.EmploymentRelationship) + ", " +
                "EMP_2_FULL_PT_STATUS = " + Escape(employment2.CasualStatus) + ", " +
                "EMP_2_HOURS_WORKED_WEEK = " + employment2.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_2_POSITION = " + Escape(employment2.PrimaryRole) + ", " +
                "EMP_2_TYPE = " + Escape(employment2.PracticeSetting) + ", " +
                "EMP_2_AREA_PRACTICE = " + Escape(employment2.MajorServices) + ", " +
                "EMP_2_CLNT_ARNGE = " + Escape(employment2.ClientAgeRange) + ", " +
                "EMP_2_FUNDING_SOURCE = " + Escape(employment2.FundingSource) + ", " +
                "EMP_2_HEALTH_CONDITION = " + Escape(employment2.HealthCondition) + ", ";

                // third employment
                sql_UpdateEmp +=
                "EMP_3_SOURCE = " + Escape(employment3.Status) + ", " +
                "EMP_3_NAME = " + Escape(employment3.EmployerName) + ", ";
                var address3 = employment3.Address;
                if (address2 != null)
                {
                    sql_UpdateEmp += "EMP_3_ADDR_1 = " + Escape(address3.Address1) + ", " +
                    "EMP_3_ADDR_2 = " + Escape(address3.Address2) + ", " +
                    "EMP_3_CITY = " + Escape(address3.City) + ", " +
                    "EMP_3_PROV = " + Escape(address3.Province) + ", " +
                    "EMP_3_POSTAL = " + Escape(address3.PostalCode) + ", " +
                    "EMP_3_COUNTRY = " + Escape(address3.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_3_PHONE = " + Escape(employment3.Phone) + ", " +
                "EMP_3_FAX = " + Escape(employment3.Fax) + ", " +
                "EMP_3_PC_REF_SP = " + Escape(employment3.PostalCodeReflectPractice) + ", ";

                if (employment3.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment3.StartDate;
                    sql_UpdateEmp += "EMP_3_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_3_START_DATE = NULL, ";
                }

                if (employment3.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment3.EndDate;
                    sql_UpdateEmp += "EMP_3_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_3_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_3_CATEGORY = " + Escape(employment3.EmploymentRelationship) + ", " +
                "EMP_3_FULL_PT_STATUS = " + Escape(employment3.CasualStatus) + ", " +
                "EMP_3_HOURS_WORKED_WEEK = " + employment3.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_3_POSITION = " + Escape(employment3.PrimaryRole) + ", " +
                "EMP_3_TYPE = " + Escape(employment3.PracticeSetting) + ", " +
                "EMP_3_AREA_PRACTICE = " + Escape(employment3.MajorServices) + ", " +
                "EMP_3_CLNT_ARNGE = " + Escape(employment3.ClientAgeRange) + ", " +
                "EMP_3_FUNDING_SOURCE = " + Escape(employment3.FundingSource) + ", " +
                "EMP_3_HEALTH_CONDITION = " + Escape(employment3.HealthCondition) +

                " WHERE ID = " + Escape(user.Id);

                db.ExecuteCommand(sql_UpdateEmp);

                if (prevMoreThreePract != (morethree))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.MORE_THREE_PRACT_SITES: " + prevMoreThreePract + " -> " + (morethree));
                }

                if (prevCollegeMailing != user.CollegeMailings)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.PRIMARY_EMPL_PREFERRED: " + prevCollegeMailing + " -> " + user.CollegeMailings);
                }

                #region emp1
                if (prevEmp1Status != employment1.Status)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_SOURCE: " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", prevEmp1Status) + " -> " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", employment1.Status));
                }
                if (prevEmp1EmployerName != employment1.EmployerName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_NAME: " + prevEmp1EmployerName + " -> " + employment1.EmployerName);
                }
                if (prevEmp1Address1 != employment1.Address.Address1)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_ADDR_1: " + prevEmp1Address1 + " -> " + employment1.Address.Address1);
                }
                if (prevEmp1Address2 != employment1.Address.Address2)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_ADDR_2: " + prevEmp1Address2 + " -> " + employment1.Address.Address2);
                }
                if (prevEmp1City != employment1.Address.City)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_CITY: " + prevEmp1City + " -> " + employment1.Address.City);
                }
                if (prevEmp1Prov != employment1.Address.Province)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_PROV: " + prevEmp1Prov + " -> " + employment1.Address.Province);
                }
                if (prevEmp1Postal != employment1.Address.PostalCode)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_POSTAL: " + prevEmp1Postal + " -> " + employment1.Address.PostalCode);
                }
                if (prevEmp1Country != employment1.Address.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_COUNTRY: " + prevEmp1Country + " -> " + employment1.Address.Country);
                }
                if (prevEmp1Phone != employment1.Phone)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_PHONE: " + prevEmp1Phone + " -> " + employment1.Phone);
                }
                if (prevEmp1Fax != employment1.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_FAX: " + prevEmp1Fax + " -> " + employment1.Fax);
                }
                if (prevEmp1PostalCodeReflectPractice != employment1.PostalCodeReflectPractice)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_PC_REF_SP: " + prevEmp1PostalCodeReflectPractice + " -> " + employment1.PostalCodeReflectPractice);
                }
                if (prevEmp1Fax != employment1.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_FAX: " + prevEmp1Fax + " -> " + employment1.Fax);
                }
                if (prevEmp1StartDate != (employment1.StartDate != null ? ((DateTime)employment1.StartDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_START_DATE: " + prevEmp1StartDate + " -> " + (employment1.StartDate != null ? ((DateTime)employment1.StartDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp1EndDate != (employment1.EndDate != null ? ((DateTime)employment1.EndDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.CURRENT_EMPLOYMENT_END_DATE: " + prevEmp1EndDate + " -> " + (employment1.EndDate != null ? ((DateTime)employment1.EndDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp1Category != employment1.EmploymentRelationship)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_CATEGORY: " + GetGeneralNameImprv("EMPLOY_CATEGORY", prevEmp1Category) + " -> " + GetGeneralNameImprv("EMPLOY_CATEGORY", employment1.EmploymentRelationship));
                }
                if (prevEmp1CasualStatus != employment1.CasualStatus)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_FULL_PT_STATUS: " + GetGeneralNameImprv("FULL_PT_STATUS", prevEmp1CasualStatus) + " -> " + GetGeneralNameImprv("FULL_PT_STATUS", employment1.CasualStatus));
                }
                if (prevEmp1AvgWeeklyHours != employment1.AverageWeeklyHours.ToString())
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_HOURS_WORKED_WEEK: " + prevEmp1AvgWeeklyHours + " -> " + employment1.AverageWeeklyHours.ToString());
                }
                if (prevEmp1PrimaryRole != employment1.PrimaryRole)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_POSITION: " + GetGeneralNameImprv("POSITION", prevEmp1PrimaryRole) + " -> " + GetGeneralNameImprv("POSITION", employment1.PrimaryRole));
                }
                if (prevEmp1PracticeSetting != employment1.PracticeSetting)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_TYPE: " + GetGeneralNameImprv("EMPLOYER_TYPE", prevEmp1PracticeSetting) + " -> " + GetGeneralNameImprv("EMPLOYER_TYPE", employment1.PracticeSetting));
                }
                if (prevEmp1MajorService != employment1.MajorServices)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_AREA_PRACTICE: " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", prevEmp1MajorService) + " -> " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", employment1.MajorServices));
                }
                if (prevEmp1ClientAgeRange != employment1.ClientAgeRange)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_CLNT_ARNGE: " + GetGeneralNameImprv("CLIENT_AGE_RANGE", prevEmp1ClientAgeRange) + " -> " + GetGeneralNameImprv("CLIENT_AGE_RANGE", employment1.ClientAgeRange));
                }
                if (prevEmp1FundingSource != employment1.FundingSource)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_FUNDING_SOURCE: " + GetGeneralNameImprv("FUNDING", prevEmp1FundingSource) + " -> " + GetGeneralNameImprv("FUNDING", employment1.FundingSource));
                }
                if (prevEmp1HealthCondition != employment1.HealthCondition)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_HEALTH_CONDITION: " + GetGeneralNameImprv("HEALTH_CONDITION", prevEmp1HealthCondition) + " -> " + GetGeneralNameImprv("HEALTH_CONDITION", employment1.HealthCondition));
                }
                #endregion
                #region emp2
                if (prevEmp2Status != employment2.Status)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_SOURCE: " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", prevEmp2Status) + " -> " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", employment2.Status));
                }
                if (prevEmp2EmployerName != employment2.EmployerName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_NAME: " + prevEmp2EmployerName + " -> " + employment2.EmployerName);
                }
                if (prevEmp2Address1 != employment2.Address.Address1)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_ADDR_1: " + prevEmp2Address1 + " -> " + employment2.Address.Address1);
                }
                if (prevEmp2Address2 != employment2.Address.Address2)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_ADDR_2: " + prevEmp2Address2 + " -> " + employment2.Address.Address2);
                }
                if (prevEmp2City != employment2.Address.City)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_CITY: " + prevEmp2City + " -> " + employment2.Address.City);
                }
                if (prevEmp2Prov != employment2.Address.Province)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_PROV: " + prevEmp2Prov + " -> " + employment2.Address.Province);
                }
                if (prevEmp2Postal != employment2.Address.PostalCode)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_POSTAL: " + prevEmp2Postal + " -> " + employment2.Address.PostalCode);
                }
                if (prevEmp2Country != employment2.Address.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_COUNTRY: " + prevEmp2Country + " -> " + employment2.Address.Country);
                }
                if (prevEmp2Phone != employment2.Phone)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_PHONE: " + prevEmp2Phone + " -> " + employment2.Phone);
                }
                if (prevEmp2Fax != employment2.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_FAX: " + prevEmp2Fax + " -> " + employment2.Fax);
                }
                if (prevEmp2PostalCodeReflectPractice != employment2.PostalCodeReflectPractice)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_PC_REF_SP: " + prevEmp2PostalCodeReflectPractice + " -> " + employment2.PostalCodeReflectPractice);
                }
                if (prevEmp2Fax != employment2.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_FAX: " + prevEmp2Fax + " -> " + employment2.Fax);
                }
                if (prevEmp2StartDate != (employment2.StartDate != null ? ((DateTime)employment2.StartDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_START_DATE: " + prevEmp2StartDate + " -> " + (employment2.StartDate != null ? ((DateTime)employment2.StartDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp2EndDate != (employment2.EndDate != null ? ((DateTime)employment2.EndDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_END_DATE: " + prevEmp2EndDate + " -> " + (employment2.EndDate != null ? ((DateTime)employment2.EndDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp2Category != employment2.EmploymentRelationship)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_CATEGORY: " + GetGeneralNameImprv("EMPLOY_CATEGORY", prevEmp2Category) + " -> " + GetGeneralNameImprv("EMPLOY_CATEGORY", employment2.EmploymentRelationship));
                }
                if (prevEmp2CasualStatus != employment2.CasualStatus)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_FULL_PT_STATUS: " + GetGeneralNameImprv("FULL_PT_STATUS", prevEmp2CasualStatus) + " -> " + GetGeneralNameImprv("FULL_PT_STATUS", employment2.CasualStatus));
                }
                if (prevEmp2AvgWeeklyHours != employment2.AverageWeeklyHours.ToString())
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_HOURS_WORKED_WEEK: " + prevEmp2AvgWeeklyHours + " -> " + employment2.AverageWeeklyHours.ToString());
                }
                if (prevEmp2PrimaryRole != employment2.PrimaryRole)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_POSITION: " + GetGeneralNameImprv("POSITION", prevEmp2PrimaryRole) + " -> " + GetGeneralNameImprv("POSITION", employment2.PrimaryRole));
                }
                if (prevEmp2PracticeSetting != employment2.PracticeSetting)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_TYPE: " + GetGeneralNameImprv("EMPLOYER_TYPE", prevEmp2PracticeSetting) + " -> " + GetGeneralNameImprv("EMPLOYER_TYPE", employment2.PracticeSetting));
                }
                if (prevEmp2MajorService != employment2.MajorServices)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_AREA_PRACTICE: " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", prevEmp2MajorService) + " -> " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", employment2.MajorServices));
                }
                if (prevEmp2ClientAgeRange != employment2.ClientAgeRange)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_CLNT_ARNGE: " + GetGeneralNameImprv("CLIENT_AGE_RANGE", prevEmp2ClientAgeRange) + " -> " + GetGeneralNameImprv("CLIENT_AGE_RANGE", employment2.ClientAgeRange));
                }
                if (prevEmp2FundingSource != employment2.FundingSource)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_FUNDING_SOURCE: " + GetGeneralNameImprv("FUNDING", prevEmp2FundingSource) + " -> " + GetGeneralNameImprv("FUNDING", employment2.FundingSource));
                }
                if (prevEmp2HealthCondition != employment2.HealthCondition)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_HEALTH_CONDITION: " + GetGeneralNameImprv("HEALTH_CONDITION", prevEmp2HealthCondition) + " -> " + GetGeneralNameImprv("HEALTH_CONDITION", employment2.HealthCondition));
                }
                #endregion
                #region emp3
                if (prevEmp3Status != employment3.Status)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_SOURCE: " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", prevEmp3Status) + " -> " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", employment3.Status));
                }
                if (prevEmp3EmployerName != employment3.EmployerName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_NAME: " + prevEmp3EmployerName + " -> " + employment3.EmployerName);
                }
                if (prevEmp3Address1 != employment3.Address.Address1)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_ADDR_1: " + prevEmp3Address1 + " -> " + employment3.Address.Address1);
                }
                if (prevEmp3Address2 != employment3.Address.Address2)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_ADDR_2: " + prevEmp3Address2 + " -> " + employment3.Address.Address2);
                }
                if (prevEmp3City != employment3.Address.City)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_CITY: " + prevEmp3City + " -> " + employment3.Address.City);
                }
                if (prevEmp3Prov != employment3.Address.Province)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_PROV: " + prevEmp3Prov + " -> " + employment3.Address.Province);
                }
                if (prevEmp3Postal != employment3.Address.PostalCode)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_POSTAL: " + prevEmp3Postal + " -> " + employment3.Address.PostalCode);
                }
                if (prevEmp3Country != employment3.Address.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_COUNTRY: " + prevEmp3Country + " -> " + employment3.Address.Country);
                }
                if (prevEmp3Phone != employment3.Phone)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_PHONE: " + prevEmp3Phone + " -> " + employment3.Phone);
                }
                if (prevEmp3Fax != employment3.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_FAX: " + prevEmp3Fax + " -> " + employment3.Fax);
                }
                if (prevEmp3PostalCodeReflectPractice != employment3.PostalCodeReflectPractice)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_PC_REF_SP: " + prevEmp3PostalCodeReflectPractice + " -> " + employment3.PostalCodeReflectPractice);
                }
                if (prevEmp3Fax != employment3.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_FAX: " + prevEmp3Fax + " -> " + employment3.Fax);
                }
                if (prevEmp3StartDate != (employment3.StartDate != null ? ((DateTime)employment3.StartDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_START_DATE: " + prevEmp3StartDate + " -> " + (employment3.StartDate != null ? ((DateTime)employment3.StartDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp3EndDate != (employment3.EndDate != null ? ((DateTime)employment3.EndDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_END_DATE: " + prevEmp3EndDate + " -> " + (employment3.EndDate != null ? ((DateTime)employment3.EndDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp3Category != employment3.EmploymentRelationship)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_CATEGORY: " + GetGeneralNameImprv("EMPLOY_CATEGORY", prevEmp3Category) + " -> " + GetGeneralNameImprv("EMPLOY_CATEGORY", employment3.EmploymentRelationship));
                }
                if (prevEmp3CasualStatus != employment3.CasualStatus)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_FULL_PT_STATUS: " + GetGeneralNameImprv("FULL_PT_STATUS", prevEmp3CasualStatus) + " -> " + GetGeneralNameImprv("FULL_PT_STATUS", employment3.CasualStatus));
                }
                if (prevEmp3AvgWeeklyHours != employment3.AverageWeeklyHours.ToString())
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_HOURS_WORKED_WEEK: " + prevEmp3AvgWeeklyHours + " -> " + employment3.AverageWeeklyHours.ToString());
                }
                if (prevEmp3PrimaryRole != employment3.PrimaryRole)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_POSITION: " + GetGeneralNameImprv("POSITION", prevEmp3PrimaryRole) + " -> " + GetGeneralNameImprv("POSITION", employment3.PrimaryRole));
                }
                if (prevEmp3PracticeSetting != employment3.PracticeSetting)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_TYPE: " + GetGeneralNameImprv("EMPLOYER_TYPE", prevEmp3PracticeSetting) + " -> " + GetGeneralNameImprv("EMPLOYER_TYPE", employment3.PracticeSetting));
                }
                if (prevEmp3MajorService != employment3.MajorServices)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_AREA_PRACTICE: " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", prevEmp3MajorService) + " -> " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", employment3.MajorServices));
                }
                if (prevEmp3ClientAgeRange != employment3.ClientAgeRange)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_CLNT_ARNGE: " + GetGeneralNameImprv("CLIENT_AGE_RANGE", prevEmp3ClientAgeRange) + " -> " + GetGeneralNameImprv("CLIENT_AGE_RANGE", employment3.ClientAgeRange));
                }
                if (prevEmp3FundingSource != employment3.FundingSource)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_FUNDING_SOURCE: " + GetGeneralNameImprv("FUNDING", prevEmp3FundingSource) + " -> " + GetGeneralNameImprv("FUNDING", employment3.FundingSource));
                }
                if (prevEmp3HealthCondition != employment3.HealthCondition)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_HEALTH_CONDITION: " + GetGeneralNameImprv("HEALTH_CONDITION", prevEmp3HealthCondition) + " -> " + GetGeneralNameImprv("HEALTH_CONDITION", employment3.HealthCondition));
                }
                #endregion
            }
        }
        
        public void UpdateUserEmploymentPersonal(User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            var employment1 = user.EmploymentList.Find(E => E.Index == 1);
            var employment2 = user.EmploymentList.Find(E => E.Index == 2);
            var employment3 = user.EmploymentList.Find(E => E.Index == 3);
            if (employment1 != null && employment2 != null && employment3 != null)
            {

                string sql_UpdateEmp = "UPDATE EMPLOYMENT_ALL SET ";
                // update morethreepractsites and current employment status
                sql_UpdateEmp += "CURRENT_EMPLOYMENT_STATUS = " + Escape(user.CurrentEmploymentStatus) + ", " +
                " FULL_PT_STATUS_PREF = " + Escape(user.CurrentWorkPreference.ToString()) + ", " +
                " NATURE_OF_PRACTICE = " + Escape(user.NaturePractice.ToString()) + ", ";

                string morethree = string.Empty;
                if (user.MoreThreePractSites != null)
                {
                    bool bln_MoreThree = (bool)user.MoreThreePractSites;
                    morethree = bln_MoreThree ? "Yes" : "No";
                }

                sql_UpdateEmp += " MORE_THREE_PRACT_SITES = " + Escape(morethree) + ", " +
                  " PRIMARY_EMPL_PREFERRED = " + Escape(user.CollegeMailings) + ", ";
                // first employment
                sql_UpdateEmp += " EMP_1_SOURCE = " + Escape(employment1.Status) + ", " +
                " EMP_1_NAME = " + Escape(employment1.EmployerName) + ", ";
                var address1 = employment1.Address;
                if (address1 != null)
                {
                    sql_UpdateEmp += "EMP_1_ADDR_1 = " + Escape(address1.Address1) + ", " +
                    "EMP_1_ADDR_2 = " + Escape(address1.Address2) + ", " +
                    "EMP_1_CITY = " + Escape(address1.City) + ", " +
                    "EMP_1_PROV = " + Escape(address1.Province) + ", " +
                    "EMP_1_POSTAL = " + Escape(address1.PostalCode) + ", " +
                    "EMP_1_COUNTRY = " + Escape(address1.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_1_PHONE = " + Escape(employment1.Phone) + ", " +
                "EMP_1_FAX = " + Escape(employment1.Fax) + ", " +
                "EMP_1_PC_REF_SP = " + Escape(employment1.PostalCodeReflectPractice) + ", ";

                if (employment1.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment1.StartDate;
                    sql_UpdateEmp += "EMP_1_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_1_START_DATE = NULL, ";
                }

                if (employment1.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment1.EndDate;
                    sql_UpdateEmp += "CURRENT_EMPLOYMENT_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "CURRENT_EMPLOYMENT_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_1_CATEGORY = " + Escape(employment1.EmploymentRelationship) + ", " +
                "EMP_1_FULL_PT_STATUS = " + Escape(employment1.CasualStatus) + ", " +
                "EMP_1_HOURS_WORKED_WEEK = " + employment1.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_1_POSITION = " + Escape(employment1.PrimaryRole) + ", " +
                "EMP_1_TYPE = " + Escape(employment1.PracticeSetting) + ", " +
                "EMP_1_AREA_PRACTICE = " + Escape(employment1.MajorServices) + ", " +
                "EMP_1_CLNT_ARNGE = " + Escape(employment1.ClientAgeRange) + ", " +
                "EMP_1_FUNDING_SOURCE = " + Escape(employment1.FundingSource) + ", " +
                "EMP_1_HEALTH_CONDITION = " + Escape(employment1.HealthCondition) + ", ";

                // second employment
                sql_UpdateEmp +=
                "EMP_2_SOURCE = " + Escape(employment2.Status) + ", " +
                "EMP_2_NAME = " + Escape(employment2.EmployerName) + ", ";
                var address2 = employment2.Address;
                if (address2 != null)
                {
                    sql_UpdateEmp += "EMP_2_ADDR_1 = " + Escape(address2.Address1) + ", " +
                    "EMP_2_ADDR_2 = " + Escape(address2.Address2) + ", " +
                    "EMP_2_CITY = " + Escape(address2.City) + ", " +
                    "EMP_2_PROV = " + Escape(address2.Province) + ", " +
                    "EMP_2_POSTAL = " + Escape(address2.PostalCode) + ", " +
                    "EMP_2_COUNTRY = " + Escape(address2.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_2_PHONE = " + Escape(employment2.Phone) + ", " +
                "EMP_2_FAX = " + Escape(employment2.Fax) + ", " +
                "EMP_2_PC_REF_SP = " + Escape(employment2.PostalCodeReflectPractice) + ", ";

                if (employment2.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment2.StartDate;
                    sql_UpdateEmp += "EMP_2_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_2_START_DATE = NULL, ";
                }

                if (employment2.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment2.EndDate;
                    sql_UpdateEmp += "EMP_2_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_2_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_2_CATEGORY = " + Escape(employment2.EmploymentRelationship) + ", " +
                "EMP_2_FULL_PT_STATUS = " + Escape(employment2.CasualStatus) + ", " +
                "EMP_2_HOURS_WORKED_WEEK = " + employment2.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_2_POSITION = " + Escape(employment2.PrimaryRole) + ", " +
                "EMP_2_TYPE = " + Escape(employment2.PracticeSetting) + ", " +
                "EMP_2_AREA_PRACTICE = " + Escape(employment2.MajorServices) + ", " +
                "EMP_2_CLNT_ARNGE = " + Escape(employment2.ClientAgeRange) + ", " +
                "EMP_2_FUNDING_SOURCE = " + Escape(employment2.FundingSource) + ", " +
                "EMP_2_HEALTH_CONDITION = " + Escape(employment2.HealthCondition) + ", ";

                // third employment
                sql_UpdateEmp +=
                "EMP_3_SOURCE = " + Escape(employment3.Status) + ", " +
                "EMP_3_NAME = " + Escape(employment3.EmployerName) + ", ";
                var address3 = employment3.Address;
                if (address2 != null)
                {
                    sql_UpdateEmp += "EMP_3_ADDR_1 = " + Escape(address3.Address1) + ", " +
                    "EMP_3_ADDR_2 = " + Escape(address3.Address2) + ", " +
                    "EMP_3_CITY = " + Escape(address3.City) + ", " +
                    "EMP_3_PROV = " + Escape(address3.Province) + ", " +
                    "EMP_3_POSTAL = " + Escape(address3.PostalCode) + ", " +
                    "EMP_3_COUNTRY = " + Escape(address3.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_3_PHONE = " + Escape(employment3.Phone) + ", " +
                "EMP_3_FAX = " + Escape(employment3.Fax) + ", " +
                "EMP_3_PC_REF_SP = " + Escape(employment3.PostalCodeReflectPractice) + ", ";

                if (employment3.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment3.StartDate;
                    sql_UpdateEmp += "EMP_3_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_3_START_DATE = NULL, ";
                }

                if (employment3.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment3.EndDate;
                    sql_UpdateEmp += "EMP_3_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_3_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_3_CATEGORY = " + Escape(employment3.EmploymentRelationship) + ", " +
                "EMP_3_FULL_PT_STATUS = " + Escape(employment3.CasualStatus) + ", " +
                "EMP_3_HOURS_WORKED_WEEK = " + employment3.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_3_POSITION = " + Escape(employment3.PrimaryRole) + ", " +
                "EMP_3_TYPE = " + Escape(employment3.PracticeSetting) + ", " +
                "EMP_3_AREA_PRACTICE = " + Escape(employment3.MajorServices) + ", " +
                "EMP_3_CLNT_ARNGE = " + Escape(employment3.ClientAgeRange) + ", " +
                "EMP_3_FUNDING_SOURCE = " + Escape(employment3.FundingSource) + ", " +
                "EMP_3_HEALTH_CONDITION = " + Escape(employment3.HealthCondition) +

                " WHERE ID = " + Escape(user.Id);

                db.ExecuteCommand(sql_UpdateEmp);
            }
            if (!(user.CurrentEmploymentStatus == "10" || user.CurrentEmploymentStatus == "11"))
            {
                UpdateUserEmploymentPersonalClearAdditionalFields(user.Id);
            }
        }

        public void UpdateUserEmploymentPersonalLogged(User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            var employment1 = user.EmploymentList.Find(E => E.Index == 1);
            var employment2 = user.EmploymentList.Find(E => E.Index == 2);
            var employment3 = user.EmploymentList.Find(E => E.Index == 3);
            if (employment1 != null && employment2 != null && employment3 != null)
            {
                string prevCurrentEmploymentStatus = string.Empty;
                string prevCurrentWorkPreference = string.Empty;
                string prevNaturePractice = string.Empty;
                string prevMoreThreePract = string.Empty;
                string prevCollegeMailing = string.Empty;

                string prevEmp1Status = string.Empty;
                string prevEmp1EmployerName = string.Empty;
                string prevEmp1Address1 = string.Empty;
                string prevEmp1Address2 = string.Empty;
                string prevEmp1City = string.Empty;
                string prevEmp1Prov = string.Empty;
                string prevEmp1Postal = string.Empty;
                string prevEmp1Country = string.Empty;
                string prevEmp1Phone = string.Empty;
                string prevEmp1Fax = string.Empty;
                string prevEmp1PostalCodeReflectPractice = string.Empty;
                string prevEmp1StartDate = string.Empty;
                string prevEmp1EndDate = string.Empty;
                string prevEmp1Category = string.Empty;
                string prevEmp1CasualStatus = string.Empty;
                string prevEmp1AvgWeeklyHours = string.Empty;
                string prevEmp1PrimaryRole = string.Empty;
                string prevEmp1PracticeSetting = string.Empty;
                string prevEmp1MajorService = string.Empty;
                string prevEmp1ClientAgeRange = string.Empty;
                string prevEmp1FundingSource = string.Empty;
                string prevEmp1HealthCondition = string.Empty;

                string prevEmp2Status = string.Empty;
                string prevEmp2EmployerName = string.Empty;
                string prevEmp2Address1 = string.Empty;
                string prevEmp2Address2 = string.Empty;
                string prevEmp2City = string.Empty;
                string prevEmp2Prov = string.Empty;
                string prevEmp2Postal = string.Empty;
                string prevEmp2Country = string.Empty;
                string prevEmp2Phone = string.Empty;
                string prevEmp2Fax = string.Empty;
                string prevEmp2PostalCodeReflectPractice = string.Empty;
                string prevEmp2StartDate = string.Empty;
                string prevEmp2EndDate = string.Empty;
                string prevEmp2Category = string.Empty;
                string prevEmp2CasualStatus = string.Empty;
                string prevEmp2AvgWeeklyHours = string.Empty;
                string prevEmp2PrimaryRole = string.Empty;
                string prevEmp2PracticeSetting = string.Empty;
                string prevEmp2MajorService = string.Empty;
                string prevEmp2ClientAgeRange = string.Empty;
                string prevEmp2FundingSource = string.Empty;
                string prevEmp2HealthCondition = string.Empty;

                string prevEmp3Status = string.Empty;
                string prevEmp3EmployerName = string.Empty;
                string prevEmp3Address1 = string.Empty;
                string prevEmp3Address2 = string.Empty;
                string prevEmp3City = string.Empty;
                string prevEmp3Prov = string.Empty;
                string prevEmp3Postal = string.Empty;
                string prevEmp3Country = string.Empty;
                string prevEmp3Phone = string.Empty;
                string prevEmp3Fax = string.Empty;
                string prevEmp3PostalCodeReflectPractice = string.Empty;
                string prevEmp3StartDate = string.Empty;
                string prevEmp3EndDate = string.Empty;
                string prevEmp3Category = string.Empty;
                string prevEmp3CasualStatus = string.Empty;
                string prevEmp3AvgWeeklyHours = string.Empty;
                string prevEmp3PrimaryRole = string.Empty;
                string prevEmp3PracticeSetting = string.Empty;
                string prevEmp3MajorService = string.Empty;
                string prevEmp3ClientAgeRange = string.Empty;
                string prevEmp3FundingSource = string.Empty;
                string prevEmp3HealthCondition = string.Empty;

                string sql_GetCurrentEmployment = "SELECT * FROM EMPLOYMENT_ALL WHERE ID=" + Escape(user.Id);
                var table = db.GetData(sql_GetCurrentEmployment);
                if (table != null && table.Rows.Count == 1)
                {
                    DataRow row = table.Rows[0];
                    prevCurrentEmploymentStatus = (string)row["CURRENT_EMPLOYMENT_STATUS"];
                    prevCurrentWorkPreference = (string)row["FULL_PT_STATUS_PREF"];
                    prevNaturePractice = (string)row["NATURE_OF_PRACTICE"];
                    prevMoreThreePract = (string)row["MORE_THREE_PRACT_SITES"];
                    prevCollegeMailing = (string)row["PRIMARY_EMPL_PREFERRED"];
                    #region mep1
                    prevEmp1Status = (string)row["EMP_1_SOURCE"];
                    prevEmp1EmployerName = (string)row["EMP_1_NAME"];
                    prevEmp1Address1 = (string)row["EMP_1_ADDR_1"];
                    prevEmp1Address2 = (string)row["EMP_1_ADDR_2"];
                    prevEmp1City = (string)row["EMP_1_CITY"];
                    prevEmp1Prov = (string)row["EMP_1_PROV"];
                    prevEmp1Postal = (string)row["EMP_1_POSTAL"];
                    prevEmp1Country = (string)row["EMP_1_COUNTRY"];
                    prevEmp1Phone = (string)row["EMP_1_PHONE"];
                    prevEmp1Fax = (string)row["EMP_1_FAX"];
                    prevEmp1PostalCodeReflectPractice = (string)row["EMP_1_PC_REF_SP"];
                    if (row["EMP_1_START_DATE"] != DBNull.Value)
                    {
                        prevEmp1StartDate = ((DateTime)row["EMP_1_START_DATE"]).ToString("dd/MM/yyyy");
                    }
                    if (row["CURRENT_EMPLOYMENT_END_DATE"] != DBNull.Value)
                    {
                        prevEmp1EndDate = ((DateTime)row["CURRENT_EMPLOYMENT_END_DATE"]).ToString("dd/MM/yyyy");
                    }

                    prevEmp1Category = (string)row["EMP_1_CATEGORY"];
                    prevEmp1CasualStatus = (string)row["EMP_1_FULL_PT_STATUS"];
                    prevEmp1AvgWeeklyHours = ((double)row["EMP_1_HOURS_WORKED_WEEK"]).ToString();
                    prevEmp1PrimaryRole = (string)row["EMP_1_POSITION"];
                    prevEmp1PracticeSetting = (string)row["EMP_1_TYPE"];
                    prevEmp1MajorService = (string)row["EMP_1_AREA_PRACTICE"];
                    prevEmp1ClientAgeRange = (string)row["EMP_1_CLNT_ARNGE"];
                    prevEmp1FundingSource = (string)row["EMP_1_FUNDING_SOURCE"];
                    prevEmp1HealthCondition = (string)row["EMP_1_HEALTH_CONDITION"];
                    #endregion
                    #region emp2
                    prevEmp2Status = (string)row["EMP_2_SOURCE"];
                    prevEmp2EmployerName = (string)row["EMP_2_NAME"];
                    prevEmp2Address1 = (string)row["EMP_2_ADDR_1"];
                    prevEmp2Address2 = (string)row["EMP_2_ADDR_2"];
                    prevEmp2City = (string)row["EMP_2_CITY"];
                    prevEmp2Prov = (string)row["EMP_2_PROV"];
                    prevEmp2Postal = (string)row["EMP_2_POSTAL"];
                    prevEmp2Country = (string)row["EMP_2_COUNTRY"];
                    prevEmp2Phone = (string)row["EMP_2_PHONE"];
                    prevEmp2Fax = (string)row["EMP_2_FAX"];
                    prevEmp2PostalCodeReflectPractice = (string)row["EMP_2_PC_REF_SP"];
                    if (row["EMP_2_START_DATE"] != DBNull.Value)
                    {
                        prevEmp2StartDate = ((DateTime)row["EMP_2_START_DATE"]).ToString("dd/MM/yyyy");
                    }
                    if (row["EMP_2_END_DATE"] != DBNull.Value)
                    {
                        prevEmp2EndDate = ((DateTime)row["EMP_2_END_DATE"]).ToString("dd/MM/yyyy");
                    }

                    prevEmp2Category = (string)row["EMP_2_CATEGORY"];
                    prevEmp2CasualStatus = (string)row["EMP_2_FULL_PT_STATUS"];
                    prevEmp2AvgWeeklyHours = ((double)row["EMP_2_HOURS_WORKED_WEEK"]).ToString();
                    prevEmp2PrimaryRole = (string)row["EMP_2_POSITION"];
                    prevEmp2PracticeSetting = (string)row["EMP_2_TYPE"];
                    prevEmp2MajorService = (string)row["EMP_2_AREA_PRACTICE"];
                    prevEmp2ClientAgeRange = (string)row["EMP_2_CLNT_ARNGE"];
                    prevEmp2FundingSource = (string)row["EMP_2_FUNDING_SOURCE"];
                    prevEmp2HealthCondition = (string)row["EMP_2_HEALTH_CONDITION"];
                    #endregion
                    #region emp3
                    prevEmp3Status = (string)row["EMP_3_SOURCE"];
                    prevEmp3EmployerName = (string)row["EMP_3_NAME"];
                    prevEmp3Address1 = (string)row["EMP_3_ADDR_1"];
                    prevEmp3Address2 = (string)row["EMP_3_ADDR_2"];
                    prevEmp3City = (string)row["EMP_3_CITY"];
                    prevEmp3Prov = (string)row["EMP_3_PROV"];
                    prevEmp3Postal = (string)row["EMP_3_POSTAL"];
                    prevEmp3Country = (string)row["EMP_3_COUNTRY"];
                    prevEmp3Phone = (string)row["EMP_3_PHONE"];
                    prevEmp3Fax = (string)row["EMP_3_FAX"];
                    prevEmp3PostalCodeReflectPractice = (string)row["EMP_3_PC_REF_SP"];
                    if (row["EMP_3_START_DATE"] != DBNull.Value)
                    {
                        prevEmp3StartDate = ((DateTime)row["EMP_3_START_DATE"]).ToString("dd/MM/yyyy");
                    }
                    if (row["EMP_3_END_DATE"] != DBNull.Value)
                    {
                        prevEmp3EndDate = ((DateTime)row["EMP_3_END_DATE"]).ToString("dd/MM/yyyy");
                    }

                    prevEmp3Category = (string)row["EMP_3_CATEGORY"];
                    prevEmp3CasualStatus = (string)row["EMP_3_FULL_PT_STATUS"];
                    prevEmp3AvgWeeklyHours = ((double)row["EMP_3_HOURS_WORKED_WEEK"]).ToString();
                    prevEmp3PrimaryRole = (string)row["EMP_3_POSITION"];
                    prevEmp3PracticeSetting = (string)row["EMP_3_TYPE"];
                    prevEmp3MajorService = (string)row["EMP_3_AREA_PRACTICE"];
                    prevEmp3ClientAgeRange = (string)row["EMP_3_CLNT_ARNGE"];
                    prevEmp3FundingSource = (string)row["EMP_3_FUNDING_SOURCE"];
                    prevEmp3HealthCondition = (string)row["EMP_3_HEALTH_CONDITION"];
                    #endregion
                }

                string sql_UpdateEmp = "UPDATE EMPLOYMENT_ALL SET ";
                // update morethreepractsites and current employment status
                sql_UpdateEmp += "CURRENT_EMPLOYMENT_STATUS = " + Escape(user.CurrentEmploymentStatus) + ", " +
                " FULL_PT_STATUS_PREF = " + Escape(user.CurrentWorkPreference.ToString()) + ", " +
                " NATURE_OF_PRACTICE = " + Escape(user.NaturePractice.ToString()) + ", ";

                string morethree = string.Empty;
                if (user.MoreThreePractSites != null)
                {
                    bool bln_MoreThree = (bool)user.MoreThreePractSites;
                    morethree = bln_MoreThree ? "Yes" : "No";
                }

                sql_UpdateEmp += " MORE_THREE_PRACT_SITES = " + Escape(morethree) + ", " +
                  " PRIMARY_EMPL_PREFERRED = " + Escape(user.CollegeMailings) + ", ";
                // first employment
                sql_UpdateEmp += " EMP_1_SOURCE = " + Escape(employment1.Status) + ", " +
                " EMP_1_NAME = " + Escape(employment1.EmployerName) + ", ";
                var address1 = employment1.Address;
                if (address1 != null)
                {
                    sql_UpdateEmp += "EMP_1_ADDR_1 = " + Escape(address1.Address1) + ", " +
                    "EMP_1_ADDR_2 = " + Escape(address1.Address2) + ", " +
                    "EMP_1_CITY = " + Escape(address1.City) + ", " +
                    "EMP_1_PROV = " + Escape(address1.Province) + ", " +
                    "EMP_1_POSTAL = " + Escape(address1.PostalCode) + ", " +
                    "EMP_1_COUNTRY = " + Escape(address1.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_1_PHONE = " + Escape(employment1.Phone) + ", " +
                "EMP_1_FAX = " + Escape(employment1.Fax) + ", " +
                "EMP_1_PC_REF_SP = " + Escape(employment1.PostalCodeReflectPractice) + ", ";

                if (employment1.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment1.StartDate;
                    sql_UpdateEmp += "EMP_1_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_1_START_DATE = NULL, ";
                }

                if (employment1.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment1.EndDate;
                    sql_UpdateEmp += "CURRENT_EMPLOYMENT_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "CURRENT_EMPLOYMENT_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_1_CATEGORY = " + Escape(employment1.EmploymentRelationship) + ", " +
                "EMP_1_FULL_PT_STATUS = " + Escape(employment1.CasualStatus) + ", " +
                "EMP_1_HOURS_WORKED_WEEK = " + employment1.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_1_POSITION = " + Escape(employment1.PrimaryRole) + ", " +
                "EMP_1_TYPE = " + Escape(employment1.PracticeSetting) + ", " +
                "EMP_1_AREA_PRACTICE = " + Escape(employment1.MajorServices) + ", " +
                "EMP_1_CLNT_ARNGE = " + Escape(employment1.ClientAgeRange) + ", " +
                "EMP_1_FUNDING_SOURCE = " + Escape(employment1.FundingSource) + ", " +
                "EMP_1_HEALTH_CONDITION = " + Escape(employment1.HealthCondition) + ", ";

                // second employment
                sql_UpdateEmp +=
                "EMP_2_SOURCE = " + Escape(employment2.Status) + ", " +
                "EMP_2_NAME = " + Escape(employment2.EmployerName) + ", ";
                var address2 = employment2.Address;
                if (address2 != null)
                {
                    sql_UpdateEmp += "EMP_2_ADDR_1 = " + Escape(address2.Address1) + ", " +
                    "EMP_2_ADDR_2 = " + Escape(address2.Address2) + ", " +
                    "EMP_2_CITY = " + Escape(address2.City) + ", " +
                    "EMP_2_PROV = " + Escape(address2.Province) + ", " +
                    "EMP_2_POSTAL = " + Escape(address2.PostalCode) + ", " +
                    "EMP_2_COUNTRY = " + Escape(address2.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_2_PHONE = " + Escape(employment2.Phone) + ", " +
                "EMP_2_FAX = " + Escape(employment2.Fax) + ", " +
                "EMP_2_PC_REF_SP = " + Escape(employment2.PostalCodeReflectPractice) + ", ";

                if (employment2.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment2.StartDate;
                    sql_UpdateEmp += "EMP_2_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_2_START_DATE = NULL, ";
                }

                if (employment2.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment2.EndDate;
                    sql_UpdateEmp += "EMP_2_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_2_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_2_CATEGORY = " + Escape(employment2.EmploymentRelationship) + ", " +
                "EMP_2_FULL_PT_STATUS = " + Escape(employment2.CasualStatus) + ", " +
                "EMP_2_HOURS_WORKED_WEEK = " + employment2.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_2_POSITION = " + Escape(employment2.PrimaryRole) + ", " +
                "EMP_2_TYPE = " + Escape(employment2.PracticeSetting) + ", " +
                "EMP_2_AREA_PRACTICE = " + Escape(employment2.MajorServices) + ", " +
                "EMP_2_CLNT_ARNGE = " + Escape(employment2.ClientAgeRange) + ", " +
                "EMP_2_FUNDING_SOURCE = " + Escape(employment2.FundingSource) + ", " +
                "EMP_2_HEALTH_CONDITION = " + Escape(employment2.HealthCondition) + ", ";

                // third employment
                sql_UpdateEmp +=
                "EMP_3_SOURCE = " + Escape(employment3.Status) + ", " +
                "EMP_3_NAME = " + Escape(employment3.EmployerName) + ", ";
                var address3 = employment3.Address;
                if (address2 != null)
                {
                    sql_UpdateEmp += "EMP_3_ADDR_1 = " + Escape(address3.Address1) + ", " +
                    "EMP_3_ADDR_2 = " + Escape(address3.Address2) + ", " +
                    "EMP_3_CITY = " + Escape(address3.City) + ", " +
                    "EMP_3_PROV = " + Escape(address3.Province) + ", " +
                    "EMP_3_POSTAL = " + Escape(address3.PostalCode) + ", " +
                    "EMP_3_COUNTRY = " + Escape(address3.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_3_PHONE = " + Escape(employment3.Phone) + ", " +
                "EMP_3_FAX = " + Escape(employment3.Fax) + ", " +
                "EMP_3_PC_REF_SP = " + Escape(employment3.PostalCodeReflectPractice) + ", ";

                if (employment3.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment3.StartDate;
                    sql_UpdateEmp += "EMP_3_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_3_START_DATE = NULL, ";
                }

                if (employment3.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment3.EndDate;
                    sql_UpdateEmp += "EMP_3_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_3_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_3_CATEGORY = " + Escape(employment3.EmploymentRelationship) + ", " +
                "EMP_3_FULL_PT_STATUS = " + Escape(employment3.CasualStatus) + ", " +
                "EMP_3_HOURS_WORKED_WEEK = " + employment3.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_3_POSITION = " + Escape(employment3.PrimaryRole) + ", " +
                "EMP_3_TYPE = " + Escape(employment3.PracticeSetting) + ", " +
                "EMP_3_AREA_PRACTICE = " + Escape(employment3.MajorServices) + ", " +
                "EMP_3_CLNT_ARNGE = " + Escape(employment3.ClientAgeRange) + ", " +
                "EMP_3_FUNDING_SOURCE = " + Escape(employment3.FundingSource) + ", " +
                "EMP_3_HEALTH_CONDITION = " + Escape(employment3.HealthCondition) +

                " WHERE ID = " + Escape(user.Id);

                db.ExecuteCommand(sql_UpdateEmp);
                
                if (prevCurrentEmploymentStatus != user.CurrentEmploymentStatus)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.CURRENT_EMPLOYMENT_STATUS: " + prevCurrentEmploymentStatus + " -> " + user.CurrentEmploymentStatus);
                }

                if (prevCurrentWorkPreference != user.CurrentWorkPreference)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.FULL_PT_STATUS_PREF: " + prevCurrentWorkPreference + " -> " + user.CurrentWorkPreference);
                }

                if (prevNaturePractice != user.NaturePractice)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.NATURE_OF_PRACTICE: " + prevNaturePractice + " -> " + user.NaturePractice);
                }

                if (prevMoreThreePract != (morethree))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.MORE_THREE_PRACT_SITES: " + prevMoreThreePract + " -> " + (morethree));
                }

                if (prevCollegeMailing != user.CollegeMailings)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.PRIMARY_EMPL_PREFERRED: " + prevCollegeMailing + " -> " + user.CollegeMailings);
                }

                #region emp1
                if (prevEmp1Status != employment1.Status)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_SOURCE: " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", prevEmp1Status) + " -> " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", employment1.Status));
                }
                if (prevEmp1EmployerName != employment1.EmployerName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_NAME: " + prevEmp1EmployerName + " -> " + employment1.EmployerName);
                }
                if (prevEmp1Address1 != employment1.Address.Address1)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_ADDR_1: " + prevEmp1Address1 + " -> " + employment1.Address.Address1);
                }
                if (prevEmp1Address2 != employment1.Address.Address2)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_ADDR_2: " + prevEmp1Address2 + " -> " + employment1.Address.Address2);
                }
                if (prevEmp1City != employment1.Address.City)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_CITY: " + prevEmp1City + " -> " + employment1.Address.City);
                }
                if (prevEmp1Prov != employment1.Address.Province)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_PROV: " + prevEmp1Prov + " -> " + employment1.Address.Province);
                }
                if (prevEmp1Postal != employment1.Address.PostalCode)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_POSTAL: " + prevEmp1Postal + " -> " + employment1.Address.PostalCode);
                }
                if (prevEmp1Country != employment1.Address.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_COUNTRY: " + prevEmp1Country + " -> " + employment1.Address.Country);
                }
                if (prevEmp1Phone != employment1.Phone)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_PHONE: " + prevEmp1Phone + " -> " + employment1.Phone);
                }
                if (prevEmp1Fax != employment1.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_FAX: " + prevEmp1Fax + " -> " + employment1.Fax);
                }
                if (prevEmp1PostalCodeReflectPractice != employment1.PostalCodeReflectPractice)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_PC_REF_SP: " + prevEmp1PostalCodeReflectPractice + " -> " + employment1.PostalCodeReflectPractice);
                }
                if (prevEmp1Fax != employment1.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_FAX: " + prevEmp1Fax + " -> " + employment1.Fax);
                }
                if (prevEmp1StartDate != (employment1.StartDate != null ? ((DateTime)employment1.StartDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_START_DATE: " + prevEmp1StartDate + " -> " + (employment1.StartDate != null ? ((DateTime)employment1.StartDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp1EndDate != (employment1.EndDate != null ? ((DateTime)employment1.EndDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.CURRENT_EMPLOYMENT_END_DATE: " + prevEmp1EndDate + " -> " + (employment1.EndDate != null ? ((DateTime)employment1.EndDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp1Category != employment1.EmploymentRelationship)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_CATEGORY: " + GetGeneralNameImprv("EMPLOY_CATEGORY", prevEmp1Category) + " -> " + GetGeneralNameImprv("EMPLOY_CATEGORY", employment1.EmploymentRelationship));
                }
                if (prevEmp1CasualStatus != employment1.CasualStatus)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_FULL_PT_STATUS: " + GetGeneralNameImprv("FULL_PT_STATUS", prevEmp1CasualStatus) + " -> " + GetGeneralNameImprv("FULL_PT_STATUS", employment1.CasualStatus));
                }
                if (prevEmp1AvgWeeklyHours != employment1.AverageWeeklyHours.ToString())
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_HOURS_WORKED_WEEK: " + prevEmp1AvgWeeklyHours + " -> " + employment1.AverageWeeklyHours.ToString());
                }
                if (prevEmp1PrimaryRole != employment1.PrimaryRole)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_POSITION: " + GetGeneralNameImprv("POSITION", prevEmp1PrimaryRole) + " -> " + GetGeneralNameImprv("POSITION", employment1.PrimaryRole));
                }
                if (prevEmp1PracticeSetting != employment1.PracticeSetting)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_TYPE: " + GetGeneralNameImprv("EMPLOYER_TYPE", prevEmp1PracticeSetting) + " -> " + GetGeneralNameImprv("EMPLOYER_TYPE", employment1.PracticeSetting));
                }
                if (prevEmp1MajorService != employment1.MajorServices)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_AREA_PRACTICE: " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", prevEmp1MajorService) + " -> " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", employment1.MajorServices));
                }
                if (prevEmp1ClientAgeRange != employment1.ClientAgeRange)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_CLNT_ARNGE: " + GetGeneralNameImprv("CLIENT_AGE_RANGE", prevEmp1ClientAgeRange) + " -> " + GetGeneralNameImprv("CLIENT_AGE_RANGE", employment1.ClientAgeRange));
                }
                if (prevEmp1FundingSource != employment1.FundingSource)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_FUNDING_SOURCE: " + GetGeneralNameImprv("FUNDING", prevEmp1FundingSource) + " -> " + GetGeneralNameImprv("FUNDING", employment1.FundingSource));
                }
                if (prevEmp1HealthCondition != employment1.HealthCondition)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_1_HEALTH_CONDITION: " + GetGeneralNameImprv("HEALTH_CONDITION", prevEmp1HealthCondition) + " -> " + GetGeneralNameImprv("HEALTH_CONDITION", employment1.HealthCondition));
                }
                #endregion
                #region emp2
                if (prevEmp2Status != employment2.Status)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_SOURCE: " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", prevEmp2Status) + " -> " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", employment2.Status));
                }
                if (prevEmp2EmployerName != employment2.EmployerName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_NAME: " + prevEmp2EmployerName + " -> " + employment2.EmployerName);
                }
                if (prevEmp2Address1 != employment2.Address.Address1)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_ADDR_1: " + prevEmp2Address1 + " -> " + employment2.Address.Address1);
                }
                if (prevEmp2Address2 != employment2.Address.Address2)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_ADDR_2: " + prevEmp2Address2 + " -> " + employment2.Address.Address2);
                }
                if (prevEmp2City != employment2.Address.City)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_CITY: " + prevEmp2City + " -> " + employment2.Address.City);
                }
                if (prevEmp2Prov != employment2.Address.Province)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_PROV: " + prevEmp2Prov + " -> " + employment2.Address.Province);
                }
                if (prevEmp2Postal != employment2.Address.PostalCode)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_POSTAL: " + prevEmp2Postal + " -> " + employment2.Address.PostalCode);
                }
                if (prevEmp2Country != employment2.Address.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_COUNTRY: " + prevEmp2Country + " -> " + employment2.Address.Country);
                }
                if (prevEmp2Phone != employment2.Phone)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_PHONE: " + prevEmp2Phone + " -> " + employment2.Phone);
                }
                if (prevEmp2Fax != employment2.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_FAX: " + prevEmp2Fax + " -> " + employment2.Fax);
                }
                if (prevEmp2PostalCodeReflectPractice != employment2.PostalCodeReflectPractice)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_PC_REF_SP: " + prevEmp2PostalCodeReflectPractice + " -> " + employment2.PostalCodeReflectPractice);
                }
                if (prevEmp2Fax != employment2.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_FAX: " + prevEmp2Fax + " -> " + employment2.Fax);
                }
                if (prevEmp2StartDate != (employment2.StartDate != null ? ((DateTime)employment2.StartDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_START_DATE: " + prevEmp2StartDate + " -> " + (employment2.StartDate != null ? ((DateTime)employment2.StartDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp2EndDate != (employment2.EndDate != null ? ((DateTime)employment2.EndDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_END_DATE: " + prevEmp2EndDate + " -> " + (employment2.EndDate != null ? ((DateTime)employment2.EndDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp2Category != employment2.EmploymentRelationship)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_CATEGORY: " + GetGeneralNameImprv("EMPLOY_CATEGORY", prevEmp2Category) + " -> " + GetGeneralNameImprv("EMPLOY_CATEGORY", employment2.EmploymentRelationship));
                }
                if (prevEmp2CasualStatus != employment2.CasualStatus)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_FULL_PT_STATUS: " + GetGeneralNameImprv("FULL_PT_STATUS", prevEmp2CasualStatus) + " -> " + GetGeneralNameImprv("FULL_PT_STATUS", employment2.CasualStatus));
                }
                if (prevEmp2AvgWeeklyHours != employment2.AverageWeeklyHours.ToString())
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_HOURS_WORKED_WEEK: " + prevEmp2AvgWeeklyHours + " -> " + employment2.AverageWeeklyHours.ToString());
                }
                if (prevEmp2PrimaryRole != employment2.PrimaryRole)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_POSITION: " + GetGeneralNameImprv("POSITION", prevEmp2PrimaryRole) + " -> " + GetGeneralNameImprv("POSITION", employment2.PrimaryRole));
                }
                if (prevEmp2PracticeSetting != employment2.PracticeSetting)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_TYPE: " + GetGeneralNameImprv("EMPLOYER_TYPE", prevEmp2PracticeSetting) + " -> " + GetGeneralNameImprv("EMPLOYER_TYPE", employment2.PracticeSetting));
                }
                if (prevEmp2MajorService != employment2.MajorServices)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_AREA_PRACTICE: " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", prevEmp2MajorService) + " -> " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", employment2.MajorServices));
                }
                if (prevEmp2ClientAgeRange != employment2.ClientAgeRange)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_CLNT_ARNGE: " + GetGeneralNameImprv("CLIENT_AGE_RANGE", prevEmp2ClientAgeRange) + " -> " + GetGeneralNameImprv("CLIENT_AGE_RANGE", employment2.ClientAgeRange));
                }
                if (prevEmp2FundingSource != employment2.FundingSource)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_FUNDING_SOURCE: " + GetGeneralNameImprv("FUNDING", prevEmp2FundingSource) + " -> " + GetGeneralNameImprv("FUNDING", employment2.FundingSource));
                }
                if (prevEmp2HealthCondition != employment2.HealthCondition)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_2_HEALTH_CONDITION: " + GetGeneralNameImprv("HEALTH_CONDITION", prevEmp2HealthCondition) + " -> " + GetGeneralNameImprv("HEALTH_CONDITION", employment2.HealthCondition));
                }
                #endregion
                #region emp3
                if (prevEmp3Status != employment3.Status)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_SOURCE: " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", prevEmp3Status) + " -> " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", employment3.Status));
                }
                if (prevEmp3EmployerName != employment3.EmployerName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_NAME: " + prevEmp3EmployerName + " -> " + employment3.EmployerName);
                }
                if (prevEmp3Address1 != employment3.Address.Address1)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_ADDR_1: " + prevEmp3Address1 + " -> " + employment3.Address.Address1);
                }
                if (prevEmp3Address2 != employment3.Address.Address2)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_ADDR_2: " + prevEmp3Address2 + " -> " + employment3.Address.Address2);
                }
                if (prevEmp3City != employment3.Address.City)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_CITY: " + prevEmp3City + " -> " + employment3.Address.City);
                }
                if (prevEmp3Prov != employment3.Address.Province)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_PROV: " + prevEmp3Prov + " -> " + employment3.Address.Province);
                }
                if (prevEmp3Postal != employment3.Address.PostalCode)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_POSTAL: " + prevEmp3Postal + " -> " + employment3.Address.PostalCode);
                }
                if (prevEmp3Country != employment3.Address.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_COUNTRY: " + prevEmp3Country + " -> " + employment3.Address.Country);
                }
                if (prevEmp3Phone != employment3.Phone)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_PHONE: " + prevEmp3Phone + " -> " + employment3.Phone);
                }
                if (prevEmp3Fax != employment3.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_FAX: " + prevEmp3Fax + " -> " + employment3.Fax);
                }
                if (prevEmp3PostalCodeReflectPractice != employment3.PostalCodeReflectPractice)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_PC_REF_SP: " + prevEmp3PostalCodeReflectPractice + " -> " + employment3.PostalCodeReflectPractice);
                }
                if (prevEmp3Fax != employment3.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_FAX: " + prevEmp3Fax + " -> " + employment3.Fax);
                }
                if (prevEmp3StartDate != (employment3.StartDate != null ? ((DateTime)employment3.StartDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_START_DATE: " + prevEmp3StartDate + " -> " + (employment3.StartDate != null ? ((DateTime)employment3.StartDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp3EndDate != (employment3.EndDate != null ? ((DateTime)employment3.EndDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_END_DATE: " + prevEmp3EndDate + " -> " + (employment3.EndDate != null ? ((DateTime)employment3.EndDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp3Category != employment3.EmploymentRelationship)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_CATEGORY: " + GetGeneralNameImprv("EMPLOY_CATEGORY", prevEmp3Category) + " -> " + GetGeneralNameImprv("EMPLOY_CATEGORY", employment3.EmploymentRelationship));
                }
                if (prevEmp3CasualStatus != employment3.CasualStatus)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_FULL_PT_STATUS: " + GetGeneralNameImprv("FULL_PT_STATUS", prevEmp3CasualStatus) + " -> " + GetGeneralNameImprv("FULL_PT_STATUS", employment3.CasualStatus));
                }
                if (prevEmp3AvgWeeklyHours != employment3.AverageWeeklyHours.ToString())
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_HOURS_WORKED_WEEK: " + prevEmp3AvgWeeklyHours + " -> " + employment3.AverageWeeklyHours.ToString());
                }
                if (prevEmp3PrimaryRole != employment3.PrimaryRole)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_POSITION: " + GetGeneralNameImprv("POSITION", prevEmp3PrimaryRole) + " -> " + GetGeneralNameImprv("POSITION", employment3.PrimaryRole));
                }
                if (prevEmp3PracticeSetting != employment3.PracticeSetting)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_TYPE: " + GetGeneralNameImprv("EMPLOYER_TYPE", prevEmp3PracticeSetting) + " -> " + GetGeneralNameImprv("EMPLOYER_TYPE", employment3.PracticeSetting));
                }
                if (prevEmp3MajorService != employment3.MajorServices)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_AREA_PRACTICE: " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", prevEmp3MajorService) + " -> " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", employment3.MajorServices));
                }
                if (prevEmp3ClientAgeRange != employment3.ClientAgeRange)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_CLNT_ARNGE: " + GetGeneralNameImprv("CLIENT_AGE_RANGE", prevEmp3ClientAgeRange) + " -> " + GetGeneralNameImprv("CLIENT_AGE_RANGE", employment3.ClientAgeRange));
                }
                if (prevEmp3FundingSource != employment3.FundingSource)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_FUNDING_SOURCE: " + GetGeneralNameImprv("FUNDING", prevEmp3FundingSource) + " -> " + GetGeneralNameImprv("FUNDING", employment3.FundingSource));
                }
                if (prevEmp3HealthCondition != employment3.HealthCondition)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", user.Id, user.Id, "EMPLOYMENT_ALL.EMP_3_HEALTH_CONDITION: " + GetGeneralNameImprv("HEALTH_CONDITION", prevEmp3HealthCondition) + " -> " + GetGeneralNameImprv("HEALTH_CONDITION", employment3.HealthCondition));
                }
                #endregion

            }
            if (!(user.CurrentEmploymentStatus == "10" || user.CurrentEmploymentStatus == "11"))
            {
                UpdateUserEmploymentPersonalClearAdditionalFields(user.Id);
            }
        }

        public void UpdateUserEmploymentPersonalClearAdditionalFields(string userID)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql_UpdateEmp = "UPDATE EMPLOYMENT_ALL SET ";

            sql_UpdateEmp += " PROVIDE_STUDENT_SUPERVISION = '' ";
            sql_UpdateEmp += ", ANTICIPATE_STUDENT_SUPER = '' ";
            sql_UpdateEmp += ", NO_CLINICAL_CLIENTS = '' ";
            
            sql_UpdateEmp += ", NUMBER_WEEKS_PRACTICE_YR = 0";
            sql_UpdateEmp += ", AVG_HOURS_PRACTICE_YR = 0";
            
            sql_UpdateEmp += ", WEEKLY_HRS_DIRECT_PROFESSION = 0";
            sql_UpdateEmp += ", WEEKLY_HRS_TEACH = 0 ";
            sql_UpdateEmp += ", WEEKLY_HRS_CLINIC_EDUC = 0";
            sql_UpdateEmp += ", WEEKLY_HRS_RESEARCH = 0";
            sql_UpdateEmp += ", WEEKLY_HRS_ADMIN = 0";
            sql_UpdateEmp += ", WEEKLY_HRS_OTHER = 0";
            sql_UpdateEmp += " WHERE ID = " + Escape(userID);
            db.ExecuteCommand(sql_UpdateEmp);
        }

        public void UpdateUserEducation(User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            var education = user.Education;

            if (education != null)
            {
                int field_updates = 0;

                var entryDegree = user.Education.EntryDegree;
                var degree2 = user.Education.Degree2;
                var degree3 = user.Education.Degree3;
                var otherDegree1 = user.Education.OtherDegree1;
                var otherDegree2 = user.Education.OtherDegree2;

                string sql_UpdateEducation = "UPDATE EDUCATION_ALL SET ";

                if (!string.IsNullOrEmpty(degree2.OtherUniversityNotListed))
                {
                    sql_UpdateEducation += " UNIV_2_OTHER_OT_NL = " + Escape(degree2.OtherUniversityNotListed);
                    field_updates++;
                }

                if (!string.IsNullOrEmpty(degree3.OtherUniversityNotListed))
                {
                    sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIV_3_OTHER_OT_NL = " + Escape(degree3.OtherUniversityNotListed);
                    field_updates++;
                }



                if (entryDegree != null)
                {
                    if (!string.IsNullOrEmpty(entryDegree.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_ENTRY_OT = " + Escape(entryDegree.DiplomaName);
                        field_updates++;
                    }
                    if (!string.IsNullOrEmpty(entryDegree.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_ENTRY_OT = " + Escape(entryDegree.CanadianUniversity);
                        field_updates++;
                    }
                    if (!string.IsNullOrEmpty(entryDegree.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_ENTRY_OTHER_OT = " + Escape(entryDegree.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(entryDegree.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " PROVINCE_STATE_ENTRY_OT = " + Escape(entryDegree.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(entryDegree.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_ENTRY_OT = " + Escape(entryDegree.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(entryDegree.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_ENTRY_OT = " + Escape(entryDegree.GraduationYear);
                        field_updates++;
                    }
                }

                if (degree2 != null)
                {
                    if (!string.IsNullOrEmpty(degree2.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_2_OT = " + Escape(degree2.DiplomaName);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_2_OT = " + Escape(degree2.CanadianUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_2_OTHER_OT = " + Escape(degree2.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " PROVINCE_STATE_2_OT = " + Escape(degree2.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_2_OT = " + Escape(degree2.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_2_OT = " + Escape(degree2.GraduationYear);
                        field_updates++;
                    }
                }

                if (degree3 != null)
                {
                    if (!string.IsNullOrEmpty(degree3.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_3_OT = " + Escape(degree3.DiplomaName);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_3_OT = " + Escape(degree3.CanadianUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_3_OTHER_OT = " + Escape(degree3.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " PROVINCE_STATE_3_OT = " + Escape(degree3.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_3_OT = " + Escape(degree3.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_3_OT = " + Escape(degree3.GraduationYear);
                        field_updates++;
                    }
                }

                if (!string.IsNullOrEmpty(otherDegree1.OtherUniversityNotListed))
                {
                    sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIV_1_OTHER_INTL_NL = " + Escape(otherDegree1.OtherUniversityNotListed);
                    field_updates++;
                }

                if (!string.IsNullOrEmpty(otherDegree2.OtherUniversityNotListed))
                {
                    sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIV_2_OTHER_INTL_NL = " + Escape(otherDegree2.OtherUniversityNotListed);
                    field_updates++;
                }


                if (otherDegree1 != null)
                {
                    if (!string.IsNullOrEmpty(otherDegree1.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_1_OTHER = " + Escape(otherDegree1.DiplomaName);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_1_OTHER = " + Escape(otherDegree1.CanadianUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_1_INTL = " + Escape(otherDegree1.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.StudyField))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " FIELD_STUDY_1_OTHER = " + Escape(otherDegree1.StudyField);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " STATE_PROV_1_OTHER = " + Escape(otherDegree1.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_1_OTHER = " + Escape(otherDegree1.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_1_OTHER = " + Escape(otherDegree1.GraduationYear);
                        field_updates++;
                    }
                }

                if (otherDegree2 != null)
                {
                    if (!string.IsNullOrEmpty(otherDegree2.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_2_OTHER = " + Escape(otherDegree2.DiplomaName);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_2_OTHER = " + Escape(otherDegree2.CanadianUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_2_INTL = " + Escape(otherDegree2.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.StudyField))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " FIELD_STUDY_2_OTHER = " + Escape(otherDegree2.StudyField);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " STATE_PROV_2_OTHER = " + Escape(otherDegree2.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_2_OTHER = " + Escape(otherDegree2.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_2_OTHER = " + Escape(otherDegree2.GraduationYear);
                        field_updates++;
                    }
                }

                sql_UpdateEducation += " WHERE ID = " + Escape(user.Id);
                if (field_updates > 0)
                {
                    db.ExecuteCommand(sql_UpdateEducation);
                }

            }
        }

        public void UpdateUserEducationLogged(string UserID, User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string prevEntryDiploma = string.Empty;
            string prevEntryCanUniv = string.Empty;
            string prevEntryOthUniv = string.Empty;
            string prevEntryOthUnivNotListed = string.Empty;
            string prevEntryProvince = string.Empty;
            string prevEntryCountry = string.Empty;
            string prevEntryYear = string.Empty;

            string prevDegree2Diploma = string.Empty;
            string prevDegree2CanUniv = string.Empty;
            string prevDegree2OthUniv = string.Empty;
            string prevDegree2OthUnivNotListed = string.Empty;
            string prevDegree2Province = string.Empty;
            string prevDegree2Country = string.Empty;
            string prevDegree2Year = string.Empty;

            string prevDegree3Diploma = string.Empty;
            string prevDegree3CanUniv = string.Empty;
            string prevDegree3OthUniv = string.Empty;
            string prevDegree3OthUnivNotListed = string.Empty;
            string prevDegree3Province = string.Empty;
            string prevDegree3Country = string.Empty;
            string prevDegree3Year = string.Empty;

            string prevOtherDegree1Diploma = string.Empty;
            string prevOtherDegree1CanUniv = string.Empty;
            string prevOtherDegree1OthUniv = string.Empty;
            string prevOtherDegree1OthUnivNotListed = string.Empty;
            string prevOtherDegree1Province = string.Empty;
            string prevOtherDegree1Country = string.Empty;
            string prevOtherDegree1Year = string.Empty;
            string prevOtherDegree1FieldStudy = string.Empty;

            string prevOtherDegree2Diploma = string.Empty;
            string prevOtherDegree2CanUniv = string.Empty;
            string prevOtherDegree2OthUniv = string.Empty;
            string prevOtherDegree2OthUnivNotListed = string.Empty;
            string prevOtherDegree2Province = string.Empty;
            string prevOtherDegree2Country = string.Empty;
            string prevOtherDegree2Year = string.Empty;
            string prevOtherDegree2FieldStudy = string.Empty;

            string sql_GetEducation = " SELECT * FROM EDUCATION_ALL WHERE  ID = " + Escape(user.Id);
            //db.ExecuteCommand(sql_GetHomeAddress);

            var table = db.GetData(sql_GetEducation);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                prevEntryDiploma = (string)row["DEGREE_ENTRY_OT"];
                prevEntryCanUniv = (string)row["UNIVERSITY_ENTRY_OT"];
                prevEntryOthUniv = (string)row["UNIVERSITY_ENTRY_OTHER_OT"];
                //prevEntryOthUnivNotListed = string.Empty;
                prevEntryProvince = (string)row["PROVINCE_STATE_ENTRY_OT"];
                prevEntryCountry = (string)row["COUNTRY_ENTRY_OT"];
                prevEntryYear = (string)row["GRAD_YEAR_ENTRY_OT"];

                prevDegree2Diploma = (string)row["DEGREE_2_OT"];
                prevDegree2CanUniv = (string)row["UNIVERSITY_2_OT"];
                prevDegree2OthUniv = (string)row["UNIVERSITY_2_OTHER_OT"];
                prevDegree2OthUnivNotListed = (string)row["UNIV_2_OTHER_OT_NL"];
                prevDegree2Province = (string)row["PROVINCE_STATE_2_OT"];
                prevDegree2Country = (string)row["COUNTRY_2_OT"];
                prevDegree2Year = (string)row["GRAD_YEAR_2_OT"];

                prevDegree3Diploma = (string)row["DEGREE_3_OT"];
                prevDegree3CanUniv = (string)row["UNIVERSITY_3_OT"];
                prevDegree3OthUniv = (string)row["UNIVERSITY_3_OTHER_OT"];
                prevDegree3OthUnivNotListed = (string)row["UNIV_3_OTHER_OT_NL"];
                prevDegree3Province = (string)row["PROVINCE_STATE_3_OT"];
                prevDegree3Country = (string)row["COUNTRY_3_OT"];
                prevDegree3Year = (string)row["GRAD_YEAR_3_OT"];

                prevOtherDegree1Diploma = (string)row["DEGREE_1_OTHER"];
                prevOtherDegree1CanUniv = (string)row["UNIVERSITY_1_OTHER"];
                prevOtherDegree1OthUniv = (string)row["UNIVERSITY_1_INTL"];
                prevOtherDegree1OthUnivNotListed = (string)row["UNIV_1_OTHER_INTL_NL"];
                prevOtherDegree1FieldStudy = (string)row["FIELD_STUDY_1_OTHER"];
                prevOtherDegree1Province = (string)row["STATE_PROV_1_OTHER"];
                prevOtherDegree1Country = (string)row["COUNTRY_1_OTHER"];
                prevOtherDegree1Year = (string)row["GRAD_YEAR_1_OTHER"];

                prevOtherDegree2Diploma = (string)row["DEGREE_2_OTHER"];
                prevOtherDegree2CanUniv = (string)row["UNIVERSITY_2_OTHER"];
                prevOtherDegree2OthUniv = (string)row["UNIVERSITY_2_INTL"];
                prevOtherDegree2OthUnivNotListed = (string)row["UNIV_2_OTHER_INTL_NL"];
                prevOtherDegree2FieldStudy = (string)row["FIELD_STUDY_2_OTHER"];
                prevOtherDegree2Province = (string)row["STATE_PROV_2_OTHER"];
                prevOtherDegree2Country = (string)row["COUNTRY_2_OTHER"];
                prevOtherDegree2Year = (string)row["GRAD_YEAR_2_OTHER"];

            }

            var education = user.Education;

            if (education != null)
            {
                int field_updates = 0;

                var entryDegree = user.Education.EntryDegree;
                var degree2 = user.Education.Degree2;
                var degree3 = user.Education.Degree3;
                var otherDegree1 = user.Education.OtherDegree1;
                var otherDegree2 = user.Education.OtherDegree2;

                string sql_UpdateEducation = "UPDATE EDUCATION_ALL SET ";

                if (!string.IsNullOrEmpty(degree2.OtherUniversityNotListed))
                {
                    sql_UpdateEducation += " UNIV_2_OTHER_OT_NL = " + Escape(degree2.OtherUniversityNotListed);
                    field_updates++;
                }

                if (!string.IsNullOrEmpty(degree3.OtherUniversityNotListed))
                {
                    sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIV_3_OTHER_OT_NL = " + Escape(degree3.OtherUniversityNotListed);
                    field_updates++;
                }

                if (entryDegree != null)
                {
                    if (!string.IsNullOrEmpty(entryDegree.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_ENTRY_OT = " + Escape(entryDegree.DiplomaName);
                        field_updates++;
                    }
                    if (!string.IsNullOrEmpty(entryDegree.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_ENTRY_OT = " + Escape(entryDegree.CanadianUniversity);
                        field_updates++;
                    }
                    if (!string.IsNullOrEmpty(entryDegree.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_ENTRY_OTHER_OT = " + Escape(entryDegree.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(entryDegree.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " PROVINCE_STATE_ENTRY_OT = " + Escape(entryDegree.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(entryDegree.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_ENTRY_OT = " + Escape(entryDegree.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(entryDegree.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_ENTRY_OT = " + Escape(entryDegree.GraduationYear);
                        field_updates++;
                    }
                }

                if (degree2 != null)
                {
                    if (!string.IsNullOrEmpty(degree2.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_2_OT = " + Escape(degree2.DiplomaName);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_2_OT = " + Escape(degree2.CanadianUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_2_OTHER_OT = " + Escape(degree2.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " PROVINCE_STATE_2_OT = " + Escape(degree2.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_2_OT = " + Escape(degree2.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_2_OT = " + Escape(degree2.GraduationYear);
                        field_updates++;
                    }
                }

                if (degree3 != null)
                {
                    if (!string.IsNullOrEmpty(degree3.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_3_OT = " + Escape(degree3.DiplomaName);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_3_OT = " + Escape(degree3.CanadianUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_3_OTHER_OT = " + Escape(degree3.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " PROVINCE_STATE_3_OT = " + Escape(degree3.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_3_OT = " + Escape(degree3.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_3_OT = " + Escape(degree3.GraduationYear);
                        field_updates++;
                    }
                }

                if (!string.IsNullOrEmpty(otherDegree1.OtherUniversityNotListed))
                {
                    sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIV_1_OTHER_INTL_NL = " + Escape(otherDegree1.OtherUniversityNotListed);
                    field_updates++;
                }

                if (!string.IsNullOrEmpty(otherDegree2.OtherUniversityNotListed))
                {
                    sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIV_2_OTHER_INTL_NL = " + Escape(otherDegree2.OtherUniversityNotListed);
                    field_updates++;
                }


                if (otherDegree1 != null)
                {
                    if (!string.IsNullOrEmpty(otherDegree1.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_1_OTHER = " + Escape(otherDegree1.DiplomaName);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_1_OTHER = " + Escape(otherDegree1.CanadianUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_1_INTL = " + Escape(otherDegree1.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.StudyField))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " FIELD_STUDY_1_OTHER = " + Escape(otherDegree1.StudyField);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " STATE_PROV_1_OTHER = " + Escape(otherDegree1.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_1_OTHER = " + Escape(otherDegree1.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_1_OTHER = " + Escape(otherDegree1.GraduationYear);
                        field_updates++;
                    }
                }

                if (otherDegree2 != null)
                {
                    if (!string.IsNullOrEmpty(otherDegree2.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_2_OTHER = " + Escape(otherDegree2.DiplomaName);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_2_OTHER = " + Escape(otherDegree2.CanadianUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_2_INTL = " + Escape(otherDegree2.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.StudyField))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " FIELD_STUDY_2_OTHER = " + Escape(otherDegree2.StudyField);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " STATE_PROV_2_OTHER = " + Escape(otherDegree2.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_2_OTHER = " + Escape(otherDegree2.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_2_OTHER = " + Escape(otherDegree2.GraduationYear);
                        field_updates++;
                    }
                }

                sql_UpdateEducation += " WHERE ID = " + Escape(user.Id);
                if (field_updates > 0)
                {
                    db.ExecuteCommand(sql_UpdateEducation);
                }
                // add changes to name_log table

                if (!string.IsNullOrEmpty(entryDegree.DiplomaName) && prevEntryDiploma != entryDegree.DiplomaName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.DEGREE_ENTRY_OT: " + GetGeneralNameImprv("DEGREE_OT_ENTRY", prevEntryDiploma) + " -> " + GetGeneralNameImprv("DEGREE_OT_ENTRY", entryDegree.DiplomaName));
                }

                if (!string.IsNullOrEmpty(entryDegree.CanadianUniversity) && prevEntryCanUniv != entryDegree.CanadianUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_ENTRY_OT: " + GetGeneralNameImprv("UNIV_OT", prevEntryCanUniv) + " -> " + GetGeneralNameImprv("UNIV_OT", entryDegree.CanadianUniversity));
                }

                if (!string.IsNullOrEmpty(entryDegree.OtherUniversity) && prevEntryOthUniv != entryDegree.OtherUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_ENTRY_OTHER_OT: " + GetGeneralNameImprv("UNIV_INTL", prevEntryOthUniv) + " -> " + GetGeneralNameImprv("UNIV_INTL", entryDegree.OtherUniversity));
                }

                if (!string.IsNullOrEmpty(entryDegree.ProvinceState) && prevEntryProvince != entryDegree.ProvinceState)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.PROVINCE_STATE_ENTRY_OT: " + prevEntryProvince + " -> " + entryDegree.ProvinceState);
                }

                if (!string.IsNullOrEmpty(entryDegree.Country) && prevEntryCountry != entryDegree.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.COUNTRY_ENTRY_OT: " + prevEntryCountry + " -> " + entryDegree.Country);
                }

                if (!string.IsNullOrEmpty(entryDegree.GraduationYear) && prevEntryYear != entryDegree.GraduationYear)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.GRAD_YEAR_ENTRY_OT: " + prevEntryYear + " -> " + entryDegree.GraduationYear);
                }

                if (!string.IsNullOrEmpty(degree2.DiplomaName) && prevDegree2Diploma != degree2.DiplomaName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.DEGREE_2_OT: " + GetGeneralNameImprv("DEGREE_OT_POST_ENTRY", prevDegree2Diploma) + " -> " + GetGeneralNameImprv("DEGREE_OT_POST_ENTRY", degree2.DiplomaName));
                }

                if (!string.IsNullOrEmpty(degree2.CanadianUniversity) && prevDegree2CanUniv != degree2.CanadianUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_2_OT: " + GetGeneralNameImprv("UNIV_OT", prevDegree2CanUniv) + " -> " + GetGeneralNameImprv("UNIV_OT", degree2.CanadianUniversity));
                }

                if (!string.IsNullOrEmpty(degree2.OtherUniversity) && prevDegree2OthUniv != degree2.OtherUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_2_OTHER_OT: " + GetGeneralNameImprv("UNIV_INTL", prevDegree2OthUniv) + " -> " + GetGeneralNameImprv("UNIV_INTL", degree2.OtherUniversity));
                }

                if (!string.IsNullOrEmpty(degree2.OtherUniversityNotListed) && prevDegree2OthUnivNotListed != degree2.OtherUniversityNotListed)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIV_2_OTHER_OT_NL: " + prevDegree2OthUnivNotListed + " -> " + degree2.OtherUniversityNotListed);
                }

                if (!string.IsNullOrEmpty(degree2.ProvinceState) && prevDegree2Province != degree2.ProvinceState)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.PROVINCE_STATE_2_OT: " + prevDegree2Province + " -> " + degree2.ProvinceState);
                }

                if (!string.IsNullOrEmpty(degree2.Country) && prevDegree2Country != degree2.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.COUNTRY_2_OT: " + prevDegree2Country + " -> " + degree2.Country);
                }

                if (!string.IsNullOrEmpty(degree2.GraduationYear) && prevDegree2Year != degree2.GraduationYear)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.GRAD_YEAR_2_OT: " + prevDegree2Year + " -> " + degree2.GraduationYear);
                }

                if (!string.IsNullOrEmpty(degree3.DiplomaName) && prevDegree3Diploma != degree3.DiplomaName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.DEGREE_3_OT: " + GetGeneralNameImprv("DEGREE_OT_POST_ENTRY", prevDegree3Diploma) + " -> " + GetGeneralNameImprv("DEGREE_OT_POST_ENTRY", degree3.DiplomaName));
                }

                if (!string.IsNullOrEmpty(degree3.CanadianUniversity) && prevDegree3CanUniv != degree3.CanadianUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_3_OT: " + GetGeneralNameImprv("UNIV_OT", prevDegree3CanUniv) + " -> " + GetGeneralNameImprv("UNIV_OT", degree3.CanadianUniversity));
                }

                if (!string.IsNullOrEmpty(degree3.OtherUniversity) && prevDegree3OthUniv != degree3.OtherUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_3_OTHER_OT: " + GetGeneralNameImprv("UNIV_INTL", prevDegree3OthUniv) + " -> " + GetGeneralNameImprv("UNIV_INTL", degree3.OtherUniversity));
                }

                if (!string.IsNullOrEmpty(degree3.OtherUniversityNotListed) && prevDegree3OthUnivNotListed != degree3.OtherUniversityNotListed)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIV_3_OTHER_OT_NL: " + prevDegree3OthUnivNotListed + " -> " + degree3.OtherUniversityNotListed);
                }

                if (!string.IsNullOrEmpty(degree3.ProvinceState) && prevDegree3Province != degree3.ProvinceState)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.PROVINCE_STATE_3_OT: " + prevDegree3Province + " -> " + degree3.ProvinceState);
                }

                if (!string.IsNullOrEmpty(degree3.Country) && prevDegree3Country != degree3.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.COUNTRY_3_OT: " + prevDegree3Country + " -> " + degree3.Country);
                }

                if (!string.IsNullOrEmpty(degree3.GraduationYear) && prevDegree3Year != degree3.GraduationYear)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.GRAD_YEAR_3_OT: " + prevDegree3Year + " -> " + degree3.GraduationYear);
                }

                if (!string.IsNullOrEmpty(otherDegree1.DiplomaName) && prevOtherDegree1Diploma != otherDegree1.DiplomaName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.DEGREE_1_OTHER: " + GetGeneralNameImprv("DEGREE_OTHER", prevOtherDegree1Diploma) + " -> " + GetGeneralNameImprv("DEGREE_OTHER", otherDegree1.DiplomaName));
                }

                if (!string.IsNullOrEmpty(otherDegree1.CanadianUniversity) && prevOtherDegree1CanUniv != otherDegree1.CanadianUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_1_OTHER: " + prevOtherDegree1CanUniv + " -> " + otherDegree1.CanadianUniversity);
                }

                if (!string.IsNullOrEmpty(otherDegree1.OtherUniversity) && prevOtherDegree1OthUniv != otherDegree1.OtherUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_1_INTL: " + GetGeneralNameImprv("UNIV_INTL", prevOtherDegree1OthUniv) + " -> " + GetGeneralNameImprv("UNIV_INTL", otherDegree1.OtherUniversity));
                }

                if (!string.IsNullOrEmpty(otherDegree1.OtherUniversityNotListed) && prevOtherDegree1OthUnivNotListed != otherDegree1.OtherUniversityNotListed)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIV_1_OTHER_INTL_NL: " + prevOtherDegree1OthUnivNotListed + " -> " + otherDegree1.OtherUniversityNotListed);
                }

                if (!string.IsNullOrEmpty(otherDegree1.StudyField) && prevOtherDegree1FieldStudy != otherDegree1.StudyField)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.FIELD_STUDY_1_OTHER: " + prevOtherDegree1FieldStudy + " -> " + otherDegree1.StudyField);
                }

                if (!string.IsNullOrEmpty(otherDegree1.ProvinceState) && prevOtherDegree1Province != otherDegree1.ProvinceState)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.STATE_PROV_1_OTHER: " + prevOtherDegree1Province + " -> " + otherDegree1.ProvinceState);
                }

                if (!string.IsNullOrEmpty(otherDegree1.Country) && prevOtherDegree1Country != otherDegree1.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.COUNTRY_1_OTHER: " + prevOtherDegree1Country + " -> " + otherDegree1.Country);
                }

                if (!string.IsNullOrEmpty(otherDegree1.GraduationYear) && prevOtherDegree1Year != otherDegree1.GraduationYear)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.GRAD_YEAR_1_OTHER: " + prevOtherDegree1Year + " -> " + otherDegree1.GraduationYear);
                }

                if (!string.IsNullOrEmpty(otherDegree2.DiplomaName) && prevOtherDegree2Diploma != otherDegree2.DiplomaName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.DEGREE_2_OTHER: " + GetGeneralNameImprv("DEGREE_OTHER", prevOtherDegree2Diploma) + " -> " + GetGeneralNameImprv("DEGREE_OTHER", otherDegree2.DiplomaName));
                }

                if (!string.IsNullOrEmpty(otherDegree2.CanadianUniversity) && prevOtherDegree2CanUniv != otherDegree2.CanadianUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_2_OTHER: " + prevOtherDegree2CanUniv + " -> " + otherDegree2.CanadianUniversity);
                }

                if (!string.IsNullOrEmpty(otherDegree2.OtherUniversity) && prevOtherDegree2OthUniv != otherDegree2.OtherUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_2_INTL: " + GetGeneralNameImprv("UNIV_INTL", prevOtherDegree2OthUniv) + " -> " + GetGeneralNameImprv("UNIV_INTL", otherDegree2.OtherUniversity));
                }

                if (!string.IsNullOrEmpty(otherDegree2.OtherUniversityNotListed) && prevOtherDegree2OthUnivNotListed != otherDegree2.OtherUniversityNotListed)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIV_2_OTHER_INTL_NL: " + prevOtherDegree2OthUnivNotListed + " -> " + otherDegree2.OtherUniversityNotListed);
                }

                if (!string.IsNullOrEmpty(otherDegree2.StudyField) && prevOtherDegree2FieldStudy != otherDegree2.StudyField)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.FIELD_STUDY_2_OTHER: " + prevOtherDegree2FieldStudy + " -> " + otherDegree2.StudyField);
                }

                if (!string.IsNullOrEmpty(otherDegree2.ProvinceState) && prevOtherDegree2Province != otherDegree2.ProvinceState)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.STATE_PROV_2_OTHER: " + prevOtherDegree2Province + " -> " + otherDegree2.ProvinceState);
                }

                if (!string.IsNullOrEmpty(otherDegree2.Country) && prevOtherDegree2Country != otherDegree2.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.COUNTRY_2_OTHER: " + prevOtherDegree2Country + " -> " + otherDegree2.Country);
                }

                if (!string.IsNullOrEmpty(otherDegree2.GraduationYear) && prevOtherDegree2Year != otherDegree2.GraduationYear)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.GRAD_YEAR_2_OTHER: " + prevOtherDegree2Year + " -> " + otherDegree2.GraduationYear);
                }
            }
        }

        public void UpdateUserEmploymentProfile(User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            var profile = user.EmploymentProfile;

            if (profile != null)
            {
                string sql_UpdateProfile = "UPDATE EMPLOYMENT_ALL SET ";
                sql_UpdateProfile += " CURRENT_EMPLOYMENT_STATUS = " + Escape(profile.PracticeStatus);
                //sql_UpdateProfile += ", PRIMARY_EMPL_PREFERRED = " + Escape(profile.CollegeMailings);
                sql_UpdateProfile += ", FULL_PT_STATUS_PREF = " + Escape(profile.CurrentWorkPreference);
                sql_UpdateProfile += ", PROVIDE_STUDENT_SUPERVISION = " + Escape(profile.StudentSupervisionLastYear);
                sql_UpdateProfile += ", ANTICIPATE_STUDENT_SUPER = " + Escape(profile.StudentSupervisionComingYear);
                sql_UpdateProfile += ", NO_CLINICAL_CLIENTS = " + Escape(profile.HaveClinicalClients);
                sql_UpdateProfile += ", NUMBER_WEEKS_PRACTICE_YR = " + profile.PracticingNumberWeeks.ToString();
                sql_UpdateProfile += ", AVG_HOURS_PRACTICE_YR = " + profile.PracticingNumberHoursPerWeek.ToString();
                sql_UpdateProfile += ", NATURE_OF_PRACTICE = " + Escape(profile.NaturePractice);
                sql_UpdateProfile += ", WEEKLY_HRS_DIRECT_PROFESSION = " + profile.TimeSpentProfServices.ToString();
                sql_UpdateProfile += ", WEEKLY_HRS_TEACH = " + profile.TimeSpentTeaching.ToString();
                sql_UpdateProfile += ", WEEKLY_HRS_CLINIC_EDUC = " + profile.TimeSpentClinicalEducation.ToString();
                sql_UpdateProfile += ", WEEKLY_HRS_RESEARCH = " + profile.TimeSpentResearch.ToString();
                sql_UpdateProfile += ", WEEKLY_HRS_ADMIN = " + profile.TimeSpentAdministration.ToString();
                sql_UpdateProfile += ", WEEKLY_HRS_OTHER = " + profile.TimeSpentOtherActivities.ToString();

                sql_UpdateProfile += " WHERE ID = " + Escape(user.Id);

                db.ExecuteCommand(sql_UpdateProfile);
            }
        }

        public void UpdateUserLogin(string UserID, DateTime ExpirationDate)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql_Update = "UPDATE Name_Security SET ";
            sql_Update += " ExpirationDate = " + Escape(ExpirationDate.ToString("yyyy-MM-dd"));
            if (ExpirationDate < DateTime.Now)
            {
                sql_Update += ", Login_disabled = 1 ";
            }
            sql_Update += " WHERE ID = " + Escape(UserID);

            db.ExecuteCommand(sql_Update);
        }

        public void UpdateUserEmploymentProfileLogged(string UserID, User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string prevPracticeStatus = string.Empty;
            string prevWrokPreference = string.Empty;
            string prevStudentSupervisionLastYear = string.Empty;
            string prevStudentSupervisionComingYear = string.Empty;
            string prevNaturePractice = string.Empty;
            string prevHaveClient = string.Empty;

            int prevNumberWeeks = 0;
            int prevNumberHours = 0;

            int prevTimeDirectProf = 0;
            int prevTimeTeaching = 0;
            int prevTimeClinicEduc = 0;
            int prevTimeResearch = 0;
            int prevTimeAdministr = 0;
            int prevTimeOtherActiv = 0;

            string sql_GetProfile = " SELECT * FROM EMPLOYMENT_ALL WHERE  ID = " + Escape(UserID);
            //db.ExecuteCommand(sql_GetHomeAddress);

            var table = db.GetData(sql_GetProfile);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                prevPracticeStatus = (string)row["CURRENT_EMPLOYMENT_STATUS"];
                prevWrokPreference = (string)row["FULL_PT_STATUS_PREF"];
                prevStudentSupervisionLastYear = (string)row["PROVIDE_STUDENT_SUPERVISION"];
                prevStudentSupervisionComingYear = (string)row["ANTICIPATE_STUDENT_SUPER"];
                prevNaturePractice = (string)row["NATURE_OF_PRACTICE"];
                prevHaveClient = (string)row["NO_CLINICAL_CLIENTS"];

                prevNumberWeeks = (int)row["NUMBER_WEEKS_PRACTICE_YR"];
                prevNumberHours = (int)row["AVG_HOURS_PRACTICE_YR"];

                prevTimeDirectProf = (int)row["WEEKLY_HRS_DIRECT_PROFESSION"];
                prevTimeTeaching = (int)row["WEEKLY_HRS_TEACH"];
                prevTimeClinicEduc = (int)row["WEEKLY_HRS_CLINIC_EDUC"];
                prevTimeResearch = (int)row["WEEKLY_HRS_RESEARCH"];
                prevTimeAdministr = (int)row["WEEKLY_HRS_ADMIN"];
                prevTimeOtherActiv = (int)row["WEEKLY_HRS_OTHER"];
            }

            var profile = user.EmploymentProfile;

            if (profile != null)
            {
                string sql_UpdateProfile = "UPDATE EMPLOYMENT_ALL SET ";
                sql_UpdateProfile += " CURRENT_EMPLOYMENT_STATUS = " + Escape(profile.PracticeStatus);
                //sql_UpdateProfile += ", PRIMARY_EMPL_PREFERRED = " + Escape(profile.CollegeMailings);
                sql_UpdateProfile += ", FULL_PT_STATUS_PREF = " + Escape(profile.CurrentWorkPreference);
                sql_UpdateProfile += ", PROVIDE_STUDENT_SUPERVISION = " + Escape(profile.StudentSupervisionLastYear);
                sql_UpdateProfile += ", ANTICIPATE_STUDENT_SUPER = " + Escape(profile.StudentSupervisionComingYear);
                sql_UpdateProfile += ", NO_CLINICAL_CLIENTS = " + Escape(profile.HaveClinicalClients);
                sql_UpdateProfile += ", NUMBER_WEEKS_PRACTICE_YR = " + profile.PracticingNumberWeeks.ToString();
                sql_UpdateProfile += ", AVG_HOURS_PRACTICE_YR = " + profile.PracticingNumberHoursPerWeek.ToString();
                sql_UpdateProfile += ", NATURE_OF_PRACTICE = " + Escape(profile.NaturePractice);
                sql_UpdateProfile += ", WEEKLY_HRS_DIRECT_PROFESSION = " + profile.TimeSpentProfServices.ToString();
                sql_UpdateProfile += ", WEEKLY_HRS_TEACH = " + profile.TimeSpentTeaching.ToString();
                sql_UpdateProfile += ", WEEKLY_HRS_CLINIC_EDUC = " + profile.TimeSpentClinicalEducation.ToString();
                sql_UpdateProfile += ", WEEKLY_HRS_RESEARCH = " + profile.TimeSpentResearch.ToString();
                sql_UpdateProfile += ", WEEKLY_HRS_ADMIN = " + profile.TimeSpentAdministration.ToString();
                sql_UpdateProfile += ", WEEKLY_HRS_OTHER = " + profile.TimeSpentOtherActivities.ToString();

                sql_UpdateProfile += " WHERE ID = " + Escape(user.Id);

                db.ExecuteCommand(sql_UpdateProfile);

                if (prevPracticeStatus != profile.PracticeStatus)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.CURRENT_EMPLOYMENT_STATUS: " + GetGeneralNameImprv("EMPLOYMENT_STATUS", prevPracticeStatus) + " -> " + GetGeneralNameImprv("EMPLOYMENT_STATUS", profile.PracticeStatus));
                }

                if (prevWrokPreference != profile.CurrentWorkPreference)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.FULL_PT_STATUS_PREF: " + GetGeneralNameImprv("FULL_PT_STATUS", prevWrokPreference) + " -> " + GetGeneralNameImprv("FULL_PT_STATUS", profile.CurrentWorkPreference));
                }

                if (prevStudentSupervisionLastYear != profile.StudentSupervisionLastYear)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.PROVIDE_STUDENT_SUPERVISION: " + prevStudentSupervisionLastYear + " -> " + profile.StudentSupervisionLastYear);
                }

                if (prevStudentSupervisionComingYear != profile.StudentSupervisionComingYear)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.ANTICIPATE_STUDENT_SUPER: " + prevStudentSupervisionComingYear + " -> " + profile.StudentSupervisionComingYear);
                }

                if (prevNaturePractice != profile.NaturePractice)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.NATURE_OF_PRACTICE: " + GetGeneralNameImprv("NATURE_PRACTICE", prevNaturePractice) + " -> " + GetGeneralNameImprv("NATURE_PRACTICE", profile.NaturePractice));
                }

                if (prevHaveClient != profile.HaveClinicalClients)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.NO_CLINICAL_CLIENTS: " + prevHaveClient + " -> " + profile.HaveClinicalClients);
                }

                if (prevNumberWeeks != profile.PracticingNumberWeeks)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.NUMBER_WEEKS_PRACTICE_YR: " + prevNumberWeeks.ToString() + " -> " + profile.PracticingNumberWeeks.ToString());
                }

                if (prevNumberHours != profile.PracticingNumberHoursPerWeek)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.AVG_HOURS_PRACTICE_YR: " + prevNumberHours.ToString() + " -> " + profile.PracticingNumberHoursPerWeek.ToString());
                }

                if (prevTimeDirectProf != profile.TimeSpentProfServices)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.WEEKLY_HRS_DIRECT_PROFESSION: " + prevTimeDirectProf.ToString() + " -> " + profile.TimeSpentProfServices.ToString());
                }

                if (prevTimeTeaching != profile.TimeSpentTeaching)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.WEEKLY_HRS_TEACH: " + prevTimeTeaching.ToString() + " -> " + profile.TimeSpentTeaching.ToString());
                }

                if (prevTimeClinicEduc != profile.TimeSpentClinicalEducation)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.WEEKLY_HRS_CLINIC_EDUC: " + prevTimeClinicEduc.ToString() + " -> " + profile.TimeSpentClinicalEducation.ToString());
                }

                if (prevTimeResearch != profile.TimeSpentResearch)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.WEEKLY_HRS_RESEARCH: " + prevTimeResearch.ToString() + " -> " + profile.TimeSpentResearch.ToString());
                }

                if (prevTimeAdministr != profile.TimeSpentAdministration)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.WEEKLY_HRS_ADMIN: " + prevTimeAdministr.ToString() + " -> " + profile.TimeSpentAdministration.ToString());
                }

                if (prevTimeOtherActiv != profile.TimeSpentOtherActivities)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.WEEKLY_HRS_OTHER: " + prevTimeOtherActiv.ToString() + " -> " + profile.TimeSpentOtherActivities.ToString());
                }
            }
        }

        // not using anymore
        public void UpdateUserPracticeHistoryOld(User user)
        {

            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            var profile = user.PracticeHistory;

            if (profile != null)
            {
                string sql_UpdateProfile = "UPDATE COTO_REGDATA SET ";
                sql_UpdateProfile += " Country_First_Practice_Prof = " + Escape(profile.FirstCountryPractice);
                sql_UpdateProfile += ", Prov_State_First_Practice = " + Escape(profile.FirstProvinceStatePractice);
                sql_UpdateProfile += ", First_Cdn_Prov_Practice = " + Escape(profile.FirstProvincePractice);
                sql_UpdateProfile += ", First_Year_CDN_Pract_Prof = " + Escape(profile.FirstYearCanadianPractice);
                sql_UpdateProfile += ", First_Year_Practice_Prof = " + Escape(profile.FirstYearPractice);
                sql_UpdateProfile += ", Most_Recent_Prev_Country = " + Escape(profile.LastCountryPractice);
                sql_UpdateProfile += ", PREVIOUS_PROV = " + Escape(profile.LastProvincePractice);
                sql_UpdateProfile += ", Most_Recent_Prev_Prov = " + Escape(profile.LastProvinceStatePractice);
                sql_UpdateProfile += ", MOST_RECENT_PREV_YEAR = " + Escape(profile.LastYearOutsideOntarioPractice);
                sql_UpdateProfile += " WHERE ID = " + Escape(user.Id);
                db.ExecuteCommand(sql_UpdateProfile);
            }
        }

        public void UpdateUserPracticeHistory(User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            var profile = user.PracticeHistory;

            try
            {
                if (profile != null)
                {
                    string sql_UpdateProfile = "UPDATE COTO_REGDATA SET ";
                    sql_UpdateProfile += " Country_First_Practice_Prof = " + Escape(profile.FirstCountryPractice);
                    sql_UpdateProfile += ", Prov_State_First_Practice = " + Escape(profile.FirstProvinceStatePractice);
                    sql_UpdateProfile += ", First_Cdn_Prov_Practice = " + Escape(profile.FirstProvincePractice);
                    sql_UpdateProfile += ", First_Year_CDN_Pract_Prof = " + Escape(profile.FirstYearCanadianPractice);
                    sql_UpdateProfile += ", First_Year_Practice_Prof = " + Escape(profile.FirstYearPractice);
                    sql_UpdateProfile += ", Most_Recent_Prev_Country = " + Escape(profile.LastCountryPractice);
                    //sql_UpdateProfile += ", PREVIOUS_PROV = " + Escape(profile.LastProvincePractice);
                    sql_UpdateProfile += ", Most_Recent_Prev_Prov = " + Escape(profile.LastProvinceStatePractice);
                    sql_UpdateProfile += ", MOST_RECENT_PREV_YEAR = " + Escape(profile.LastYearOutsideOntarioPractice);
                    sql_UpdateProfile += " WHERE ID = " + Escape(user.Id);

                    db.ExecuteCommand(sql_UpdateProfile);
                }
            }
            catch
            {
                //Message = "Exception.Message = " + ex.Message + ", Exception.InnerException = " + ex.InnerException;
            }
        }

        public void UpdateUserPracticeHistoryLogged(string UserID, User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql_GetProfile = " SELECT * FROM COTO_REGDATA WHERE  ID = " + Escape(UserID);
            //db.ExecuteCommand(sql_GetHomeAddress);

            string prevRecentCountry = string.Empty;
            string prevRecentProvince = string.Empty;
            string prevRecentYear = string.Empty;

            var table = db.GetData(sql_GetProfile);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                prevRecentCountry = (string)row["Most_Recent_Prev_Country"];
                prevRecentProvince = (string)row["Most_Recent_Prev_Prov"];
                prevRecentYear = (string)row["MOST_RECENT_PREV_YEAR"];
            }

            var profile = user.PracticeHistory;
            try
            {
                if (profile != null)
                {
                    string sql_UpdateProfile = "UPDATE COTO_REGDATA SET ";
                    //sql_UpdateProfile += " Country_First_Practice_Prof = " + Escape(profile.FirstCountryPractice);
                    //sql_UpdateProfile += ", Prov_State_First_Practice = " + Escape(profile.FirstProvinceStatePractice);
                    //sql_UpdateProfile += ", First_Cdn_Prov_Practice = " + Escape(profile.FirstProvincePractice);
                    //sql_UpdateProfile += ", First_Year_CDN_Pract_Prof = " + Escape(profile.FirstYearCanadianPractice);
                    //sql_UpdateProfile += ", First_Year_Practice_Prof = " + Escape(profile.FirstYearPractice);
                    sql_UpdateProfile += " Most_Recent_Prev_Country = " + Escape(profile.LastCountryPractice);
                    //sql_UpdateProfile += ", PREVIOUS_PROV = " + Escape(profile.LastProvincePractice);
                    sql_UpdateProfile += ", Most_Recent_Prev_Prov = " + Escape(profile.LastProvinceStatePractice);
                    sql_UpdateProfile += ", MOST_RECENT_PREV_YEAR = " + Escape(profile.LastYearOutsideOntarioPractice);
                    sql_UpdateProfile += " WHERE ID = " + Escape(user.Id);

                    db.ExecuteCommand(sql_UpdateProfile);

                    if (prevRecentCountry != profile.LastCountryPractice)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.Most_Recent_Prev_Country: " + prevRecentCountry + " -> " + profile.LastCountryPractice);
                    }
                    if (prevRecentProvince != profile.LastProvinceStatePractice)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.Most_Recent_Prev_Prov: " + prevRecentProvince + " -> " + profile.LastProvinceStatePractice);
                    }
                    if (prevRecentYear != profile.LastYearOutsideOntarioPractice)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.MOST_RECENT_PREV_YEAR: " + prevRecentYear + " -> " + profile.LastYearOutsideOntarioPractice);
                    }
                }
            }
            catch
            {
                //Message = "Exception.Message = " + ex.Message + ", Exception.InnerException = " + ex.InnerException;
            }
        }

        public User GetUserCurrencyLanguage(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM COTO_REGDATA " +
                " WHERE ID = '" + User_Id + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.CurrencyLanguageServices = new COTO_RegOnly.Classes.CurrencyLanguage();
                //returnValue.CurrencyLanguageServices.Currency = (string)row["CURRENCY_DATA"];
                returnValue.CurrencyLanguageServices.Lang_Service1 = (string)row["LANG_OF_SVCE_1"];
                returnValue.CurrencyLanguageServices.Lang_Service2 = (string)row["LANG_OF_SVCE_2"];
                returnValue.CurrencyLanguageServices.Lang_Service3 = (string)row["LANG_OF_SVCE_3"];
                returnValue.CurrencyLanguageServices.Lang_Service4 = (string)row["LANG_OF_SVCE_4"];
                returnValue.CurrencyLanguageServices.Lang_Service5 = (string)row["LANG_OF_SVCE_5"];
            }
            return returnValue;
        }

        public User GetUserSelfIdentification(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM COTO_REGDATA " +
                " WHERE ID = '" + User_Id + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.SelfIdentificationQuestion1 = (string)row["INDIGENOUS"];
                returnValue.SelfIdentificationQuestion2 = (string)row["INDIGENOUS_CONTACT"];
            }
            return returnValue;
        }

        public User GetUserCurrencyHoursInfo(string User_Id, int CurrentYear)
        {
            User returnValue = null;
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM CURRENCY_HOURS " +
                " WHERE ID = '" + User_Id + "' AND REGISTRATION_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.CurrencyLanguageServices = new COTO_RegOnly.Classes.CurrencyLanguage();
                returnValue.CurrencyLanguageServices.Currency = (string)row["CURRENCY_REQ_MET"];
            }
            return returnValue;
        }

        public User GetUserConductInfo(string User_Id, int CurrentYear)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM CONDUCT_HISTORY " +
                " WHERE ID = '" + User_Id + "' AND RENEWAL_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.ConductInfo = new COTO_RegOnly.Classes.Conduct();
                returnValue.ConductInfo.Refused_OT_Other = (string)row["REFUSED_OT_OTHER"];
                returnValue.ConductInfo.Refused_OT_Other_Details = (string)row["REFUSED_OT_OTHER_DETAILS"];

                returnValue.ConductInfo.Finding_Other_Juris = (string)row["FINDING_OTHER_JURIS"];
                returnValue.ConductInfo.Finding_Other_Juris_Details = (string)row["FINDING_OTHER_JURIS_DETAILS"];

                returnValue.ConductInfo.Finding_Other_Prof = (string)row["FINDING_OTHER_PROF"];
                returnValue.ConductInfo.Finding_Other_Prof_Details = (string)row["FINDING_OTHER_PROF_DETAILS"];

                //returnValue.ConductInfo.Guilty_Rel_OT = (string)row["GUILTY_REL_OT"];
                //returnValue.ConductInfo.Guilty_Rel_OT_Details = (string)row["GUILTY_REL_OT_DETAILS"];

                returnValue.ConductInfo.Criminal_Guilt = (string)row["CRIMINAL_GUILT"];
                returnValue.ConductInfo.Criminal_Guilt_Details = (string)row["CRIMINAL_GUILT_DETAILS"];

                returnValue.ConductInfo.Lack_Know_Skill_Judg = (string)row["LACK_KNOW_SKILL_JUDG"];
                returnValue.ConductInfo.Lack_Know_Skill_Judg_Details = (string)row["LACK_KNOW_SKILL_JUDG_DETAILS"];

                returnValue.ConductInfo.Finding_Neg_Malpractice = (string)row["FINDING_NEG_MALPRACTICE"];
                returnValue.ConductInfo.Finding_Neg_Malpractice_Details = (string)row["FINDING_NEG_MALPRACTICE_DETAIL"];

                returnValue.ConductInfo.Facing_Proc = (string)row["FACING_PROC"];
                returnValue.ConductInfo.Facing_Proc_Details = (string)row["FACING_PROC_DETAILS"];
                returnValue.ConductInfo.Facing_Other_Prof = (string)row["FACING_OTHER_PROF"];
                returnValue.ConductInfo.Facing_Other_Prof_Details = (string)row["FACING_OTHER_PROF_DETAILS"];
                
            }
            return returnValue;
        }

        public ConductNew GetUserConductInfoNew(string User_Id)
        {
            ConductNew returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT TOP(1) * FROM COTO_CONDUCT " +
                " WHERE ID = " + Escape(User_Id) + " ORDER BY SEQN DESC";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new ConductNew();
                returnValue.UserID = User_Id;
                returnValue.SEQN = (int)row["SEQN"];
                if (row["DATE_REPORTED"] != DBNull.Value) returnValue.ReportedDate = (DateTime)row["DATE_REPORTED"];


                returnValue.RefusedByRegBody = (string)row["REFUSED_BY_REG_BODY"];
                returnValue.RefusedByRegBodyDetails = (string)row["REFUSED_BY_REG_BODY_DETAILS"];

                returnValue.FindMisconductIncomp = (string)row["FIND_MISCOND_INCOMP_INCAP"];
                returnValue.FindMisconductIncompDetails = (string)row["FIND_MISCON_INCOMP_INCAP_DETA"];

                returnValue.FacingMisconduct = (string)row["FACING_MISCON_INCOMP_INCAP"];
                returnValue.FacingMisconductDetails = (string)row["FACING_MISCON_INCOMP_INCAP_DET"];

                returnValue.FindNegMalpract = (string)row["FIND_NEG_MALPRACT"];
                returnValue.FindNegMalpractDetails = (string)row["FIND_NEG_MALPRACT_DETAILS"];

                returnValue.PrevConduct = (string)row["PREVIOUS_CONDUCT"];
                returnValue.PrevConductDetails = (string)row["PREVIOUS_CONDUCT_DETAILS"];

                returnValue.Guilty_Authority = (string)row["GUILTY_AUTHORITY_OFFENCE"];
                returnValue.Guilty_Authority_Details = (string)row["GUILTY_AUTH_OFFENCE_DETAILS"];

                returnValue.CondRestrict = (string)row["COND_RESTRICT"];
                returnValue.CondRestrictDetails = (string)row["COND_RESTRICT_DETAILS"];

            }
            return returnValue;
        }

        public ConductNew GetUserConductInfoNew2(string User_Id, int CurrentYear)
        {
            ConductNew returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = string.Format("SELECT TOP(1) * FROM COTO_SUIT_PRACTICE  WHERE ID = {0} AND RENEWAL_YEAR = {1}  ORDER BY SEQN DESC", Escape(User_Id), Escape(CurrentYear.ToString()));
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new ConductNew();
                returnValue.UserID = User_Id;
                returnValue.SEQN = (int)row["SEQN"];
                if (row["DATE_REPORTED"] != DBNull.Value) returnValue.ReportedDate = (DateTime)row["DATE_REPORTED"];

                // #1
                returnValue.FacingMisconduct = (string)row["FACING_MISCON_INCOMP_INCAP"];
                if (row["FACING_MISCON_INCOMP_INCAP_DET"] != DBNull.Value) returnValue.FacingMisconductDetails = (string)row["FACING_MISCON_INCOMP_INCAP_DET"];

                // #2
                returnValue.FindMisconductIncomp = (string)row["FIND_MISCOND_INCOMP_INCAP"];
                if (row["FIND_MISCON_INCOMP_INCAP_DET"] != DBNull.Value) returnValue.FindMisconductIncompDetails = (string)row["FIND_MISCON_INCOMP_INCAP_DET"];

                //#3
                returnValue.FindNegMalpract = (string)row["FIND_NEG_MALPRACT"];
                if (row["FIND_NEG_MALPRACT_DETAILS"] != DBNull.Value) returnValue.FindNegMalpractDetails = (string)row["FIND_NEG_MALPRACT_DETAILS"];

                // #4
                returnValue.ChargedOffence = (string)row["CHARGED_OFFENCE"];
                if (row["CHARGED_OFFENCE_DETAILS"] != DBNull.Value) returnValue.ChargedOffenceDetails = (string)row["CHARGED_OFFENCE_DETAILS"];

                // #5
                returnValue.CondRestrict = (string)row["COND_RESTRICT"];
                if (row["COND_RESTRICT_DETAILS"] != DBNull.Value)  returnValue.CondRestrictDetails = (string)row["COND_RESTRICT_DETAILS"];

                // #6
                returnValue.Guilty_Authority = (string)row["GUILTY_AUTHORITY_OFFENCE"];
                if (row["GUILTY_AUTH_OFFENCE_DETAILS"] != DBNull.Value) returnValue.Guilty_Authority_Details = (string)row["GUILTY_AUTH_OFFENCE_DETAILS"];

                // #7
                returnValue.EventCircumstance = (string)row["EVENT_CIRCUMSTANCE"];
                if (row["EVENT_CIRCUMSTANCE_DETAILS"] != DBNull.Value) returnValue.EventCircumstanceDetails = (string)row["EVENT_CIRCUMSTANCE_DETAILS"];


            }
            return returnValue;
        }

        public ConductNew GetUserApplicationConductInfoPrev(string User_Id)
        {
            ConductNew conductNew = null;
            var application = GetUserApplicationConuctSubmittedDate(User_Id);

            if (application == null || application.ConductSubmitted == DateTime.MinValue)
            {
                return conductNew;
            }

            WebConfigItems.GetConnectionString();
            DataTable data = new clsDB().GetData("SELECT TOP(1) * FROM COTO_CONDUCT  WHERE ID = " + this.Escape(User_Id) + " ORDER BY DATE_REPORTED DESC");
            if (data != null && data.Rows.Count == 1)
            {
                DataRow row = data.Rows[0];
                DateTime minValue = DateTime.MinValue;
                if (row["DATE_REPORTED"] != DBNull.Value)
                    minValue = (DateTime)row["DATE_REPORTED"];
                if (minValue != DateTime.MinValue && minValue == application.ConductSubmitted)
                {
                    conductNew = new ConductNew();
                    conductNew.UserID = User_Id;
                    conductNew.SEQN = (int)row["SEQN"];
                    if (row["DATE_REPORTED"] != DBNull.Value)  conductNew.ReportedDate = (DateTime)row["DATE_REPORTED"];

                    conductNew.RefusedByRegBody = (string)row["REFUSED_BY_REG_BODY"];
                    if (row["REFUSED_BY_REG_BODY_DETAILS"] != DBNull.Value) conductNew.RefusedByRegBodyDetails = (string)row["REFUSED_BY_REG_BODY_DETAILS"];

                    conductNew.FindMisconductIncomp = (string)row["FIND_MISCOND_INCOMP_INCAP"];
                    if (row["FIND_MISCON_INCOMP_INCAP_DETA"] != DBNull.Value) conductNew.FindMisconductIncompDetails = (string)row["FIND_MISCON_INCOMP_INCAP_DETA"];

                    conductNew.FacingMisconduct = (string)row["FACING_MISCON_INCOMP_INCAP"];
                    if (row["FACING_MISCON_INCOMP_INCAP_DET"] != DBNull.Value) conductNew.FacingMisconductDetails = (string)row["FACING_MISCON_INCOMP_INCAP_DET"];

                    conductNew.FindNegMalpract = (string)row["FIND_NEG_MALPRACT"];
                    if (row["FIND_NEG_MALPRACT_DETAILS"] != DBNull.Value) conductNew.FindNegMalpractDetails = (string)row["FIND_NEG_MALPRACT_DETAILS"];

                    conductNew.PrevConduct = (string)row["PREVIOUS_CONDUCT"];
                    if (row["PREVIOUS_CONDUCT_DETAILS"] != DBNull.Value) conductNew.PrevConductDetails = (string)row["PREVIOUS_CONDUCT_DETAILS"];

                    conductNew.Guilty_Authority = (string)row["GUILTY_AUTHORITY_OFFENCE"];
                    if (row["GUILTY_AUTH_OFFENCE_DETAILS"] != DBNull.Value) conductNew.Guilty_Authority_Details = (string)row["GUILTY_AUTH_OFFENCE_DETAILS"];

                    conductNew.CondRestrict = (string)row["COND_RESTRICT"];
                    if (row["COND_RESTRICT_DETAILS"] != DBNull.Value) conductNew.CondRestrictDetails = (string)row["COND_RESTRICT_DETAILS"];
                }
            }
            return conductNew;
        }

        public ConductNew GetUserApplicationConductInfoNew(string User_Id)
        {
            ConductNew returnValue = null;

            var application = GetUserApplicationConuctSubmittedDate(User_Id);

            if (application == null || application.ConductSubmitted == DateTime.MinValue)
            {
                return returnValue;
            }

            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT TOP(1) * FROM COTO_CONDUCT " +
                " WHERE ID = " + Escape(User_Id) + " ORDER BY DATE_REPORTED DESC";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];

                var date_reported = DateTime.MinValue;
                if (row["DATE_REPORTED"] != DBNull.Value) date_reported = (DateTime)row["DATE_REPORTED"];

                // if date_reported in conduct table is the same as in coto_application then we returning the existing last record from coto_conduct table

                if (date_reported!=DateTime.MinValue && date_reported == application.ConductSubmitted)
                {
                    returnValue = new ConductNew();
                    returnValue.UserID = User_Id;
                    returnValue.SEQN = (int)row["SEQN"];
                    if (row["DATE_REPORTED"] != DBNull.Value) returnValue.ReportedDate = (DateTime)row["DATE_REPORTED"];

                    returnValue.RefusedByRegBody = (string)row["REFUSED_BY_REG_BODY"];
                    if (row["REFUSED_BY_REG_BODY_DETAILS"] != DBNull.Value) returnValue.RefusedByRegBodyDetails = (string)row["REFUSED_BY_REG_BODY_DETAILS"];

                    returnValue.FindMisconductIncomp = (string)row["FIND_MISCOND_INCOMP_INCAP"];
                    if (row["FIND_MISCON_INCOMP_INCAP_DETA"] != DBNull.Value) returnValue.FindMisconductIncompDetails = (string)row["FIND_MISCON_INCOMP_INCAP_DETA"];

                    returnValue.FacingMisconduct = (string)row["FACING_MISCON_INCOMP_INCAP"];
                    if (row["FACING_MISCON_INCOMP_INCAP_DET"] != DBNull.Value) returnValue.FacingMisconductDetails = (string)row["FACING_MISCON_INCOMP_INCAP_DET"];

                    returnValue.FindNegMalpract = (string)row["FIND_NEG_MALPRACT"];
                    if (row["FIND_NEG_MALPRACT_DETAILS"] != DBNull.Value) returnValue.FindNegMalpractDetails = (string)row["FIND_NEG_MALPRACT_DETAILS"];

                    //returnValue.PrevConduct = (string)row["PREVIOUS_CONDUCT"];
                    //returnValue.PrevConductDetails = (string)row["PREVIOUS_CONDUCT_DETAILS"];

                    returnValue.ChargedOffence = (string)row["CHARGED_OFFENCE"];
                    if (row["CHARGED_OFFENCE_DETAILS"]!=DBNull.Value) returnValue.ChargedOffenceDetails = (string)row["CHARGED_OFFENCE_DETAILS"];

                    returnValue.CondRestrict = (string)row["COND_RESTRICT"];
                    if (row["COND_RESTRICT_DETAILS"] != DBNull.Value) returnValue.CondRestrictDetails = (string)row["COND_RESTRICT_DETAILS"];

                    returnValue.Guilty_Authority = (string)row["GUILTY_AUTHORITY_OFFENCE"];
                    if (row["GUILTY_AUTH_OFFENCE_DETAILS"] != DBNull.Value) returnValue.Guilty_Authority_Details = (string)row["GUILTY_AUTH_OFFENCE_DETAILS"];

                    returnValue.EventCircumstance = (string)row["EVENT_CIRCUMSTANCE"];
                    if (row["EVENT_CIRCUMSTANCE_DETAILS"] != DBNull.Value) returnValue.EventCircumstanceDetails = (string)row["EVENT_CIRCUMSTANCE_DETAILS"];

                }
            }
            return returnValue;
        }

        public ControlledAct GetUserControlledActInfo(string User_Id, int CurrentYear)
        {
            ControlledAct retValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM COTO_CONTROLACTS " +
                " WHERE ID = " + Escape(User_Id) + " AND RENEWAL_YEAR = " + CurrentYear.ToString();
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                retValue = new ControlledAct();
                retValue.Comm_Diagnosis_1 = (string)row["COMM_DIAGNOSIS"];
                retValue.Below_Dermis_2 = (string)row["BELOW_DERMIS"];
                retValue.Setting_Casting_3 = (string)row["SETTING_CASTING"];
                retValue.Spinal_Manipulation_4 = (string)row["SPINAL_MANIPULATION"];
                retValue.Administer_Substance_5 = (string)row["ADMINISTER_SUBSTANCE"];
                retValue.Instr_Hand_Finger_6 = (string)row["INSTRUMENT_HAND_FINGER"];
                retValue.Xrays_7 = (string)row["XRAYS"];
                retValue.Drugs_8 = (string)row["DRUGS"];
                retValue.Vision_9 = (string)row["VISION"];
                retValue.Psyhotherapy_10 = (string)row["PSYCHOTHERAPY"];
                retValue.Acupuncture_11 = (string)row["ACUPUNCTURE"];

            }
            return retValue;
        }

        public User GetUserInsuranceInfo(string User_Id, int CurrentYear)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM INSURANCE " +
                " WHERE ID = '" + User_Id + "' AND INS_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.InsuranceInfo = new COTO_RegOnly.Classes.Insurance();
                returnValue.InsuranceInfo.PlanHeld = (string)row["INS_PLAN_HELD"];
                returnValue.InsuranceInfo.OtherInsuranceProvider = (string)row["INS_INSURE_OTHER"];

                if (row["INS_EXPIRY_DATE"] != DBNull.Value)
                {
                    returnValue.InsuranceInfo.ExpiryDate = (DateTime)row["INS_EXPIRY_DATE"];
                }
                else
                {
                    returnValue.InsuranceInfo.ExpiryDate = DateTime.MinValue;
                }

                returnValue.InsuranceInfo.CertificateNumber = (string)row["INS_CERT_NUMBER"];


            }
            return returnValue;
        }

        public User GetUserDeclarationInfo(string User_Id, int CurrentYear)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM DECLARATION " +
                " WHERE ID = '" + User_Id + "' AND DECLARE_SIGNED_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.DeclarationInfo = new COTO_RegOnly.Classes.Declaration();
                returnValue.DeclarationInfo.RegistrationDeclaration = (string)row["DECLARE_SIGNED"];
                returnValue.DeclarationInfo.QADeclaration = (string)row["QA_DECLARE_SIGNED"];
            }
            return returnValue;
        }

        public User GetUserEducationInfo(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM EDUCATION_ALL " +
                " WHERE ID = " + Escape(User_Id);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.Education = new UserEducation();
                returnValue.Education.EntryDegree = new Education();
                returnValue.Education.EntryDegree.DiplomaName = (string)row["DEGREE_ENTRY_OT"]; // GetGeneralName("EDUCATION_ALL", (string)row["DEGREE_ENTRY_OT"]);
                returnValue.Education.EntryDegree.CanadianUniversity = (string)row["UNIVERSITY_ENTRY_OT"];
                returnValue.Education.EntryDegree.OtherUniversity = (string)row["UNIVERSITY_ENTRY_OTHER_OT"];
                returnValue.Education.EntryDegree.ProvinceState = (string)row["PROVINCE_STATE_ENTRY_OT"];
                returnValue.Education.EntryDegree.Country = (string)row["COUNTRY_ENTRY_OT"];
                returnValue.Education.EntryDegree.GraduationYear = (string)row["GRAD_YEAR_ENTRY_OT"];

                returnValue.Education.Degree2 = new Education();
                returnValue.Education.Degree2.DiplomaName = (string)row["DEGREE_2_OT"]; // GetGeneralName("EDUCATION_ALL", (string)row["DEGREE_2_OT"]); 
                returnValue.Education.Degree2.CanadianUniversity = (string)row["UNIVERSITY_2_OT"];
                returnValue.Education.Degree2.OtherUniversity = (string)row["UNIVERSITY_2_OTHER_OT"];
                returnValue.Education.Degree2.OtherUniversityNotListed = (string)row["UNIV_2_OTHER_OT_NL"];
                returnValue.Education.Degree2.ProvinceState = (string)row["PROVINCE_STATE_2_OT"];
                returnValue.Education.Degree2.Country = (string)row["COUNTRY_2_OT"];
                returnValue.Education.Degree2.GraduationYear = (string)row["GRAD_YEAR_2_OT"];

                returnValue.Education.Degree3 = new Education();
                returnValue.Education.Degree3.DiplomaName = (string)row["DEGREE_3_OT"]; // GetGeneralName("EDUCATION_ALL", (string)row["DEGREE_3_OT"]); 
                returnValue.Education.Degree3.CanadianUniversity = (string)row["UNIVERSITY_3_OT"];
                returnValue.Education.Degree3.OtherUniversity = (string)row["UNIVERSITY_3_OTHER_OT"];
                returnValue.Education.Degree3.OtherUniversityNotListed = (string)row["UNIV_3_OTHER_OT_NL"];
                returnValue.Education.Degree3.ProvinceState = (string)row["PROVINCE_STATE_3_OT"];
                returnValue.Education.Degree3.Country = (string)row["COUNTRY_3_OT"];
                returnValue.Education.Degree3.GraduationYear = (string)row["GRAD_YEAR_3_OT"];

                returnValue.Education.OtherDegree1 = new Education();
                returnValue.Education.OtherDegree1.DiplomaName = (string)row["DEGREE_1_OTHER"]; // GetGeneralName("EDUCATION_ALL", (string)row["DEGREE_1_OTHER"]); 
                returnValue.Education.OtherDegree1.CanadianUniversity = (string)row["UNIVERSITY_1_OTHER"];
                returnValue.Education.OtherDegree1.OtherUniversity = (string)row["UNIVERSITY_1_INTL"];
                returnValue.Education.OtherDegree1.OtherUniversityNotListed = (string)row["UNIV_1_OTHER_INTL_NL"];
                returnValue.Education.OtherDegree1.StudyField = (string)row["FIELD_STUDY_1_OTHER"];
                returnValue.Education.OtherDegree1.ProvinceState = (string)row["STATE_PROV_1_OTHER"];
                returnValue.Education.OtherDegree1.Country = (string)row["COUNTRY_1_OTHER"];
                returnValue.Education.OtherDegree1.GraduationYear = (string)row["GRAD_YEAR_1_OTHER"];

                returnValue.Education.OtherDegree2 = new Education();
                returnValue.Education.OtherDegree2.DiplomaName = (string)row["DEGREE_2_OTHER"]; // GetGeneralName("EDUCATION_ALL", (string)row["DEGREE_2_OTHER"]); 
                returnValue.Education.OtherDegree2.CanadianUniversity = (string)row["UNIVERSITY_2_OTHER"];
                returnValue.Education.OtherDegree2.OtherUniversity = (string)row["UNIVERSITY_2_INTL"];
                returnValue.Education.OtherDegree2.OtherUniversityNotListed = (string)row["UNIV_2_OTHER_INTL_NL"];
                returnValue.Education.OtherDegree2.StudyField = (string)row["FIELD_STUDY_2_OTHER"];
                returnValue.Education.OtherDegree2.ProvinceState = (string)row["STATE_PROV_2_OTHER"];
                returnValue.Education.OtherDegree2.Country = (string)row["COUNTRY_2_OTHER"];
                returnValue.Education.OtherDegree2.GraduationYear = (string)row["GRAD_YEAR_2_OTHER"];
            }
            return returnValue;
        }

        public User GetUserProfessionalHistoryInfo(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM ANN_REG_ADDL_INFO " +
                " WHERE ID = '" + User_Id + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.ProfessionalHistoryInfo = new ProfessionalHistory();
                returnValue.ProfessionalHistoryInfo.RegisteredPractiseInOtherProvince = (string)row["OTHER_PROV_OT_PRACTICE"];
                returnValue.ProfessionalHistoryInfo.RegisteredPractiseInOtherProfession = (string)row["OTHER_PROF_PRACTICE"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail1 = new ProfessionalHistoryPracticeDetail();
                returnValue.ProfessionalHistoryInfo.PracticeDetail2 = new ProfessionalHistoryPracticeDetail();
                returnValue.ProfessionalHistoryInfo.PracticeDetail3 = new ProfessionalHistoryPracticeDetail();
                returnValue.ProfessionalHistoryInfo.PracticeDetail4 = new ProfessionalHistoryPracticeDetail();

                returnValue.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBody = (string)row["OPROV_REG_BODY_1"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyOther = (string)row["OPROV_OTHER_1"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail1.Province = (string)row["OPROV_PROV_1"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail1.Country = (string)row["OPROV_COUNTRY_1"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail1.License = (string)row["OPROV_REG_LIC_1"];
                if (row["OPROV_EXP_DATE_1"] != DBNull.Value)
                {
                    returnValue.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate = (DateTime)row["OPROV_EXP_DATE_1"];
                }
                else
                {
                    returnValue.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate = null;
                }

                returnValue.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBody = (string)row["OPROV_REG_BODY_2"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyOther = (string)row["OPROV_OTHER_2"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail2.Province = (string)row["OPROV_PROV_2"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail2.Country = (string)row["OPROV_COUNTRY_2"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail2.License = (string)row["OPROV_REG_LIC_2"];
                if (row["OPROV_EXP_DATE_2"] != DBNull.Value)
                {
                    returnValue.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate = (DateTime)row["OPROV_EXP_DATE_2"];
                }
                else
                {
                    returnValue.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate = null;
                }

                returnValue.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBody = (string)row["OPROV_REG_BODY_3"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyOther = (string)row["OPROV_OTHER_3"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail3.Province = (string)row["OPROV_PROV_3"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail3.Country = (string)row["OPROV_COUNTRY_3"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail3.License = (string)row["OPROV_REG_LIC_3"];
                if (row["OPROV_EXP_DATE_3"] != DBNull.Value)
                {
                    returnValue.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate = (DateTime)row["OPROV_EXP_DATE_3"];
                }
                else
                {
                    returnValue.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate = null;
                }

                returnValue.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBody = (string)row["OPROV_REG_BODY_4"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyOther = (string)row["OPROV_OTHER_4"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail4.Province = (string)row["OPROV_PROV_4"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail4.Country = (string)row["OPROV_COUNTRY_4"];
                returnValue.ProfessionalHistoryInfo.PracticeDetail4.License = (string)row["OPROV_REG_LIC_4"];
                if (row["OPROV_EXP_DATE_4"] != DBNull.Value)
                {
                    returnValue.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate = (DateTime)row["OPROV_EXP_DATE_4"];
                }
                else
                {
                    returnValue.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate = null;
                }

                returnValue.ProfessionalHistoryInfo.ProfessionDetail1 = new ProfessionalHistoryProfessionDetail();
                returnValue.ProfessionalHistoryInfo.ProfessionDetail2 = new ProfessionalHistoryProfessionDetail();

                returnValue.ProfessionalHistoryInfo.ProfessionDetail1.ProfessionName = (string)row["OTHER_PROF_PROF_NAME_1"];
                returnValue.ProfessionalHistoryInfo.ProfessionDetail1.RegulatoryBody = (string)row["OTHER_PROF_REG_BODY_1"];
                returnValue.ProfessionalHistoryInfo.ProfessionDetail1.Province = (string)row["OTHER_PROF_PROV_STATE_1"];
                returnValue.ProfessionalHistoryInfo.ProfessionDetail1.Country = (string)row["OTHER_PROF_COUNTRY_1"];
                returnValue.ProfessionalHistoryInfo.ProfessionDetail1.License = (string)row["OTHER_PROF_REG_LICE_1"];
                if (row["OTHER_PROF_EXP_DATE_1"] != DBNull.Value)
                {
                    returnValue.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate = (DateTime)row["OTHER_PROF_EXP_DATE_1"];
                }
                else
                {
                    returnValue.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate = null;
                }

                returnValue.ProfessionalHistoryInfo.ProfessionDetail2.ProfessionName = (string)row["OTHER_PROF_PROF_NAME_2"];
                returnValue.ProfessionalHistoryInfo.ProfessionDetail2.RegulatoryBody = (string)row["OTHER_PROF_REG_BODY_2"];
                returnValue.ProfessionalHistoryInfo.ProfessionDetail2.Province = (string)row["OTHER_PROF_PROV_STATE_2"];
                returnValue.ProfessionalHistoryInfo.ProfessionDetail2.Country = (string)row["OTHER_PROF_COUNTRY_2"];
                returnValue.ProfessionalHistoryInfo.ProfessionDetail2.License = (string)row["OTHER_PROF_REG_LICE_2"];
                if (row["OTHER_PROF_EXP_DATE_2"] != DBNull.Value)
                {
                    returnValue.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate = (DateTime)row["OTHER_PROF_EXP_DATE_2"];
                }
                else
                {
                    returnValue.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate = null;
                }
            }
            return returnValue;
        }

        public List<Activity> GetUserPreviousNames(string User_Id)
        {
            List<Activity> returnValue = new List<Activity>();
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM Activity WHERE Activity_Type = 'L_P_NAME' AND ID = " + Escape(User_Id) + " ORDER BY UF_3";

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    var activity = new Activity();
                    activity.ID = (string)row["ID"];
                    activity.ActivityType = (string)row["ACTIVITY_TYPE"];
                    activity.UF_1 = (string)row["UF_1"];
                    activity.UF_2 = (string)row["UF_2"];
                    activity.UF_3 = (string)row["UF_3"];

                    returnValue.Add(activity);
                }
            }
            return returnValue;
        }

        public User GetUserPersonalInfo(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = " SELECT Name.Last_Name, Name.Middle_Name, Name.First_Name, COTO_REGDATA.LEGAL_PREVIOUS_FIRST_NAME, COTO_REGDATA.LEGAL_PREVIOUS_LAST_NAME, COTO_REGDATA.Legal_First_Name, COTO_REGDATA.Legal_Last_Name " +
                         " FROM Name INNER JOIN COTO_REGDATA ON Name.ID = COTO_REGDATA.ID WHERE NAME.ID  = " + Escape(User_Id);

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                returnValue = new User();
                returnValue.LegalLastName = (string)row["Legal_Last_Name"];
                returnValue.LegalFirstName = (string)row["Legal_First_Name"];
                //returnValue.LegalMiddleName = (string)row["MIDDLE_NAME"];
                returnValue.PreviousLegalLastName = (string)row["LEGAL_PREVIOUS_LAST_NAME"];
                returnValue.PreviousLegalFirstName = (string)row["LEGAL_PREVIOUS_FIRST_NAME"];
                returnValue.CommonlyUsedLastName = (string)row["LAST_NAME"];
                returnValue.CommonlyUsedFirstName = (string)row["FIRST_NAME"];
                returnValue.CommonlyUsedMiddleName = (string)row["Middle_Name"];
            }
            return returnValue;
        }

        public List<GenClass> GetUserPreviousRegistrationNumbers(string User_Id)
        {
            List<GenClass> returnValue = new List<GenClass>();
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT  * FROM Name_Log WHERE Id =  " + Escape(User_Id) +
                        " AND Log_Text LIKE 'Name.MAJOR_KEY:%' ";

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    var item = new GenClass();
                    string strValue = (string)row["Log_Text"];
                    strValue = strValue.Substring(0, strValue.IndexOf("->") - 1);
                    if (strValue.IndexOf("Name.MAJOR_KEY: T") >= 0)
                    {
                        strValue = strValue.Substring(strValue.IndexOf(": T") + 2);
                    }

                    if (strValue.IndexOf("Name.MAJOR_KEY: P") >= 0)
                    {
                        strValue = strValue.Substring(strValue.IndexOf(": P") + 2);
                    }

                    if (strValue.IndexOf("Name.MAJOR_KEY: G") >= 0)
                    {
                        strValue = strValue.Substring(strValue.IndexOf(": G") + 2);
                    }

                    if (strValue.IndexOf("Name.MAJOR_KEY: ") >= 0)
                    {
                        strValue = strValue.Substring(strValue.IndexOf("Name.MAJOR_KEY: ") + 16);
                    }

                    item.Description = strValue;
                    returnValue.Add(item);
                }
            }
            return returnValue;
        }

        public List<GenClass> GetUserTermsConditions(string User_Id)
        {
            List<GenClass> returnValue = new List<GenClass>();
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT Note FROM Activity WHERE  Id = " + Escape(User_Id) + " AND  Activity_Type = 'TCLS' AND (effective_date<=getdate() OR effective_date is null) and (thru_date>=getdate() OR thru_date is null)";

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    var item = new GenClass();
                    string strValue = (string)row["Note"];

                    item.Description = strValue;
                    returnValue.Add(item);
                }
            }
            return returnValue;
        }

        public List<Discipline> GetUserDisciplines(string User_Id)
        {
            List<Discipline> returnValue = new List<Discipline>();
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM ICRC_INFO WHERE COMP_OUTCOME='Referred to discipline'" +
                        " AND ID=" + Escape(User_Id) + " AND REMOVAL_REG_DATE > GetDate()";

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    var item = new Discipline();
                    if (row["DECISION_MADE"] != DBNull.Value)
                    {
                        item.DecisionMade = (DateTime)row["DECISION_MADE"];
                    }

                    if (row["HEAR_ALLEGATIONS"] != DBNull.Value)
                    {
                        item.HearAllegations = (string)row["HEAR_ALLEGATIONS"];
                    }

                    if (row["DISC_HEARING_DATE"] != DBNull.Value)
                    {
                        item.DiscHearingDate = (string)row["DISC_HEARING_DATE"];
                    }

                    if (row["PENALTY_HEARING_DATE"] != DBNull.Value)
                    {
                        item.PenaltyHearingDate = (string)row["PENALTY_HEARING_DATE"];
                    }

                    if (row["HEAR_OUTCOME_SUMM"] != DBNull.Value)
                    {
                        item.HearOutcomeSummary = (string)row["HEAR_OUTCOME_SUMM"];
                    }

                    //item.AdditionalInfo = (string)row["MISC_2"];
                    returnValue.Add(item);
                }
            }
            return returnValue;
        }

        public List<Discipline> GetUserDisciplinesNew(string User_Id)
        {
            List<Discipline> returnValue = new List<Discipline>();
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM IR_DISCIPLINE WHERE DECISION_FINDING_DATE IS NOT NULL " +
                        " AND ID=" + Escape(User_Id) + " AND  NOT (DECISION_FINDING_DATE  <'2009-08-01' AND  DATEADD(year, 6, DECISION_FINDING_DATE )  <= GETDATE())";

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    var item = new Discipline();
                    if (row["REF_TO_DISC_DATE"] != DBNull.Value)
                    {
                        item.RefToDiscComm = (DateTime)row["REF_TO_DISC_DATE"];
                    }

                    if (row["ALLEGATIONS_REFERRED"] != DBNull.Value)
                    {
                        item.SummaryAllegation = (string)row["ALLEGATIONS_REFERRED"];
                    }

                    if (row["DISC_HEARING_DATE"] != DBNull.Value)
                    {
                        item.DiscHearingDate = (string)row["DISC_HEARING_DATE"];
                    }

                    if (row["PENALTY_HEARING_DATE"] != DBNull.Value)
                    {
                        item.PenaltyHearingDate = (string)row["PENALTY_HEARING_DATE"];
                    }

                    if (row["HEARING_PANEL_MBRS"] != DBNull.Value)
                    {
                        item.HearingPanelMembers = (string)row["HEARING_PANEL_MBRS"];
                    }

                    if (row["DECISION_FINDING_DATE"] != DBNull.Value)
                    {
                        item.DecisionFinding = (DateTime)row["DECISION_FINDING_DATE"];
                    }

                    if (row["PUBLICATION_BAN_TERMS"] != DBNull.Value)
                    {
                        item.PublicationBanTerms = (string)row["PUBLICATION_BAN_TERMS"];
                    }

                    if (row["HEARING_OUTCOME"] != DBNull.Value)
                    {
                        item.HearingOutcome = (string)row["HEARING_OUTCOME"];
                    }

                    if (row["HEARING_SUMMARY"] != DBNull.Value)
                    {
                        item.HearingSummary = (string)row["HEARING_SUMMARY"];
                    }
                    //item.AdditionalInfo = (string)row["MISC_2"];
                    returnValue.Add(item);
                }
            }
            return returnValue;
        }

        public List<Discipline> GetUserDisciplinesNew2(string User_Id)
        {
            List<Discipline> returnValue = new List<Discipline>();
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM IR_DISCIPLINE WHERE DECISION_FINDING_DATE IS NOT NULL " +
                        " AND ID=" + Escape(User_Id);

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    var item = new Discipline();
                    if (row["DECISION_FINDING_DATE"] != DBNull.Value)
                    {
                        item.DecisionFinding = (DateTime)row["DECISION_FINDING_DATE"];
                    }

                    if (row["PUBLICATION_BAN_TERMS"] != DBNull.Value)
                    {
                        item.PublicationBanTerms = (string)row["PUBLICATION_BAN_TERMS"];
                    }

                    if (row["HEARING_OUTCOME"] != DBNull.Value)
                    {
                        item.HearingOutcome = (string)row["HEARING_OUTCOME"];
                    }

                    if (row["HEARING_SUMMARY"] != DBNull.Value)
                    {
                        item.HearingSummary = (string)row["HEARING_SUMMARY"];
                    }

                    returnValue.Add(item);
                }
            }
            return returnValue;
        }
        public List<User> FindUserInfo(string FirstName, string LastName, string Reg_Number, string City, string PostalCode, string Language,
            string FundSource, string MajorService, string ClientAge, string PracticeSetting, string Letter, int returnResult, int StartIndex, out string sqlquery)
        {

            List<User> returnValue = new List<User>();
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "WITH FindResults AS ( SELECT *, ROW_NUMBER() OVER (ORDER BY LAST_NAME ASC, FIRST_NAME ASC) AS RANK FROM (SELECT DISTINCT(n.ID), n.First_Name, n.Last_Name, n.Middle_Name, n.MAJOR_KEY, n.STATUS, n.INFORMAL, curr.LEGAL_PREVIOUS_LAST_NAME, " +
                " ISNULL(gt.Description,'') AS [STATUS_NAME], " +
                " curr.LEGAL_LAST_NAME" +
                " FROM	Name as n " +
                " LEFT OUTER  join Name_Address as na ON na.ID = n.ID  AND (na.PURPOSE = 'PRIMARY EMPLOYMENT' OR na.PURPOSE = 'SEC. EMPLOYMENT')" +
                " LEFT OUTER  join COTO_REGDATA as curr ON curr.ID = n.ID " +
                " LEFT OUTER  join ANN_REG_ADDL_INFO ON n.id = ANN_REG_ADDL_INFO.ID " +
                " LEFT OUTER  join EMPLOYMENT_ALL ON n.id = EMPLOYMENT_ALL.ID " +
                " LEFT OUTER  join gen_tables as gt ON gt.CODE = n.Status AND gt.TABLE_NAME = 'Member_Status'" +
                " LEFT OUTER  join Activity as act ON act.ID = n.ID AND act.Activity_Type = 'L_P_NAME' " +
                " WHERE " +
                " ( ( n.STATUS not in ('P','D') and n.member_type='reg'  ) OR ( n.STATUS  in ('AF','IF') and n.member_type='APP' ) )";

            if (!string.IsNullOrEmpty(Language))
            {
                sql += " AND ( " +
                " CURR.LANG_OF_SVCE_1 = " + Escape(Language) +
                " OR CURR.LANG_OF_SVCE_2 = " + Escape(Language) +
                " OR CURR.LANG_OF_SVCE_3 = " + Escape(Language) +
                " OR CURR.LANG_OF_SVCE_4 = " + Escape(Language) +
                " OR CURR.LANG_OF_SVCE_5 = " + Escape(Language) + ")";
            }

            if (!string.IsNullOrEmpty(Reg_Number))
            {
                sql += " AND N.Major_Key = @1"; // +Escape(Reg_Number);
            }

            if (!string.IsNullOrEmpty(FundSource))
            {
                sql += " AND ( " +
                " EMPLOYMENT_ALL.EMP_1_FUNDING_SOURCE = " + Escape(FundSource) +
                " OR EMPLOYMENT_ALL.EMP_2_FUNDING_SOURCE = " + Escape(FundSource) +
                " OR EMPLOYMENT_ALL.EMP_3_FUNDING_SOURCE = " + Escape(FundSource) + ")";
            }

            if (!string.IsNullOrEmpty(MajorService))
            {
                sql += " AND ( " +
                " EMPLOYMENT_ALL.EMP_1_AREA_PRACTICE = " + Escape(MajorService) +
                " OR EMPLOYMENT_ALL.EMP_2_AREA_PRACTICE = " + Escape(MajorService) +
                " OR EMPLOYMENT_ALL.EMP_3_AREA_PRACTICE = " + Escape(MajorService) + ")";
            }

            if (!string.IsNullOrEmpty(ClientAge))
            {
                sql += " AND ( " +
                " EMPLOYMENT_ALL.EMP_1_CLNT_ARNGE = " + Escape(ClientAge) +
                " OR EMPLOYMENT_ALL.EMP_2_CLNT_ARNGE = " + Escape(ClientAge) +
                " OR EMPLOYMENT_ALL.EMP_3_CLNT_ARNGE = " + Escape(ClientAge) + ")";
            }

            if (!string.IsNullOrEmpty(PracticeSetting))
            {
                sql += " AND ( " +
                " EMPLOYMENT_ALL.EMP_1_TYPE = " + Escape(PracticeSetting) +
                " OR EMPLOYMENT_ALL.EMP_2_TYPE = " + Escape(PracticeSetting) +
                " OR EMPLOYMENT_ALL.EMP_3_TYPE = " + Escape(PracticeSetting) + ")";
            }

            if (!string.IsNullOrEmpty(FirstName))
            {
                sql += " AND (N.First_Name LIKE @2  OR N.INFORMAL LIKE @2 " +   // + Escape(FirstName + "%") +   //+ Escape(FirstName + "%")
                 " OR curr.LEGAL_FIRST_NAME LIKE @2 OR curr.LEGAL_PREVIOUS_FIRST_NAME LIKE @2 )"; // +Escape(FirstName + "%") + " )";  //+ Escape(FirstName + "%") +
            }

            if (!string.IsNullOrEmpty(LastName))
            {
                sql += " AND ( (N.Last_Name LIKE @3)";  //+ Escape(LastName + "%")
                sql += " OR (curr.Legal_Last_Name LIKE @3)"; //+ Escape(LastName + "%")
                sql += " OR (curr.LEGAL_PREVIOUS_LAST_NAME LIKE @3)";  //+ Escape(LastName + "%")
                sql += " OR ( act.UF_3 LIKE @3 ) )";  //+ Escape(LastName + "%")
                //sql += " OR N.ID IN (SELECT ID FROM Activity WHERE Activity.Activity_Type = 'L_P_NAME' AND Activity.UF_3 LIKE @3 ) )";  //+ Escape(LastName + "%")
            }

            if (!string.IsNullOrEmpty(City))
            {
                sql += " AND (EMPLOYMENT_ALL.EMP_1_CITY LIKE @4)";   // + Escape(City + "%") 
            }

            if (!string.IsNullOrEmpty(PostalCode))
            {
                sql += " AND (EMPLOYMENT_ALL.EMP_1_POSTAL LIKE @5 )";  // + Escape(PostalCode + "%")
            }

            if (!string.IsNullOrEmpty(Letter))
            {
                sql += " AND n.LAST_NAME LIKE " + Escape(Letter + "%");
            }

            sql += " AND n.ID NOT IN ('6969') " +
                //" AND RANK >=" + StartIndex.ToString() + " AND RANK < " + (StartIndex + returnResult).ToString() +
                "  ) AS MAIN_SELECT) SELECT * FROM  FindResults";
            sql += " WHERE RANK >=" + StartIndex.ToString() + " AND RANK <= " + (StartIndex + returnResult).ToString() +
                " ORDER BY RANK ";

            var connection = new SqlConnection(dbconn);
            SqlCommand command = new SqlCommand(sql, connection);
            //command.CommandType = CommandType.TableDirect;

            if (!string.IsNullOrEmpty(Reg_Number))
            {
                command.Parameters.Add(new SqlParameter("@1", SqlDbType.VarChar));
                command.Parameters["@1"].Value = Reg_Number; // Escape(Reg_Number);
            }

            if (!string.IsNullOrEmpty(FirstName))
            {
                command.Parameters.Add(new SqlParameter("@2", SqlDbType.VarChar));
                command.Parameters["@2"].Value = FirstName + "%"; // Escape(FirstName + "%");
            }

            if (!string.IsNullOrEmpty(LastName))
            {
                command.Parameters.Add(new SqlParameter("@3", SqlDbType.VarChar));
                command.Parameters["@3"].Value = LastName + "%";  //Escape(LastName + "%");
            }

            if (!string.IsNullOrEmpty(City))
            {
                command.Parameters.Add(new SqlParameter("@4", SqlDbType.VarChar));
                command.Parameters["@4"].Value = City + "%"; //Escape(City + "%");
            }

            if (!string.IsNullOrEmpty(PostalCode))
            {
                command.Parameters.Add(new SqlParameter("@5", SqlDbType.VarChar));
                command.Parameters["@5"].Value = PostalCode + "%"; // Escape(PostalCode + "%");
            }
            sqlquery = "com-text: " + command.CommandText + ", sql: " + sql;

            var table = db.ExecuteReader(command);
            //var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    var user = new User();
                    user.Row_Number = (long)row["Rank"];
                    user.Id = (string)row["ID"];
                    user.FirstName = (string)row["First_Name"];
                    user.LastName = (string)row["Last_Name"];
                    user.MiddleName = (string)row["Middle_Name"];
                    user.InformalName = (string)row["INFORMAL"];
                    user.LegalLastName = (string)row["Legal_Last_Name"];
                    user.PreviousLegalLastName = (string)row["Legal_Previous_Last_Name"];
                    //user.CommonlyUsedLastName = (string)row["UF_3"];
                    user.Registration = (string)row["MAJOR_KEY"];
                    user.CurrentStatus = (string)row["STATUS_NAME"];
                    user.CurrentStatusCode = (string)row["STATUS"];
                    user.PreviousLastNames = GetUserPreviousNames((string)row["ID"]);
                    returnValue.Add(user);
                }
            }
            return returnValue;
        }

        public int FindUserInfoCount(string FirstName, string LastName, string Reg_Number, string City, string PostalCode, string Language,
            string FundSource, string MajorService, string ClientAge, string PracticeSetting, string Letter, out string sqlquery)
        {

            int returnValue = 0;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT COUNT(DISTINCT(n.ID)) AS [COUNT]" +
                " FROM	Name as n" +
                " LEFT OUTER  join Name_Address as na ON na.ID = n.ID  AND (na.PURPOSE = 'PRIMARY EMPLOYMENT' OR na.PURPOSE = 'SEC. EMPLOYMENT')" +
                " LEFT OUTER  join COTO_REGDATA as curr ON curr.ID = n.ID " +
                " LEFT OUTER  join ANN_REG_ADDL_INFO ON n.id = ANN_REG_ADDL_INFO.ID " +
                " LEFT OUTER  join EMPLOYMENT_ALL ON n.id = EMPLOYMENT_ALL.ID " +
                " LEFT OUTER  join gen_tables as gt ON gt.CODE = n.Status AND gt.TABLE_NAME = 'Member_Status'" +
                " WHERE " +
               " ( ( n.STATUS not in ('P','D') and n.member_type='reg'  ) OR ( n.STATUS  in ('AF','IF') and n.member_type='APP' ) )";


            if (!string.IsNullOrEmpty(Language))
            {
                sql += " AND ( " +
                " CURR.LANG_OF_SVCE_1 = " + Escape(Language) +
                " OR CURR.LANG_OF_SVCE_2 = " + Escape(Language) +
                " OR CURR.LANG_OF_SVCE_3 = " + Escape(Language) +
                " OR CURR.LANG_OF_SVCE_4 = " + Escape(Language) +
                " OR CURR.LANG_OF_SVCE_5 = " + Escape(Language) + ")";
            }

            if (!string.IsNullOrEmpty(Reg_Number))
            {
                sql += " AND N.Major_Key = @1 "; // +Escape(Reg_Number);
            }

            if (!string.IsNullOrEmpty(FundSource))
            {
                sql += " AND ( " +
                " EMPLOYMENT_ALL.EMP_1_FUNDING_SOURCE = " + Escape(FundSource) +
                " OR EMPLOYMENT_ALL.EMP_2_FUNDING_SOURCE = " + Escape(FundSource) +
                " OR EMPLOYMENT_ALL.EMP_3_FUNDING_SOURCE = " + Escape(FundSource) + ")";
            }

            if (!string.IsNullOrEmpty(MajorService))
            {
                sql += " AND ( " +
                " EMPLOYMENT_ALL.EMP_1_AREA_PRACTICE = " + Escape(MajorService) +
                " OR EMPLOYMENT_ALL.EMP_2_AREA_PRACTICE = " + Escape(MajorService) +
                " OR EMPLOYMENT_ALL.EMP_3_AREA_PRACTICE = " + Escape(MajorService) + ")";
            }

            if (!string.IsNullOrEmpty(ClientAge))
            {
                sql += " AND ( " +
                " EMPLOYMENT_ALL.EMP_1_CLNT_ARNGE = " + Escape(ClientAge) +
                " OR EMPLOYMENT_ALL.EMP_2_CLNT_ARNGE = " + Escape(ClientAge) +
                " OR EMPLOYMENT_ALL.EMP_3_CLNT_ARNGE = " + Escape(ClientAge) + ")";
            }

            if (!string.IsNullOrEmpty(PracticeSetting))
            {
                sql += " AND ( " +
                " EMPLOYMENT_ALL.EMP_1_TYPE = " + Escape(PracticeSetting) +
                " OR EMPLOYMENT_ALL.EMP_2_TYPE = " + Escape(PracticeSetting) +
                " OR EMPLOYMENT_ALL.EMP_3_TYPE = " + Escape(PracticeSetting) + ")";
            }

            if (!string.IsNullOrEmpty(FirstName))
            {
                sql += " AND (N.First_Name LIKE @2  OR N.INFORMAL LIKE @2 " +   // + Escape(FirstName + "%") +   //+ Escape(FirstName + "%")
                 " OR curr.LEGAL_FIRST_NAME LIKE @2 OR curr.LEGAL_PREVIOUS_FIRST_NAME LIKE @2 )"; // +Escape(FirstName + "%") + " )";  //+ Escape(FirstName + "%") +
            }

            if (!string.IsNullOrEmpty(LastName))
            {
                sql += " AND ( (N.Last_Name LIKE @3)";  //+ Escape(LastName + "%")
                sql += " OR (curr.Legal_Last_Name LIKE @3)"; //+ Escape(LastName + "%")
                sql += " OR (curr.LEGAL_PREVIOUS_LAST_NAME LIKE @3)";  //+ Escape(LastName + "%")
                sql += " OR N.ID IN (SELECT ID FROM Activity WHERE Activity.Activity_Type = 'L_P_NAME' AND Activity.UF_3 LIKE @3 ) )";  //+ Escape(LastName + "%")
            }

            if (!string.IsNullOrEmpty(City))
            {
                sql += " AND (EMPLOYMENT_ALL.EMP_1_CITY LIKE @4)";   // + Escape(City + "%") 
            }

            if (!string.IsNullOrEmpty(PostalCode))
            {
                sql += " AND (EMPLOYMENT_ALL.EMP_1_POSTAL LIKE @5 )";  // + Escape(PostalCode + "%")
            }

            if (!string.IsNullOrEmpty(Letter))
            {
                sql += " AND n.LAST_NAME LIKE " + Escape(Letter + "%");
            }

            sql += " AND n.ID NOT IN ('6969') ";

            var connection = new SqlConnection(dbconn);
            SqlCommand command = new SqlCommand(sql, connection);

            if (!string.IsNullOrEmpty(Reg_Number))
            {
                command.Parameters.Add(new SqlParameter("@1", SqlDbType.VarChar));
                command.Parameters["@1"].Value = Escape(Reg_Number);
            }

            if (!string.IsNullOrEmpty(FirstName))
            {
                command.Parameters.Add(new SqlParameter("@2", SqlDbType.VarChar));
                command.Parameters["@2"].Value = Escape(FirstName + "%");
            }

            if (!string.IsNullOrEmpty(LastName))
            {
                command.Parameters.Add(new SqlParameter("@3", SqlDbType.VarChar));
                command.Parameters["@3"].Value = Escape(LastName + "%");
            }

            if (!string.IsNullOrEmpty(City))
            {
                command.Parameters.Add(new SqlParameter("@4", SqlDbType.VarChar));
                command.Parameters["@4"].Value = Escape(City + "%");
            }

            if (!string.IsNullOrEmpty(PostalCode))
            {
                command.Parameters.Add(new SqlParameter("@5", SqlDbType.VarChar));
                command.Parameters["@5"].Value = Escape(PostalCode + "%");
            }

            sqlquery = "com-text: " + command.CommandText + ", sql: " + sql;

            var table = db.ExecuteReader(command);

            //var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                returnValue = (int)row["COUNT"];
            }
            return returnValue;
        }

        public int FindUserInfoCountImprv(string FirstName, string LastName, string Reg_Number, string City, string PostalCode, string Language,
            string FundSource, string MajorService, string ClientAge, string PracticeSetting, string Letter)
        {

            int returnValue = 0;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            using (SqlConnection myConnection = new SqlConnection(dbconn))
            {

                SqlCommand myCommand = new SqlCommand();
                myCommand.Connection = myConnection;
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "spVA_GetCountOTRegistrants";
                //var par1 = ;

                if (!string.IsNullOrEmpty(Language))
                {
                    myCommand.Parameters.Add(new SqlParameter("@Language", SqlDbType.VarChar, 5) { Value = Language });
                }

                if (!string.IsNullOrEmpty(FundSource))
                {
                    myCommand.Parameters.Add(new SqlParameter("@FundSource", SqlDbType.VarChar, 40) { Value = FundSource });
                }

                if (!string.IsNullOrEmpty(MajorService))
                {
                    myCommand.Parameters.Add(new SqlParameter("@MajorService", SqlDbType.VarChar, 60) { Value = MajorService });
                }

                if (!string.IsNullOrEmpty(ClientAge))
                {
                    myCommand.Parameters.Add(new SqlParameter("@ClientAge", SqlDbType.VarChar, 50) { Value = ClientAge });
                }

                if (!string.IsNullOrEmpty(PracticeSetting))
                {
                    myCommand.Parameters.Add(new SqlParameter("@PracticeSetting", SqlDbType.VarChar, 70) { Value = PracticeSetting });
                }

                if (!string.IsNullOrEmpty(Letter))
                {
                    Letter = Letter + "%";
                    myCommand.Parameters.Add(new SqlParameter("@Letter", SqlDbType.VarChar, 2) { Value = Letter });
                }

                if (!string.IsNullOrEmpty(Reg_Number))
                {
                    myCommand.Parameters.Add(new SqlParameter("@RegNumber", SqlDbType.VarChar, 15) { Value = Reg_Number });
                }

                if (!string.IsNullOrEmpty(FirstName))
                {
                    FirstName = FirstName + "%";
                    myCommand.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.VarChar, 30) { Value = FirstName });
                }

                if (!string.IsNullOrEmpty(LastName))
                {
                    LastName = LastName + "%";
                    myCommand.Parameters.Add(new SqlParameter("@LastName", SqlDbType.VarChar, 30) { Value = LastName });
                }

                if (!string.IsNullOrEmpty(City))
                {
                    City = City + "%";
                    myCommand.Parameters.Add(new SqlParameter("@City", SqlDbType.VarChar, 40) { Value = City });
                }

                if (!string.IsNullOrEmpty(PostalCode))
                {
                    PostalCode = PostalCode + "%";
                    myCommand.Parameters.Add(new SqlParameter("@PostalCode", SqlDbType.VarChar, 10) { Value = PostalCode });
                }

                var connection = new SqlConnection(dbconn);
                connection.Open();

                //To get Return value from StoreProcedure     
                myCommand.Parameters.Add("@RET_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                myConnection.Open();

                myCommand.ExecuteScalar();

                //Returnvalue from SP     

                returnValue = Convert.ToInt32(myCommand.Parameters["@RET_VALUE"].Value);
            }



            //var table = db.ExecuteReader(cmd);

            //var table = db.GetData(sql);
            //if (table != null)  // . && table.Rows.Count >0
            // {
            // DataRow row = table.Rows[0];
            // returnValue = (int)row["ROWS_COUNT"];
            //}

            //var ds = SQLHelper.ExecuteDataset(dbconn, "spVA_GetCountOTRegistrants", FirstName, LastName, Reg_Number, City, PostalCode, Language, 
            //    FundSource, MajorService, ClientAge,  PracticeSetting, Letter);

            //if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count == 1)
            //{
            //    returnValue = (int)ds.Tables[0].Rows[0].ItemArray[0];
            //}

            return returnValue;
        }

        public List<User> FindProfessionalCorporations()
        {
            List<User> returnValue = new List<User>();
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT distinct(n.ID), n.Company," +
                        " n.MAJOR_KEY, n.STATUS, gt.Description" +
                        " FROM Name as n " +
                        " LEFT OUTER  join gen_tables as gt ON gt.CODE = n.Status AND gt.TABLE_NAME = 'Member_Status'" +
                        " LEFT OUTER join Name_Address as na ON n.ID = na.ID" +
                        " WHERE n.ID = na.ID AND" +
                        " n.STATUS not in ('P','D','X')" +
                        " AND n.member_type='CORP'" +
                        " AND (na.PURPOSE = 'PRIMARY EMPLOYMENT' OR na.PURPOSE = 'SEC. EMPLOYMENT')	ORDER BY n.COMPANY";

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    var user = new User();
                    user.Id = (string)row["ID"];
                    user.FullName = (string)row["Company"];
                    user.Registration = (string)row["MAJOR_KEY"];
                    user.CurrentStatus = (string)row["Description"];

                    returnValue.Add(user);
                }
            }
            return returnValue;
        }

        public void UpdateUserProfessionalHistory(User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            if (user.ProfessionalHistoryInfo != null)
            {

                string sql_UpdateHist = "UPDATE ANN_REG_ADDL_INFO SET ";


                // first practise
                if (user.ProfessionalHistoryInfo.PracticeDetail1 != null)
                {
                    sql_UpdateHist += " OPROV_REG_BODY_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBody) + ", " +
                    "OPROV_OTHER_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyOther) + ", " +
                    "OPROV_PROV_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.Province) + ", " +
                    "OPROV_COUNTRY_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.Country) + ", " +
                    "OPROV_REG_LIC_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.License) + ", ";

                    if (user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate;
                        sql_UpdateHist += "OPROV_EXP_DATE_1 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OPROV_EXP_DATE_1 = NULL, ";
                    }
                }

                // second practise
                if (user.ProfessionalHistoryInfo.PracticeDetail2 != null)
                {
                    sql_UpdateHist += " OPROV_REG_BODY_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBody) + ", " +
                    "OPROV_OTHER_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyOther) + ", " +
                    "OPROV_PROV_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.Province) + ", " +
                    "OPROV_COUNTRY_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.Country) + ", " +
                    "OPROV_REG_LIC_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.License) + ", ";

                    if (user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate;
                        sql_UpdateHist += "OPROV_EXP_DATE_2 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OPROV_EXP_DATE_2 = NULL, ";
                    }
                }

                // third practise
                if (user.ProfessionalHistoryInfo.PracticeDetail3 != null)
                {
                    sql_UpdateHist += " OPROV_REG_BODY_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBody) + ", " +
                    "OPROV_OTHER_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyOther) + ", " +
                    "OPROV_PROV_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.Province) + ", " +
                    "OPROV_COUNTRY_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.Country) + ", " +
                    "OPROV_REG_LIC_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.License) + ", ";

                    if (user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate;
                        sql_UpdateHist += "OPROV_EXP_DATE_3 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OPROV_EXP_DATE_3 = NULL, ";
                    }
                }

                // fourth practise
                if (user.ProfessionalHistoryInfo.PracticeDetail4 != null)
                {
                    sql_UpdateHist += " OPROV_REG_BODY_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBody) + ", " +
                    "OPROV_OTHER_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyOther) + ", " +
                    "OPROV_PROV_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.Province) + ", " +
                    "OPROV_COUNTRY_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.Country) + ", " +
                    "OPROV_REG_LIC_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.License) + ", ";

                    if (user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate;
                        sql_UpdateHist += "OPROV_EXP_DATE_4 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OPROV_EXP_DATE_4 = NULL, ";
                    }
                }

                // first profession
                if (user.ProfessionalHistoryInfo.ProfessionDetail1 != null)
                {
                    sql_UpdateHist += " OTHER_PROF_PROF_NAME_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.ProfessionName) + ", " +
                    "OTHER_PROF_REG_BODY_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.RegulatoryBody) + ", " +
                    "OTHER_PROF_PROV_STATE_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.Province) + ", " +
                    "OTHER_PROF_COUNTRY_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.Country) + ", " +
                    "OTHER_PROF_REG_LICE_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.License) + ", ";

                    if (user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate;
                        sql_UpdateHist += "OTHER_PROF_EXP_DATE_1 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OTHER_PROF_EXP_DATE_1 = NULL, ";
                    }
                }

                // second profession
                if (user.ProfessionalHistoryInfo.ProfessionDetail2 != null)
                {
                    sql_UpdateHist += " OTHER_PROF_PROF_NAME_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.ProfessionName) + ", " +
                    "OTHER_PROF_REG_BODY_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.RegulatoryBody) + ", " +
                    "OTHER_PROF_PROV_STATE_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.Province) + ", " +
                    "OTHER_PROF_COUNTRY_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.Country) + ", " +
                    "OTHER_PROF_REG_LICE_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.License) + ", ";

                    if (user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate;
                        sql_UpdateHist += "OTHER_PROF_EXP_DATE_2 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OTHER_PROF_EXP_DATE_2 = NULL, ";
                    }
                }

                sql_UpdateHist += " OTHER_PROV_OT_PRACTICE = " + Escape(user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProvince) + ", " +
                    "OTHER_PROF_PRACTICE = " + Escape(user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProfession);
                sql_UpdateHist += " WHERE ID = " + Escape(user.Id);

                db.ExecuteCommand(sql_UpdateHist);
            }
        }

        public void UpdateUserProfessionalHistoryLogged(string UserID, User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            if (user.ProfessionalHistoryInfo != null)
            {
                string prevOtherProvince = string.Empty;
                string prevOtherProfession = string.Empty;

                string prevPract1RegBody = string.Empty;
                string prevPract1RegBodyOther = string.Empty;
                string prevPract1Province = string.Empty;
                string prevPract1Country = string.Empty;
                string prevPract1License = string.Empty;
                string prevPract1ExpireDate = string.Empty;

                string prevPract2RegBody = string.Empty;
                string prevPract2RegBodyOther = string.Empty;
                string prevPract2Province = string.Empty;
                string prevPract2Country = string.Empty;
                string prevPract2License = string.Empty;
                string prevPract2ExpireDate = string.Empty;

                string prevPract3RegBody = string.Empty;
                string prevPract3RegBodyOther = string.Empty;
                string prevPract3Province = string.Empty;
                string prevPract3Country = string.Empty;
                string prevPract3License = string.Empty;
                string prevPract3ExpireDate = string.Empty;

                string prevPract4RegBody = string.Empty;
                string prevPract4RegBodyOther = string.Empty;
                string prevPract4Province = string.Empty;
                string prevPract4Country = string.Empty;
                string prevPract4License = string.Empty;
                string prevPract4ExpireDate = string.Empty;

                string prevProf1RegBody = string.Empty;
                string prevProf1ProfName = string.Empty;
                string prevProf1Province = string.Empty;
                string prevProf1Country = string.Empty;
                string prevProf1License = string.Empty;
                string prevProf1ExpireDate = string.Empty;

                string prevProf2RegBody = string.Empty;
                string prevProf2ProfName = string.Empty;
                string prevProf2Province = string.Empty;
                string prevProf2Country = string.Empty;
                string prevProf2License = string.Empty;
                string prevProf2ExpireDate = string.Empty;

                string sql_GetCurrentHist = "SELECT * FROM ANN_REG_ADDL_INFO WHERE ID=" + Escape(UserID);

                var table = db.GetData(sql_GetCurrentHist);
                if (table != null && table.Rows.Count == 1)
                {
                    DataRow row = table.Rows[0];

                    prevOtherProvince = (string)row["OTHER_PROV_OT_PRACTICE"];
                    prevOtherProfession = (string)row["OTHER_PROF_PRACTICE"];

                    prevPract1RegBody = GetGeneralName("REG_BODIES", (string)row["OPROV_REG_BODY_1"]);
                    prevPract1RegBodyOther = (string)row["OPROV_OTHER_1"];
                    prevPract1Province = (string)row["OPROV_PROV_1"];
                    prevPract1Country = (string)row["OPROV_COUNTRY_1"];
                    prevPract1License = (string)row["OPROV_REG_LIC_1"];
                    if (row["OPROV_EXP_DATE_1"] != DBNull.Value)
                    {
                        prevPract1ExpireDate = ((DateTime)row["OPROV_EXP_DATE_1"]).ToString("MM/dd/yyyy");
                    }

                    prevPract2RegBody = GetGeneralName("REG_BODIES", (string)row["OPROV_REG_BODY_2"]);
                    prevPract2RegBodyOther = (string)row["OPROV_OTHER_2"];
                    prevPract2Province = (string)row["OPROV_PROV_2"];
                    prevPract2Country = (string)row["OPROV_COUNTRY_2"];
                    prevPract2License = (string)row["OPROV_REG_LIC_2"];
                    if (row["OPROV_EXP_DATE_2"] != DBNull.Value)
                    {
                        prevPract2ExpireDate = ((DateTime)row["OPROV_EXP_DATE_2"]).ToString("MM/dd/yyyy");
                    }

                    prevPract3RegBody = GetGeneralName("REG_BODIES", (string)row["OPROV_REG_BODY_3"]);
                    prevPract3RegBodyOther = (string)row["OPROV_OTHER_3"];
                    prevPract3Province = (string)row["OPROV_PROV_3"];
                    prevPract3Country = (string)row["OPROV_COUNTRY_3"];
                    prevPract3License = (string)row["OPROV_REG_LIC_3"];
                    if (row["OPROV_EXP_DATE_3"] != DBNull.Value)
                    {
                        prevPract3ExpireDate = ((DateTime)row["OPROV_EXP_DATE_3"]).ToString("MM/dd/yyyy");
                    }

                    prevPract4RegBody = GetGeneralName("REG_BODIES", (string)row["OPROV_REG_BODY_4"]);
                    prevPract4RegBodyOther = (string)row["OPROV_OTHER_4"];
                    prevPract4Province = (string)row["OPROV_PROV_4"];
                    prevPract4Country = (string)row["OPROV_COUNTRY_4"];
                    prevPract4License = (string)row["OPROV_REG_LIC_4"];
                    if (row["OPROV_EXP_DATE_4"] != DBNull.Value)
                    {
                        prevPract4ExpireDate = ((DateTime)row["OPROV_EXP_DATE_4"]).ToString("MM/dd/yyyy");
                    }

                    prevProf1RegBody = (string)row["OTHER_PROF_REG_BODY_1"];
                    prevProf1ProfName = (string)row["OTHER_PROF_PROF_NAME_1"];
                    prevProf1Province = (string)row["OTHER_PROF_PROV_STATE_1"];
                    prevProf1Country = (string)row["OTHER_PROF_COUNTRY_1"];
                    prevProf1License = (string)row["OTHER_PROF_REG_LICE_1"];
                    if (row["OTHER_PROF_EXP_DATE_1"] != DBNull.Value)
                    {
                        prevProf1ExpireDate = ((DateTime)row["OTHER_PROF_EXP_DATE_1"]).ToString("MM/dd/yyyy");
                    }

                    prevProf2RegBody = (string)row["OTHER_PROF_REG_BODY_2"];
                    prevProf2ProfName = (string)row["OTHER_PROF_PROF_NAME_2"];
                    prevProf2Province = (string)row["OTHER_PROF_PROV_STATE_2"];
                    prevProf2Country = (string)row["OTHER_PROF_COUNTRY_2"];
                    prevProf2License = (string)row["OTHER_PROF_REG_LICE_2"];
                    if (row["OTHER_PROF_EXP_DATE_2"] != DBNull.Value)
                    {
                        prevProf2ExpireDate = ((DateTime)row["OTHER_PROF_EXP_DATE_2"]).ToString("MM/dd/yyyy");
                    }
                }

                string sql_UpdateHist = "UPDATE ANN_REG_ADDL_INFO SET ";

                // first practise
                if (user.ProfessionalHistoryInfo.PracticeDetail1 != null)
                {
                    sql_UpdateHist += " OPROV_REG_BODY_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBody) + ", " +
                    "OPROV_OTHER_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyOther) + ", " +
                    "OPROV_PROV_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.Province) + ", " +
                    "OPROV_COUNTRY_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.Country) + ", " +
                    "OPROV_REG_LIC_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.License) + ", ";

                    if (user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate;
                        sql_UpdateHist += "OPROV_EXP_DATE_1 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OPROV_EXP_DATE_1 = NULL, ";
                    }
                    //prevPract1RegBody = (string)row["OPROV_REG_BODY_1"];
                    //prevPract1RegBodyOther = (string)row["OPROV_OTHER_1"];
                    //prevPract1Province = (string)row["OPROV_PROV_1"];
                    //prevPract1Country = (string)row["OPROV_COUNTRY_1"];
                    //prevPract1License = (string)row["OPROV_REG_LIC_1"];
                    //if (row["OPROV_EXP_DATE_1"] != DBNull.Value)
                    //{
                    //    prevPract1ExpireDate = ((DateTime)row["OPROV_EXP_DATE_1"]).ToString("MM/dd/yyyy");
                    //}

                    if (prevPract1RegBody != user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyText)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_BODY_1: " + prevPract1RegBody + " -> " + user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyText);
                    }
                    if (prevPract1RegBodyOther != user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyOther)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_OTHER_1: " + prevPract1RegBodyOther + " -> " + user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyOther);
                    }
                    if (prevPract1Province != user.ProfessionalHistoryInfo.PracticeDetail1.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_PROV_1: " + prevPract1Province + " -> " + user.ProfessionalHistoryInfo.PracticeDetail1.Province);
                    }
                    if (prevPract1Country != user.ProfessionalHistoryInfo.PracticeDetail1.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_COUNTRY_1: " + prevPract1Country + " -> " + user.ProfessionalHistoryInfo.PracticeDetail1.Country);
                    }
                    if (prevPract1License != user.ProfessionalHistoryInfo.PracticeDetail1.License)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_LIC_1: " + prevPract1License + " -> " + user.ProfessionalHistoryInfo.PracticeDetail1.License);
                    }
                    if (prevPract1ExpireDate != ((user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_EXP_DATE_1: " + prevPract1ExpireDate + " -> " + ((user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }
                }

                // second practise
                if (user.ProfessionalHistoryInfo.PracticeDetail2 != null)
                {
                    sql_UpdateHist += " OPROV_REG_BODY_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBody) + ", " +
                    "OPROV_OTHER_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyOther) + ", " +
                    "OPROV_PROV_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.Province) + ", " +
                    "OPROV_COUNTRY_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.Country) + ", " +
                    "OPROV_REG_LIC_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.License) + ", ";

                    if (user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate;
                        sql_UpdateHist += "OPROV_EXP_DATE_2 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OPROV_EXP_DATE_2 = NULL, ";
                    }

                    if (prevPract2RegBody != user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyText)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_BODY_2: " + prevPract2RegBody + " -> " + user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyText);
                    }
                    if (prevPract2RegBodyOther != user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyOther)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_OTHER_2: " + prevPract2RegBodyOther + " -> " + user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyOther);
                    }
                    if (prevPract2Province != user.ProfessionalHistoryInfo.PracticeDetail2.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_PROV_2: " + prevPract2Province + " -> " + user.ProfessionalHistoryInfo.PracticeDetail2.Province);
                    }
                    if (prevPract2Country != user.ProfessionalHistoryInfo.PracticeDetail2.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_COUNTRY_2: " + prevPract2Country + " -> " + user.ProfessionalHistoryInfo.PracticeDetail2.Country);
                    }
                    if (prevPract2License != user.ProfessionalHistoryInfo.PracticeDetail2.License)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_LIC_2: " + prevPract2License + " -> " + user.ProfessionalHistoryInfo.PracticeDetail2.License);
                    }
                    if (prevPract2ExpireDate != ((user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_EXP_DATE_2: " + prevPract2ExpireDate + " -> " + ((user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }
                }

                // third practise
                if (user.ProfessionalHistoryInfo.PracticeDetail3 != null)
                {
                    sql_UpdateHist += " OPROV_REG_BODY_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBody) + ", " +
                    "OPROV_OTHER_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyOther) + ", " +
                    "OPROV_PROV_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.Province) + ", " +
                    "OPROV_COUNTRY_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.Country) + ", " +
                    "OPROV_REG_LIC_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.License) + ", ";

                    if (user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate;
                        sql_UpdateHist += "OPROV_EXP_DATE_3 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OPROV_EXP_DATE_3 = NULL, ";
                    }

                    if (prevPract3RegBody != user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyText)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_BODY_3: " + prevPract3RegBody + " -> " + user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyText);
                    }
                    if (prevPract3RegBodyOther != user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyOther)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_OTHER_3: " + prevPract3RegBodyOther + " -> " + user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyOther);
                    }
                    if (prevPract3Province != user.ProfessionalHistoryInfo.PracticeDetail3.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_PROV_3: " + prevPract3Province + " -> " + user.ProfessionalHistoryInfo.PracticeDetail3.Province);
                    }
                    if (prevPract3Country != user.ProfessionalHistoryInfo.PracticeDetail3.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_COUNTRY_3: " + prevPract3Country + " -> " + user.ProfessionalHistoryInfo.PracticeDetail3.Country);
                    }
                    if (prevPract3License != user.ProfessionalHistoryInfo.PracticeDetail3.License)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_LIC_3: " + prevPract3License + " -> " + user.ProfessionalHistoryInfo.PracticeDetail3.License);
                    }
                    if (prevPract3ExpireDate != ((user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_EXP_DATE_3: " + prevPract3ExpireDate + " -> " + ((user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }

                }

                // fourth practise
                if (user.ProfessionalHistoryInfo.PracticeDetail4 != null)
                {
                    sql_UpdateHist += " OPROV_REG_BODY_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBody) + ", " +
                    "OPROV_OTHER_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyOther) + ", " +
                    "OPROV_PROV_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.Province) + ", " +
                    "OPROV_COUNTRY_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.Country) + ", " +
                    "OPROV_REG_LIC_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.License) + ", ";

                    if (user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate;
                        sql_UpdateHist += "OPROV_EXP_DATE_4 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OPROV_EXP_DATE_4 = NULL, ";
                    }

                    if (prevPract4RegBody != user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyText)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_BODY_4: " + prevPract4RegBody + " -> " + user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyText);
                    }
                    if (prevPract4RegBodyOther != user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyOther)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_OTHER_4: " + prevPract4RegBodyOther + " -> " + user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyOther);
                    }
                    if (prevPract4Province != user.ProfessionalHistoryInfo.PracticeDetail4.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_PROV_4: " + prevPract4Province + " -> " + user.ProfessionalHistoryInfo.PracticeDetail4.Province);
                    }
                    if (prevPract4Country != user.ProfessionalHistoryInfo.PracticeDetail4.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_COUNTRY_4: " + prevPract4Country + " -> " + user.ProfessionalHistoryInfo.PracticeDetail4.Country);
                    }
                    if (prevPract4License != user.ProfessionalHistoryInfo.PracticeDetail4.License)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_LIC_4: " + prevPract4License + " -> " + user.ProfessionalHistoryInfo.PracticeDetail4.License);
                    }
                    if (prevPract4ExpireDate != ((user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_EXP_DATE_4: " + prevPract4ExpireDate + " -> " + ((user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }
                }

                // first profession
                if (user.ProfessionalHistoryInfo.ProfessionDetail1 != null)
                {
                    sql_UpdateHist += " OTHER_PROF_PROF_NAME_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.ProfessionName) + ", " +
                    "OTHER_PROF_REG_BODY_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.RegulatoryBody) + ", " +
                    "OTHER_PROF_PROV_STATE_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.Province) + ", " +
                    "OTHER_PROF_COUNTRY_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.Country) + ", " +
                    "OTHER_PROF_REG_LICE_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.License) + ", ";

                    if (user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate;
                        sql_UpdateHist += "OTHER_PROF_EXP_DATE_1 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OTHER_PROF_EXP_DATE_1 = NULL, ";
                    }

                    if (prevProf1RegBody != user.ProfessionalHistoryInfo.ProfessionDetail1.RegulatoryBody)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_REG_BODY_1: " + prevProf1RegBody + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail1.RegulatoryBody);
                    }
                    if (prevProf1ProfName != user.ProfessionalHistoryInfo.ProfessionDetail1.ProfessionName)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_PROF_NAME_1: " + prevProf1ProfName + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail1.ProfessionName);
                    }
                    if (prevProf1Province != user.ProfessionalHistoryInfo.ProfessionDetail1.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_PROV_STATE_1: " + prevProf1Province + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail1.Province);
                    }
                    if (prevProf1Country != user.ProfessionalHistoryInfo.ProfessionDetail1.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_COUNTRY_1: " + prevProf1Country + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail1.Country);
                    }
                    if (prevProf1License != user.ProfessionalHistoryInfo.ProfessionDetail1.License)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_REG_LICE_1: " + prevProf1License + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail1.License);
                    }
                    if (prevProf1ExpireDate != ((user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_EXP_DATE_1: " + prevProf1ExpireDate + " -> " + ((user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }
                }

                // second profession
                if (user.ProfessionalHistoryInfo.ProfessionDetail2 != null)
                {
                    sql_UpdateHist += " OTHER_PROF_PROF_NAME_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.ProfessionName) + ", " +
                    "OTHER_PROF_REG_BODY_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.RegulatoryBody) + ", " +
                    "OTHER_PROF_PROV_STATE_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.Province) + ", " +
                    "OTHER_PROF_COUNTRY_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.Country) + ", " +
                    "OTHER_PROF_REG_LICE_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.License) + ", ";

                    if (user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate;
                        sql_UpdateHist += "OTHER_PROF_EXP_DATE_2 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OTHER_PROF_EXP_DATE_2 = NULL, ";
                    }

                    if (prevProf2RegBody != user.ProfessionalHistoryInfo.ProfessionDetail2.RegulatoryBody)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_REG_BODY_2: " + prevProf2RegBody + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail2.RegulatoryBody);
                    }
                    if (prevProf2ProfName != user.ProfessionalHistoryInfo.ProfessionDetail2.ProfessionName)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_PROF_NAME_2: " + prevProf2ProfName + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail2.ProfessionName);
                    }
                    if (prevProf2Province != user.ProfessionalHistoryInfo.ProfessionDetail2.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_PROV_STATE_2: " + prevProf2Province + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail2.Province);
                    }
                    if (prevProf2Country != user.ProfessionalHistoryInfo.ProfessionDetail2.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_COUNTRY_2: " + prevProf2Country + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail2.Country);
                    }
                    if (prevProf2License != user.ProfessionalHistoryInfo.ProfessionDetail2.License)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_REG_LICE_2: " + prevProf2License + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail2.License);
                    }
                    if (prevProf2ExpireDate != ((user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_EXP_DATE_2: " + prevProf2ExpireDate + " -> " + ((user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }
                }

                sql_UpdateHist += " OTHER_PROV_OT_PRACTICE = " + Escape(user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProvince) + ", " +
                    "OTHER_PROF_PRACTICE = " + Escape(user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProfession);
                sql_UpdateHist += " WHERE ID = " + Escape(user.Id);

                if (prevOtherProvince != user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProvince)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROV_OT_PRACTICE: " + prevOtherProvince + " -> " + user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProvince);
                }
                if (prevOtherProfession != user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProfession)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_PRACTICE: " + prevOtherProfession + " -> " + user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProfession);
                }

                db.ExecuteCommand(sql_UpdateHist);
            }
        }

        public void UpdateUserCurrencyLanguage(User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql_UpdateUser = "UPDATE COTO_REGDATA SET " +
                //" CURRENCY_DATA = '" + user.CurrencyLanguageServices.Currency + "', " +
                    " LANG_OF_SVCE_1 = '" + user.CurrencyLanguageServices.Lang_Service1 + "', " +
                    " LANG_OF_SVCE_2 = '" + user.CurrencyLanguageServices.Lang_Service2 + "', " +
                    " LANG_OF_SVCE_3 = '" + user.CurrencyLanguageServices.Lang_Service3 + "', " +
                    " LANG_OF_SVCE_4 = '" + user.CurrencyLanguageServices.Lang_Service4 + "', " +
                    " LANG_OF_SVCE_5 = '" + user.CurrencyLanguageServices.Lang_Service5 + "' " +
                    " WHERE ID = " + Escape(user.Id);
            db.ExecuteCommand(sql_UpdateUser);
        }

        public void UpdateUserCurrencyLanguageLogged(string UserID, User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string prevLangService1 = string.Empty;
            string prevLangService2 = string.Empty;
            string prevLangService3 = string.Empty;
            string prevLangService4 = string.Empty;
            string prevLangService5 = string.Empty;

            string sql_GetCurrentData = " SELECT * FROM COTO_REGDATA WHERE  ID = " + Escape(user.Id);
            //db.ExecuteCommand(sql_GetHomeAddress);

            var table = db.GetData(sql_GetCurrentData);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                prevLangService1 = (string)row["LANG_OF_SVCE_1"];
                prevLangService2 = (string)row["LANG_OF_SVCE_2"];
                prevLangService3 = (string)row["LANG_OF_SVCE_3"];
                prevLangService4 = (string)row["LANG_OF_SVCE_4"];
                prevLangService5 = (string)row["LANG_OF_SVCE_5"];
            }

            string sql_UpdateUser = "UPDATE COTO_REGDATA SET " +
                //" CURRENCY_DATA = '" + user.CurrencyLanguageServices.Currency + "', " +
                    " LANG_OF_SVCE_1 = '" + user.CurrencyLanguageServices.Lang_Service1 + "', " +
                    " LANG_OF_SVCE_2 = '" + user.CurrencyLanguageServices.Lang_Service2 + "', " +
                    " LANG_OF_SVCE_3 = '" + user.CurrencyLanguageServices.Lang_Service3 + "', " +
                    " LANG_OF_SVCE_4 = '" + user.CurrencyLanguageServices.Lang_Service4 + "', " +
                    " LANG_OF_SVCE_5 = '" + user.CurrencyLanguageServices.Lang_Service5 + "' " +
                    " WHERE ID = " + Escape(user.Id);
            db.ExecuteCommand(sql_UpdateUser);

            if (prevLangService1 != user.CurrencyLanguageServices.Lang_Service1)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LANG_OF_SVCE_1: " + prevLangService1 + " -> " + user.CurrencyLanguageServices.Lang_Service1);
            }

            if (prevLangService2 != user.CurrencyLanguageServices.Lang_Service2)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LANG_OF_SVCE_2: " + prevLangService2 + " -> " + user.CurrencyLanguageServices.Lang_Service2);
            }

            if (prevLangService3 != user.CurrencyLanguageServices.Lang_Service3)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LANG_OF_SVCE_3: " + prevLangService3 + " -> " + user.CurrencyLanguageServices.Lang_Service3);
            }

            if (prevLangService4 != user.CurrencyLanguageServices.Lang_Service4)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LANG_OF_SVCE_4: " + prevLangService4 + " -> " + user.CurrencyLanguageServices.Lang_Service4);
            }

            if (prevLangService5 != user.CurrencyLanguageServices.Lang_Service5)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LANG_OF_SVCE_5: " + prevLangService5 + " -> " + user.CurrencyLanguageServices.Lang_Service5);
            }
        }

        public void UpdateUserSelfIdentificationLogged(string UserID, User user)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string prevQuestion1 = string.Empty;
            string prevQuestion2 = string.Empty;

            string sql_GetCurrentData = " SELECT * FROM COTO_REGDATA WHERE  ID = " + Escape(user.Id);

            var table = db.GetData(sql_GetCurrentData);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                prevQuestion1 = (string)row["INDIGENOUS"];
                prevQuestion2 = (string)row["INDIGENOUS_CONTACT"];
            }

            string sql_UpdateUser = "UPDATE COTO_REGDATA SET " +
                    " INDIGENOUS = " + Escape(user.SelfIdentificationQuestion1) +
                    " , INDIGENOUS_CONTACT = " + Escape(user.SelfIdentificationQuestion2) +
                    " WHERE ID = " + Escape(user.Id);
            db.ExecuteCommand(sql_UpdateUser);

            if (prevQuestion1 != user.SelfIdentificationQuestion1)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.INDIGENOUS: " + prevQuestion1 + " -> " + user.SelfIdentificationQuestion1);
            }
            if (prevQuestion2 != user.SelfIdentificationQuestion2)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.INDIGENOUS_CONTACT: " + prevQuestion2 + " -> " + user.SelfIdentificationQuestion2);
            }
        }

        public void UpdateUserCurrencyResignCertificate(string User_ID, bool Acknowledgement, DateTime ReasonDate, string Reason, DateTime EffectiveDate)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql_UpdateUser = "UPDATE COTO_REGDATA SET " +
                " INACTIVE_ACKNOWLEDGMENT =	" + (Acknowledgement ? "1" : "0") + ", " +
                " INACTIVE_REASON_DATE = " + Escape(ReasonDate.ToString("yyyy/MM/dd")) + ", " +
                " INACTIVE_REASON = " + Escape(Reason) + ", " +
                " RESIGN_EFFECTIVE_DATE = " + Escape(EffectiveDate.ToString("yyyy/MM/dd")) +
                " WHERE ID = " + Escape(User_ID);
            db.ExecuteCommand(sql_UpdateUser);
        }

        public void UpdateUserCurrencyResignCertificateLogged(string User_ID, bool Acknowledgement, DateTime ReasonDate, string Reason, DateTime EffectiveDate)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string prevAcknowledgment = string.Empty;
            string prevReasonDate = string.Empty;
            string prevReason = string.Empty;
            string prevResignEffDate = string.Empty;

            string sql = "SELECT * FROM COTO_REGDATA WHERE ID = " + Escape(User_ID);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];
                prevAcknowledgment = (bool)row["INACTIVE_ACKNOWLEDGMENT"] ? "1" : "0";

                if (row["INACTIVE_REASON_DATE"] != DBNull.Value)
                {
                    prevReasonDate = ((DateTime)row["INACTIVE_REASON_DATE"]).ToString("yyyy/MM/dd");
                }
                prevReason = (string)row["INACTIVE_REASON"];

                if (row["RESIGN_EFFECTIVE_DATE"] != DBNull.Value)
                {
                    prevResignEffDate = ((DateTime)row["RESIGN_EFFECTIVE_DATE"]).ToString("yyyy/MM/dd");
                }
            }

            string sql_UpdateUser = "UPDATE COTO_REGDATA SET " +
                " INACTIVE_ACKNOWLEDGMENT =	" + (Acknowledgement ? "1" : "0") + ", " +
                " INACTIVE_REASON_DATE = " + Escape(ReasonDate.ToString("yyyy/MM/dd")) + ", " +
                " INACTIVE_REASON = " + Escape(Reason) + ", " +
                " RESIGN_EFFECTIVE_DATE = " + Escape(EffectiveDate.ToString("yyyy/MM/dd")) +
                " WHERE ID = " + Escape(User_ID);
            db.ExecuteCommand(sql_UpdateUser);

            if (prevAcknowledgment != (Acknowledgement ? "1" : "0"))
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", User_ID, User_ID, "COTO_REGDATA.INACTIVE_ACKNOWLEDGMENT: " + prevAcknowledgment + " -> " + (Acknowledgement ? "1" : "0"));
            }

            if (prevReasonDate != (ReasonDate.ToString("yyyy/MM/dd")))
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", User_ID, User_ID, "COTO_REGDATA.INACTIVE_REASON_DATE: " + prevReasonDate + " -> " + (ReasonDate.ToString("yyyy/MM/dd")));
            }

            if (prevReason != Reason)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", User_ID, User_ID, "COTO_REGDATA.INACTIVE_REASON: " + prevReason + " -> " + Reason);
            }

            if (prevResignEffDate != (EffectiveDate.ToString("yyyy/MM/dd")))
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", User_ID, User_ID, "COTO_REGDATA.RESIGN_EFFECTIVE_DATE: " + prevResignEffDate + " -> " + (EffectiveDate.ToString("yyyy/MM/dd")));
            }

        }

        public void UpdateUserCurrencyHoursInfo(User user, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT COUNT(1) FROM CURRENCY_HOURS  " +
                " WHERE ID = " + Escape(user.Id) + " AND REGISTRATION_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                if ((int)row[0] == 0)
                {
                    int newSEQN = GetCounter("CURRENCY_HOURS");
                    string sql_InsertUserCurrencyHours = "INSERT INTO CURRENCY_HOURS (ID, SEQN, REGISTRATION_YEAR, CURRENCY_REQ_MET)" +
                        " VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", '" + CurrentYear.ToString() + "', '" + user.CurrencyLanguageServices.Currency + "')";
                    db.ExecuteCommand(sql_InsertUserCurrencyHours);
                }
                else
                {
                    string sql_UpdateUserCurrencyHours = "UPDATE CURRENCY_HOURS SET " +
                    " CURRENCY_REQ_MET = '" + user.CurrencyLanguageServices.Currency + "' " +
                    " WHERE ID = " + Escape(user.Id) + " AND REGISTRATION_YEAR = '" + CurrentYear.ToString() + "'";
                    db.ExecuteCommand(sql_UpdateUserCurrencyHours);
                }
            }
        }

        public void UpdateUserCurrencyHoursInfoLogged(string UserID, User user, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT * FROM CURRENCY_HOURS  " +
                " WHERE ID = " + Escape(user.Id) + " AND REGISTRATION_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 0)
            {
                // creating new record
                int newSEQN = GetCounter("CURRENCY_HOURS");
                string sql_InsertUserCurrencyHours = "INSERT INTO CURRENCY_HOURS (ID, SEQN, REGISTRATION_YEAR, CURRENCY_REQ_MET)" +
                    " VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", '" + CurrentYear.ToString() + "', '" + user.CurrencyLanguageServices.Currency + "')";
                db.ExecuteCommand(sql_InsertUserCurrencyHours);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CURRENCY_HOURS.CURRENCY_REQ_MET (" + CurrentYear.ToString() + "): -> " + user.CurrencyLanguageServices.Currency);
            }
            else
            {
                // updating prev record
                var row = table.Rows[0];
                string prevCurrency = (string)row["CURRENCY_REQ_MET"];

                string sql_UpdateUserCurrencyHours = "UPDATE CURRENCY_HOURS SET " +
                    " CURRENCY_REQ_MET = '" + user.CurrencyLanguageServices.Currency + "' " +
                    " WHERE ID = " + Escape(user.Id) + " AND REGISTRATION_YEAR = '" + CurrentYear.ToString() + "'";
                db.ExecuteCommand(sql_UpdateUserCurrencyHours);

                if (prevCurrency != user.CurrencyLanguageServices.Currency)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CURRENCY_HOURS.CURRENCY_REQ_MET (" + CurrentYear.ToString() + "): " + prevCurrency + " -> " + user.CurrencyLanguageServices.Currency);
                }
            }
        }

        public void UpdateUserConductInfo(User user, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT COUNT(1) FROM CONDUCT_HISTORY  " +
                " WHERE ID = " + Escape(user.Id) + " AND RENEWAL_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                if ((int)row[0] == 0)
                {
                    int newSEQN = GetCounter("CONDUCT_HISTORY");
                    string sql_InsertUserConduct = "INSERT INTO CONDUCT_HISTORY (ID, SEQN, RENEWAL_YEAR, REFUSED_OT_OTHER, REFUSED_OT_OTHER_DETAILS, " +
                    " FINDING_OTHER_JURIS, FINDING_OTHER_JURIS_DETAILS, FINDING_OTHER_PROF, FINDING_OTHER_PROF_DETAILS, GUILTY_REL_OT, " +
                    " GUILTY_REL_OT_DETAILS, CRIMINAL_GUILT, CRIMINAL_GUILT_DETAILS, LACK_KNOW_SKILL_JUDG, LACK_KNOW_SKILL_JUDG_DETAILS, " +
                    " FINDING_NEG_MALPRACTICE, FINDING_NEG_MALPRACTICE_DETAIL) VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", '" + CurrentYear.ToString() + "', '" +
                    user.ConductInfo.Refused_OT_Other + "', '" + user.ConductInfo.Refused_OT_Other_Details + "', '" +
                    user.ConductInfo.Finding_Other_Juris + "', '" + user.ConductInfo.Finding_Other_Juris_Details + "', '" +
                    user.ConductInfo.Finding_Other_Prof + "', '" + user.ConductInfo.Finding_Other_Prof_Details + "', '" +
                    user.ConductInfo.Guilty_Rel_OT + "', '" + user.ConductInfo.Guilty_Rel_OT_Details + "', '" +
                    user.ConductInfo.Criminal_Guilt + "', '" + user.ConductInfo.Criminal_Guilt_Details + "', '" +
                    user.ConductInfo.Lack_Know_Skill_Judg + "', '" + user.ConductInfo.Lack_Know_Skill_Judg_Details + "', '" +
                    user.ConductInfo.Finding_Neg_Malpractice + "', '" + user.ConductInfo.Finding_Neg_Malpractice_Details + "') ";
                    db.ExecuteCommand(sql_InsertUserConduct);
                }
                else
                {
                    string sql_UpdateUserConduct = "UPDATE CONDUCT_HISTORY SET " +
                    " REFUSED_OT_OTHER = '" + user.ConductInfo.Refused_OT_Other + "', " +
                    " REFUSED_OT_OTHER_DETAILS = '" + user.ConductInfo.Refused_OT_Other_Details + "', " +

                    " FINDING_OTHER_JURIS = '" + user.ConductInfo.Finding_Other_Juris + "', " +
                    " FINDING_OTHER_JURIS_DETAILS = '" + user.ConductInfo.Finding_Other_Juris_Details + "', " +

                    " FINDING_OTHER_PROF = '" + user.ConductInfo.Finding_Other_Prof + "', " +
                    " FINDING_OTHER_PROF_DETAILS = '" + user.ConductInfo.Finding_Other_Prof_Details + "', " +

                    " GUILTY_REL_OT = '" + user.ConductInfo.Guilty_Rel_OT + "', " +
                    " GUILTY_REL_OT_DETAILS = '" + user.ConductInfo.Guilty_Rel_OT_Details + "', " +

                    " CRIMINAL_GUILT = '" + user.ConductInfo.Criminal_Guilt + "', " +
                    " CRIMINAL_GUILT_DETAILS = '" + user.ConductInfo.Criminal_Guilt_Details + "', " +

                    " LACK_KNOW_SKILL_JUDG = '" + user.ConductInfo.Lack_Know_Skill_Judg + "', " +
                    " LACK_KNOW_SKILL_JUDG_DETAILS = '" + user.ConductInfo.Lack_Know_Skill_Judg_Details + "', " +

                    " FINDING_NEG_MALPRACTICE = '" + user.ConductInfo.Finding_Neg_Malpractice + "', " +
                    " FINDING_NEG_MALPRACTICE_DETAIL = '" + user.ConductInfo.Finding_Neg_Malpractice_Details + "' " +

                    " WHERE ID = " + Escape(user.Id) + " AND RENEWAL_YEAR = '" + CurrentYear.ToString() + "'";
                    db.ExecuteCommand(sql_UpdateUserConduct);
                }
            }
        }

        public void UpdateUserConductInfoLogged(string UserID, User user, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT * FROM CONDUCT_HISTORY  " +
                " WHERE ID = " + Escape(user.Id) + " AND RENEWAL_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];
                string prevRefusedOther = (string)row["REFUSED_OT_OTHER"];
                string prevRefusedOtherDetails = (string)row["REFUSED_OT_OTHER_DETAILS"];
                string prevFindingJuris = (string)row["FINDING_OTHER_JURIS"];
                string prevFindingJurisDetails = (string)row["FINDING_OTHER_JURIS_DETAILS"];
                string prevFindingProf = (string)row["FINDING_OTHER_PROF"];
                string prevFindingProfDetails = (string)row["FINDING_OTHER_PROF_DETAILS"];
                //string prevGuiltyRel = (string)row["GUILTY_REL_OT"];
                //string prevGuiltyRelDetails = (string)row["GUILTY_REL_OT_DETAILS"];
                string prevCriminalGuilty = (string)row["CRIMINAL_GUILT"];
                string prevCriminalGuiltyDetails = (string)row["CRIMINAL_GUILT_DETAILS"];
                string prevLackKnowSkill = (string)row["LACK_KNOW_SKILL_JUDG"];
                string prevLackKnowSkillDetails = (string)row["LACK_KNOW_SKILL_JUDG_DETAILS"];
                string FindingMalpractice = (string)row["FINDING_NEG_MALPRACTICE"];
                string FindingMalpracticeDetails = (string)row["FINDING_NEG_MALPRACTICE_DETAIL"];
                string prevFacingProc = (string)row["FACING_PROC"];
                string prevFacingProcDetails = (string)row["FACING_PROC_DETAILS"];
                string prevFacingOtherProf = (string)row["FACING_OTHER_PROF"];
                string prevFacingOtherProfDetails = (string)row["FACING_OTHER_PROF_DETAILS"];

                string sql_UpdateUserConduct = "UPDATE CONDUCT_HISTORY SET " +
                " REFUSED_OT_OTHER = '" + user.ConductInfo.Refused_OT_Other + "', " +
                " REFUSED_OT_OTHER_DETAILS = '" + user.ConductInfo.Refused_OT_Other_Details + "', " +

                " FINDING_OTHER_JURIS = '" + user.ConductInfo.Finding_Other_Juris + "', " +
                " FINDING_OTHER_JURIS_DETAILS = '" + user.ConductInfo.Finding_Other_Juris_Details + "', " +

                " FINDING_OTHER_PROF = '" + user.ConductInfo.Finding_Other_Prof + "', " +
                " FINDING_OTHER_PROF_DETAILS = '" + user.ConductInfo.Finding_Other_Prof_Details + "', " +

                //" GUILTY_REL_OT = '" + user.ConductInfo.Guilty_Rel_OT + "', " +
                //" GUILTY_REL_OT_DETAILS = '" + user.ConductInfo.Guilty_Rel_OT_Details + "', " +

                " CRIMINAL_GUILT = '" + user.ConductInfo.Criminal_Guilt + "', " +
                " CRIMINAL_GUILT_DETAILS = '" + user.ConductInfo.Criminal_Guilt_Details + "', " +

                " LACK_KNOW_SKILL_JUDG = '" + user.ConductInfo.Lack_Know_Skill_Judg + "', " +
                " LACK_KNOW_SKILL_JUDG_DETAILS = '" + user.ConductInfo.Lack_Know_Skill_Judg_Details + "', " +

                " FINDING_NEG_MALPRACTICE = '" + user.ConductInfo.Finding_Neg_Malpractice + "', " +
                " FINDING_NEG_MALPRACTICE_DETAIL = '" + user.ConductInfo.Finding_Neg_Malpractice_Details + "', " +

                " FACING_PROC = '" + user.ConductInfo.Facing_Proc + "', " +
                " FACING_PROC_DETAILS = '" + user.ConductInfo.Facing_Proc_Details + "', " +

                " FACING_OTHER_PROF = '" + user.ConductInfo.Facing_Other_Prof + "', " +
                " FACING_OTHER_PROF_DETAILS = '" + user.ConductInfo.Facing_Other_Prof_Details + "' " +

                " WHERE ID = " + Escape(user.Id) + " AND RENEWAL_YEAR = '" + CurrentYear.ToString() + "'";
                db.ExecuteCommand(sql_UpdateUserConduct);

                if (prevRefusedOther != user.ConductInfo.Refused_OT_Other)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.REFUSED_OT_OTHER (" + CurrentYear.ToString() + "): " + prevRefusedOther + " -> " + user.ConductInfo.Refused_OT_Other);
                }
                if (prevRefusedOtherDetails != user.ConductInfo.Refused_OT_Other_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.REFUSED_OT_OTHER_DETAILS (" + CurrentYear.ToString() + "): " + prevRefusedOtherDetails + " -> " + user.ConductInfo.Refused_OT_Other_Details);
                }
                if (prevFindingJuris != user.ConductInfo.Finding_Other_Juris)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_JURIS (" + CurrentYear.ToString() + "): " + prevFindingJuris + " -> " + user.ConductInfo.Finding_Other_Juris);
                }
                if (prevFindingJurisDetails != user.ConductInfo.Finding_Other_Juris_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_JURIS_DETAILS (" + CurrentYear.ToString() + "): " + prevFindingJurisDetails + " -> " + user.ConductInfo.Finding_Other_Juris_Details);
                }
                if (prevFindingProf != user.ConductInfo.Finding_Other_Prof)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_PROF (" + CurrentYear.ToString() + "): " + prevFindingProf + " -> " + user.ConductInfo.Finding_Other_Prof);
                }
                if (prevFindingProfDetails != user.ConductInfo.Finding_Other_Prof_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_PROF_DETAILS (" + CurrentYear.ToString() + "): " + prevFindingProfDetails + " -> " + user.ConductInfo.Finding_Other_Prof_Details);
                }
                //if (prevGuiltyRel != user.ConductInfo.Guilty_Rel_OT)
                //{
                //    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.GUILTY_REL_OT (" + CurrentYear.ToString() + "): " + prevGuiltyRel + " -> " + user.ConductInfo.Guilty_Rel_OT);
                //}
                //if (prevGuiltyRelDetails != user.ConductInfo.Guilty_Rel_OT_Details)
                //{
                //    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.GUILTY_REL_OT_DETAILS (" + CurrentYear.ToString() + "): " + prevGuiltyRelDetails + " -> " + user.ConductInfo.Guilty_Rel_OT_Details);
                //}
                if (prevCriminalGuilty != user.ConductInfo.Criminal_Guilt)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.CRIMINAL_GUILT (" + CurrentYear.ToString() + "): " + prevCriminalGuilty + " -> " + user.ConductInfo.Criminal_Guilt);
                }
                if (prevCriminalGuiltyDetails != user.ConductInfo.Criminal_Guilt_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.CRIMINAL_GUILT_DETAILS (" + CurrentYear.ToString() + "): " + prevCriminalGuiltyDetails + " -> " + user.ConductInfo.Criminal_Guilt_Details);
                }
                if (prevLackKnowSkill != user.ConductInfo.Lack_Know_Skill_Judg)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.LACK_KNOW_SKILL_JUDG (" + CurrentYear.ToString() + "): " + prevLackKnowSkill + " -> " + user.ConductInfo.Lack_Know_Skill_Judg);
                }
                if (prevLackKnowSkillDetails != user.ConductInfo.Lack_Know_Skill_Judg_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.LACK_KNOW_SKILL_JUDG_DETAILS (" + CurrentYear.ToString() + "): " + prevLackKnowSkillDetails + " -> " + user.ConductInfo.Lack_Know_Skill_Judg_Details);
                }
                if (FindingMalpractice != user.ConductInfo.Finding_Neg_Malpractice)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_NEG_MALPRACTICE (" + CurrentYear.ToString() + "): " + FindingMalpractice + " -> " + user.ConductInfo.Finding_Neg_Malpractice);
                }
                if (FindingMalpracticeDetails != user.ConductInfo.Finding_Neg_Malpractice_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_NEG_MALPRACTICE_DETAIL (" + CurrentYear.ToString() + "): " + FindingMalpracticeDetails + " -> " + user.ConductInfo.Finding_Neg_Malpractice_Details);
                }
                if (prevFacingProc != user.ConductInfo.Facing_Proc)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FACING_PROC (" + CurrentYear.ToString() + "): " + prevFacingProc + " -> " + user.ConductInfo.Facing_Proc);
                }
                if (prevFacingProcDetails != user.ConductInfo.Facing_Proc_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FACING_PROC_DETAILS (" + CurrentYear.ToString() + "): " + prevFacingProcDetails + " -> " + user.ConductInfo.Facing_Proc_Details);
                }
                if (prevFacingOtherProf != user.ConductInfo.Facing_Other_Prof)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FACING_OTHER_PROF (" + CurrentYear.ToString() + "): " + prevFacingOtherProf + " -> " + user.ConductInfo.Facing_Other_Prof);
                }
                if (prevFacingOtherProfDetails != user.ConductInfo.Facing_Other_Prof_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FACING_OTHER_PROF_DETAILS (" + CurrentYear.ToString() + "): " + prevFacingOtherProfDetails + " -> " + user.ConductInfo.Facing_Other_Prof_Details);
                }
            }
            else
            {
                int newSEQN = GetCounter("CONDUCT_HISTORY");
                string sql_InsertUserConduct = "INSERT INTO CONDUCT_HISTORY (ID, SEQN, RENEWAL_YEAR, REFUSED_OT_OTHER, REFUSED_OT_OTHER_DETAILS, " +
                " FINDING_OTHER_JURIS, FINDING_OTHER_JURIS_DETAILS, FINDING_OTHER_PROF, FINDING_OTHER_PROF_DETAILS, CRIMINAL_GUILT, CRIMINAL_GUILT_DETAILS," +
                " LACK_KNOW_SKILL_JUDG, LACK_KNOW_SKILL_JUDG_DETAILS, FINDING_NEG_MALPRACTICE, FINDING_NEG_MALPRACTICE_DETAIL, " + 
                " FACING_PROC, FACING_PROC_DETAILS, FACING_OTHER_PROF, FACING_OTHER_PROF_DETAILS)" +
                "  VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", '" + CurrentYear.ToString() + "', '" +
                user.ConductInfo.Refused_OT_Other + "', '" + user.ConductInfo.Refused_OT_Other_Details + "', '" +
                user.ConductInfo.Finding_Other_Juris + "', '" + user.ConductInfo.Finding_Other_Juris_Details + "', '" +
                user.ConductInfo.Finding_Other_Prof + "', '" + user.ConductInfo.Finding_Other_Prof_Details + "', '" +
                //user.ConductInfo.Guilty_Rel_OT + "', '" + user.ConductInfo.Guilty_Rel_OT_Details + "', '" +
                user.ConductInfo.Criminal_Guilt + "', '" + user.ConductInfo.Criminal_Guilt_Details + "', '" +
                user.ConductInfo.Lack_Know_Skill_Judg + "', '" + user.ConductInfo.Lack_Know_Skill_Judg_Details + "', '" +
                user.ConductInfo.Finding_Neg_Malpractice + "', '" + user.ConductInfo.Finding_Neg_Malpractice_Details + "', " +
                Escape(user.ConductInfo.Facing_Proc) + ", " + Escape(user.ConductInfo.Facing_Proc_Details) + ", " +
                Escape(user.ConductInfo.Facing_Other_Prof) + ", " + Escape(user.ConductInfo.Facing_Other_Prof_Details) + ") ";
                db.ExecuteCommand(sql_InsertUserConduct);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.REFUSED_OT_OTHER (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Refused_OT_Other);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.REFUSED_OT_OTHER_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Refused_OT_Other_Details);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_JURIS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Finding_Other_Juris);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_JURIS_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Finding_Other_Juris_Details);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_PROF (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Finding_Other_Prof);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_PROF_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Finding_Other_Prof_Details);
                //AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.GUILTY_REL_OT (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Guilty_Rel_OT);
                //AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.GUILTY_REL_OT_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Guilty_Rel_OT_Details);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.CRIMINAL_GUILT (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Criminal_Guilt);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.CRIMINAL_GUILT_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Criminal_Guilt_Details);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.LACK_KNOW_SKILL_JUDG (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Lack_Know_Skill_Judg);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.LACK_KNOW_SKILL_JUDG_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Lack_Know_Skill_Judg_Details);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_NEG_MALPRACTICE (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Finding_Neg_Malpractice);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_NEG_MALPRACTICE_DETAIL (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Finding_Neg_Malpractice_Details);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FACING_PROC (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Facing_Proc);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FACING_PROC_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Facing_Proc_Details);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FACING_OTHER_PROF (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Facing_Other_Prof);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FACING_OTHER_PROF_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Facing_Other_Prof_Details);
            }
        }

        public void UpdateUserConductInfoLoggedPrev(string UserID, ConductNew conduct)
        {
            WebConfigItems.GetConnectionString();
            clsDB clsDb = new clsDB();
            if (conduct.SEQN > 0)
            {
                string sql = "SELECT * FROM COTO_CONDUCT   WHERE ID = " + this.Escape(UserID) + " AND SEQN = " + (object)conduct.SEQN;
                DataTable data = clsDb.GetData(sql);
                if (data != null && data.Rows.Count == 1)
                {
                    DataRow row = data.Rows[0];
                    string empty = string.Empty;
                    if (row["DATE_REPORTED"] != DBNull.Value)
                        empty = ((DateTime)row["DATE_REPORTED"]).ToString("yyyy-MM-dd");
                    string str1 = (string)row["REFUSED_BY_REG_BODY"];
                    string str2 = row["REFUSED_BY_REG_BODY_DETAILS"] != DBNull.Value ? (string)row["REFUSED_BY_REG_BODY_DETAILS"] : string.Empty;

                    string str3 = (string)row["FIND_MISCOND_INCOMP_INCAP"];
                    string str4 = row["FIND_MISCON_INCOMP_INCAP_DETA"] != DBNull.Value ? (string)row["FIND_MISCON_INCOMP_INCAP_DETA"] : string.Empty;

                    string str5 = (string)row["FACING_MISCON_INCOMP_INCAP"];
                    string str6 = row["FACING_MISCON_INCOMP_INCAP_DET"] != DBNull.Value ? (string)row["FACING_MISCON_INCOMP_INCAP_DET"] : string.Empty;

                    string str7 = (string)row["FIND_NEG_MALPRACT"];
                    string str8 = row["FIND_NEG_MALPRACT_DETAILS"] != DBNull.Value ? (string)row["FIND_NEG_MALPRACT_DETAILS"] : string.Empty;

                    string str9 = (string)row["PREVIOUS_CONDUCT"];
                    string str10 = row["PREVIOUS_CONDUCT_DETAILS"]!=DBNull.Value ? (string)row["PREVIOUS_CONDUCT_DETAILS"] : string.Empty;

                    string str11 = (string)row["GUILTY_AUTHORITY_OFFENCE"];
                    string str12 = row["GUILTY_AUTH_OFFENCE_DETAILS"] != DBNull.Value ? (string)row["GUILTY_AUTH_OFFENCE_DETAILS"] : string.Empty;

                    string str13 = (string)row["COND_RESTRICT"];
                    string str14 = row["COND_RESTRICT_DETAILS"] != DBNull.Value ? (string)row["COND_RESTRICT_DETAILS"] : string.Empty;

                    string strSQL = "UPDATE COTO_CONDUCT SET  DATE_REPORTED = " + this.Escape(conduct.ReportedDate.ToString("yyyy-MM-dd")) + ",  REFUSED_BY_REG_BODY = " + this.Escape(conduct.RefusedByRegBody) + ",  REFUSED_BY_REG_BODY_DETAILS = " + this.Escape(conduct.RefusedByRegBodyDetails) + ",  FIND_MISCOND_INCOMP_INCAP = " + this.Escape(conduct.FindMisconductIncomp) + ",  FIND_MISCON_INCOMP_INCAP_DETA = " + this.Escape(conduct.FindMisconductIncompDetails) + ",  FACING_MISCON_INCOMP_INCAP = " + this.Escape(conduct.FacingMisconduct) + ",  FACING_MISCON_INCOMP_INCAP_DET = " + this.Escape(conduct.FacingMisconductDetails) + ",  FIND_NEG_MALPRACT = " + this.Escape(conduct.FindNegMalpract) + ",  FIND_NEG_MALPRACT_DETAILS = " + this.Escape(conduct.FindNegMalpractDetails) + ",  PREVIOUS_CONDUCT = " + this.Escape(conduct.PrevConduct) + ",  PREVIOUS_CONDUCT_DETAILS = " + this.Escape(conduct.PrevConductDetails) + ",  GUILTY_AUTHORITY_OFFENCE = " + this.Escape(conduct.Guilty_Authority) + ",  GUILTY_AUTH_OFFENCE_DETAILS = " + this.Escape(conduct.Guilty_Authority_Details) + ",  COND_RESTRICT = " + this.Escape(conduct.CondRestrict) + ",  COND_RESTRICT_DETAILS = " + this.Escape(conduct.CondRestrictDetails) + " WHERE ID = " + this.Escape(UserID) + " AND SEQN = " + (object)conduct.SEQN;
                    clsDb.ExecuteCommand(strSQL);
                    string str15 = empty;
                    DateTime reportedDate = conduct.ReportedDate;
                    string str16 = reportedDate.ToString("yyyy-MM-dd");
                    if (str15 != str16)
                    {
                        DateTime now = DateTime.Now;
                        string Log_type = "CHANGE";
                        string SubType = "CHANGE";
                        string UserId = UserID;
                        string ID = UserID;
                        object[] objArray = new object[7]
                        {
              (object) "COTO_CONDUCT.DATE_REPORTED (",
              (object) conduct.SEQN,
              (object) "): '",
              (object) empty,
              (object) "' -> '",
              null,
              null
                        };
                        int index = 5;
                        reportedDate = conduct.ReportedDate;
                        string str17 = reportedDate.ToString("yyyy-MM-dd");
                        objArray[index] = (object)str17;
                        objArray[6] = (object)"'";
                        string LogText = string.Concat(objArray);
                        this.AddLogEntry(now, Log_type, SubType, UserId, ID, LogText);
                    }
                    if (str1 != conduct.RefusedByRegBody)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY (" + (object)conduct.SEQN + "): " + str1 + " -> " + conduct.RefusedByRegBody);
                    if (str2 != conduct.RefusedByRegBodyDetails)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY_DETAILS (" + (object)conduct.SEQN + "): " + str2 + " -> " + conduct.RefusedByRegBodyDetails);
                    if (str3 != conduct.FindMisconductIncomp)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCOND_INCOMP_INCAP (" + (object)conduct.SEQN + "): " + str3 + " -> " + conduct.FindMisconductIncomp);
                    if (str4 != conduct.FindMisconductIncompDetails)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCON_INCOMP_INCAP_DETA (" + (object)conduct.SEQN + "): " + str4 + " -> " + conduct.FindMisconductIncompDetails);
                    if (str5 != conduct.FacingMisconduct)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP (" + (object)conduct.SEQN + "): " + str5 + " -> " + conduct.FacingMisconduct);
                    if (str6 != conduct.FacingMisconductDetails)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP_DET (" + (object)conduct.SEQN + "): " + str6 + " -> " + conduct.FacingMisconductDetails);
                    if (str7 != conduct.FindNegMalpract)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT (" + (object)conduct.SEQN + "): " + str7 + " -> " + conduct.FindNegMalpract);
                    if (str8 != conduct.FindNegMalpractDetails)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT_DETAILS (" + (object)conduct.SEQN + "): " + str8 + " -> " + conduct.FindNegMalpractDetails);
                    if (str9 != conduct.PrevConduct)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.PREVIOUS_CONDUCT (" + (object)conduct.SEQN + "): " + str9 + " -> " + conduct.PrevConduct);
                    if (str10 != conduct.PrevConductDetails)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.PREVIOUS_CONDUCT_DETAILS (" + (object)conduct.SEQN + "): " + str10 + " -> " + conduct.PrevConductDetails);
                    if (str11 != conduct.Guilty_Authority)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTHORITY_OFFENCE (" + (object)conduct.SEQN + "): " + str11 + " -> " + conduct.Guilty_Authority);
                    if (str12 != conduct.Guilty_Authority_Details)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTH_OFFENCE_DETAILS (" + (object)conduct.SEQN + "): " + str12 + " -> " + conduct.Guilty_Authority_Details);
                    if (str13 != conduct.CondRestrict)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT (" + (object)conduct.SEQN + "): " + str13 + " -> " + conduct.CondRestrict);
                    if (str14 != conduct.CondRestrictDetails)
                        this.AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT_DETAIL (" + (object)conduct.SEQN + "): " + str14 + " -> " + conduct.CondRestrictDetails);
                }
                else
                {
                    int counter = this.GetCounter("COTO_CONDUCT");
                    string[] strArray1 = new string[35];
                    strArray1[0] = "INSERT INTO COTO_CONDUCT ([ID],[SEQN],[DATE_REPORTED],[REFUSED_BY_REG_BODY],[REFUSED_BY_REG_BODY_DETAILS], [FIND_MISCOND_INCOMP_INCAP],[FIND_MISCON_INCOMP_INCAP_DETA],[FACING_MISCON_INCOMP_INCAP],[FACING_MISCON_INCOMP_INCAP_DET], [FIND_NEG_MALPRACT],[FIND_NEG_MALPRACT_DETAILS],[PREVIOUS_CONDUCT],[PREVIOUS_CONDUCT_DETAILS],[GUILTY_AUTHORITY_OFFENCE], [GUILTY_AUTH_OFFENCE_DETAILS],[COND_RESTRICT],[COND_RESTRICT_DETAILS])  VALUES (";
                    strArray1[1] = this.Escape(UserID);
                    strArray1[2] = ", ";
                    strArray1[3] = counter.ToString();
                    strArray1[4] = ", ";
                    int index1 = 5;
                    DateTime reportedDate = conduct.ReportedDate;
                    string str1 = this.Escape(reportedDate.ToString("yyyy-MM-dd"));
                    strArray1[index1] = str1;
                    strArray1[6] = ", ";
                    strArray1[7] = this.Escape(conduct.RefusedByRegBody);
                    strArray1[8] = ", ";
                    strArray1[9] = this.Escape(conduct.RefusedByRegBodyDetails);
                    strArray1[10] = ", ";
                    strArray1[11] = this.Escape(conduct.FindMisconductIncomp);
                    strArray1[12] = ", ";
                    strArray1[13] = this.Escape(conduct.FindMisconductIncompDetails);
                    strArray1[14] = ", ";
                    strArray1[15] = this.Escape(conduct.FacingMisconduct);
                    strArray1[16] = ", ";
                    strArray1[17] = this.Escape(conduct.FacingMisconductDetails);
                    strArray1[18] = ", ";
                    strArray1[19] = this.Escape(conduct.FindNegMalpract);
                    strArray1[20] = ", ";
                    strArray1[21] = this.Escape(conduct.FindNegMalpractDetails);
                    strArray1[22] = ", ";
                    strArray1[23] = this.Escape(conduct.PrevConduct);
                    strArray1[24] = ", ";
                    strArray1[25] = this.Escape(conduct.PrevConductDetails);
                    strArray1[26] = ", ";
                    strArray1[27] = this.Escape(conduct.Guilty_Authority);
                    strArray1[28] = ", ";
                    strArray1[29] = this.Escape(conduct.Guilty_Authority_Details);
                    strArray1[30] = ", ";
                    strArray1[31] = this.Escape(conduct.CondRestrict);
                    strArray1[32] = ", ";
                    strArray1[33] = this.Escape(conduct.CondRestrictDetails);
                    strArray1[34] = ")";
                    string strSQL = string.Concat(strArray1);
                    clsDb.ExecuteCommand(strSQL);
                    DateTime now = DateTime.Now;
                    string Log_type = "ADD";
                    string SubType = "CHANGE";
                    string UserId = UserID;
                    string ID = UserID;
                    string[] strArray2 = new string[5]
                    {
            "COTO_CONDUCT.DATE_REPORTED (",
            counter.ToString(),
            "):  -> '",
            null,
            null
                    };
                    int index2 = 3;
                    reportedDate = conduct.ReportedDate;
                    string str2 = reportedDate.ToString("yyyy-MM-dd");
                    strArray2[index2] = str2;
                    strArray2[4] = "'";
                    string LogText = string.Concat(strArray2);
                    this.AddLogEntry(now, Log_type, SubType, UserId, ID, LogText);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY (" + counter.ToString() + "):  -> " + conduct.RefusedByRegBody);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY_DETAILS (" + counter.ToString() + "):  -> " + conduct.RefusedByRegBodyDetails);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCOND_INCOMP_INCAP (" + counter.ToString() + "):  -> " + conduct.FindMisconductIncomp);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCON_INCOMP_INCAP_DETA (" + counter.ToString() + "):  -> " + conduct.FindMisconductIncompDetails);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP (" + counter.ToString() + "):  -> " + conduct.FacingMisconduct);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP_DET (" + counter.ToString() + "):  -> " + conduct.FacingMisconductDetails);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT (" + counter.ToString() + "):  -> " + conduct.FindNegMalpract);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT_DETAILS (" + counter.ToString() + "):  -> " + conduct.FindNegMalpractDetails);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.PREVIOUS_CONDUCT (" + counter.ToString() + "):  -> " + conduct.PrevConduct);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.PREVIOUS_CONDUCT_DETAILS (" + counter.ToString() + "):  -> " + conduct.PrevConductDetails);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTHORITY_OFFENCE (" + counter.ToString() + "):  -> " + conduct.Guilty_Authority);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTH_OFFENCE_DETAILS (" + counter.ToString() + "):  -> " + conduct.Guilty_Authority_Details);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT (" + counter.ToString() + "):  -> " + conduct.CondRestrict);
                    this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT_DETAIL (" + counter.ToString() + "):  -> " + conduct.CondRestrictDetails);
                }
            }
            else
            {
                int counter = this.GetCounter("COTO_CONDUCT");
                string[] strArray1 = new string[35];
                strArray1[0] = "INSERT INTO COTO_CONDUCT ([ID],[SEQN],[DATE_REPORTED],[REFUSED_BY_REG_BODY],[REFUSED_BY_REG_BODY_DETAILS], [FIND_MISCOND_INCOMP_INCAP],[FIND_MISCON_INCOMP_INCAP_DETA],[FACING_MISCON_INCOMP_INCAP],[FACING_MISCON_INCOMP_INCAP_DET], [FIND_NEG_MALPRACT],[FIND_NEG_MALPRACT_DETAILS],[PREVIOUS_CONDUCT],[PREVIOUS_CONDUCT_DETAILS],[GUILTY_AUTHORITY_OFFENCE], [GUILTY_AUTH_OFFENCE_DETAILS],[COND_RESTRICT],[COND_RESTRICT_DETAILS])  VALUES (";
                strArray1[1] = this.Escape(UserID);
                strArray1[2] = ", ";
                strArray1[3] = counter.ToString();
                strArray1[4] = ", ";
                int index1 = 5;
                DateTime reportedDate = conduct.ReportedDate;
                string str1 = this.Escape(reportedDate.ToString("yyyy-MM-dd"));
                strArray1[index1] = str1;
                strArray1[6] = ", ";
                strArray1[7] = this.Escape(conduct.RefusedByRegBody);
                strArray1[8] = ", ";
                strArray1[9] = this.Escape(conduct.RefusedByRegBodyDetails);
                strArray1[10] = ", ";
                strArray1[11] = this.Escape(conduct.FindMisconductIncomp);
                strArray1[12] = ", ";
                strArray1[13] = this.Escape(conduct.FindMisconductIncompDetails);
                strArray1[14] = ", ";
                strArray1[15] = this.Escape(conduct.FacingMisconduct);
                strArray1[16] = ", ";
                strArray1[17] = this.Escape(conduct.FacingMisconductDetails);
                strArray1[18] = ", ";
                strArray1[19] = this.Escape(conduct.FindNegMalpract);
                strArray1[20] = ", ";
                strArray1[21] = this.Escape(conduct.FindNegMalpractDetails);
                strArray1[22] = ", ";
                strArray1[23] = this.Escape(conduct.PrevConduct);
                strArray1[24] = ", ";
                strArray1[25] = this.Escape(conduct.PrevConductDetails);
                strArray1[26] = ", ";
                strArray1[27] = this.Escape(conduct.Guilty_Authority);
                strArray1[28] = ", ";
                strArray1[29] = this.Escape(conduct.Guilty_Authority_Details);
                strArray1[30] = ", ";
                strArray1[31] = this.Escape(conduct.CondRestrict);
                strArray1[32] = ", ";
                strArray1[33] = this.Escape(conduct.CondRestrictDetails);
                strArray1[34] = ")";
                string strSQL = string.Concat(strArray1);
                clsDb.ExecuteCommand(strSQL);
                DateTime now = DateTime.Now;
                string Log_type = "ADD";
                string SubType = "CHANGE";
                string UserId = UserID;
                string ID = UserID;
                string[] strArray2 = new string[5]
                {
          "COTO_CONDUCT.DATE_REPORTED (",
          counter.ToString(),
          "):  -> '",
          null,
          null
                };
                int index2 = 3;
                reportedDate = conduct.ReportedDate;
                string str2 = reportedDate.ToString("yyyy-MM-dd");
                strArray2[index2] = str2;
                strArray2[4] = "'";
                string LogText = string.Concat(strArray2);
                this.AddLogEntry(now, Log_type, SubType, UserId, ID, LogText);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY (" + counter.ToString() + "):  -> " + conduct.RefusedByRegBody);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY_DETAILS (" + counter.ToString() + "):  -> " + conduct.RefusedByRegBodyDetails);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCOND_INCOMP_INCAP (" + counter.ToString() + "):  -> " + conduct.FindMisconductIncomp);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCON_INCOMP_INCAP_DETA (" + counter.ToString() + "):  -> " + conduct.FindMisconductIncompDetails);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP (" + counter.ToString() + "):  -> " + conduct.FacingMisconduct);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP_DET (" + counter.ToString() + "):  -> " + conduct.FacingMisconductDetails);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT (" + counter.ToString() + "):  -> " + conduct.FindNegMalpract);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT_DETAILS (" + counter.ToString() + "):  -> " + conduct.FindNegMalpractDetails);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.PREVIOUS_CONDUCT (" + counter.ToString() + "):  -> " + conduct.PrevConduct);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.PREVIOUS_CONDUCT_DETAILS (" + counter.ToString() + "):  -> " + conduct.PrevConductDetails);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTHORITY_OFFENCE (" + counter.ToString() + "):  -> " + conduct.Guilty_Authority);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTH_OFFENCE_DETAILS (" + counter.ToString() + "):  -> " + conduct.Guilty_Authority_Details);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT (" + counter.ToString() + "):  -> " + conduct.CondRestrict);
                this.AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT_DETAIL (" + counter.ToString() + "):  -> " + conduct.CondRestrictDetails);
            }
            ApplicationStatus application = new ApplicationStatus();
            application.ConductSubmitted = conduct.ReportedDate;
            this.UpdateApplicationConductSubmitted(UserID, application);
        }

        public void UpdateUserConductInfoLoggedNew(string UserID, ConductNew conduct)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            if (conduct.SEQN > 0 )
            {
                string sql = "SELECT * FROM COTO_CONDUCT  " +
                " WHERE ID = " + Escape(UserID) + " AND SEQN = " + conduct.SEQN;
                var table = db.GetData(sql);
                if (table != null && table.Rows.Count == 1)
                {
                    var row = table.Rows[0];
                    string prevReportedDate = string.Empty;
                    if (row["DATE_REPORTED"] != DBNull.Value) prevReportedDate = ((DateTime)row["DATE_REPORTED"]).ToString("yyyy-MM-dd");

                    string prevRefusedByRegBody = (string)row["REFUSED_BY_REG_BODY"];
                    string prevRefusedByRegBodyDetails = row["REFUSED_BY_REG_BODY_DETAILS"] != DBNull.Value ? (string)row["REFUSED_BY_REG_BODY_DETAILS"] : string.Empty;

                    string prevFindMisconductIncomp = (string)row["FIND_MISCOND_INCOMP_INCAP"];
                    string prevFindMisconductIncompDetails = row["FIND_MISCON_INCOMP_INCAP_DETA"] != DBNull.Value ? (string)row["FIND_MISCON_INCOMP_INCAP_DETA"] : string.Empty;

                    string prevFacingMisconduct = (string)row["FACING_MISCON_INCOMP_INCAP"];
                    string prevFacingMisconductDetails = row["FACING_MISCON_INCOMP_INCAP_DET"] != DBNull.Value ? (string)row["FACING_MISCON_INCOMP_INCAP_DET"] : string.Empty;

                    string prevFindNegMalpract = (string)row["FIND_NEG_MALPRACT"];
                    string prevFindNegMalpractDetails = row["FIND_NEG_MALPRACT_DETAILS"] != DBNull.Value ? (string)row["FIND_NEG_MALPRACT_DETAILS"] : string.Empty;

                    string prevChangeOffence = (string)row["CHARGED_OFFENCE"];
                    string prevChangeOffenceDetails = row["CHARGED_OFFENCE_DETAILS"] !=DBNull.Value? (string)row["CHARGED_OFFENCE_DETAILS"]: string.Empty;

                    string prevCondRestrict = (string)row["COND_RESTRICT"];
                    string prevCondRestrictDetails = row["COND_RESTRICT_DETAILS"] != DBNull.Value ? (string)row["COND_RESTRICT_DETAILS"] : string.Empty;

                    string prevGuilty_Authority = (string)row["GUILTY_AUTHORITY_OFFENCE"];
                    string prevGuilty_Authority_Details = row["GUILTY_AUTH_OFFENCE_DETAILS"] != DBNull.Value ? (string)row["GUILTY_AUTH_OFFENCE_DETAILS"] : string.Empty;

                    string prevEvent_Circum = (string)row["EVENT_CIRCUMSTANCE"];
                    string prevEvent_Circum_Details = row["EVENT_CIRCUMSTANCE_DETAILS"] !=DBNull.Value? (string)row["EVENT_CIRCUMSTANCE_DETAILS"]: string.Empty;


                    string sql_UpdateUserConduct = "UPDATE COTO_CONDUCT SET " +
                    " DATE_REPORTED = " + Escape(conduct.ReportedDate.ToString("yyyy-MM-dd")) + ", " +
                    " REFUSED_BY_REG_BODY = " + Escape(conduct.RefusedByRegBody) + ", " +
                    " REFUSED_BY_REG_BODY_DETAILS = " + Escape(conduct.RefusedByRegBodyDetails) + ", " +

                    " FIND_MISCOND_INCOMP_INCAP = " + Escape(conduct.FindMisconductIncomp) + ", " +
                    " FIND_MISCON_INCOMP_INCAP_DETA = " + Escape(conduct.FindMisconductIncompDetails) + ", " +

                    " FACING_MISCON_INCOMP_INCAP = " + Escape(conduct.FacingMisconduct) + ", " +
                    " FACING_MISCON_INCOMP_INCAP_DET = " + Escape(conduct.FacingMisconductDetails) + ", " +

                    " FIND_NEG_MALPRACT = " + Escape(conduct.FindNegMalpract) + ", " +
                    " FIND_NEG_MALPRACT_DETAILS = " + Escape(conduct.FindNegMalpractDetails) + ", " +

                    " CHARGED_OFFENCE = " + Escape(conduct.ChargedOffence) + ", " +
                    " CHARGED_OFFENCE_DETAILS = " + Escape(conduct.ChargedOffenceDetails) + ", " +

                    " COND_RESTRICT = " + Escape(conduct.CondRestrict) + ", " +
                    " COND_RESTRICT_DETAILS = " + Escape(conduct.CondRestrictDetails) + ", " +

                    " GUILTY_AUTHORITY_OFFENCE = " + Escape(conduct.Guilty_Authority) + ", " +
                    " GUILTY_AUTH_OFFENCE_DETAILS = " + Escape(conduct.Guilty_Authority_Details) + ", " +

                    " EVENT_CIRCUMSTANCE = " + Escape(conduct.EventCircumstance) + ", " +
                    " EVENT_CIRCUMSTANCE_DETAILS = " + Escape(conduct.EventCircumstanceDetails) + 

                    " WHERE ID = " + Escape(UserID) + " AND SEQN = " + conduct.SEQN;

                    db.ExecuteCommand(sql_UpdateUserConduct);

                    if (prevReportedDate != conduct.ReportedDate.ToString("yyyy-MM-dd"))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.DATE_REPORTED (" + conduct.SEQN + "): '" + prevReportedDate + "' -> '" + conduct.ReportedDate.ToString("yyyy-MM-dd") + "'");
                    }
                    if (prevRefusedByRegBody != conduct.RefusedByRegBody)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY (" + conduct.SEQN + "): " + prevRefusedByRegBody + " -> " + conduct.RefusedByRegBody);
                    }
                    if (prevRefusedByRegBodyDetails != conduct.RefusedByRegBodyDetails)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY_DETAILS (" + conduct.SEQN + "): " + prevRefusedByRegBodyDetails + " -> " + conduct.RefusedByRegBodyDetails);
                    }
                    if (prevFindMisconductIncomp != conduct.FindMisconductIncomp)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCOND_INCOMP_INCAP (" + conduct.SEQN + "): " + prevFindMisconductIncomp + " -> " + conduct.FindMisconductIncomp);
                    }
                    if (prevFindMisconductIncompDetails != conduct.FindMisconductIncompDetails)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCON_INCOMP_INCAP_DETA (" + conduct.SEQN + "): " + prevFindMisconductIncompDetails + " -> " + conduct.FindMisconductIncompDetails);
                    }
                    if (prevFacingMisconduct != conduct.FacingMisconduct)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP (" + conduct.SEQN + "): " + prevFacingMisconduct + " -> " + conduct.FacingMisconduct);
                    }
                    if (prevFacingMisconductDetails != conduct.FacingMisconductDetails)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP_DET (" + conduct.SEQN + "): " + prevFacingMisconductDetails + " -> " + conduct.FacingMisconductDetails);
                    }
                    if (prevFindNegMalpract != conduct.FindNegMalpract)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT (" + conduct.SEQN + "): " + prevFindNegMalpract + " -> " + conduct.FindNegMalpract);
                    }
                    if (prevFindNegMalpractDetails != conduct.FindNegMalpractDetails)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT_DETAILS (" + conduct.SEQN + "): " + prevFindNegMalpractDetails + " -> " + conduct.FindNegMalpractDetails);
                    }
                    if (prevChangeOffence != conduct.ChargedOffence)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.CHARGED_OFFENCE (" + conduct.SEQN + "): " + prevChangeOffence + " -> " + conduct.ChargedOffence);
                    }
                    if (prevChangeOffenceDetails != conduct.ChargedOffenceDetails)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.CHARGED_OFFENCE_DETAILS (" + conduct.SEQN + "): " + prevChangeOffenceDetails + " -> " + conduct.ChargedOffenceDetails);
                    }
                    if (prevCondRestrict != conduct.CondRestrict)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT (" + conduct.SEQN + "): " + prevCondRestrict + " -> " + conduct.CondRestrict);
                    }
                    if (prevCondRestrictDetails != conduct.CondRestrictDetails)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT_DETAIL (" + conduct.SEQN + "): " + prevCondRestrictDetails + " -> " + conduct.CondRestrictDetails);
                    }
                    if (prevGuilty_Authority != conduct.Guilty_Authority)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTHORITY_OFFENCE (" + conduct.SEQN + "): " + prevGuilty_Authority + " -> " + conduct.Guilty_Authority);
                    }
                    if (prevGuilty_Authority_Details != conduct.Guilty_Authority_Details)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTH_OFFENCE_DETAILS (" + conduct.SEQN + "): " + prevGuilty_Authority_Details + " -> " + conduct.Guilty_Authority_Details);
                    }
                    if (prevEvent_Circum != conduct.EventCircumstance)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.EVENT_CIRCUMSTANCE (" + conduct.SEQN + "): " + prevEvent_Circum + " -> " + conduct.EventCircumstance);
                    }
                    if (prevEvent_Circum_Details != conduct.EventCircumstanceDetails)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.EVENT_CIRCUMSTANCE_DETAILS (" + conduct.SEQN + "): " + prevEvent_Circum_Details + " -> " + conduct.EventCircumstanceDetails);
                    }
                }
                else
                {
                    int newSEQN = GetCounter("COTO_CONDUCT");
                    string sql_InsertUserConduct = "INSERT INTO COTO_CONDUCT ([ID],[SEQN],[DATE_REPORTED],[REFUSED_BY_REG_BODY],[REFUSED_BY_REG_BODY_DETAILS], " +
                                                    " [FIND_MISCOND_INCOMP_INCAP],[FIND_MISCON_INCOMP_INCAP_DETA],[FACING_MISCON_INCOMP_INCAP],[FACING_MISCON_INCOMP_INCAP_DET], " +
                                                    " [FIND_NEG_MALPRACT],[FIND_NEG_MALPRACT_DETAILS],[CHARGED_OFFENCE],[CHARGED_OFFENCE_DETAILS], " +
                                                    " [COND_RESTRICT],[COND_RESTRICT_DETAILS],[GUILTY_AUTHORITY_OFFENCE], [GUILTY_AUTH_OFFENCE_DETAILS]," +
                                                    " [EVENT_CIRCUMSTANCE],[EVENT_CIRCUMSTANCE_DETAILS])" +
                                                    "  VALUES (" + Escape(UserID) + ", " + newSEQN.ToString() + ", " + Escape(conduct.ReportedDate.ToString("yyyy-MM-dd")) + ", " +
                                                    Escape(conduct.RefusedByRegBody) + ", " + Escape(conduct.RefusedByRegBodyDetails) + ", " + Escape(conduct.FindMisconductIncomp) + ", " +
                                                    Escape(conduct.FindMisconductIncompDetails) + ", " + Escape(conduct.FacingMisconduct) + ", " + Escape(conduct.FacingMisconductDetails) + ", " +
                                                    Escape(conduct.FindNegMalpract) + ", " + Escape(conduct.FindNegMalpractDetails) + ", " + Escape(conduct.ChargedOffence) + ", " +
                                                    Escape(conduct.ChargedOffenceDetails) + ", " + Escape(conduct.CondRestrict) + ", " + Escape(conduct.CondRestrictDetails) + ", " +
                                                    Escape(conduct.Guilty_Authority) + ", " + Escape(conduct.Guilty_Authority_Details) + ", " +
                                                    Escape(conduct.EventCircumstance) + ", " + Escape(conduct.EventCircumstanceDetails) + ")";
                    db.ExecuteCommand(sql_InsertUserConduct);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.DATE_REPORTED (" + newSEQN.ToString() + "): " + " -> '" + conduct.ReportedDate.ToString("yyyy-MM-dd") + "'");

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY (" + newSEQN.ToString() + "): " + " -> " + conduct.RefusedByRegBody);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.RefusedByRegBodyDetails);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCOND_INCOMP_INCAP (" + newSEQN.ToString() + "): " + " -> " + conduct.FindMisconductIncomp);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCON_INCOMP_INCAP_DETA (" + newSEQN.ToString() + "): " + " -> " + conduct.FindMisconductIncompDetails);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP (" + newSEQN.ToString() + "): " + " -> " + conduct.FacingMisconduct);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP_DET (" + newSEQN.ToString() + "): " + " -> " + conduct.FacingMisconductDetails);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT (" + newSEQN.ToString() + "): " + " -> " + conduct.FindNegMalpract);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.FindNegMalpractDetails);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.CHARGED_OFFENCE (" + newSEQN.ToString() + "): " + " -> " + conduct.ChargedOffence);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.CHARGED_OFFENCE_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.ChargedOffenceDetails);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT (" + newSEQN.ToString() + "): " + " -> " + conduct.CondRestrict);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT_DETAIL (" + newSEQN.ToString() + "): " + " -> " + conduct.CondRestrictDetails);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTHORITY_OFFENCE (" + newSEQN.ToString() + "): " + " -> " + conduct.Guilty_Authority);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTH_OFFENCE_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.Guilty_Authority_Details);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.EVENT_CIRCUMSTANCE (" + newSEQN.ToString() + "): " + " -> " + conduct.EventCircumstance);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.EVENT_CIRCUMSTANCE_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.EventCircumstanceDetails);
                }
            }
            else
            {
                int newSEQN = GetCounter("COTO_CONDUCT");
                string sql_InsertUserConduct = "INSERT INTO COTO_CONDUCT ([ID],[SEQN],[DATE_REPORTED],[REFUSED_BY_REG_BODY],[REFUSED_BY_REG_BODY_DETAILS], " +
                                                "[FIND_MISCOND_INCOMP_INCAP],[FIND_MISCON_INCOMP_INCAP_DETA],[FACING_MISCON_INCOMP_INCAP],[FACING_MISCON_INCOMP_INCAP_DET], " +
                                                "[FIND_NEG_MALPRACT],[FIND_NEG_MALPRACT_DETAILS],[CHARGED_OFFENCE],[CHARGED_OFFENCE_DETAILS],[COND_RESTRICT],[COND_RESTRICT_DETAILS], " +
                                                "[GUILTY_AUTHORITY_OFFENCE], [GUILTY_AUTH_OFFENCE_DETAILS], [EVENT_CIRCUMSTANCE], [EVENT_CIRCUMSTANCE_DETAILS])" +
                                                "  VALUES (" + Escape(UserID) + ", " + newSEQN.ToString() + ", " + Escape(conduct.ReportedDate.ToString("yyyy-MM-dd")) + ", " +
                                                Escape(conduct.RefusedByRegBody) + ", " + Escape(conduct.RefusedByRegBodyDetails) + ", " + Escape(conduct.FindMisconductIncomp) + ", " +
                                                Escape(conduct.FindMisconductIncompDetails) + ", " + Escape(conduct.FacingMisconduct) + ", " + Escape(conduct.FacingMisconductDetails) + ", " +
                                                Escape(conduct.FindNegMalpract) + ", " + Escape(conduct.FindNegMalpractDetails) + ", " + Escape(conduct.ChargedOffence) + ", " +
                                                Escape(conduct.ChargedOffenceDetails) + ", " + Escape(conduct.CondRestrict) + ", " + Escape(conduct.CondRestrictDetails) + ", " +
                                                Escape(conduct.Guilty_Authority) + ", " + Escape(conduct.Guilty_Authority_Details) + ", " +
                                                Escape(conduct.EventCircumstance) + ", " + Escape(conduct.EventCircumstanceDetails) + ")";
                db.ExecuteCommand(sql_InsertUserConduct);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.DATE_REPORTED (" + newSEQN.ToString() + "): " + " -> '" + conduct.ReportedDate.ToString("yyyy-MM-dd") + "'");

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY (" + newSEQN.ToString() + "): " + " -> " + conduct.RefusedByRegBody);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.RefusedByRegBodyDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCOND_INCOMP_INCAP (" + newSEQN.ToString() + "): " + " -> " + conduct.FindMisconductIncomp);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCON_INCOMP_INCAP_DETA (" + newSEQN.ToString() + "): " + " -> " + conduct.FindMisconductIncompDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP (" + newSEQN.ToString() + "): " + " -> " + conduct.FacingMisconduct);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP_DET (" + newSEQN.ToString() + "): " + " -> " + conduct.FacingMisconductDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT (" + newSEQN.ToString() + "): " + " -> " + conduct.FindNegMalpract);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.FindNegMalpractDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.CHARGED_OFFENCE (" + newSEQN.ToString() + "): " + " -> " + conduct.ChargedOffence);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.CHARGED_OFFENCE_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.ChargedOffenceDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT (" + newSEQN.ToString() + "): " + " -> " + conduct.CondRestrict);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT_DETAIL (" + newSEQN.ToString() + "): " + " -> " + conduct.CondRestrictDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTHORITY_OFFENCE (" + newSEQN.ToString() + "): " + " -> " + conduct.Guilty_Authority);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTH_OFFENCE_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.Guilty_Authority_Details);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.EVENT_CIRCUMSTANCE (" + newSEQN.ToString() + "): " + " -> " + conduct.EventCircumstance);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.EVENT_CIRCUMSTANCE_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.EventCircumstanceDetails);
            }

            var appStatus = new ApplicationStatus();
            appStatus.ConductSubmitted = conduct.ReportedDate;
            UpdateApplicationConductSubmitted(UserID, appStatus);

        }

        public void UpdateUserConductInfoLoggedSEQN(string UserID, ConductNew conduct)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT * FROM COTO_CONDUCT  " +
                " WHERE ID = " + Escape(UserID) + " AND SEQN = " + conduct.SEQN.ToString();
            var table = db.GetData(sql);
            if (conduct.SEQN > 0 && table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];
                string prevReportedDate = string.Empty;
                if (row["DATE_REPORTED"] != DBNull.Value) prevReportedDate = ((DateTime)row["DATE_REPORTED"]).ToString("yyyy-MM-dd");

                string prevRefusedByRegBody = (string)row["REFUSED_BY_REG_BODY"];
                string prevRefusedByRegBodyDetails = (string)row["REFUSED_BY_REG_BODY_DETAILS"];

                string prevFindMisconductIncomp = (string)row["FIND_MISCOND_INCOMP_INCAP"];
                string prevFindMisconductIncompDetails = (string)row["FIND_MISCON_INCOMP_INCAP_DETA"];

                string prevFacingMisconduct = (string)row["FACING_MISCON_INCOMP_INCAP"];
                string prevFacingMisconductDetails = (string)row["FACING_MISCON_INCOMP_INCAP_DET"];

                string prevFindNegMalpract = (string)row["FIND_NEG_MALPRACT"];
                string prevFindNegMalpractDetails = (string)row["FIND_NEG_MALPRACT_DETAILS"];

                string prevPrevConduct = (string)row["PREVIOUS_CONDUCT"];
                string prevPrevConductDetails = (string)row["PREVIOUS_CONDUCT_DETAILS"];

                string prevGuilty_Authority = (string)row["GUILTY_AUTHORITY_OFFENCE"];
                string prevGuilty_Authority_Details = (string)row["GUILTY_AUTH_OFFENCE_DETAILS"];

                string prevCondRestrict = (string)row["COND_RESTRICT"];
                string prevCondRestrictDetails = (string)row["COND_RESTRICT_DETAILS"];

                string sql_UpdateUserConduct = "UPDATE COTO_CONDUCT SET " +
                " DATE_REPORTED = " + Escape(conduct.ReportedDate.ToString("yyyy-MM-dd")) + ", " +
                " REFUSED_BY_REG_BODY = " + Escape(conduct.RefusedByRegBody) + ", " +
                " REFUSED_BY_REG_BODY_DETAILS = " + Escape(conduct.RefusedByRegBodyDetails) + ", " +

                " FIND_MISCOND_INCOMP_INCAP = " + Escape(conduct.FindMisconductIncomp) + ", " +
                " FIND_MISCON_INCOMP_INCAP_DETA = " + Escape(conduct.FindMisconductIncompDetails) + ", " +

                " FACING_MISCON_INCOMP_INCAP = " + Escape(conduct.FacingMisconduct) + ", " +
                " FACING_MISCON_INCOMP_INCAP_DET = " + Escape(conduct.FacingMisconductDetails) + ", " +

                " FIND_NEG_MALPRACT = " + Escape(conduct.FindNegMalpract) + ", " +
                " FIND_NEG_MALPRACT_DETAILS = " + Escape(conduct.FindNegMalpractDetails) + ", " +

                " PREVIOUS_CONDUCT = " + Escape(conduct.PrevConduct) + ", " +
                " PREVIOUS_CONDUCT_DETAILS = " + Escape(conduct.PrevConductDetails) + ", " +

                " GUILTY_AUTHORITY_OFFENCE = " + Escape(conduct.Guilty_Authority) + ", " +
                " GUILTY_AUTH_OFFENCE_DETAILS = " + Escape(conduct.Guilty_Authority_Details) + ", " +

                " COND_RESTRICT = " + Escape(conduct.CondRestrict) + ", " +
                " COND_RESTRICT_DETAILS = " + Escape(conduct.CondRestrictDetails) +

                " WHERE ID = " + Escape(UserID) + " AND SEQN = " + conduct.SEQN.ToString();

                db.ExecuteCommand(sql_UpdateUserConduct);

                if (prevReportedDate != conduct.ReportedDate.ToString("yyyy-MM-dd"))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.DATE_REPORTED (" + conduct.SEQN.ToString() + "): " + prevReportedDate + " -> " + conduct.ReportedDate.ToString("yyyy-MM-dd"));
                }
                if (prevRefusedByRegBody != conduct.RefusedByRegBody)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY (" + conduct.SEQN.ToString() + "): " + prevRefusedByRegBody + " -> " + conduct.RefusedByRegBody);
                }
                if (prevRefusedByRegBodyDetails != conduct.RefusedByRegBodyDetails)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY_DETAILS (" + conduct.SEQN.ToString() + "): " + prevRefusedByRegBodyDetails + " -> " + conduct.RefusedByRegBodyDetails);
                }
                if (prevFindMisconductIncomp != conduct.FindMisconductIncomp)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCOND_INCOMP_INCAP (" + conduct.SEQN.ToString() + "): " + prevFindMisconductIncomp + " -> " + conduct.FindMisconductIncomp);
                }
                if (prevFindMisconductIncompDetails != conduct.FindMisconductIncompDetails)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCON_INCOMP_INCAP_DETA (" + conduct.SEQN.ToString() + "): " + prevFindMisconductIncompDetails + " -> " + conduct.FindMisconductIncompDetails);
                }
                if (prevFacingMisconduct != conduct.FacingMisconduct)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP (" + conduct.SEQN.ToString() + "): " + prevFacingMisconduct + " -> " + conduct.FacingMisconduct);
                }
                if (prevFacingMisconductDetails != conduct.FacingMisconductDetails)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP_DET (" + conduct.SEQN.ToString() + "): " + prevFacingMisconductDetails + " -> " + conduct.FacingMisconductDetails);
                }
                if (prevFindNegMalpract != conduct.FindNegMalpract)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT (" + conduct.SEQN.ToString() + "): " + prevFindNegMalpract + " -> " + conduct.FindNegMalpract);
                }
                if (prevFindNegMalpractDetails != conduct.FindNegMalpractDetails)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT_DETAILS (" + conduct.SEQN.ToString() + "): " + prevFindNegMalpractDetails + " -> " + conduct.FindNegMalpractDetails);
                }
                if (prevPrevConduct != conduct.PrevConduct)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.PREVIOUS_CONDUCT (" + conduct.SEQN.ToString() + "): " + prevPrevConduct + " -> " + conduct.PrevConduct);
                }
                if (prevPrevConductDetails != conduct.PrevConductDetails)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.PREVIOUS_CONDUCT_DETAILS (" + conduct.SEQN.ToString() + "): " + prevPrevConductDetails + " -> " + conduct.PrevConductDetails);
                }
                if (prevGuilty_Authority != conduct.Guilty_Authority)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTHORITY_OFFENCE (" + conduct.SEQN.ToString() + "): " + prevGuilty_Authority + " -> " + conduct.Guilty_Authority);
                }
                if (prevGuilty_Authority_Details != conduct.Guilty_Authority_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTH_OFFENCE_DETAILS (" + conduct.SEQN.ToString() + "): " + prevGuilty_Authority_Details + " -> " + conduct.Guilty_Authority_Details);
                }
                if (prevCondRestrict != conduct.CondRestrict)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT (" + conduct.SEQN.ToString() + "): " + prevCondRestrict + " -> " + conduct.CondRestrict);
                }
                if (prevCondRestrictDetails != conduct.CondRestrictDetails)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT_DETAIL (" + conduct.SEQN.ToString() + "): " + prevCondRestrictDetails + " -> " + conduct.CondRestrictDetails);
                }
            }
            else
            {
                int newSEQN = GetCounter("COTO_CONDUCT");
                string sql_InsertUserConduct = "INSERT INTO COTO_CONDUCT ([ID],[SEQN],[DATE_REPORTED],[REFUSED_BY_REG_BODY],[REFUSED_BY_REG_BODY_DETAILS], " +
                                                "[FIND_MISCOND_INCOMP_INCAP],[FIND_MISCON_INCOMP_INCAP_DETA],[FACING_MISCON_INCOMP_INCAP],[FACING_MISCON_INCOMP_INCAP_DET], " +
                                                "[FIND_NEG_MALPRACT],[FIND_NEG_MALPRACT_DETAILS],[PREVIOUS_CONDUCT],[PREVIOUS_CONDUCT_DETAILS],[GUILTY_AUTHORITY_OFFENCE], " +
                                                "[GUILTY_AUTH_OFFENCE_DETAILS],[COND_RESTRICT],[COND_RESTRICT_DETAILS])" +
                                                "  VALUES (" + Escape(UserID) + ", " + newSEQN.ToString() + ", " + Escape(conduct.ReportedDate.ToString("yyyy-MM-dd")) + ", " +
                                                Escape(conduct.RefusedByRegBody) + ", " + Escape(conduct.RefusedByRegBodyDetails) + ", " + Escape(conduct.FindMisconductIncomp) + ", " +
                                                Escape(conduct.FindMisconductIncompDetails) + ", " + Escape(conduct.FacingMisconduct) + ", " + Escape(conduct.FacingMisconductDetails) + ", " +
                                                Escape(conduct.FindNegMalpract) + ", " + Escape(conduct.FindNegMalpractDetails) + ", " + Escape(conduct.PrevConduct) + ", " +
                                                Escape(conduct.PrevConductDetails) + ", " + Escape(conduct.Guilty_Authority) + ", " + Escape(conduct.Guilty_Authority_Details) + ", " +
                                                Escape(conduct.CondRestrict) + ", " + Escape(conduct.CondRestrictDetails) + ")";
                db.ExecuteCommand(sql_InsertUserConduct);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.DATE_REPORTED (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.ReportedDate.ToString("yyyy-MM-dd"));

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.RefusedByRegBody);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.REFUSED_BY_REG_BODY_DETAILS (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.RefusedByRegBodyDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCOND_INCOMP_INCAP (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.FindMisconductIncomp);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_MISCON_INCOMP_INCAP_DETA (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.FindMisconductIncompDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.FacingMisconduct);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FACING_MISCON_INCOMP_INCAP_DET (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.FacingMisconductDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.FindNegMalpract);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.FIND_NEG_MALPRACT_DETAILS (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.FindNegMalpractDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.PREVIOUS_CONDUCT (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.PrevConduct);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.PREVIOUS_CONDUCT_DETAILS (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.PrevConductDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTHORITY_OFFENCE (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.Guilty_Authority);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.GUILTY_AUTH_OFFENCE_DETAILS (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.Guilty_Authority_Details);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.CondRestrict);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONDUCT.COND_RESTRICT_DETAIL (" + conduct.SEQN.ToString() + "): " + " -> " + conduct.CondRestrictDetails);

            }
        }

        public void UpdateUserConductInfoLoggedSEQNNew(string UserID, ConductNew conduct)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT * FROM COTO_SUIT_PRACTICE  " +
                " WHERE ID = " + Escape(UserID) + " AND RENEWAL_YEAR = " + Escape(conduct.RenewalYear);
            var table = db.GetData(sql);
            if (conduct.SEQN > 0 && table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];
                string prevReportedDate = string.Empty;
                if (row["DATE_REPORTED"] != DBNull.Value) prevReportedDate = ((DateTime)row["DATE_REPORTED"]).ToString("yyyy-MM-dd");

                string prevFacingMisconduct = (string)row["FACING_MISCON_INCOMP_INCAP"];
                string prevFacingMisconductDetails =  (row["FACING_MISCON_INCOMP_INCAP_DET"] != DBNull.Value)? (string)row["FACING_MISCON_INCOMP_INCAP_DET"] : string.Empty;

                string prevFindMisconductIncomp = (string)row["FIND_MISCOND_INCOMP_INCAP"];
                string prevFindMisconductIncompDetails =  (row["FIND_MISCON_INCOMP_INCAP_DET"] != DBNull.Value)? (string)row["FIND_MISCON_INCOMP_INCAP_DET"] : string.Empty;

                string prevFindNegMalpract = (string)row["FIND_NEG_MALPRACT"];
                string prevFindNegMalpractDetails = (row["FIND_NEG_MALPRACT_DETAILS"] != DBNull.Value)?  (string)row["FIND_NEG_MALPRACT_DETAILS"]: string.Empty;

                string prevChangeOffence = (string)row["CHARGED_OFFENCE"];
                string prevChangeOffenceDetails = row["CHARGED_OFFENCE_DETAILS"] != DBNull.Value ? (string)row["CHARGED_OFFENCE_DETAILS"] : string.Empty;

                string prevCondRestrict = (string)row["COND_RESTRICT"];
                string prevCondRestrictDetails = (row["COND_RESTRICT_DETAILS"] != DBNull.Value) ? (string)row["COND_RESTRICT_DETAILS"] : string.Empty;

                string prevGuilty_Authority = (string)row["GUILTY_AUTHORITY_OFFENCE"];
                string prevGuilty_Authority_Details = (row["GUILTY_AUTH_OFFENCE_DETAILS"] != DBNull.Value) ? (string)row["GUILTY_AUTH_OFFENCE_DETAILS"] : string.Empty;

                string prevEvent_Circum = (string)row["EVENT_CIRCUMSTANCE"];
                string prevEvent_Circum_Details = row["EVENT_CIRCUMSTANCE_DETAILS"] != DBNull.Value ? (string)row["EVENT_CIRCUMSTANCE_DETAILS"] : string.Empty;


                string sql_UpdateUserConduct = "UPDATE COTO_SUIT_PRACTICE SET " +
                " DATE_REPORTED = " + Escape(conduct.ReportedDate.ToString("yyyy-MM-dd")) + ", " +
                " FACING_MISCON_INCOMP_INCAP = " + Escape(conduct.FacingMisconduct) + ", " +
                " FACING_MISCON_INCOMP_INCAP_DET = " + Escape(conduct.FacingMisconductDetails) + ", " +

                " FIND_MISCOND_INCOMP_INCAP = " + Escape(conduct.FindMisconductIncomp) + ", " +
                " FIND_MISCON_INCOMP_INCAP_DET = " + Escape(conduct.FindMisconductIncompDetails) + ", " +

                " FIND_NEG_MALPRACT = " + Escape(conduct.FindNegMalpract) + ", " +
                " FIND_NEG_MALPRACT_DETAILS = " + Escape(conduct.FindNegMalpractDetails) + ", " +

                " CHARGED_OFFENCE = " + Escape(conduct.ChargedOffence) + ", " +
                " CHARGED_OFFENCE_DETAILS = " + Escape(conduct.ChargedOffenceDetails) + ", " +

                " COND_RESTRICT = " + Escape(conduct.CondRestrict) + ", " +
                " COND_RESTRICT_DETAILS = " + Escape(conduct.CondRestrictDetails) + ", " +

                " GUILTY_AUTHORITY_OFFENCE = " + Escape(conduct.Guilty_Authority) + ", " +
                " GUILTY_AUTH_OFFENCE_DETAILS = " + Escape(conduct.Guilty_Authority_Details) + ", " +

                " EVENT_CIRCUMSTANCE = " + Escape(conduct.EventCircumstance) + ", " +
                " EVENT_CIRCUMSTANCE_DETAILS = " + Escape(conduct.EventCircumstanceDetails) +

                " WHERE ID = " + Escape(UserID) + " AND RENEWAL_YEAR = " + Escape(conduct.RenewalYear);

                db.ExecuteCommand(sql_UpdateUserConduct);

                if (prevReportedDate != conduct.ReportedDate.ToString("yyyy-MM-dd"))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.DATE_REPORTED (" + conduct.SEQN.ToString() + "): " + prevReportedDate + " -> " + conduct.ReportedDate.ToString("yyyy-MM-dd"));
                }

                if (prevFacingMisconduct != conduct.FacingMisconduct)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.FACING_MISCON_INCOMP_INCAP (" + conduct.SEQN.ToString() + "): " + prevFacingMisconduct + " -> " + conduct.FacingMisconduct);
                }
                if (prevFacingMisconductDetails != conduct.FacingMisconductDetails)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.FACING_MISCON_INCOMP_INCAP_DET (" + conduct.SEQN.ToString() + "): " + prevFacingMisconductDetails + " -> " + conduct.FacingMisconductDetails);
                }

                if (prevFindMisconductIncomp != conduct.FindMisconductIncomp)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.FIND_MISCOND_INCOMP_INCAP (" + conduct.SEQN.ToString() + "): " + prevFindMisconductIncomp + " -> " + conduct.FindMisconductIncomp);
                }
                if (prevFindMisconductIncompDetails != conduct.FindMisconductIncompDetails)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.FIND_MISCON_INCOMP_INCAP_DET (" + conduct.SEQN.ToString() + "): " + prevFindMisconductIncompDetails + " -> " + conduct.FindMisconductIncompDetails);
                }
                
                if (prevFindNegMalpract != conduct.FindNegMalpract)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.FIND_NEG_MALPRACT (" + conduct.SEQN.ToString() + "): " + prevFindNegMalpract + " -> " + conduct.FindNegMalpract);
                }
                if (prevFindNegMalpractDetails != conduct.FindNegMalpractDetails)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.FIND_NEG_MALPRACT_DETAILS (" + conduct.SEQN.ToString() + "): " + prevFindNegMalpractDetails + " -> " + conduct.FindNegMalpractDetails);
                }

                if (prevChangeOffence != conduct.ChargedOffence)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.CHARGED_OFFENCE (" + conduct.SEQN + "): " + prevChangeOffence + " -> " + conduct.ChargedOffence);
                }
                if (prevChangeOffenceDetails != conduct.ChargedOffenceDetails)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.CHARGED_OFFENCE_DETAILS (" + conduct.SEQN + "): " + prevChangeOffenceDetails + " -> " + conduct.ChargedOffenceDetails);
                }
                if (prevCondRestrict != conduct.CondRestrict)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.COND_RESTRICT (" + conduct.SEQN + "): " + prevCondRestrict + " -> " + conduct.CondRestrict);
                }
                if (prevCondRestrictDetails != conduct.CondRestrictDetails)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.COND_RESTRICT_DETAIL (" + conduct.SEQN + "): " + prevCondRestrictDetails + " -> " + conduct.CondRestrictDetails);
                }
                if (prevGuilty_Authority != conduct.Guilty_Authority)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.GUILTY_AUTHORITY_OFFENCE (" + conduct.SEQN + "): " + prevGuilty_Authority + " -> " + conduct.Guilty_Authority);
                }
                if (prevGuilty_Authority_Details != conduct.Guilty_Authority_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.GUILTY_AUTH_OFFENCE_DETAILS (" + conduct.SEQN + "): " + prevGuilty_Authority_Details + " -> " + conduct.Guilty_Authority_Details);
                }
                if (prevEvent_Circum != conduct.EventCircumstance)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.EVENT_CIRCUMSTANCE (" + conduct.SEQN + "): " + prevEvent_Circum + " -> " + conduct.EventCircumstance);
                }
                if (prevEvent_Circum_Details != conduct.EventCircumstanceDetails)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.EVENT_CIRCUMSTANCE_DETAILS (" + conduct.SEQN + "): " + prevEvent_Circum_Details + " -> " + conduct.EventCircumstanceDetails);
                }
            }
            else
            {
                int newSEQN = GetCounter("COTO_SUIT_PRACTICE");
                string sql_InsertUserConduct = "INSERT INTO COTO_SUIT_PRACTICE ([ID],[SEQN], [DATE_REPORTED], " +
                                                " [FACING_MISCON_INCOMP_INCAP],[FACING_MISCON_INCOMP_INCAP_DET], [FIND_MISCOND_INCOMP_INCAP],[FIND_MISCON_INCOMP_INCAP_DET], " +
                                                " [FIND_NEG_MALPRACT],[FIND_NEG_MALPRACT_DETAILS],[CHARGED_OFFENCE],[CHARGED_OFFENCE_DETAILS], " +
                                                " [COND_RESTRICT],[COND_RESTRICT_DETAILS],[GUILTY_AUTHORITY_OFFENCE], [GUILTY_AUTH_OFFENCE_DETAILS]," +
                                                " [EVENT_CIRCUMSTANCE],[EVENT_CIRCUMSTANCE_DETAILS], RENEWAL_YEAR)" +
                                                "  VALUES (" + Escape(UserID) + ", " + newSEQN.ToString() + ", " + Escape(conduct.ReportedDate.ToString("yyyy-MM-dd")) + ", " +
                                                Escape(conduct.FacingMisconduct) + ", " + Escape(conduct.FacingMisconductDetails) + ", " + Escape(conduct.FindMisconductIncomp) + ", " + Escape(conduct.FindMisconductIncompDetails) + ", " + 
                                                Escape(conduct.FindNegMalpract) + ", " + Escape(conduct.FindNegMalpractDetails) + ", " + Escape(conduct.ChargedOffence) + ", " + Escape(conduct.ChargedOffenceDetails) + ", " + 
                                                Escape(conduct.CondRestrict) + ", " + Escape(conduct.CondRestrictDetails) + ", " + Escape(conduct.Guilty_Authority) + ", " + Escape(conduct.Guilty_Authority_Details) + ", " +
                                                Escape(conduct.EventCircumstance) + ", " + Escape(conduct.EventCircumstanceDetails) + ", " + Escape(conduct.RenewalYear) + ")";
                db.ExecuteCommand(sql_InsertUserConduct);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.DATE_REPORTED (" + newSEQN.ToString() + "): " + " -> " + conduct.ReportedDate.ToString("yyyy-MM-dd"));

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.FACING_MISCON_INCOMP_INCAP (" + newSEQN.ToString() + "): " + " -> " + conduct.FacingMisconduct);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.FACING_MISCON_INCOMP_INCAP_DET (" + newSEQN.ToString() + "): " + " -> " + conduct.FacingMisconductDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.FIND_MISCOND_INCOMP_INCAP (" + newSEQN.ToString() + "): " + " -> " + conduct.FindMisconductIncomp);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.FIND_MISCON_INCOMP_INCAP_DET (" + newSEQN.ToString() + "): " + " -> " + conduct.FindMisconductIncompDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.FIND_NEG_MALPRACT (" + newSEQN.ToString() + "): " + " -> " + conduct.FindNegMalpract);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.FIND_NEG_MALPRACT_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.FindNegMalpractDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.CHARGED_OFFENCE (" + newSEQN.ToString() + "): " + " -> " + conduct.ChargedOffence);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.CHARGED_OFFENCE_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.ChargedOffenceDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.GUILTY_AUTHORITY_OFFENCE (" + newSEQN.ToString() + "): " + " -> " + conduct.Guilty_Authority);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.GUILTY_AUTH_OFFENCE_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.Guilty_Authority_Details);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.COND_RESTRICT (" + newSEQN.ToString() + "): " + " -> " + conduct.CondRestrict);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.COND_RESTRICT_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.CondRestrictDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.EVENT_CIRCUMSTANCE (" + newSEQN.ToString() + "): " + " -> " + conduct.EventCircumstance);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.EVENT_CIRCUMSTANCE_DETAILS (" + newSEQN.ToString() + "): " + " -> " + conduct.EventCircumstanceDetails);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_SUIT_PRACTICE.RENEWAL_YEAR (" + newSEQN.ToString() + "): " + " -> " + conduct.RenewalYear);
            }
        }

        public void UpdateUserControlledActsInfo(string ID, ControlledAct act, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT COUNT(1) FROM COTO_CONTROLACTS  " +
                " WHERE ID = " + Escape(ID) + " AND RENEWAL_YEAR =" + CurrentYear.ToString();
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                if ((int)row[0] == 0)
                {
                    int newSEQN = GetCounter("COTO_CONTROLACTS");
                    string sql_InsertUserConduct = "INSERT INTO COTO_CONTROLACTS (ID, SEQN, RENEWAL_YEAR, COMM_DIAGNOSIS, BELOW_DERMIS, " +
                    " SETTING_CASTING, SPINAL_MANIPULATION, ADMINISTER_SUBSTANCE, INSTRUMENT_HAND_FINGER, XRAYS, " +
                    " DRUGS, VISION, PSYCHOTHERAPY, ACUPUNCTURE) VALUES ( " + Escape(ID) + ", " + newSEQN.ToString() + ", " + CurrentYear.ToString() + ", " +
                    Escape(act.Comm_Diagnosis_1) + ", " + Escape(act.Below_Dermis_2) + ", " + Escape(act.Setting_Casting_3) + ", " + Escape(act.Spinal_Manipulation_4) + ", " +
                    Escape(act.Administer_Substance_5) + ", " + Escape(act.Instr_Hand_Finger_6) + ", " + Escape(act.Xrays_7) + ", " + Escape(act.Drugs_8) + ", " +
                    Escape(act.Vision_9) + ", " + Escape(act.Psyhotherapy_10) + ", " + Escape(act.Acupuncture_11) + ") ";
                    db.ExecuteCommand(sql_InsertUserConduct);


                }
                else
                {
                    string sql_UpdateUserConduct = "UPDATE COTO_CONTROLACTS SET " +

                    " COMM_DIAGNOSIS = " + Escape(act.Comm_Diagnosis_1) + ", " +
                    " BELOW_DERMIS = " + Escape(act.Below_Dermis_2) + ", " +

                    " SETTING_CASTING = " + Escape(act.Setting_Casting_3) + ", " +
                    " SPINAL_MANIPULATION = " + Escape(act.Spinal_Manipulation_4) + ", " +

                    " ADMINISTER_SUBSTANCE = " + Escape(act.Administer_Substance_5) + ", " +
                    " INSTRUMENT_HAND_FINGER = " + Escape(act.Instr_Hand_Finger_6) + ", " +

                    " XRAYS = " + Escape(act.Xrays_7) + ", " +
                    " DRUGS = " + Escape(act.Drugs_8) + ", " +

                    " VISION = " + Escape(act.Vision_9) + ", " +
                    " PSYCHOTHERAPY = " + Escape(act.Psyhotherapy_10) + ", " +

                    " ACUPUNCTURE = " + Escape(act.Acupuncture_11) +

                    " WHERE ID = " + Escape(ID) + " AND RENEWAL_YEAR = " + CurrentYear.ToString();
                    db.ExecuteCommand(sql_UpdateUserConduct);
                }
            }
        }

        public void UpdateUserControlledActsInfoLogged(string UserID, ControlledAct act, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT TOP (1) * FROM COTO_CONTROLACTS  " +
                " WHERE ID = " + Escape(UserID) + " AND RENEWAL_YEAR =" + CurrentYear.ToString();
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];

                string Contr_act_1 = (string)row["COMM_DIAGNOSIS"];
                string Contr_act_2 = (string)row["BELOW_DERMIS"];
                string Contr_act_3 = (string)row["SETTING_CASTING"];
                string Contr_act_4 = (string)row["SPINAL_MANIPULATION"];
                string Contr_act_5 = (string)row["ADMINISTER_SUBSTANCE"];
                string Contr_act_6 = (string)row["INSTRUMENT_HAND_FINGER"];
                string Contr_act_7 = (string)row["XRAYS"];
                string Contr_act_8 = (string)row["DRUGS"];
                string Contr_act_9 = (string)row["VISION"];
                string Contr_act_10 = (string)row["PSYCHOTHERAPY"];
                string Contr_act_11 = (string)row["ACUPUNCTURE"];

                string sql_UpdateUserConduct = "UPDATE COTO_CONTROLACTS SET " +

                " COMM_DIAGNOSIS = " + Escape(act.Comm_Diagnosis_1) + ", " +
                " BELOW_DERMIS = " + Escape(act.Below_Dermis_2) + ", " +

                " SETTING_CASTING = " + Escape(act.Setting_Casting_3) + ", " +
                " SPINAL_MANIPULATION = " + Escape(act.Spinal_Manipulation_4) + ", " +

                " ADMINISTER_SUBSTANCE = " + Escape(act.Administer_Substance_5) + ", " +
                " INSTRUMENT_HAND_FINGER = " + Escape(act.Instr_Hand_Finger_6) + ", " +

                " XRAYS = " + Escape(act.Xrays_7) + ", " +
                " DRUGS = " + Escape(act.Drugs_8) + ", " +

                " VISION = " + Escape(act.Vision_9) + ", " +
                " PSYCHOTHERAPY = " + Escape(act.Psyhotherapy_10) + ", " +

                " ACUPUNCTURE = " + Escape(act.Acupuncture_11) +

                " WHERE ID = " + Escape(UserID) + " AND RENEWAL_YEAR = " + CurrentYear.ToString();
                db.ExecuteCommand(sql_UpdateUserConduct);

                if (Contr_act_1 != act.Comm_Diagnosis_1)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.COMM_DIAGNOSIS (" + CurrentYear.ToString() + "): " + Contr_act_1 + " -> " + act.Comm_Diagnosis_1);
                }

                if (Contr_act_2 != act.Below_Dermis_2)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.BELOW_DERMIS (" + CurrentYear.ToString() + "): " + Contr_act_2 + " -> " + act.Below_Dermis_2);
                }

                if (Contr_act_3 != act.Setting_Casting_3)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.SETTING_CASTING (" + CurrentYear.ToString() + "): " + Contr_act_3 + " -> " + act.Setting_Casting_3);
                }

                if (Contr_act_4 != act.Spinal_Manipulation_4)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.SPINAL_MANIPULATION (" + CurrentYear.ToString() + "): " + Contr_act_4 + " -> " + act.Spinal_Manipulation_4);
                }

                if (Contr_act_5 != act.Administer_Substance_5)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.ADMINISTER_SUBSTANCE (" + CurrentYear.ToString() + "): " + Contr_act_5 + " -> " + act.Administer_Substance_5);
                }

                if (Contr_act_6 != act.Instr_Hand_Finger_6)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.INSTRUMENT_HAND_FINGER (" + CurrentYear.ToString() + "): " + Contr_act_6 + " -> " + act.Instr_Hand_Finger_6);
                }

                if (Contr_act_7 != act.Xrays_7)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.XRAYS (" + CurrentYear.ToString() + "): " + Contr_act_7 + " -> " + act.Xrays_7);
                }

                if (Contr_act_8 != act.Drugs_8)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.DRUGS (" + CurrentYear.ToString() + "): " + Contr_act_8 + " -> " + act.Drugs_8);
                }

                if (Contr_act_9 != act.Vision_9)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.VISION (" + CurrentYear.ToString() + "): " + Contr_act_9 + " -> " + act.Vision_9);
                }

                if (Contr_act_10 != act.Psyhotherapy_10)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.PSYCHOTHERAPY (" + CurrentYear.ToString() + "): " + Contr_act_10 + " -> " + act.Psyhotherapy_10);
                }

                if (Contr_act_11 != act.Acupuncture_11)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.ACUPUNCTURE (" + CurrentYear.ToString() + "): " + Contr_act_11 + " -> " + act.Acupuncture_11);
                }

            }
            else
            {
                int newSEQN = GetCounter("COTO_CONTROLACTS");
                string sql_InsertUserConduct = "INSERT INTO COTO_CONTROLACTS (ID, SEQN, RENEWAL_YEAR, COMM_DIAGNOSIS, BELOW_DERMIS, " +
                " SETTING_CASTING, SPINAL_MANIPULATION, ADMINISTER_SUBSTANCE, INSTRUMENT_HAND_FINGER, XRAYS, " +
                " DRUGS, VISION, PSYCHOTHERAPY, ACUPUNCTURE) VALUES ( " + Escape(UserID) + ", " + newSEQN.ToString() + ", " + CurrentYear.ToString() + ", " +
                Escape(act.Comm_Diagnosis_1) + ", " + Escape(act.Below_Dermis_2) + ", " + Escape(act.Setting_Casting_3) + ", " + Escape(act.Spinal_Manipulation_4) + ", " +
                Escape(act.Administer_Substance_5) + ", " + Escape(act.Instr_Hand_Finger_6) + ", " + Escape(act.Xrays_7) + ", " + Escape(act.Drugs_8) + ", " +
                Escape(act.Vision_9) + ", " + Escape(act.Psyhotherapy_10) + ", " + Escape(act.Acupuncture_11) + ") ";
                db.ExecuteCommand(sql_InsertUserConduct);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.COMM_DIAGNOSIS (" + CurrentYear.ToString() + "): -> " + act.Comm_Diagnosis_1);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.BELOW_DERMIS (" + CurrentYear.ToString() + "): -> " + act.Below_Dermis_2);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.SETTING_CASTING (" + CurrentYear.ToString() + "): -> " + act.Setting_Casting_3);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.SPINAL_MANIPULATION (" + CurrentYear.ToString() + "): -> " + act.Spinal_Manipulation_4);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.ADMINISTER_SUBSTANCE (" + CurrentYear.ToString() + "): -> " + act.Administer_Substance_5);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.INSTRUMENT_HAND_FINGER (" + CurrentYear.ToString() + "): -> " + act.Instr_Hand_Finger_6);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.XRAYS (" + CurrentYear.ToString() + "): -> " + act.Xrays_7);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.DRUGS (" + CurrentYear.ToString() + "): -> " + act.Drugs_8);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.VISION (" + CurrentYear.ToString() + "): -> " + act.Vision_9);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.PSYCHOTHERAPY (" + CurrentYear.ToString() + "): -> " + act.Psyhotherapy_10);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_CONTROLACTS.ACUPUNCTURE (" + CurrentYear.ToString() + "): -> " + act.Acupuncture_11);

            }
        }

        public void UpdateUserInsuranceInfo(User user, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT COUNT(1) FROM INSURANCE  " +
                " WHERE ID = " + Escape(user.Id) + " AND INS_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                if ((int)row[0] == 0)
                {
                    int newSEQN = GetCounter("INSURANCE");
                    string sql_InsertUserInsurance = "INSERT INTO INSURANCE (ID, SEQN, INS_YEAR, INS_PLAN_HELD, INS_INSURE_OTHER, " +
                    " INS_EXPIRY_DATE, INS_CERT_NUMBER) VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", '" + CurrentYear.ToString() + "', '" +
                    user.InsuranceInfo.PlanHeld + "', '" + user.InsuranceInfo.OtherInsuranceProvider + "', " +
                    ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? " NULL " : ("'" + user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd") + "'")) + ", '" + user.InsuranceInfo.CertificateNumber + "') ";
                    db.ExecuteCommand(sql_InsertUserInsurance);
                }
                else
                {
                    string sql_UpdateUserInsurance = "UPDATE INSURANCE SET " +
                    " INS_PLAN_HELD = '" + user.InsuranceInfo.PlanHeld + "', " +
                    " INS_INSURE_OTHER = '" + user.InsuranceInfo.OtherInsuranceProvider + "', ";

                    if (user.InsuranceInfo.ExpiryDate == DateTime.MinValue)
                    {
                        sql_UpdateUserInsurance += " INS_EXPIRY_DATE = NULL, ";
                    }
                    else
                    {
                        sql_UpdateUserInsurance += " INS_EXPIRY_DATE = '" + user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd") + "', ";
                    }

                    sql_UpdateUserInsurance += " INS_CERT_NUMBER = '" + user.InsuranceInfo.CertificateNumber + "' " +

                    " WHERE ID = " + Escape(user.Id) + " AND INS_YEAR = '" + CurrentYear.ToString() + "'";
                    db.ExecuteCommand(sql_UpdateUserInsurance);
                }
            }
        }

        public void UpdateUserInsuranceInfoLogged(string UserID, User user, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT * FROM INSURANCE  " +
                " WHERE ID = " + Escape(user.Id) + " AND INS_YEAR = " + Escape(CurrentYear.ToString());
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];

                string prevInsPlanHeld = (string)row["INS_PLAN_HELD"];
                string prevInsInsureOther = (string)row["INS_INSURE_OTHER"];
                string prevInsExpiryDate = string.Empty;
                if (row["INS_EXPIRY_DATE"] != DBNull.Value)
                {
                    prevInsExpiryDate = ((DateTime)row["INS_EXPIRY_DATE"]).ToString("MM/dd/yyyy");
                }
                string prevCertNumber = (string)row["INS_CERT_NUMBER"];


                string sql_UpdateUserInsurance = "UPDATE INSURANCE SET " +
                " INS_PLAN_HELD = '" + user.InsuranceInfo.PlanHeld + "', " +
                " INS_INSURE_OTHER = '" + user.InsuranceInfo.OtherInsuranceProvider + "', ";

                if (user.InsuranceInfo.ExpiryDate == DateTime.MinValue)
                {
                    sql_UpdateUserInsurance += " INS_EXPIRY_DATE = NULL, ";
                }
                else
                {
                    sql_UpdateUserInsurance += " INS_EXPIRY_DATE = " + Escape(user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd")) + ", ";
                }

                sql_UpdateUserInsurance += " INS_CERT_NUMBER = " + Escape(user.InsuranceInfo.CertificateNumber) + " " +

                " WHERE ID = " + Escape(user.Id) + " AND INS_YEAR = " + Escape(CurrentYear.ToString());
                db.ExecuteCommand(sql_UpdateUserInsurance);

                if (prevInsPlanHeld != user.InsuranceInfo.PlanHeld)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "INSURANCE.INS_PLAN_HELD (" + CurrentYear.ToString() + "): " + prevInsPlanHeld + " -> " + user.InsuranceInfo.PlanHeld);
                }
                if (prevInsInsureOther != user.InsuranceInfo.OtherInsuranceProvider)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "INSURANCE.INS_INSURE_OTHER (" + CurrentYear.ToString() + "): " + prevInsInsureOther + " -> " + user.InsuranceInfo.OtherInsuranceProvider);
                }
                if (prevInsExpiryDate != ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.ExpiryDate.ToString("MM/dd/yyyy")))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "INSURANCE.INS_EXPIRY_DATE (" + CurrentYear.ToString() + "): " + prevInsExpiryDate + " -> " + ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.ExpiryDate.ToString("MM/dd/yyyy")));
                }
                if (prevCertNumber != user.InsuranceInfo.CertificateNumber)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "INSURANCE.INS_CERT_NUMBER (" + CurrentYear.ToString() + "): " + prevCertNumber + " -> " + user.InsuranceInfo.CertificateNumber);
                }
            }
            else
            {
                int newSEQN = GetCounter("INSURANCE");
                string sql_InsertUserInsurance = "INSERT INTO INSURANCE (ID, SEQN, INS_YEAR, INS_PLAN_HELD, INS_INSURE_OTHER, " +
                " INS_EXPIRY_DATE, INS_CERT_NUMBER) VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", '" + CurrentYear.ToString() + "', '" +
                user.InsuranceInfo.PlanHeld + "', '" + user.InsuranceInfo.OtherInsuranceProvider + "', " +
                ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? " NULL " : ("'" + user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd") + "'")) + ", '" + user.InsuranceInfo.CertificateNumber + "') ";
                db.ExecuteCommand(sql_InsertUserInsurance);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "INSURANCE.INS_PLAN_HELD (" + CurrentYear.ToString() + "):  -> " + user.InsuranceInfo.PlanHeld);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "INSURANCE.INS_INSURE_OTHER (" + CurrentYear.ToString() + "): -> " + user.InsuranceInfo.OtherInsuranceProvider);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "INSURANCE.INS_EXPIRY_DATE (" + CurrentYear.ToString() + "):  -> " + ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd")));
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "INSURANCE.INS_CERT_NUMBER (" + CurrentYear.ToString() + "): -> " + user.InsuranceInfo.CertificateNumber);
            }
        }

        public void UpdateUserDeclarationInfo(User user, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT COUNT(1) FROM DECLARATION  " +
                " WHERE ID = " + Escape(user.Id) + " AND DECLARE_SIGNED_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                if ((int)row[0] == 0)
                {
                    int newSEQN = GetCounter("DECLARATION");
                    string sql_InsertUserDeclaration = "INSERT INTO DECLARATION (ID, SEQN, DECLARE_SIGNED_YEAR, DECLARE_SIGNED, QA_DECLARE_SIGNED) " +
                    "VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", '" + CurrentYear.ToString() + "', '" +
                    user.DeclarationInfo.RegistrationDeclaration + "', '" + user.DeclarationInfo.QADeclaration + "') ";
                    db.ExecuteCommand(sql_InsertUserDeclaration);
                }
                else
                {
                    string sql_UpdateUserConduct = "UPDATE DECLARATION SET " +
                    " DECLARE_SIGNED = '" + user.DeclarationInfo.RegistrationDeclaration + "', " +
                    " QA_DECLARE_SIGNED = '" + user.DeclarationInfo.QADeclaration + "' " +

                    " WHERE ID = " + Escape(user.Id) + " AND DECLARE_SIGNED_YEAR = '" + CurrentYear.ToString() + "'";
                    db.ExecuteCommand(sql_UpdateUserConduct);
                }
            }
        }

        public void UpdateUserDeclarationInfoLogged(string UserID, User user, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT * FROM DECLARATION  " +
                " WHERE ID = " + Escape(user.Id) + " AND DECLARE_SIGNED_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];
                string prevRegDeclaration = (string)row["DECLARE_SIGNED"];
                string prevQADeclaration = (string)row["QA_DECLARE_SIGNED"];

                string sql_UpdateUserConduct = "UPDATE DECLARATION SET " +
                " DECLARE_SIGNED = '" + user.DeclarationInfo.RegistrationDeclaration + "', " +
                " QA_DECLARE_SIGNED = '" + user.DeclarationInfo.QADeclaration + "' " +
                " WHERE ID = " + Escape(user.Id) + " AND DECLARE_SIGNED_YEAR = '" + CurrentYear.ToString() + "'";
                db.ExecuteCommand(sql_UpdateUserConduct);

                if (prevRegDeclaration != user.DeclarationInfo.RegistrationDeclaration)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "DECLARATION.DECLARE_SIGNED (" + CurrentYear.ToString() + "): " + prevRegDeclaration + " -> " + user.DeclarationInfo.RegistrationDeclaration);
                }
                if (prevQADeclaration != user.DeclarationInfo.QADeclaration)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "DECLARATION.QA_DECLARE_SIGNED (" + CurrentYear.ToString() + "): " + prevQADeclaration + " -> " + user.DeclarationInfo.QADeclaration);
                }

            }
            else
            {
                int newSEQN = GetCounter("DECLARATION");
                string sql_InsertUserDeclaration = "INSERT INTO DECLARATION (ID, SEQN, DECLARE_SIGNED_YEAR, DECLARE_SIGNED, QA_DECLARE_SIGNED) " +
                "VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", '" + CurrentYear.ToString() + "', '" +
                user.DeclarationInfo.RegistrationDeclaration + "', '" + user.DeclarationInfo.QADeclaration + "') ";
                db.ExecuteCommand(sql_InsertUserDeclaration);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "DECLARATION.DECLARE_SIGNED (" + CurrentYear.ToString() + "): -> " + user.DeclarationInfo.RegistrationDeclaration);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "DECLARATION.QA_DECLARE_SIGNED (" + CurrentYear.ToString() + "): -> " + user.DeclarationInfo.QADeclaration);
            }
        }

        #region Renewal new Methods

        #region Step 9

        public User GetRenewalUserInsuranceInfoNew(string User_Id)
        {
            User returnValue = null;

            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT TOP(1) ci.*, gt.Description FROM COTO_INSURANCE AS ci " +
                " LEFT OUTER JOIN Gen_Tables AS gt ON gt.CODE = ci.PLAN_WITH AND gt.Table_Name = 'INSURANCE' " +
                " WHERE ci.ID = " + Escape(User_Id) + " ORDER BY ci.SEQN DESC";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];

                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.InsuranceInfo = new COTO_RegOnly.Classes.Insurance();
                returnValue.InsuranceInfo.PlanHeld = (string)row["PLAN_WITH"];
                if (row["Description"] != DBNull.Value)
                {
                    returnValue.InsuranceInfo.PlanHeldName = (string)row["Description"];
                }
                else
                {
                    returnValue.InsuranceInfo.PlanHeldName = (string)row["PLAN_WITH"];
                }
                returnValue.InsuranceInfo.SEQN = (int)row["SEQN"];
                returnValue.InsuranceInfo.OtherInsuranceProvider = (string)row["PLAN_WITH_OTHER"];
                if (row["EFFECTIVE_DATE"] != DBNull.Value)
                {
                    returnValue.InsuranceInfo.StartDate = (DateTime)row["EFFECTIVE_DATE"];
                }
                else
                {
                    returnValue.InsuranceInfo.StartDate = DateTime.MinValue;
                }
                if (row["EXPIRY_DATE"] != DBNull.Value)
                {
                    returnValue.InsuranceInfo.ExpiryDate = (DateTime)row["EXPIRY_DATE"];
                }
                else
                {
                    returnValue.InsuranceInfo.ExpiryDate = DateTime.MinValue;
                }
                returnValue.InsuranceInfo.CertificateNumber = (string)row["CERTIFICATE"];
            }
            return returnValue;
        }

        #endregion

        #endregion

        #region RenewalPaymentChecks

        public decimal GetRenewalFeePaidAmount(string UserId, string ProductCode, int EffectiveYear)
        {
            decimal retValue = 0;

            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = string.Format("SELECT ISNULL(SUM(ABS(AMOUNT)),0) as 'AMOUNT' FROM Trans WHERE BT_ID = {0} " +
            " AND PRODUCT_CODE = {1} AND YEAR(TRANSACTION_DATE) = {2} AND MONTH(Transaction_date)>=3 ", Escape(UserId), Escape(ProductCode), EffectiveYear);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                retValue = (decimal)row["AMOUNT"];
            }
            return retValue;
        }

        public decimal GetRenewalFee(string ProductCode)
        {
            decimal retValue = 0;

            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = string.Format("SELECT Price_1 FROM Product WHERE PRODUCT_CODE = {0} ", Escape(ProductCode));
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                retValue = (decimal)row["Price_1"];
            }
            return retValue;
        }

        #endregion

        public DataTable GetGeneralInfo(string Table_name)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM Gen_Tables " +
                " WHERE Table_name = " + Escape(Table_name) + " ORDER BY CODE";
            var table = db.GetData(sql);
            return table;
        }

        public List<GenClass> GetGeneralList(string Table_name)
        {
            var list = new List<GenClass>();
            var table = GetGeneralInfo(Table_name);
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    var example = new GenClass { Code = (string)row["CODE"], Substitute = (string)row["SUBSTITUTE"], Description = (string)row["DESCRIPTION"] };
                    list.Add(example);
                }
            }
            return list;
        }

        public List<GenClass> GetCountriesNew()
        {
            var list = new List<GenClass>();

            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "select country, country_code, iso_3digit_code from country_names WHERE iso_3digit_code<>'' ORDER BY Country";
            var table = db.GetData(sql);

            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    var example = new GenClass { Code = (string)row["country_code"], Substitute = (string)row["iso_3digit_code"], Description = (string)row["country"] };
                    list.Add(example);
                }
            }

            return list;
        }

        public List<GenClass> GetCanadianProvincesList()
        {
            var list = new List<GenClass>();

            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM State_Codes ORDER BY STATE_PROVINCE";
            var table = db.GetData(sql);

            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    var example = new GenClass { Code = (string)row["STATE_PROVINCE"], Substitute = (string)row["TITLE"], Description = (string)row["TITLE"] };
                    list.Add(example);
                }
            }

            return list;
        }

        public List<GenClass> GetCanadianProvincesListNew()
        {
            var list = new List<GenClass>();

            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM State_Codes WHERE STATE_PROVINCE NOT IN ('N/A', '98') ORDER BY STATE_PROVINCE";
            var table = db.GetData(sql);

            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    var example = new GenClass { Code = (string)row["STATE_PROVINCE"], Substitute = (string)row["TITLE"], Description = (string)row["TITLE"] };
                    list.Add(example);
                }
            }

            return list;
        }

        public List<GenClass> GetUSStatesList()
        {
            var list = new List<GenClass>();

            list = GetGeneralList("FROM_STATE");

            return list;
        }

        public List<GenClass> GetStatuses()
        {
            return GetGeneralList("Member_Status");
        }

        public List<GenClass> GetLanguagesServices()
        {
            return GetGeneralList("LANGUAGES_OF_SERVICE");
        }

        public string GetGeneralName(string Table_name, string ID)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            string retValue = string.Empty;

            var db = new clsDB();
            string sql = "SELECT * FROM Gen_tables " +
                " WHERE Table_name = " + Escape(Table_name) + " AND CODE = " + Escape(ID);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                retValue = (string)row["DESCRIPTION"];
                return retValue;
            }
            return retValue;
        }

        public string GetGeneralNameImprv(string Table_name, string ID)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            string retValue = ID;

            var db = new clsDB();
            string sql = "SELECT * FROM Gen_tables " +
                " WHERE Table_name = " + Escape(Table_name) + " AND CODE = " + Escape(ID);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                retValue = (string)row["DESCRIPTION"];
                return retValue;
            }
            return retValue;
        }

        public string GetGeneralNameSubstImprv(string Table_name, string ID)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            string retValue = ID;

            var db = new clsDB();
            string sql = "SELECT * FROM Gen_tables " +
                " WHERE Table_name = " + Escape(Table_name) + " AND CODE =" + Escape(ID);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                retValue = (string)row["DESCRIPTION"];
                return retValue;
            }
            return retValue;
        }

        public void AddLogEntry(DateTime UpdateDate, string Log_type, string SubType, string UserId, string ID, string LogText)
        {
            var db = new clsDB();
            string sql = string.Format("Insert into Name_Log ([Date_Time], [Log_Type], [Sub_Type], [User_ID], [ID], [Log_Text]) " +
                " values ({0}, {1}, {2}, {3}, {4}, {5})", Escape(UpdateDate.ToString("yyyy-MM-dd HH:mm:ss")), Escape(Log_type), Escape(SubType), Escape(UserId), Escape(ID), Escape(LogText));
            db.ExecuteCommand(sql);
        }

        public void AddErrorEntry(string Message, string UserId, DateTime Created)
        {
            if (WebConfigItems.SaveErrorMessage)
            {
                var db = new clsDB();
                string sql = string.Format("Insert into RP_EMAIL_MESSAGES (Message, [UID]) values ({0}, {1})", Escape(Message), Escape(UserId));
                // (Created!=null)?(((DateTime)Created).ToString("yyyy-MM-dd HH:mm:ss")): "NULL"
                db.ExecuteCommand(sql);
            }
        }


        private string Escape(string val)
        {
            string str;
            if (string.IsNullOrEmpty(val))
            {
                val = string.Empty;
            }
            str = val.Replace("'", "''");
            str = "'" + str + "'";
            return str;
        }

        public int GetCounter(string CounterName)
        {
            int returnValue = 1;
            string dbconn = WebConfigItems.GetConnectionString();
            var ds = SQLHelper.ExecuteDataset(dbconn, "sp_asi_GetCounter", CounterName, 1, 0);
            if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count == 1)
            {
                returnValue = (int)ds.Tables[0].Rows[0].ItemArray[0];
            }
            return returnValue;
        }

        #region Application

        // step 1
        public User GetUserRegistrationInfo(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = " SELECT Name.Last_Name, Name.Middle_Name, Name.First_Name, Name.Full_Name, Name.Category, Name.Status, COTO_REGDATA.ID AS [COTO_REGDATA_ID], COTO_REGDATA.Citizen, COTO_REGDATA.LEGAL_PREVIOUS_FIRST_NAME, COTO_REGDATA.LEGAL_PREVIOUS_LAST_NAME, COTO_REGDATA.Legal_First_Name, COTO_REGDATA.Legal_Last_Name, COTO_APPLICATION.REGISTER_BY " +
                         " FROM Name LEFT OUTER JOIN COTO_REGDATA ON Name.ID = COTO_REGDATA.ID " + 
                         " LEFT OUTER JOIN COTO_APPLICATION ON Name.Id = COTO_APPLICATION.ID " + 
                         " WHERE NAME.ID  = " + Escape(User_Id);

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                returnValue = new User();
                if (row["Legal_Last_Name"] != DBNull.Value) returnValue.LegalLastName = (string)row["Legal_Last_Name"];
                if (row["Legal_First_Name"] != DBNull.Value) returnValue.LegalFirstName = (string)row["Legal_First_Name"];
                returnValue.FullName = (string)row["Full_Name"];
                //returnValue.LegalMiddleName = (string)row["MIDDLE_NAME"];
                if (row["LEGAL_PREVIOUS_LAST_NAME"] != DBNull.Value) returnValue.PreviousLegalLastName = (string)row["LEGAL_PREVIOUS_LAST_NAME"];
                if (row["LEGAL_PREVIOUS_FIRST_NAME"] != DBNull.Value) returnValue.PreviousLegalFirstName = (string)row["LEGAL_PREVIOUS_FIRST_NAME"];
                returnValue.CommonlyUsedLastName = (string)row["LAST_NAME"];
                returnValue.CommonlyUsedFirstName = (string)row["FIRST_NAME"];
                returnValue.CommonlyUsedMiddleName = (string)row["Middle_Name"];
                if (row["CITIZEN"]!=DBNull.Value) returnValue.Citizenship = (string)row["CITIZEN"];
                returnValue.Category = (string)row["CATEGORY"];
                returnValue.CurrentStatus =  GetGeneralName("MEMBER_STATUS", (string)row["STATUS"]);
                if (row["REGISTER_BY"] != DBNull.Value) returnValue.RegisterBy = (DateTime)row["REGISTER_BY"];

                if (row["COTO_REGDATA_ID"] == DBNull.Value)
                {
                    string sql_CreateNew = " INSERT INTO COTO_REGDATA (ID) VALUES (" + Escape(User_Id) + ")";
                    db.ExecuteCommand(sql_CreateNew);
                }
            }

            return returnValue;
        }

        public void UpdateApplicationUserRegistrationApplicationLogged(string UserID, User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string prevCategory = string.Empty;
            string prevCitizen = string.Empty;
            string prevRegisterBy = string.Empty;
            string sql_GetCurrentData = " SELECT * FROM COTO_REGDATA WHERE  ID = " + Escape(UserID);

            var table = db.GetData(sql_GetCurrentData);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                prevCitizen = (string)row["CITIZEN"];
            }
            else
            { //just in case if record doesn't exist
                string sql_CreateNew = " INSERT INTO COTO_REGDATA (ID) VALUES (" + Escape(UserID) + ")";
                db.ExecuteCommand(sql_CreateNew);
            }

            string sql_UpdateUser = "UPDATE COTO_REGDATA SET " +
                    " CITIZEN = " + Escape(user.Citizenship);
            //if (user.RegisterBy != DateTime.MinValue) sql_UpdateUser += ", REGISTER_BY = " + Escape(user.RegisterBy.ToString("yyyy-MM-dd"));
                    sql_UpdateUser += " WHERE ID = " + Escape(user.Id);
            db.ExecuteCommand(sql_UpdateUser);

            if (prevCitizen != user.Citizenship)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.CITIZEN: " + prevCitizen + " -> " + user.Citizenship);
            }
            

            string sql_GetName = " SELECT * FROM Name " +
                    " WHERE  Name.ID = " + Escape(user.Id);

            var table2 = db.GetData(sql_GetName);
            if (table2 != null && table2.Rows.Count == 1)
            {
                DataRow row = table2.Rows[0];
                prevCategory = (string)row["CATEGORY"];
            }

            string sql_UpdateCategory = "UPDATE NAME SET " +
                    " CATEGORY = " + Escape(user.Category) +
                    " WHERE ID = " + Escape(user.Id);
            db.ExecuteCommand(sql_UpdateCategory);

            if (prevCategory != user.Category)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "NAME.CATEGORY: " + prevCategory + " -> " + user.Category);
            }

            string sql_GetApplication = " SELECT * FROM COTO_APPLICATION " +
                    " WHERE  ID = " + Escape(user.Id);

            var table3 = db.GetData(sql_GetApplication);
            if (table3 != null && table3.Rows.Count == 1)
            {
                DataRow row = table3.Rows[0];
                if (row["REGISTER_BY"] != DBNull.Value) prevRegisterBy = ((DateTime)row["REGISTER_BY"]).ToString("yyyy-MM-dd"); 
            }

            string currentRegisterBy = string.Empty;
            if (user.RegisterBy != DateTime.MinValue) currentRegisterBy = user.RegisterBy.ToString("yyyy-MM-dd");
            
            if (prevRegisterBy != currentRegisterBy)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_APPLICATION.REGISTER_BY: " + prevRegisterBy + " -> " + currentRegisterBy);
            }
            //if (string.IsNullOrEmpty(currentRegisterBy)) currentRegisterBy = "NULL";

            string sql_UpdateApplication = "UPDATE COTO_APPLICATION SET " +
                " REGISTER_BY = " + ((user.RegisterBy != DateTime.MinValue)? Escape(currentRegisterBy) : "NULL") +
                    " WHERE ID = " + Escape(user.Id);
            db.ExecuteCommand(sql_UpdateApplication);
            
        }

        public void UpdateApplicationConductSubmitted(string UserID, ApplicationStatus application)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string prevConductSubmitted = string.Empty;

            string sql_GetApplication = " SELECT * FROM COTO_APPLICATION " +
                    " WHERE  ID = " + Escape(UserID);

            var table3 = db.GetData(sql_GetApplication);
            if (table3 != null && table3.Rows.Count == 1)
            {
                DataRow row = table3.Rows[0];
                if (row["CONDUCT_SUBMITTED"] != DBNull.Value) prevConductSubmitted = ((DateTime)row["CONDUCT_SUBMITTED"]).ToString("yyyy-MM-dd");
            }

            string currentConductSubmitted = string.Empty;
            if (application.ConductSubmitted != DateTime.MinValue) currentConductSubmitted = application.ConductSubmitted.ToString("yyyy-MM-dd");

            if (prevConductSubmitted != currentConductSubmitted)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_APPLICATION.CONDUCT_SUBMITTED: '" + prevConductSubmitted + "' -> '" + currentConductSubmitted + "'");
            }
            //if (string.IsNullOrEmpty(currentRegisterBy)) currentRegisterBy = "NULL";

            string sql_UpdateApplication = "UPDATE COTO_APPLICATION SET " +
                " CONDUCT_SUBMITTED = " + ((application.ConductSubmitted != DateTime.MinValue) ? Escape(application.ConductSubmitted.ToString("yyyy-MM-dd")) : "NULL") +
                    " WHERE ID = " + Escape(UserID);
            db.ExecuteCommand(sql_UpdateApplication);
        }

        // step 2
        public User GetApplicationUserInfo(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT Name.ID AS [Name_ID], Name.*, Curr.*, App.REGISTER_BY, Curr.MIDDLE_NAME AS [MID_NAME] FROM Name " +
                "  INNER JOIN COTO_REGDATA Curr ON Name.Id = Curr.id " +  
                "  INNER JOIN COTO_APPLICATION App ON Name.Id = App.id " +      // CURRENCY
                " WHERE Name.ID = '" + User_Id + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User
                {
                    Id = (string)row["Name_ID"],
                    FullName = (string)row["FIRST_NAME"] + " " + (string)row["LAST_NAME"], //(string)row["FULL_NAME"],
                    FullAddress = (string)row["FULL_ADDRESS"],

                    LegalLastName = (string)row["Legal_Last_Name"],
                    LegalFirstName = (string)row["Legal_First_Name"],
                    LegalMiddleName = (string)row["MID_NAME"],
                    PreviousLegalLastName = (string)row["LEGAL_PREVIOUS_LAST_NAME"],
                    PreviousLegalFirstName = (string)row["LEGAL_PREVIOUS_FIRST_NAME"],
                    CommonlyUsedLastName = (string)row["LAST_NAME"],
                    CommonlyUsedFirstName = (string)row["FIRST_NAME"],
                    CommonlyUsedMiddleName = (string)row["Middle_Name"],
                    
                    FirstName = (string)row["FIRST_NAME"],
                    LastName = (string)row["LAST_NAME"],
                    Registration = (string)row["MAJOR_KEY"],
                    Email = (string)row["EMAIL"],
                    HomePhone = (string)row["HOME_PHONE"],
                    //CertificateExpiry = (DateTime?)row["PAID_THRU"],
                    CurrentStatus = (string)row["STATUS"],
                };
                if (row["REGISTER_BY"] != DBNull.Value) returnValue.RegisterBy = (DateTime)row["REGISTER_BY"];
                if (row["PAID_THRU"] != DBNull.Value)
                {
                    returnValue.CertificateExpiry = (DateTime)row["PAID_THRU"];
                }
                else
                {
                    returnValue.CertificateExpiry = DateTime.MinValue;
                }

                if (row["BIRTH_DATE_COTO"] != DBNull.Value)
                {
                    returnValue.BirthDate = (DateTime)row["BIRTH_DATE_COTO"];
                }
                else
                {
                    returnValue.BirthDate = DateTime.MinValue;
                }

                if (row["GENDER_COTO"] != DBNull.Value)
                {
                    returnValue.Gender = (string)row["GENDER_COTO"];
                }

                if (row["GENDER_DESCRIBE"] != DBNull.Value)
                {
                    returnValue.GenderSelfDescribe = (string)row["GENDER_DESCRIBE"];
                }

                returnValue.Category = GetGeneralName("CATEGORY", (string)row["CATEGORY"]);
                returnValue.CurrentStatus = GetGeneralName("MEMBER_STATUS", (string)row["STATUS"]);
            }

            string sql_address = "SELECT * FROM Name_Address WHERE ID = '" + User_Id + "' AND PURPOSE='Home'";
            var table_address = db.GetData(sql_address);
            if (table_address != null && table_address.Rows.Count > 0)
            {
                var row = table_address.Rows[0];
                var address = new Address
                {
                    Address1 = (string)row["ADDRESS_1"],
                    Address2 = (string)row["ADDRESS_2"],
                    Address3 = (string)row["ADDRESS_3"],
                    City = (string)row["CITY"],
                    Province = (string)row["STATE_PROVINCE"],
                    PostalCode = (string)row["ZIP"],
                    Country = (string)row["COUNTRY"]
                };
                returnValue.HomeAddress = address;
            }
            return returnValue;
        }

        public string UpdateApplicationUserInfoLogged(string UserID, User user, bool SaveBirthDate)
        {
            string errorMessage = string.Empty;
            try
            {

                string prevEmail = string.Empty;
                string prevHomePhone = string.Empty;
                string prevFullAddress = string.Empty;

                string prevHomeAddress1 = string.Empty;
                string prevHomeAddress2 = string.Empty;
                string prevHomeAddress3 = string.Empty;

                string prevHomeCity = string.Empty;
                string prevHomeProvince = string.Empty;
                string prevHomeCountry = string.Empty;
                string prevHomePostalCode = string.Empty;

                //var db = new clsDB();
                string dbconn = WebConfigItems.GetConnectionString();
                var db = new clsDB();

                bool pref_Mail = false;

                string sql_GetHomeAddress = " SELECT * FROM Name_Address " +
                    " WHERE  Name_Address.ID = " + Escape(user.Id) + " AND Name_Address.PURPOSE='Home'";
                //db.ExecuteCommand(sql_GetHomeAddress);

                var table = db.GetData(sql_GetHomeAddress);
                if (table != null && table.Rows.Count == 1)
                {
                    DataRow row = table.Rows[0];
                    int address_num = (int)row["ADDRESS_NUM"];
                    pref_Mail = (bool)row["PREFERRED_MAIL"];

                    prevHomePhone = (string)row["PHONE"];
                    prevFullAddress = (string)row["FULL_ADDRESS"];

                    prevHomeAddress1 = (string)row["ADDRESS_1"];
                    prevHomeAddress2 = (string)row["ADDRESS_2"];
                    prevHomeAddress3 = (string)row["ADDRESS_3"];

                    prevHomeCity = (string)row["CITY"];
                    prevHomeProvince = (string)row["STATE_PROVINCE"];
                    prevHomeCountry = (string)row["COUNTRY"];
                    prevHomePostalCode = (string)row["ZIP"];

                    string sql_UpdateUserAddress = "UPDATE Name_Address SET " +
                        " PHONE = " + Escape(user.HomePhone) + ", " +
                        " ADDRESS_1 = " + Escape(user.HomeAddress.Address1) + ", " +
                        " ADDRESS_2 = " + Escape(user.HomeAddress.Address2) + ", " +
                        " ADDRESS_3 = " + Escape(user.HomeAddress.Address3) + ", " +
                        " CITY = " + Escape(user.HomeAddress.City) + ", " +
                        " STATE_PROVINCE = " + Escape(user.HomeAddress.Province) + ", " +
                        " ZIP = " + Escape(user.HomeAddress.PostalCode) + ", " +
                        " COUNTRY = " + Escape(user.HomeAddress.Country) + ", " +
                        " FULL_ADDRESS = " + Escape(user.FullAddressDB) + ", " +
                        " LAST_UPDATED = Getdate() " +
                        " WHERE ID = " + Escape(user.Id) + " AND PURPOSE='Home'";

                    db.ExecuteCommand(sql_UpdateUserAddress);

                    

                    // save changes in name_address to name_log table

                    if (prevHomeAddress1 != user.HomeAddress.Address1)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_1 ( Addr_Num:" + address_num.ToString() + "): " + prevHomeAddress1 + " -> " + user.HomeAddress.Address1);
                    }

                    if (prevHomeAddress2 != user.HomeAddress.Address2)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_2 ( Addr_Num:" + address_num.ToString() + "): " + prevHomeAddress2 + " -> " + user.HomeAddress.Address2);
                    }

                    if (prevHomeAddress3 != user.HomeAddress.Address3)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_3 ( Addr_Num:" + address_num.ToString() + "): " + prevHomeAddress3 + " -> " + user.HomeAddress.Address3);
                    }

                    if (prevHomeCity != user.HomeAddress.City)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.CITY ( Addr_Num:" + address_num.ToString() + "): " + prevHomeCity + " -> " + user.HomeAddress.City);
                    }

                    if (prevHomeProvince != user.HomeAddress.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.STATE_PROVINCE ( Addr_Num:" + address_num.ToString() + "): " + prevHomeProvince + " -> " + user.HomeAddress.Province);
                    }

                    if (prevHomeCountry != user.HomeAddress.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.COUNTRY ( Addr_Num:" + address_num.ToString() + "): " + prevHomeCountry + " -> " + user.HomeAddress.Country);
                    }

                    if (prevHomePostalCode != user.HomeAddress.PostalCode)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.ZIP ( Addr_Num:" + address_num.ToString() + "): " + prevHomePostalCode + " -> " + user.HomeAddress.PostalCode);
                    }

                    if (prevFullAddress != user.FullAddressDB)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.FULL_ADDRESS ( Addr_Num:" + address_num.ToString() + "): " + prevFullAddress + " -> " + user.FullAddressDB);
                    }

                    if (prevHomePhone != user.HomePhone)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name_ADDRESS.PHONE ( Addr_Num:" + address_num.ToString() + "): " + prevHomePhone + " -> " + user.HomePhone);
                    }

                }
                else if (table != null && table.Rows.Count == 0)
                {
                    int newSEQN = GetCounter("Name_Address");
                    string sql_InsertNewUserAddress = "INSERT INTO Name_Address " +
                        " (ID, ADDRESS_NUM, PURPOSE, ADDRESS_1, ADDRESS_2, ADDRESS_3, CITY, STATE_PROVINCE, ZIP, COUNTRY, FULL_ADDRESS, LAST_UPDATED, PHONE) ";
                    sql_InsertNewUserAddress += string.Format(" VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12})", Escape(user.Id), newSEQN.ToString(), Escape("Home"), Escape(user.HomeAddress.Address1),
                         Escape(user.HomeAddress.Address2), Escape(user.HomeAddress.Address3), Escape(user.HomeAddress.City), Escape(user.HomeAddress.Province), Escape(user.HomeAddress.PostalCode),
                         Escape(user.HomeAddress.Country), Escape(user.FullAddressDB), "Getdate()", Escape(user.HomePhone));
                    db.ExecuteCommand(sql_InsertNewUserAddress);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_1 ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Address1);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_2 ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Address2);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.ADDRESS_3 ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Address3);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.CITY ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.City);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.STATE_PROVINCE ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Province);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.COUNTRY ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.Country);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.ZIP ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomeAddress.PostalCode);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.FULL_ADDRESS ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.FullAddressDB);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "Name_ADDRESS.PHONE ( Addr_Num:" + newSEQN.ToString() + "): -> " + user.HomePhone);

                }

                // this section saves old values of email and home phone if they are different than new values
                // also saves old value of full_address if it preferred mail
                string prev_Email = string.Empty;
                string prev_HomePhone = string.Empty;
                string full_address = string.Empty;
                string prevGender = string.Empty;
                string prevGenderDescribe = string.Empty;
                string prevPrevLegalLastName = string.Empty;
                string prevPrevLegalFirstName = string.Empty;
                string prevCommonlyUsedLastName = string.Empty;
                string prevCommonlyUsedFirstName = string.Empty;
                //string prevCommonlyUsedMiddleName = string.Empty;

                string prevLegalLastName = string.Empty;
                string prevLegalFirstName = string.Empty;
                string prevLegalMiddleName = string.Empty;

                string prevBirthDate = string.Empty;

                string sql_GetNameInfo = " SELECT Name.*, CRD.LEGAL_PREVIOUS_LAST_NAME, CRD.LEGAL_PREVIOUS_FIRST_NAME, CRD.GENDER_COTO, CRD.GENDER_DESCRIBE, CRD.BIRTH_DATE_COTO, " + 
                    " CRD.LEGAL_FIRST_NAME, CRD.LEGAL_LAST_NAME, CRD.MIDDLE_NAME AS [MID_NAME] FROM Name " +
                    " LEFT OUTER JOIN COTO_REGDATA AS CRD ON CRD.ID = Name.ID " +
                    " WHERE  Name.ID = " + Escape(user.Id);
                var table2 = db.GetData(sql_GetNameInfo);

                if (table2 != null && table2.Rows.Count == 1)
                {
                    DataRow row2 = table2.Rows[0];
                    prev_Email = (string)row2["EMAIL"];
                    prev_HomePhone = (string)row2["HOME_PHONE"];
                    full_address = (string)row2["FULL_ADDRESS"];
                    prevGender = (string)row2["GENDER_COTO"]; 
                    prevGenderDescribe = (string)row2["GENDER_DESCRIBE"];
                    prevPrevLegalLastName = (string)row2["LEGAL_PREVIOUS_LAST_NAME"];
                    prevPrevLegalFirstName = (string)row2["LEGAL_PREVIOUS_FIRST_NAME"];
                    prevCommonlyUsedLastName = (string)row2["LAST_NAME"];
                    prevCommonlyUsedFirstName = (string)row2["FIRST_NAME"];
                    //prevCommonlyUsedMiddleName = (string)row2["MIDDLE_NAME"];
                    prevLegalLastName = (string)row2["LEGAL_LAST_NAME"];
                    prevLegalFirstName = (string)row2["LEGAL_FIRST_NAME"];
                    prevLegalMiddleName = (string)row2["MID_NAME"];

                    if (row2["BIRTH_DATE_COTO"]!= DBNull.Value)
                    {
                        prevBirthDate = ((DateTime)row2["BIRTH_DATE_COTO"]).ToString("yyyy/MM/dd");
                    }
                }

                if (!user.Email.Equals(prev_Email))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name.EMAIL: " + prev_Email + " -> " + user.Email);
                }

                if (!user.HomePhone.Equals(prev_HomePhone))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name.HOME_PHONE: " + prev_HomePhone + " -> " + user.HomePhone);
                }

                if (pref_Mail && !user.FullAddressDB.Equals(full_address))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "Name.FULL_ADDRESS: " + full_address + " -> " + user.FullAddressDB);
                }

                if (prevGender != user.Gender)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.Gender_COTO : " + prevGender + " -> " + user.Gender);
                }

                if (prevGenderDescribe != user.GenderSelfDescribe)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.GENDER_DESCRIBE : " + prevGenderDescribe + " -> " + user.GenderSelfDescribe);
                }

                if (SaveBirthDate &&  (prevBirthDate != user.BirthDate.ToString("yyyy/MM/dd")))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.BIRTH_DATE_COTO : " + prevBirthDate + " -> " + user.BirthDate.ToString("yyyy/MM/dd"));
                }

                if ( user.CurrentStatus=="New" && prevPrevLegalLastName != user.PreviousLegalLastName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LEGAL_PREVIOUS_LAST_NAME : " + prevPrevLegalLastName + " -> " + user.PreviousLegalLastName);
                }

                if (user.CurrentStatus == "New" && prevPrevLegalFirstName != user.PreviousLegalFirstName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LEGAL_PREVIOUS_FIRST_NAME : " + prevPrevLegalFirstName + " -> " + user.PreviousLegalFirstName);
                }

                if (prevCommonlyUsedLastName != user.CommonlyUsedLastName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "NAME.LAST_NAME : " + prevCommonlyUsedLastName + " -> " + user.LegalMiddleName);
                }

                if (prevCommonlyUsedFirstName != user.CommonlyUsedFirstName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "NAME.FIRST_NAME : " + prevCommonlyUsedFirstName + " -> " + user.CommonlyUsedFirstName);
                }

                //if (prevCommonlyUsedMiddleName != user.CommonlyUsedMiddleName)
                //{
                //    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "NAME.MIDDLE_NAME : " + prevCommonlyUsedMiddleName + " -> " + user.CommonlyUsedMiddleName);
                //}

                if (prevLegalLastName != user.LegalLastName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LEGAL_LAST_NAME : " + prevLegalLastName + " -> " + user.LegalLastName);
                }

                if (prevLegalFirstName != user.LegalFirstName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LEGAL_FIRST_NAME : " + prevLegalFirstName + " -> " + user.LegalFirstName);
                }

                if (prevLegalMiddleName != user.LegalMiddleName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.MIDDLE_NAME : " + prevLegalMiddleName + " -> " + user.LegalMiddleName);
                }

                string sql_UpdateUser = "UPDATE Name SET " +
                        " EMAIL = " + Escape(user.Email) +
                        ", HOME_PHONE = " + Escape(user.HomePhone);
                sql_UpdateUser += ", FIRST_NAME = " + Escape(user.CommonlyUsedFirstName);
                sql_UpdateUser += ", LAST_NAME = " + Escape(user.CommonlyUsedLastName);
                //sql_UpdateUser += ", MIDDLE_NAME = " + Escape(user.CommonlyUsedMiddleName);
                
                if (pref_Mail)
                {
                    sql_UpdateUser += ", FULL_ADDRESS = " + Escape(user.FullAddressDB);
                    sql_UpdateUser += ", CITY = " + Escape(user.HomeAddress.City);
                    sql_UpdateUser += ", STATE_PROVINCE = " + Escape(user.HomeAddress.Province);
                    sql_UpdateUser += ", ZIP = " + Escape(user.HomeAddress.PostalCode);
                    sql_UpdateUser += ", COUNTRY = " + Escape(user.HomeAddress.Country);
                }
                sql_UpdateUser += " WHERE ID = " + Escape(user.Id);
                db.ExecuteCommand(sql_UpdateUser);

                string sql_UpdateRegdata = "UPDATE COTO_REGDATA SET " +
                        " GENDER_COTO = " + Escape(user.Gender) +
                        ", GENDER_DESCRIBE = " + Escape(user.GenderSelfDescribe);
                if (SaveBirthDate) sql_UpdateRegdata += ", BIRTH_DATE_COTO = " + Escape(user.BirthDate.ToString("yyyy-MM-dd"));
                if (user.CurrentStatus == "New")
                {
                    sql_UpdateRegdata += ", LEGAL_PREVIOUS_LAST_NAME = " + Escape(user.PreviousLegalLastName) +
                    ", LEGAL_PREVIOUS_FIRST_NAME = " + Escape(user.PreviousLegalFirstName);
                }
                sql_UpdateRegdata += ", LEGAL_LAST_NAME = " + Escape(user.LegalLastName) +
                   ", LEGAL_FIRST_NAME = " + Escape(user.LegalFirstName) + ", MIDDLE_NAME = " + Escape(user.LegalMiddleName);

                sql_UpdateRegdata += " WHERE ID = " + Escape(user.Id);

                db.ExecuteCommand(sql_UpdateRegdata);

            }
            catch (Exception ex)
            {
                errorMessage = "Error message: " + ex.Message + "; Error Inner Exception: " + ex.InnerException + " Error stackTrace: " + ex.StackTrace; // +"SQL = " + working_sql;
            }
            return errorMessage;

        }

        // step 3
        public User GetApplicationUserCurrencyLanguage(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM COTO_REGDATA " +
                " WHERE ID = '" + User_Id + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.CurrencyLanguageServices = new COTO_RegOnly.Classes.CurrencyLanguage();
                //returnValue.CurrencyLanguageServices.Currency = (string)row["CURRENCY_DATA"];
                returnValue.CurrencyLanguageServices.Lang_Service1 = (string)row["LANG_OF_SVCE_1"];
                returnValue.CurrencyLanguageServices.Lang_Service2 = (string)row["LANG_OF_SVCE_2"];
                returnValue.CurrencyLanguageServices.Lang_Service3 = (string)row["LANG_OF_SVCE_3"];
                returnValue.CurrencyLanguageServices.Lang_Service4 = (string)row["LANG_OF_SVCE_4"];
                returnValue.CurrencyLanguageServices.Lang_Service5 = (string)row["LANG_OF_SVCE_5"];
                returnValue.CurrencyLanguageServices.First_Language = (string)row["FIRST_LANG"];
                returnValue.CurrencyLanguageServices.Lang_OT_Instr = (string)row["LANG_OT_INST"];
                returnValue.CurrencyLanguageServices.Lang_Preferred_Doc = (string)row["LANG_PREF"];

            }
            return returnValue;
        }

        public User GetApplicationUserCurrencyHoursInfo(string User_Id, int CurrentYear)
        {
            User returnValue = null;
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM CURRENCY_HOURS " +
                " WHERE ID = '" + User_Id + "' AND REGISTRATION_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.CurrencyLanguageServices = new COTO_RegOnly.Classes.CurrencyLanguage();
                returnValue.CurrencyLanguageServices.Currency = (string)row["CURRENCY_REQ_MET"];
            }
            return returnValue;
        }

        public void UpdateApplicationUserCurrencyLanguageLogged(string UserID, User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string prevLangService1 = string.Empty;
            string prevLangService2 = string.Empty;
            string prevLangService3 = string.Empty;
            string prevLangService4 = string.Empty;
            string prevLangService5 = string.Empty;
            string prevFirstLang = string.Empty;
            string prevOTInstr = string.Empty;
            string prevPrefDoc = string.Empty;

            string sql_GetCurrentData = " SELECT * FROM COTO_REGDATA WHERE  ID = " + Escape(user.Id);
            //db.ExecuteCommand(sql_GetHomeAddress);

            var table = db.GetData(sql_GetCurrentData);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                prevLangService1 = (string)row["LANG_OF_SVCE_1"];
                prevLangService2 = (string)row["LANG_OF_SVCE_2"];
                prevLangService3 = (string)row["LANG_OF_SVCE_3"];
                prevLangService4 = (string)row["LANG_OF_SVCE_4"];
                prevLangService5 = (string)row["LANG_OF_SVCE_5"];
                prevFirstLang = (string)row["FIRST_LANG"];
                prevOTInstr = (string)row["LANG_OT_INST"];
                prevPrefDoc = (string)row["LANG_PREF"];
            }
            else
            { //just in case if record doesn't exist
                string sql_CreateNew = " INSERT INTO COTO_REGDATA (ID) VALUES (" + Escape(user.Id) + ")";
                db.ExecuteCommand(sql_CreateNew);
            }

            string sql_UpdateUser = "UPDATE COTO_REGDATA SET " +
                //" CURRENCY_DATA = '" + user.CurrencyLanguageServices.Currency + "', " +
                    " FIRST_LANG = " + Escape( user.CurrencyLanguageServices.First_Language) + ", " +
                    " LANG_OT_INST = " + Escape(user.CurrencyLanguageServices.Lang_OT_Instr) + ", " +
                    " LANG_PREF = " + Escape(user.CurrencyLanguageServices.Lang_Preferred_Doc) + ", " +
                    " LANG_OF_SVCE_1 = " + Escape(user.CurrencyLanguageServices.Lang_Service1) + ", " +
                    " LANG_OF_SVCE_2 = " + Escape(user.CurrencyLanguageServices.Lang_Service2) + ", " +
                    " LANG_OF_SVCE_3 = " + Escape(user.CurrencyLanguageServices.Lang_Service3) + ", " +
                    " LANG_OF_SVCE_4 = " + Escape(user.CurrencyLanguageServices.Lang_Service4) + ", " +
                    " LANG_OF_SVCE_5 = " + Escape(user.CurrencyLanguageServices.Lang_Service5) + 
                    " WHERE ID = " + Escape(user.Id);
            db.ExecuteCommand(sql_UpdateUser);

            if (prevFirstLang != user.CurrencyLanguageServices.First_Language)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.FIRST_LANG: " + prevFirstLang + " -> " + user.CurrencyLanguageServices.First_Language);
            }

            if (prevOTInstr != user.CurrencyLanguageServices.Lang_OT_Instr)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LANG_OT_INST: " + prevOTInstr + " -> " + user.CurrencyLanguageServices.Lang_OT_Instr);
            }

            if (prevPrefDoc != user.CurrencyLanguageServices.Lang_Preferred_Doc)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LANG_PREF: " + prevPrefDoc + " -> " + user.CurrencyLanguageServices.Lang_Preferred_Doc);
            }

            if (prevLangService1 != user.CurrencyLanguageServices.Lang_Service1)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LANG_OF_SVCE_1: " + prevLangService1 + " -> " + user.CurrencyLanguageServices.Lang_Service1);
            }

            if (prevLangService2 != user.CurrencyLanguageServices.Lang_Service2)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LANG_OF_SVCE_2: " + prevLangService2 + " -> " + user.CurrencyLanguageServices.Lang_Service2);
            }

            if (prevLangService3 != user.CurrencyLanguageServices.Lang_Service3)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LANG_OF_SVCE_3: " + prevLangService3 + " -> " + user.CurrencyLanguageServices.Lang_Service3);
            }

            if (prevLangService4 != user.CurrencyLanguageServices.Lang_Service4)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LANG_OF_SVCE_4: " + prevLangService4 + " -> " + user.CurrencyLanguageServices.Lang_Service4);
            }

            if (prevLangService5 != user.CurrencyLanguageServices.Lang_Service5)
            {
                AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.LANG_OF_SVCE_5: " + prevLangService5 + " -> " + user.CurrencyLanguageServices.Lang_Service5);
            }
        }

        public void UpdateApplicationUserCurrencyHoursInfoLogged(string UserID, User user, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT * FROM CURRENCY_HOURS  " +
                " WHERE ID = " + Escape(user.Id) + " AND REGISTRATION_YEAR = " + Escape(CurrentYear.ToString());
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 0)
            {
                // creating new record
                int newSEQN = GetCounter("CURRENCY_HOURS");
                string sql_InsertUserCurrencyHours = "INSERT INTO CURRENCY_HOURS (ID, SEQN, REGISTRATION_YEAR, CURRENCY_REQ_MET)" +
                    " VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", '" + CurrentYear.ToString() + "', '" + user.CurrencyLanguageServices.Currency + "')";
                db.ExecuteCommand(sql_InsertUserCurrencyHours);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CURRENCY_HOURS.CURRENCY_REQ_MET (" + CurrentYear.ToString() + "): -> " + user.CurrencyLanguageServices.Currency);
            }
            else
            {
                // updating prev record
                var row = table.Rows[0];
                string prevCurrency = (string)row["CURRENCY_REQ_MET"];

                string sql_UpdateUserCurrencyHours = "UPDATE CURRENCY_HOURS SET " +
                    " CURRENCY_REQ_MET = " + Escape(user.CurrencyLanguageServices.Currency) + 
                    " WHERE ID = " + Escape(user.Id) + " AND REGISTRATION_YEAR = " + Escape(CurrentYear.ToString());
                db.ExecuteCommand(sql_UpdateUserCurrencyHours);

                if (prevCurrency != user.CurrencyLanguageServices.Currency)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CURRENCY_HOURS.CURRENCY_REQ_MET (" + CurrentYear.ToString() + "): " + prevCurrency + " -> " + user.CurrencyLanguageServices.Currency);
                }
            }
        }

        // step 4
        public User GetApplicationUserEducationInfo(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM EDUCATION_ALL " +
                " WHERE ID = " + Escape(User_Id);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.Education = new UserEducation();
                returnValue.Education.EntryDegree = new Education();
                returnValue.Education.EntryDegree.DiplomaName = (string)row["DEGREE_ENTRY_OT"]; // GetGeneralName("EDUCATION_ALL", (string)row["DEGREE_ENTRY_OT"]);
                returnValue.Education.EntryDegree.CanadianUniversity = (string)row["UNIVERSITY_ENTRY_OT"];
                returnValue.Education.EntryDegree.OtherUniversity = (string)row["UNIVERSITY_ENTRY_OTHER_OT"];
                returnValue.Education.EntryDegree.OtherUniversityNotListed = (string)row["UNIV_ENTRY_OTHER_NL"];
                returnValue.Education.EntryDegree.ProvinceState = (string)row["PROVINCE_STATE_ENTRY_OT"];
                returnValue.Education.EntryDegree.Country = (string)row["COUNTRY_ENTRY_OT"];
                returnValue.Education.EntryDegree.GraduationYear = (string)row["GRAD_YEAR_ENTRY_OT"];

                returnValue.Education.Degree2 = new Education();
                returnValue.Education.Degree2.DiplomaName = (string)row["DEGREE_2_OT"]; // GetGeneralName("EDUCATION_ALL", (string)row["DEGREE_2_OT"]); 
                returnValue.Education.Degree2.CanadianUniversity = (string)row["UNIVERSITY_2_OT"];
                returnValue.Education.Degree2.OtherUniversity = (string)row["UNIVERSITY_2_OTHER_OT"];
                returnValue.Education.Degree2.OtherUniversityNotListed = (string)row["UNIV_2_OTHER_OT_NL"];
                returnValue.Education.Degree2.ProvinceState = (string)row["PROVINCE_STATE_2_OT"];
                returnValue.Education.Degree2.Country = (string)row["COUNTRY_2_OT"];
                returnValue.Education.Degree2.GraduationYear = (string)row["GRAD_YEAR_2_OT"];

                returnValue.Education.Degree3 = new Education();
                returnValue.Education.Degree3.DiplomaName = (string)row["DEGREE_3_OT"]; // GetGeneralName("EDUCATION_ALL", (string)row["DEGREE_3_OT"]); 
                returnValue.Education.Degree3.CanadianUniversity = (string)row["UNIVERSITY_3_OT"];
                returnValue.Education.Degree3.OtherUniversity = (string)row["UNIVERSITY_3_OTHER_OT"];
                returnValue.Education.Degree3.OtherUniversityNotListed = (string)row["UNIV_3_OTHER_OT_NL"];
                returnValue.Education.Degree3.ProvinceState = (string)row["PROVINCE_STATE_3_OT"];
                returnValue.Education.Degree3.Country = (string)row["COUNTRY_3_OT"];
                returnValue.Education.Degree3.GraduationYear = (string)row["GRAD_YEAR_3_OT"];

                returnValue.Education.OtherDegree1 = new Education();
                returnValue.Education.OtherDegree1.DiplomaName = (string)row["DEGREE_1_OTHER"]; // GetGeneralName("EDUCATION_ALL", (string)row["DEGREE_1_OTHER"]); 
                returnValue.Education.OtherDegree1.CanadianUniversity = (string)row["UNIVERSITY_1_OTHER"];
                returnValue.Education.OtherDegree1.OtherUniversity = (string)row["UNIVERSITY_1_INTL"];
                returnValue.Education.OtherDegree1.OtherUniversityNotListed = (string)row["UNIV_1_OTHER_INTL_NL"];
                returnValue.Education.OtherDegree1.StudyField = (string)row["FIELD_STUDY_1_OTHER"];
                returnValue.Education.OtherDegree1.ProvinceState = (string)row["STATE_PROV_1_OTHER"];
                returnValue.Education.OtherDegree1.Country = (string)row["COUNTRY_1_OTHER"];
                returnValue.Education.OtherDegree1.GraduationYear = (string)row["GRAD_YEAR_1_OTHER"];

                returnValue.Education.OtherDegree2 = new Education();
                returnValue.Education.OtherDegree2.DiplomaName = (string)row["DEGREE_2_OTHER"]; // GetGeneralName("EDUCATION_ALL", (string)row["DEGREE_2_OTHER"]); 
                returnValue.Education.OtherDegree2.CanadianUniversity = (string)row["UNIVERSITY_2_OTHER"];
                returnValue.Education.OtherDegree2.OtherUniversity = (string)row["UNIVERSITY_2_INTL"];
                returnValue.Education.OtherDegree2.OtherUniversityNotListed = (string)row["UNIV_2_OTHER_INTL_NL"];
                returnValue.Education.OtherDegree2.StudyField = (string)row["FIELD_STUDY_2_OTHER"];
                returnValue.Education.OtherDegree2.ProvinceState = (string)row["STATE_PROV_2_OTHER"];
                returnValue.Education.OtherDegree2.Country = (string)row["COUNTRY_2_OTHER"];
                returnValue.Education.OtherDegree2.GraduationYear = (string)row["GRAD_YEAR_2_OTHER"];
            }
            else
            {
                string sql_InsertNewEducation = "INSERT INTO EDUCATION_ALL (ID) VALUES (" + Escape(User_Id) + ")";
                db.ExecuteCommand(sql_InsertNewEducation);

                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.Education = new UserEducation();
                returnValue.Education.EntryDegree = new Education();
                returnValue.Education.EntryDegree.DiplomaName = string.Empty;
                returnValue.Education.EntryDegree.CanadianUniversity = string.Empty;
                returnValue.Education.EntryDegree.OtherUniversity = string.Empty;
                returnValue.Education.EntryDegree.OtherUniversityNotListed = string.Empty;
                returnValue.Education.EntryDegree.ProvinceState = string.Empty;
                returnValue.Education.EntryDegree.Country = string.Empty;
                returnValue.Education.EntryDegree.GraduationYear = string.Empty;

                returnValue.Education.Degree2 = new Education();
                returnValue.Education.Degree2.DiplomaName = string.Empty;
                returnValue.Education.Degree2.CanadianUniversity = string.Empty;
                returnValue.Education.Degree2.OtherUniversity = string.Empty;
                returnValue.Education.Degree2.OtherUniversityNotListed = string.Empty;
                returnValue.Education.Degree2.ProvinceState = string.Empty;
                returnValue.Education.Degree2.Country = string.Empty;
                returnValue.Education.Degree2.GraduationYear = string.Empty;

                returnValue.Education.Degree3 = new Education();
                returnValue.Education.Degree3.DiplomaName = string.Empty;
                returnValue.Education.Degree3.CanadianUniversity = string.Empty;
                returnValue.Education.Degree3.OtherUniversity = string.Empty;
                returnValue.Education.Degree3.OtherUniversityNotListed = string.Empty;
                returnValue.Education.Degree3.ProvinceState = string.Empty;
                returnValue.Education.Degree3.Country = string.Empty;
                returnValue.Education.Degree3.GraduationYear = string.Empty;

                returnValue.Education.OtherDegree1 = new Education();
                returnValue.Education.OtherDegree1.DiplomaName = string.Empty;
                returnValue.Education.OtherDegree1.CanadianUniversity = string.Empty;
                returnValue.Education.OtherDegree1.OtherUniversity = string.Empty;
                returnValue.Education.OtherDegree1.OtherUniversityNotListed = string.Empty;
                returnValue.Education.OtherDegree1.StudyField = string.Empty;
                returnValue.Education.OtherDegree1.ProvinceState = string.Empty;
                returnValue.Education.OtherDegree1.Country = string.Empty;
                returnValue.Education.OtherDegree1.GraduationYear = string.Empty;

                returnValue.Education.OtherDegree2 = new Education();
                returnValue.Education.OtherDegree2.DiplomaName = string.Empty;
                returnValue.Education.OtherDegree2.CanadianUniversity = string.Empty;
                returnValue.Education.OtherDegree2.OtherUniversity = string.Empty;
                returnValue.Education.OtherDegree2.OtherUniversityNotListed = string.Empty;
                returnValue.Education.OtherDegree2.StudyField = string.Empty;
                returnValue.Education.OtherDegree2.ProvinceState = string.Empty;
                returnValue.Education.OtherDegree2.Country = string.Empty;
                returnValue.Education.OtherDegree2.GraduationYear = string.Empty;

            }
            return returnValue;
        }

        public void UpdateApplicationUserEducationLogged(string UserID, User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string prevEntryDiploma = string.Empty;
            string prevEntryCanUniv = string.Empty;
            string prevEntryOthUniv = string.Empty;
            string prevEntryOthUnivNotListed = string.Empty;
            string prevEntryProvince = string.Empty;
            string prevEntryCountry = string.Empty;
            string prevEntryYear = string.Empty;

            string prevDegree2Diploma = string.Empty;
            string prevDegree2CanUniv = string.Empty;
            string prevDegree2OthUniv = string.Empty;
            string prevDegree2OthUnivNotListed = string.Empty;
            string prevDegree2Province = string.Empty;
            string prevDegree2Country = string.Empty;
            string prevDegree2Year = string.Empty;

            string prevDegree3Diploma = string.Empty;
            string prevDegree3CanUniv = string.Empty;
            string prevDegree3OthUniv = string.Empty;
            string prevDegree3OthUnivNotListed = string.Empty;
            string prevDegree3Province = string.Empty;
            string prevDegree3Country = string.Empty;
            string prevDegree3Year = string.Empty;

            string prevOtherDegree1Diploma = string.Empty;
            string prevOtherDegree1CanUniv = string.Empty;
            string prevOtherDegree1OthUniv = string.Empty;
            string prevOtherDegree1OthUnivNotListed = string.Empty;
            string prevOtherDegree1Province = string.Empty;
            string prevOtherDegree1Country = string.Empty;
            string prevOtherDegree1Year = string.Empty;
            string prevOtherDegree1FieldStudy = string.Empty;

            string prevOtherDegree2Diploma = string.Empty;
            string prevOtherDegree2CanUniv = string.Empty;
            string prevOtherDegree2OthUniv = string.Empty;
            string prevOtherDegree2OthUnivNotListed = string.Empty;
            string prevOtherDegree2Province = string.Empty;
            string prevOtherDegree2Country = string.Empty;
            string prevOtherDegree2Year = string.Empty;
            string prevOtherDegree2FieldStudy = string.Empty;

            string sql_GetEducation = " SELECT * FROM EDUCATION_ALL WHERE  ID = " + Escape(user.Id);
            //db.ExecuteCommand(sql_GetHomeAddress);

            var table = db.GetData(sql_GetEducation);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                prevEntryDiploma = (string)row["DEGREE_ENTRY_OT"];
                prevEntryCanUniv = (string)row["UNIVERSITY_ENTRY_OT"];
                prevEntryOthUniv = (string)row["UNIVERSITY_ENTRY_OTHER_OT"];
                prevEntryOthUnivNotListed = (string)row["UNIV_ENTRY_OTHER_NL"];
                prevEntryProvince = (string)row["PROVINCE_STATE_ENTRY_OT"];
                prevEntryCountry = (string)row["COUNTRY_ENTRY_OT"];
                prevEntryYear = (string)row["GRAD_YEAR_ENTRY_OT"];

                prevDegree2Diploma = (string)row["DEGREE_2_OT"];
                prevDegree2CanUniv = (string)row["UNIVERSITY_2_OT"];
                prevDegree2OthUniv = (string)row["UNIVERSITY_2_OTHER_OT"];
                prevDegree2OthUnivNotListed = (string)row["UNIV_2_OTHER_OT_NL"];
                prevDegree2Province = (string)row["PROVINCE_STATE_2_OT"];
                prevDegree2Country = (string)row["COUNTRY_2_OT"];
                prevDegree2Year = (string)row["GRAD_YEAR_2_OT"];

                prevDegree3Diploma = (string)row["DEGREE_3_OT"];
                prevDegree3CanUniv = (string)row["UNIVERSITY_3_OT"];
                prevDegree3OthUniv = (string)row["UNIVERSITY_3_OTHER_OT"];
                prevDegree3OthUnivNotListed = (string)row["UNIV_3_OTHER_OT_NL"];
                prevDegree3Province = (string)row["PROVINCE_STATE_3_OT"];
                prevDegree3Country = (string)row["COUNTRY_3_OT"];
                prevDegree3Year = (string)row["GRAD_YEAR_3_OT"];

                prevOtherDegree1Diploma = (string)row["DEGREE_1_OTHER"];
                prevOtherDegree1CanUniv = (string)row["UNIVERSITY_1_OTHER"];
                prevOtherDegree1OthUniv = (string)row["UNIVERSITY_1_INTL"];
                prevOtherDegree1OthUnivNotListed = (string)row["UNIV_1_OTHER_INTL_NL"];
                prevOtherDegree1FieldStudy = (string)row["FIELD_STUDY_1_OTHER"];
                prevOtherDegree1Province = (string)row["STATE_PROV_1_OTHER"];
                prevOtherDegree1Country = (string)row["COUNTRY_1_OTHER"];
                prevOtherDegree1Year = (string)row["GRAD_YEAR_1_OTHER"];

                prevOtherDegree2Diploma = (string)row["DEGREE_2_OTHER"];
                prevOtherDegree2CanUniv = (string)row["UNIVERSITY_2_OTHER"];
                prevOtherDegree2OthUniv = (string)row["UNIVERSITY_2_INTL"];
                prevOtherDegree2OthUnivNotListed = (string)row["UNIV_2_OTHER_INTL_NL"];
                prevOtherDegree2FieldStudy = (string)row["FIELD_STUDY_2_OTHER"];
                prevOtherDegree2Province = (string)row["STATE_PROV_2_OTHER"];
                prevOtherDegree2Country = (string)row["COUNTRY_2_OTHER"];
                prevOtherDegree2Year = (string)row["GRAD_YEAR_2_OTHER"];

            }

            var education = user.Education;

            if (education != null)
            {
                int field_updates = 0;

                var entryDegree = user.Education.EntryDegree;
                var degree2 = user.Education.Degree2;
                var degree3 = user.Education.Degree3;
                var otherDegree1 = user.Education.OtherDegree1;
                var otherDegree2 = user.Education.OtherDegree2;

                string sql_UpdateEducation = "UPDATE EDUCATION_ALL SET ";

                if (!string.IsNullOrEmpty(entryDegree.OtherUniversityNotListed))
                {
                    sql_UpdateEducation += " UNIV_ENTRY_OTHER_NL = " + Escape(entryDegree.OtherUniversityNotListed);
                    field_updates++;
                }

                if (!string.IsNullOrEmpty(degree2.OtherUniversityNotListed))
                {
                    sql_UpdateEducation += " UNIV_2_OTHER_OT_NL = " + Escape(degree2.OtherUniversityNotListed);
                    field_updates++;
                }

                if (!string.IsNullOrEmpty(degree3.OtherUniversityNotListed))
                {
                    sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIV_3_OTHER_OT_NL = " + Escape(degree3.OtherUniversityNotListed);
                    field_updates++;
                }

                if (entryDegree != null)
                {
                    if (!string.IsNullOrEmpty(entryDegree.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_ENTRY_OT = " + Escape(entryDegree.DiplomaName);
                        field_updates++;
                    }
                    if (!string.IsNullOrEmpty(entryDegree.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_ENTRY_OT = " + Escape(entryDegree.CanadianUniversity);
                        field_updates++;
                    }
                    if (!string.IsNullOrEmpty(entryDegree.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_ENTRY_OTHER_OT = " + Escape(entryDegree.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(entryDegree.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " PROVINCE_STATE_ENTRY_OT = " + Escape(entryDegree.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(entryDegree.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_ENTRY_OT = " + Escape(entryDegree.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(entryDegree.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_ENTRY_OT = " + Escape(entryDegree.GraduationYear);
                        field_updates++;
                    }
                }

                if (degree2 != null)
                {
                    if (!string.IsNullOrEmpty(degree2.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_2_OT = " + Escape(degree2.DiplomaName);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_2_OT = " + Escape(degree2.CanadianUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_2_OTHER_OT = " + Escape(degree2.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " PROVINCE_STATE_2_OT = " + Escape(degree2.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_2_OT = " + Escape(degree2.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree2.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_2_OT = " + Escape(degree2.GraduationYear);
                        field_updates++;
                    }
                }

                if (degree3 != null)
                {
                    if (!string.IsNullOrEmpty(degree3.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_3_OT = " + Escape(degree3.DiplomaName);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_3_OT = " + Escape(degree3.CanadianUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_3_OTHER_OT = " + Escape(degree3.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " PROVINCE_STATE_3_OT = " + Escape(degree3.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_3_OT = " + Escape(degree3.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(degree3.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_3_OT = " + Escape(degree3.GraduationYear);
                        field_updates++;
                    }
                }

                if (!string.IsNullOrEmpty(otherDegree1.OtherUniversityNotListed))
                {
                    sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIV_1_OTHER_INTL_NL = " + Escape(otherDegree1.OtherUniversityNotListed);
                    field_updates++;
                }

                if (!string.IsNullOrEmpty(otherDegree2.OtherUniversityNotListed))
                {
                    sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIV_2_OTHER_INTL_NL = " + Escape(otherDegree2.OtherUniversityNotListed);
                    field_updates++;
                }


                if (otherDegree1 != null)
                {
                    if (!string.IsNullOrEmpty(otherDegree1.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_1_OTHER = " + Escape(otherDegree1.DiplomaName);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_1_OTHER = " + Escape(otherDegree1.CanadianUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_1_INTL = " + Escape(otherDegree1.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.StudyField))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " FIELD_STUDY_1_OTHER = " + Escape(otherDegree1.StudyField);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " STATE_PROV_1_OTHER = " + Escape(otherDegree1.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_1_OTHER = " + Escape(otherDegree1.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree1.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_1_OTHER = " + Escape(otherDegree1.GraduationYear);
                        field_updates++;
                    }
                }

                if (otherDegree2 != null)
                {
                    if (!string.IsNullOrEmpty(otherDegree2.DiplomaName))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " DEGREE_2_OTHER = " + Escape(otherDegree2.DiplomaName);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.CanadianUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_2_OTHER = " + Escape(otherDegree2.CanadianUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.OtherUniversity))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " UNIVERSITY_2_INTL = " + Escape(otherDegree2.OtherUniversity);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.StudyField))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " FIELD_STUDY_2_OTHER = " + Escape(otherDegree2.StudyField);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.ProvinceState))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " STATE_PROV_2_OTHER = " + Escape(otherDegree2.ProvinceState);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.Country))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " COUNTRY_2_OTHER = " + Escape(otherDegree2.Country);
                        field_updates++;
                    }

                    if (!string.IsNullOrEmpty(otherDegree2.GraduationYear))
                    {
                        sql_UpdateEducation += (field_updates > 0 ? ", " : string.Empty) + " GRAD_YEAR_2_OTHER = " + Escape(otherDegree2.GraduationYear);
                        field_updates++;
                    }
                }

                sql_UpdateEducation += " WHERE ID = " + Escape(user.Id);
                if (field_updates > 0)
                {
                    db.ExecuteCommand(sql_UpdateEducation);
                }
                // add changes to name_log table

                if (!string.IsNullOrEmpty(entryDegree.DiplomaName) && prevEntryDiploma != entryDegree.DiplomaName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.DEGREE_ENTRY_OT: " + GetGeneralNameImprv("DEGREE_OT_ENTRY", prevEntryDiploma) + " -> " + GetGeneralNameImprv("DEGREE_OT_ENTRY", entryDegree.DiplomaName));
                }

                if (!string.IsNullOrEmpty(entryDegree.CanadianUniversity) && prevEntryCanUniv != entryDegree.CanadianUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_ENTRY_OT: " + GetGeneralNameImprv("UNIV_OT", prevEntryCanUniv) + " -> " + GetGeneralNameImprv("UNIV_OT", entryDegree.CanadianUniversity));
                }

                if (!string.IsNullOrEmpty(entryDegree.OtherUniversity) && prevEntryOthUniv != entryDegree.OtherUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_ENTRY_OTHER_OT: " + GetGeneralNameImprv("UNIV_INTL", prevEntryOthUniv) + " -> " + GetGeneralNameImprv("UNIV_INTL", entryDegree.OtherUniversity));
                }

                if (!string.IsNullOrEmpty(entryDegree.ProvinceState) && prevEntryProvince != entryDegree.ProvinceState)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.PROVINCE_STATE_ENTRY_OT: " + prevEntryProvince + " -> " + entryDegree.ProvinceState);
                }

                if (!string.IsNullOrEmpty(entryDegree.Country) && prevEntryCountry != entryDegree.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.COUNTRY_ENTRY_OT: " + prevEntryCountry + " -> " + entryDegree.Country);
                }

                if (!string.IsNullOrEmpty(entryDegree.GraduationYear) && prevEntryYear != entryDegree.GraduationYear)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.GRAD_YEAR_ENTRY_OT: " + prevEntryYear + " -> " + entryDegree.GraduationYear);
                }

                if (!string.IsNullOrEmpty(degree2.DiplomaName) && prevDegree2Diploma != degree2.DiplomaName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.DEGREE_2_OT: " + GetGeneralNameImprv("DEGREE_OT_POST_ENTRY", prevDegree2Diploma) + " -> " + GetGeneralNameImprv("DEGREE_OT_POST_ENTRY", degree2.DiplomaName));
                }

                if (!string.IsNullOrEmpty(degree2.CanadianUniversity) && prevDegree2CanUniv != degree2.CanadianUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_2_OT: " + GetGeneralNameImprv("UNIV_OT", prevDegree2CanUniv) + " -> " + GetGeneralNameImprv("UNIV_OT", degree2.CanadianUniversity));
                }

                if (!string.IsNullOrEmpty(degree2.OtherUniversity) && prevDegree2OthUniv != degree2.OtherUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_2_OTHER_OT: " + GetGeneralNameImprv("UNIV_INTL", prevDegree2OthUniv) + " -> " + GetGeneralNameImprv("UNIV_INTL", degree2.OtherUniversity));
                }

                if (!string.IsNullOrEmpty(degree2.OtherUniversityNotListed) && prevDegree2OthUnivNotListed != degree2.OtherUniversityNotListed)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIV_2_OTHER_OT_NL: " + prevDegree2OthUnivNotListed + " -> " + degree2.OtherUniversityNotListed);
                }

                if (!string.IsNullOrEmpty(degree2.ProvinceState) && prevDegree2Province != degree2.ProvinceState)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.PROVINCE_STATE_2_OT: " + prevDegree2Province + " -> " + degree2.ProvinceState);
                }

                if (!string.IsNullOrEmpty(degree2.Country) && prevDegree2Country != degree2.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.COUNTRY_2_OT: " + prevDegree2Country + " -> " + degree2.Country);
                }

                if (!string.IsNullOrEmpty(degree2.GraduationYear) && prevDegree2Year != degree2.GraduationYear)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.GRAD_YEAR_2_OT: " + prevDegree2Year + " -> " + degree2.GraduationYear);
                }

                if (!string.IsNullOrEmpty(degree3.DiplomaName) && prevDegree3Diploma != degree3.DiplomaName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.DEGREE_3_OT: " + GetGeneralNameImprv("DEGREE_OT_POST_ENTRY", prevDegree3Diploma) + " -> " + GetGeneralNameImprv("DEGREE_OT_POST_ENTRY", degree3.DiplomaName));
                }

                if (!string.IsNullOrEmpty(degree3.CanadianUniversity) && prevDegree3CanUniv != degree3.CanadianUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_3_OT: " + GetGeneralNameImprv("UNIV_OT", prevDegree3CanUniv) + " -> " + GetGeneralNameImprv("UNIV_OT", degree3.CanadianUniversity));
                }

                if (!string.IsNullOrEmpty(degree3.OtherUniversity) && prevDegree3OthUniv != degree3.OtherUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_3_OTHER_OT: " + GetGeneralNameImprv("UNIV_INTL", prevDegree3OthUniv) + " -> " + GetGeneralNameImprv("UNIV_INTL", degree3.OtherUniversity));
                }

                if (!string.IsNullOrEmpty(degree3.OtherUniversityNotListed) && prevDegree3OthUnivNotListed != degree3.OtherUniversityNotListed)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIV_3_OTHER_OT_NL: " + prevDegree3OthUnivNotListed + " -> " + degree3.OtherUniversityNotListed);
                }

                if (!string.IsNullOrEmpty(degree3.ProvinceState) && prevDegree3Province != degree3.ProvinceState)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.PROVINCE_STATE_3_OT: " + prevDegree3Province + " -> " + degree3.ProvinceState);
                }

                if (!string.IsNullOrEmpty(degree3.Country) && prevDegree3Country != degree3.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.COUNTRY_3_OT: " + prevDegree3Country + " -> " + degree3.Country);
                }

                if (!string.IsNullOrEmpty(degree3.GraduationYear) && prevDegree3Year != degree3.GraduationYear)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.GRAD_YEAR_3_OT: " + prevDegree3Year + " -> " + degree3.GraduationYear);
                }

                if (!string.IsNullOrEmpty(otherDegree1.DiplomaName) && prevOtherDegree1Diploma != otherDegree1.DiplomaName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.DEGREE_1_OTHER: " + GetGeneralNameImprv("DEGREE_OTHER", prevOtherDegree1Diploma) + " -> " + GetGeneralNameImprv("DEGREE_OTHER", otherDegree1.DiplomaName));
                }

                if (!string.IsNullOrEmpty(otherDegree1.CanadianUniversity) && prevOtherDegree1CanUniv != otherDegree1.CanadianUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_1_OTHER: " + prevOtherDegree1CanUniv + " -> " + otherDegree1.CanadianUniversity);
                }

                if (!string.IsNullOrEmpty(otherDegree1.OtherUniversity) && prevOtherDegree1OthUniv != otherDegree1.OtherUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_1_INTL: " + GetGeneralNameImprv("UNIV_INTL", prevOtherDegree1OthUniv) + " -> " + GetGeneralNameImprv("UNIV_INTL", otherDegree1.OtherUniversity));
                }

                if (!string.IsNullOrEmpty(otherDegree1.OtherUniversityNotListed) && prevOtherDegree1OthUnivNotListed != otherDegree1.OtherUniversityNotListed)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIV_1_OTHER_INTL_NL: " + prevOtherDegree1OthUnivNotListed + " -> " + otherDegree1.OtherUniversityNotListed);
                }

                if (!string.IsNullOrEmpty(otherDegree1.StudyField) && prevOtherDegree1FieldStudy != otherDegree1.StudyField)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.FIELD_STUDY_1_OTHER: " + prevOtherDegree1FieldStudy + " -> " + otherDegree1.StudyField);
                }

                if (!string.IsNullOrEmpty(otherDegree1.ProvinceState) && prevOtherDegree1Province != otherDegree1.ProvinceState)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.STATE_PROV_1_OTHER: " + prevOtherDegree1Province + " -> " + otherDegree1.ProvinceState);
                }

                if (!string.IsNullOrEmpty(otherDegree1.Country) && prevOtherDegree1Country != otherDegree1.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.COUNTRY_1_OTHER: " + prevOtherDegree1Country + " -> " + otherDegree1.Country);
                }

                if (!string.IsNullOrEmpty(otherDegree1.GraduationYear) && prevOtherDegree1Year != otherDegree1.GraduationYear)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.GRAD_YEAR_1_OTHER: " + prevOtherDegree1Year + " -> " + otherDegree1.GraduationYear);
                }

                if (!string.IsNullOrEmpty(otherDegree2.DiplomaName) && prevOtherDegree2Diploma != otherDegree2.DiplomaName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.DEGREE_2_OTHER: " + GetGeneralNameImprv("DEGREE_OTHER", prevOtherDegree2Diploma) + " -> " + GetGeneralNameImprv("DEGREE_OTHER", otherDegree2.DiplomaName));
                }

                if (!string.IsNullOrEmpty(otherDegree2.CanadianUniversity) && prevOtherDegree2CanUniv != otherDegree2.CanadianUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_2_OTHER: " + prevOtherDegree2CanUniv + " -> " + otherDegree2.CanadianUniversity);
                }

                if (!string.IsNullOrEmpty(otherDegree2.OtherUniversity) && prevOtherDegree2OthUniv != otherDegree2.OtherUniversity)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIVERSITY_2_INTL: " + GetGeneralNameImprv("UNIV_INTL", prevOtherDegree2OthUniv) + " -> " + GetGeneralNameImprv("UNIV_INTL", otherDegree2.OtherUniversity));
                }

                if (!string.IsNullOrEmpty(otherDegree2.OtherUniversityNotListed) && prevOtherDegree2OthUnivNotListed != otherDegree2.OtherUniversityNotListed)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.UNIV_2_OTHER_INTL_NL: " + prevOtherDegree2OthUnivNotListed + " -> " + otherDegree2.OtherUniversityNotListed);
                }

                if (!string.IsNullOrEmpty(otherDegree2.StudyField) && prevOtherDegree2FieldStudy != otherDegree2.StudyField)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.FIELD_STUDY_2_OTHER: " + prevOtherDegree2FieldStudy + " -> " + otherDegree2.StudyField);
                }

                if (!string.IsNullOrEmpty(otherDegree2.ProvinceState) && prevOtherDegree2Province != otherDegree2.ProvinceState)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.STATE_PROV_2_OTHER: " + prevOtherDegree2Province + " -> " + otherDegree2.ProvinceState);
                }

                if (!string.IsNullOrEmpty(otherDegree2.Country) && prevOtherDegree2Country != otherDegree2.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.COUNTRY_2_OTHER: " + prevOtherDegree2Country + " -> " + otherDegree2.Country);
                }

                if (!string.IsNullOrEmpty(otherDegree2.GraduationYear) && prevOtherDegree2Year != otherDegree2.GraduationYear)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EDUCATION_ALL.GRAD_YEAR_2_OTHER: " + prevOtherDegree2Year + " -> " + otherDegree2.GraduationYear);
                }
            }
        }

        // step 5
        public User GetApplicationUserEmployment(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM EMPLOYMENT_ALL " +
                " WHERE ID = '" + User_Id + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.EmploymentOffer = (bool)row["OFFER_OF_EMPLOYMENT"];
                returnValue.CurrentEmploymentStatus = (string)row["CURRENT_EMPLOYMENT_STATUS"];
                returnValue.CurrentWorkPreference = (string)row["FULL_PT_STATUS_PREF"];
                if (!string.IsNullOrEmpty((string)row["MORE_THREE_PRACT_SITES"]))
                {
                    returnValue.MoreThreePractSites = (((string)row["MORE_THREE_PRACT_SITES"]).ToLower() == "0" || ((string)row["MORE_THREE_PRACT_SITES"]).ToLower() == "no") ? false : true;
                }
                else
                {
                    returnValue.MoreThreePractSites = null;
                }

                returnValue.NaturePractice = (string)row["NATURE_OF_PRACTICE"];
                returnValue.CollegeMailings = (string)row["PRIMARY_EMPL_PREFERRED"];

                returnValue.EmploymentList = new List<Employment>();

                var employment1 = new Employment();
                employment1.Index = 1;
                employment1.Status = (string)row["EMP_1_SOURCE"];
                employment1.EmployerName = (string)row["EMP_1_NAME"];
                var address1 = new Address();
                address1.Address1 = (string)row["EMP_1_ADDR_1"];
                address1.Address2 = (string)row["EMP_1_ADDR_2"];
                address1.City = (string)row["EMP_1_CITY"];
                address1.Province = (string)row["EMP_1_PROV"];
                address1.PostalCode = (string)row["EMP_1_POSTAL"];
                address1.Country = (string)row["EMP_1_COUNTRY"];
                employment1.Address = address1;
                employment1.Phone = (string)row["EMP_1_PHONE"];
                employment1.Fax = (string)row["EMP_1_FAX"];

                employment1.PostalCodeReflectPractice = (string)row["EMP_1_PC_REF_SP"];



                if (row["EMP_1_START_DATE"] != DBNull.Value)
                {
                    employment1.StartDate = (DateTime?)row["EMP_1_START_DATE"];
                }
                else
                {
                    employment1.StartDate = null;
                }

                if (row["CURRENT_EMPLOYMENT_END_DATE"] != DBNull.Value)
                {
                    employment1.EndDate = (DateTime?)row["CURRENT_EMPLOYMENT_END_DATE"];
                }
                else
                {
                    employment1.EndDate = null;
                }
                //employment1.EndDate = (DateTime?)row["CURRENT_EMPLOYMENT_END_DATE"];

                employment1.EmploymentRelationship = (string)row["EMP_1_CATEGORY"];
                employment1.CasualStatus = (string)row["EMP_1_FULL_PT_STATUS"];
                employment1.AverageWeeklyHours = (double)row["EMP_1_HOURS_WORKED_WEEK"];
                employment1.PrimaryRole = (string)row["EMP_1_POSITION"];
                employment1.PracticeSetting = (string)row["EMP_1_TYPE"];
                employment1.MajorServices = (string)row["EMP_1_AREA_PRACTICE"];
                employment1.ClientAgeRange = (string)row["EMP_1_CLNT_ARNGE"];
                employment1.FundingSource = (string)row["EMP_1_FUNDING_SOURCE"];
                employment1.HealthCondition = (string)row["EMP_1_HEALTH_CONDITION"];

                returnValue.EmploymentList.Add(employment1);

                var employment2 = new Employment();
                employment2.Index = 2;
                employment2.Status = (string)row["EMP_2_SOURCE"];
                employment2.EmployerName = (string)row["EMP_2_NAME"];
                var address2 = new Address();
                address2.Address1 = (string)row["EMP_2_ADDR_1"];
                address2.Address2 = (string)row["EMP_2_ADDR_2"];
                address2.City = (string)row["EMP_2_CITY"];
                address2.Province = (string)row["EMP_2_PROV"];
                address2.PostalCode = (string)row["EMP_2_POSTAL"];
                address2.Country = (string)row["EMP_2_COUNTRY"];
                employment2.Address = address2;
                employment2.Phone = (string)row["EMP_2_PHONE"];
                employment2.Fax = (string)row["EMP_2_FAX"];

                employment2.PostalCodeReflectPractice = (string)row["EMP_2_PC_REF_SP"];

                //employment2.StartDate = (DateTime?)row["EMP_2_START_DATE"];
                //employment2.EndDate = (DateTime?)row["EMP_2_END_DATE"];

                if (row["EMP_2_START_DATE"] != DBNull.Value)
                {
                    employment2.StartDate = (DateTime?)row["EMP_2_START_DATE"];
                }
                else
                {
                    employment2.StartDate = null;
                }

                if (row["EMP_2_END_DATE"] != DBNull.Value)
                {
                    employment2.EndDate = (DateTime?)row["EMP_2_END_DATE"];
                }
                else
                {
                    employment2.EndDate = null;
                }

                employment2.EmploymentRelationship = (string)row["EMP_2_CATEGORY"];
                employment2.CasualStatus = (string)row["EMP_2_FULL_PT_STATUS"];
                employment2.AverageWeeklyHours = (double)row["EMP_2_HOURS_WORKED_WEEK"];
                employment2.PrimaryRole = (string)row["EMP_2_POSITION"];
                employment2.PracticeSetting = (string)row["EMP_2_TYPE"];
                employment2.MajorServices = (string)row["EMP_2_AREA_PRACTICE"];
                employment2.ClientAgeRange = (string)row["EMP_2_CLNT_ARNGE"];
                employment2.FundingSource = (string)row["EMP_2_FUNDING_SOURCE"];
                employment2.HealthCondition = (string)row["EMP_2_HEALTH_CONDITION"];

                returnValue.EmploymentList.Add(employment2);

                var employment3 = new Employment();
                employment3.Index = 3;
                employment3.Status = (string)row["EMP_3_SOURCE"];
                employment3.EmployerName = (string)row["EMP_3_NAME"];
                var address3 = new Address();
                address3.Address1 = (string)row["EMP_3_ADDR_1"];
                address3.Address2 = (string)row["EMP_3_ADDR_2"];
                address3.City = (string)row["EMP_3_CITY"];
                address3.Province = (string)row["EMP_3_PROV"];
                address3.PostalCode = (string)row["EMP_3_POSTAL"];
                address3.Country = (string)row["EMP_3_COUNTRY"];
                employment3.Address = address3;
                employment3.Phone = (string)row["EMP_3_PHONE"];
                employment3.Fax = (string)row["EMP_3_FAX"];

                employment3.PostalCodeReflectPractice = (string)row["EMP_3_PC_REF_SP"];

                //employment3.StartDate = (DateTime?)row["EMP_3_START_DATE"];
                //employment3.EndDate = (DateTime?)row["EMP_3_END_DATE"];

                if (row["EMP_3_START_DATE"] != DBNull.Value)
                {
                    employment3.StartDate = (DateTime?)row["EMP_3_START_DATE"];
                }
                else
                {
                    employment3.StartDate = null;
                }

                if (row["EMP_3_END_DATE"] != DBNull.Value)
                {
                    employment3.EndDate = (DateTime?)row["EMP_3_END_DATE"];
                }
                else
                {
                    employment3.EndDate = null;
                }

                employment3.EmploymentRelationship = (string)row["EMP_3_CATEGORY"];
                employment3.CasualStatus = (string)row["EMP_3_FULL_PT_STATUS"];
                employment3.AverageWeeklyHours = (double)row["EMP_3_HOURS_WORKED_WEEK"];
                employment3.PrimaryRole = (string)row["EMP_3_POSITION"];
                employment3.PracticeSetting = (string)row["EMP_3_TYPE"];
                employment3.MajorServices = (string)row["EMP_3_AREA_PRACTICE"];
                employment3.ClientAgeRange = (string)row["EMP_3_CLNT_ARNGE"];
                employment3.FundingSource = (string)row["EMP_3_FUNDING_SOURCE"];
                employment3.HealthCondition = (string)row["EMP_3_HEALTH_CONDITION"];

                returnValue.EmploymentList.Add(employment3);
            }
            return returnValue;
        }

        public void UpdateApplicationUserEmploymentLogged(string UserID, User user, out string query)
        {
            query = string.Empty;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            var employment1 = user.EmploymentList.Find(E => E.Index == 1);
            var employment2 = user.EmploymentList.Find(E => E.Index == 2);
            var employment3 = user.EmploymentList.Find(E => E.Index == 3);
            if (employment1 != null && employment2 != null && employment3 != null)
            {

                string prevMoreThreePract = string.Empty;
                string prevCollegeMailing = string.Empty;
                string prevOffer = string.Empty;
                string prevEmpStatus = string.Empty;

                string prevEmp1Status = string.Empty;
                string prevEmp1EmployerName = string.Empty;
                string prevEmp1Address1 = string.Empty;
                string prevEmp1Address2 = string.Empty;
                string prevEmp1City = string.Empty;
                string prevEmp1Prov = string.Empty;
                string prevEmp1Postal = string.Empty;
                string prevEmp1Country = string.Empty;
                string prevEmp1Phone = string.Empty;
                string prevEmp1Fax = string.Empty;
                string prevEmp1PostalCodeReflectPractice = string.Empty;
                string prevEmp1StartDate = string.Empty;
                string prevEmp1EndDate = string.Empty;
                string prevEmp1Category = string.Empty;
                string prevEmp1CasualStatus = string.Empty;
                string prevEmp1AvgWeeklyHours = string.Empty;
                string prevEmp1PrimaryRole = string.Empty;
                string prevEmp1PracticeSetting = string.Empty;
                string prevEmp1MajorService = string.Empty;
                string prevEmp1ClientAgeRange = string.Empty;
                string prevEmp1FundingSource = string.Empty;
                string prevEmp1HealthCondition = string.Empty;

                string prevEmp2Status = string.Empty;
                string prevEmp2EmployerName = string.Empty;
                string prevEmp2Address1 = string.Empty;
                string prevEmp2Address2 = string.Empty;
                string prevEmp2City = string.Empty;
                string prevEmp2Prov = string.Empty;
                string prevEmp2Postal = string.Empty;
                string prevEmp2Country = string.Empty;
                string prevEmp2Phone = string.Empty;
                string prevEmp2Fax = string.Empty;
                string prevEmp2PostalCodeReflectPractice = string.Empty;
                string prevEmp2StartDate = string.Empty;
                string prevEmp2EndDate = string.Empty;
                string prevEmp2Category = string.Empty;
                string prevEmp2CasualStatus = string.Empty;
                string prevEmp2AvgWeeklyHours = string.Empty;
                string prevEmp2PrimaryRole = string.Empty;
                string prevEmp2PracticeSetting = string.Empty;
                string prevEmp2MajorService = string.Empty;
                string prevEmp2ClientAgeRange = string.Empty;
                string prevEmp2FundingSource = string.Empty;
                string prevEmp2HealthCondition = string.Empty;

                string prevEmp3Status = string.Empty;
                string prevEmp3EmployerName = string.Empty;
                string prevEmp3Address1 = string.Empty;
                string prevEmp3Address2 = string.Empty;
                string prevEmp3City = string.Empty;
                string prevEmp3Prov = string.Empty;
                string prevEmp3Postal = string.Empty;
                string prevEmp3Country = string.Empty;
                string prevEmp3Phone = string.Empty;
                string prevEmp3Fax = string.Empty;
                string prevEmp3PostalCodeReflectPractice = string.Empty;
                string prevEmp3StartDate = string.Empty;
                string prevEmp3EndDate = string.Empty;
                string prevEmp3Category = string.Empty;
                string prevEmp3CasualStatus = string.Empty;
                string prevEmp3AvgWeeklyHours = string.Empty;
                string prevEmp3PrimaryRole = string.Empty;
                string prevEmp3PracticeSetting = string.Empty;
                string prevEmp3MajorService = string.Empty;
                string prevEmp3ClientAgeRange = string.Empty;
                string prevEmp3FundingSource = string.Empty;
                string prevEmp3HealthCondition = string.Empty;

                string sql_GetCurrentEmployment = "SELECT * FROM EMPLOYMENT_ALL WHERE ID=" + Escape(UserID);
                var table = db.GetData(sql_GetCurrentEmployment);
                if (table != null && table.Rows.Count == 1)
                {
                    DataRow row = table.Rows[0];
                    prevMoreThreePract = (string)row["MORE_THREE_PRACT_SITES"];
                    prevCollegeMailing = (string)row["PRIMARY_EMPL_PREFERRED"];
                    prevOffer = (bool)row["OFFER_OF_EMPLOYMENT"] ? "Yes" : "No";
                    prevEmpStatus = (string)row["CURRENT_EMPLOYMENT_STATUS"];

                    #region mep1
                    prevEmp1Status = (string)row["EMP_1_SOURCE"];
                    prevEmp1EmployerName = (string)row["EMP_1_NAME"];
                    prevEmp1Address1 = (string)row["EMP_1_ADDR_1"];
                    prevEmp1Address2 = (string)row["EMP_1_ADDR_2"];
                    prevEmp1City = (string)row["EMP_1_CITY"];
                    prevEmp1Prov = (string)row["EMP_1_PROV"];
                    prevEmp1Postal = (string)row["EMP_1_POSTAL"];
                    prevEmp1Country = (string)row["EMP_1_COUNTRY"];
                    prevEmp1Phone = (string)row["EMP_1_PHONE"];
                    prevEmp1Fax = (string)row["EMP_1_FAX"];
                    prevEmp1PostalCodeReflectPractice = (string)row["EMP_1_PC_REF_SP"];
                    if (row["EMP_1_START_DATE"] != DBNull.Value)
                    {
                        prevEmp1StartDate = ((DateTime)row["EMP_1_START_DATE"]).ToString("dd/MM/yyyy");
                    }
                    if (row["CURRENT_EMPLOYMENT_END_DATE"] != DBNull.Value)
                    {
                        prevEmp1EndDate = ((DateTime)row["CURRENT_EMPLOYMENT_END_DATE"]).ToString("dd/MM/yyyy");
                    }

                    prevEmp1Category = (string)row["EMP_1_CATEGORY"];
                    prevEmp1CasualStatus = (string)row["EMP_1_FULL_PT_STATUS"];
                    prevEmp1AvgWeeklyHours = ((double)row["EMP_1_HOURS_WORKED_WEEK"]).ToString();
                    prevEmp1PrimaryRole = (string)row["EMP_1_POSITION"];
                    prevEmp1PracticeSetting = (string)row["EMP_1_TYPE"];
                    prevEmp1MajorService = (string)row["EMP_1_AREA_PRACTICE"];
                    prevEmp1ClientAgeRange = (string)row["EMP_1_CLNT_ARNGE"];
                    prevEmp1FundingSource = (string)row["EMP_1_FUNDING_SOURCE"];
                    prevEmp1HealthCondition = (string)row["EMP_1_HEALTH_CONDITION"];
                    #endregion
                    #region emp2
                    prevEmp2Status = (string)row["EMP_2_SOURCE"];
                    prevEmp2EmployerName = (string)row["EMP_2_NAME"];
                    prevEmp2Address1 = (string)row["EMP_2_ADDR_1"];
                    prevEmp2Address2 = (string)row["EMP_2_ADDR_2"];
                    prevEmp2City = (string)row["EMP_2_CITY"];
                    prevEmp2Prov = (string)row["EMP_2_PROV"];
                    prevEmp2Postal = (string)row["EMP_2_POSTAL"];
                    prevEmp2Country = (string)row["EMP_2_COUNTRY"];
                    prevEmp2Phone = (string)row["EMP_2_PHONE"];
                    prevEmp2Fax = (string)row["EMP_2_FAX"];
                    prevEmp2PostalCodeReflectPractice = (string)row["EMP_2_PC_REF_SP"];
                    if (row["EMP_2_START_DATE"] != DBNull.Value)
                    {
                        prevEmp2StartDate = ((DateTime)row["EMP_2_START_DATE"]).ToString("dd/MM/yyyy");
                    }
                    if (row["EMP_2_END_DATE"] != DBNull.Value)
                    {
                        prevEmp2EndDate = ((DateTime)row["EMP_2_END_DATE"]).ToString("dd/MM/yyyy");
                    }

                    prevEmp2Category = (string)row["EMP_2_CATEGORY"];
                    prevEmp2CasualStatus = (string)row["EMP_2_FULL_PT_STATUS"];
                    prevEmp2AvgWeeklyHours = ((double)row["EMP_2_HOURS_WORKED_WEEK"]).ToString();
                    prevEmp2PrimaryRole = (string)row["EMP_2_POSITION"];
                    prevEmp2PracticeSetting = (string)row["EMP_2_TYPE"];
                    prevEmp2MajorService = (string)row["EMP_2_AREA_PRACTICE"];
                    prevEmp2ClientAgeRange = (string)row["EMP_2_CLNT_ARNGE"];
                    prevEmp2FundingSource = (string)row["EMP_2_FUNDING_SOURCE"];
                    prevEmp2HealthCondition = (string)row["EMP_2_HEALTH_CONDITION"];
                    #endregion
                    #region emp3
                    prevEmp3Status = (string)row["EMP_3_SOURCE"];
                    prevEmp3EmployerName = (string)row["EMP_3_NAME"];
                    prevEmp3Address1 = (string)row["EMP_3_ADDR_1"];
                    prevEmp3Address2 = (string)row["EMP_3_ADDR_2"];
                    prevEmp3City = (string)row["EMP_3_CITY"];
                    prevEmp3Prov = (string)row["EMP_3_PROV"];
                    prevEmp3Postal = (string)row["EMP_3_POSTAL"];
                    prevEmp3Country = (string)row["EMP_3_COUNTRY"];
                    prevEmp3Phone = (string)row["EMP_3_PHONE"];
                    prevEmp3Fax = (string)row["EMP_3_FAX"];
                    prevEmp3PostalCodeReflectPractice = (string)row["EMP_3_PC_REF_SP"];
                    if (row["EMP_3_START_DATE"] != DBNull.Value)
                    {
                        prevEmp3StartDate = ((DateTime)row["EMP_3_START_DATE"]).ToString("dd/MM/yyyy");
                    }
                    if (row["EMP_3_END_DATE"] != DBNull.Value)
                    {
                        prevEmp3EndDate = ((DateTime)row["EMP_3_END_DATE"]).ToString("dd/MM/yyyy");
                    }

                    prevEmp3Category = (string)row["EMP_3_CATEGORY"];
                    prevEmp3CasualStatus = (string)row["EMP_3_FULL_PT_STATUS"];
                    prevEmp3AvgWeeklyHours = ((double)row["EMP_3_HOURS_WORKED_WEEK"]).ToString();
                    prevEmp3PrimaryRole = (string)row["EMP_3_POSITION"];
                    prevEmp3PracticeSetting = (string)row["EMP_3_TYPE"];
                    prevEmp3MajorService = (string)row["EMP_3_AREA_PRACTICE"];
                    prevEmp3ClientAgeRange = (string)row["EMP_3_CLNT_ARNGE"];
                    prevEmp3FundingSource = (string)row["EMP_3_FUNDING_SOURCE"];
                    prevEmp3HealthCondition = (string)row["EMP_3_HEALTH_CONDITION"];
                    #endregion
                }

                string sql_UpdateEmp = "UPDATE EMPLOYMENT_ALL SET ";
                // update morethreepractsites and current employment status
                //sql_UpdateEmp += "CURRENT_EMPLOYMENT_STATUS = " + Escape(user.CurrentEmploymentStatus) + ", " +
                //" FULL_PT_STATUS_PREF = " + Escape(user.CurrentWorkPreference.ToString()) + ", " +
                //" NATURE_OF_PRACTICE = " + Escape(user.NaturePractice.ToString()) + ", " +

                string morethree = string.Empty;
                if (user.MoreThreePractSites != null)
                {
                    bool bln_MoreThree = (bool)user.MoreThreePractSites;
                    morethree = bln_MoreThree ? "Yes" : "No";
                }

                sql_UpdateEmp += " MORE_THREE_PRACT_SITES = " + Escape(morethree) + ", " +
                  " PRIMARY_EMPL_PREFERRED = " + Escape(user.CollegeMailings) + ", ";
                sql_UpdateEmp += " OFFER_OF_EMPLOYMENT = " + (user.EmploymentOffer ? "1": "0") + ", ";
                sql_UpdateEmp += " CURRENT_EMPLOYMENT_STATUS = " + (user.EmploymentOffer ? Escape("10") : "''") + ", ";
                
                // first employment
                sql_UpdateEmp += " EMP_1_SOURCE = " + Escape(employment1.Status) + ", " +
                " EMP_1_NAME = " + Escape(employment1.EmployerName) + ", ";
                var address1 = employment1.Address;
                if (address1 != null)
                {
                    sql_UpdateEmp += "EMP_1_ADDR_1 = " + Escape(address1.Address1) + ", " +
                    "EMP_1_ADDR_2 = " + Escape(address1.Address2) + ", " +
                    "EMP_1_CITY = " + Escape(address1.City) + ", " +
                    "EMP_1_PROV = " + Escape(address1.Province) + ", " +
                    "EMP_1_POSTAL = " + Escape(address1.PostalCode) + ", " +
                    "EMP_1_COUNTRY = " + Escape(address1.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_1_PHONE = " + Escape(employment1.Phone) + ", " +
                "EMP_1_FAX = " + Escape(employment1.Fax) + ", " +
                "EMP_1_PC_REF_SP = " + Escape(employment1.PostalCodeReflectPractice) + ", ";

                if (employment1.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment1.StartDate;
                    sql_UpdateEmp += "EMP_1_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_1_START_DATE = NULL, ";
                }

                if (employment1.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment1.EndDate;
                    sql_UpdateEmp += "CURRENT_EMPLOYMENT_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "CURRENT_EMPLOYMENT_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_1_CATEGORY = " + Escape(employment1.EmploymentRelationship) + ", " +
                "EMP_1_FULL_PT_STATUS = " + Escape(employment1.CasualStatus) + ", " +
                "EMP_1_HOURS_WORKED_WEEK = " + employment1.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_1_POSITION = " + Escape(employment1.PrimaryRole) + ", " +
                "EMP_1_TYPE = " + Escape(employment1.PracticeSetting) + ", " +
                "EMP_1_AREA_PRACTICE = " + Escape(employment1.MajorServices) + ", " +
                "EMP_1_CLNT_ARNGE = " + Escape(employment1.ClientAgeRange) + ", " +
                "EMP_1_FUNDING_SOURCE = " + Escape(employment1.FundingSource) + ", " +
                "EMP_1_HEALTH_CONDITION = " + Escape(employment1.HealthCondition) + ", ";

                // second employment
                sql_UpdateEmp +=
                "EMP_2_SOURCE = " + Escape(employment2.Status) + ", " +
                "EMP_2_NAME = " + Escape(employment2.EmployerName) + ", ";
                var address2 = employment2.Address;
                if (address2 != null)
                {
                    sql_UpdateEmp += "EMP_2_ADDR_1 = " + Escape(address2.Address1) + ", " +
                    "EMP_2_ADDR_2 = " + Escape(address2.Address2) + ", " +
                    "EMP_2_CITY = " + Escape(address2.City) + ", " +
                    "EMP_2_PROV = " + Escape(address2.Province) + ", " +
                    "EMP_2_POSTAL = " + Escape(address2.PostalCode) + ", " +
                    "EMP_2_COUNTRY = " + Escape(address2.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_2_PHONE = " + Escape(employment2.Phone) + ", " +
                "EMP_2_FAX = " + Escape(employment2.Fax) + ", " +
                "EMP_2_PC_REF_SP = " + Escape(employment2.PostalCodeReflectPractice) + ", ";

                if (employment2.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment2.StartDate;
                    sql_UpdateEmp += "EMP_2_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_2_START_DATE = NULL, ";
                }

                if (employment2.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment2.EndDate;
                    sql_UpdateEmp += "EMP_2_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_2_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_2_CATEGORY = " + Escape(employment2.EmploymentRelationship) + ", " +
                "EMP_2_FULL_PT_STATUS = " + Escape(employment2.CasualStatus) + ", " +
                "EMP_2_HOURS_WORKED_WEEK = " + employment2.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_2_POSITION = " + Escape(employment2.PrimaryRole) + ", " +
                "EMP_2_TYPE = " + Escape(employment2.PracticeSetting) + ", " +
                "EMP_2_AREA_PRACTICE = " + Escape(employment2.MajorServices) + ", " +
                "EMP_2_CLNT_ARNGE = " + Escape(employment2.ClientAgeRange) + ", " +
                "EMP_2_FUNDING_SOURCE = " + Escape(employment2.FundingSource) + ", " +
                "EMP_2_HEALTH_CONDITION = " + Escape(employment2.HealthCondition) + ", ";

                // third employment
                sql_UpdateEmp +=
                "EMP_3_SOURCE = " + Escape(employment3.Status) + ", " +
                "EMP_3_NAME = " + Escape(employment3.EmployerName) + ", ";
                var address3 = employment3.Address;
                if (address2 != null)
                {
                    sql_UpdateEmp += "EMP_3_ADDR_1 = " + Escape(address3.Address1) + ", " +
                    "EMP_3_ADDR_2 = " + Escape(address3.Address2) + ", " +
                    "EMP_3_CITY = " + Escape(address3.City) + ", " +
                    "EMP_3_PROV = " + Escape(address3.Province) + ", " +
                    "EMP_3_POSTAL = " + Escape(address3.PostalCode) + ", " +
                    "EMP_3_COUNTRY = " + Escape(address3.Country) + ", ";
                }
                sql_UpdateEmp += "EMP_3_PHONE = " + Escape(employment3.Phone) + ", " +
                "EMP_3_FAX = " + Escape(employment3.Fax) + ", " +
                "EMP_3_PC_REF_SP = " + Escape(employment3.PostalCodeReflectPractice) + ", ";

                if (employment3.StartDate != null)
                {
                    DateTime startdate = (DateTime)employment3.StartDate;
                    sql_UpdateEmp += "EMP_3_START_DATE = " + Escape(startdate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_3_START_DATE = NULL, ";
                }

                if (employment3.EndDate != null)
                {
                    DateTime enddate = (DateTime)employment3.EndDate;
                    sql_UpdateEmp += "EMP_3_END_DATE = " + Escape(enddate.ToString("yyyy-MM-dd")) + ", ";
                }
                else
                {
                    sql_UpdateEmp += "EMP_3_END_DATE = NULL, ";
                }

                sql_UpdateEmp += "EMP_3_CATEGORY = " + Escape(employment3.EmploymentRelationship) + ", " +
                "EMP_3_FULL_PT_STATUS = " + Escape(employment3.CasualStatus) + ", " +
                "EMP_3_HOURS_WORKED_WEEK = " + employment3.AverageWeeklyHours.ToString("F") + ", " +
                "EMP_3_POSITION = " + Escape(employment3.PrimaryRole) + ", " +
                "EMP_3_TYPE = " + Escape(employment3.PracticeSetting) + ", " +
                "EMP_3_AREA_PRACTICE = " + Escape(employment3.MajorServices) + ", " +
                "EMP_3_CLNT_ARNGE = " + Escape(employment3.ClientAgeRange) + ", " +
                "EMP_3_FUNDING_SOURCE = " + Escape(employment3.FundingSource) + ", " +
                "EMP_3_HEALTH_CONDITION = " + Escape(employment3.HealthCondition) +

                " WHERE ID = " + Escape(user.Id);
                query = sql_UpdateEmp;
                db.ExecuteCommand(sql_UpdateEmp);

                if (prevMoreThreePract != (morethree))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.MORE_THREE_PRACT_SITES: " + prevMoreThreePract + " -> " + (morethree));
                }

                if (prevCollegeMailing != user.CollegeMailings)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.PRIMARY_EMPL_PREFERRED: " + prevCollegeMailing + " -> " + user.CollegeMailings);
                }

                if (prevOffer != (user.EmploymentOffer? "Yes" : "No"))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.OFFER_OF_EMPLOYMENT: " + prevOffer + " -> " + (user.EmploymentOffer ? "Yes" : "No"));
                }

                if (prevEmpStatus != (user.EmploymentOffer ? "10" : ""))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.CURRENT_EMPLOYMENT_STATUS: " + prevEmpStatus + " -> " + (user.EmploymentOffer ? "10" : ""));
                }

                #region emp1
                if (prevEmp1Status != employment1.Status)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_SOURCE: " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", prevEmp1Status) + " -> " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", employment1.Status));
                }
                if (prevEmp1EmployerName != employment1.EmployerName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_NAME: " + prevEmp1EmployerName + " -> " + employment1.EmployerName);
                }
                if (prevEmp1Address1 != employment1.Address.Address1)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_ADDR_1: " + prevEmp1Address1 + " -> " + employment1.Address.Address1);
                }
                if (prevEmp1Address2 != employment1.Address.Address2)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_ADDR_2: " + prevEmp1Address2 + " -> " + employment1.Address.Address2);
                }
                if (prevEmp1City != employment1.Address.City)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_CITY: " + prevEmp1City + " -> " + employment1.Address.City);
                }
                if (prevEmp1Prov != employment1.Address.Province)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_PROV: " + prevEmp1Prov + " -> " + employment1.Address.Province);
                }
                if (prevEmp1Postal != employment1.Address.PostalCode)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_POSTAL: " + prevEmp1Postal + " -> " + employment1.Address.PostalCode);
                }
                if (prevEmp1Country != employment1.Address.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_COUNTRY: " + prevEmp1Country + " -> " + employment1.Address.Country);
                }
                if (prevEmp1Phone != employment1.Phone)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_PHONE: " + prevEmp1Phone + " -> " + employment1.Phone);
                }
                if (prevEmp1Fax != employment1.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_FAX: " + prevEmp1Fax + " -> " + employment1.Fax);
                }
                if (prevEmp1PostalCodeReflectPractice != employment1.PostalCodeReflectPractice)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_PC_REF_SP: " + prevEmp1PostalCodeReflectPractice + " -> " + employment1.PostalCodeReflectPractice);
                }
                if (prevEmp1Fax != employment1.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_FAX: " + prevEmp1Fax + " -> " + employment1.Fax);
                }
                if (prevEmp1StartDate != (employment1.StartDate != null ? ((DateTime)employment1.StartDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_START_DATE: " + prevEmp1StartDate + " -> " + (employment1.StartDate != null ? ((DateTime)employment1.StartDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp1EndDate != (employment1.EndDate != null ? ((DateTime)employment1.EndDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.CURRENT_EMPLOYMENT_END_DATE: " + prevEmp1EndDate + " -> " + (employment1.EndDate != null ? ((DateTime)employment1.EndDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp1Category != employment1.EmploymentRelationship)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_CATEGORY: " + GetGeneralNameImprv("EMPLOY_CATEGORY", prevEmp1Category) + " -> " + GetGeneralNameImprv("EMPLOY_CATEGORY", employment1.EmploymentRelationship));
                }
                if (prevEmp1CasualStatus != employment1.CasualStatus)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_FULL_PT_STATUS: " + GetGeneralNameImprv("FULL_PT_STATUS", prevEmp1CasualStatus) + " -> " + GetGeneralNameImprv("FULL_PT_STATUS", employment1.CasualStatus));
                }
                if (prevEmp1AvgWeeklyHours != employment1.AverageWeeklyHours.ToString())
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_HOURS_WORKED_WEEK: " + prevEmp1AvgWeeklyHours + " -> " + employment1.AverageWeeklyHours.ToString());
                }
                if (prevEmp1PrimaryRole != employment1.PrimaryRole)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_POSITION: " + GetGeneralNameImprv("POSITION", prevEmp1PrimaryRole) + " -> " + GetGeneralNameImprv("POSITION", employment1.PrimaryRole));
                }
                if (prevEmp1PracticeSetting != employment1.PracticeSetting)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_TYPE: " + GetGeneralNameImprv("EMPLOYER_TYPE", prevEmp1PracticeSetting) + " -> " + GetGeneralNameImprv("EMPLOYER_TYPE", employment1.PracticeSetting));
                }
                if (prevEmp1MajorService != employment1.MajorServices)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_AREA_PRACTICE: " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", prevEmp1MajorService) + " -> " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", employment1.MajorServices));
                }
                if (prevEmp1ClientAgeRange != employment1.ClientAgeRange)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_CLNT_ARNGE: " + GetGeneralNameImprv("CLIENT_AGE_RANGE", prevEmp1ClientAgeRange) + " -> " + GetGeneralNameImprv("CLIENT_AGE_RANGE", employment1.ClientAgeRange));
                }
                if (prevEmp1FundingSource != employment1.FundingSource)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_FUNDING_SOURCE: " + GetGeneralNameImprv("FUNDING", prevEmp1FundingSource) + " -> " + GetGeneralNameImprv("FUNDING", employment1.FundingSource));
                }
                if (prevEmp1HealthCondition != employment1.HealthCondition)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_1_HEALTH_CONDITION: " + GetGeneralNameImprv("HEALTH_CONDITION", prevEmp1HealthCondition) + " -> " + GetGeneralNameImprv("HEALTH_CONDITION", employment1.HealthCondition));
                }
                #endregion
                #region emp2
                if (prevEmp2Status != employment2.Status)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_SOURCE: " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", prevEmp2Status) + " -> " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", employment2.Status));
                }
                if (prevEmp2EmployerName != employment2.EmployerName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_NAME: " + prevEmp2EmployerName + " -> " + employment2.EmployerName);
                }
                if (prevEmp2Address1 != employment2.Address.Address1)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_ADDR_1: " + prevEmp2Address1 + " -> " + employment2.Address.Address1);
                }
                if (prevEmp2Address2 != employment2.Address.Address2)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_ADDR_2: " + prevEmp2Address2 + " -> " + employment2.Address.Address2);
                }
                if (prevEmp2City != employment2.Address.City)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_CITY: " + prevEmp2City + " -> " + employment2.Address.City);
                }
                if (prevEmp2Prov != employment2.Address.Province)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_PROV: " + prevEmp2Prov + " -> " + employment2.Address.Province);
                }
                if (prevEmp2Postal != employment2.Address.PostalCode)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_POSTAL: " + prevEmp2Postal + " -> " + employment2.Address.PostalCode);
                }
                if (prevEmp2Country != employment2.Address.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_COUNTRY: " + prevEmp2Country + " -> " + employment2.Address.Country);
                }
                if (prevEmp2Phone != employment2.Phone)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_PHONE: " + prevEmp2Phone + " -> " + employment2.Phone);
                }
                if (prevEmp2Fax != employment2.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_FAX: " + prevEmp2Fax + " -> " + employment2.Fax);
                }
                if (prevEmp2PostalCodeReflectPractice != employment2.PostalCodeReflectPractice)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_PC_REF_SP: " + prevEmp2PostalCodeReflectPractice + " -> " + employment2.PostalCodeReflectPractice);
                }
                if (prevEmp2Fax != employment2.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_FAX: " + prevEmp2Fax + " -> " + employment2.Fax);
                }
                if (prevEmp2StartDate != (employment2.StartDate != null ? ((DateTime)employment2.StartDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_START_DATE: " + prevEmp2StartDate + " -> " + (employment2.StartDate != null ? ((DateTime)employment2.StartDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp2EndDate != (employment2.EndDate != null ? ((DateTime)employment2.EndDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_END_DATE: " + prevEmp2EndDate + " -> " + (employment2.EndDate != null ? ((DateTime)employment2.EndDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp2Category != employment2.EmploymentRelationship)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_CATEGORY: " + GetGeneralNameImprv("EMPLOY_CATEGORY", prevEmp2Category) + " -> " + GetGeneralNameImprv("EMPLOY_CATEGORY", employment2.EmploymentRelationship));
                }
                if (prevEmp2CasualStatus != employment2.CasualStatus)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_FULL_PT_STATUS: " + GetGeneralNameImprv("FULL_PT_STATUS", prevEmp2CasualStatus) + " -> " + GetGeneralNameImprv("FULL_PT_STATUS", employment2.CasualStatus));
                }
                if (prevEmp2AvgWeeklyHours != employment2.AverageWeeklyHours.ToString())
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_HOURS_WORKED_WEEK: " + prevEmp2AvgWeeklyHours + " -> " + employment2.AverageWeeklyHours.ToString());
                }
                if (prevEmp2PrimaryRole != employment2.PrimaryRole)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_POSITION: " + GetGeneralNameImprv("POSITION", prevEmp2PrimaryRole) + " -> " + GetGeneralNameImprv("POSITION", employment2.PrimaryRole));
                }
                if (prevEmp2PracticeSetting != employment2.PracticeSetting)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_TYPE: " + GetGeneralNameImprv("EMPLOYER_TYPE", prevEmp2PracticeSetting) + " -> " + GetGeneralNameImprv("EMPLOYER_TYPE", employment2.PracticeSetting));
                }
                if (prevEmp2MajorService != employment2.MajorServices)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_AREA_PRACTICE: " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", prevEmp2MajorService) + " -> " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", employment2.MajorServices));
                }
                if (prevEmp2ClientAgeRange != employment2.ClientAgeRange)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_CLNT_ARNGE: " + GetGeneralNameImprv("CLIENT_AGE_RANGE", prevEmp2ClientAgeRange) + " -> " + GetGeneralNameImprv("CLIENT_AGE_RANGE", employment2.ClientAgeRange));
                }
                if (prevEmp2FundingSource != employment2.FundingSource)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_FUNDING_SOURCE: " + GetGeneralNameImprv("FUNDING", prevEmp2FundingSource) + " -> " + GetGeneralNameImprv("FUNDING", employment2.FundingSource));
                }
                if (prevEmp2HealthCondition != employment2.HealthCondition)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_2_HEALTH_CONDITION: " + GetGeneralNameImprv("HEALTH_CONDITION", prevEmp2HealthCondition) + " -> " + GetGeneralNameImprv("HEALTH_CONDITION", employment2.HealthCondition));
                }
                #endregion
                #region emp3
                if (prevEmp3Status != employment3.Status)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_SOURCE: " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", prevEmp3Status) + " -> " + GetGeneralNameImprv("EMPLOYMENT_SOURCE", employment3.Status));
                }
                if (prevEmp3EmployerName != employment3.EmployerName)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_NAME: " + prevEmp3EmployerName + " -> " + employment3.EmployerName);
                }
                if (prevEmp3Address1 != employment3.Address.Address1)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_ADDR_1: " + prevEmp3Address1 + " -> " + employment3.Address.Address1);
                }
                if (prevEmp3Address2 != employment3.Address.Address2)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_ADDR_2: " + prevEmp3Address2 + " -> " + employment3.Address.Address2);
                }
                if (prevEmp3City != employment3.Address.City)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_CITY: " + prevEmp3City + " -> " + employment3.Address.City);
                }
                if (prevEmp3Prov != employment3.Address.Province)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_PROV: " + prevEmp3Prov + " -> " + employment3.Address.Province);
                }
                if (prevEmp3Postal != employment3.Address.PostalCode)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_POSTAL: " + prevEmp3Postal + " -> " + employment3.Address.PostalCode);
                }
                if (prevEmp3Country != employment3.Address.Country)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_COUNTRY: " + prevEmp3Country + " -> " + employment3.Address.Country);
                }
                if (prevEmp3Phone != employment3.Phone)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_PHONE: " + prevEmp3Phone + " -> " + employment3.Phone);
                }
                if (prevEmp3Fax != employment3.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_FAX: " + prevEmp3Fax + " -> " + employment3.Fax);
                }
                if (prevEmp3PostalCodeReflectPractice != employment3.PostalCodeReflectPractice)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_PC_REF_SP: " + prevEmp3PostalCodeReflectPractice + " -> " + employment3.PostalCodeReflectPractice);
                }
                if (prevEmp3Fax != employment3.Fax)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_FAX: " + prevEmp3Fax + " -> " + employment3.Fax);
                }
                if (prevEmp3StartDate != (employment3.StartDate != null ? ((DateTime)employment3.StartDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_START_DATE: " + prevEmp3StartDate + " -> " + (employment3.StartDate != null ? ((DateTime)employment3.StartDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp3EndDate != (employment3.EndDate != null ? ((DateTime)employment3.EndDate).ToString("dd/MM/yyyy") : string.Empty))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_END_DATE: " + prevEmp3EndDate + " -> " + (employment3.EndDate != null ? ((DateTime)employment3.EndDate).ToString("dd/MM/yyyy") : string.Empty));
                }
                if (prevEmp3Category != employment3.EmploymentRelationship)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_CATEGORY: " + GetGeneralNameImprv("EMPLOY_CATEGORY", prevEmp3Category) + " -> " + GetGeneralNameImprv("EMPLOY_CATEGORY", employment3.EmploymentRelationship));
                }
                if (prevEmp3CasualStatus != employment3.CasualStatus)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_FULL_PT_STATUS: " + GetGeneralNameImprv("FULL_PT_STATUS", prevEmp3CasualStatus) + " -> " + GetGeneralNameImprv("FULL_PT_STATUS", employment3.CasualStatus));
                }
                if (prevEmp3AvgWeeklyHours != employment3.AverageWeeklyHours.ToString())
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_HOURS_WORKED_WEEK: " + prevEmp3AvgWeeklyHours + " -> " + employment3.AverageWeeklyHours.ToString());
                }
                if (prevEmp3PrimaryRole != employment3.PrimaryRole)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_POSITION: " + GetGeneralNameImprv("POSITION", prevEmp3PrimaryRole) + " -> " + GetGeneralNameImprv("POSITION", employment3.PrimaryRole));
                }
                if (prevEmp3PracticeSetting != employment3.PracticeSetting)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_TYPE: " + GetGeneralNameImprv("EMPLOYER_TYPE", prevEmp3PracticeSetting) + " -> " + GetGeneralNameImprv("EMPLOYER_TYPE", employment3.PracticeSetting));
                }
                if (prevEmp3MajorService != employment3.MajorServices)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_AREA_PRACTICE: " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", prevEmp3MajorService) + " -> " + GetGeneralNameImprv("MAIN_AREA_PRACTICE", employment3.MajorServices));
                }
                if (prevEmp3ClientAgeRange != employment3.ClientAgeRange)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_CLNT_ARNGE: " + GetGeneralNameImprv("CLIENT_AGE_RANGE", prevEmp3ClientAgeRange) + " -> " + GetGeneralNameImprv("CLIENT_AGE_RANGE", employment3.ClientAgeRange));
                }
                if (prevEmp3FundingSource != employment3.FundingSource)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_FUNDING_SOURCE: " + GetGeneralNameImprv("FUNDING", prevEmp3FundingSource) + " -> " + GetGeneralNameImprv("FUNDING", employment3.FundingSource));
                }
                if (prevEmp3HealthCondition != employment3.HealthCondition)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "EMPLOYMENT_ALL.EMP_3_HEALTH_CONDITION: " + GetGeneralNameImprv("HEALTH_CONDITION", prevEmp3HealthCondition) + " -> " + GetGeneralNameImprv("HEALTH_CONDITION", employment3.HealthCondition));
                }
                #endregion
            }
        }

        public void UpdateApplicationUserEmploymentAddressPreferred(string UserID, bool preferred)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string preferred_string = preferred? "1": "0";
            string no_preferred_string = preferred ? "0" : "1";
            string sql_UpdateEmplWork = "Update Name_Address SET PREFERRED_MAIL = " + preferred_string + ", PREFERRED_BILL = " + preferred_string + ", PREFERRED_SHIP = " + preferred_string + 
                " WHERE ID = " + Escape(UserID) + " AND PURPOSE = 'Primary Employment'";
            db.ExecuteCommand(sql_UpdateEmplWork);
            string sql_UpdateEmplHome = "Update Name_Address SET PREFERRED_MAIL = " + no_preferred_string + ", PREFERRED_BILL = " + no_preferred_string + ", PREFERRED_SHIP =  " + no_preferred_string +
                " WHERE ID = " + Escape(UserID) + " AND PURPOSE = 'Home'";
            db.ExecuteCommand(sql_UpdateEmplHome);
        }

        // step 6

        //public User GetApplicationUserProfessionalHistoryInfo(string User_Id)
        //{
        //    User returnValue = null;
        //    //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
        //    string dbconn = WebConfigItems.GetConnectionString();
        //    var db = new clsDB();
        //    string sql = "SELECT * FROM ANN_REG_ADDL_INFO " +
        //        " WHERE ID = '" + User_Id + "'";
        //    var table = db.GetData(sql);
        //    if (table != null && table.Rows.Count > 0)
        //    {
        //        var row = table.Rows[0];
        //        returnValue = new User();
        //        returnValue.Id = User_Id;
        //        returnValue.ProfessionalHistoryInfo = new ProfessionalHistory();
        //        returnValue.ProfessionalHistoryInfo.RegisteredPractiseInOtherProvince = (string)row["OTHER_PROV_OT_PRACTICE"];
        //        returnValue.ProfessionalHistoryInfo.RegisteredPractiseInOtherProfession = (string)row["OTHER_PROF_PRACTICE"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail1 = new ProfessionalHistoryPracticeDetail();
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail2 = new ProfessionalHistoryPracticeDetail();
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail3 = new ProfessionalHistoryPracticeDetail();
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail4 = new ProfessionalHistoryPracticeDetail();

        //        returnValue.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBody = (string)row["OPROV_REG_BODY_1"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyOther = (string)row["OPROV_OTHER_1"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail1.Province = (string)row["OPROV_PROV_1"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail1.Country = (string)row["OPROV_COUNTRY_1"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail1.License = (string)row["OPROV_REG_LIC_1"];
        //        if (row["OPROV_EXP_DATE_1"] != DBNull.Value)
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate = (DateTime)row["OPROV_EXP_DATE_1"];
        //        }
        //        else
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate = null;
        //        }

        //        returnValue.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBody = (string)row["OPROV_REG_BODY_2"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyOther = (string)row["OPROV_OTHER_2"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail2.Province = (string)row["OPROV_PROV_2"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail2.Country = (string)row["OPROV_COUNTRY_2"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail2.License = (string)row["OPROV_REG_LIC_2"];
        //        if (row["OPROV_EXP_DATE_2"] != DBNull.Value)
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate = (DateTime)row["OPROV_EXP_DATE_2"];
        //        }
        //        else
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate = null;
        //        }

        //        returnValue.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBody = (string)row["OPROV_REG_BODY_3"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyOther = (string)row["OPROV_OTHER_3"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail3.Province = (string)row["OPROV_PROV_3"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail3.Country = (string)row["OPROV_COUNTRY_3"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail3.License = (string)row["OPROV_REG_LIC_3"];
        //        if (row["OPROV_EXP_DATE_3"] != DBNull.Value)
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate = (DateTime)row["OPROV_EXP_DATE_3"];
        //        }
        //        else
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate = null;
        //        }

        //        returnValue.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBody = (string)row["OPROV_REG_BODY_4"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyOther = (string)row["OPROV_OTHER_4"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail4.Province = (string)row["OPROV_PROV_4"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail4.Country = (string)row["OPROV_COUNTRY_4"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail4.License = (string)row["OPROV_REG_LIC_4"];
        //        if (row["OPROV_EXP_DATE_4"] != DBNull.Value)
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate = (DateTime)row["OPROV_EXP_DATE_4"];
        //        }
        //        else
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate = null;
        //        }

        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail1 = new ProfessionalHistoryProfessionDetail();
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail2 = new ProfessionalHistoryProfessionDetail();

        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail1.ProfessionName = (string)row["OTHER_PROF_PROF_NAME_1"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail1.RegulatoryBody = (string)row["OTHER_PROF_REG_BODY_1"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail1.Province = (string)row["OTHER_PROF_PROV_STATE_1"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail1.Country = (string)row["OTHER_PROF_COUNTRY_1"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail1.License = (string)row["OTHER_PROF_REG_LICE_1"];
        //        if (row["OTHER_PROF_EXP_DATE_1"] != DBNull.Value)
        //        {
        //            returnValue.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate = (DateTime)row["OTHER_PROF_EXP_DATE_1"];
        //        }
        //        else
        //        {
        //            returnValue.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate = null;
        //        }

        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail2.ProfessionName = (string)row["OTHER_PROF_PROF_NAME_2"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail2.RegulatoryBody = (string)row["OTHER_PROF_REG_BODY_2"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail2.Province = (string)row["OTHER_PROF_PROV_STATE_2"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail2.Country = (string)row["OTHER_PROF_COUNTRY_2"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail2.License = (string)row["OTHER_PROF_REG_LICE_2"];
        //        if (row["OTHER_PROF_EXP_DATE_2"] != DBNull.Value)
        //        {
        //            returnValue.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate = (DateTime)row["OTHER_PROF_EXP_DATE_2"];
        //        }
        //        else
        //        {
        //            returnValue.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate = null;
        //        }
        //    }
        //    return returnValue;
        //}

        //public User GetApplicationUserProfessionalHistoryInfoNew(string User_Id)
        //{
        //    User returnValue = null;
        //    //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
        //    string dbconn = WebConfigItems.GetConnectionString();
        //    var db = new clsDB();
        //    string sql = "SELECT * FROM ANN_REG_ADDL_INFO " +
        //        " WHERE ID = '" + User_Id + "'";
        //    var table = db.GetData(sql);
        //    if (table != null && table.Rows.Count > 0)
        //    {
        //        var row = table.Rows[0];
        //        returnValue = new User();
        //        returnValue.Id = User_Id;
        //        returnValue.ProfessionalHistoryInfo = new ProfessionalHistory();
        //        returnValue.ProfessionalHistoryInfo.RegisteredPractiseInOtherProvince = (string)row["OTHER_PROV_OT_PRACTICE"];
        //        returnValue.ProfessionalHistoryInfo.RegisteredPractiseInOtherProfession = (string)row["OTHER_PROF_PRACTICE"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail1 = new ProfessionalHistoryPracticeDetail();
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail2 = new ProfessionalHistoryPracticeDetail();
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail3 = new ProfessionalHistoryPracticeDetail();
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail4 = new ProfessionalHistoryPracticeDetail();

        //        returnValue.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBody = (string)row["OPROV_REG_BODY_1"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyOther = (string)row["OPROV_OTHER_1"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail1.Province = (string)row["OPROV_PROV_1"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail1.Country = (string)row["OPROV_COUNTRY_1"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail1.License = (string)row["OPROV_REG_LIC_1"];
        //        if (row["OPROV_EXP_DATE_1"] != DBNull.Value)
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate = (DateTime)row["OPROV_EXP_DATE_1"];
        //        }
        //        else
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate = null;
        //        }

        //        returnValue.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBody = (string)row["OPROV_REG_BODY_2"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyOther = (string)row["OPROV_OTHER_2"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail2.Province = (string)row["OPROV_PROV_2"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail2.Country = (string)row["OPROV_COUNTRY_2"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail2.License = (string)row["OPROV_REG_LIC_2"];
        //        if (row["OPROV_EXP_DATE_2"] != DBNull.Value)
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate = (DateTime)row["OPROV_EXP_DATE_2"];
        //        }
        //        else
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate = null;
        //        }

        //        returnValue.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBody = (string)row["OPROV_REG_BODY_3"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyOther = (string)row["OPROV_OTHER_3"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail3.Province = (string)row["OPROV_PROV_3"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail3.Country = (string)row["OPROV_COUNTRY_3"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail3.License = (string)row["OPROV_REG_LIC_3"];
        //        if (row["OPROV_EXP_DATE_3"] != DBNull.Value)
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate = (DateTime)row["OPROV_EXP_DATE_3"];
        //        }
        //        else
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate = null;
        //        }

        //        returnValue.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBody = (string)row["OPROV_REG_BODY_4"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyOther = (string)row["OPROV_OTHER_4"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail4.Province = (string)row["OPROV_PROV_4"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail4.Country = (string)row["OPROV_COUNTRY_4"];
        //        returnValue.ProfessionalHistoryInfo.PracticeDetail4.License = (string)row["OPROV_REG_LIC_4"];
        //        if (row["OPROV_EXP_DATE_4"] != DBNull.Value)
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate = (DateTime)row["OPROV_EXP_DATE_4"];
        //        }
        //        else
        //        {
        //            returnValue.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate = null;
        //        }

        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail1 = new ProfessionalHistoryProfessionDetail();
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail2 = new ProfessionalHistoryProfessionDetail();

        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail1.ProfessionName = (string)row["OTHER_PROF_PROF_NAME_1"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail1.RegulatoryBody = (string)row["OTHER_PROF_REG_BODY_1"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail1.Province = (string)row["OTHER_PROF_PROV_STATE_1"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail1.Country = (string)row["OTHER_PROF_COUNTRY_1"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail1.License = (string)row["OTHER_PROF_REG_LICE_1"];
        //        if (row["OTHER_PROF_EXP_DATE_1"] != DBNull.Value)
        //        {
        //            returnValue.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate = (DateTime)row["OTHER_PROF_EXP_DATE_1"];
        //        }
        //        else
        //        {
        //            returnValue.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate = null;
        //        }

        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail2.ProfessionName = (string)row["OTHER_PROF_PROF_NAME_2"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail2.RegulatoryBody = (string)row["OTHER_PROF_REG_BODY_2"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail2.Province = (string)row["OTHER_PROF_PROV_STATE_2"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail2.Country = (string)row["OTHER_PROF_COUNTRY_2"];
        //        returnValue.ProfessionalHistoryInfo.ProfessionDetail2.License = (string)row["OTHER_PROF_REG_LICE_2"];
        //        if (row["OTHER_PROF_EXP_DATE_2"] != DBNull.Value)
        //        {
        //            returnValue.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate = (DateTime)row["OTHER_PROF_EXP_DATE_2"];
        //        }
        //        else
        //        {
        //            returnValue.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate = null;
        //        }
        //    }
        //    return returnValue;
        //}

        public ProfessionalHistory GetAppProfessionalHistoryInfoNew(string User_Id)
        {
            ProfessionalHistory history = null;

            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM COTO_OTHER_REG " +
                " WHERE ID = " + Escape(User_Id) + " ORDER BY SEQN";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                history = new ProfessionalHistory();
                history.PracticeDetails = new List<ProfessionalHistoryPracticeDetailNew>();
                history.PracticeOtherDetails = new List<ProfessionalHistoryPracticeDetailNew>();
                int mainPractOrder = 1;
                int mainOtherPractOrder = 1;

                foreach (DataRow row in table.Rows)
                {
                    var historyItem = new ProfessionalHistoryPracticeDetailNew();
                    historyItem.SEQN = (int)row["SEQN"];
                    historyItem.RegType = (string)row["REG_TYPE"];
                    
                    //historyItem.OtherProfessionType = (string)row["OTHER_PROF_TYPE"];
                    
                    historyItem.Province = (string)row["REG_PROVINCE_STATE"];
                    historyItem.Country = (string)row["REG_COUNTRY"];
                    historyItem.RegNumber = (string)row["REG_NUMBER"];
                    historyItem.RegStatus = (string)row["REG_STATUS"];
                    historyItem.InitMonth = (string)row["INIT_REG_MONTH"];
                    historyItem.InitYear = (string)row["INIT_REG_YEAR"];
                    if (row["EXPIRY"] != DBNull.Value)
                    {
                        historyItem.ExpiryDate = (DateTime)row["EXPIRY"];
                    }
                    else
                    {
                        historyItem.ExpiryDate = null;
                    }

                    if (historyItem.RegType == "OT")
                    {
                        historyItem.RegulatoryBody = (string)row["OT_REG_BODY"];
                        historyItem.RegulatoryBodyOther = (string)row["OT_REG_BODY_OTHER"];
                        historyItem.OrderNum = mainPractOrder; mainPractOrder++;
                        history.PracticeDetails.Add(historyItem);
                    }
                    else if (historyItem.RegType == "OTHER")
                    {
                        historyItem.OtherProfessionType = (string)row["OTHER_PROF_TYPE"];
                        historyItem.OtherProfessionTypeOther = (string)row["OTHER_PROF_TYPE_OTHER"];
                        historyItem.RegulatoryBody = (string)row["OTHER_PROF_REGBODY"];
                        historyItem.RegulatoryBodyOther = (string)row["OTHER_PROF_REGBODY_OTHER"];
                        historyItem.OrderNum = mainOtherPractOrder; mainOtherPractOrder++;
                        history.PracticeOtherDetails.Add(historyItem);
                    }
                }
            }

            return history;
        }

        public void UpdateApplicationUserProfessionalHistoryLogged(string UserID, User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            if (user.ProfessionalHistoryInfo != null)
            {
                string prevOtherProvince = string.Empty;
                string prevOtherProfession = string.Empty;

                string prevPract1RegBody = string.Empty;
                string prevPract1RegBodyOther = string.Empty;
                string prevPract1Province = string.Empty;
                string prevPract1Country = string.Empty;
                string prevPract1License = string.Empty;
                string prevPract1ExpireDate = string.Empty;

                string prevPract2RegBody = string.Empty;
                string prevPract2RegBodyOther = string.Empty;
                string prevPract2Province = string.Empty;
                string prevPract2Country = string.Empty;
                string prevPract2License = string.Empty;
                string prevPract2ExpireDate = string.Empty;

                string prevPract3RegBody = string.Empty;
                string prevPract3RegBodyOther = string.Empty;
                string prevPract3Province = string.Empty;
                string prevPract3Country = string.Empty;
                string prevPract3License = string.Empty;
                string prevPract3ExpireDate = string.Empty;

                string prevPract4RegBody = string.Empty;
                string prevPract4RegBodyOther = string.Empty;
                string prevPract4Province = string.Empty;
                string prevPract4Country = string.Empty;
                string prevPract4License = string.Empty;
                string prevPract4ExpireDate = string.Empty;

                string prevProf1RegBody = string.Empty;
                string prevProf1ProfName = string.Empty;
                string prevProf1Province = string.Empty;
                string prevProf1Country = string.Empty;
                string prevProf1License = string.Empty;
                string prevProf1ExpireDate = string.Empty;

                string prevProf2RegBody = string.Empty;
                string prevProf2ProfName = string.Empty;
                string prevProf2Province = string.Empty;
                string prevProf2Country = string.Empty;
                string prevProf2License = string.Empty;
                string prevProf2ExpireDate = string.Empty;

                string sql_GetCurrentHist = "SELECT * FROM ANN_REG_ADDL_INFO WHERE ID=" + Escape(UserID);

                var table = db.GetData(sql_GetCurrentHist);
                if (table != null && table.Rows.Count == 1)
                {
                    DataRow row = table.Rows[0];

                    prevOtherProvince = (string)row["OTHER_PROV_OT_PRACTICE"];
                    prevOtherProfession = (string)row["OTHER_PROF_PRACTICE"];

                    prevPract1RegBody = GetGeneralName("REG_BODIES", (string)row["OPROV_REG_BODY_1"]);
                    prevPract1RegBodyOther = (string)row["OPROV_OTHER_1"];
                    prevPract1Province = (string)row["OPROV_PROV_1"];
                    prevPract1Country = (string)row["OPROV_COUNTRY_1"];
                    prevPract1License = (string)row["OPROV_REG_LIC_1"];
                    if (row["OPROV_EXP_DATE_1"] != DBNull.Value)
                    {
                        prevPract1ExpireDate = ((DateTime)row["OPROV_EXP_DATE_1"]).ToString("MM/dd/yyyy");
                    }

                    prevPract2RegBody = GetGeneralName("REG_BODIES", (string)row["OPROV_REG_BODY_2"]);
                    prevPract2RegBodyOther = (string)row["OPROV_OTHER_2"];
                    prevPract2Province = (string)row["OPROV_PROV_2"];
                    prevPract2Country = (string)row["OPROV_COUNTRY_2"];
                    prevPract2License = (string)row["OPROV_REG_LIC_2"];
                    if (row["OPROV_EXP_DATE_2"] != DBNull.Value)
                    {
                        prevPract2ExpireDate = ((DateTime)row["OPROV_EXP_DATE_2"]).ToString("MM/dd/yyyy");
                    }

                    prevPract3RegBody = GetGeneralName("REG_BODIES", (string)row["OPROV_REG_BODY_3"]);
                    prevPract3RegBodyOther = (string)row["OPROV_OTHER_3"];
                    prevPract3Province = (string)row["OPROV_PROV_3"];
                    prevPract3Country = (string)row["OPROV_COUNTRY_3"];
                    prevPract3License = (string)row["OPROV_REG_LIC_3"];
                    if (row["OPROV_EXP_DATE_3"] != DBNull.Value)
                    {
                        prevPract3ExpireDate = ((DateTime)row["OPROV_EXP_DATE_3"]).ToString("MM/dd/yyyy");
                    }

                    prevPract4RegBody = GetGeneralName("REG_BODIES", (string)row["OPROV_REG_BODY_4"]);
                    prevPract4RegBodyOther = (string)row["OPROV_OTHER_4"];
                    prevPract4Province = (string)row["OPROV_PROV_4"];
                    prevPract4Country = (string)row["OPROV_COUNTRY_4"];
                    prevPract4License = (string)row["OPROV_REG_LIC_4"];
                    if (row["OPROV_EXP_DATE_4"] != DBNull.Value)
                    {
                        prevPract4ExpireDate = ((DateTime)row["OPROV_EXP_DATE_4"]).ToString("MM/dd/yyyy");
                    }

                    prevProf1RegBody = (string)row["OTHER_PROF_REG_BODY_1"];
                    prevProf1ProfName = (string)row["OTHER_PROF_PROF_NAME_1"];
                    prevProf1Province = (string)row["OTHER_PROF_PROV_STATE_1"];
                    prevProf1Country = (string)row["OTHER_PROF_COUNTRY_1"];
                    prevProf1License = (string)row["OTHER_PROF_REG_LICE_1"];
                    if (row["OTHER_PROF_EXP_DATE_1"] != DBNull.Value)
                    {
                        prevProf1ExpireDate = ((DateTime)row["OTHER_PROF_EXP_DATE_1"]).ToString("MM/dd/yyyy");
                    }

                    prevProf2RegBody = (string)row["OTHER_PROF_REG_BODY_2"];
                    prevProf2ProfName = (string)row["OTHER_PROF_PROF_NAME_2"];
                    prevProf2Province = (string)row["OTHER_PROF_PROV_STATE_2"];
                    prevProf2Country = (string)row["OTHER_PROF_COUNTRY_2"];
                    prevProf2License = (string)row["OTHER_PROF_REG_LICE_2"];
                    if (row["OTHER_PROF_EXP_DATE_2"] != DBNull.Value)
                    {
                        prevProf2ExpireDate = ((DateTime)row["OTHER_PROF_EXP_DATE_2"]).ToString("MM/dd/yyyy");
                    }
                }

                string sql_UpdateHist = "UPDATE ANN_REG_ADDL_INFO SET ";

                // first practise
                if (user.ProfessionalHistoryInfo.PracticeDetail1 != null)
                {
                    sql_UpdateHist += " OPROV_REG_BODY_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBody) + ", " +
                    "OPROV_OTHER_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyOther) + ", " +
                    "OPROV_PROV_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.Province) + ", " +
                    "OPROV_COUNTRY_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.Country) + ", " +
                    "OPROV_REG_LIC_1 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail1.License) + ", ";

                    if (user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate;
                        sql_UpdateHist += "OPROV_EXP_DATE_1 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OPROV_EXP_DATE_1 = NULL, ";
                    }
                    //prevPract1RegBody = (string)row["OPROV_REG_BODY_1"];
                    //prevPract1RegBodyOther = (string)row["OPROV_OTHER_1"];
                    //prevPract1Province = (string)row["OPROV_PROV_1"];
                    //prevPract1Country = (string)row["OPROV_COUNTRY_1"];
                    //prevPract1License = (string)row["OPROV_REG_LIC_1"];
                    //if (row["OPROV_EXP_DATE_1"] != DBNull.Value)
                    //{
                    //    prevPract1ExpireDate = ((DateTime)row["OPROV_EXP_DATE_1"]).ToString("MM/dd/yyyy");
                    //}

                    if (prevPract1RegBody != user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyText)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_BODY_1: " + prevPract1RegBody + " -> " + user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyText);
                    }
                    if (prevPract1RegBodyOther != user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyOther)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_OTHER_1: " + prevPract1RegBodyOther + " -> " + user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyOther);
                    }
                    if (prevPract1Province != user.ProfessionalHistoryInfo.PracticeDetail1.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_PROV_1: " + prevPract1Province + " -> " + user.ProfessionalHistoryInfo.PracticeDetail1.Province);
                    }
                    if (prevPract1Country != user.ProfessionalHistoryInfo.PracticeDetail1.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_COUNTRY_1: " + prevPract1Country + " -> " + user.ProfessionalHistoryInfo.PracticeDetail1.Country);
                    }
                    if (prevPract1License != user.ProfessionalHistoryInfo.PracticeDetail1.License)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_LIC_1: " + prevPract1License + " -> " + user.ProfessionalHistoryInfo.PracticeDetail1.License);
                    }
                    if (prevPract1ExpireDate != ((user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_EXP_DATE_1: " + prevPract1ExpireDate + " -> " + ((user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }
                }

                // second practise
                if (user.ProfessionalHistoryInfo.PracticeDetail2 != null)
                {
                    sql_UpdateHist += " OPROV_REG_BODY_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBody) + ", " +
                    "OPROV_OTHER_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyOther) + ", " +
                    "OPROV_PROV_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.Province) + ", " +
                    "OPROV_COUNTRY_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.Country) + ", " +
                    "OPROV_REG_LIC_2 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail2.License) + ", ";

                    if (user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate;
                        sql_UpdateHist += "OPROV_EXP_DATE_2 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OPROV_EXP_DATE_2 = NULL, ";
                    }

                    if (prevPract2RegBody != user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyText)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_BODY_2: " + prevPract2RegBody + " -> " + user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyText);
                    }
                    if (prevPract2RegBodyOther != user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyOther)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_OTHER_2: " + prevPract2RegBodyOther + " -> " + user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyOther);
                    }
                    if (prevPract2Province != user.ProfessionalHistoryInfo.PracticeDetail2.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_PROV_2: " + prevPract2Province + " -> " + user.ProfessionalHistoryInfo.PracticeDetail2.Province);
                    }
                    if (prevPract2Country != user.ProfessionalHistoryInfo.PracticeDetail2.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_COUNTRY_2: " + prevPract2Country + " -> " + user.ProfessionalHistoryInfo.PracticeDetail2.Country);
                    }
                    if (prevPract2License != user.ProfessionalHistoryInfo.PracticeDetail2.License)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_LIC_2: " + prevPract2License + " -> " + user.ProfessionalHistoryInfo.PracticeDetail2.License);
                    }
                    if (prevPract2ExpireDate != ((user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_EXP_DATE_2: " + prevPract2ExpireDate + " -> " + ((user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }
                }

                // third practise
                if (user.ProfessionalHistoryInfo.PracticeDetail3 != null)
                {
                    sql_UpdateHist += " OPROV_REG_BODY_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBody) + ", " +
                    "OPROV_OTHER_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyOther) + ", " +
                    "OPROV_PROV_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.Province) + ", " +
                    "OPROV_COUNTRY_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.Country) + ", " +
                    "OPROV_REG_LIC_3 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail3.License) + ", ";

                    if (user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate;
                        sql_UpdateHist += "OPROV_EXP_DATE_3 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OPROV_EXP_DATE_3 = NULL, ";
                    }

                    if (prevPract3RegBody != user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyText)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_BODY_3: " + prevPract3RegBody + " -> " + user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyText);
                    }
                    if (prevPract3RegBodyOther != user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyOther)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_OTHER_3: " + prevPract3RegBodyOther + " -> " + user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyOther);
                    }
                    if (prevPract3Province != user.ProfessionalHistoryInfo.PracticeDetail3.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_PROV_3: " + prevPract3Province + " -> " + user.ProfessionalHistoryInfo.PracticeDetail3.Province);
                    }
                    if (prevPract3Country != user.ProfessionalHistoryInfo.PracticeDetail3.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_COUNTRY_3: " + prevPract3Country + " -> " + user.ProfessionalHistoryInfo.PracticeDetail3.Country);
                    }
                    if (prevPract3License != user.ProfessionalHistoryInfo.PracticeDetail3.License)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_LIC_3: " + prevPract3License + " -> " + user.ProfessionalHistoryInfo.PracticeDetail3.License);
                    }
                    if (prevPract3ExpireDate != ((user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_EXP_DATE_3: " + prevPract3ExpireDate + " -> " + ((user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }

                }

                // fourth practise
                if (user.ProfessionalHistoryInfo.PracticeDetail4 != null)
                {
                    sql_UpdateHist += " OPROV_REG_BODY_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBody) + ", " +
                    "OPROV_OTHER_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyOther) + ", " +
                    "OPROV_PROV_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.Province) + ", " +
                    "OPROV_COUNTRY_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.Country) + ", " +
                    "OPROV_REG_LIC_4 = " + Escape(user.ProfessionalHistoryInfo.PracticeDetail4.License) + ", ";

                    if (user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate;
                        sql_UpdateHist += "OPROV_EXP_DATE_4 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OPROV_EXP_DATE_4 = NULL, ";
                    }

                    if (prevPract4RegBody != user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyText)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_BODY_4: " + prevPract4RegBody + " -> " + user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyText);
                    }
                    if (prevPract4RegBodyOther != user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyOther)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_OTHER_4: " + prevPract4RegBodyOther + " -> " + user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyOther);
                    }
                    if (prevPract4Province != user.ProfessionalHistoryInfo.PracticeDetail4.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_PROV_4: " + prevPract4Province + " -> " + user.ProfessionalHistoryInfo.PracticeDetail4.Province);
                    }
                    if (prevPract4Country != user.ProfessionalHistoryInfo.PracticeDetail4.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_COUNTRY_4: " + prevPract4Country + " -> " + user.ProfessionalHistoryInfo.PracticeDetail4.Country);
                    }
                    if (prevPract4License != user.ProfessionalHistoryInfo.PracticeDetail4.License)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_REG_LIC_4: " + prevPract4License + " -> " + user.ProfessionalHistoryInfo.PracticeDetail4.License);
                    }
                    if (prevPract4ExpireDate != ((user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OPROV_EXP_DATE_4: " + prevPract4ExpireDate + " -> " + ((user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }
                }

                // first profession
                if (user.ProfessionalHistoryInfo.ProfessionDetail1 != null)
                {
                    sql_UpdateHist += " OTHER_PROF_PROF_NAME_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.ProfessionName) + ", " +
                    "OTHER_PROF_REG_BODY_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.RegulatoryBody) + ", " +
                    "OTHER_PROF_PROV_STATE_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.Province) + ", " +
                    "OTHER_PROF_COUNTRY_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.Country) + ", " +
                    "OTHER_PROF_REG_LICE_1 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail1.License) + ", ";

                    if (user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate;
                        sql_UpdateHist += "OTHER_PROF_EXP_DATE_1 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OTHER_PROF_EXP_DATE_1 = NULL, ";
                    }

                    if (prevProf1RegBody != user.ProfessionalHistoryInfo.ProfessionDetail1.RegulatoryBody)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_REG_BODY_1: " + prevProf1RegBody + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail1.RegulatoryBody);
                    }
                    if (prevProf1ProfName != user.ProfessionalHistoryInfo.ProfessionDetail1.ProfessionName)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_PROF_NAME_1: " + prevProf1ProfName + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail1.ProfessionName);
                    }
                    if (prevProf1Province != user.ProfessionalHistoryInfo.ProfessionDetail1.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_PROV_STATE_1: " + prevProf1Province + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail1.Province);
                    }
                    if (prevProf1Country != user.ProfessionalHistoryInfo.ProfessionDetail1.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_COUNTRY_1: " + prevProf1Country + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail1.Country);
                    }
                    if (prevProf1License != user.ProfessionalHistoryInfo.ProfessionDetail1.License)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_REG_LICE_1: " + prevProf1License + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail1.License);
                    }
                    if (prevProf1ExpireDate != ((user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_EXP_DATE_1: " + prevProf1ExpireDate + " -> " + ((user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }
                }

                // second profession
                if (user.ProfessionalHistoryInfo.ProfessionDetail2 != null)
                {
                    sql_UpdateHist += " OTHER_PROF_PROF_NAME_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.ProfessionName) + ", " +
                    "OTHER_PROF_REG_BODY_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.RegulatoryBody) + ", " +
                    "OTHER_PROF_PROV_STATE_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.Province) + ", " +
                    "OTHER_PROF_COUNTRY_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.Country) + ", " +
                    "OTHER_PROF_REG_LICE_2 = " + Escape(user.ProfessionalHistoryInfo.ProfessionDetail2.License) + ", ";

                    if (user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate != null)
                    {
                        DateTime expdate = (DateTime)user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate;
                        sql_UpdateHist += "OTHER_PROF_EXP_DATE_2 = " + Escape(expdate.ToString("yyyy-MM-dd")) + ", ";
                    }
                    else
                    {
                        sql_UpdateHist += "OTHER_PROF_EXP_DATE_2 = NULL, ";
                    }

                    if (prevProf2RegBody != user.ProfessionalHistoryInfo.ProfessionDetail2.RegulatoryBody)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_REG_BODY_2: " + prevProf2RegBody + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail2.RegulatoryBody);
                    }
                    if (prevProf2ProfName != user.ProfessionalHistoryInfo.ProfessionDetail2.ProfessionName)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_PROF_NAME_2: " + prevProf2ProfName + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail2.ProfessionName);
                    }
                    if (prevProf2Province != user.ProfessionalHistoryInfo.ProfessionDetail2.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_PROV_STATE_2: " + prevProf2Province + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail2.Province);
                    }
                    if (prevProf2Country != user.ProfessionalHistoryInfo.ProfessionDetail2.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_COUNTRY_2: " + prevProf2Country + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail2.Country);
                    }
                    if (prevProf2License != user.ProfessionalHistoryInfo.ProfessionDetail2.License)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_REG_LICE_2: " + prevProf2License + " -> " + user.ProfessionalHistoryInfo.ProfessionDetail2.License);
                    }
                    if (prevProf2ExpireDate != ((user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_EXP_DATE_2: " + prevProf2ExpireDate + " -> " + ((user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate != null) ? ((DateTime)user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }
                }

                sql_UpdateHist += " OTHER_PROV_OT_PRACTICE = " + Escape(user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProvince) + ", " +
                    "OTHER_PROF_PRACTICE = " + Escape(user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProfession);
                sql_UpdateHist += " WHERE ID = " + Escape(user.Id);

                if (prevOtherProvince != user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProvince)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROV_OT_PRACTICE: " + prevOtherProvince + " -> " + user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProvince);
                }
                if (prevOtherProfession != user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProfession)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "ANN_REG_ADDL_INFO.OTHER_PROF_PRACTICE: " + prevOtherProfession + " -> " + user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProfession);
                }

                db.ExecuteCommand(sql_UpdateHist);
            }
        }

        public string UpdateApplicationUserProfessionalHistoryLoggedNew(string UserID, ProfessionalHistory history)
        {
            string step = "0";
            string message = string.Empty;
            try
            {
                //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
                string dbconn = WebConfigItems.GetConnectionString();
                var db = new clsDB();

                foreach (var hItem in history.PracticeDetails)
                {
                    string prevRegBody = string.Empty;
                    string prevRegBodyOther = string.Empty;
                    string prevProvince = string.Empty;
                    string prevCountry = string.Empty;
                    string prevRegNumber = string.Empty;
                    string prevRegStatus = string.Empty;
                    string prevInitMonth = string.Empty;
                    string prevInitYear = string.Empty;
                    string prevExpireDate = string.Empty;

                    if (hItem.SEQN > 0)
                    {
                        step = "1";
                        string sql_GetCurrentHist = "SELECT * FROM COTO_OTHER_REG WHERE ID =" + Escape(UserID) + " AND SEQN = " + hItem.SEQN.ToString();

                        var table = db.GetData(sql_GetCurrentHist);
                        if (table != null && table.Rows.Count == 1)
                        {
                            DataRow row = table.Rows[0];

                            prevRegBody = (string)row["OT_REG_BODY"];
                            prevRegBodyOther = (string)row["OT_REG_BODY_OTHER"];
                            prevProvince = (string)row["REG_PROVINCE_STATE"];
                            prevCountry = (string)row["REG_COUNTRY"];
                            prevRegNumber = (string)row["REG_NUMBER"];
                            prevRegStatus = (string)row["REG_STATUS"];
                            prevInitMonth = (string)row["INIT_REG_MONTH"];
                            prevInitYear = (string)row["INIT_REG_YEAR"];
                            if (row["EXPIRY"] != DBNull.Value)
                            {
                                prevExpireDate = ((DateTime)row["EXPIRY"]).ToString("MM/dd/yyyy");
                            }
                        }
                    }

                    // updating existing record
                    if (hItem.SEQN > 0 && !string.IsNullOrEmpty(hItem.RegulatoryBody))
                    {
                        step = "2";
                        string sql_UpdateHist = "UPDATE COTO_OTHER_REG SET ";
                        sql_UpdateHist += " OT_REG_BODY = " + Escape(hItem.RegulatoryBody) + ", " +
                            "OT_REG_BODY_OTHER = " + Escape(hItem.RegulatoryBodyOther) + ", " +
                            "REG_PROVINCE_STATE = " + Escape(hItem.Province) + ", " +
                            "REG_COUNTRY = " + Escape(hItem.Country) + ", " +
                            "REG_NUMBER = " + Escape(hItem.RegNumber) + ", " +
                            "REG_STATUS = " + Escape(hItem.RegStatus) + ", " +
                            "INIT_REG_MONTH = " + Escape(hItem.InitMonth) + ", " +
                            "INIT_REG_YEAR = " + Escape(hItem.InitYear) + ", ";

                        if (hItem.ExpiryDate != null)
                        {
                            DateTime expdate = (DateTime)hItem.ExpiryDate;
                            sql_UpdateHist += "EXPIRY = " + Escape(expdate.ToString("yyyy-MM-dd"));
                        }
                        else
                        {
                            sql_UpdateHist += "EXPIRY = NULL ";
                        }

                        sql_UpdateHist += " WHERE ID = " + Escape(UserID) + " AND SEQN = " + hItem.SEQN.ToString();
                        db.ExecuteCommand(sql_UpdateHist);

                        if (prevRegBody != hItem.RegulatoryBody)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OT_REG_BODY (" + hItem.SEQN.ToString() + "): " + prevRegBody + " -> " + hItem.RegulatoryBody);
                        }
                        if (!string.Equals(prevRegBodyOther, hItem.RegulatoryBodyOther))
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OT_REG_BODY_OTHER (" + hItem.SEQN.ToString() + "): " + prevRegBodyOther + " -> " + hItem.RegulatoryBodyOther);
                        }
                        if (prevProvince != hItem.Province)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_PROVINCE_STATE (" + hItem.SEQN.ToString() + "): " + prevProvince + " -> " + hItem.Province);
                        }
                        if (prevCountry != hItem.Country)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_COUNTRY (" + hItem.SEQN.ToString() + "): " + prevCountry + " -> " + hItem.Country);
                        }
                        if (prevRegNumber != hItem.RegNumber)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_NUMBER (" + hItem.SEQN.ToString() + "): " + prevRegNumber + " -> " + hItem.RegNumber);
                        }
                        if (prevRegStatus != hItem.RegStatus)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_STATUS (" + hItem.SEQN.ToString() + "): " + prevRegStatus + " -> " + hItem.RegStatus);
                        }
                        if (prevInitMonth != hItem.InitMonth)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_MONTH (" + hItem.SEQN.ToString() + "): " + prevInitMonth + " -> " + hItem.InitMonth);
                        }
                        if (prevInitYear != hItem.InitYear)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_YEAR (" + hItem.SEQN.ToString() + "): " + prevInitYear + " -> " + hItem.InitYear);
                        }
                        if (prevExpireDate != ((hItem.ExpiryDate != null) ? ((DateTime)hItem.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.EXPIRY (" + hItem.SEQN.ToString() + "): " + prevExpireDate + " -> " + ((hItem.ExpiryDate != null) ? ((DateTime)hItem.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                        }
                    }
                    else if (hItem.SEQN == 0) // inserting new record to db
                    {
                        step = "3";
                        int newSEQN = GetCounter("COTO_OTHER_REG");
                        string sql_InsertHist = "INSERT INTO COTO_OTHER_REG (ID, SEQN, REG_TYPE, OT_REG_BODY, OT_REG_BODY_OTHER, REG_PROVINCE_STATE, REG_COUNTRY, REG_NUMBER, REG_STATUS, INIT_REG_MONTH, INIT_REG_YEAR, EXPIRY ) ";
                        sql_InsertHist += string.Format(" VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})", Escape(UserID), newSEQN, Escape("OT"), Escape(hItem.RegulatoryBody),
                            Escape(hItem.RegulatoryBodyOther), Escape(hItem.Province), Escape(hItem.Country), Escape(hItem.RegNumber), Escape(hItem.RegStatus), Escape(hItem.InitMonth),
                            Escape(hItem.InitYear), (hItem.ExpiryDate != null ? Escape(((DateTime)hItem.ExpiryDate).ToString("yyyy-MM-dd")) : "NULL"));
                        db.ExecuteCommand(sql_InsertHist);

                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OT_REG_BODY (" + newSEQN.ToString() + "): " + prevRegBody + " -> " + hItem.RegulatoryBody);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OT_REG_BODY_OTHER (" + newSEQN.ToString() + "): " + prevRegBodyOther + " -> " + hItem.RegulatoryBodyOther);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_PROVINCE_STATE (" + newSEQN.ToString() + "): " + prevProvince + " -> " + hItem.Province);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_COUNTRY (" + newSEQN.ToString() + "): " + prevCountry + " -> " + hItem.Country);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_NUMBER (" + newSEQN.ToString() + "): " + prevRegNumber + " -> " + hItem.RegNumber);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_STATUS (" + newSEQN.ToString() + "): " + prevRegStatus + " -> " + hItem.RegStatus);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_MONTH (" + newSEQN.ToString() + "): " + prevInitMonth + " -> " + hItem.InitMonth);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_YEAR (" + newSEQN.ToString() + "): " + prevInitYear + " -> " + hItem.InitYear);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.EXPIRY (" + newSEQN.ToString() + "): " + prevExpireDate + " -> " + ((hItem.ExpiryDate != null) ? ((DateTime)hItem.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }
                    else if (hItem.SEQN > 0 && string.IsNullOrEmpty(hItem.RegulatoryBody))
                    {
                        step = "4";
                        string sql_DeleteHist = string.Format("DELETE FROM COTO_OTHER_REG WHERE ID = {0} AND SEQN = {1}", Escape(UserID), hItem.SEQN);
                        db.ExecuteCommand(sql_DeleteHist);
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OT_REG_BODY (" + hItem.SEQN.ToString() + "): " + prevRegBody + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OT_REG_BODY_OTHER (" + hItem.SEQN.ToString() + "): " + prevRegBodyOther + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_PROVINCE_STATE (" + hItem.SEQN.ToString() + "): " + prevProvince + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_COUNTRY (" + hItem.SEQN.ToString() + "): " + prevCountry + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_NUMBER (" + hItem.SEQN.ToString() + "): " + prevRegNumber + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_STATUS (" + hItem.SEQN.ToString() + "): " + prevRegStatus  + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_MONTH (" + hItem.SEQN.ToString() + "): " + prevInitMonth + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_YEAR (" + hItem.SEQN.ToString() + "): " + prevInitYear + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.EXPIRY (" + hItem.SEQN.ToString() + "): " + prevExpireDate + " -> ");
                    }
                }

                // other professions to update/insert
                foreach (var hItem in history.PracticeOtherDetails)
                {
                    step = "5";
                    string prevNameProf = string.Empty;
                    string prevNameProfOther = string.Empty;
                    string prevRegBody = string.Empty;
                    string prevRegBodyOther = string.Empty;
                    string prevProvince = string.Empty;
                    string prevCountry = string.Empty;
                    string prevRegNumber = string.Empty;
                    string prevRegStatus = string.Empty;
                    string prevInitMonth = string.Empty;
                    string prevInitYear = string.Empty;
                    string prevExpireDate = string.Empty;

                    if (hItem.SEQN > 0)
                    {
                        step = "6";
                        string sql_GetCurrentHist = "SELECT * FROM COTO_OTHER_REG WHERE ID =" + Escape(UserID) + " AND SEQN = " + hItem.SEQN.ToString();

                        var table = db.GetData(sql_GetCurrentHist);
                        if (table != null && table.Rows.Count == 1)
                        {
                            DataRow row = table.Rows[0];
                            prevNameProf = (string)row["OTHER_PROF_TYPE"];
                            prevNameProfOther = (string)row["OTHER_PROF_TYPE_OTHER"];
                            prevRegBody = (string)row["OTHER_PROF_REGBODY"];
                            prevRegBodyOther = (string)row["OTHER_PROF_REGBODY_OTHER"];
                            prevProvince = (string)row["REG_PROVINCE_STATE"];
                            prevCountry = (string)row["REG_COUNTRY"];
                            prevRegNumber = (string)row["REG_NUMBER"];
                            prevRegStatus = (string)row["REG_STATUS"];
                            prevInitMonth = (string)row["INIT_REG_MONTH"];
                            prevInitYear = (string)row["INIT_REG_YEAR"];
                            if (row["EXPIRY"] != DBNull.Value)
                            {
                                prevExpireDate = ((DateTime)row["EXPIRY"]).ToString("MM/dd/yyyy");
                            }
                        }
                    }

                    // updating existing record
                    if (hItem.SEQN > 0 && !string.IsNullOrEmpty(hItem.OtherProfessionType))
                    {
                        step = "7";
                        string sql_UpdateHist = "UPDATE COTO_OTHER_REG SET ";
                        sql_UpdateHist += " OTHER_PROF_TYPE = " + Escape(hItem.OtherProfessionType) + ", " +
                            " OTHER_PROF_TYPE_OTHER = " + Escape(hItem.OtherProfessionTypeOther) + ", " +
                            " OTHER_PROF_REGBODY = " + Escape(hItem.RegulatoryBody) + ", " +
                            " OTHER_PROF_REGBODY_OTHER = " + Escape(hItem.RegulatoryBodyOther) + ", " +
                            " REG_PROVINCE_STATE = " + Escape(hItem.Province) + ", " +
                            " REG_COUNTRY = " + Escape(hItem.Country) + ", " +
                            " REG_NUMBER = " + Escape(hItem.RegNumber) + ", " +
                            " REG_STATUS = " + Escape(hItem.RegStatus) + ", " +
                            " INIT_REG_MONTH = " + Escape(hItem.InitMonth) + ", " +
                            " INIT_REG_YEAR = " + Escape(hItem.InitYear) + ", ";

                        if (hItem.ExpiryDate != null)
                        {
                            DateTime expdate = (DateTime)hItem.ExpiryDate;
                            sql_UpdateHist += " EXPIRY = " + Escape(expdate.ToString("yyyy-MM-dd"));
                        }
                        else
                        {
                            sql_UpdateHist += " EXPIRY = NULL ";
                        }

                        sql_UpdateHist += " WHERE ID = " + Escape(UserID) + " AND SEQN = " + hItem.SEQN.ToString();
                        db.ExecuteCommand(sql_UpdateHist);

                        if (prevNameProf != hItem.OtherProfessionType)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_TYPE (" + hItem.SEQN.ToString() + "): " + prevNameProf + " -> " + hItem.OtherProfessionType);
                        }
                        if (prevNameProfOther != hItem.OtherProfessionTypeOther)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_TYPE_OTHER (" + hItem.SEQN.ToString() + "): " + prevNameProfOther + " -> " + hItem.OtherProfessionTypeOther);
                        }
                        if (prevRegBody != hItem.RegulatoryBody)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_REGBODY (" + hItem.SEQN.ToString() + "): " + prevRegBody + " -> " + hItem.RegulatoryBody);
                        }
                        if (!string.Equals(prevRegBodyOther, hItem.RegulatoryBodyOther))
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_REGBODY_OTHER (" + hItem.SEQN.ToString() + "): " + prevRegBodyOther + " -> " + hItem.RegulatoryBodyOther);
                        }
                        if (prevProvince != hItem.Province)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_PROVINCE_STATE (" + hItem.SEQN.ToString() + "): " + prevProvince + " -> " + hItem.Province);
                        }
                        if (prevCountry != hItem.Country)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_COUNTRY (" + hItem.SEQN.ToString() + "): " + prevCountry + " -> " + hItem.Country);
                        }
                        if (prevRegNumber != hItem.RegNumber)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_NUMBER (" + hItem.SEQN.ToString() + "): " + prevRegNumber + " -> " + hItem.RegNumber);
                        }
                        if (prevRegStatus != hItem.RegStatus)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_STATUS (" + hItem.SEQN.ToString() + "): " + prevRegStatus + " -> " + hItem.RegStatus);
                        }
                        if (prevInitMonth != hItem.InitMonth)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_MONTH (" + hItem.SEQN.ToString() + "): " + prevInitMonth + " -> " + hItem.InitMonth);
                        }
                        if (prevInitYear != hItem.InitYear)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_YEAR (" + hItem.SEQN.ToString() + "): " + prevInitYear + " -> " + hItem.InitYear);
                        }
                        if (prevExpireDate != ((hItem.ExpiryDate != null) ? ((DateTime)hItem.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.EXPIRY (" + hItem.SEQN.ToString() + "): " + prevExpireDate + " -> " + ((hItem.ExpiryDate != null) ? ((DateTime)hItem.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                        }
                    }
                    else if (hItem.SEQN == 0) // inserting new record to db
                    {
                        step = "8";
                        int newSEQN = GetCounter("COTO_OTHER_REG");
                        string sql_InsertHist = "INSERT INTO COTO_OTHER_REG (ID, SEQN, OTHER_PROF_TYPE, OTHER_PROF_TYPE_OTHER, REG_TYPE, OTHER_PROF_REGBODY, OTHER_PROF_REGBODY_OTHER, REG_PROVINCE_STATE, REG_COUNTRY, REG_NUMBER, REG_STATUS, INIT_REG_MONTH, INIT_REG_YEAR, EXPIRY ) ";
                        sql_InsertHist += string.Format(" VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13})", Escape(UserID), newSEQN, Escape(hItem.OtherProfessionType), Escape(hItem.OtherProfessionTypeOther), Escape("OTHER"), Escape(hItem.RegulatoryBody),
                            Escape(hItem.RegulatoryBodyOther), Escape(hItem.Province), Escape(hItem.Country), Escape(hItem.RegNumber), Escape(hItem.RegStatus), Escape(hItem.InitMonth),
                            Escape(hItem.InitYear), (hItem.ExpiryDate != null ? Escape(((DateTime)hItem.ExpiryDate).ToString("yyyy-MM-dd")) : "NULL"));
                        db.ExecuteCommand(sql_InsertHist);

                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_TYPE (" + newSEQN.ToString() + "): " + " -> " + hItem.OtherProfessionType);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_TYPE_OTHER (" + newSEQN.ToString() + "): " + " -> " + hItem.OtherProfessionTypeOther);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_REGBODY (" + newSEQN.ToString() + "): " + " -> " + hItem.RegulatoryBody);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_REGBODY_OTHER (" + newSEQN.ToString() + "): " + " -> " + hItem.RegulatoryBodyOther);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_PROVINCE_STATE (" + newSEQN.ToString() + "): " + " -> " + hItem.Province);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_COUNTRY (" + newSEQN.ToString() + "): " + " -> " + hItem.Country);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_NUMBER (" + newSEQN.ToString() + "): " + " -> " + hItem.RegNumber);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_STATUS (" + newSEQN.ToString() + "): " + " -> " + hItem.RegStatus);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_MONTH (" + newSEQN.ToString() + "): " + " -> " + hItem.InitMonth);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_YEAR (" + newSEQN.ToString() + "): " + " -> " + hItem.InitYear);
                        AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.EXPIRY (" + newSEQN.ToString() + "): " + " -> " + ((hItem.ExpiryDate != null) ? ((DateTime)hItem.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }
                    else if (hItem.SEQN > 0 && string.IsNullOrEmpty(hItem.OtherProfessionType))
                    {
                        step = "9";
                        string sql_DeleteHist = string.Format("DELETE FROM COTO_OTHER_REG WHERE ID = {0} AND SEQN = {1}", Escape(UserID), hItem.SEQN);
                        db.ExecuteCommand(sql_DeleteHist);
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_TYPE (" + hItem.SEQN.ToString() + "): " + prevNameProf + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_TYPE_OTHER (" + hItem.SEQN.ToString() + "): " + prevNameProfOther + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_REGBODY (" + hItem.SEQN.ToString() + "): " + prevRegBody + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_REGBODY_OTHER (" + hItem.SEQN.ToString() + "): " + prevRegBodyOther + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_PROVINCE_STATE (" + hItem.SEQN.ToString() + "): " + prevProvince + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_COUNTRY (" + hItem.SEQN.ToString() + "): " + prevCountry + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_NUMBER (" + hItem.SEQN.ToString() + "): " + prevRegNumber + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_STATUS (" + hItem.SEQN.ToString() + "): " + prevRegStatus + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_MONTH (" + hItem.SEQN.ToString() + "): " + prevInitMonth + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_YEAR (" + hItem.SEQN.ToString() + "): " + prevInitYear + " -> ");
                        AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.EXPIRY (" + hItem.SEQN.ToString() + "): " + prevExpireDate + " -> ");
                    }
                }

            }
            catch (Exception ex)
            {
                message = string.Format("Error Message: {0}; InnerExpection: {1}; StackTrace: {2}; Source: {3}, step: {4} ", ex.Message, ex.InnerException, ex.StackTrace, ex.Source, step);
            }

            return message;
        }

        // new methods

        public List<ProfessionalHistoryPracticeDetailNew> GetAppProfessionalHistoryRecords(string User_Id)
        {
            List<ProfessionalHistoryPracticeDetailNew> history = new List<ProfessionalHistoryPracticeDetailNew>();

            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT cor.*, gt1.DESCRIPTION AS OTHER_PROF_TYPE_DESCR, " +
                " gt2.DESCRIPTION AS OTHER_PROF_REGBODY_DESCR, gt3.DESCRIPTION AS OT_REG_BODY_DESCR, " +
                " gt4.DESCRIPTION AS REG_COUNTRY_DESCR, gt5.DESCRIPTION AS REG_PROVINCE_STATE_DESCR " +
                " FROM COTO_OTHER_REG AS cor " +
                " LEFT OUTER JOIN GEN_TABLES as gt1 ON gt1.CODE = cor.OTHER_PROF_TYPE AND gt1.TABLE_NAME = 'REG_OTHER_PROF' " +
                " LEFT OUTER JOIN GEN_TABLES as gt2 ON gt2.CODE = cor.OTHER_PROF_REGBODY AND gt2.TABLE_NAME LIKE 'REG_OTHER_PROF_%' " +
                " LEFT OUTER JOIN GEN_TABLES as gt3 ON gt3.CODE = cor.OT_REG_BODY AND gt3.TABLE_NAME = 'REG_BODIES' " +
                " LEFT OUTER JOIN GEN_TABLES as gt4 ON gt4.CODE = cor.REG_COUNTRY AND gt4.TABLE_NAME = 'COUNTRY' " +
                " LEFT OUTER JOIN GEN_TABLES as gt5 ON gt5.CODE = cor.REG_PROVINCE_STATE AND gt5.TABLE_NAME = 'PROVINCE_STATE' " +
                " WHERE cor.ID = " + Escape(User_Id) + " ORDER BY cor.SEQN";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                //history = new List<ProfessionalHistoryPracticeDetailNew>();
                foreach (DataRow row in table.Rows)
                {
                    var historyItem = new ProfessionalHistoryPracticeDetailNew();
                    historyItem.SEQN = (int)row["SEQN"];
                    historyItem.RegType = (string)row["REG_TYPE"];

                    historyItem.RegulatoryBody = (string)row["OT_REG_BODY"];
                    if (row["OT_REG_BODY_DESCR"] != DBNull.Value) historyItem.RegulatoryBodyDescription = (string)row["OT_REG_BODY_DESCR"];
                    historyItem.RegulatoryBodyOther = (string)row["OT_REG_BODY_OTHER"];

                    historyItem.OtherProfessionType = (string)row["OTHER_PROF_TYPE"];
                    if (row["OTHER_PROF_TYPE_DESCR"]!= DBNull.Value) historyItem.OtherProfessionTypeDescription = (string)row["OTHER_PROF_TYPE_DESCR"];
                    historyItem.OtherProfessionTypeOther = (string)row["OTHER_PROF_TYPE_OTHER"];
                    historyItem.OtherRegulatoryBody = (string)row["OTHER_PROF_REGBODY"];
                    if (row["OTHER_PROF_REGBODY_DESCR"] != DBNull.Value) historyItem.OtherRegulatoryBodyDescription = (string)row["OTHER_PROF_REGBODY_DESCR"];
                    historyItem.OtherRegulatoryBodyOther = (string)row["OTHER_PROF_REGBODY_OTHER"];

                    historyItem.Province = (string)row["REG_PROVINCE_STATE"];
                    if (row["REG_PROVINCE_STATE_DESCR"] != DBNull.Value) historyItem.ProvinceDescription = (string)row["REG_PROVINCE_STATE_DESCR"];
                    historyItem.Country = (string)row["REG_COUNTRY"];
                    if (row["REG_COUNTRY_DESCR"] !=DBNull.Value) historyItem.CountryDescription = (string)row["REG_COUNTRY_DESCR"];
                    historyItem.RegNumber = (string)row["REG_NUMBER"];
                    historyItem.RegStatus = (string)row["REG_STATUS"];
                    historyItem.InitMonth = (string)row["INIT_REG_MONTH"];
                    historyItem.InitYear = (string)row["INIT_REG_YEAR"];
                    if (row["EXPIRY"] != DBNull.Value)
                    {
                        historyItem.ExpiryDate = (DateTime)row["EXPIRY"];
                    }
                    else
                    {
                        historyItem.ExpiryDate = DateTime.MinValue;
                    }
                    history.Add(historyItem);
                }
            }

            return history;
        }

        public ProfessionalHistoryPracticeDetailNew GetAppProfessionalHistoryRecord(int recSeqn)
        {
            ProfessionalHistoryPracticeDetailNew retValue = null;
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT cor.* FROM COTO_OTHER_REG as cor" +
                " WHERE cor.SEQN = " + recSeqn.ToString();
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];
                retValue = new ProfessionalHistoryPracticeDetailNew();
                retValue.SEQN = (int)row["SEQN"];
                retValue.RegType = (string)row["REG_TYPE"];
                retValue.OtherProfessionType = (string)row["OTHER_PROF_TYPE"];
                retValue.OtherProfessionTypeOther = (string)row["OTHER_PROF_TYPE_OTHER"];
                retValue.RegulatoryBody = (string)row["OT_REG_BODY"];
                retValue.RegulatoryBodyOther = (string)row["OT_REG_BODY_OTHER"];
                retValue.OtherRegulatoryBody = (string)row["OTHER_PROF_REGBODY"];
                retValue.OtherRegulatoryBodyOther = (string)row["OTHER_PROF_REGBODY_OTHER"];
                retValue.Province = (string)row["REG_PROVINCE_STATE"];
                retValue.Country = (string)row["REG_COUNTRY"];
                retValue.RegNumber = (string)row["REG_NUMBER"];
                retValue.RegStatus = (string)row["REG_STATUS"];
                retValue.InitMonth = (string)row["INIT_REG_MONTH"];
                retValue.InitYear = (string)row["INIT_REG_YEAR"];
                if (row["EXPIRY"] != DBNull.Value)
                {
                    retValue.ExpiryDate = (DateTime)row["EXPIRY"];
                }
                else
                {
                    retValue.ExpiryDate = DateTime.MinValue;
                }
            }
            return retValue;
        }

        public string UpdateProfessionalHistoryDetailLogged(string UserID, ProfessionalHistoryPracticeDetailNew history)
        {
            string message = string.Empty;
            try
            {
                //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
                string dbconn = WebConfigItems.GetConnectionString();
                var db = new clsDB();

                string prevRegType = string.Empty;
                string prevNameProf = string.Empty;
                string prevNameProfOther = string.Empty;
                string prevRegBody = string.Empty;
                string prevRegBodyOther = string.Empty;
                string prevOtherRegBody = string.Empty;
                string prevOtherRegBodyOther = string.Empty;
                string prevProvince = string.Empty;
                string prevCountry = string.Empty;
                string prevRegNumber = string.Empty;
                string prevRegStatus = string.Empty;
                string prevInitMonth = string.Empty;
                string prevInitYear = string.Empty;
                string prevExpireDate = string.Empty;

                if (history.SEQN > 0)
                {
                    string sql_GetCurrentHist = "SELECT * FROM COTO_OTHER_REG WHERE ID =" + Escape(UserID) + " AND SEQN = " + history.SEQN.ToString();

                    var table = db.GetData(sql_GetCurrentHist);
                    if (table != null && table.Rows.Count == 1)
                    {
                        DataRow row = table.Rows[0];
                        prevRegType = (string)row["REG_TYPE"];
                        prevNameProf = (string)row["OTHER_PROF_TYPE"];
                        prevNameProfOther = (string)row["OTHER_PROF_TYPE_OTHER"];
                        prevRegBody = (string)row["OT_REG_BODY"];
                        prevRegBodyOther = (string)row["OT_REG_BODY_OTHER"];
                        prevOtherRegBody = (string)row["OTHER_PROF_REGBODY"];
                        prevOtherRegBodyOther = (string)row["OTHER_PROF_REGBODY_OTHER"];
                        prevProvince = (string)row["REG_PROVINCE_STATE"];
                        prevCountry = (string)row["REG_COUNTRY"];
                        prevRegNumber = (string)row["REG_NUMBER"];
                        prevRegStatus = (string)row["REG_STATUS"];
                        prevInitMonth = (string)row["INIT_REG_MONTH"];
                        prevInitYear = (string)row["INIT_REG_YEAR"];
                        if (row["EXPIRY"] != DBNull.Value)
                        {
                            prevExpireDate = ((DateTime)row["EXPIRY"]).ToString("MM/dd/yyyy");
                        }
                    }
                }
                // updating existing record
                if (history.SEQN > 0 && !string.IsNullOrEmpty(history.RegType))
                {
                    string sql_UpdateHist = "UPDATE COTO_OTHER_REG SET ";
                    sql_UpdateHist += " OTHER_PROF_TYPE = " + Escape(history.OtherProfessionType) + ", " +
                            " OTHER_PROF_TYPE_OTHER = " + Escape(history.OtherProfessionTypeOther) + ", " +
                            " OTHER_PROF_REGBODY = " + Escape(history.OtherRegulatoryBody) + ", " +
                            " OTHER_PROF_REGBODY_OTHER = " + Escape(history.OtherRegulatoryBodyOther) + ", " +
                            " OT_REG_BODY = " + Escape(history.RegulatoryBody) + ", " +
                            " OT_REG_BODY_OTHER = " + Escape(history.RegulatoryBodyOther) + ", " +
                            " REG_PROVINCE_STATE = " + Escape(history.Province) + ", " +
                            " REG_COUNTRY = " + Escape(history.Country) + ", " +
                            " REG_NUMBER = " + Escape(history.RegNumber) + ", " +
                            " REG_STATUS = " + Escape(history.RegStatus) + ", " +
                            " INIT_REG_MONTH = " + Escape(history.InitMonth) + ", " +
                            " INIT_REG_YEAR = " + Escape(history.InitYear) + ", ";
                    if (history.ExpiryDate != DateTime.MinValue)
                    {
                        DateTime expdate = (DateTime)history.ExpiryDate;
                        sql_UpdateHist += "EXPIRY = " + Escape(expdate.ToString("yyyy-MM-dd"));
                    }
                    else
                    {
                        sql_UpdateHist += "EXPIRY = NULL ";
                    }
                    sql_UpdateHist += " WHERE ID = " + Escape(UserID) + " AND SEQN = " + history.SEQN.ToString();
                    db.ExecuteCommand(sql_UpdateHist);

                    if (prevNameProf != history.OtherProfessionType)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_TYPE (" + history.SEQN.ToString() + "): " + prevNameProf + " -> " + history.OtherProfessionType);
                    }
                    if (prevNameProfOther != history.OtherProfessionTypeOther)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_TYPE_OTHER (" + history.SEQN.ToString() + "): " + prevNameProfOther + " -> " + history.OtherProfessionTypeOther);
                    }
                    if (prevOtherRegBody != history.OtherRegulatoryBody)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_REGBODY (" + history.SEQN.ToString() + "): " + prevOtherRegBody + " -> " + history.OtherRegulatoryBody);
                    }
                    if (!string.Equals(prevOtherRegBodyOther, history.OtherRegulatoryBodyOther))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_REGBODY_OTHER (" + history.SEQN.ToString() + "): " + prevOtherRegBodyOther + " -> " + history.OtherRegulatoryBodyOther);
                    }
                    if (prevRegBody != history.RegulatoryBody)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OT_REG_BODY (" + history.SEQN.ToString() + "): " + prevRegBody + " -> " + history.RegulatoryBody);
                    }
                    if (!string.Equals(prevRegBodyOther, history.RegulatoryBodyOther))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OT_REG_BODY_OTHER (" + history.SEQN.ToString() + "): " + prevRegBodyOther + " -> " + history.RegulatoryBodyOther);
                    }
                    if (prevProvince != history.Province)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_PROVINCE_STATE (" + history.SEQN.ToString() + "): " + prevProvince + " -> " + history.Province);
                    }
                    if (prevCountry != history.Country)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_COUNTRY (" + history.SEQN.ToString() + "): " + prevCountry + " -> " + history.Country);
                    }
                    if (prevRegNumber != history.RegNumber)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_NUMBER (" + history.SEQN.ToString() + "): " + prevRegNumber + " -> " + history.RegNumber);
                    }
                    if (prevRegStatus != history.RegStatus)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_STATUS (" + history.SEQN.ToString() + "): " + prevRegStatus + " -> " + history.RegStatus);
                    }
                    if (prevInitMonth != history.InitMonth)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_MONTH (" + history.SEQN.ToString() + "): " + prevInitMonth + " -> " + history.InitMonth);
                    }
                    if (prevInitYear != history.InitYear)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_YEAR (" + history.SEQN.ToString() + "): " + prevInitYear + " -> " + history.InitYear);
                    }
                    if (prevExpireDate != ((history.ExpiryDate != DateTime.MinValue) ? ((DateTime)history.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty))
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.EXPIRY (" + history.SEQN.ToString() + "): " + prevExpireDate + " -> " + ((history.ExpiryDate != DateTime.MinValue) ? ((DateTime)history.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                    }
                }
                else if (history.SEQN == 0) // inserting new record to db
                {
                    int newSEQN = GetCounter("COTO_OTHER_REG");
                    string sql_InsertHist = "INSERT INTO COTO_OTHER_REG (ID, SEQN, REG_TYPE, OTHER_PROF_TYPE, OTHER_PROF_TYPE_OTHER, OTHER_PROF_REGBODY, OTHER_PROF_REGBODY_OTHER, OT_REG_BODY, OT_REG_BODY_OTHER, REG_PROVINCE_STATE, REG_COUNTRY, REG_NUMBER, REG_STATUS, INIT_REG_MONTH, INIT_REG_YEAR, EXPIRY ) ";
                    sql_InsertHist += string.Format(" VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15})", Escape(UserID), newSEQN, Escape(history.RegType),
                        Escape(history.OtherProfessionType), Escape(history.OtherProfessionTypeOther), Escape(history.OtherRegulatoryBody), Escape(history.OtherRegulatoryBodyOther), Escape(history.RegulatoryBody),
                        Escape(history.RegulatoryBodyOther), Escape(history.Province), Escape(history.Country), Escape(history.RegNumber), Escape(history.RegStatus), Escape(history.InitMonth),
                        Escape(history.InitYear), (history.ExpiryDate != DateTime.MinValue ? Escape(((DateTime)history.ExpiryDate).ToString("yyyy-MM-dd")) : "NULL"));
                    db.ExecuteCommand(sql_InsertHist);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_TYPE (" + newSEQN.ToString() + "): " + " -> " + history.RegType);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_TYPE (" + newSEQN.ToString() + "): " + " -> " + history.OtherProfessionType);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_TYPE_OTHER (" + newSEQN.ToString() + "): " + " -> " + history.OtherProfessionTypeOther);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_REGBODY (" + newSEQN.ToString() + "): " + " -> " + history.OtherRegulatoryBody);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_REGBODY_OTHER (" + newSEQN.ToString() + "): " + " -> " + history.OtherRegulatoryBodyOther);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OT_REG_BODY (" + newSEQN.ToString() + "): " + prevRegBody + " -> " + history.RegulatoryBody);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OT_REG_BODY_OTHER (" + newSEQN.ToString() + "): " + prevRegBodyOther + " -> " + history.RegulatoryBodyOther);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_PROVINCE_STATE (" + newSEQN.ToString() + "): " + prevProvince + " -> " + history.Province);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_COUNTRY (" + newSEQN.ToString() + "): " + prevCountry + " -> " + history.Country);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_NUMBER (" + newSEQN.ToString() + "): " + prevRegNumber + " -> " + history.RegNumber);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_STATUS (" + newSEQN.ToString() + "): " + prevRegStatus + " -> " + history.RegStatus);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_MONTH (" + newSEQN.ToString() + "): " + prevInitMonth + " -> " + history.InitMonth);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_YEAR (" + newSEQN.ToString() + "): " + prevInitYear + " -> " + history.InitYear);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_OTHER_REG.EXPIRY (" + newSEQN.ToString() + "): " + prevExpireDate + " -> " + ((history.ExpiryDate != DateTime.MinValue) ? ((DateTime)history.ExpiryDate).ToString("MM/dd/yyyy") : string.Empty));
                }
                else if (history.SEQN > 0 && string.IsNullOrEmpty(history.RegType))
                {
                    string sql_DeleteHist = string.Format("DELETE FROM COTO_OTHER_REG WHERE ID = {0} AND SEQN = {1}", Escape(UserID), history.SEQN);
                    db.ExecuteCommand(sql_DeleteHist);
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_TYPE (" + history.SEQN.ToString() + "): " + prevRegType + " -> ");
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_TYPE (" + history.SEQN.ToString() + "): " + prevNameProf + " -> ");
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_TYPE_OTHER (" + history.SEQN.ToString() + "): " + prevNameProfOther + " -> ");
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_REGBODY (" + history.SEQN.ToString() + "): " + prevOtherRegBody + " -> ");
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OTHER_PROF_REGBODY_OTHER (" + history.SEQN.ToString() + "): " + prevOtherRegBodyOther + " -> ");
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OT_REG_BODY (" + history.SEQN.ToString() + "): " + prevRegBody + " -> ");
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.OT_REG_BODY_OTHER (" + history.SEQN.ToString() + "): " + prevRegBodyOther + " -> ");
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_PROVINCE_STATE (" + history.SEQN.ToString() + "): " + prevProvince + " -> ");
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_COUNTRY (" + history.SEQN.ToString() + "): " + prevCountry + " -> ");
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_NUMBER (" + history.SEQN.ToString() + "): " + prevRegNumber + " -> ");
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.REG_STATUS (" + history.SEQN.ToString() + "): " + prevRegStatus + " -> ");
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_MONTH (" + history.SEQN.ToString() + "): " + prevInitMonth + " -> ");
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.INIT_REG_YEAR (" + history.SEQN.ToString() + "): " + prevInitYear + " -> ");
                    AddLogEntry(DateTime.Now, "DELETE", "CHANGE", UserID, UserID, "COTO_OTHER_REG.EXPIRY (" + history.SEQN.ToString() + "): " + prevExpireDate + " -> ");
                }

            }
            catch (Exception ex)
            {
                message = string.Format("Error Message: {0}; InnerExpection: {1}; StackTrace: {2}; Source: {3}", ex.Message, ex.InnerException, ex.StackTrace, ex.Source);
            }

            return message;
        }

        // step 7

        public User GetApplicationUserPracticeHistoryInfo(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM COTO_REGDATA " +
                        " WHERE ID = " + Escape(User_Id);

            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.PracticeHistory = new PracticeHistory
                {
                    FirstYearPractice = (string)row["First_Year_Practice_Prof"],
                    FirstCountryPractice = (string)row["Country_First_Practice_Prof"],
                    FirstProvinceStatePractice = (string)row["Prov_State_First_Practice"],
                    FirstYearCanadianPractice = (string)row["First_Year_CDN_Pract_Prof"],
                    FirstProvincePractice = (string)row["First_Cdn_Prov_Practice"],

                    LastYearOutsideOntarioPractice = (string)row["MOST_RECENT_PREV_YEAR"],
                    LastCountryPractice = (string)row["Most_Recent_Prev_Country"],
                    LastProvinceStatePractice = (string)row["Most_Recent_Prev_Prov"],
                    //LastProvincePractice = (string)row["PREVIOUS_PROV"],
                    //PracticedOutsideOntario = (string)row["WEEKLY_HRS_TEACH"],
                };

            }

            return returnValue;
        }

        public void UpdateApplicationUserPracticeHistoryLogged(string UserID, User user)
        {
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql_GetProfile = " SELECT * FROM COTO_REGDATA WHERE  ID = " + Escape(UserID);
            //db.ExecuteCommand(sql_GetHomeAddress);

            string prevFirstYearProf = string.Empty;
            string prevFirstCountryProf = string.Empty;
            string prevFirstProvProf = string.Empty;

            string prevFirstYearCanadaProf = string.Empty;
            string prevFirstCanadaProvProf = string.Empty;

            string prevRecentCountry = string.Empty;
            string prevRecentProvince = string.Empty;
            string prevRecentYear = string.Empty;

            var table = db.GetData(sql_GetProfile);
            if (table != null && table.Rows.Count == 1)
            {
                DataRow row = table.Rows[0];

                prevFirstYearProf = (string)row["First_Year_Practice_Prof"];
                prevFirstCountryProf = (string)row["Country_First_Practice_Prof"];
                prevFirstProvProf = (string)row["Prov_State_First_Practice"];
                prevFirstYearCanadaProf = (string)row["First_Year_CDN_Pract_Prof"];
                prevFirstCanadaProvProf = (string)row["First_Cdn_Prov_Practice"];

                prevRecentCountry = (string)row["Most_Recent_Prev_Country"];
                prevRecentProvince = (string)row["Most_Recent_Prev_Prov"];
                prevRecentYear = (string)row["MOST_RECENT_PREV_YEAR"];
            }

            var profile = user.PracticeHistory;
            //try
            //{
                if (profile != null)
                {
                    string sql_UpdateProfile = "UPDATE COTO_REGDATA SET ";
                    sql_UpdateProfile += " First_Year_Practice_Prof = " + Escape(profile.FirstYearPractice);
                    sql_UpdateProfile += ", Country_First_Practice_Prof = " + Escape(profile.FirstCountryPractice);
                    sql_UpdateProfile += ", Prov_State_First_Practice = " + Escape(profile.FirstProvinceStatePractice);
                    sql_UpdateProfile += ", First_Cdn_Prov_Practice = " + Escape(profile.FirstProvincePractice);
                    sql_UpdateProfile += ", First_Year_CDN_Pract_Prof = " + Escape(profile.FirstYearCanadianPractice);
                    
                    sql_UpdateProfile += ", Most_Recent_Prev_Country = " + Escape(profile.LastCountryPractice);
                    //sql_UpdateProfile += ", PREVIOUS_PROV = " + Escape(profile.LastProvincePractice);
                    sql_UpdateProfile += ", Most_Recent_Prev_Prov = " + Escape(profile.LastProvinceStatePractice);
                    sql_UpdateProfile += ", MOST_RECENT_PREV_YEAR = " + Escape(profile.LastYearOutsideOntarioPractice);
                    sql_UpdateProfile += " WHERE ID = " + Escape(user.Id);

                    db.ExecuteCommand(sql_UpdateProfile);

                    if (prevRecentCountry != profile.LastCountryPractice)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.Most_Recent_Prev_Country: " + prevRecentCountry + " -> " + profile.LastCountryPractice);
                    }
                    if (prevRecentProvince != profile.LastProvinceStatePractice)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.Most_Recent_Prev_Prov: " + prevRecentProvince + " -> " + profile.LastProvinceStatePractice);
                    }
                    if (prevRecentYear != profile.LastYearOutsideOntarioPractice)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.MOST_RECENT_PREV_YEAR: " + prevRecentYear + " -> " + profile.LastYearOutsideOntarioPractice);
                    }
                    //
                    if (prevFirstYearProf != profile.FirstYearPractice)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.First_Year_Practice_Prof: " + prevFirstYearProf + " -> " + profile.FirstYearPractice);
                    }

                    if (prevFirstCountryProf != profile.FirstCountryPractice)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.Country_First_Practice_Prof: " + prevFirstCountryProf + " -> " + profile.FirstCountryPractice);
                    }

                    if (prevFirstProvProf != profile.FirstProvinceStatePractice)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.Prov_State_First_Practice: " + prevFirstProvProf + " -> " + profile.FirstProvinceStatePractice);
                    }

                    if (prevFirstYearCanadaProf != profile.FirstProvincePractice)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.First_Year_CDN_Pract_Prof: " + prevFirstYearCanadaProf + " -> " + profile.FirstProvincePractice);
                    }

                    if (prevFirstCanadaProvProf != profile.FirstYearCanadianPractice)
                    {
                        AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.First_Cdn_Prov_Practice: " + prevFirstCanadaProvProf + " -> " + profile.FirstYearCanadianPractice);
                    }


                }
            //}
            //catch
            //{
                //Message = "Exception.Message = " + ex.Message + ", Exception.InnerException = " + ex.InnerException;
            //}
        }

        // step 8

        public User GetApplicationUserConductInfo(string User_Id, int CurrentYear)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM CONDUCT_HISTORY " +
                " WHERE ID = '" + User_Id + "' AND RENEWAL_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.ConductInfo = new COTO_RegOnly.Classes.Conduct();
                returnValue.ConductInfo.Refused_OT_Other = (string)row["REFUSED_OT_OTHER"];
                returnValue.ConductInfo.Refused_OT_Other_Details = (string)row["REFUSED_OT_OTHER_DETAILS"];

                returnValue.ConductInfo.Finding_Other_Juris = (string)row["FINDING_OTHER_JURIS"];
                returnValue.ConductInfo.Finding_Other_Juris_Details = (string)row["FINDING_OTHER_JURIS_DETAILS"];

                returnValue.ConductInfo.Finding_Other_Prof = (string)row["FINDING_OTHER_PROF"];
                returnValue.ConductInfo.Finding_Other_Prof_Details = (string)row["FINDING_OTHER_PROF_DETAILS"];

                returnValue.ConductInfo.Guilty_Rel_OT = (string)row["GUILTY_REL_OT"];
                returnValue.ConductInfo.Guilty_Rel_OT_Details = (string)row["GUILTY_REL_OT_DETAILS"];

                returnValue.ConductInfo.Criminal_Guilt = (string)row["CRIMINAL_GUILT"];
                returnValue.ConductInfo.Criminal_Guilt_Details = (string)row["CRIMINAL_GUILT_DETAILS"];

                returnValue.ConductInfo.Lack_Know_Skill_Judg = (string)row["LACK_KNOW_SKILL_JUDG"];
                returnValue.ConductInfo.Lack_Know_Skill_Judg_Details = (string)row["LACK_KNOW_SKILL_JUDG_DETAILS"];

                returnValue.ConductInfo.Finding_Neg_Malpractice = (string)row["FINDING_NEG_MALPRACTICE"];
                returnValue.ConductInfo.Finding_Neg_Malpractice_Details = (string)row["FINDING_NEG_MALPRACTICE_DETAIL"];

            }
            return returnValue;
        }

        public void UpdateApplicationUserConductInfoLogged(string UserID, User user, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();



            string sql = "SELECT * FROM CONDUCT_HISTORY  " +
                " WHERE ID = " + Escape(user.Id) + " AND RENEWAL_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];
                string prevRefusedOther = (string)row["REFUSED_OT_OTHER"];
                string prevRefusedOtherDetails = (string)row["REFUSED_OT_OTHER_DETAILS"];
                string prevFindingJuris = (string)row["FINDING_OTHER_JURIS"];
                string prevFindingJurisDetails = (string)row["FINDING_OTHER_JURIS_DETAILS"];
                string prevFindingProf = (string)row["FINDING_OTHER_PROF"];
                string prevFindingProfDetails = (string)row["FINDING_OTHER_PROF_DETAILS"];
                string prevGuiltyRel = (string)row["GUILTY_REL_OT"];
                string prevGuiltyRelDetails = (string)row["GUILTY_REL_OT_DETAILS"];
                string prevCriminalGuilty = (string)row["CRIMINAL_GUILT"];
                string prevCriminalGuiltyDetails = (string)row["CRIMINAL_GUILT_DETAILS"];
                string prevLackKnowSkill = (string)row["LACK_KNOW_SKILL_JUDG"];
                string prevLackKnowSkillDetails = (string)row["LACK_KNOW_SKILL_JUDG_DETAILS"];
                string FindingMalpractice = (string)row["FINDING_NEG_MALPRACTICE"];
                string FindingMalpracticeDetails = (string)row["FINDING_NEG_MALPRACTICE_DETAIL"];

                string sql_UpdateUserConduct = "UPDATE CONDUCT_HISTORY SET " +
                " REFUSED_OT_OTHER = '" + user.ConductInfo.Refused_OT_Other + "', " +
                " REFUSED_OT_OTHER_DETAILS = '" + user.ConductInfo.Refused_OT_Other_Details + "', " +

                " FINDING_OTHER_JURIS = '" + user.ConductInfo.Finding_Other_Juris + "', " +
                " FINDING_OTHER_JURIS_DETAILS = '" + user.ConductInfo.Finding_Other_Juris_Details + "', " +

                " FINDING_OTHER_PROF = '" + user.ConductInfo.Finding_Other_Prof + "', " +
                " FINDING_OTHER_PROF_DETAILS = '" + user.ConductInfo.Finding_Other_Prof_Details + "', " +

                " GUILTY_REL_OT = '" + user.ConductInfo.Guilty_Rel_OT + "', " +
                " GUILTY_REL_OT_DETAILS = '" + user.ConductInfo.Guilty_Rel_OT_Details + "', " +

                " CRIMINAL_GUILT = '" + user.ConductInfo.Criminal_Guilt + "', " +
                " CRIMINAL_GUILT_DETAILS = '" + user.ConductInfo.Criminal_Guilt_Details + "', " +

                " LACK_KNOW_SKILL_JUDG = '" + user.ConductInfo.Lack_Know_Skill_Judg + "', " +
                " LACK_KNOW_SKILL_JUDG_DETAILS = '" + user.ConductInfo.Lack_Know_Skill_Judg_Details + "', " +

                " FINDING_NEG_MALPRACTICE = '" + user.ConductInfo.Finding_Neg_Malpractice + "', " +
                " FINDING_NEG_MALPRACTICE_DETAIL = '" + user.ConductInfo.Finding_Neg_Malpractice_Details + "' " +

                " WHERE ID = " + Escape(user.Id) + " AND RENEWAL_YEAR = '" + CurrentYear.ToString() + "'";
                db.ExecuteCommand(sql_UpdateUserConduct);

                if (prevRefusedOther != user.ConductInfo.Refused_OT_Other)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.REFUSED_OT_OTHER (" + CurrentYear.ToString() + "): " + prevRefusedOther + " -> " + user.ConductInfo.Refused_OT_Other);
                }
                if (prevRefusedOtherDetails != user.ConductInfo.Refused_OT_Other_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.REFUSED_OT_OTHER_DETAILS (" + CurrentYear.ToString() + "): " + prevRefusedOtherDetails + " -> " + user.ConductInfo.Refused_OT_Other_Details);
                }
                if (prevFindingJuris != user.ConductInfo.Finding_Other_Juris)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_JURIS (" + CurrentYear.ToString() + "): " + prevFindingJuris + " -> " + user.ConductInfo.Finding_Other_Juris);
                }
                if (prevFindingJurisDetails != user.ConductInfo.Finding_Other_Juris_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_JURIS_DETAILS (" + CurrentYear.ToString() + "): " + prevFindingJurisDetails + " -> " + user.ConductInfo.Finding_Other_Juris_Details);
                }
                if (prevFindingProf != user.ConductInfo.Finding_Other_Prof)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_PROF (" + CurrentYear.ToString() + "): " + prevFindingProf + " -> " + user.ConductInfo.Finding_Other_Prof);
                }
                if (prevFindingProfDetails != user.ConductInfo.Finding_Other_Prof_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_PROF_DETAILS (" + CurrentYear.ToString() + "): " + prevFindingProfDetails + " -> " + user.ConductInfo.Finding_Other_Prof_Details);
                }
                if (prevGuiltyRel != user.ConductInfo.Guilty_Rel_OT)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.GUILTY_REL_OT (" + CurrentYear.ToString() + "): " + prevGuiltyRel + " -> " + user.ConductInfo.Guilty_Rel_OT);
                }
                if (prevGuiltyRelDetails != user.ConductInfo.Guilty_Rel_OT_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.GUILTY_REL_OT_DETAILS (" + CurrentYear.ToString() + "): " + prevGuiltyRelDetails + " -> " + user.ConductInfo.Guilty_Rel_OT_Details);
                }
                if (prevCriminalGuilty != user.ConductInfo.Criminal_Guilt)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.CRIMINAL_GUILT (" + CurrentYear.ToString() + "): " + prevCriminalGuilty + " -> " + user.ConductInfo.Criminal_Guilt);
                }
                if (prevCriminalGuiltyDetails != user.ConductInfo.Criminal_Guilt_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.CRIMINAL_GUILT_DETAILS (" + CurrentYear.ToString() + "): " + prevCriminalGuiltyDetails + " -> " + user.ConductInfo.Criminal_Guilt_Details);
                }
                if (prevLackKnowSkill != user.ConductInfo.Lack_Know_Skill_Judg)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.LACK_KNOW_SKILL_JUDG (" + CurrentYear.ToString() + "): " + prevLackKnowSkill + " -> " + user.ConductInfo.Lack_Know_Skill_Judg);
                }
                if (prevLackKnowSkillDetails != user.ConductInfo.Lack_Know_Skill_Judg_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.LACK_KNOW_SKILL_JUDG_DETAILS (" + CurrentYear.ToString() + "): " + prevLackKnowSkillDetails + " -> " + user.ConductInfo.Lack_Know_Skill_Judg_Details);
                }
                if (FindingMalpractice != user.ConductInfo.Finding_Neg_Malpractice)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_NEG_MALPRACTICE (" + CurrentYear.ToString() + "): " + FindingMalpractice + " -> " + user.ConductInfo.Finding_Neg_Malpractice);
                }
                if (FindingMalpracticeDetails != user.ConductInfo.Finding_Neg_Malpractice_Details)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_NEG_MALPRACTICE_DETAIL (" + CurrentYear.ToString() + "): " + FindingMalpracticeDetails + " -> " + user.ConductInfo.Finding_Neg_Malpractice_Details);
                }
            }
            else
            {
                int newSEQN = GetCounter("CONDUCT_HISTORY");
                string sql_InsertUserConduct = "INSERT INTO CONDUCT_HISTORY (ID, SEQN, RENEWAL_YEAR, REFUSED_OT_OTHER, REFUSED_OT_OTHER_DETAILS, " +
                " FINDING_OTHER_JURIS, FINDING_OTHER_JURIS_DETAILS, FINDING_OTHER_PROF, FINDING_OTHER_PROF_DETAILS, GUILTY_REL_OT, " +
                " GUILTY_REL_OT_DETAILS, CRIMINAL_GUILT, CRIMINAL_GUILT_DETAILS, LACK_KNOW_SKILL_JUDG, LACK_KNOW_SKILL_JUDG_DETAILS, " +
                " FINDING_NEG_MALPRACTICE, FINDING_NEG_MALPRACTICE_DETAIL) VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", '" + CurrentYear.ToString() + "', '" +
                user.ConductInfo.Refused_OT_Other + "', '" + user.ConductInfo.Refused_OT_Other_Details + "', '" +
                user.ConductInfo.Finding_Other_Juris + "', '" + user.ConductInfo.Finding_Other_Juris_Details + "', '" +
                user.ConductInfo.Finding_Other_Prof + "', '" + user.ConductInfo.Finding_Other_Prof_Details + "', '" +
                user.ConductInfo.Guilty_Rel_OT + "', '" + user.ConductInfo.Guilty_Rel_OT_Details + "', '" +
                user.ConductInfo.Criminal_Guilt + "', '" + user.ConductInfo.Criminal_Guilt_Details + "', '" +
                user.ConductInfo.Lack_Know_Skill_Judg + "', '" + user.ConductInfo.Lack_Know_Skill_Judg_Details + "', '" +
                user.ConductInfo.Finding_Neg_Malpractice + "', '" + user.ConductInfo.Finding_Neg_Malpractice_Details + "') ";
                db.ExecuteCommand(sql_InsertUserConduct);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.REFUSED_OT_OTHER (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Refused_OT_Other);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.REFUSED_OT_OTHER_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Refused_OT_Other_Details);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_JURIS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Finding_Other_Juris);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_JURIS_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Finding_Other_Juris_Details);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_PROF (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Finding_Other_Prof);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_OTHER_PROF_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Finding_Other_Prof_Details);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.GUILTY_REL_OT (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Guilty_Rel_OT);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.GUILTY_REL_OT_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Guilty_Rel_OT_Details);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.CRIMINAL_GUILT (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Criminal_Guilt);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.CRIMINAL_GUILT_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Criminal_Guilt_Details);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.LACK_KNOW_SKILL_JUDG (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Lack_Know_Skill_Judg);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.LACK_KNOW_SKILL_JUDG_DETAILS (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Lack_Know_Skill_Judg_Details);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_NEG_MALPRACTICE (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Finding_Neg_Malpractice);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "CONDUCT_HISTORY.FINDING_NEG_MALPRACTICE_DETAIL (" + CurrentYear.ToString() + "): -> " + user.ConductInfo.Finding_Neg_Malpractice_Details);
            }
        }

        // step 9

        public User GetApplicationUserInsuranceInfo(string User_Id, int CurrentYear)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM INSURANCE " +
                " WHERE ID = '" + User_Id + "' AND INS_YEAR = '" + CurrentYear.ToString() + "'";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.InsuranceInfo = new COTO_RegOnly.Classes.Insurance();
                returnValue.InsuranceInfo.PlanHeld = (string)row["INS_PLAN_HELD"];
                returnValue.InsuranceInfo.OtherInsuranceProvider = (string)row["INS_INSURE_OTHER"];

                if (row["INS_EXPIRY_DATE"] != DBNull.Value)
                {
                    returnValue.InsuranceInfo.ExpiryDate = (DateTime)row["INS_EXPIRY_DATE"];
                }
                else
                {
                    returnValue.InsuranceInfo.ExpiryDate = DateTime.MinValue;
                }

                returnValue.InsuranceInfo.CertificateNumber = (string)row["INS_CERT_NUMBER"];


            }
            return returnValue;
        }

        public User GetApplicationUserInsuranceInfoNew(string User_Id)
        {
            User returnValue = null;

            var application = GetUserApplicationConuctSubmittedDate(User_Id);
            if (application == null || application.ConductSubmitted == DateTime.MinValue)
            {
                return returnValue;
            }

            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT TOP(1) ci.*, gt.Description FROM COTO_INSURANCE AS ci " +
                " LEFT OUTER JOIN Gen_Tables AS gt ON gt.CODE = ci.PLAN_WITH AND gt.Table_Name = 'INSURANCE' " +
                " WHERE ci.ID = " + Escape(User_Id) + " ORDER BY ci.SEQN DESC";
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];

                var date_reported = DateTime.MinValue;
                if (row["DATE_REPORTED"] != DBNull.Value) date_reported = (DateTime)row["DATE_REPORTED"];
                if (date_reported != DateTime.MinValue && date_reported.Date == application.ConductSubmitted.Date)
                {
                    returnValue = new User();
                    returnValue.Id = User_Id;
                    returnValue.InsuranceInfo = new COTO_RegOnly.Classes.Insurance();
                    returnValue.InsuranceInfo.PlanHeld = (string)row["PLAN_WITH"];
                    if (row["Description"] != DBNull.Value)
                    {
                        returnValue.InsuranceInfo.PlanHeldName = (string)row["Description"];
                    }
                    else
                    {
                        returnValue.InsuranceInfo.PlanHeldName = (string)row["PLAN_WITH"];
                    }
                    returnValue.InsuranceInfo.SEQN = (int)row["SEQN"];
                    returnValue.InsuranceInfo.OtherInsuranceProvider = (string)row["PLAN_WITH_OTHER"];
                    if (row["EFFECTIVE_DATE"] != DBNull.Value)
                    {
                        returnValue.InsuranceInfo.StartDate = (DateTime)row["EFFECTIVE_DATE"];
                    }
                    else
                    {
                        returnValue.InsuranceInfo.StartDate = DateTime.MinValue;
                    }
                    if (row["EXPIRY_DATE"] != DBNull.Value)
                    {
                        returnValue.InsuranceInfo.ExpiryDate = (DateTime)row["EXPIRY_DATE"];
                    }
                    else
                    {
                        returnValue.InsuranceInfo.ExpiryDate = DateTime.MinValue;
                    }
                    returnValue.InsuranceInfo.CertificateNumber = (string)row["CERTIFICATE"];
                }
            }
            return returnValue;
        }

        public User GetApplicationUserExaminationInfo(string User_Id)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM COTO_REGDATA " +
                " WHERE ID = " + Escape(User_Id);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count > 0)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.ExaminationInfo = new Examination();
                returnValue.ExaminationInfo.ExaminationStatus = (string)row["ENTRY_CATEGORY"];
                
                if (row["CAOT_DATE"] != DBNull.Value)
                {
                    returnValue.ExaminationInfo.CompletionDate = (DateTime)row["CAOT_DATE"];
                }
                else
                {
                    returnValue.ExaminationInfo.CompletionDate = DateTime.MinValue;
                }
                returnValue.ExaminationInfo.ExaminationDateExt = (string)row["CAOT_EXAM_REG_DATE"];

                //if (row["CAOT_REG_EXAM_DT"] != DBNull.Value)
                //{
                //    returnValue.ExaminationInfo.ExaminationDate = (DateTime)row["CAOT_REG_EXAM_DT"];
                //}
                //else
                //{
                //    returnValue.ExaminationInfo.ExaminationDate = DateTime.MinValue;
                //}

            }
            return returnValue;
        }

        public void UpdateApplicationUserInsuranceInfoLogged(string UserID, User user, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT * FROM INSURANCE  " +
                " WHERE ID = " + Escape(user.Id) + " AND INS_YEAR = " + Escape(CurrentYear.ToString());
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];

                string prevInsPlanHeld = (string)row["INS_PLAN_HELD"];
                string prevInsInsureOther = (string)row["INS_INSURE_OTHER"];
                string prevInsExpiryDate = string.Empty;
                if (row["INS_EXPIRY_DATE"] != DBNull.Value)
                {
                    prevInsExpiryDate = ((DateTime)row["INS_EXPIRY_DATE"]).ToString("MM/dd/yyyy");
                }
                string prevCertNumber = (string)row["INS_CERT_NUMBER"];


                string sql_UpdateUserInsurance = "UPDATE INSURANCE SET " +
                " INS_PLAN_HELD = '" + user.InsuranceInfo.PlanHeld + "', " +
                " INS_INSURE_OTHER = '" + user.InsuranceInfo.OtherInsuranceProvider + "', ";

                if (user.InsuranceInfo.ExpiryDate == DateTime.MinValue)
                {
                    sql_UpdateUserInsurance += " INS_EXPIRY_DATE = NULL, ";
                }
                else
                {
                    sql_UpdateUserInsurance += " INS_EXPIRY_DATE = " + Escape(user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd")) + ", ";
                }

                sql_UpdateUserInsurance += " INS_CERT_NUMBER = " + Escape(user.InsuranceInfo.CertificateNumber) + " " +

                " WHERE ID = " + Escape(user.Id) + " AND INS_YEAR = " + Escape(CurrentYear.ToString());
                db.ExecuteCommand(sql_UpdateUserInsurance);

                if (prevInsPlanHeld != user.InsuranceInfo.PlanHeld)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "INSURANCE.INS_PLAN_HELD (" + CurrentYear.ToString() + "): " + prevInsPlanHeld + " -> " + user.InsuranceInfo.PlanHeld);
                }
                if (prevInsInsureOther != user.InsuranceInfo.OtherInsuranceProvider)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "INSURANCE.INS_INSURE_OTHER (" + CurrentYear.ToString() + "): " + prevInsInsureOther + " -> " + user.InsuranceInfo.OtherInsuranceProvider);
                }
                if (prevInsExpiryDate != ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.ExpiryDate.ToString("MM/dd/yyyy")))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "INSURANCE.INS_EXPIRY_DATE (" + CurrentYear.ToString() + "): " + prevInsExpiryDate + " -> " + ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.ExpiryDate.ToString("MM/dd/yyyy")));
                }
                if (prevCertNumber != user.InsuranceInfo.CertificateNumber)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "INSURANCE.INS_CERT_NUMBER (" + CurrentYear.ToString() + "): " + prevCertNumber + " -> " + user.InsuranceInfo.CertificateNumber);
                }
            }
            else
            {
                int newSEQN = GetCounter("INSURANCE");
                string sql_InsertUserInsurance = "INSERT INTO INSURANCE (ID, SEQN, INS_YEAR, INS_PLAN_HELD, INS_INSURE_OTHER, " +
                " INS_EXPIRY_DATE, INS_CERT_NUMBER) VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", '" + CurrentYear.ToString() + "', '" +
                user.InsuranceInfo.PlanHeld + "', '" + user.InsuranceInfo.OtherInsuranceProvider + "', " +
                ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? " NULL " : ("'" + user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd") + "'")) + ", '" + user.InsuranceInfo.CertificateNumber + "') ";
                db.ExecuteCommand(sql_InsertUserInsurance);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "INSURANCE.INS_PLAN_HELD (" + CurrentYear.ToString() + "):  -> " + user.InsuranceInfo.PlanHeld);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "INSURANCE.INS_INSURE_OTHER (" + CurrentYear.ToString() + "): -> " + user.InsuranceInfo.OtherInsuranceProvider);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "INSURANCE.INS_EXPIRY_DATE (" + CurrentYear.ToString() + "):  -> " + ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd")));
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "INSURANCE.INS_CERT_NUMBER (" + CurrentYear.ToString() + "): -> " + user.InsuranceInfo.CertificateNumber);
            }
        }

        public int UpdateApplicationUserInsuranceInfoLoggedNew(string UserID, User user)
        {
            int insSEQN = user.InsuranceInfo.SEQN;
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            // updating existing record
            if (user.InsuranceInfo.SEQN > 0)
            {
                string sql = "SELECT * FROM COTO_INSURANCE  " +
                " WHERE ID = " + Escape(user.Id) + " AND SEQN = " + user.InsuranceInfo.SEQN.ToString();
                var table = db.GetData(sql);
                if (table != null && table.Rows.Count == 1)
                {
                    var row = table.Rows[0];

                    string prevInsPlanHeld = (string)row["PLAN_WITH"];
                    string prevInsInsureOther = (string)row["PLAN_WITH_OTHER"];
                    string prevInsStartDate = string.Empty;
                    if (row["EFFECTIVE_DATE"] != DBNull.Value)
                    {
                        prevInsStartDate = ((DateTime)row["EFFECTIVE_DATE"]).ToString("MM/dd/yyyy");
                    }
                    string prevInsExpiryDate = string.Empty;
                    if (row["EXPIRY_DATE"] != DBNull.Value)
                    {
                        prevInsExpiryDate = ((DateTime)row["EXPIRY_DATE"]).ToString("MM/dd/yyyy");
                    }
                    string prevCertNumber = (string)row["CERTIFICATE"];

                    string prevInsReportedDate = string.Empty;
                    if (row["DATE_REPORTED"] != DBNull.Value)
                    {
                        prevInsReportedDate = ((DateTime)row["DATE_REPORTED"]).ToString("MM/dd/yyyy hh:mm:ss");
                    }
                    //// if start date has changed then we creating new record in database
                    //if (prevInsStartDate == user.InsuranceInfo.StartDate.ToString("MM/dd/yyyy"))
                    //{
                    DateTime todayDate = DateTime.Now;
                    string today = todayDate.ToString("yyyy-MM-dd HH:mm:ss");

                    string sql_UpdateUserInsurance = "UPDATE COTO_INSURANCE SET " +
                        " DATE_REPORTED = " + Escape(today) + ", " + 
                        " PLAN_WITH = " + Escape(user.InsuranceInfo.PlanHeld) + ", " +
                        " PLAN_WITH_OTHER = " + Escape(user.InsuranceInfo.OtherInsuranceProvider) + ", ";

                        if (user.InsuranceInfo.StartDate == DateTime.MinValue)
                        {
                            sql_UpdateUserInsurance += " EFFECTIVE_DATE = NULL, ";
                        }
                        else
                        {
                            sql_UpdateUserInsurance += " EFFECTIVE_DATE = " + Escape(user.InsuranceInfo.StartDate.ToString("yyyy-MM-dd")) + ", ";
                        }

                        if (user.InsuranceInfo.ExpiryDate == DateTime.MinValue)
                        {
                            sql_UpdateUserInsurance += " EXPIRY_DATE = NULL, ";
                        }
                        else
                        {
                            sql_UpdateUserInsurance += " EXPIRY_DATE = " + Escape(user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd")) + ", ";
                        }

                        sql_UpdateUserInsurance += " CERTIFICATE = " + Escape(user.InsuranceInfo.CertificateNumber) + " " +

                        " WHERE ID = " + Escape(user.Id) + " AND SEQN = " + user.InsuranceInfo.SEQN.ToString();
                        db.ExecuteCommand(sql_UpdateUserInsurance);

                        if (prevInsPlanHeld != user.InsuranceInfo.PlanHeld)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_INSURANCE.PLAN_WITH (" + user.InsuranceInfo.SEQN.ToString() + "): " + prevInsPlanHeld + " -> " + user.InsuranceInfo.PlanHeld);
                        }
                        if (prevInsInsureOther != user.InsuranceInfo.OtherInsuranceProvider)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_INSURANCE.PLAN_WITH_OTHER (" + user.InsuranceInfo.SEQN.ToString() + "): " + prevInsInsureOther + " -> " + user.InsuranceInfo.OtherInsuranceProvider);
                        }
                        if (prevInsStartDate != ((user.InsuranceInfo.StartDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.StartDate.ToString("MM/dd/yyyy")))
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_INSURANCE.EFFECTIVE_DATE (" + user.InsuranceInfo.SEQN.ToString() + "): " + prevInsStartDate + " -> " + ((user.InsuranceInfo.StartDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.StartDate.ToString("MM/dd/yyyy")));
                        }
                        if (prevInsExpiryDate != ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.ExpiryDate.ToString("MM/dd/yyyy")))
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_INSURANCE.EXPIRY_DATE (" + user.InsuranceInfo.SEQN.ToString() + "): " + prevInsExpiryDate + " -> " + ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.ExpiryDate.ToString("MM/dd/yyyy")));
                        }
                        if (prevCertNumber != user.InsuranceInfo.CertificateNumber)
                        {
                            AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_INSURANCE.CERTIFICATE (" + user.InsuranceInfo.SEQN.ToString() + "): " + prevCertNumber + " -> " + user.InsuranceInfo.CertificateNumber);
                        }

                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_INSURANCE.DATE_REPORTED (" + user.InsuranceInfo.SEQN.ToString() + "): " + prevInsReportedDate + " -> " + todayDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    //}
                    //else
                    //{
                    //    int newSEQN = GetCounter("COTO_INSURANCE");
                    //    string sql_InsertUserInsurance = "INSERT INTO COTO_INSURANCE (ID, SEQN, EFFECTIVE_DATE, PLAN_WITH, PLAN_WITH_OTHER, " +
                    //    " EXPIRY_DATE, CERTIFICATE) VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", " +
                    //    ((user.InsuranceInfo.StartDate == DateTime.MinValue) ? " NULL " : (Escape(user.InsuranceInfo.StartDate.ToString("yyyy-MM-dd")))) + ", " + 
                    //    Escape(user.InsuranceInfo.PlanHeld) + ", " + Escape(user.InsuranceInfo.OtherInsuranceProvider) + ", " +
                    //    ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? " NULL " : ( Escape(user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd")))) + ", " + Escape(user.InsuranceInfo.CertificateNumber) + ") ";
                    //    db.ExecuteCommand(sql_InsertUserInsurance);

                    //    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.PLAN_WITH (" + newSEQN.ToString() + "):  -> " + user.InsuranceInfo.PlanHeld);
                    //    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.PLAN_WITH_OTHER (" + newSEQN.ToString() + "): -> " + user.InsuranceInfo.OtherInsuranceProvider);
                    //    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.EFFECTIVE_DATE (" + newSEQN.ToString() + "):  -> " + ((user.InsuranceInfo.StartDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.StartDate.ToString("yyyy-MM-dd")));
                    //    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.EXPIRY_DATE (" + newSEQN.ToString() + "):  -> " + ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd")));
                    //    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.CERTIFICATE (" + newSEQN.ToString() + "): -> " + user.InsuranceInfo.CertificateNumber);
                    //}
                }
                else
                {
                    DateTime todayDate = DateTime.Now;
                    // just in case the record was deleted when it was updated - creating new one
                    int newSEQN = GetCounter("COTO_INSURANCE");
                    string sql_InsertUserInsurance = "INSERT INTO COTO_INSURANCE (ID, SEQN, EFFECTIVE_DATE, PLAN_WITH, PLAN_WITH_OTHER, " +
                    " EXPIRY_DATE, CERTIFICATE, DATE_REPORTED) VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", " +
                    ((user.InsuranceInfo.StartDate == DateTime.MinValue) ? " NULL " : (Escape(user.InsuranceInfo.StartDate.ToString("yyyy-MM-dd")))) + ", " +
                    Escape(user.InsuranceInfo.PlanHeld) + ", " + Escape(user.InsuranceInfo.OtherInsuranceProvider) + ", " +
                    ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? " NULL " : (Escape(user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd")))) + ", " + 
                    Escape(user.InsuranceInfo.CertificateNumber) + ", " + Escape(todayDate.ToString("yyyy-MM-dd HH:mm:ss")) + ") ";
                    db.ExecuteCommand(sql_InsertUserInsurance);

                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.PLAN_WITH (" + newSEQN.ToString() + "):  -> " + user.InsuranceInfo.PlanHeld);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.PLAN_WITH_OTHER (" + newSEQN.ToString() + "): -> " + user.InsuranceInfo.OtherInsuranceProvider);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.EFFECTIVE_DATE (" + newSEQN.ToString() + "):  -> " + ((user.InsuranceInfo.StartDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.StartDate.ToString("yyyy-MM-dd")));
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.EXPIRY_DATE (" + newSEQN.ToString() + "):  -> " + ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd")));
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.CERTIFICATE (" + newSEQN.ToString() + "): -> " + user.InsuranceInfo.CertificateNumber);
                    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.DATE_REPORTED (" + newSEQN.ToString() + "): -> " + todayDate.ToString("yyyy-MM-dd HH:mm:ss"));
                }
            }
            else
            {
                DateTime todayDate = DateTime.Now;
                int newSEQN = GetCounter("COTO_INSURANCE");
                insSEQN = newSEQN;
                string sql_InsertUserInsurance = "INSERT INTO COTO_INSURANCE (ID, SEQN, EFFECTIVE_DATE, PLAN_WITH, PLAN_WITH_OTHER, " +
                " EXPIRY_DATE, CERTIFICATE, DATE_REPORTED) VALUES (" + Escape(user.Id) + ", " + newSEQN.ToString() + ", " +
                ((user.InsuranceInfo.StartDate == DateTime.MinValue) ? " NULL " : (Escape(user.InsuranceInfo.StartDate.ToString("yyyy-MM-dd")))) + ", " +
                Escape(user.InsuranceInfo.PlanHeld) + ", " + Escape(user.InsuranceInfo.OtherInsuranceProvider) + ", " +
                ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? " NULL " : (Escape(user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd")))) + ", " + 
                Escape(user.InsuranceInfo.CertificateNumber) + ", " + Escape(todayDate.ToString("yyyy-MM-dd HH:mm:ss")) + ") ";
                db.ExecuteCommand(sql_InsertUserInsurance);

                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.PLAN_WITH (" + newSEQN.ToString() + "):  -> " + user.InsuranceInfo.PlanHeld);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.PLAN_WITH_OTHER (" + newSEQN.ToString() + "): -> " + user.InsuranceInfo.OtherInsuranceProvider);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.EFFECTIVE_DATE (" + newSEQN.ToString() + "):  -> " + ((user.InsuranceInfo.StartDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.StartDate.ToString("yyyy-MM-dd")));
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.EXPIRY_DATE (" + newSEQN.ToString() + "):  -> " + ((user.InsuranceInfo.ExpiryDate == DateTime.MinValue) ? string.Empty : user.InsuranceInfo.ExpiryDate.ToString("yyyy-MM-dd")));
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.CERTIFICATE (" + newSEQN.ToString() + "): -> " + user.InsuranceInfo.CertificateNumber);
                AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_INSURANCE.DATE_REPORTED (" + newSEQN.ToString() + "): -> " + todayDate.ToString("yyyy-MM-dd HH:mm:ss"));
            }

            return insSEQN;
        }

        public void UpdateApplicationUserExamInfoLogged(string UserID, User user)
        {
            
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT * FROM COTO_REGDATA  " +
                " WHERE ID = " + Escape(user.Id);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];

                string prevExamCategory = (string)row["ENTRY_CATEGORY"];
                string prevComplDate = string.Empty;
                string prevExamDate = string.Empty;

                if (row["CAOT_DATE"] != DBNull.Value)
                {
                    prevComplDate = ((DateTime)row["CAOT_DATE"]).ToString("MM/dd/yyyy");
                }
                    
                
                if (row["CAOT_REG_EXAM_DT"] != DBNull.Value)
                {
                    prevExamDate = ((DateTime)row["CAOT_REG_EXAM_DT"]).ToString("MM/dd/yyyy");
                }


                string sql_UpdateUserExam = "UPDATE COTO_REGDATA SET " +
                " ENTRY_CATEGORY = " + Escape(user.ExaminationInfo.ExaminationStatus);
                
                string currComplDate = string.Empty;
                if (user.ExaminationInfo.CompletionDate == DateTime.MinValue)
                {
                    sql_UpdateUserExam += ", CAOT_DATE = NULL ";
                }
                else
                {
                    currComplDate = user.ExaminationInfo.CompletionDate.ToString("yyyy-MM-dd");
                    sql_UpdateUserExam += ", CAOT_DATE = " + Escape(currComplDate);
                }
                string currExamDate = user.ExaminationInfo.ExaminationDateExt;
                if (string.IsNullOrEmpty(user.ExaminationInfo.ExaminationDateExt))
                {
                    sql_UpdateUserExam += ", CAOT_EXAM_REG_DATE = '' ";
                }
                else
                {
                    sql_UpdateUserExam += ", CAOT_EXAM_REG_DATE = " + Escape(user.ExaminationInfo.ExaminationDateExt);
                }
                //string currExamDate = string.Empty;
                //if (user.ExaminationInfo.ExaminationDate == DateTime.MinValue)
                //{
                //    sql_UpdateUserExam += ", CAOT_EXAM_REG_DATE = '' ";
                //}
                //else
                //{
                //    currExamDate = user.ExaminationInfo.ExaminationDate.ToString("yyyy-MM-dd");
                //    sql_UpdateUserExam += ", CAOT_EXAM_REG_DATE = " + Escape(currExamDate);
                //}

                sql_UpdateUserExam += " WHERE ID = " + Escape(user.Id);
                db.ExecuteCommand(sql_UpdateUserExam);


                if (prevExamCategory != user.ExaminationInfo.ExaminationStatus)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.ENTRY_CATEGORY : " + prevExamCategory + " -> " + user.ExaminationInfo.ExaminationStatus);
                }
                if (prevComplDate != currComplDate)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.CAOT_DATE : " + prevComplDate + " -> " + currComplDate);
                }
                if (prevExamDate != currExamDate)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_REGDATA.CAOT_EXAM_REG_DATE : " + prevExamDate + " -> " + currExamDate);
                }
            }
        }

        // step 10

        public User GetApplicationUserDeclarationInfo(string User_Id, int CurrentYear)
        {
            User returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM COTO_APPLICATION " +
                " WHERE ID = " + Escape(User_Id);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];
                returnValue = new User();
                returnValue.Id = User_Id;
                returnValue.DeclarationInfo = new COTO_RegOnly.Classes.Declaration();
                if (row["AUTHORIZATION_RECD"] != DBNull.Value)
                {
                    returnValue.DeclarationInfo.ApplicationAuthorizationDate = (DateTime)row["AUTHORIZATION_RECD"];
                    returnValue.DeclarationInfo.ApplicationAuthorization = (returnValue.DeclarationInfo.ApplicationAuthorizationDate != DateTime.MinValue);
                }
                else
                {
                    returnValue.DeclarationInfo.ApplicationAuthorizationDate = null;
                    returnValue.DeclarationInfo.ApplicationAuthorization = false;
                }

                if (row["DECLARATION"] != DBNull.Value)
                {
                    returnValue.DeclarationInfo.ApplicationDeclarationDate = (DateTime)row["DECLARATION"];
                    returnValue.DeclarationInfo.ApplicationDeclaration = (returnValue.DeclarationInfo.ApplicationDeclarationDate != DateTime.MinValue);
                }
                else
                {
                    returnValue.DeclarationInfo.ApplicationDeclarationDate = null;
                    returnValue.DeclarationInfo.ApplicationDeclaration = false;
                }
                returnValue.DeclarationInfo.PaymentApplicationFee = (((string)row["DECLARE_PAYMENT"]).ToUpper() == "YES");
                returnValue.DeclarationInfo.NewsletterUpdates = (((string)row["NEWSLETTER_UPDATES"]).ToUpper() == "YES");
                returnValue.DeclarationInfo.Satisfaction = (double)row["SATISFACTION"];
                if (row["SATISFACTION_COMMENTS"] != DBNull.Value) returnValue.DeclarationInfo.SatisfactionNotes = (string)row["SATISFACTION_COMMENTS"];

            }
            else
            {
                // create new record for this id

                string sql_createNewDeclaration = string.Format("INSERT INTO  COTO_APPLICATION (ID) VALUES ({0})", Escape(User_Id));
                db.ExecuteCommand(sql_createNewDeclaration);

                returnValue.DeclarationInfo.ApplicationAuthorizationDate = null;
                returnValue.DeclarationInfo.ApplicationAuthorization = false;
                returnValue.DeclarationInfo.ApplicationDeclarationDate = null;
                returnValue.DeclarationInfo.ApplicationDeclaration = false;
            }
            return returnValue;
        }

        public void UpdateApplicationUserDeclarationInfoLogged(string UserID, User user, int CurrentYear)
        {
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();

            string sql = "SELECT * FROM COTO_APPLICATION  " +
                " WHERE ID = " + Escape(user.Id);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];
                string prevRegDeclaration = string.Empty;
                if (row["DECLARATION"] != DBNull.Value)
                {
                    prevRegDeclaration = ((DateTime)row["DECLARATION"]).ToString("MM/dd/yyyy");
                }

                string prevAuthDeclaration = string.Empty;
                if (row["AUTHORIZATION_RECD"] != DBNull.Value)
                {
                    prevAuthDeclaration = ((DateTime)row["AUTHORIZATION_RECD"]).ToString("MM/dd/yyyy");
                }

                string prevPayment = (string)row["DECLARE_PAYMENT"];
                double prevSatisfaction = (double)row["SATISFACTION"];
                string prevSatisfactionNotes = string.Empty;
                if (row["SATISFACTION_COMMENTS"] != DBNull.Value) 
                {
                    prevSatisfactionNotes = (string)row["SATISFACTION_COMMENTS"];
                }
                string prevNewsletter = (string)row["NEWSLETTER_UPDATES"];

                DateTime declarationDate = (DateTime)user.DeclarationInfo.ApplicationDeclarationDate;
                string curDeclarationDate = declarationDate.ToString("MM/dd/yyyy");
                DateTime authDate = (DateTime)user.DeclarationInfo.ApplicationAuthorizationDate;
                string curAuthDate = authDate.ToString("MM/dd/yyyy");
                string sql_UpdateUserDeclaration = "UPDATE COTO_APPLICATION SET " +
                " DECLARATION = " + Escape(declarationDate.ToString("yyyy/MM/dd")) + ", " +
                " AUTHORIZATION_RECD = " + Escape(authDate.ToString("yyyy/MM/dd")) + ", " +
                " DECLARE_PAYMENT = " + Escape(user.DeclarationInfo.PaymentApplicationFee ? "Yes" : "No") + ", " +
                " NEWSLETTER_UPDATES = " + Escape(user.DeclarationInfo.NewsletterUpdates ? "Yes" : "No") + ", " +
                " SATISFACTION = " + user.DeclarationInfo.Satisfaction.ToString() + ", " +
                " SATISFACTION_COMMENTS = " + Escape(user.DeclarationInfo.SatisfactionNotes) + 
                " WHERE ID = " + Escape(user.Id);
                db.ExecuteCommand(sql_UpdateUserDeclaration);

                if (prevRegDeclaration != curDeclarationDate)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_APPLICATION.DECLARATION : " + prevRegDeclaration + " -> " + curDeclarationDate);
                }
                if (prevAuthDeclaration != curAuthDate)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_APPLICATION.AUTHORIZATION_RECD : " + prevAuthDeclaration + " -> " + curAuthDate);
                }
                if (prevPayment != (user.DeclarationInfo.PaymentApplicationFee ? "Yes" : "No"))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_APPLICATION.DECLARE_PAYMENT : " + prevPayment + " -> " + (user.DeclarationInfo.PaymentApplicationFee ? "Yes" : "No"));
                }
                if (prevNewsletter != (user.DeclarationInfo.NewsletterUpdates ? "Yes" : "No"))
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_APPLICATION.NEWSLETTER_UPDATES : " + prevNewsletter + " -> " + (user.DeclarationInfo.NewsletterUpdates ? "Yes" : "No"));
                }
                if (prevSatisfaction != user.DeclarationInfo.Satisfaction)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_APPLICATION.SATISFACTION : " + prevSatisfaction.ToString() + " -> " + user.DeclarationInfo.Satisfaction.ToString());
                }
                if (prevSatisfactionNotes != user.DeclarationInfo.SatisfactionNotes)
                {
                    AddLogEntry(DateTime.Now, "CHANGE", "CHANGE", UserID, UserID, "COTO_APPLICATION.SATISFACTION_COMMENTS : " + prevSatisfactionNotes + " -> " + user.DeclarationInfo.SatisfactionNotes);
                }
            }
            //else
            //{
                
            //    DateTime declarationDate = (DateTime)user.DeclarationInfo.ApplicationDeclarationDate;
            //    string curDeclarationDate = declarationDate.ToString("MM/dd/yyyy");
            //    DateTime authDate = (DateTime)user.DeclarationInfo.ApplicationAuthorizationDate;
            //    string curAuthDate = authDate.ToString("MM/dd/yyyy");

            //    string sql_InsertUserDeclaration = "INSERT INTO COTO_APPLICATION (ID, DECLARATION,  AUTHORIZATION_RECD) " +
            //    "VALUES (" + Escape(user.Id) + ", " + Escape(declarationDate.ToString("yyyy/MM/dd")) + ", " + Escape(declarationDate.ToString("yyyy/MM/dd")) + ")";
                
            //    db.ExecuteCommand(sql_InsertUserDeclaration);

            //    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_APPLICATION.DECLARATION : -> " + curDeclarationDate);
            //    AddLogEntry(DateTime.Now, "ADD", "CHANGE", UserID, UserID, "COTO_APPLICATION.AUTHORIZATION_RECD : -> " + curAuthDate);
            //}
        }

        // application check form

        public ApplicationStatus GetUserApplicationConuctSubmittedDate(string User_Id)
        {
            ApplicationStatus returnValue = null;
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM COTO_APPLICATION " +
                " WHERE ID = " + Escape(User_Id);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];
                returnValue = new ApplicationStatus();

                if (row["CONDUCT_SUBMITTED"] != DBNull.Value)
                {
                    returnValue.ConductSubmitted = (DateTime)row["CONDUCT_SUBMITTED"];
                }
                else
                {
                    returnValue.ConductSubmitted = DateTime.MinValue;
                }
            }
            return returnValue;
        }

        public ApplicationStatus GetUserApplicationStatusInfo(string User_Id)
        {
            ApplicationStatus returnValue = null;
            //var db = new GT_Admin.BusinessResources.BusinessEntities.DAL.clsDB();
            string dbconn = WebConfigItems.GetConnectionString();
            var db = new clsDB();
            string sql = "SELECT * FROM COTO_APPLICATION " +
                " WHERE ID = " + Escape(User_Id);
            var table = db.GetData(sql);
            if (table != null && table.Rows.Count == 1)
            {
                var row = table.Rows[0];
                returnValue = new ApplicationStatus();

                if (row["APPLICATION"] != DBNull.Value)
                {
                    returnValue.ApplicationReceivedDate = (DateTime)row["APPLICATION"];
                }
                else
                {
                    returnValue.ApplicationReceivedDate = DateTime.MinValue;
                }

                if (row["APPLICATION_FEE"] != DBNull.Value)
                {
                    returnValue.ApplicationFeeReceivedDate = (DateTime)row["APPLICATION_FEE"];
                }
                else
                {
                    returnValue.ApplicationFeeReceivedDate = DateTime.MinValue;
                }

                if (row["CURRENCY_REQUIREMENT"] != DBNull.Value)
                {
                    returnValue.CurrencyRequrementMetDate = (DateTime)row["CURRENCY_REQUIREMENT"];
                }
                else
                {
                    returnValue.CurrencyRequrementMetDate = DateTime.MinValue;
                }

                if (row["ADDTL_CURRENCY_SHEET"] != DBNull.Value)
                {
                    returnValue.AddCurrencyDataSheetReceivedDate = (DateTime)row["ADDTL_CURRENCY_SHEET"];
                }
                else
                {
                    returnValue.AddCurrencyDataSheetReceivedDate = DateTime.MinValue;
                }

                if (row["PRACTICE_SUPERVISOR_FORM"] != DBNull.Value)
                {
                    returnValue.PracticeSupervisorFormReceivedDate = (DateTime)row["PRACTICE_SUPERVISOR_FORM"];
                }
                else
                {
                    returnValue.PracticeSupervisorFormReceivedDate = DateTime.MinValue;
                }

                if (row["INITIAL_LEARNING_CONTRACT"] != DBNull.Value)
                {
                    returnValue.InitialLearningContactDate = (DateTime)row["INITIAL_LEARNING_CONTRACT"];
                }
                else
                {
                    returnValue.InitialLearningContactDate = DateTime.MinValue;
                }

                if (row["FINAL_LEARNING_CONTRACT"] != DBNull.Value)
                {
                    returnValue.FinalLearningContactDate = (DateTime)row["FINAL_LEARNING_CONTRACT"];
                }
                else
                {
                    returnValue.FinalLearningContactDate = DateTime.MinValue;
                }

                if (row["VS_CHECK"] != DBNull.Value)
                {
                    returnValue.VulnerableCheckDate = (DateTime)row["VS_CHECK"];
                }
                else
                {
                    returnValue.VulnerableCheckDate = DateTime.MinValue;
                }

                if (row["CONDUCT_REQUIREMENT"] != DBNull.Value)
                {
                    returnValue.ConductRequirementMetDate = (DateTime)row["CONDUCT_REQUIREMENT"];
                }
                else
                {
                    returnValue.ConductRequirementMetDate = DateTime.MinValue;
                }

                if (row["CONDUCT_REVIEW_FEE"] != DBNull.Value)
                {
                    returnValue.ConductReviewFeeReceivedDate = (DateTime)row["CONDUCT_REVIEW_FEE"];
                }
                else
                {
                    returnValue.ConductReviewFeeReceivedDate = DateTime.MinValue;
                }

                if (row["LANGUAGE_PROFICIENCY"] != DBNull.Value)
                {
                    returnValue.LanguageFluencyReceivedDate = (DateTime)row["LANGUAGE_PROFICIENCY"];
                }
                else
                {
                    returnValue.LanguageFluencyReceivedDate = DateTime.MinValue;
                }

                if (row["LEGAL_WORK_AUTHORIZATION"] != DBNull.Value)
                {
                    returnValue.WorkEligibilityLegalWorkDate = (DateTime)row["LEGAL_WORK_AUTHORIZATION"];
                }
                else
                {
                    returnValue.WorkEligibilityLegalWorkDate = DateTime.MinValue;
                }

                //if (row["WES_REPORT"] != DBNull.Value)
                //{
                //    returnValue.WESReportReceivedDate = (DateTime)row["WES_REPORT"];
                //}
                //else
                //{
                //    returnValue.WESReportReceivedDate = DateTime.MinValue;
                //}

                //if (row["ACADEMIC_EQUIV_REVIEW"] != DBNull.Value)
                //{
                //    returnValue.AcademicEquivalencyReviewFeeReceivedDate = (DateTime)row["ACADEMIC_EQUIV_REVIEW"];
                //}
                //else
                //{
                //    returnValue.AcademicEquivalencyReviewFeeReceivedDate = DateTime.MinValue;
                //}

                //if (row["ACADEMIC_REVIEW_TOOL"] != DBNull.Value)
                //{
                //    returnValue.AcademicReviewToolReceivedDate = (DateTime)row["ACADEMIC_REVIEW_TOOL"];
                //}
                //else
                //{
                //    returnValue.AcademicReviewToolReceivedDate = DateTime.MinValue;
                //}

                //if (row["CURRICULUM"] != DBNull.Value)
                //{
                //    returnValue.CirriculumReceivedDate = (DateTime)row["CURRICULUM"];
                //}
                //else
                //{
                //    returnValue.CirriculumReceivedDate = DateTime.MinValue;
                //}

                if (row["EDUCATIONAL_TRANSCRIPT"] != DBNull.Value)
                {
                    returnValue.FinalEducationalTranscriptReceivedDate = (DateTime)row["EDUCATIONAL_TRANSCRIPT"];
                }
                else
                {
                    returnValue.FinalEducationalTranscriptReceivedDate = DateTime.MinValue;
                }

                //if (row["PROGRAM_APPROVED_COTO"] != DBNull.Value)
                //{
                //    returnValue.ProgramApprovedCotoDate = (DateTime)row["PROGRAM_APPROVED_COTO"];
                //}
                //else
                //{
                //    returnValue.ProgramApprovedCotoDate = DateTime.MinValue;
                //}

                if (row["SEAS_DISPOSITION_RPT"] != DBNull.Value) {
                    returnValue.SeasDispositionReport = (DateTime)row["SEAS_DISPOSITION_RPT"];
                }
                else {
                    returnValue.SeasDispositionReport = DateTime.MinValue;
                }

                if (row["LMSA"] != DBNull.Value)
                {
                    returnValue.LMSA_FormReceived = (DateTime)row["LMSA"];
                }
                else
                {
                    returnValue.LMSA_FormReceived = DateTime.MinValue;
                }

                if (row["OT_REG_HIST_1"] != DBNull.Value)
                {
                    returnValue.OTRegulatoryHistory1ReceivedDate = (DateTime)row["OT_REG_HIST_1"];
                }
                else
                {
                    returnValue.OTRegulatoryHistory1ReceivedDate = DateTime.MinValue;
                }

                if (row["OT_REG_HIST_2"] != DBNull.Value)
                {
                    returnValue.OTRegulatoryHistory2ReceivedDate = (DateTime)row["OT_REG_HIST_2"];
                }
                else
                {
                    returnValue.OTRegulatoryHistory2ReceivedDate = DateTime.MinValue;
                }

                if (row["OT_REG_HIST_3"] != DBNull.Value)
                {
                    returnValue.OTRegulatoryHistory3ReceivedDate = (DateTime)row["OT_REG_HIST_3"];
                }
                else
                {
                    returnValue.OTRegulatoryHistory3ReceivedDate = DateTime.MinValue;
                }

                if (row["OT_REG_HIST_4"] != DBNull.Value)
                {
                    returnValue.OTRegulatoryHistory4ReceivedDate = (DateTime)row["OT_REG_HIST_4"];
                }
                else
                {
                    returnValue.OTRegulatoryHistory4ReceivedDate = DateTime.MinValue;
                }

                if (row["OT_REG_HIST_5"] != DBNull.Value)
                {
                    returnValue.OTRegulatoryHistory5ReceivedDate = (DateTime)row["OT_REG_HIST_5"];
                }
                else
                {
                    returnValue.OTRegulatoryHistory5ReceivedDate = DateTime.MinValue;
                }

                if (row["OTHER_PROF_REG_1"] != DBNull.Value)
                {
                    returnValue.OtherProfessionRegistration1Date = (DateTime)row["OTHER_PROF_REG_1"];
                }
                else
                {
                    returnValue.OtherProfessionRegistration1Date = DateTime.MinValue;
                }

                if (row["OTHER_PROF_REG_2"] != DBNull.Value)
                {
                    returnValue.OtherProfessionRegistration2Date = (DateTime)row["OTHER_PROF_REG_2"];
                }
                else
                {
                    returnValue.OtherProfessionRegistration2Date = DateTime.MinValue;
                }

                if (row["OTHER_PROF_REG_3"] != DBNull.Value)
                {
                    returnValue.OtherProfessionRegistration3Date = (DateTime)row["OTHER_PROF_REG_3"];
                }
                else
                {
                    returnValue.OtherProfessionRegistration3Date = DateTime.MinValue;
                }

                if (row["PROF_LIABILITY_INSURANCE"] != DBNull.Value)
                {
                    returnValue.ProfessionLiabilityInsuranceReceivedDate = (DateTime)row["PROF_LIABILITY_INSURANCE"];
                }
                else
                {
                    returnValue.ProfessionLiabilityInsuranceReceivedDate = DateTime.MinValue;
                }

                if (row["AFFIDAVIT"] != DBNull.Value)
                {
                    returnValue.AffidavitReceivedDate = (DateTime)row["AFFIDAVIT"];
                }
                else
                {
                    returnValue.AffidavitReceivedDate = DateTime.MinValue;
                }

                if (row["EMPL_ACKNOWLEDGE_FORM"] != DBNull.Value)
                {
                    returnValue.EmployerAcknowledgeFormReceivedDate = (DateTime)row["EMPL_ACKNOWLEDGE_FORM"];
                }
                else
                {
                    returnValue.EmployerAcknowledgeFormReceivedDate = DateTime.MinValue;
                }

                if (row["CAOT_EXAM_REGISTRATION"] != DBNull.Value)
                {
                    returnValue.ConfirmationRegistrationCAOTExamReceivedDate = (DateTime)row["CAOT_EXAM_REGISTRATION"];
                }
                else
                {
                    returnValue.ConfirmationRegistrationCAOTExamReceivedDate = DateTime.MinValue;
                }

                if (row["CAOT_EXAM_RESULTS"] != DBNull.Value)
                {
                    returnValue.CAOTExamResultsReceivedDate = (DateTime)row["CAOT_EXAM_RESULTS"];
                }
                else
                {
                    returnValue.CAOTExamResultsReceivedDate = DateTime.MinValue;
                }

                if (row["REGISTRATION_FEE"] != DBNull.Value)
                {
                    returnValue.RegistrationFeeReceivedDate = (DateTime)row["REGISTRATION_FEE"];
                }
                else
                {
                    returnValue.RegistrationFeeReceivedDate = DateTime.MinValue;
                }

                returnValue.ReadyOnlineView = (bool)row["READY_ONLINE_VIEW"];

                returnValue.AddCurrencyDataSheetReceivedReqired = (bool)row["ADDTL_CURRENCY_SHEET_REQD"];

                returnValue.PracticeSupervisorFormReceivedRequired = (bool)row["PRACTICE_SUPERVISOR_REQD"];

                returnValue.InitialLearningContactRequired = (bool)row["INITIAL_LEARNING_CONTRACT_REQD"];

                returnValue.FinalLearningContactRequired = (bool)row["FINAL_LEARNING_CONTRACT_REQD"];

                returnValue.ConductReviewFeeReceivedRequired = (bool)row["CONDUCT_REVIEW_REQD"];

                returnValue.LanguageFluencyReceivedRequired = (bool)row["PROOF_LANG_PROF_RECD_REQD"];

                returnValue.WESReportReceivedRequired = (bool)row["WES_REPORT_REQD"];

                returnValue.AcademicEquivalencyReviewFeeReceivedRequired = (bool)row["ACADEMIC_REVIEW_REQD"];

                returnValue.AcademicReviewToolReceivedRequired = (bool)row["ACADEMIC_REVIEW_TOOL_REQD"];

                returnValue.CourseCirriculumReceivedRequired = (bool)row["CURRICULUM_REQD"];

                returnValue.LMSA_FormReceivedRequired = (bool)row["LMSA_REQD"];

                
                returnValue.FinalEducationalTranscriptReceivedRequired = (bool)row["EDUCATIONAL_TRANSCRIPT_REQD"];

                returnValue.SeasDispositionReportRequired = (bool)row["SEAS_DISPOSITION_RPT_REQD"];

                returnValue.OTRegulatoryHistory1ReceivedRequired = (bool)row["OT_REG_HIST_1_REQD"];

                returnValue.OTRegulatoryHistory2ReceivedRequired = (bool)row["OT_REG_HIST_2_REQD"];

                returnValue.OTRegulatoryHistory3ReceivedRequired = (bool)row["OT_REG_HIST_3_REQD"];

                returnValue.OTRegulatoryHistory4ReceivedRequired = (bool)row["OT_REG_HIST_4_REQD"];

                returnValue.OTRegulatoryHistory5ReceivedRequired = (bool)row["OT_REG_HIST_5_REQD"];

                returnValue.OtherProfessionRegistration1Required = (bool)row["OTHER_PROF_REG_1_REQD"];

                returnValue.OtherProfessionRegistration2Required = (bool)row["OTHER_PROF_REG_2_REQD"];

                returnValue.OtherProfessionRegistration3Required = (bool)row["OTHER_PROF_REG_3_REQD"];

                returnValue.AffidavitReceivedRequired = (bool)row["AFFIDAVIT_REQD"];

                returnValue.EmployerAcknowledgeFormReceivedRequired = (bool)row["EMPL_ACKNOWLEDGE_REQD"];

                returnValue.ConfirmationRegistrationCAOTExamReceivedRequired = (bool)row["CAO_EXAM_REG_REQD"];
            }
            
            return returnValue;
        }

        // OfflinePayment page

        //public CContact GetContactByID(string iMIS_ID)
        //{
        //    Asi.iBO.ContactManagement.CContact contact;
        //    IiMISUser user;
        //    //user = CStaffUser.LoginByUserId("manager");
        //    user = CContactUser.LoginByWebLogin(Asi.Security.AppPrincipal.CurrentIdentity.UserId);
        //    user.ThrowExceptionOnError = true;
        //    contact = new Asi.iBO.ContactManagement.CContact(user, iMIS_ID);
        //    return contact;
        //}

        public List<Subscription> GetUserApplicationSubscriptions(string UserID)
        {
            List<Subscription> list = null;
            var db = new clsDB();
            string sql = " SELECT Subscriptions.ID," +
                         " Subscriptions.PRODUCT_CODE," +
                         " Subscriptions.BILL_DATE," +
                         " Subscriptions.BALANCE," +
                         " Subscriptions.BILL_BEGIN," +
                         " Subscriptions.BILL_THRU," +
                         " Subscriptions.BILL_AMOUNT," +
                         " Subscriptions.PAID_THRU," +
                         " Subscriptions.STATUS," +
                         " Subscriptions.RENEW_MONTHS," +
                         " Subscriptions.INVOICE_REFERENCE_NUM," +
                         " Product.PRICE_1," +
                         " Product.PROD_TYPE," +
                         " Product.TITLE," +
                         " Product.INCOME_ACCOUNT," +
                         " Product.DEFERRED_INCOME_ACCOUNT," +
                         " Product.PRICE_RULES_EXIST," +
                         " Product.RENEW_MONTHS," +
                         " Product.PAYMENT_PRIORITY," +
                         " Product.DESCRIPTION," +
                         " Product.IS_FR_ITEM," +
                         " Product.APPEAL_CODE," +
                         " Product.CAMPAIGN_CODE," +
                         " Product.FAIR_MARKET_VALUE," +
                         " Product.ORG_CODE" +
                         " FROM	Subscriptions, Product WHERE Subscriptions.PRODUCT_CODE = Product.PRODUCT_CODE" +
                         " AND Subscriptions.ID = " + Escape(UserID) +
                         " AND Subscriptions.STATUS = 'A'" +
                         " ORDER BY PAYMENT_PRIORITY ASC";

            var table = db.GetData(sql);
            if (table != null && table.Rows != null && table.Rows.Count > 0)
            {
                list = new List<Subscription>();
                foreach (DataRow row in table.Rows)
                {
                    var item = new Subscription();
                    item.ID = UserID;
                    item.ProductCode = (string)row["PRODUCT_CODE"];
                    item.Amount = (decimal)row["BILL_AMOUNT"];
                    list.Add(item);
                }
            }

            return list;
        }


        //public List<CSubscription> GetUserApplicationSubscriptions(string CurrentId)
        //{
        //    List<CSubscription> list = null;
        //    var contact = GetContactByID(CurrentId);
        //    if (contact != null)
        //    {
        //        list = contact.Subscriptions.ToList();
        //    }

        //    return list;
        //}

        #endregion

        #region IMIS

        //public CContact GetContactByID(string iMIS_ID)
        //{
        //    Asi.iBO.ContactManagement.CContact contact;
        //    IiMISUser user;
        //    //user = CStaffUser.LoginByUserId("manager");
        //    user = CContactUser.LoginByWebLogin(Asi.Security.AppPrincipal.CurrentIdentity.UserId);
        //    user.ThrowExceptionOnError = true;
        //    contact = new Asi.iBO.ContactManagement.CContact(user, iMIS_ID);
        //    return contact;
        //}

        //public string CreateNewContact(string Prefix, string FirstName, string MiddleName, string LastName, string Email, string Username, string Password, string MemberStatus)
        //{
        //    //DeleteIBOGuest();

        //    Asi.iBO.ContactManagement.CContact contact;

        //    IiMISUser user;
        //    //user = CStaffUser.LoginByUserId("manager");
        //    user = CContactUser.LoginByWebLogin(Asi.Security.AppPrincipal.CurrentIdentity.UserId);
        //    user.ThrowExceptionOnError = true;
        //    contact = new Asi.iBO.ContactManagement.CContact(user);

        //    contact.Prefix = Prefix;
        //    contact.FirstName = FirstName;
        //    contact.MiddleName = MiddleName;
        //    contact.LastName = LastName;
        //    //contact.Suffix = Suffix;

        //    contact.EmailAddress = Email;
        //    //contact.WebsiteAddress = Website;
        //    //contact.InstituteName = Company;
        //    //contact.InstituteContactId = CompanyId;
        //    //contact.Title = JobTitle;
        //    //contact.CustomerTypeCode = MemberTypeCode;
        //    contact.ContactStatusCode = MemberStatus;
        //    contact.JoinDate = DateTime.Today;

        //    contact.Validate();

        //    contact.Save();

        //    contact.CreateUserSecurity(Username, Password);
        //    contact.Save();

        //    contact.DefaultAddress.EmailAddress = Email;
        //    contact.EmailAddress = Email;
        //    contact.Save();


        //    var newUser = CContactUser.LoginByWebLogin(Username);
        //    if (newUser != null)
        //    {
        //        return newUser.ContactId;
        //    }

        //    return contact.ContactId;
        //}

        //public void DeleteIBOGuest()
        //{
        //    string dbconn = _ConnString;

        //    var db = new clsDB();

        //    SQLHelper.ExecuteNonQuery(dbconn, "VA_DeleteIBOGuest");
        //}

        //public string UpdateContact(string iMIS_ID, string Prefix, string FirstName, string MiddleName, string LastName, string Email )
        //{
        //    Asi.iBO.ContactManagement.CContact contact;
        //    IiMISUser user;
        //    //user = CStaffUser.LoginByUserId("manager");
        //    user = CContactUser.LoginByWebLogin(Asi.Security.AppPrincipal.CurrentIdentity.UserId);
        //    user.ThrowExceptionOnError = true;
        //    contact = new Asi.iBO.ContactManagement.CContact(user, iMIS_ID);


        //    contact.Prefix = Prefix;
        //    contact.FirstName = FirstName;
        //    contact.MiddleName = MiddleName;
        //    contact.LastName = LastName;
        //    //contact.Suffix = Suffix;
        //    contact.EmailAddress = Email;
        //    //contact.WebsiteAddress = Website;
        //    //contact.InstituteName = Company;
        //    //contact.InstituteContactId = CompanyId;
        //    //contact.Title = JobTitle;
        //    //contact.CustomerTypeCode = MemberTypeCode;

        //    contact.Validate();

        //    contact.Save();
        //    return contact.ContactId;
        //}

        //public void UpdateAddress(string iMIS_ID, string AddressPuprose, string AddressLine1, string AddressLine2, string AddressLine3, string City, string Country, string Province,
        //    string PostalCode, string Phone, string Fax, bool Billing, bool Shipping, bool Mailing)
        //{
        //    Asi.iBO.ContactManagement.CContact contact;
        //    IiMISUser user;
        //    //user = CStaffUser.LoginByUserId("manager");
        //    user = CContactUser.LoginByWebLogin(Asi.Security.AppPrincipal.CurrentIdentity.UserId);
        //    user.ThrowExceptionOnError = true;
        //    contact = new Asi.iBO.ContactManagement.CContact(user, iMIS_ID);

        //    Asi.iBO.ContactManagement.CAddress address = contact.GetAddressByPurpose(AddressPuprose);
        //    if (address == null)
        //    {
        //        address = contact.NewAddress();
        //        address.AddressPurpose = AddressPuprose;
        //    }

        //    address.Address1 = AddressLine1;
        //    address.Address2 = AddressLine2;
        //    address.Address3 = AddressLine3;
        //    address.City = City;
        //    address.StateProvince = Province;
        //    address.PostalCode = PostalCode;
        //    address.Phone = Phone;
        //    address.Fax = Fax;
        //    address.Country = Country;
        //    if (Billing) address.IsPreferredBill = Billing;
        //    if (Mailing) address.IsPreferredMail = Mailing;
        //    if (Shipping) address.IsPreferredShip = Shipping;

        //    contact.Validate();
        //    contact.Save();

        //    //return "";
        //}

        //public bool CheckUserNameIfExists(string username)
        //{
        //    return CContactUser.CheckWebLogin(username);
        //}

        public bool CheckUserEmailExists(string email)
        {
            string dbconn = WebConfigItems.GetConnectionString();

            var db = new clsDB();
            string sql = "SELECT count(Email) AS EmailCounts FROM Name" +
                         " WHERE Email = '" + email + "' ";

            var table = db.GetData(sql);
            if (table != null && table.Rows != null && table.Rows.Count==1)
            {
                var row = table.Rows[0];
                var counts = (int)row["EmailCounts"];
                return (counts >= 1);
            }
            return false;
        }

        //public List<GenClass> GetCountryNames()
        //{
        //    List<GenClass> gen_lkp = new List<GenClass>();

        //    CCountry[] countries = iboAdmin.ReferenceData.Countries;
        //    foreach (CCountry country in countries)
        //    {
        //        GenClass tmp = new GenClass();
        //        tmp.Code = country.CountryName;
        //        tmp.Description = country.CountryName;
        //        gen_lkp.Add(tmp);
        //    }
        //    return gen_lkp;
        //}

        //public List<GenClass> GetStatesProvinces()
        //{
        //    List<GenClass> gen_lkp = new List<GenClass>();

        //    var prefixes = iboAdmin.ReferenceData.StateProvinceLookup;
        //    foreach (var prefix in prefixes)
        //    {
        //        GenClass tmp = new GenClass();
        //        tmp.Code = prefix.Key;
        //        tmp.Description = prefix.Value;
        //        gen_lkp.Add(tmp);
        //    }
        //    return gen_lkp;
        //}

        //public List<GenClass> GetPrefixes()
        //{
        //    List<GenClass> gen_lkp = new List<GenClass>();

        //    string[] prefixes = iboAdmin.ReferenceData.Prefixes;
        //    foreach (string prefix in prefixes)
        //    {
        //        GenClass tmp = new GenClass();
        //        tmp.Code = prefix;
        //        tmp.Description = prefix;
        //        gen_lkp.Add(tmp);
        //    }
        //    return gen_lkp;
        //}

        #endregion
    }
}