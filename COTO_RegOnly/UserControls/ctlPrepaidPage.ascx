﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlPrepaidPage.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlPrepaidPage1" %>
<%@ Import Namespace="COTO_RegOnly.Classes" %>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="RowTitle">
                        <td>
                            <asp:Label ID="lblPersonalInformationSectionTitle" CssClass="heading" runat="server"
                                Text="Annual Registration Renewal Summary" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblMessage" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <%--<asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                Visible="false" ValidationGroup="PersonalValidation" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">
                            <p style="font-weight: bold;">
                                Thank you for completing your annual registration renewal online.
                            </p>
                            <p>
                                Our records indicate that we have received your annual registration fee.<br />
                                If this is incorrect, please contact Clara Lau, Manager Registration at <a href="mailto:clau@coto.org">clau@coto.org</a> or at 416-214-1177 x229.
                            </p>
                            <p>
                                If you wish to print a PDF report of your online annual renewal information please&nbsp;
                                <asp:LinkButton ID="lbtnPrintReport" runat="server" Text="click here" OnClick="lbtnPrintReportClick" /><br />
                                <i>After displaying the annual renewal information report, use the browser back button to return to this page.</i>
                            </p>
                            <p>
                                If you would like to provide feedback with regard to your online registration renewal
                                experience, please&nbsp;<asp:LinkButton ID="lbtnFeedbackLink" runat="server" Text="click here" OnClick="lbtnFeedbackLinkClick" />
                            </p>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>