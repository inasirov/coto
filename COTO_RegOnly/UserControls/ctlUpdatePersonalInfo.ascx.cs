﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlUpdatePersonalInfo : System.Web.UI.UserControl
    {
        #region Consts

        private const string VIEW_STATE_CURRENT_FIRST_NAME = "CurrentFirstName";
        private const string VIEW_STATE_CURRENT_MIDDLE_NAME = "CurrentMiddleName";
        private const string VIEW_STATE_CURRENT_LAST_NAME = "CurrentLastName";

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;
            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }
            if (!IsPostBack)
            {
                var repository = new Repository();

                var user = repository.GetUserPersonalInfo(CurrentUserId);
                if (user != null)
                {
                    //txtLegalLastName.Text = user.LegalLastName;
                    //txtLegalFistName.Text = user.LegalFirstName;
                    //txtPreviousLegalFirstName.Text = user.PreviousLegalFirstName;
                    //txtPreviousLegalLastName.Text = user.PreviousLegalLastName;
                    txtCommonlyUsedFirstName.Text = user.CommonlyUsedFirstName;
                    txtCommonlyUsedLastName.Text = user.CommonlyUsedLastName;
                    txtCommonlyUsedMiddleName.Text = user.CommonlyUsedMiddleName;
                    CurrentFirstName = user.CommonlyUsedFirstName;
                    CurrentMiddleName = user.CommonlyUsedMiddleName;
                    CurrentLastName = user.CommonlyUsedLastName;
                }
            }
        }

        protected void ibtnLegalLastNameHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("The name provided here must be the same name that appears on your documentation to demonstrate citizenship " + 
            "or work permit. You do not necessarily have to use your full legal name in your professional practice. ", "Legal Name");
        }


        protected void ibtnPreviousLegalLastNameHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("This is the name that appears on your OT entry-level degree. Provide this information if it is not the name " +
            "you are currently using in practice. ", "Previous Legal Name");
        }

        protected void ibtnCommonlyUsedFirstNameHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("This is the name currently on file and appearing on the Public Register. The name on the Public Register " + 
                "must be the name that you use in your professional practice. It does not necessarily have to be your full legal name. " + 
                "Changes to the name appearing here must be done by email or fax requesting a name change. ", "Commonly Used Name");
            
        }

        protected void btnSubmitClick(object sender, ImageClickEventArgs e)
        {
            if (CurrentFirstName == txtCommonlyUsedFirstName.Text && CurrentMiddleName == txtCommonlyUsedMiddleName.Text && CurrentLastName == txtCommonlyUsedLastName.Text)
            {
                lblMessage.Text = "You have not changed any of your name information. You can either enter your information and click the \"Send\" button again, or click the \"Back\" button to return to the previous page.";
                return;
            }

            try
            {
                var repository = new Repository();
                var user2 = repository.GetUserInfo(CurrentUserId);
                string message = string.Format("Member ID: {0}<br />", CurrentUserId);
                message += string.Format("Full Name: {0}<br /><br />", user2.FullName);
                //message += string.Format("Legal First Name: {0}<br />", txtLegalFistName.Text);
                //message += string.Format("Legal Middle Name: {0}<br />", txtLegalMiddleName.Text);
                //message += string.Format("Legal Last Name: {0}<br />", txtLegalLastName.Text);

                //message += string.Format("Previous Legal First Name: {0}<br />", txtPreviousLegalFirstName.Text);
                //message += string.Format("Previous Legal Last Name: {0}<br />", txtPreviousLegalLastName.Text);

                message += string.Format("Requested Commonly Used First Name: {0}<br />", txtCommonlyUsedFirstName.Text);
                message += string.Format("Requested Commonly Used Middle Name: {0}<br />", txtCommonlyUsedMiddleName.Text);
                message += string.Format("Requested Commonly Used Last Name: {0}<br />", txtCommonlyUsedLastName.Text);
                message += string.Format("Date: {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

                string emailSubject = string.Format("Update Name Information for {0} ({1}) {2}", user2.FullName, CurrentUserId, WebConfigItems.GetTestLabel);
                string emailTo = WebConfigItems.RegistrationRegistrationContactEmail;
                var tool = new Tools();
                tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);
            }
            catch (Exception ex)
            {
                string message = string.Format("Message : {0}, innerExeption: {1}", ex.Message, ex.InnerException);
                string emailTo = "inasirov@visualantidote.com";
                var tool = new Tools();
                tool.SendConfirmationEmail(CurrentUserId, message, "Error message", emailTo, string.Empty);
            }
            
            Response.Redirect(string.Format("{0}?common=updated", WebConfigItems.Step1));
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(WebConfigItems.Step1);
        }

        #endregion

        #region Methods

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public string CurrentFirstName
        {
            get
            {
                if (ViewState[VIEW_STATE_CURRENT_FIRST_NAME] != null)
                {
                    return (string)ViewState[VIEW_STATE_CURRENT_FIRST_NAME];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState[VIEW_STATE_CURRENT_FIRST_NAME] = value;
            }
        }

        public string CurrentMiddleName
        {
            get
            {
                if (ViewState[VIEW_STATE_CURRENT_MIDDLE_NAME] != null)
                {
                    return (string)ViewState[VIEW_STATE_CURRENT_MIDDLE_NAME];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState[VIEW_STATE_CURRENT_MIDDLE_NAME] = value;
            }
        }

        public string CurrentLastName
        {
            get
            {
                if (ViewState[VIEW_STATE_CURRENT_LAST_NAME] != null)
                {
                    return (string)ViewState[VIEW_STATE_CURRENT_LAST_NAME];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState[VIEW_STATE_CURRENT_LAST_NAME] = value;
            }
        }
        #endregion
    }
}