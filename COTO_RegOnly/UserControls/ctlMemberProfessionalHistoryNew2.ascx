﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberProfessionalHistoryNew2.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlMemberProfessionalHistoryNew2" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>

    
    <style type="text/css">
        body
        {
            font-family: Arial, Helvetica, sans-serif;
            /*font-size: 13px;*/
        }
        .errMessage
        {
            width:300px;
            border: 1px solid;
            margin: 10px 0px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            position: relative;
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('images/info.png');
        }
        .modalPopup6
        {
            -moz-box-shadow: 0 0 5px #000000;
            -webkit-box-shadow: 0 0 5px #000000;
            box-shadow: 0 0 5px #000000;
            padding: 0px;
            background-color: White;
            border-width: 1px;
            -moz-border-radius: 1px;
            border-style: solid;
            border-color: Black;
            width: 500px;
            z-index: 4001 !important;
        }
        .modalPopup6Background {
            z-index: 4000 !important;
        }
        .modalPopup{
            z-index: 6001 !important;
        }
        .modalBackground {
            z-index: 6000 !important;
        }
        .modalOK
        {
            background-color: #9B9B9B;
            color: White;
            font-size: 14px;
            font-family: Arial,sans-serif;
            border-style: none;
            -moz-box-shadow: 1px 1px 2px #000000;
            -webkit-box-shadow: 1px 1px 2px #000000;
            box-shadow: 1px 1px 2px #000000;
            padding: 3px;
            border-radius: 3px;
            width: 100px;
            height: 30px;
            white-space: normal;
            vertical-align: top;
        }
        .modalCancel
        {
            color: rgb(61, 54, 40);
            border-color: rgb(113, 113, 113);
            border-style: none;
            border-radius: 3px;
            background-color: rgb(255, 255, 255);
            font-size: 14px;
            font-family: Arial,sans-serif;
            -moz-box-shadow: 1px 1px 2px #000000;
            -webkit-box-shadow: 1px 1px 2px #000000;
            box-shadow: 1px 1px 2px #000000;
            padding: 3px;
            width: 100px;
            height: 30px;
            white-space: normal;
            vertical-align: top;
        }
    
        .empRow
    {
        border-color: gray;
        border-style: inset;
        vertical-align: top;
        padding: 2px;
    }
    .empRowH
    {
        border-color: gray;
        border-style: inset;
        background-color: #EFEFDE;
        padding: 2px;
    }
    .eBorderRight
    {
        border-right: 1px;
    }
    .eBorderLeft
    {
        border-left: 1px;
    }
    .eBorderTop
    {
        border-top: 1px;
    }
    .eBorderBottom
    {
        border-bottom: 1px;
    }
    .eBorderRightDis
    {
        border-right: 0px;
    }
    .eBorderLeftDis
    {
        border-left: 0px;
    }
    .eBorderTopDis
    {
        border-top: 0px;
    }
    .eBorderBottomDis
    {
        border-bottom: 0px;
    }
     .EN12
    {
        color: #000000;
        font-family: Verdana,Arial,Helvetica,sans-serif;
        font-size: 10pt;
    }
    .eRegStrong
    {
        font-size: 10pt;
        font-weight: bold;
    }
     /* Table hover style */
    table.AltRow tr th
    {
        background-color: #dadada;
        font-weight: bold;
    }
    
    /* Table odd rows style */
    table.AltRow tr:nth-child(odd)
    {
        background-color: #efefef;
    }
    
    /* Table even rows style */
    table.AltRow tr:nth-child(even)
    {
        
        background-color: #ffffff;
    }
    
    /* Table hover style */
    table.AltRow tr:hover
    {
        background-color: #dadada;
    }
        .auto-style1 {
            height: 22px;
        }


    .modalPopup6 table tr td
    {
        padding: 3px;
    }
    #ctl00_TemplateBody_ctlMemberProfessionalHistory1_ceExpiryDate_daysTable  tr td{
        padding: 0px;
    }

    button[disabled]:active, button[disabled],
    input[type="reset"][disabled]:active,
    input[type="reset"][disabled],
    input[type="button"][disabled]:active,
    input[type="button"][disabled],
    select[disabled] > input[type="button"],
    select[disabled] > input[type="button"]:active,
    select[disabled],
    input[type="submit"][disabled]:active,
    input[type="submit"][disabled] {
        padding: 0px 6px 0px 6px;
        /*border: 2px outset ButtonFace;*/
        color: GrayText!important;
        cursor: inherit;
        background-color: lightgray;
    }


    </style>

<script type="text/javascript">
    function Confirmation() {
        if (confirm("Are you sure you want to delete this record?")) {
            return true;
        }
        return false;
    }
</script>
    
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="2" width="100%">
                    <tr class="HeaderTitle">
                        <td class="HeaderTitle" style="text-align:right;">
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 8 of 12" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right;">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr class="RowTitle">
                        <td>
                            <div>
                                <asp:Label ID="lblPersonalEmploymentInformationTitle" runat="server" CssClass="heading"
                                Text="Professional Registration" />
                            </div>
                            
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; font-weight:bold; padding-top: 10px; padding-bottom: 10px;">
                            <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/2021/2021_Annual_Renewal_Glossary_S8_OccupationalTherapyPractice_English.pdf" target="_blank">Glossary</a>&nbsp;/&nbsp;
                            <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/2021/2021_Annual_Renewal_Glossary_S8_OccupationalTherapyPractice_French.pdf" target="_blank">Glossaire</a>
                        </td>
                    </tr>
                    <%--<tr>
                        <td style="text-align: left;">
                                <asp:Label ID="lblAdditionalDetailsDescription" runat="server" Text="For additional details about a field please click" />&nbsp;
                                <asp:Image ID="imgQuestionMark" runat="server" ImageUrl="~/Images/qmark.jpg" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            <table class="memberInfo" style="width: 100%;">
                                <%--<tr>
                                    <td style="text-align: left;" colspan="2">
                                        <p>
                                            <asp:Label ID="lblDenotesRequiredTitle" runat="server"  Text="* denotes required field" />
                                        </p>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td style="text-align: left; padding-top: 10px!important; padding-bottom: 10px!important;" colspan="2">
                                        <span style="font-weight: bold; font-size: larger;">Occupational Therapy Registration Outside of Ontario</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblPracticeOutsideOntario" runat="server" Text="Have you ever practised outside of Ontario?" />
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:DropDownList ID="ddlOutsideOntarioPractice" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlOutsideOntarioPracticeSelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="rfvOutsideOntarioPractice" runat="server" ControlToValidate="ddlOutsideOntarioPractice" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please select &quot;Yes&quot; or &quot;No&quot;" ForeColor="Red" InitialValue="" ValidationGroup="GeneralValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                                <%--<tr id="trOutsideON1" runat="server" visible="false">
                                    <td colspan="2" style="text-align: left;">
                                        <span style="font-weight: bold; font-size: larger; ">Most Recent Non-Ontario Practice Information</span>
                                    </td>
                                </tr>--%>
                                <tr id="trOutsideON2" runat="server" visible="false">
                                    <td class="RightColumn">
                                        <asp:Label ID="lblLastYearOutsideOntarioTitle" runat="server" Text="What is the &lt;b&gt;most recent year&lt;/b&gt; in which you practiced OT &lt;b&gt;outside of Ontario&lt;/b&gt;?" />
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:DropDownList ID="ddlLastYearOutsideOntario" runat="server">
                                        </asp:DropDownList>
                                        &nbsp;
                                        <asp:RequiredFieldValidator ID="rfvLastYearOutsideOntario" runat="server" ControlToValidate="ddlLastYearOutsideOntario" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please provide your last year of practise in jurisdiction outside of Ontario" ForeColor="Red" InitialValue="" ValidationGroup="GeneralValidation" />
                                    </td>
                                </tr>
                                <tr id="trOutsideON3" runat="server" visible="false">
                                    <td colspan="2" class="auto-style1"></td>
                                </tr>
                                <tr id="trOutsideON4" runat="server" visible="false">
                                    <td class="RightColumn">
                                        <asp:Label ID="lblLastCountryOTPractice" runat="server" Text="What is the most &lt;b&gt;recent country&lt;/b&gt; in which you &lt;b&gt;practiced OT&lt;/b&gt; outside of Ontario?" />
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:DropDownList ID="ddlLastCountryOTPractice" runat="server" OnSelectedIndexChanged="ddlLastCountryOTPractice_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                        &nbsp;
                                        <asp:RequiredFieldValidator ID="rfvLastCountryOTPractice" runat="server" ControlToValidate="ddlLastCountryOTPractice" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please provide your last country of OT Practice" ForeColor="Red" InitialValue="" ValidationGroup="GeneralValidation" />
                                    </td>
                                </tr>
                                <tr id="trOutsideON5" runat="server" visible="false">
                                    <td colspan="2"></td>
                                </tr>
                                <tr id="trOutsideON6" runat="server" visible="false">
                                    <td class="RightColumn">
                                        <asp:Label ID="lblLastStateTitle" runat="server" Text="Last Canadian province/territory or US state of OT practice (Canada or US only)" />
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:DropDownList ID="ddlLastStateOrCanadianProvince" runat="server">
                                        </asp:DropDownList>
                                        &nbsp;
                                        <asp:RequiredFieldValidator ID="rfvLastStateOrCanadianProvince" runat="server" ControlToValidate="ddlLastStateOrCanadianProvince" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please provide your last province/territory/state of OT Practice" ForeColor="Red" InitialValue="" ValidationGroup="GeneralValidation" />
                                    </td>
                                </tr>
                                <tr id="trOutsideON7" runat="server" visible="false">
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left; padding: 8px;">
                                        <span style="font-weight: bold; font-size: larger; ">Other Occupational Therapy Registration</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;" colspan="2">
                                        <p>Details about your registration, membership or licensure with any occupational therapy regulatory body outside of Ontario are required. 
                                            Please provide the required information, or make updates to the required fields. </p>
                                        <p>Information on professional memberships is not permitted (e.g. OSOT, CAOT, etc.).</p>
                                        <p>If you hold registration with a regulatory body that is not linked to any one state or province indicate where 
                                            the regulatory body holds an office or choose "not applicable". 
                                        </p>
                                        <p><strong>If you do not see your OT regulatory body on the list please send us an email</strong> at 
                                            <a href="mailto: registration@coto.org">registration@coto.org</a>. In the email, be sure to include your name, 
                                            the name of the regulatory body and the country where you are registered/licensed.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn" colspan="2">
                                        <asp:ListView ID="lvJurisdictions" runat="server" OnItemCommand="lvJurisdictionsCommand">
                                            <LayoutTemplate>
                                                <table style="width: 100%; text-align: left; padding: 3px;" class="AltRow">
                                                    <tr style="border: 1px solid black;">
                                                        <td class="empRowH eBorderLeft eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 22%">
                                                            <span class="eRegStrong">Regulatory Body</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 12%" >
                                                            <span class="eRegStrong">Country</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 12%" >
                                                            <span class="eRegStrong">Province/State</span>
                                                        </td>
                                                        
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 10%" >
                                                            <span class="eRegStrong">Status</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: center; vertical-align: top; width: 10%" >
                                                            <span class="eRegStrong">Expiry Date</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRight eBorderTop eBorderBottom" style=" text-align: center; vertical-align: top; width: 10%">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr id="ItemPlaceHolder" runat="server" />
                                                    <tr style="border: 1px solid black;">
                                                        <td colspan="6" class="empRowH eBorderLeftDis eBorderRight eBorderTop eBorderBottom" style=" text-align: right;">
                                                            <asp:LinkButton ID="lbtnNew" runat="server" Text="Add Registration" OnClick="lbtnNewClick" />&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </LayoutTemplate>
                                            <EmptyDataTemplate>
                                                <table style="width: 100%; text-align: left; padding: 3px;">
                                                    <tr>
                                                        <td class="empRowH eBorderLeft eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 22%">
                                                            <span class="eRegStrong">Regulatory Body</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 12%" >
                                                            <span class="eRegStrong">Country</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 12%" >
                                                            <span class="eRegStrong">Province/State</span>
                                                        </td>
                                                        
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 10%" >
                                                            <span class="eRegStrong">Status</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: center; vertical-align: top; width: 10%" >
                                                            <span class="eRegStrong">Expiry Date</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRight eBorderTop eBorderBottom" style=" text-align: center; vertical-align: top; width: 10%">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9">
                                                            <span>No Records</span>
                                                        </td>
                                                    </tr>
                                                    <tr style="border: 1px solid black;">
                                                        <td colspan="6" class="empRowH eBorderLeftDis eBorderRight eBorderTop eBorderBottom" style=" text-align: right;">
                                                            <asp:LinkButton ID="lbtnNew" runat="server" Text="Add Registration" OnClick="lbtnNewClick" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EmptyDataTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="empRow eBorderLeft eBorderRightDis eBorderTopDis eBorderBottomDis">
                                                        <asp:Label ID="lblRegBody" runat="server" Text='<%# (string)Eval("RegulatoryBodyDescription") %>' />
                                                        <asp:HiddenField ID="hfSeqn" runat="server" Value='<%# ((int)Eval("SEQN")).ToString() %>' />
                                                    </td>
                                                    <td class="empRow eBorderLeft eBorderRightDis eBorderTopDis eBorderBottomDis">
                                                        <asp:Label ID="lblCountry" runat="server" Text='<%# (string)Eval("CountryDescription") %>' />
                                                    </td>
                                                    <td class="empRow eBorderLeft eBorderRightDis eBorderTopDis eBorderBottomDis">
                                                        <asp:Label ID="lblProvince" runat="server" Text='<%# (string)Eval("ProvinceDescription") %>' />
                                                    </td>
                                                    
                                                    <td class="empRow eBorderLeft eBorderRightDis eBorderTopDis eBorderBottomDis">
                                                        <asp:Label ID="lblRegStatus" runat="server" Text='<%# (string)Eval("RegStatus") %>' />
                                                    </td>
                                                    <td class="empRow eBorderLeft eBorderRightDis eBorderTopDis eBorderBottomDis" style="text-align: center;">
                                                        <asp:Label ID="lblExpiryDateDate" runat="server" Text='<%# (DateTime)Eval("ExpiryDate") != DateTime.MinValue ? ((DateTime)Eval("ExpiryDate")).ToString("MM/dd/yyyy")  : string.Empty %>'/>
                                                    </td>
                                                    <td class="empRow eBorderLeft eBorderRight eBorderTopDis eBorderBottomDis" style="text-align: center;">
                                                        <asp:LinkButton ID="lbtnUpdate" runat="server" Text="Update" CommandName="EditRecord" CommandArgument='<%# (int)Eval("Seqn") %>' />&nbsp;
                                                        <%--<asp:LinkButton ID="lbtnRemove" runat="server" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this record?');" CommandName="Remove" CommandArgument='<%# (int)Eval("Seqn") %>' />--%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; padding: 8px;">
                            <span style="font-weight: bold; font-size: larger;">Other Professional Registration</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="RightColumn">
                            <p>
                               Details about your registration, membership or licensure in other regulated professions in Canada or elsewhere are required. 
                                Please provide the required information, or make updates to the required fields. Examples of other regulated professions include lawyers, 
                                kinesiologists, social workers, and engineers. For more examples, please see the drop down menu. 
                            </p>
                            <p><strong>If you do not see your profession on the list please send us an email</strong> at 
                                            <a href="mailto: registration@coto.org">registration@coto.org</a>. In the email, be sure to include your name and
                                            the name of the profession, regulatory body and country where you are registered/licensed.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" style="width: 100%;">
                                <tr>
                                    <td class="RightColumn">
                                        <asp:ListView ID="lvOtherProfessions" runat="server" OnItemCommand="lvOtherProfessionsCommand">
                                            <LayoutTemplate>
                                                <table style="width: 100%; text-align: left; padding: 3px;" class="AltRow">
                                                    <tr style="border: 1px solid black;">
                                                        <td class="empRowH eBorderLeft eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 25%">
                                                            <span class="eRegStrong">Profession</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 15%" >
                                                            <span class="eRegStrong">Country</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 15%" >
                                                            <span class="eRegStrong">Province/State</span>
                                                        </td>
                                                        
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 15%" >
                                                            <span class="eRegStrong">Status</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: center; vertical-align: top; width: 15%" >
                                                            <span class="eRegStrong">Expiry Date</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRight eBorderTop eBorderBottom" style=" text-align: center; vertical-align: top; width: 15%">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr id="ItemPlaceHolder" runat="server" />
                                                    <tr  style="border: 1px solid black;">
                                                        <td  colspan="6" class="empRowH eBorderLeftDis eBorderRight eBorderTop eBorderBottom" style=" text-align: right; padding-right: 15px;">
                                                            <asp:LinkButton ID="lbtnNew" runat="server" Text="Add Registration" OnClick="lbtnNew2Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </LayoutTemplate>
                                            <EmptyDataTemplate>
                                                <table style="width: 100%; text-align: left; padding: 3px;">
                                                    <tr>
                                                        <td class="empRowH eBorderLeft eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 25%">
                                                            <span class="eRegStrong">Name of Profession</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 12%" >
                                                            <span class="eRegStrong">Country</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 12%" >
                                                            <span class="eRegStrong">Province/State</span>
                                                        </td>
                                                        
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: left; vertical-align: top; width: 12%" >
                                                            <span class="eRegStrong">Status</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRightDis eBorderTop eBorderBottom" style=" text-align: center; vertical-align: top; width: 12%" >
                                                            <span class="eRegStrong">Expiry Date</span>
                                                        </td>
                                                        <td class="empRowH eBorderLeftDis eBorderRight eBorderTop eBorderBottom" style=" text-align: center; vertical-align: top; width: 12%">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6">
                                                            <span>No Records</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="empRowH eBorderLeftDis eBorderRight eBorderTop eBorderBottom" style=" text-align: right; padding-right: 15px;">
                                                            <asp:LinkButton ID="lbtnNew" runat="server" Text="Add Registration" OnClick="lbtnNew2Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EmptyDataTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="empRow eBorderLeft eBorderRightDis eBorderTopDis eBorderBottomDis">
                                                        <asp:Label ID="lblRegBody" runat="server" Text='<%# (string)Eval("OtherProfessionTypeDescription") %>' />
                                                        <asp:HiddenField ID="hfSeqn" runat="server" Value='<%# ((int)Eval("SEQN")).ToString() %>' />
                                                    </td>
                                                    <td class="empRow eBorderLeft eBorderRightDis eBorderTopDis eBorderBottomDis">
                                                        <asp:Label ID="lblCountry" runat="server" Text='<%# (string)Eval("CountryDescription") %>' />
                                                    </td>
                                                    <td class="empRow eBorderLeft eBorderRightDis eBorderTopDis eBorderBottomDis">
                                                        <asp:Label ID="lblProvince" runat="server" Text='<%# (string)Eval("ProvinceDescription") %>' />
                                                    </td>
                                                    <td class="empRow eBorderLeft eBorderRightDis eBorderTopDis eBorderBottomDis">
                                                        <asp:Label ID="lblRegStatus" runat="server" Text='<%# (string)Eval("RegStatus") %>' />
                                                    </td>
                                                    <td class="empRow eBorderLeft eBorderRightDis eBorderTopDis eBorderBottomDis" style="text-align: center;">
                                                        <asp:Label ID="lblExpiryDateDate" runat="server" Text='<%# (DateTime)Eval("ExpiryDate") != DateTime.MinValue ? ((DateTime)Eval("ExpiryDate")).ToString("MM/dd/yyyy")  : string.Empty %>'/>
                                                    </td>
                                                    <td class="empRow eBorderLeft eBorderRight eBorderTopDis eBorderBottomDis" style="text-align: center;">
                                                        <asp:LinkButton ID="lbtnUpdate" runat="server" Text="Update" CommandName="EditRecord" CommandArgument='<%# (int)Eval("Seqn") %>' />&nbsp;
                                                        <%--<asp:LinkButton ID="lbtnRemove" runat="server" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this record?');" CommandName="Remove" CommandArgument='<%# (int)Eval("Seqn") %>' />--%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDebugMessage" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right; padding: 10px;">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" UseSubmitBehavior="true" TabIndex="0"/>--%>
                        </td>
                    </tr>
                </table>

                <ajaxToolkit:ModalPopupExtender ID="mpext6" runat="server" BackgroundCssClass="modalPopup6Background"
                    TargetControlID="dummy2" PopupControlID="pnlPopup6">
                </ajaxToolkit:ModalPopupExtender>
                <asp:HiddenField ID="dummy2" runat="server"  />  <%----%>
                <asp:Panel ID="pnlPopup6" runat="server" CssClass="modalPopup6" Style="display: none;">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
                        <ContentTemplate>
                            <table style="width: 100%; padding: 2px; border-spacing: 4px;">
                                <tr>
                                    <td class="EN12" style="text-align:left; border-width: 0 0 1px; background-color: #EFEFDE; border-style: inset;" colspan="2" >
                                        <asp:Label ID="lblPopupTitle" runat="server" CssClass="EH3plain" Text ="Add/Edit Record" />
                                    </td> 
                                </tr>
                                <tr id="trRegBody" runat="server">
                                    <td class="LeftTitle" style="width: 35%">
                                        <asp:Label ID="lblRegBody1Title" runat="server" Text="Regulatory Body " />
                                        <%--<asp:ImageButton ID="ibRegulatoryBody" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibRegulatoryBodyHelpClick" />--%>
                                    </td>
                                    <td class="auto-style2">
                                        <asp:DropDownList ID="ddlRegBody" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRegBody_SelectedIndexChanged" style="max-width: 310px" />
                                        <asp:RequiredFieldValidator ID="rfvRegBody" runat="server" ControlToValidate="ddlRegBody"
                                            ValidationGroup="JurisdictionValidation" ErrorMessage="<br />Regulatory Body is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trRegBodyOther" runat="server" visible="false">
                                    <td class="LeftTitle" style="width: 35%">
                                        <asp:Label ID="lblRegBodyOtherTitle" runat="server" Text="Regulatory Body Other " />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtRegBodyOther" runat="server" TextMode="MultiLine" Rows="3" Columns="40" />
                                    </td>
                                </tr>
                                <tr id="trNameProfession" runat="server">
                                    <td class="LeftTitle" style="width: 35%">
                                        <asp:Label ID="lblNameProfession" runat="server" Text="Name of Profession" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlNameProfession" runat="server" OnSelectedIndexChanged="ddlNameProfession_SelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvNameProfession" runat="server" ControlToValidate="ddlNameProfession"
                                            ValidationGroup="JurisdictionValidation" ErrorMessage="<br />Name of Profession is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trNameProfessionOther" runat="server" visible="false">
                                    <td class="LeftTitle" style="width: 35%">
                                        <asp:Label ID="lblNameProfessionOther" runat="server" Text="Name of Profession Other" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtNameProfessionOther" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvNameProfessionOther" runat="server" ControlToValidate="txtNameProfessionOther"
                                            ValidationGroup="JurisdictionValidation" ErrorMessage="<br />Name Profession Other is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trOtherRegBody" runat="server">
                                    <td class="LeftTitle" style="width: 35%">
                                        <asp:Label ID="lblOtherRegBodyOP1" runat="server" Text="Regulatory Body" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlOtherRegBody" runat="server" OnSelectedIndexChanged="ddlOtherRegBody_SelectedIndexChanged" AutoPostBack="true" style="max-width: 310px" />
                                        <asp:RequiredFieldValidator ID="rfvOtherRegBody" runat="server" ControlToValidate="ddlOtherRegBody"
                                            ValidationGroup="JurisdictionValidation" ErrorMessage="<br />Regulatory Body is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trOtherRegBodyOther" runat="server" visible="false">
                                    <td class="LeftTitle" style="width: 35%">
                                        <asp:Label ID="lblOtherRegBodyOtherTitle" runat="server" Text="Regulatory Body Other" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtOtherRegBodyOther" runat="server" TextMode="MultiLine" Rows="3" Columns="40"/>
                                        <asp:RequiredFieldValidator ID="rfvOtherRegBodyOther" runat="server" ControlToValidate="txtOtherRegBodyOther"
                                            ValidationGroup="JurisdictionValidation" ErrorMessage="<br />Regulatory Body Other is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftTitle" style="width: 35%">
                                        <asp:Label ID="lblProvinceStateLabel" runat="server" Text="Province/State" />&nbsp;
                                        &nbsp;
                                        <%--<asp:ImageButton ID="ibProvinceState" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibProvinceStateHelpClick" />--%>
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlProvince" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvProvince" runat="server" ControlToValidate="ddlProvince"
                                            InitialValue="" ValidationGroup="JurisdictionValidation" ErrorMessage="<br />Province is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftTitle" style="width: 35%">
                                        <asp:Label ID="lblCountryLabel" runat="server" Text="Country" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlCountry" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry"
                                            InitialValue="" ValidationGroup="JurisdictionValidation" ErrorMessage="<br />Country is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftTitle">
                                        <asp:Label ID="lblLicenseLabel" runat="server" Text="Registration/License #" />&nbsp;
                                    &nbsp;
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtLicense" runat="server" Columns="20" MaxLength="30" />
                                        <asp:RequiredFieldValidator ID="rfvLicense" runat="server" ControlToValidate="txtLicense" 
                                            ValidationGroup="JurisdictionValidation" ErrorMessage="<br />Registration/License # is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftTitle">
                                        <asp:Label ID="lblRegStatusTitle" runat="server" Text="Registration Status" />&nbsp;
                                    &nbsp;
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtRegStatus" runat="server" Columns="20" MaxLength="200" />
                                        <asp:RequiredFieldValidator ID="rfvRegStatus" runat="server" ControlToValidate="txtRegStatus" 
                                            ValidationGroup="JurisdictionValidation" ErrorMessage="<br />Registration status is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftTitle" style="width: 35%">
                                        <asp:Label ID="lblInitMonthTitle" runat="server" Text="Initial Month of Registration" />&nbsp;
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlInitMonth" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvInitMonth" runat="server" ControlToValidate="ddlInitMonth"
                                            InitialValue="" ValidationGroup="JurisdictionValidation" ErrorMessage="<br />Initial Month is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftTitle" style="width: 35%">
                                        <asp:Label ID="lblInitYearTitle" runat="server" Text="Initial Year of Registration" />&nbsp;
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlInitYear" runat="server"/>
                                        <asp:RequiredFieldValidator ID="rfvInitYear" runat="server" ControlToValidate="ddlInitYear"
                                            InitialValue="" ValidationGroup="JurisdictionValidation" ErrorMessage="<br />Initial Year is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftTitle">
                                        <asp:Label ID="lblExpiryDate1Labe" runat="server" Text="Expiry Date (MM/DD/YYYY)" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtExpiryDate" runat="server" OnTextChanged="txtExpiryDate_TextChanged" AutoPostBack="true" />
                                        <ajaxToolkit:CalendarExtender ID="ceExpiryDate" runat="server" TargetControlID="txtExpiryDate" Format="MM/dd/yyyy"  />
                                        <%--<rjs:PopCalendar ID="PopCalendar1" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy" Format="dd/mm/yyyy"
                                            Separator="/" Control="txtExpiryDate"  From-Date="1950-01-01" MessageAlignment="RightCalendarControl" OnSelectionChanged="PopCalendar1_SelectionChanged" />--%>
                                        &nbsp;
                                        <asp:CheckBox ID="cbNoExpiration" runat="server" Text="No Expiration" OnCheckedChanged="cbNoExpirationCheckedChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvExpiryDate" runat="server" ControlToValidate="txtExpiryDate" 
                                            ValidationGroup="JurisdictionValidation" ErrorMessage="<br />Expiry Date is blank." Display="Dynamic" ForeColor="Red" />
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td class="EN12" style="text-align:center; padding: 10px;" colspan="2" >
                                        <asp:HiddenField ID="hfPracticeSeqn" runat="server" />&nbsp;
                                        <asp:HiddenField ID="hfRegType" runat="server" />&nbsp;
                                        <asp:Button ID="btnSave" runat="server" Text="Submit" OnClick="btnSaveClick" class="modalOK"  ValidationGroup="JurisdictionValidation"/>&nbsp;
                                        <asp:Button ID="btnEditCancel" runat="server" Text="Cancel" class="modalCancel" OnClick="btnEditCancelCancelClick" />
                                    </td> 
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
