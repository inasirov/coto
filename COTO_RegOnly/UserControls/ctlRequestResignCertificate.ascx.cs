﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using Classes;
using COTO_RegOnly.Classes;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlRequestResignCertificate : System.Web.UI.UserControl
    {
        private string _Key = "Yz7!~3";


        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 1)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack)
            {
                BindLists();
                BindData();
            }
        }

        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            if (ValidationCheck())
            {
                UpdateUserInfo();

            }
            else
            {
                if (Messages != null && Messages.Count > 0)
                {
                    string message = string.Empty;
                    foreach (var item in Messages)
                    {
                        message += ((PageMessage)item).Message + "<br />";
                    }
                    message = string.Format("<font color='Red'>{0}</font>", message);

                    Messages.Clear();
                    ShowErrorMessage(message);
                }
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(CurrentReturnPageUrl);
        }

        protected void ibStatus1HelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("This represents your status for all OT employment.", "Status");
        }
        #endregion

        #region Methods

        protected void ShowErrorMessage(string Message)
        {
            lblErrorMessage.Text = Message;
            update.Update();
            modalPopupEx.Show();
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        /// <summary>
        ///   Allow developer adds custom errors, warning into Messages Collection.
        /// </summary>
        /// <param name="message">Message information.</param>
        /// <param name="messageType">Message type.</param>
        public void AddMessage(string message, PageMessageType messageType, string returnURL)
        {
            SessionParameters.ReturnUrl = returnURL;
            this.Messages.Add(new PageMessage(message, messageType));
        }

        protected bool ValidationCheck()
        {
            lblErrorMessage.Text = string.Empty;

            Page.Validate("GeneralValidation");
            bool valid = true;
            if (!cbConfirm.Checked)
            {
                AddMessage("Please check the box indicating your wish to cancel or resign.", PageMessageType.UserError, string.Empty);
                valid = false;
            }

            CultureInfo provider = CultureInfo.InvariantCulture;
            DateTime resignDate = DateTime.ParseExact(txtLastDayEmployment.Text, "MM/dd/yyyy", provider);
            //DateTime effectiveDate = new DateTime(DateTime.Now.Year, 6, 1).AddDays(-1);

            //DateTime maxResignationDate = new DateTime(2015, 05, 31);

            //if (trEffectiveDate.Visible)
            //{
            DateTime effectiveDate = DateTime.ParseExact(txtEffectiveDate.Text, "MM/dd/yyyy", provider);

            if (resignDate > effectiveDate)
            {
                AddMessage("Last day of OT Employment cannot be greater than Effective day", PageMessageType.UserError, string.Empty);
                valid = false;
            }

            //if (resignDate > effectiveDate)
            //{
            //    AddMessage("Last day of OT Employment cannot be greater than Effective day", PageMessageType.UserError, string.Empty);
            //    valid = false;
            //}

            // the check is no longer valid after passes May 31, 2014
            //if (resignDate > maxResignationDate || effectiveDate > maxResignationDate)
            //{
            //    AddMessage("If you will be working past May 31, 2014 you must renew your certificate of registration", PageMessageType.UserError, string.Empty);
            //    valid = false;
            //}


            if (!Page.IsValid || !valid)
            {
                //update.Update();
                //modalPopupEx.Show();
                return false;
            }
            return true;
        }

        protected void BindLists()
        {
            var repository = new Repository();
            var list1 = repository.GetGeneralList("INACTIVE_REASONS");

            ddlReasons.DataSource = list1;
            ddlReasons.DataValueField = "Code";
            ddlReasons.DataTextField = "Description";
            ddlReasons.DataBind();
            ddlReasons.Items.Insert(0, string.Empty);
        }
        protected void BindData()
        {
            //DateTime newDate = new DateTime(DateTime.Today.Year, 5, 31);
            DateTime newDate = DateTime.Today;
            DateTime newDate2 = new DateTime(2018, 5, 31);

            //if (DateTime.Today > newDate)
            //{
            //    newDate = newDate.AddYears(1);
            //    newDate2 = newDate2.AddYears(1);
            //}
            //lblConfrimText.Text = string.Format(lblConfrimText.Text, newDate.ToString("MMMM dd, yyyy"));
            //if (DateTime.Today.Month <= 5)
            //{
            //  trEffectiveDate.Visible = false;
            //}
            txtEffectiveDate.Text = newDate.ToString("MM/dd/yyyy");

            //PopCalendar1.From.Date = newDate; //DateTime.Now.AddYears(4);
            PopCalendar2.SelectedDate = newDate.ToString("MM/dd/yyyy");  //(new DateTime((int.Parse(HardcodedValues.RenewalYear)), 5, 31)).ToString("dd/MM/yyyy");
            PopCalendar2.To.Date = newDate2; //DateTime.Now.AddYears(4);
            PopCalendar2.From.Date = newDate;
            //PopCalendar2.From.Date = newDate2;
            
        }

        protected void UpdateUserInfo()
        {
            string message2 = string.Empty;
            try
            {
                // update user info
                var repository = new Repository();
                CultureInfo provider = CultureInfo.InvariantCulture;
                DateTime resignDate = DateTime.ParseExact(txtLastDayEmployment.Text, "MM/dd/yyyy", provider);
                //DateTime effectiveDate = new DateTime(DateTime.Now.Year, 6, 1).AddDays(-1);

                //if (trEffectiveDate.Visible)
                //{
                DateTime effectiveDate = DateTime.ParseExact(txtEffectiveDate.Text, "MM/dd/yyyy", provider);
                //}
                
                //ShowErrorMessage( resignDate.ToString("dd/MM/yyyy"));

                repository.UpdateUserCurrencyResignCertificateLogged(CurrentUserId, cbConfirm.Checked, resignDate, ddlReasons.SelectedValue, effectiveDate);
                // send confirmation email

                DateTime newDate = new DateTime(DateTime.Today.Year, 5, 31);
                if (DateTime.Today.Month > 5)
                {
                    newDate = newDate.AddYears(1);
                }

                var user = repository.GetUserGeneralInfo(CurrentUserId);
                if (user != null)
                {
                    //string message = "This email is to confirm that the College has received your request to resign your Certificate of Registration, " +
                    //" which will become effective ";
                    //if (trEffectiveDate.Visible)
                    //{
                    //    message += effectiveDate.ToString("MMM dd, yyyy") + ". The College";
                    //}
                    //else
                    //{
                    //    message += " May 31, " + HardcodedValues.RenewalYear.ToString() + ".  In early June, the College";
                    //}
                    //message +=  " will follow-up with further confirmation of your change in status. <br><br>";
                    //message += string.Format("Name: {0}<br />ID: {1}<br />The reason of resignation: {4}<br />Last Employment Date: {3}<br />Effective Date: {2}<br>", user.FullName, CurrentUserId, effectiveDate.ToString("dd/MM/yyyy"), resignDate.ToString("dd/MM/yyyy"), ddlReasons.SelectedItem.Text);
                    //message += "<em>This is an automatic email for your records only, please do not to reply to this email.</em>";
                    //string emailSubject = "Request to Resign Your Certificate of Registration Received";
                    //string emailTo = WebConfigItems.RegistrationRegistrationContactEmail; // user.Email;   //WebConfigItems.RegistrationSupportEmail;
                    //string emailBCC =  WebConfigItems.RegistrationQualityEmail;
                    //if (!string.IsNullOrEmpty(txtEmail.Text))
                    //{
                    //    emailBCC += ", " + txtEmail.Text;
                    //}
                    //var tool = new Tools();
                    //tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, emailBCC);

                    message2 = ReadHtmlPage("~/UserControls/ResignCertificateEmailBody.htm");
                    //string.Format(message2, CurrentUserId, effectiveDate.ToString("dd/MM/yyyy"));
                   // message2 = message2.Replace("{0}", user.Registration);
                    message2 = message2.Replace("{1}", effectiveDate.ToString("MM/dd/yyyy"));
                    message2 = message2.Replace("{2}", user.FirstName);
                    message2 = message2.Replace("{3}", user.LastName);
                    message2 = message2.Replace("{4}", CurrentUserId);
                    string emailSubject = "Confirmation of Resignation " + WebConfigItems.GetTestLabel;
                    string emailTo = WebConfigItems.RegistrationRegistrationContactEmail;
                    string emailBCC = string.Empty;
                    if (!string.IsNullOrEmpty(txtEmail.Text))
                    {
                        emailBCC = txtEmail.Text;
                    }

                    AlternateView av1 = AlternateView.CreateAlternateViewFromString(message2, null, MediaTypeNames.Text.Html);
                    //string path = Server.MapPath(@"~/images/DeputyRegistrarSignature.jpg"); // my logo is placed in images folder
                    //LinkedResource logo = new LinkedResource(path, "image/jpeg");
                    //logo.ContentId = "signature";
                    //av1.LinkedResources.Add(logo);

                    var tool = new Tools();
                    tool.SendConfirmationEmailAlternate(CurrentUserId, message2, av1, emailSubject, emailTo, emailBCC);
                }

                // disable user's login
                // not implemented yet
                // repository.UpdateUserLogin(CurrentUserId, effectiveDate); ?? need to check requirements ?

                // show message in the browser
                tbMainTable.Visible = false;
                tbConfirmTable.Visible = true;
                lblConfirmMessage.Text = string.Format(lblConfirmMessage.Text, effectiveDate.ToString("MM/dd/yyyy"));
            }
            catch(Exception ex)
            {
                ShowErrorMessage( ex.Message);
            }
        }

        private string ReadHtmlPage(string url)
        {
            string fileNamePath = HttpContext.Current.Server.MapPath(string.Format("{0}", url));
            var sr = new System.IO.StreamReader(fileNamePath);
            //var fileInfo = new System.IO.FileInfo(fileNamePath);
            string strContent = string.Empty;
            if (File.Exists(fileNamePath))
            {
                sr = System.IO.File.OpenText(fileNamePath);
                strContent += sr.ReadToEnd();
                sr.Close();
            }
            return strContent;
        }

        private void securityCheck()
        {
            //string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }

            if (Request.QueryString["FromMenu"] != null && !string.IsNullOrEmpty(Request.QueryString["FromMenu"]))
            {
                if (Request.QueryString["FromMenu"] == "1")
                {
                    CurrentReturnPageUrl = WebConfigItems.GetCOTOWelcomePageUrl;
                }
            }
            //lblMessage.Text = "Return Url = " + CurrentReturnPageUrl;

        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #endregion

        #region Properties

        /// <summary>
        ///  Messages collection. Contains all custom warning or errors which will be populated on the page.
        /// </summary>
        public PageMessages Messages
        {
            get
            {
                return (PageMessages)SessionParameters.PageMessages;
            }
            set
            {
                SessionParameters.PageMessages = value;
            }
        }

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public string CurrentReturnPageUrl
        {
            get
            {
                if (ViewState["CurrentReturnPageUrl"] != null && !string.IsNullOrEmpty((string)ViewState["CurrentReturnPageUrl"]))
                {
                    return (string)ViewState["CurrentReturnPageUrl"];
                }
                else
                    return WebConfigItems.Step0;
            }
            set
            {
                ViewState["CurrentReturnPageUrl"] = value;
            }
        }
        #endregion

    }
}