﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlRequestResignCertificate.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlRequestResignCertificate" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>

<style type="text/css">
    .RightColumn {
        font-size: 11pt!important;
    }
    .RightColumn p{
        padding: 5px;
    }
</style>

<div style="width: 80%; margin: 0 auto;">

    <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr class="HeaderTitle" align="right">
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc:MessageBox ID="omb" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="memberInfo" width="100%" cellspacing="2" cellpadding="3" id="tbMainTable" runat="server">
                            <tr>
                                <td style="font-size: 17px; font-weight: bold;">
                                    <asp:Label ID="lblSectionTitle" runat="server"  Text="Request to Resign my Certificate of Registration" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 10px!important;" class="RightColumn">
                                    <asp:Label ID="Label2" runat="server"
                                        Text="Please read the following information carefully" Font-Bold="true" />
                                </td>
                            </tr>
                            <tr>
                                <td class="RightColumn">
                                    <p>
                                        If you will not be working or using the title occupational therapist in Ontario, you may choose to resign your registration.
                                    </p>
                                    <p>
                                        Resigning means you will not be able to work as an occupational therapist, use the title occupational therapist, abbreviation OT, or designation OT Reg. (Ont.), in Ontario, until you are re-registered with the College.
                                    </p>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 10px!important;" class="RightColumn">
                                    <asp:Label ID="Label3" runat="server"
                                        Text="Request to resign my registration" Font-Bold="true" />
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <rjs:PopCalendarMessageContainer ID="PopCalMessages" runat="server" Calendar="PopCalendar2" />
                                </td>
                            </tr>
                            <tr>
                                <td class="RightColumn">
                                    <p>
                                        <asp:CheckBox ID="cbConfirm" runat="server" />&nbsp;
                                            <span>I declare that effective</span>&nbsp;
                                            <asp:TextBox ID="txtEffectiveDate" runat="server" />
                                        <rjs:PopCalendar ID="PopCalendar2" runat="server" AutoPostBack="False" BlankFieldText="mm dd yyyy" 
                                            Format="mm dd yyyy" Separator="/" Control="txtEffectiveDate" InvalidDateMessage="The effective date for resignation must be within the current registration year."
                                            ValidationGroup="GeneralValidation" RequiredDate="true" RequiredDateMessage="Please enter a valid  Effective day of OT employment date in mm/dd/yyyy format: field is blank." Culture="en-US" />

                                        I will be resigning my certificate of registration with the College of Occupational Therapists of Ontario (the “College”). 
                                    </p>
                                    <p>
                                        I understand that if I wish to reapply for registration with the College in the future, I must submit a new application form, application fee, and meet the registration requirements in place at the time of my application. 
                                    </p>
                                    <p>
                                        I understand that I must be issued a certificate of registration and registration number prior to returning to work as an occupational therapist in Ontario; this includes orientation at the work place.
                                    </p>
                                    <p>
                                        I understand that I will not be able to work as an occupational therapist, use the title occupational therapist, abbreviation OT, or designation OT Reg. (Ont.), in Ontario, until I am re-registered with the College.
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="RightColumn">
                                    <asp:Label ID="lblMessage" runat="server" />&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="RightColumn">
                                    <asp:Label ID="lblLastDayEmployment" runat="server" Text="Last day of OT employment:" />&nbsp;
                                        <asp:TextBox ID="txtLastDayEmployment" runat="server" />
                                    <rjs:PopCalendar ID="PopCalendar1" runat="server" AutoPostBack="False" BlankFieldText="mm dd yyyy"
                                        Format="mm dd yyyy" Separator="/" Control="txtLastDayEmployment" From-Date="2012-01-01"
                                        ValidationGroup="GeneralValidation" MessageAlignment="RightCalendarControl"  Culture="en-US" />
                                    <asp:RequiredFieldValidator ID="rfvLastDayEmployment" runat="server" ControlToValidate="txtLastDayEmployment"
                                        ErrorMessage="<br />Please enter a valid  Last day of OT employment date in mm/dd/yyyy format: field is blank." Display="Dynamic"
                                        ForeColor="Red" ValidationGroup="GeneralValidation"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="RightColumn">
                                    <asp:Label ID="lblReasonText" runat="server" Text="Please select the reason for resigning your registration" />
                                    <br />
                                    <asp:DropDownList ID="ddlReasons" runat="server" />
                                    <asp:RequiredFieldValidator ID="rfvReasons" runat="server" ControlToValidate="ddlReasons"
                                        InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please select reason for cancelling your registration"
                                        Display="Dynamic" ForeColor="Red" />
                                </td>
                            </tr>

                            <tr>
                                <td class="RightColumn">
                                    <asp:Label ID="lblEmail" runat="server" Text="Please confirm the email address you would like your confirmation of resignation sent to *" />
                                    <br />
                                    <asp:TextBox ID="txtEmail" runat="server"  Width="300px" />
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="<br />Email address is required" ValidationGroup="GeneralValidation" />
                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                        ErrorMessage="<br />Please provide a valid email address" Display="Dynamic" ForeColor="Red"
                                        ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                        ValidationGroup="GeneralValidation" />
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align: right">
                                    <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                        OnClick="ibtnBackClick" />&nbsp;&nbsp;
                                        <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                            OnClick="ibtnNextClick" ValidationGroup="GeneralValidation" />

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="memberInfo" width="100%" cellspacing="2" cellpadding="3" id="tbConfirmTable" runat="server" visible="false">
                            <tr>
                                <td style="font-size: 17px; font-weight: bold;">
                                    <asp:Label ID="Label1" runat="server"
                                        Text="Confirmation of Resignation" />
                                </td>
                            </tr>
                            <tr>
                                <td class="RightColumn">
                                    <asp:Label ID="lblConfirmMessage" runat="server"
                                        Text="The request to resign your certificate of registration has been received. 
                                        Your registration will be resigned effective {0}. You are not able to work as an occupational therapist, 
                                        use the title occupational therapist, abbreviation OT, or designation OT Reg. (Ont.), in Ontario until you are re-registered with the College. 
                                        Confirmation of your resignation will be sent to the email address you provided." />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
            <asp:Panel ID="errorsPanel" runat="server" Style="display: none; width: 750px;" CssClass="modalPopup" DefaultButton="okBtn">
                <table width="100%" cellspacing="2" cellpadding="2">
                    <tr class="topHandleRed">
                        <td align="left" runat="server" id="tdCaption">
                            <asp:Label ID="lblCaption" runat="server" Text="Error Messages:"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="text-align: left">
                                <asp:Label ID="lblErrorMessage" runat="server" />
                                <asp:ValidationSummary ID="ValidationSummary" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                    ValidationGroup="GeneralValidation" />
                                <div style="text-align: right">
                                    <asp:Button ID="okBtn" runat="server" Text="Ok" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="modalPopupEx" runat="server" PopupControlID="errorsPanel"
                TargetControlID="invisibleTarget" CancelControlID="okBtn" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Label ID="invisibleTarget" runat="server" Style="display: none" />
        </ContentTemplate>
    </asp:UpdatePanel>

</div>
