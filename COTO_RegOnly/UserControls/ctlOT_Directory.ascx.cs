﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using System.Security.Cryptography;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlOT_Directory : System.Web.UI.UserControl
    {
        #region Consts
        private const string VIEW_STATE_CURRENT_FIRST_NAME = "CurrentFirstName";
        private const string VIEW_STATE_CURRENT_LAST_NAME = "CurrentLastName";
        private const string VIEW_STATE_CURRENT_MIDDLE_NAME = "CurrentFirstName";
        private const string VIEW_STATE_CURRENT_REGISTR_NUMBER = "CurrentMiddleName";
        private const string VIEW_STATE_CURRENT_CITY = "CurrentCity";
        private const string VIEW_STATE_CURRENT_POSTAL_CODE = "CurrentPostalCode";
        private const string VIEW_STATE_CURRENT_LANGUAGE = "CurrentLanguage";
        private const string VIEW_STATE_CURRENT_FUNDING = "CurrentFunding";
        private const string VIEW_STATE_CURRENT_MAJOR_SERVICE = "CurrentMajorService";
        private const string VIEW_STATE_CURRENT_CLIENT_AGE = "CurrentClientAge";
        private const string VIEW_STATE_CURRENT_PRACTICE_SETTING = "CurrentPracticeSetting";
        private const string VIEW_STATE_CURRENT_LETTER = "CurrentLetter";
        private const string VIEW_STATE_CURRENT_START_DISPLAY = "CurrentStartDisplay";

        private const int itemsOnPage = 25;
        #endregion 

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) // first time loading 
            {
                ClearSessionParameters();
                BindLists();
            }
        }

        protected void btnSearchClick(object sender, EventArgs e)
        {
            Session[VIEW_STATE_CURRENT_FIRST_NAME] = txtFirstName.Text;
            Session[VIEW_STATE_CURRENT_LAST_NAME] = txtLastName.Text;
            Session[VIEW_STATE_CURRENT_REGISTR_NUMBER] = txtRegNumber.Text;
            Session[VIEW_STATE_CURRENT_CITY] = txtCity.Text;
            Session[VIEW_STATE_CURRENT_POSTAL_CODE] = txtPostalCode.Text;
            Session[VIEW_STATE_CURRENT_LANGUAGE] = ddlLanguage.SelectedItem.Value;
            Session[VIEW_STATE_CURRENT_FUNDING] = ddlFundingSource.SelectedValue;
            Session[VIEW_STATE_CURRENT_MAJOR_SERVICE] = ddlMajorService.SelectedValue;
            Session[VIEW_STATE_CURRENT_CLIENT_AGE] = ddlAgeRange.SelectedValue;
            Session[VIEW_STATE_CURRENT_PRACTICE_SETTING] = ddlPracticeSetting.SelectedValue;
            Response.Redirect("~/PublicRegisterProcess.aspx");  //redirect to process page
        }

        #endregion

        #region Methods

        protected void BindLists()
        {
            var repository = new Repository();

            var list1 = repository.GetGeneralList("LANGUAGE_INST_AH"); // get data for nature of practice field
            ddlLanguage.DataSource = list1;
            ddlLanguage.DataValueField = "CODE";
            ddlLanguage.DataTextField = "DESCRIPTION";
            ddlLanguage.DataBind();
            ddlLanguage.Items.Insert(0, string.Empty);

            var list2 = repository.GetGeneralList("FUNDING");
            ddlFundingSource.DataSource = list2;
            ddlFundingSource.DataValueField = "CODE";
            ddlFundingSource.DataTextField = "DESCRIPTION";
            ddlFundingSource.DataBind();
            ddlFundingSource.Items.Insert(0, string.Empty);

            var list3 = repository.GetGeneralList("MAIN_AREA_PRACTICE");
            ddlMajorService.DataSource = list3;
            ddlMajorService.DataValueField = "CODE";
            ddlMajorService.DataTextField = "DESCRIPTION";
            ddlMajorService.DataBind();
            ddlMajorService.Items.Insert(0, string.Empty);

            var list4 = repository.GetGeneralList("CLIENT_AGE_RANGE");
            ddlAgeRange.DataSource = list4;
            ddlAgeRange.DataValueField = "CODE";
            ddlAgeRange.DataTextField = "DESCRIPTION";
            ddlAgeRange.DataBind();
            ddlAgeRange.Items.Insert(0, string.Empty);

            var list5 = repository.GetGeneralList("EMPLOYER_TYPE");
            ddlPracticeSetting.DataSource = list5;
            ddlPracticeSetting.DataValueField = "CODE";
            ddlPracticeSetting.DataTextField = "DESCRIPTION";
            ddlPracticeSetting.DataBind();
            ddlPracticeSetting.Items.Insert(0, string.Empty);

        }

        public void ClearSessionParameters()
        {
            if (Session[VIEW_STATE_CURRENT_FIRST_NAME] != null)
            {
                Session[VIEW_STATE_CURRENT_FIRST_NAME] = null;
            }

            if (Session[VIEW_STATE_CURRENT_LAST_NAME] != null)
            {
                Session[VIEW_STATE_CURRENT_LAST_NAME] = null;
            }

            if (Session[VIEW_STATE_CURRENT_REGISTR_NUMBER] != null)
            {
                Session[VIEW_STATE_CURRENT_REGISTR_NUMBER] = null;
            }

            if (Session[VIEW_STATE_CURRENT_CITY] != null)
            {
                Session[VIEW_STATE_CURRENT_CITY] = null;
            }

            if (Session[VIEW_STATE_CURRENT_POSTAL_CODE] != null)
            {
                Session[VIEW_STATE_CURRENT_POSTAL_CODE] = null;
            }

            if (Session[VIEW_STATE_CURRENT_LANGUAGE] != null)
            {
                Session[VIEW_STATE_CURRENT_LANGUAGE] = null;
            }

            if (Session[VIEW_STATE_CURRENT_FUNDING] != null)
            {
                Session[VIEW_STATE_CURRENT_FUNDING] = null;
            }

            if (Session[VIEW_STATE_CURRENT_MAJOR_SERVICE] != null)
            {
                Session[VIEW_STATE_CURRENT_MAJOR_SERVICE] = null;
            }

            if (Session[VIEW_STATE_CURRENT_CLIENT_AGE] != null)
            {
                Session[VIEW_STATE_CURRENT_CLIENT_AGE] = null;
            }

            if (Session[VIEW_STATE_CURRENT_PRACTICE_SETTING] != null)
            {
                Session[VIEW_STATE_CURRENT_PRACTICE_SETTING] = null;
            }

            if (Session[VIEW_STATE_CURRENT_LETTER] != null)
            {
                Session[VIEW_STATE_CURRENT_LETTER] = null;
            }

            if (Session[VIEW_STATE_CURRENT_START_DISPLAY] != null)
            {
                Session[VIEW_STATE_CURRENT_START_DISPLAY] = null;
            }
           
        }

        #endregion      

        
    }
}
