﻿using System;
using System.Web;
using System.Web.UI;
using System.Text;
using Classes;
using System.Linq;
using System.Security.Cryptography;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberCurrencyLanguage : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;
        
        private string PrevStep = WebConfigItems.Step1;
        private string NextStep = WebConfigItems.Step3;
        
        private const int CurrentStep = 2;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.RenewalStep = CurrentStep;
                
                BindLists();
                BindData();
            }
        }

        protected void btnBackClick(object sender, EventArgs e)
        {
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }
        protected void btnUpdateClick(object sender, EventArgs e)
        {
            UpdateUserInfo();
            //Response.Redirect("MemberDisplay.aspx");

            //string fullName = string.Empty;
            //if (CurrentUser != null)
            //{
            //    fullName = CurrentUser.FullName;
            //}
            UpdateSteps(1);
            Session["CustomAction"] = "renewal";
            Response.Redirect(NextStep); // + "&ID=" + CurrentUserId + "&FULL_NAME=" + HttpUtility.UrlEncode(fullName));
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            UpdateUserInfo();
            //Response.Redirect("MemberDisplay.aspx");

            //string fullName = string.Empty;
            //if (CurrentUser != null)
            //{
            //    fullName = CurrentUser.FullName;
            //}
            UpdateSteps(1);
            Session["CustomAction"] = "renewal";
            Response.Redirect(NextStep); // + "&ID=" + CurrentUserId + "&FULL_NAME=" + HttpUtility.UrlEncode(fullName) + "&CustomAction=renewal");
        }

        protected void ibLanguageServiceHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("Please indicate all language(s) in which you can competently provide OT services.", "Help Languages");
        }

        #endregion

        #region Methods

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        protected void UpdateUserInfo()
        {
            User user = new User();
            user.Id = CurrentUserId;
            user.CurrencyLanguageServices = new Classes.CurrencyLanguage();
            user.CurrencyLanguageServices.Currency = ddlCurrencyHours.SelectedValue;
            user.CurrencyLanguageServices.Lang_Service1 = ddlService1.SelectedValue;
            user.CurrencyLanguageServices.Lang_Service2 = ddlService2.SelectedValue;
            user.CurrencyLanguageServices.Lang_Service3 = ddlService3.SelectedValue;
            user.CurrencyLanguageServices.Lang_Service4 = ddlService4.SelectedValue;
            user.CurrencyLanguageServices.Lang_Service5 = ddlService5.SelectedValue;

            var repository = new Repository();
            repository.UpdateUserCurrencyLanguageLogged( CurrentUserId, user);
            repository.UpdateUserCurrencyHoursInfoLogged(CurrentUserId, user, DateTime.Now.Year);

            //if (ddlCurrencyHours.SelectedValue.ToUpper().Contains("REVIEW") || ddlCurrencyHours.SelectedValue.ToUpper().Contains("LESS300"))
            //{
            //    string message = string.Format("Member ID: {0}; ", CurrentUserId); ;
            //     message += string.Format("Date: {0}" , DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

            //     string emailSubject = "Registrant Requires Currency Review " + WebConfigItems.GetTestLabel;
            //    string emailTo = WebConfigItems.RegistrationManagerContactEmail;
            //    var tool = new Tools();
            //    tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);
            //}
        }

        protected void BindLists()
        {
            var repository = new Repository();

            var list1 = repository.GetGeneralList("CURRENCY_REQ"); // get data for status field
            list1 = list1.Where(I => I.Code.StartsWith("R_")).ToList();
            ddlCurrencyHours.DataSource = list1;
            ddlCurrencyHours.DataValueField = "CODE";
            ddlCurrencyHours.DataTextField = "DESCRIPTION";
            ddlCurrencyHours.DataBind();
            ddlCurrencyHours.Items.Insert(0, string.Empty);

            var list2 = repository.GetGeneralList("LANGUAGE"); // get data for nature of practice field
            ddlService1.DataSource = list2;
            ddlService1.DataValueField = "CODE";
            ddlService1.DataTextField = "DESCRIPTION";
            ddlService1.DataBind();
            ddlService1.Items.Insert(0, string.Empty);


            var list3 = repository.GetGeneralList("LANGUAGE_INST_AH");
            list3 = list3.OrderBy(I => I.Description).ToList();
            ddlService2.DataSource = list3;
            ddlService2.DataValueField = "CODE";
            ddlService2.DataTextField = "DESCRIPTION";
            ddlService2.DataBind();
            ddlService2.Items.Insert(0, string.Empty);

            ddlService3.DataSource = list3;
            ddlService3.DataValueField = "CODE";
            ddlService3.DataTextField = "DESCRIPTION";
            ddlService3.DataBind();
            ddlService3.Items.Insert(0, string.Empty);

            ddlService4.DataSource = list3;
            ddlService4.DataValueField = "CODE";
            ddlService4.DataTextField = "DESCRIPTION";
            ddlService4.DataBind();
            ddlService4.Items.Insert(0, string.Empty);

            ddlService5.DataSource = list3;
            ddlService5.DataValueField = "CODE";
            ddlService5.DataTextField = "DESCRIPTION";
            ddlService5.DataBind();
            ddlService5.Items.Insert(0, string.Empty);

        }

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetUserCurrencyLanguage(CurrentUserId);
            if (user != null)
            {
                if (user.CurrencyLanguageServices != null)
                {
                    //ddlCurrencyHours.SelectedValue = user.CurrencyLanguageServices.Currency;
                    ddlService1.SelectedValue = user.CurrencyLanguageServices.Lang_Service1;
                    ddlService2.SelectedValue = user.CurrencyLanguageServices.Lang_Service2;
                    
                    
                    //ddlService5.SelectedValue = user.CurrencyLanguageServices.Lang_Service5;
                    if (string.IsNullOrEmpty(ddlService2.SelectedValue))
                    {
                        ddlService3.Enabled = false;
                    }
                    else
                    {
                        ddlService3.SelectedValue = user.CurrencyLanguageServices.Lang_Service3;
                    }
                    if (string.IsNullOrEmpty(ddlService3.SelectedValue))
                    {
                        ddlService4.Enabled = false;
                    }
                    else
                    {
                        ddlService4.SelectedValue = user.CurrencyLanguageServices.Lang_Service4;
                    }
                    if (string.IsNullOrEmpty(ddlService4.SelectedValue))
                    {
                        ddlService5.Enabled = false;
                    }
                    else
                    {
                        ddlService5.SelectedValue = user.CurrencyLanguageServices.Lang_Service5;
                    }
                }
            }

            var user0 = repository.GetUserCurrencyHoursInfo(CurrentUserId, DateTime.Now.Year);
            if (user0 != null)
            {
                if (user0.CurrencyLanguageServices != null)
                {
                    ddlCurrencyHours.SelectedValue = user0.CurrencyLanguageServices.Currency;
                }
            }

            var user2 = repository.GetUserInfo(CurrentUserId);
            if (user2 != null)
            {
                //CurrentUser = user2;
                lblCurrencyLanguageTitle.Text = "Currency and Languages of Service for " + user2.FullName;
            }
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {

                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                if (SessionParameters.CurrentUser != null)
                {
                    return SessionParameters.CurrentUser;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUser = value;
            }
        }
        #endregion

        protected void ddlService2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlService2.SelectedValue))
            {
                ddlService3.Enabled = true;
            }
            else
            {
                ddlService3.SelectedIndex = 0;
                ddlService3.Enabled = false;
                ddlService4.SelectedIndex = 0;
                ddlService4.Enabled = false;
                ddlService5.SelectedIndex = 0;
                ddlService5.Enabled = false;
            }
        }

        protected void ddlService3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlService3.SelectedValue))
            {
                ddlService4.Enabled = true;
            }
            else
            {
                ddlService4.SelectedIndex = 0;
                ddlService4.Enabled = false;
                ddlService5.SelectedIndex = 0;
                ddlService5.Enabled = false;
            }
        }

        protected void ddlService4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlService4.SelectedValue))
            {
                ddlService5.Enabled = true;
            }
            else
            {
                ddlService5.SelectedIndex = 0;
                ddlService5.Enabled = false;
            }
        }
    }
}