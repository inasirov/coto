﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Classes;
using System.Security.Cryptography;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberEditConduct : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;
        private string PrevStep = WebConfigItems.Step8;
        private string NextStep = WebConfigItems.Step10;
        private const int CurrentStep = 9;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.RenewalStep = CurrentStep;
                BindLists();
                BindData();
            }
        }

        protected void btnBackClick(object sender, EventArgs e)
        {
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(-1);
            Response.Redirect(PrevStep + "&ID=" + CurrentUserId);
        }
        protected void btnUpdateClick(object sender, EventArgs e)
        {
            UpdateUserInfo();
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(1);
            Response.Redirect(NextStep);
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(-1);
            Session["CustomAction"] = "renewal";
            Response.Redirect(PrevStep); // + "&ID=" + CurrentUserId + "&CustomAction=renewal");
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            Page.Validate("PersonalValidation");
            if (Page.IsValid)
            {
                UpdateUserInfo();
                //Response.Redirect("MemberDisplay.aspx");
                UpdateSteps(1);
                Session["CustomAction"] = "renewal";
                Response.Redirect(NextStep);
            }
        }

        protected void ddlQuestion1SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion1.SelectedValue.ToUpper() == "YES")
            {
                trQuestion1Details.Visible = true;
                txtQuestion1Details.Focus();
            }
            else
            {
                txtQuestion1Details.Text = string.Empty;
                trQuestion1Details.Visible = false;
                //Page.Validate("PersonalValidation");
            }
        }

        protected void ddlQuestion2SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion2.SelectedValue.ToUpper() == "YES")
            {
                trQuestion2Details.Visible = true;
                txtQuestion2Details.Focus();
            }
            else
            {
                txtQuestion2Details.Text = string.Empty;
                trQuestion2Details.Visible = false;
                //Page.Validate("PersonalValidation");
            }
        }

        protected void ddlQuestion3SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion3.SelectedValue.ToUpper() == "YES")
            {
                trQuestion3Details.Visible = true;
                txtQuestion3Details.Focus();
            }
            else
            {
                txtQuestion3Details.Text = string.Empty;
                trQuestion3Details.Visible = false;
                //Page.Validate("PersonalValidation");
            }
        }

        //protected void ddlQuestion4SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlQuestion4.SelectedValue.ToUpper() == "YES")
        //    {
        //        trQuestion4Details.Visible = true;
        //        txtQuestion4Details.Focus();
        //    }
        //    else
        //    {
        //        txtQuestion4Details.Text = string.Empty;
        //        trQuestion4Details.Visible = false;
        //        //Page.Validate("PersonalValidation");
        //    }
        //}

        protected void ddlQuestion5SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion5.SelectedValue.ToUpper() == "YES")
            {
                trQuestion5Details.Visible = true;
                txtQuestion5Details.Focus();
            }
            else
            {
                txtQuestion5Details.Text = string.Empty;
                trQuestion5Details.Visible = false;
                //Page.Validate("PersonalValidation");
            }
        }

        protected void ddlQuestion6SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion6.SelectedValue.ToUpper() == "YES")
            {
                trQuestion6Details.Visible = true;
                txtQuestion6Details.Focus();
            }
            else
            {
                txtQuestion6Details.Text = string.Empty;
                trQuestion6Details.Visible = false;
                //Page.Validate("PersonalValidation");
            }
        }

        protected void ddlQuestion7SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion7.SelectedValue.ToUpper() == "YES")
            {
                trQuestion7Details.Visible = true;
                txtQuestion7Details.Focus();
            }
            else
            {
                txtQuestion7Details.Text = string.Empty;
                trQuestion7Details.Visible = false;
               // Page.Validate("PersonalValidation");
            }
        }

        protected void ddlQuestion8SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion8.SelectedValue.ToUpper() == "YES")
            {
                trQuestion8Details.Visible = true;
                txtQuestion8Details.Focus();
            }
            else
            {
                txtQuestion8Details.Text = string.Empty;
                trQuestion8Details.Visible = false;
                //Page.Validate("PersonalValidation");
            }
        }

        protected void ddlQuestion9SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion9.SelectedValue.ToUpper() == "YES")
            {
                trQuestion9Details.Visible = true;
                txtQuestion9Details.Focus();
            }
            else
            {
                txtQuestion9Details.Text = string.Empty;
                trQuestion9Details.Visible = false;
                // Page.Validate("PersonalValidation");
            }
        }

        #endregion

        #region Methods

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        protected void UpdateUserInfo()
        {
            int currentYear = DateTime.Now.Year;
            User user = new User();

            user.Id = CurrentUserId;
            user.ConductInfo = new Classes.Conduct();

            user.ConductInfo.Refused_OT_Other = ddlQuestion1.SelectedValue;
            user.ConductInfo.Refused_OT_Other_Details = txtQuestion1Details.Text;

            user.ConductInfo.Finding_Other_Juris = ddlQuestion2.SelectedValue;
            user.ConductInfo.Finding_Other_Juris_Details = txtQuestion2Details.Text;

            user.ConductInfo.Finding_Other_Prof = ddlQuestion3.SelectedValue;
            user.ConductInfo.Finding_Other_Prof_Details = txtQuestion3Details.Text;

            //user.ConductInfo.Guilty_Rel_OT = ddlQuestion4.SelectedValue;
            //user.ConductInfo.Guilty_Rel_OT_Details = txtQuestion4Details.Text;

            user.ConductInfo.Criminal_Guilt = ddlQuestion5.SelectedValue;
            user.ConductInfo.Criminal_Guilt_Details = txtQuestion5Details.Text;

            user.ConductInfo.Lack_Know_Skill_Judg = ddlQuestion6.SelectedValue;
            user.ConductInfo.Lack_Know_Skill_Judg_Details = txtQuestion6Details.Text;

            user.ConductInfo.Finding_Neg_Malpractice = ddlQuestion7.SelectedValue;
            user.ConductInfo.Finding_Neg_Malpractice_Details = txtQuestion7Details.Text;

            user.ConductInfo.Facing_Proc = ddlQuestion8.SelectedValue;
            user.ConductInfo.Facing_Proc_Details = txtQuestion8Details.Text;

            user.ConductInfo.Facing_Other_Prof = ddlQuestion9.SelectedValue;
            user.ConductInfo.Facing_Other_Prof_Details = txtQuestion9Details.Text;


            var repository = new Repository();
            repository.UpdateUserConductInfoLogged( CurrentUserId, user, currentYear);

            if (ddlQuestion1.SelectedValue.ToUpper() == "YES" || ddlQuestion2.SelectedValue.ToUpper() == "YES" || ddlQuestion3.SelectedValue.ToUpper() == "YES" ||
                ddlQuestion5.SelectedValue.ToUpper() == "YES" || ddlQuestion6.SelectedValue.ToUpper() == "YES" || ddlQuestion7.SelectedValue.ToUpper() == "YES" ||
                ddlQuestion8.SelectedValue.ToUpper() == "YES" || ddlQuestion9.SelectedValue.ToUpper() == "YES")
            {
                string message = string.Format("Member ID: {0}; ", CurrentUserId); ;
                message += string.Format("Date: {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

                string emailSubject = "Registrant Requires Conduct Review " + WebConfigItems.GetTestLabel;
                string emailTo = WebConfigItems.RegistrationManagerContactEmail;
                var tool = new Tools();
                tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);
            }
        }

        protected void BindLists()
        {
            var list1 = new List<GenClass>();
            list1.Add(new GenClass { Code = "", Description = "" });
            list1.Add(new GenClass { Code = "No", Description = "No" });
            list1.Add(new GenClass { Code = "Yes", Description = "Yes" });
            

            ddlQuestion1.DataSource = list1;
            ddlQuestion1.DataValueField = "Code";
            ddlQuestion1.DataTextField = "Description";
            ddlQuestion1.DataBind();

            ddlQuestion2.DataSource = list1;
            ddlQuestion2.DataValueField = "Code";
            ddlQuestion2.DataTextField = "Description";
            ddlQuestion2.DataBind();

            ddlQuestion3.DataSource = list1;
            ddlQuestion3.DataValueField = "Code";
            ddlQuestion3.DataTextField = "Description";
            ddlQuestion3.DataBind();

            //ddlQuestion4.DataSource = list1;
            //ddlQuestion4.DataValueField = "Code";
            //ddlQuestion4.DataTextField = "Description";
            //ddlQuestion4.DataBind();

            ddlQuestion5.DataSource = list1;
            ddlQuestion5.DataValueField = "Code";
            ddlQuestion5.DataTextField = "Description";
            ddlQuestion5.DataBind();

            ddlQuestion6.DataSource = list1;
            ddlQuestion6.DataValueField = "Code";
            ddlQuestion6.DataTextField = "Description";
            ddlQuestion6.DataBind();

            ddlQuestion7.DataSource = list1;
            ddlQuestion7.DataValueField = "Code";
            ddlQuestion7.DataTextField = "Description";
            ddlQuestion7.DataBind();

            ddlQuestion8.DataSource = list1;
            ddlQuestion8.DataValueField = "Code";
            ddlQuestion8.DataTextField = "Description";
            ddlQuestion8.DataBind();

            ddlQuestion9.DataSource = list1;
            ddlQuestion9.DataValueField = "Code";
            ddlQuestion9.DataTextField = "Description";
            ddlQuestion9.DataBind();
        }

        protected void BindData()
        {
            var repository = new Repository();
            int currentYear = DateTime.Now.Year;

            var user = repository.GetUserConductInfo(CurrentUserId, currentYear);
            if (user != null)
            {
                if (user.ConductInfo != null)
                {
                    ddlQuestion1.SelectedValue = user.ConductInfo.Refused_OT_Other;
                    if (ddlQuestion1.SelectedValue.ToUpper() == "YES")
                    {
                        trQuestion1Details.Visible = true;
                        txtQuestion1Details.Text = user.ConductInfo.Refused_OT_Other_Details;
                    }

                    ddlQuestion2.SelectedValue = user.ConductInfo.Finding_Other_Juris;
                    if (ddlQuestion2.SelectedValue.ToUpper() == "YES")
                    {
                        trQuestion2Details.Visible = true;
                        txtQuestion2Details.Text = user.ConductInfo.Finding_Other_Juris_Details;
                    }

                    ddlQuestion3.SelectedValue = user.ConductInfo.Finding_Other_Prof;
                    if (ddlQuestion3.SelectedValue.ToUpper() == "YES")
                    {
                        trQuestion3Details.Visible = true;
                        txtQuestion3Details.Text = user.ConductInfo.Finding_Other_Prof_Details;
                    }

                    //ddlQuestion4.SelectedValue = user.ConductInfo.Guilty_Rel_OT;
                    //if (ddlQuestion4.SelectedValue.ToUpper() == "YES")
                    //{
                    //    trQuestion4Details.Visible = true;
                    //    txtQuestion4Details.Text = user.ConductInfo.Guilty_Rel_OT_Details;
                    //}

                    ddlQuestion5.SelectedValue = user.ConductInfo.Criminal_Guilt;
                    if (ddlQuestion5.SelectedValue.ToUpper() == "YES")
                    {
                        trQuestion5Details.Visible = true;
                        txtQuestion5Details.Text = user.ConductInfo.Criminal_Guilt_Details;
                    }

                    ddlQuestion6.SelectedValue = user.ConductInfo.Lack_Know_Skill_Judg;
                    if (ddlQuestion6.SelectedValue.ToUpper() == "YES")
                    {
                        trQuestion6Details.Visible = true;
                        txtQuestion6Details.Text = user.ConductInfo.Lack_Know_Skill_Judg_Details;
                    }

                    ddlQuestion7.SelectedValue = user.ConductInfo.Finding_Neg_Malpractice;
                    if (ddlQuestion7.SelectedValue.ToUpper() == "YES")
                    {
                        trQuestion7Details.Visible = true;
                        txtQuestion7Details.Text = user.ConductInfo.Finding_Neg_Malpractice_Details;
                    }

                    ddlQuestion8.SelectedValue = user.ConductInfo.Facing_Proc;
                    if (ddlQuestion8.SelectedValue.ToUpper() == "YES")
                    {
                        trQuestion8Details.Visible = true;
                        txtQuestion8Details.Text = user.ConductInfo.Facing_Proc_Details;
                    }

                    ddlQuestion9.SelectedValue = user.ConductInfo.Facing_Other_Prof;
                    if (ddlQuestion9.SelectedValue.ToUpper() == "YES")
                    {
                        trQuestion9Details.Visible = true;
                        txtQuestion9Details.Text = user.ConductInfo.Facing_Other_Prof_Details;
                    }
                }
            }
            var user2 = repository.GetUserInfo(CurrentUserId);
            if (user2 != null)
            {
                //CurrentUser = user2;
                lblConductTitle.Text = "Conduct for " + user2.FullName;
            }
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }
        #endregion


    }
}