﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Classes;
using System.Security.Cryptography;
using System.Globalization;
using COTO_RegOnly.Classes;


namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberConductRenewalNew : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        private string PrevStep = WebConfigItems.Step8;
        private string NextStep = WebConfigItems.Step10;
        private const int CurrentStep = 9;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.RenewalStep = CurrentStep;
                BindLists();
                BindData();
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            UpdateSteps(-1);
            Session["CustomAction"] = "renewal";
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            Page.Validate("PersonalValidation");
            if (Page.IsValid)
            {
                UpdateUserInfo();
                UpdateSteps(1);
                Session["CustomAction"] = "renewal";
                Response.Redirect(NextStep);
            }
        }

        protected void ddlQuestion1SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion1.SelectedValue.ToUpper() == "YES")
            {
                trQuestion1Details.Visible = true;
                txtQuestion1Details.Focus();
            }
            else
            {
                txtQuestion1Details.Text = string.Empty;
                trQuestion1Details.Visible = false;
            }
        }

        protected void ddlQuestion2SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion2.SelectedValue.ToUpper() == "YES")
            {
                trQuestion2Details.Visible = true;
                txtQuestion2Details.Focus();
            }
            else
            {
                txtQuestion2Details.Text = string.Empty;
                trQuestion2Details.Visible = false;
            }
        }

        protected void ddlQuestion3SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion3.SelectedValue.ToUpper() == "YES")
            {
                trQuestion3Details.Visible = true;
                txtQuestion3Details.Focus();
            }
            else
            {
                txtQuestion3Details.Text = string.Empty;
                trQuestion3Details.Visible = false;
            }
        }

        protected void ddlQuestion4SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion4.SelectedValue.ToUpper() == "YES")
            {
                trQuestion4Details.Visible = true;
                txtQuestion4Details.Focus();
            }
            else
            {
                txtQuestion4Details.Text = string.Empty;
                trQuestion4Details.Visible = false;
            }
        }

        protected void ddlQuestion5SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion5.SelectedValue.ToUpper() == "YES")
            {
                trQuestion5Details.Visible = true;
                txtQuestion5Details.Focus();
            }
            else
            {
                txtQuestion5Details.Text = string.Empty;
                trQuestion5Details.Visible = false;
            }
        }

        protected void ddlQuestion6SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion6.SelectedValue.ToUpper() == "YES")
            {
                trQuestion6Details.Visible = true;
                txtQuestion6Details.Focus();
            }
            else
            {
                txtQuestion6Details.Text = string.Empty;
                trQuestion6Details.Visible = false;
            }
        }

        protected void ddlQuestion7SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion7.SelectedValue.ToUpper() == "YES")
            {
                trQuestion7Details.Visible = true;
                txtQuestion7Details.Focus();
            }
            else
            {
                txtQuestion7Details.Text = string.Empty;
                trQuestion7Details.Visible = false;
            }
        }

        #endregion

        #region Methods

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        protected void UpdateUserInfo()
        {
            DateTime reportedDate = DateTime.Now;
            //if (!string.IsNullOrEmpty(hfReportedDate.Value))
            //{
            //    CultureInfo provider = CultureInfo.InvariantCulture;
            //    //var culture = new CultureInfo("en-CA");
            //    reportedDate = DateTime.ParseExact(hfReportedDate.Value, "yyyy-MM-dd", provider);
            //}

            int newSEQN = 0;
            int.TryParse(hfReportedSEQN.Value, out newSEQN);

            User user = new User();

            user.Id = CurrentUserId;

            var conductInfo = new ConductNew();
            conductInfo.ReportedDate = reportedDate;
            conductInfo.RenewalYear = DateTime.Now.Year.ToString();
            conductInfo.SEQN = newSEQN;

            conductInfo.FacingMisconduct = ddlQuestion1.SelectedValue;
            conductInfo.FacingMisconductDetails = txtQuestion1Details.Text;

            conductInfo.FindMisconductIncomp = ddlQuestion2.SelectedValue;
            conductInfo.FindMisconductIncompDetails = txtQuestion2Details.Text;

            conductInfo.FindNegMalpract = ddlQuestion3.SelectedValue;
            conductInfo.FindNegMalpractDetails = txtQuestion3Details.Text;

            conductInfo.ChargedOffence = ddlQuestion4.SelectedValue;
            conductInfo.ChargedOffenceDetails = txtQuestion4Details.Text;

            conductInfo.CondRestrict = ddlQuestion5.SelectedValue;
            conductInfo.CondRestrictDetails = txtQuestion5Details.Text;

            conductInfo.Guilty_Authority = ddlQuestion6.SelectedValue;
            conductInfo.Guilty_Authority_Details = txtQuestion6Details.Text;

            conductInfo.EventCircumstance = ddlQuestion7.SelectedValue;
            conductInfo.EventCircumstanceDetails = txtQuestion7Details.Text;

            if (conductInfo.FacingMisconduct.ToUpper() == "YES") conductInfo.ReportType = "FACING_PM_I_I";
            if (conductInfo.FindMisconductIncomp.ToUpper() == "YES") conductInfo.ReportType = "FINDING_PM_I_I";
            if (conductInfo.FindNegMalpract.ToUpper() == "YES") conductInfo.ReportType = "FINDING_NEG_MAL";
            if (conductInfo.ChargedOffence.ToUpper() == "YES") conductInfo.ReportType = "CHARGED_OFFENCE";
            if (conductInfo.CondRestrict.ToUpper() == "YES") conductInfo.ReportType = "COND_RESTRICT";
            if (conductInfo.Guilty_Authority.ToUpper() == "YES") conductInfo.ReportType = "GUILTY_OFFENCE";
            //if (conductInfo.EventCircumstance.ToUpper() == "YES") conductInfo.ReportType = "";

            var repository = new Repository();
            repository.UpdateUserConductInfoLoggedSEQNNew(CurrentUserId, conductInfo);

            if (ddlQuestion1.SelectedValue.ToUpper() == "YES" || ddlQuestion2.SelectedValue.ToUpper() == "YES" || ddlQuestion3.SelectedValue.ToUpper() == "YES" ||
                ddlQuestion4.SelectedValue.ToUpper() == "YES" || ddlQuestion5.SelectedValue.ToUpper() == "YES" || ddlQuestion6.SelectedValue.ToUpper() == "YES" ||
                ddlQuestion7.SelectedValue.ToUpper() == "YES")
            {
                string message = string.Format("Member ID: {0}; ", CurrentUserId); ;
                message += string.Format("Date: {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

                string emailSubject = "Member Requires Conduct Review";
                string emailTo = WebConfigItems.RegistrationConductDeclaredEmail;
                var tool = new Tools();
                tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);
            }
        }

        protected void BindLists()
        {
            var list1 = new List<GenClass>();
            list1.Add(new GenClass { Code = "", Description = "" });
            list1.Add(new GenClass { Code = "No", Description = "No" });
            list1.Add(new GenClass { Code = "Yes", Description = "Yes" });

            ddlQuestion1.DataSource = list1;
            ddlQuestion1.DataValueField = "Code";
            ddlQuestion1.DataTextField = "Description";
            ddlQuestion1.DataBind();

            ddlQuestion2.DataSource = list1;
            ddlQuestion2.DataValueField = "Code";
            ddlQuestion2.DataTextField = "Description";
            ddlQuestion2.DataBind();

            ddlQuestion3.DataSource = list1;
            ddlQuestion3.DataValueField = "Code";
            ddlQuestion3.DataTextField = "Description";
            ddlQuestion3.DataBind();

            ddlQuestion4.DataSource = list1;
            ddlQuestion4.DataValueField = "Code";
            ddlQuestion4.DataTextField = "Description";
            ddlQuestion4.DataBind();

            ddlQuestion5.DataSource = list1;
            ddlQuestion5.DataValueField = "Code";
            ddlQuestion5.DataTextField = "Description";
            ddlQuestion5.DataBind();

            ddlQuestion6.DataSource = list1;
            ddlQuestion6.DataValueField = "Code";
            ddlQuestion6.DataTextField = "Description";
            ddlQuestion6.DataBind();

            ddlQuestion7.DataSource = list1;
            ddlQuestion7.DataValueField = "Code";
            ddlQuestion7.DataTextField = "Description";
            ddlQuestion7.DataBind();
        }

        protected void BindData()
        {
            var repository = new Repository();
            int currentYear = DateTime.Now.Year;
            var user = repository.GetUserConductInfoNew2(CurrentUserId, currentYear);
            if (user != null)
            {
                //hfReportedDate.Value = user.ReportedDate.ToString("yyyy-MM-dd");
                hfReportedSEQN.Value = user.SEQN.ToString();
                ddlQuestion1.SelectedValue = user.FacingMisconduct;
                if (ddlQuestion1.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion1Details.Visible = true;
                    txtQuestion1Details.Text = user.FacingMisconductDetails;
                }

                ddlQuestion2.SelectedValue = user.FindMisconductIncomp;
                if (ddlQuestion2.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion2Details.Visible = true;
                    txtQuestion2Details.Text = user.FindMisconductIncompDetails;
                }

                ddlQuestion3.SelectedValue = user.FindNegMalpract;
                if (ddlQuestion3.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion3Details.Visible = true;
                    txtQuestion3Details.Text = user.FindNegMalpractDetails;
                }

                ddlQuestion4.SelectedValue = user.ChargedOffence;
                if (ddlQuestion4.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion4Details.Visible = true;
                    txtQuestion4Details.Text = user.ChargedOffenceDetails;
                }

                ddlQuestion5.SelectedValue = user.CondRestrict;
                if (ddlQuestion5.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion5Details.Visible = true;
                    txtQuestion5Details.Text = user.CondRestrictDetails;
                }

                ddlQuestion6.SelectedValue = user.Guilty_Authority;
                if (ddlQuestion6.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion6Details.Visible = true;
                    txtQuestion6Details.Text = user.Guilty_Authority_Details;
                }

                ddlQuestion7.SelectedValue = user.EventCircumstance;
                if (ddlQuestion7.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion7Details.Visible = true;
                    txtQuestion7Details.Text = user.EventCircumstanceDetails;
                }
            }
            var user2 = repository.GetUserInfo(CurrentUserId);
            if (user2 != null)
            {
                lblConductTitle.Text = "Suitability to Practise for " + user2.FullName;
            }
        }

        //protected void ShowMessage(string Message)
        //{
        //    omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        //}

        //protected void ShowMessage(string Message, string Caption)
        //{
        //    omb.ShowMessage(Message, Caption);
        //}

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }
        #endregion
    }
}