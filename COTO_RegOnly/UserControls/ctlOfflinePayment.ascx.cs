﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using COTO_RegOnly.Classes;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;
using System.Text;
using System.IO;


namespace COTO_RegOnly.UserControls
{
    public partial class ctlOfflinePayment : System.Web.UI.UserControl
    {

        private string PrevStep = WebConfigItems.Step12;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }
            BindData();
            DataBind();
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(PrevStep);
        }

        protected void BindData()
        {
            var repository = new Repository();
            var user2 = repository.GetUserInfo(CurrentUserId);
            if (user2 != null)
            {
                lblPersonalInformationSectionTitle.Text = string.Format("Offline Payment for {0} {1}", user2.FullName, user2.Registration);
                //lblImisID.Text = CurrentUserId;
            }
        }

        protected void lbtnPrintReportClick(object sender, EventArgs e)
        {
            //Session["ReportID"] = CurrentUserId;
            Response.Redirect(WebConfigItems.Step12_PDF_Report);
        }
        public string GetPrintReportUrl()
        {
            return WebConfigItems.Step12_PDF_Report;
        }

        protected void lbtnFeedbackLinkClick(object sender, EventArgs e)
        {
            SessionParameters.ReturnUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            Response.Redirect(WebConfigItems.Step12_Feedback);
        }

        protected void lbtnPrintFormClick(object sender, EventArgs e)
        {
            var repository = new Repository();
            var user = repository.GetUserInfo(CurrentUserId);
            if (user != null)
            {
                string fullName = user.FullName;
                string major_Key = user.Registration;

                //OpenDoc();

                ////-- begin
                //string fileName = HttpContext.Current.Server.MapPath("~/Report/2019_Offline_Payment_Page.pdf");
                ////string shortFileName = "Payment_Form.pdf";

                //MemoryStream ms = PopulatePDF(fileName, fullName, major_Key);

                //Response.Buffer = true;
                //Response.Charset = "";
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                ////Response.ContentType = ContentType;

                ////opens pdf in new tab after save/open option
                //Response.AddHeader("content-disposition", "attachment; filename=Payment_Form.pdf");

                //Response.ContentType = "application/pdf";
                //Response.BinaryWrite(ms.ToArray());
                //Response.Flush();
                //Response.End();    

                //// -- end

                // new version
                string fileName = HttpContext.Current.Server.MapPath("~/Report/2021_Offline_Payment_Page.pdf");
                MemoryStream ms = PopulatePDF(fileName, fullName, major_Key);

                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=2021_Offline_Payment_Page.pdf");
                HttpContext.Current.Response.BinaryWrite(ms.ToArray());
                HttpContext.Current.Response.End();

                ms.Dispose();



                // end of new version
            }
        }

        //protected void OpenDoc()
        //{
        //    using (var ms = new MemoryStream())
        //    {
        //        using (var document = new Document(PageSize.A4, 50, 50, 15, 15))
        //        {
        //            PdfWriter.GetInstance(document, ms);
        //            document.Open();
        //            document.Add(new Paragraph("HelloWorld"));
        //            document.Close();
        //        }
        //        Response.Clear();
        //        //Response.ContentType = "application/pdf";
        //        Response.ContentType = "application/octet-stream";
        //        Response.AddHeader("content-disposition", "attachment;filename= Test.pdf");
        //        Response.Buffer = true;
        //        Response.Clear();
        //        var bytes = ms.ToArray();
        //        Response.BinaryWrite(bytes);
        //        Response.Flush();
        //        Response.End();    
        //    } 
        //}
        

        private MemoryStream PopulatePDF(string fileName, string FullName, string Registration)
        {

            MemoryStream ms = new MemoryStream();
            PdfStamper Stamper = null;
            PdfReader Reader = new PdfReader(fileName);
            try
            {
                PdfCopyFields Copier = new PdfCopyFields(ms);
                Copier.AddDocument(Reader);
                Copier.Close();

                PdfReader docReader = new PdfReader(ms.ToArray());
                ms = new MemoryStream();

                //PdfReader docReader = new PdfReader(ms.ToArray());
                Stamper = new PdfStamper(docReader, ms);
                AcroFields Fields = Stamper.AcroFields;

                hfDocumentFields.Value = string.Empty;
                foreach (var item in Stamper.AcroFields.Fields)
                {
                    hfDocumentFields.Value += "key:= " + item.Key + "; value:= " + item.Value + "<br />";
                }

                //fill form fields here                      

                Fields.SetField("Text Field 1", FullName);
                Fields.SetField("Text Field 51", Registration);
                //Fields.SetField("topmostSubform[0].Page1[0].TextField1[0]", CurrentUserId);
                Stamper.FormFlattening = false;
            }
            finally
            {
                if (Stamper != null)
                {
                    Stamper.Close();
                }
                //Reader.Close();
            }
            return ms;
        }


        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        #endregion

    }
}