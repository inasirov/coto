﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberPracticeHistoryRenewal.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlMemberPracticeHistoryRenewal" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="2" width="100%">
                    <tr class="HeaderTitle">
                        <td class="HeaderTitle" align="right">
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 8 of 12" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" UseSubmitBehavior="true" TabIndex="0"/>--%>
                        </td>
                    </tr>
                    <%--<tr>
                        <td align="right">
                            <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back" OnClick="btnBackClick" />&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Update" OnClick="btnUpdateClick"  />
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr class="RowTitle">
                        <td>
                            <asp:Label ID="lblPracticeHistoryTitle" runat="server" CssClass="heading" Text="OT Practice History" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; font-weight: bold;">
                            <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/Glossary-OccupationalTherapyPracticeEN.pdf" target="_blank">Glossary</a>&nbsp;/&nbsp;
                            <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/Glossary-OccupationalTherapyPracticeFR.pdf" target="_blank">Glossaire</a>
                        </td>
                        <tr>
                            <td style="text-align: left;">
                                <p>
                                    The following questions identify the initial practice location of OTs, allow analysis of inter-provincial migration and mobility between the USA and Canada, and information on the mobility of internationally educated OTs. Your answers to these questions will help the Ministry of Health and Long-Term Care develop policies and programs that address supply and distribution, education, recruitment and retention for occupational therapists. To protect your privacy, the data we submit to the Ministry will be anonymized.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                <p>
                                    The fields below are read only. You will not be able to make any changes. If any of the information displayed is <b>incorrect</b>, please&nbsp;
                                    <asp:LinkButton ID="lbtnCorrectDetails" runat="server" Font-Bold="true" OnClick="lbtnCorrectDetailsClick" Text="click here " />
                                    &nbsp; to provide the correct details to the College.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp; </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="memberInfo" width="100%">
                                    <tr>
                                        <td colspan="2" style="text-align: left;">
                                            <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* - denotes required field" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp; </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: left;"><span style="font-size: Medium; font-weight: bold;">Initial Practice Information</span>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblFirstYearOTPractice" runat="server" Text="What is the &lt;b&gt;first year&lt;/b&gt; you began practising OT?" />
                                            &nbsp;&nbsp; </td>
                                        <td style="text-align: right;">
                                            <asp:DropDownList ID="ddlFirstYearOTPractice" runat="server" Enabled="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblFirstCountryOTPractice" runat="server" Text="What is the &lt;b&gt;first country&lt;/b&gt; in which you practised OT after completing your entry level OT education?" />
                                            &nbsp; </td>
                                        <td style="text-align: right;">
                                            <asp:DropDownList ID="ddlFirstCountryOTPractice" runat="server" Enabled="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblFirstStateOTPractice" runat="server" Text="What is the &lt;b&gt;first province or territory&lt;/b&gt; (if Canada), &lt;b&gt;or state&lt;/b&gt; (if USA) in which you began practising OT after completing your entry level OT education? If your first country of OT practice is not Canada or the USA, please select &quot;Not Applicable&quot;" />
                                            &nbsp;&nbsp; </td>
                                        <td style="text-align: right;">
                                            <asp:DropDownList ID="ddlFirstStateOTPractice" runat="server" Enabled="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: left;"><span style="font-size: Medium; font-weight: bold;">First Canadian Practice Information</span>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblFirstYearCanadianOTPractice" runat="server" Text="What is the &lt;b&gt;first year&lt;/b&gt; you practiced OT in &lt;b&gt;Canada&lt;/b&gt;?" />
                                            &nbsp;&nbsp; </td>
                                        <td style="text-align: right;">
                                            <asp:DropDownList ID="ddlFirstYearCanadianOTPractice" runat="server" Enabled="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblFirstProvinceOTPractice" runat="server" Text="What is the &lt;b&gt;first&lt;/b&gt; Canadian &lt;b&gt;province or territory&lt;/b&gt; in which you practiced OT?" />
                                            &nbsp;&nbsp; </td>
                                        <td style="text-align: right;">
                                            <asp:DropDownList ID="ddlFirstProvinceOTPractice" runat="server" Enabled="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp; </td>
                                    </tr>
                                    <tr>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblPracticeOutsideOntario" runat="server" Text="Have you ever practised outside of Ontario? *" />
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:DropDownList ID="ddlOutsideOntarioPractice" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlOutsideOntarioPracticeSelectedIndexChanged" />
                                            <asp:RequiredFieldValidator ID="rfvOutsideOntarioPractice" runat="server" ControlToValidate="ddlOutsideOntarioPractice" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please select &quot;Yes&quot; or &quot;No&quot;" ForeColor="Red" InitialValue="" ValidationGroup="GeneralValidation" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr id="trOutsideON1" runat="server" visible="false">
                                        <td colspan="2" style="text-align: left;"><span style="font-size: Medium; font-weight: bold;">Most Recent Non-Ontario Practice Information</span> </td>
                                    </tr>
                                    <tr id="trOutsideON2" runat="server" visible="false">
                                        <td class="RightColumn">
                                            <asp:Label ID="lblLastYearOutsideOntarioTitle" runat="server" Text="What is the &lt;b&gt;most recent year&lt;/b&gt; in which you practiced OT &lt;b&gt;outside of Ontario&lt;/b&gt;?" />
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:DropDownList ID="ddlLastYearOutsideOntario" runat="server">
                                            </asp:DropDownList>
                                            &nbsp;
                                            <asp:RequiredFieldValidator ID="rfvLastYearOutsideOntario" runat="server" ControlToValidate="ddlLastYearOutsideOntario" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please provide your last year of practise in jurisdiction outside of Ontario" ForeColor="Red" InitialValue="" ValidationGroup="GeneralValidation" />
                                        </td>
                                    </tr>
                                    <tr id="trOutsideON3" runat="server" visible="false">
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr id="trOutsideON4" runat="server" visible="false">
                                        <td class="RightColumn">
                                            <asp:Label ID="lblLastCountryOTPractice" runat="server" Text="What is the most &lt;b&gt;recent country&lt;/b&gt; in which you &lt;b&gt;practiced OT&lt;/b&gt; outside of Canada?" />
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:DropDownList ID="ddlLastCountryOTPractice" runat="server">
                                            </asp:DropDownList>
                                            &nbsp;
                                            <asp:RequiredFieldValidator ID="rfvLastCountryOTPractice" runat="server" ControlToValidate="ddlLastCountryOTPractice" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please provide your last country of OT Practice" ForeColor="Red" InitialValue="" ValidationGroup="GeneralValidation" />
                                        </td>
                                    </tr>
                                    <tr id="trOutsideON5" runat="server" visible="false">
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr id="trOutsideON6" runat="server" visible="false">
                                        <td class="RightColumn">
                                            <asp:Label ID="lblLastStateTitle" runat="server" Text="Last Canadian province/territory or US state of OT practice (Canada or US only) &lt;sup&gt;*&lt;/sup&gt;" />
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:DropDownList ID="ddlLastStateOrCanadianProvince" runat="server">
                                            </asp:DropDownList>
                                            &nbsp;
                                            <asp:RequiredFieldValidator ID="rfvLastStateOrCanadianProvince" runat="server" ControlToValidate="ddlLastStateOrCanadianProvince" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please provide your last province/territory/state of OT Practice" ForeColor="Red" InitialValue="" ValidationGroup="GeneralValidation" />
                                        </td>
                                    </tr>
                                    <tr id="trOutsideON7" runat="server" visible="false">
                                        <td colspan="2"></td>
                                    </tr>
                                    <%-- <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblOutsideOntarioPracticeTitle" runat="server" Text="Have you ever practised outside of Ontario? *" />&nbsp;
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:DropDownList ID="ddlOutsideOntarioPractice" runat="server">
                                        </asp:DropDownList>
                                        &nbsp;
                                        <asp:RequiredFieldValidator ID="rfvCurrentPracticeStatus" runat="server" ControlToValidate="ddlOutsideOntarioPractice"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please indicate whether or not you have ever practised outside of Ontario"
                                            Display="None" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblLastCanadianProvinceTitle" runat="server" Text="Last Canadian Province/Territory <sup>*</sup>" />
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:DropDownList ID="ddlLastCanadianProvince" runat="server">
                                        </asp:DropDownList>
                                        &nbsp;
                                        <asp:RequiredFieldValidator ID="rfvLastCanadianProvince" runat="server" ControlToValidate="ddlLastCanadianProvince"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please indicate whether or not you have ever practised outside of Ontario"
                                            Display="None" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>--%>
                                    <tr id="trOutsideON8" runat="server" visible="false">
                                        <td colspan="2">&nbsp; </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp; </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />
                                &nbsp;&nbsp;
                                <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                                <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" UseSubmitBehavior="true" TabIndex="0"/>--%></td>
                        </tr>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
