﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;
using Classes;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberEditRenewal : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;

        private string PrevStep = WebConfigItems.Step0;
        private string NextStep = WebConfigItems.Step2;
        private const int CurrentStep = 1;

        #endregion

        #region Events

        protected void Page_Init(object sender, EventArgs e)
        {
            //lblDebug.Text += string.Format("<br/>Init load, Session['ID']={0}", Session["ID"]);
            if (!Page.ClientScript.IsStartupScriptRegistered(GetType(), "MaskedEditFix"))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "MaskedEditFix", String.Format("<script type='text/javascript' src='{0}'></script>", Page.ResolveUrl("~/Scripts/MaskedEditFix.js")));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            //lblDebug.Text += string.Format("<br/>First load, Session['ID']={0}", Session["ID"]);
            //if (Request.QueryString["common"] != null && Request.QueryString["common"] == "updated")
            //{
            //    trEditConfirmation.Visible = true;
            //}
            //else
            //{
            //    trEditConfirmation.Visible = false;
            //}

            if (Request.QueryString.Count > 1)
                securityCheck();

            //lblDebug.Text += string.Format("<br/>after securityCheck, Session['ID']={0}", Session["ID"]);

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.RenewalStep = CurrentStep;
                BindLists();
                BindData();
                
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            txtPostalCode.MaxLength = 10;
        }

        protected void btnBackClick(object sender, EventArgs e)
        {
            Response.Redirect(PrevStep);
            UpdateSteps(-1);
            //Response.Redirect("MemberDisplay.aspx");
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(PrevStep);
            UpdateSteps(-1);
        }

        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            if (UpdateUserInfo())
            {
                //Response.Redirect("MemberDisplay.aspx");
                UpdateSteps(1);
                Response.Redirect(NextStep);
            }
            
        }

        protected void btnUpdateClick(object sender, EventArgs e)
        {
            UpdateUserInfo();
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(1);
            Response.Redirect(NextStep);
        }

        //protected void ibPostalCodeHelpClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("Please use the appropriate format for postal codes, i.e. capital letters without any spaces (ANANAN). ", "Postal Code");
        //}

        //protected void ibCountryHelpClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("If you reside in Canada, this field can be left blank.", "Country");
        //}

        //protected void ibLegalLastNameClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("The name provided here must be the same name that appears on your documentation to demonstrate citizenship or work permit. " + 
        //    "You do not necessarily have to use your full legal name in your professional practice. ", "Legal Name");
        //}
        //protected void ibPreviousLegalLastNameClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("This is the name that appears on your OT entry-level degree. Provide this information if it is not the name you are currently using in practice. ", "Previous Legal Name");
        //}

        //protected void ibCommonlyUsedLastNameClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("This is the name currently on file and appearing on the Public Register. The name on the Public Register must be the name " +
        //        "that you use in your professional practice. It does not necessarily have to be your full legal name.", "Commonly Used Name");
        //}

        protected void btnEditProfileClick(object sender, EventArgs e)
        {
            string new_url = string.Format("{0}?id={1}", WebConfigItems.Step1_EditName, CurrentUserId);
            Response.Redirect(new_url);
        }

        protected void ddlProvinceSelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlProvince.SelectedValue == "N/A")
            //{
            //    //trCountry.Visible = true;
            //    if (ddlCountry.SelectedItem.Text.ToUpper() != "CANADA")
            //    {
            //        meePostalCode.Enabled = false;
            //        revPostalCode.Enabled = false;
            //        txtPostalCode.MaxLength = 10;
            //    }
            //    else
            //    {
            //        meePostalCode.Enabled = true;
            //        revPostalCode.Enabled = true;
            //    }
            //}
            //else
            //{
            //    //trCountry.Visible = false;
            //    meePostalCode.Enabled = true;
            //    revPostalCode.Enabled = true;
            //}
        }

        protected void ddlCountrySelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlCountry.SelectedItem.Text.ToUpper() != "CANADA")
            //{
            //    meePostalCode.Enabled = false;
            //    revPostalCode.Enabled = false;
            //    txtPostalCode.MaxLength = 10;
            //}
            //else
            //{
            //    meePostalCode.Enabled = true;
            //    revPostalCode.Enabled = true;
            //}

            var repository = new Repository();

            revPostalCodeCA.Enabled = false;
            revPostalCodeUS.Enabled = false;
            //rfvPostalCode.Enabled = true;
            //revPostalCode.Enabled = false;
            txtPostalCode.MaxLength = 10;
            //rfvProvince.Enabled = true;

            if (ddlCountry.SelectedItem.Text.ToUpper().Contains("CANADA") || ddlCountry.SelectedItem.Text.ToUpper().Contains("UNITED STATES"))
            {
                txtProvince.Visible = false; txtProvince.Text = string.Empty;
                //txtProvince.Text = string.Empty;
                txtPostalCode.Text = string.Empty;
                ddlProvince.Visible = true;
                ddlProvince.SelectedIndex = 0;
                
                meeHomePhone.Enabled = true;
                mevHomePhone.Enabled = true;

                //revHomePhone.Enabled = true;
                if (ddlCountry.SelectedItem.Text.ToUpper().Contains("CANADA"))
                {
                    var list0 = repository.GetCanadianProvincesListNew();

                    list0.Insert(0, new GenClass { Code = "", Description = "" });

                    ddlProvince.DataSource = list0;
                    ddlProvince.DataValueField = "Code";
                    ddlProvince.DataTextField = "Description";
                    ddlProvince.DataBind();

                    revPostalCodeCA.Enabled = true;
                    //mevPostalCode.Enabled = true;
                    //rfvPostalCode.Enabled = false;
                }
                if (ddlCountry.SelectedItem.Text.ToUpper().Contains("UNITED STATES"))
                {
                    var list1 = repository.GetUSStatesList();

                    list1.Insert(0, new GenClass { Code = "", Description = "" });

                    ddlProvince.DataSource = list1;
                    ddlProvince.DataValueField = "Code";
                    ddlProvince.DataTextField = "Description";
                    ddlProvince.DataBind();

                    revPostalCodeUS.Enabled = true;
                }

            }
            else
            {
                txtProvince.Visible = true; txtProvince.Text = "Not applicable"; txtProvince.ReadOnly = true;
                ddlProvince.Visible = false;
                //rfvProvince.Enabled = false;
                meeHomePhone.Enabled = false;
                mevHomePhone.Enabled = false;
                txtPostalCode.MaxLength = 10;
                txtPostalCode.Text = string.Empty;
                //meePostalCode.Enabled = false;
                //revPostalCode.Enabled = false;
                //revHomePhone.Enabled = false;
            }
        }

        #endregion

        #region Methods

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        protected bool UpdateUserInfo()
        {
            User user = new User();
            user.Email = txtEmailText.Text;

            user.Id = CurrentUserId;
            user.HomePhone = txtHomePhoneText.Text;
            user.HomeAddress = new Address();
            user.HomeAddress.Address1 = txtHomeAddress1Text.Text;
            user.HomeAddress.Address2 = txtHomeAddress2Text.Text;
            user.HomeAddress.Address3 = txtHomeAddress3Text.Text;

            //if (!string.IsNullOrEmpty(txtBirthDate.Text))
            //{
            //    CultureInfo provider = CultureInfo.InvariantCulture;
            //    //var culture = new CultureInfo("en-CA");
            //    var birthDate = DateTime.ParseExact(txtBirthDate.Text, "MM/dd/yyyy", provider);
            //    user.BirthDate = birthDate;
            //}
            //else
            //{
            //    user.BirthDate = DateTime.MinValue;
            //}

            user.HomeAddress.City = txtCityText.Text;
            user.HomeAddress.Country = ddlCountry.SelectedItem.Text;
            user.HomeAddress.PostalCode = txtPostalCode.Text.ToUpper();
            if (ddlProvince.Visible)
            {
                user.HomeAddress.Province = ddlProvince.SelectedValue;
            }
            else
            {
                user.HomeAddress.Province = string.Empty; // txtProvince.Text;
            }
            
            user.CommonlyUsedFirstName = txtCommonlyUsedFirstName.Text;
            user.CommonlyUsedLastName = txtCommonlyUsedLastName.Text;

            var repository = new Repository();
            string message = repository.UpdateUserInfoLogged(CurrentUserId,  user);
            if (!string.IsNullOrEmpty(message))
            {
                ShowMessage(message);
                return false;
            }

            if (CurrentPrevCommonlyUsedFirstName != user.CommonlyUsedFirstName || CurrentPrevCommonlyUsedLastName != user.CommonlyUsedLastName)
            {
                SendNotificationEmail();
            }

            //// save all changes to name_log table
            //if (CurrentUser != null)
            //{
            //    if (CurrentUser.Email != user.Email)
            //    {
            //       // repository.AddLogEntry( DateTime.Now, "Change", 
            //    }
            //}
            return true;
        }

        protected void BindLists()
        {
            var repository = new Repository();

            var list1 = repository.GetCanadianProvincesListNew();
            
            list1.Insert(0, new GenClass { Code = "", Description = "" });

            ddlProvince.DataSource = list1;
            ddlProvince.DataValueField = "Code";
            ddlProvince.DataTextField = "Description";
            ddlProvince.DataBind();

            //var list2 = Countries.ListOfCountries;
            
            var list2 = repository.GetGeneralList("COUNTRY");
            //var list2 = repository.GetCountriesNew();

            list2 = list2.OrderBy(I => I.Description).ToList();
            ddlCountry.DataSource = list2;
            ddlCountry.DataValueField = "Code";
            ddlCountry.DataTextField = "Description";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, string.Empty);
        }

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetUserInfo(CurrentUserId);
            if (user != null)
            {
                //CurrentUser = user; // saved previous information about user to viewstate

                lblPersonalInformationSectionTitle.Text = "Personal Information for " + user.FullName;
                txtEmailText.Text = user.Email;
                txtHomePhoneText.Text = user.HomePhone;

                lblLegalLastName.Text = user.LegalLastName;
                lblLegalFirstName.Text = user.LegalFirstName;
                lblLegalMiddleName.Text = user.LegalMiddleName;

                lblPreviousLegalLastName.Text = user.PreviousLegalLastName;
                lblPreviousLegalFirstName.Text = user.PreviousLegalFirstName;
                txtCommonlyUsedLastName.Text = user.CommonlyUsedLastName;
                txtCommonlyUsedFirstName.Text = user.CommonlyUsedFirstName;

                CurrentPrevCommonlyUsedFirstName = user.CommonlyUsedFirstName;
                CurrentPrevCommonlyUsedLastName = user.CommonlyUsedLastName;

                //lblCommonlyUsedMiddleName.Text = user.CommonlyUsedMiddleName;

                if (user.BirthDate != DateTime.MinValue)
                {
                    txtBirthDate.Text = user.BirthDate.ToString("MM/dd/yyyy");
                }

                if (user.HomeAddress != null)
                {
                    txtHomeAddress1Text.Text = user.HomeAddress.Address1;
                    txtHomeAddress2Text.Text = user.HomeAddress.Address2;
                    txtHomeAddress3Text.Text = user.HomeAddress.Address3;

                    txtCityText.Text = user.HomeAddress.City;

                    if (string.IsNullOrEmpty(user.HomeAddress.Country))
                    {
                        user.HomeAddress.Country = "Canada";
                    }

                    var foundCountry = ddlCountry.Items.FindByText(user.HomeAddress.Country);
                    if (foundCountry != null)
                    {
                        ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(foundCountry);
                    }

                    //meePostalCode.Enabled = false;
                    //mevPostalCode.Enabled = false;
                    rfvPostalCode.Enabled = true;
                    txtPostalCode.MaxLength = 10;
                    if (user.HomeAddress.Country.ToUpper().Contains("CANADA") || user.HomeAddress.Country.ToUpper().Contains("UNITED STATES"))
                    {
                        txtProvince.Visible = false;
                        ddlProvince.Visible = true;
                        //rfvProvince.Enabled = true;
                        meeHomePhone.Enabled = true;
                        mevHomePhone.Enabled = true;

                        //ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByText(user.HomeAddress.Country));
                        if (user.HomeAddress.Country.ToUpper().Contains("CANADA"))
                        {
                            revPostalCodeCA.Enabled = true;
                            //rfvPostalCode.Enabled = false;
                        }
                        if (user.HomeAddress.Country.ToUpper().Contains("UNITED STATES"))
                        {
                            var list1 = repository.GetUSStatesList();

                            list1.Insert(0, new GenClass { Code = "", Description = "" });

                            ddlProvince.DataSource = list1;
                            ddlProvince.DataValueField = "Code";
                            ddlProvince.DataTextField = "Description";
                            ddlProvince.DataBind();

                            revPostalCodeUS.Enabled = true;
                        }

                        if (ddlProvince.Items.FindByValue(user.HomeAddress.Province) != null)
                        {
                            ddlProvince.SelectedValue = user.HomeAddress.Province;
                        }
                    }
                    else
                    {
                        txtProvince.Visible = true; txtProvince.Text = "Not applicable"; txtProvince.ReadOnly = true;
                        //txtProvince.Text = user.HomeAddress.Province;
                        ddlProvince.Visible = false;
                        //meePostalCode.Enabled = false;
                        //rfvProvince.Enabled = false;
                        meeHomePhone.Enabled = false;
                        mevHomePhone.Enabled = false;
                    }

                    txtPostalCode.Text = user.HomeAddress.PostalCode;
                    txtPostalCode.MaxLength = 10;
                }
            }
        }

        protected void SendNotificationEmail()
        {
            string message = string.Format("Member ID: {0}; ", CurrentUserId); ;
            message += string.Format("Date: {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

            string emailSubject = "Commonly Used Name Change " + WebConfigItems.GetTestLabel;
            string emailTo = WebConfigItems.RegistrationRegistrationContactEmail;
            var tool = new Tools();
            tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);
        }

        private void securityCheck()
        {
            //string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];
            
            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            
            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                if (ViewState["CURRENT_PREV_USER_INFO"] != null)
                {
                    return (User)ViewState["CURRENT_PREV_USER_INFO"];
                }
                else
                    return null; ;
            }
            set
            {
                ViewState["CURRENT_PREV_USER_INFO"] = value;
            }
        }

        public string CurrentPrevCommonlyUsedLastName
        {
            get
            {
                if (ViewState["CURRENT_PREV_COMM_USED_LAST_NAME"] != null)
                {
                    return (string)ViewState["CURRENT_PREV_COMM_USED_LAST_NAME"];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState["CURRENT_PREV_COMM_USED_LAST_NAME"] = value;
            }
        }

        public string CurrentPrevCommonlyUsedFirstName
        {
            get
            {
                if (ViewState["CURRENT_PREV_COMM_USED_FIRST_NAME"] != null)
                {
                    return (string)ViewState["CURRENT_PREV_COMM_USED_FIRST_NAME"];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState["CURRENT_PREV_COMM_USED_FIRST_NAME"] = value;
            }
        }

        #endregion
    }
}