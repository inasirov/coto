﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace COTO_RegOnly.UserControls
{
    public partial class DialogBox : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnConfirm.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnConfirm.UniqueID, "");
            btnClose.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnClose.UniqueID, "");
        }

        public void ShowMessage(string Message)
        {
            lblConfirmationMessage.Text = Message;
            lblWarningTitle.Text = "";
            //tdCaption.Visible = false;
            mpext5.Show();
        }

        public void ShowMessage(string Message, string Caption)
        {
            lblConfirmationMessage.Text = Message;
            lblWarningTitle.Text = Caption;
            //tdCaption.Visible = true;
            mpext5.Show();
        }

        private void Hide()
        {
            lblConfirmationMessage.Text = "";
            lblWarningTitle.Text = "";
            mpext5.Hide();
        }

        public void btnConfirm_Click(object sender, EventArgs e)
        {
            OnOkButtonPressed(e);
        }

        public void btnClose_Click(object sender, EventArgs e)
        {
            OnCancelButtonPressed(e);
        }

        public delegate void OkButtonPressedHandler(object sender, EventArgs args);
        public event OkButtonPressedHandler OkButtonPressed;
        protected virtual void OnOkButtonPressed(EventArgs e)
        {
            if (OkButtonPressed != null)
                OkButtonPressed(btnConfirm, e);
        }
        public delegate void CancelButtonPressedHandler(object sender, EventArgs args);
        public event CancelButtonPressedHandler CancelButtonPressed;
        protected virtual void OnCancelButtonPressed(EventArgs e)
        {
            if (CancelButtonPressed != null)
               CancelButtonPressed(btnClose, e);
        }
    }
}