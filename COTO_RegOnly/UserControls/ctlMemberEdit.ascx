﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls.ctlMemberEdit"
    CodeBehind="ctlMemberEdit.ascx.cs" %>
<script src="../Scripts/MaskedEditFix.js" type="text/javascript"></script>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="2" cellspacing="2" width="600">
                    <tr class="HeaderTitle">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="College of Occupational Therapists of Ontario" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Update" OnClick="btnUpdateClick"
                                ValidationGroup="PersonalValidation" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" width="100%" cellspacing="2" cellpadding="3">
                                <tr class="RowTitle">
                                    <td colspan="3">
                                        <asp:Label ID="lblPersonalInformationSectionTitle" CssClass="heading" runat="server"
                                            Text="Personal Information" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftTitle" style="width: 30%">
                                        <asp:Label ID="lblEmailLabel" runat="server" Text="Email: *" />
                                    </td>
                                    <td style="width: 3%">
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtEmailText" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmailText"
                                            ErrorMessage="<br />Please provide Email: Field is blank." Display="Dynamic"
                                            ValidationGroup="PersonalValidation" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmailText"
                                            ErrorMessage="<br />Please provide a valid email" Display="Dynamic" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                            ValidationGroup="PersonalValidation" />
                                        <asp:HiddenField ID="hfBirthDate" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftTitle">
                                        <asp:Label ID="lblHomePhoneLabel" runat="server" Text="Home Phone: *" />
                                    </td>
                                    <td style="width: 3%">
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtHomePhoneText" runat="server" />
                                        <ajaxToolkit:MaskedEditExtender ID="meeHomePhone" TargetControlID="txtHomePhoneText"
                                            Mask="(999) 999-9999" ClearMaskOnLostFocus="false" MaskType="None" OnFocusCssClass="MaskedEditFocus"
                                            OnInvalidCssClass="MaskedEditError" InputDirection="LeftToRight" ErrorTooltipEnabled="True"
                                            runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvHomePhone" runat="server" ControlToValidate="txtHomePhoneText"
                                            ErrorMessage="<br />Please provide Home Phone: field is blank." Display="Dynamic"
                                            ForeColor="Red" ValidationGroup="PersonalValidation"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revHomePhone" runat="server" ControlToValidate="txtHomePhoneText"
                                            ErrorMessage="<br />Home Phone number is invalid." Display="Dynamic" ValidationGroup="PersonalValidation"
                                            ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}$"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="3" valign="top" class="LeftTitle">
                                        <asp:Label ID="lblHomeAddressLabel" runat="server" Text="Home Address: *" />
                                    </td>
                                    <td style="width: 3%">
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtHomeAddress1Text" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvAddress1" runat="server" ControlToValidate="txtHomeAddress1Text"
                                            ForeColor="Red" ErrorMessage="<br />Please provide Address: Field is blank."
                                            Display="Dynamic" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 3%">
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtHomeAddress2Text" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 3%">
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtHomeAddress3Text" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftTitle">
                                        <asp:Label ID="lblCountry" runat="server" Text="Country:" />&nbsp;
                                        <asp:ImageButton ID="ibCountryHelp" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibCountryHelpClick" />&nbsp;*
                                    </td>
                                    <td style="width: 3%">
                                    </td>
                                    <td class="RightColumn">
                                        <%--<asp:TextBox ID="txtCountry" runat="server" />--%>
                                        <asp:DropDownList ID="ddlCountry" runat="server" OnSelectedIndexChanged="ddlCountrySelectedIndexChanged" AutoPostBack="true"  />
                                        <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry"
                                            InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide Country: Field is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftTitle">
                                        <asp:Label ID="lblProvince" runat="server" Text="Province/State: *" />
                                    </td>
                                    <td style="width: 3%">
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtProvince" runat="server" Visible="false" Columns="20" MaxLength="15" />
                                        <asp:DropDownList ID="ddlProvince" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvProvince" runat="server" ControlToValidate="ddlProvince"
                                            InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide Province: Field is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftTitle">
                                        <asp:Label ID="lblCityLabel" runat="server" Text="City: *" />
                                    </td>
                                    <td style="width: 3%">
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtCityText" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCityText"
                                            ForeColor="Red" ErrorMessage="<br />Please provide City: Field is blank." Display="Dynamic"
                                            ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="LeftTitle">
                                        <asp:Label ID="lblPostalCode" runat="server" Text="Postal Code:" />&nbsp;
                                        <asp:ImageButton ID="ibPostalCodeHelp" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibPostalCodeHelpClick" />&nbsp;*
                                    </td>
                                    <td style="width: 3%">
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtPostalCode" runat="server" />
                                        <ajaxToolkit:MaskedEditExtender ID="meePostalCode" TargetControlID="txtPostalCode"
                                            Mask="LNLNLN" ClearMaskOnLostFocus="false" MaskType="None" ErrorTooltipEnabled="True"
                                            runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvPostalCode" runat="server" ControlToValidate="txtPostalCode"
                                            SetFocusOnError="True" ErrorMessage="<br />Please provide Postal Code: Field is blank."
                                            Display="Dynamic" ValidationGroup="PersonalValidation" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revPostalCode" runat="server" ControlToValidate="txtPostalCode"
                                            ErrorMessage="<br />Please provide a valid postal code without any spaces" Display="Dynamic"
                                            ValidationExpression="^([abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][a-zA-Z][0-9][a-zA-Z][0-9])$"
                                            ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
