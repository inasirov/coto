﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberInsuranceNew.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlMemberInsuranceNew" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl" TagPrefix="rjs" %>


<div class="MainForm">
    <center>
    <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr class="HeaderTitle" align="right">
                <td>
                    <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 10 of 12" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <uc:MessageBox ID="omb" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <table class="memberInfo" style="width: 100%; padding: 3px;">
                        <tr class="RowTitle">
                            <td colspan="2">
                                <div>
                                    <asp:Label ID="lblInsuranceTitle" CssClass="heading"  runat="server" Text="Insurance" />
                                </div>
                                
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="2" style="text-align: left;">
                                <p>
                                    <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" />
                                </p>
                            </td>
                        </tr>--%>
                        <tr>
                            <td colspan="2" style="text-align: left; padding-top: 10px!important; padding-bottom: 10px!important;">
                                 <asp:Label ID="lblProfessionalLiabilityTitle" runat="server" Text="Professional Liability Insurance" Font-Bold="true"
                                    Font-Size="Medium" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <p>Professional liability insurance with a sexual abuse therapy and counseling fund endorsement is mandatory for registration. 
                                    The requirements for professional liability insurance are set out in <a href="https://www.coto.org/docs/default-source/bylaws/2017_bylaws.pdf?sfvrsn=2" target="_blank">Part 19 of the College Bylaws</a>. 
                                </p>
                                <p>The College has confirmed that the insurance coverage offered (as of August 1, 2017) by the Canadian Association of Occupational Therapists (CAOT), 
                                    the Ontario Society of Occupational Therapists (OSOT) and Aon Healthcare Advantage (Insurance Solutions for Occupational Therapists) 
                                    meets the College requirements. </p>
                                <p>
                                    The College does not endorse any insurance provider. It is the responsibility of each OT to determine which insurance provider 
                                    and policy best meets their needs. The above noted insurance providers are third parties and not affiliated with the College. 
                                    The College provides this information so OTs are aware of policies that the College has confirmed meet the requirements of the bylaws. 
                                </p>
                                <p>
                                    If you have insurance with another provider, you are required to send a copy of the policy to the College 
                                    to confirm that it is in keeping with the bylaws. 
                                </p>
                                <p><strong>Please confirm that your insurance details are correct. If the information is correct, no updates are required. 
                                    If the information is not correct, please make updates as required.</strong></p>
                                <%--<asp:Label ID="lblProfessionalLiabilityDesc" runat="server" Text="Documentation verifying purchase of insurance stating the certificate number and expiry date is required for all applications. If your insurance is not with the Canadian Association of Occupational Therapists (CAOT), the Ontario Society of Occupational Therapists (OSOT) or Aon Healthcare Advantage you must also forward a complete policy description. If you do not know your certificate number, please leave this field blank and send the College a copy of the policy once available.  A certificate of registration will not be issued until acceptable proof of insurance is received." />--%>
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>--%>
                        <tr>
                            <td colspan="2" style="text-align: left">
                               <asp:Label ID="lblCurrentInsurance" runat="server" Text="Current Insurance Information" Font-Bold="true" Font-Underline="true" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:30%; text-align: left;">
                                <asp:Label ID="lblPlanHeldWithTitle" CssClass="LeftTitle" runat="server" Text="Insurer Name:" />
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblPlanHeldWith" runat="server" />
                            </td>
                        </tr>
                        <tr id="trOtherInsurance" runat="server" visible="false">
                            <td style="text-align: left; vertical-align: top;">
                                <asp:Label ID="lblOtherTitle" runat="server" CssClass="LeftTitle" Text="Other (name of insurance provider if applicable): " />
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblOther" runat="server" />
                            </td>
                        </tr>
                        <tr id="trCertificate" runat="server">
                            <td style="text-align: left;">
                                <asp:Label ID="lblCertificateTitle" runat="server" CssClass="LeftTitle" Text="Certificate Number:" />
                            </td>
                            <td class="RightColumn">
                                 <asp:Label ID="lblCertificate" runat="server"/>                                       
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                <asp:Label ID="lblStartDatetitle" runat="server" CssClass="LeftTitle" Text="Start Date (MM/DD/YYYY):" />
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblStartDate" runat="server" />
                            </td>
                        </tr>
                        <tr id="trExpiryDate" runat="server">
                            <td style="text-align: left;">
                                <asp:Label ID="lblExpiryDateTitle" runat="server" CssClass="LeftTitle" Text="Expiry Date (MM/DD/YYYY):" />
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblExpiryDate" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    <asp:Label ID="lblGeneralMessage" runat="server" ForeColor="Red" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnUpdatePolicy" runat="server" Text="Update Policy" OnClick="btnUpdatePolicy_Click" CssClass="button"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                </td>
            </tr>
        </table>
            <ajaxToolkit:ModalPopupExtender ID="mpext6" runat="server" BackgroundCssClass="modalPopup6Background"
                    TargetControlID="dummy2" PopupControlID="pnlPopup6">
                </ajaxToolkit:ModalPopupExtender>
            <asp:HiddenField ID="dummy2" runat="server" />
            <asp:Panel ID="pnlPopup6" runat="server" CssClass="modalPopup6">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table class="memberInfo" style="width:100%">
                                <tr>
                                    <td colspan="2" style="text-align: left; padding: 10px!important">
                                        <asp:Label ID="lblAddNewPolicyTitle" runat="server" Text="Add New Insurance Information" Font-Bold="true" Font-Underline="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="lblErrorMessages" runat="server" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:40%; text-align: left;">
                                        <asp:Label ID="Label5" CssClass="LeftTitle" runat="server" Text="Insurer Name:" />
                                        <asp:HiddenField ID="hfInsuranceSEQN" runat="server" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlPlanHeldWith" runat="server" OnSelectedIndexChanged="ddlPlanHeldWithSelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvPlanHeldWith" runat="server" ControlToValidate="ddlPlanHeldWith" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select a value" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trOtherInsurance2" runat="server" visible="false">
                                    <td style="text-align: left; vertical-align: top;">
                                        <asp:Label ID="Label6" runat="server" CssClass="LeftTitle" Text="Other (name of insurance provider if applicable): " />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtOther" runat="server" Columns="25" Width="240px" MaxLength="25" />
                                        <asp:RequiredFieldValidator ID="rfvOther" runat="server" ControlToValidate="txtOther" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide a name of insurance provider" Display="Dynamic" ForeColor="Red" />
                                        <br />
                                        <asp:Label ID="lblSendFaxMessage" runat="server" Font-Bold="true" ForeColor="DarkRed" Text="You must submit a complete policy description to the College for review." />
                                    </td>
                                </tr>
                                <tr id="trCertificate2" runat="server">
                                    <td style="text-align: left;">
                                        <asp:Label ID="Label8" runat="server" CssClass="LeftTitle" Text="Certificate Number:" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtCertificate" runat="server" MaxLength="10" />
                                        <asp:RequiredFieldValidator ID="rfvCertificate" runat="server" ControlToValidate="txtCertificate" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide certificate number" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <asp:Label ID="Label9" runat="server" CssClass="LeftTitle" Text="Start Date (MM/DD/YYYY):" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtStartDate" runat="server" />&nbsp;
                                        <ajaxToolkit:CalendarExtender ID="ceStartDate" runat="server" TargetControlID="txtStartDate" Format="MM/dd/yyyy"  />
                                        <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="txtStartDate" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select Start Date" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trExpiryDate2" runat="server">
                                    <td style="text-align: left;">
                                        <asp:Label ID="Label10" runat="server" CssClass="LeftTitle" Text="Expiry Date (MM/DD/YYYY):" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtExpiryDate" runat="server" />&nbsp;
                                        <ajaxToolkit:CalendarExtender ID="ceExpiryDate" runat="server" TargetControlID="txtExpiryDate" Format="MM/dd/yyyy" />
                                        <asp:RequiredFieldValidator ID="rfvExpiryDate" runat="server" ControlToValidate="txtExpiryDate" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select Expiry Date" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="EN12" style="text-align:center; padding: 10px!important;" colspan="2" >
                                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSaveClick" class="modalOK"  ValidationGroup="PersonalValidation"/>&nbsp;
                                        <asp:Button ID="btnEditCancel" runat="server" Text="Cancel" class="modalCancel" OnClick="btnEditCancelCancelClick" />
                                    </td> 
                                </tr>
                            </table>
                         </ContentTemplate>
                     </asp:UpdatePanel>
                </asp:Panel>
        </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>

<style type="text/css">
    .modalPopup6 {
        -moz-box-shadow: 0 0 5px #000000;
        -webkit-box-shadow: 0 0 5px #000000;
        box-shadow: 0 0 5px #000000;
        padding: 0px;
        background-color: White;
        border-width: 1px;
        -moz-border-radius: 1px;
        border-style: solid;
        border-color: Black;
        width: 600px;
        z-index: 4001 !important;
    }
    .modalPopup6 table tr td{
        padding: 3px;
    }
    .modalPopup6Background {
        z-index: 4000 !important;
    }

    .modalPopup {
        z-index: 6001 !important;
    }

    .modalBackground {
        z-index: 6000 !important;
    }

    .modalOK
        {
            background-color: #9B9B9B;
            color: White;
            font-size: 14px;
            font-family: Arial,sans-serif;
            border-style: none;
            -moz-box-shadow: 1px 1px 2px #000000;
            -webkit-box-shadow: 1px 1px 2px #000000;
            box-shadow: 1px 1px 2px #000000;
            padding: 3px;
            border-radius: 3px;
            width: 100px;
            height: 30px;
            white-space: normal;
            vertical-align: top;
        }
        .modalCancel
        {
            color: rgb(61, 54, 40);
            border-color: rgb(113, 113, 113);
            border-style: none;
            border-radius: 3px;
            background-color: rgb(255, 255, 255);
            font-size: 14px;
            font-family: Arial,sans-serif;
            -moz-box-shadow: 1px 1px 2px #000000;
            -webkit-box-shadow: 1px 1px 2px #000000;
            box-shadow: 1px 1px 2px #000000;
            padding: 3px;
            width: 100px;
            height: 30px;
            white-space: normal;
            vertical-align: top;
        }
</style>
