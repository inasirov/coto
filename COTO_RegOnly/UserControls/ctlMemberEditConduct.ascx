﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberEditConduct.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlMemberEditConduct" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion1" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion2" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion3" />
                
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion5" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion6" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion7" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion8" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion9" />
            </Triggers>
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="HeaderTitle" align="right">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 9 of 12" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" style="width: 100%; padding: 3px; border-spacing: 2px">
                                <tr class="RowTitle" >
                                    <td colspan="3" style="text-align: left">
                                        <asp:Label ID="lblConductTitle" CssClass="heading" runat="server" Text="Conduct" />
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td style="text-align:left" colspan="3">
                                        <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" />
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td style="width: 15%">
                                        &nbsp;
                                    </td>
                                    <td style="width: 65%">
                                        &nbsp;
                                    </td>
                                    <td style="width: 20%">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left" colspan="3">
                                        <p>These questions pertain to occupational therapy practice, the practice of other professions, criminal offences and other conduct. </p>
                                        <p>All questions must be answered. A response of ‘Yes’ will require further explanation in the field provided. If you have missed a question, you will be prompted to provide an answer before you can proceed to the next page.</p>
                                        <p><b><u>Practice of Occupational Therapy</u></b></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion1Title" runat="server" Text="Have you ever been refused registration in 
                                an occupational therapy body that has not previously been reported to the College? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion1" runat="server" OnSelectedIndexChanged="ddlQuestion1SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion1" runat="server" ControlToValidate="ddlQuestion1" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion1Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="lblQuestion1DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion1Details" TextMode="MultiLine" runat="server" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rvfQuestion1Details" runat="server" ControlToValidate="txtQuestion1Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion2Title" runat="server" Text="Have you had a finding of professional misconduct, 
                                            incompetency, incapacity or a similar issue as an occupational therapist in any jurisdiction that has 
                                            not been previously reported to the College? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion2" runat="server" OnSelectedIndexChanged="ddlQuestion2SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion2" runat="server" ControlToValidate="ddlQuestion2" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion2Details" runat="server" visible="false">
                                    <td style="vertical-align: top;">
                                        <asp:Label ID="lblQuestion2DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion2Details" runat="server" TextMode="MultiLine" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion2Details" runat="server" ControlToValidate="txtQuestion2Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion8Title" runat="server" Text="Are you currently facing  a proceeding for professional misconduct, 
                                            incompetency, incapacity or a similar issue as an occupational therapist in any jurisdiction 
                                            that has not been previously reported to the College? *" />
                                        <p>Note: A proceeding includes an appeal process.  </p>
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion8" runat="server" OnSelectedIndexChanged="ddlQuestion8SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion8" runat="server" ControlToValidate="ddlQuestion8" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion8Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="lblQuestion8DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion8Details" runat="server" TextMode="MultiLine" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion8Details" runat="server" ControlToValidate="txtQuestion8Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                     <td colspan="3" style="vertical-align: top; text-align: left">
                                         <p><b><u>Practice of Other Professions</u></b></p>
                                     </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion3Title" runat="server" Text="Have you had a finding of professional misconduct, 
                                            incompetency, incapacity or a similar issue in any profession other than occupational therapy in 
                                            any jurisdiction that has not been previously reported to the College? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion3" runat="server" OnSelectedIndexChanged="ddlQuestion3SelectedIndexChanged" AutoPostBack="true" CausesValidation="false"/>
                                        <asp:RequiredFieldValidator ID="rfvQuestion3" runat="server" ControlToValidate="ddlQuestion3" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion3Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="lblQuestion3DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion3Details" runat="server" TextMode="MultiLine" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion3Details" runat="server" ControlToValidate="txtQuestion3Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion9Title" runat="server" Text="Are you currently facing  a proceeding for professional misconduct, 
                                            incompetency, incapacity or a similar issue in any profession other than occupational therapy in any jurisdiction 
                                            that has not been previously reported to the College? *" />
                                        <p>Note: A proceeding includes an appeal process.  </p>
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion9" runat="server" OnSelectedIndexChanged="ddlQuestion9SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion9" runat="server" ControlToValidate="ddlQuestion9" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion9Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="lblQuestion9DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion9Details" runat="server" TextMode="MultiLine" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion9Details" runat="server" ControlToValidate="txtQuestion9Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion4Title" runat="server" Text="Have you been found guilty of an offence related 
                                        to the practice of occupational therapy that has not been previously reported to the College? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion4" runat="server" OnSelectedIndexChanged="ddlQuestion4SelectedIndexChanged" AutoPostBack="true" CausesValidation="false"/>
                                        <asp:RequiredFieldValidator ID="rfvQuestion4" runat="server" ControlToValidate="ddlQuestion4" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion4Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="lblQuestion4DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion4Details" runat="server" TextMode="MultiLine" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion4Details" runat="server" ControlToValidate="txtQuestion4Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>--%>
                                <tr>
                                     <td colspan="3" style="vertical-align: top; text-align: left">
                                         <p><b><u>Criminal Offences</u></b></p>
                                     </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion5Title" runat="server" Text="Have you been found guilty of any offence that has not been previously reported to the College? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion5" runat="server" OnSelectedIndexChanged="ddlQuestion5SelectedIndexChanged" AutoPostBack="true" CausesValidation="false"/>
                                        <asp:RequiredFieldValidator ID="rfvQuestion5" runat="server" ControlToValidate="ddlQuestion5" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion5Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="lblQuestion5DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion5Details" runat="server" TextMode="MultiLine" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion5Details" runat="server" ControlToValidate="txtQuestion5Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                     <td colspan="3" style="vertical-align: top; text-align: left">
                                         <p><b><u>Other</u></b></p>
                                     </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="lblQuestion6Title" runat="server" Text="Is there anything else in your previous conduct that 
                                            would afford reasonable grounds for the belief that you lack the knowledge, skill, 
                                            and/or judgement to practise safely and ethically?   *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion6" runat="server" OnSelectedIndexChanged="ddlQuestion6SelectedIndexChanged" AutoPostBack="true" CausesValidation="false"/>
                                        <asp:RequiredFieldValidator ID="rfvQuestion6" runat="server" ControlToValidate="ddlQuestion6" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion6Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="lblQuestion6DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion6Details" runat="server" TextMode="MultiLine" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion6Details" runat="server" ControlToValidate="txtQuestion6Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion7Title" runat="server" Text="Have you ever had a finding of professional negligence  or malpractice, 
                                            which may or may not relate to your suitability to practice, which has not previously been reported to the College? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion7" runat="server" OnSelectedIndexChanged="ddlQuestion7SelectedIndexChanged" AutoPostBack="true" CausesValidation="false"/>
                                        <asp:RequiredFieldValidator ID="rfvQuestion7" runat="server" ControlToValidate="ddlQuestion7" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion7Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="lblQuestion7DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion7Details" TextMode="MultiLine" runat="server" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion7Details" runat="server" ControlToValidate="txtQuestion7Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
