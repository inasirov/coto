﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberEducationEditRenewal.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlMemberEducationEditRenewal" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="HeaderTitle" align="right">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 4 of 12" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" ValidationGroup="PersonalValidation" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" width="100%" cellspacing="2" cellpadding="3">
                                <tr class="RowTitle">
                                    <td colspan="4">
                                        <div>
                                            <asp:Label ID="lblPersonalInformationSectionTitle" CssClass="heading" runat="server"
                                            Text="Education" />
                                        </div>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <%--<asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" />--%>
                                    </td>
                                    <td colspan="2" style="text-align: right; font-weight:bold;">
                                        <p>
                                            <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/2021/2021_Annual_Renewal_Glossary_S4_EducationOtherThanOT_English.pdf" target="_blank">Glossary</a>&nbsp;/&nbsp;
                                            <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/2021/2021_Annual_Renewal_Glossary_S4_EducationOtherThanOT_French.pdf" target="_blank">Glossaire</a>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: left;">
                                        <asp:Label ID="lblErrorMessages" runat="server" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trEditConfirmation" runat="server" visible="false">
                                    <td colspan="4" style="text-align: left">
                                        <asp:Label ID="lblMessage" runat="server" Text="Your request to update your education information has been received." ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: left;">
                                        <span style="font-size: Medium; font-weight: bold;">Occupational Therapy Education</span>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: left;">
                                        <p>
                                            <span>Please verify that the information displayed below is correct. Provide additional
                                                information as necessary.&nbsp;<b>Only include information if you have completed an additional degree in occupational therapy (i.e. a Masters or PhD in Occupational Therapy)</b>.&nbsp;
                                                If the information displayed is incorrect, please&nbsp;<asp:LinkButton ID="lbtnUpdateEducation" runat="server" OnClick="lbtnUpdateEducationClick"
                                                Text="click here" />
                                                &nbsp;to send an email notification to the College. </span>
                                            <br />
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#EFEFDE" style="width:20%; border-top:1pt solid #C0C0C0; border-right:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                        &nbsp;
                                    </td>
                                    <td bgcolor="#EFEFDE" align="center" style="width: 27%; border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                        <span style="text-align: left; font-weight: bold;">Entry Degree / Diploma </span>
                                    </td>
                                    <td bgcolor="#EFEFDE" align="center" style="width: 27%; border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                        <span style="text-align: left; font-weight: bold;">Degree 2</span>
                                    </td>
                                    <td bgcolor="#EFEFDE" align="center" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                        <span style="text-align: left; font-weight: bold;">Degree 3</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#EFEFDE" align="left" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                        <span>Degree / Diploma </span>
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblDiplomaEntry" runat="server" />
                                        <asp:DropDownList ID="ddlDiplomaEntry" runat="server" CssClass="educationselect" OnSelectedIndexChanged="ddlDiplomaEntrySelectedIndexChanged" AutoPostBack="true" />
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblDiplomaDegree2" runat="server" />
                                        <asp:DropDownList ID="ddlDiplomaDegree2" runat="server"  CssClass="educationselect" OnSelectedIndexChanged="ddlDiplomaDegree2SelectedIndexChanged" AutoPostBack="true"/>
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblDiplomaDegree3" runat="server" />
                                        <asp:DropDownList ID="ddlDiplomaDegree3" runat="server" CssClass="educationselect" OnSelectedIndexChanged="ddlDiplomaDegree3SelectedIndexChanged" AutoPostBack="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#EFEFDE" align="left" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                        <span>Canadian University</span>
                                        <%--<asp:ImageButton ID="ibCanadianUniversity" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibCanadianUniversityClick" />--%>
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblCanadianUniversityEntryDegree" runat="server" />
                                        <asp:DropDownList ID="ddlCanadianUniversityEntryDegree" runat="server" CssClass="educationselect" OnSelectedIndexChanged="ddlCanadianUniversityEntryDegreeSelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvCanadianUniversityEntryDegree" runat="server" ControlToValidate="ddlCanadianUniversityEntryDegree"
                                            InitialValue="" ValidationGroup="EntryDegreeValidation" ErrorMessage="<br />Canadian University field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblCanadianUniversityDegree2" runat="server" />
                                        <asp:DropDownList ID="ddlCanadianUniversityDegree2" runat="server" CssClass="educationselect" OnSelectedIndexChanged="ddlCanadianUniversityDegree2SelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvCanadianUniversityDegree2" runat="server" ControlToValidate="ddlCanadianUniversityDegree2"
                                            InitialValue="" ValidationGroup="Degree2Validation" ErrorMessage="<br />Canadian University field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblCanadianUniversityDegree3" runat="server" />
                                        <asp:DropDownList ID="ddlCanadianUniversityDegree3" runat="server" CssClass="educationselect" OnSelectedIndexChanged="ddlCanadianUniversityDegree3SelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvCanadianUniversityDegree3" runat="server" ControlToValidate="ddlCanadianUniversityDegree3"
                                            InitialValue="" ValidationGroup="Degree3Validation" ErrorMessage="<br />Degree/Diploma field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#EFEFDE" align="left" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                        <span>Name of Other University <br /> (if applicable)</span>
                                        <%--<asp:ImageButton ID="ibOtherUniversity" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibOtherUniversityClick" />--%>
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblOtherUniversityEntry" runat="server" />
                                        <asp:DropDownList ID="ddlOtherUniversityEntry" runat="server" CssClass="educationselect" />
                                        <asp:RequiredFieldValidator ID="rfvOtherUniversityEntry" runat="server" ControlToValidate="ddlOtherUniversityEntry"
                                            InitialValue="" ValidationGroup="EntryDegreeValidation" ErrorMessage="<br />Other University field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblOtherUniversityDegree2" runat="server" />
                                        <asp:DropDownList ID="ddlOtherUniversityDegree2" runat="server" OnSelectedIndexChanged="ddlOtherUniversityDegree2SelectedIndexChanged" AutoPostBack="true"
                                        CssClass="educationselect"  />
                                        <asp:RequiredFieldValidator ID="rfvOtherUniversityDegree2" runat="server" ControlToValidate="ddlOtherUniversityDegree2"
                                            InitialValue="" ValidationGroup="Degree2Validation" ErrorMessage="<br />Other University field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblOtherUniversityDegree3" runat="server" />
                                        <asp:DropDownList ID="ddlOtherUniversityDegree3" runat="server" OnSelectedIndexChanged="ddlOtherUniversityDegree3SelectedIndexChanged" AutoPostBack="true"
                                        CssClass="educationselect"  />
                                        <asp:RequiredFieldValidator ID="rfvOtherUniversityDegree3" runat="server" ControlToValidate="ddlOtherUniversityDegree3"
                                            InitialValue="" ValidationGroup="Degree3Validation" ErrorMessage="<br />Other University field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                </tr>
                                <tr id="trOtherUniversityNotListed" runat="server" visible="false">
                                    <td bgcolor="#EFEFDE" align="left" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                        <span>Other University <br />(not listed above)</span>
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <%--<asp:Label ID="lblOtherNotListedEntry" runat="server" />--%>&nbsp;
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:TextBox ID="txtOtherNotListedDegree2" runat="server" Visible="false" />
                                        <asp:Label ID="lblOtherNotListedDegree2" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvOtherNotListedDegree2" runat="server" ControlToValidate="txtOtherNotListedDegree2"
                                            InitialValue="" ValidationGroup="Degree2Validation" ErrorMessage="<br />Other University field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:TextBox ID="txtOtherNotListedDegree3" runat="server" Visible="false"/>
                                        <asp:Label ID="lblOtherNotListedDegree3" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvOtherNotListedDegree3" runat="server" ControlToValidate="txtOtherNotListedDegree3"
                                            InitialValue="" ValidationGroup="Degree3Validation" ErrorMessage="<br />Other University field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#EFEFDE" align="left" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                        <span>Country</span>
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblCountryGradEntry" runat="server" />
                                        <asp:DropDownList ID="ddlCountryGradEntry" runat="server" CssClass="educationselect"  OnSelectedIndexChanged="ddlCountryGradEntrySelectIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvCountryGradEntry" runat="server" ControlToValidate="ddlCountryGradEntry"
                                            InitialValue="" ValidationGroup="EntryDegreeValidation" ErrorMessage="<br />Country field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblCountryGradDegree2" runat="server" />
                                        <asp:DropDownList ID="ddlCountryGradDegree2" runat="server" CssClass="educationselect" OnSelectedIndexChanged="ddlCountryGradDegree2SelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvCountryGradDegree2" runat="server" ControlToValidate="ddlCountryGradDegree2"
                                            InitialValue="" ValidationGroup="Degree2Validation" ErrorMessage="<br />Country field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblCountryGradDegree3" runat="server" />
                                        <asp:DropDownList ID="ddlCountryGradDegree3" runat="server" CssClass="educationselect" OnSelectedIndexChanged="ddlCountryGradDegree3SelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvCountryGradDegree3" runat="server" ControlToValidate="ddlCountryGradDegree3"
                                            InitialValue="" ValidationGroup="Degree3Validation" ErrorMessage="<br />Country field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#EFEFDE" align="left" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                        <span>Province/State</span>
                                        <%--<asp:ImageButton ID="ibProvinceState" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibProvinceStateClick" />--%>
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblProvinceEntry" runat="server" />
                                        <asp:DropDownList ID="ddlProvinceEntry" runat="server" CssClass="educationselect" />
                                        <asp:RequiredFieldValidator ID="rfvProvinceEntry" runat="server" ControlToValidate="ddlProvinceEntry"
                                            InitialValue="" ValidationGroup="EntryDegreeValidation" ErrorMessage="<br />Province/State field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" Enabled="false" />

                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblProvinceDegree2" runat="server" />
                                        <asp:DropDownList ID="ddlProvinceDegree2" runat="server" CssClass="educationselect" />
                                        <asp:RequiredFieldValidator ID="rfvProvinceDegree2" runat="server" ControlToValidate="ddlProvinceDegree2"
                                            InitialValue="" ValidationGroup="Degree2Validation" ErrorMessage="<br />Province/State field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" Enabled="false"/>
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                        <asp:Label ID="lblProvinceDegree3" runat="server" />
                                        <asp:DropDownList ID="ddlProvinceDegree3" runat="server" CssClass="educationselect" />
                                        <asp:RequiredFieldValidator ID="rfvProvinceDegree3" runat="server" ControlToValidate="ddlProvinceDegree3"
                                            InitialValue="" ValidationGroup="Degree3Validation" ErrorMessage="<br />Province/State field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false"  Enabled="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#EFEFDE" align="left" style="border:1pt solid #C0C0C0;">
                                        <span>Year of Graduation</span>
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-bottom:1pt solid #C0C0C0">
                                        <asp:Label ID="lblYearGradEntry" runat="server" />
                                        <asp:DropDownList ID="ddlYearGradEntry" runat="server" CssClass="educationselect" />
                                        <asp:RequiredFieldValidator ID="rfvYearGradEntry" runat="server" ControlToValidate="ddlYearGradEntry"
                                            InitialValue="" ValidationGroup="EntryDegreeValidation" ErrorMessage="<br />Year of Graduation field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-bottom:1pt solid #C0C0C0">
                                        <asp:Label ID="lblYearGradDegree2" runat="server" />
                                        <asp:DropDownList ID="ddlYearGradDegree2" runat="server" CssClass="educationselect" />
                                        <asp:RequiredFieldValidator ID="rfvYearGradDegree2" runat="server" ControlToValidate="ddlYearGradDegree2"
                                            InitialValue="" ValidationGroup="Degree2Validation" ErrorMessage="<br />Year of Graduation field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                    <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-bottom:1pt solid #C0C0C0">
                                        <asp:Label ID="lblYearGradDegree3" runat="server" />
                                        <asp:DropDownList ID="ddlYearGradDegree3" runat="server" CssClass="educationselect" />
                                        <asp:RequiredFieldValidator ID="rfvYearGradDegree3" runat="server" ControlToValidate="ddlYearGradDegree3"
                                            InitialValue="" ValidationGroup="Degree3Validation" ErrorMessage="<br />Year of Graduation field is blank."
                                            Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: left;">
                                        <span style="font-size: Medium; font-weight: bold;">Education Other Than Occupational Therapy </span>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: left;">
                                        <p>
                                            <span>Please verify that the information displayed below is correct. Provide additional
                                            information as necessary.&nbsp;Only include information if you have completed your degree.
                                        </p>
                                        <p>
                                            <strong>If your field of study is not listed please choose 'other field of study'.</strong>
                                        </p>
                                        <p>
                                            If the information displayed is incorrect, please&nbsp;
                                            <asp:LinkButton ID="lbtnUpdateEducation2" runat="server" OnClick="lbtnUpdateEducationClick" Text="click here" />&nbsp;
                                            to send an email notification to the College.</span>
                                            <br />
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%">
                                            <tr>
                                                <td bgcolor="#EFEFDE" style="width:22%; border-top:1pt solid #C0C0C0; border-right:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                                    &nbsp;
                                                </td>
                                                <td bgcolor="#EFEFDE" align="center" style="width:39%; border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <span style="text-align: left; font-weight: bold;">Degree 1</span>
                                                </td>
                                                <td bgcolor="#EFEFDE" align="center" style="width:39%; border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <span style="text-align: left; font-weight: bold;">Degree 2</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#EFEFDE" align="left" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                                    <span>Degree / Diploma </span>
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherDiplomaDegree1" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherDiplomaDegree1" runat="server" CssClass="educationselect2" OnSelectedIndexChanged="ddlOtherDiplomaDegree1SelectedIndexChanged" AutoPostBack="true" />
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherDiplomaDegree2" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherDiplomaDegree2" runat="server"  CssClass="educationselect2" OnSelectedIndexChanged="ddlOtherDiplomaDegree2SelectedIndexChanged" AutoPostBack="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#EFEFDE" align="left" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                                    <span>Canadian University</span>
                                                    <%--<asp:ImageButton ID="ibtnCanadianUniversityOther" ImageUrl="~/images/qmark.jpg" runat="server" OnClick="ibCanadianUniversityClick" />--%>
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherCanadianUniversityDegree1" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherCanadianUniversityDegree1" runat="server" CssClass="educationselect2" OnSelectedIndexChanged="ddlOtherCanadianUniversityDegree1SelectedIndexChanged" AutoPostBack="true" />
                                                    <asp:RequiredFieldValidator ID="rfvOtherCanadianUniversityDegree1" runat="server" ControlToValidate="ddlOtherCanadianUniversityDegree1"
                                                        InitialValue="" ValidationGroup="OtherDegree1Validation" ErrorMessage="<br />Canadian University field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherCanadianUniversityDegree2" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherCanadianUniversityDegree2" runat="server" CssClass="educationselect2" OnSelectedIndexChanged="ddlOtherCanadianUniversityDegree2SelectedIndexChanged" AutoPostBack="true" />
                                                    <asp:RequiredFieldValidator ID="rfvOtherCanadianUniversityDegree2" runat="server" ControlToValidate="ddlOtherCanadianUniversityDegree2"
                                                        InitialValue="" ValidationGroup="OtherDegree2Validation" ErrorMessage="<br />Canadian University field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#EFEFDE" align="left" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                                    <span>Name of Other University <br /> (if applicable)</span>
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherOtherUniversityDegree1" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherOtherUniversityDegree1" runat="server" OnSelectedIndexChanged="ddlOtherOtherUniversityDegree1SelectedIndexChanged" 
                                                    CssClass="educationselect2" AutoPostBack="true" Enabled="false"  />
                                                    <asp:RequiredFieldValidator ID="rfvOtherOtherUniversityDegree1" runat="server" ControlToValidate="ddlOtherOtherUniversityDegree1"
                                                        InitialValue="" ValidationGroup="OtherDegree1Validation" ErrorMessage="<br />Other University field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" Enabled="false" />
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherOtherUniversityDegree2" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherOtherUniversityDegree2" runat="server" OnSelectedIndexChanged="ddlOtherOtherUniversityDegree2SelectedIndexChanged" 
                                                    CssClass="educationselect2" AutoPostBack="true" Enabled="false" />
                                                    <asp:RequiredFieldValidator ID="rfvOtherOtherUniversityDegree2" runat="server" ControlToValidate="ddlOtherOtherUniversityDegree2"
                                                        InitialValue="" ValidationGroup="OtherDegree2Validation" ErrorMessage="<br />Other University field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" Enabled="false" />
                                                </td>
                                            </tr>
                                            <tr id="trOtherOtherUniversityNotListed" runat="server" visible="false">
                                                <td bgcolor="#EFEFDE" align="left" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                                    <span>Other University (not listed above)</span>
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <asp:TextBox ID="txtOtherOtherUniversityNotListedDegree1" runat="server" Visible="false" />
                                                    <asp:Label ID="lblOtherOtherUniversityNotListedDegree1" runat="server" Visible="false" />
                                                    <asp:RequiredFieldValidator ID="rfvOtherOtherUniversityNotListedDegree1" runat="server" ControlToValidate="txtOtherOtherUniversityNotListedDegree1"
                                                        InitialValue="" ValidationGroup="OtherDegree1Validation" ErrorMessage="<br />Other University (not listed) field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" Enabled="false" />
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <asp:TextBox ID="txtOtherOtherUniversityNotListedDegree2" runat="server" Visible="false" />
                                                    <asp:Label ID="lblOtherOtherUniversityNotListedDegree2" runat="server" Visible="false"/>
                                                    <asp:RequiredFieldValidator ID="rfvOtherOtherUniversityNotListedDegree2" runat="server" ControlToValidate="txtOtherOtherUniversityNotListedDegree2"
                                                        InitialValue="" ValidationGroup="OtherDegree2Validation" ErrorMessage="<br />Other University (not listed) field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" Enabled="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#EFEFDE" align="left" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0;border-left:1pt solid #C0C0C0;">
                                                    <span>Field of Study</span>&nbsp;
                                                    <%--<asp:ImageButton ID="ibtnFieldOfStudyHelp" ImageUrl="~/images/qmark.jpg" runat="server" OnClick="ibtnFieldOfStudyHelpClick" />--%>
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherFieldStudyDegree1" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherFieldStudyDegree1" runat="server" CssClass="educationselect2" />
                                                    <asp:RequiredFieldValidator ID="rfvOtherFieldStudyDegree1" runat="server" ControlToValidate="ddlOtherFieldStudyDegree1"
                                                        InitialValue="" ValidationGroup="OtherDegree1Validation" ErrorMessage="<br />Field of Study field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherFieldStudyDegree2" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherFieldStudyDegree2" runat="server" CssClass="educationselect2" />
                                                    <asp:RequiredFieldValidator ID="rfvOtherFieldStudyDegree2" runat="server" ControlToValidate="ddlOtherFieldStudyDegree2"
                                                        InitialValue="" ValidationGroup="OtherDegree2Validation" ErrorMessage="<br />Field of Study field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#EFEFDE" align="left" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-left:1pt solid #C0C0C0;">
                                                    <span>Year of Graduation</span>
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherYearGradDegree1" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherYearGradDegree1" runat="server" CssClass="educationselect2" />
                                                    <asp:RequiredFieldValidator ID="rfvOtherYearGradDegree1" runat="server" ControlToValidate="ddlOtherYearGradDegree1"
                                                        InitialValue="" ValidationGroup="OtherDegree1Validation" ErrorMessage="<br />Year of Graduation field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherYearGradDegree2" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherYearGradDegree2" runat="server" CssClass="educationselect2" />
                                                    <asp:RequiredFieldValidator ID="rfvOtherYearGradDegree2" runat="server" ControlToValidate="ddlOtherYearGradDegree2"
                                                        InitialValue="" ValidationGroup="OtherDegree2Validation" ErrorMessage="<br />Year of Graduation field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#EFEFDE" align="left" style="border:1pt solid #C0C0C0;">
                                                    <span>Country</span>
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-bottom:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherCountryDegree1" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherCountryDegree1" runat="server" CssClass="educationselect2" OnSelectedIndexChanged="ddlOtherCountryDegree1SelectedIndexChanged" AutoPostBack="true" />
                                                    <asp:RequiredFieldValidator ID="rfvOtherCountryDegree1" runat="server" ControlToValidate="ddlOtherCountryDegree1"
                                                        InitialValue="" ValidationGroup="OtherDegree1Validation" ErrorMessage="<br />Country field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0;border-bottom:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherCountryDegree2" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherCountryDegree2" runat="server" CssClass="educationselect2" OnSelectedIndexChanged="ddlOtherCountryDegree2SelectedIndexChanged" AutoPostBack="true" />
                                                    <asp:RequiredFieldValidator ID="rfvOtherCountryDegree2" runat="server" ControlToValidate="ddlOtherCountryDegree2"
                                                        InitialValue="" ValidationGroup="OtherDegree2Validation" ErrorMessage="<br />Country field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#EFEFDE" align="left" style="border:1pt solid #C0C0C0;">
                                                    <span>Province/State</span>
                                                    <%--<asp:ImageButton ID="ibtnProvinceStateOther" ImageUrl="~/images/qmark.jpg" runat="server" OnClick="ibProvinceStateClick" Height="16px" />--%>
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0; border-bottom:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherProvinceDegree1" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherProvinceDegree1" runat="server" CssClass="educationselect2" />
                                                    <asp:RequiredFieldValidator ID="rfvOtherProvinceDegree1" runat="server" ControlToValidate="ddlOtherProvinceDegree1"
                                                        InitialValue="" ValidationGroup="OtherDegree1Validation" ErrorMessage="<br />Province/State field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" Enabled="false" />
                                                </td>
                                                <td class="RightColumn ETD" valign="top" style="border-right:1pt solid #C0C0C0;border-top:1pt solid #C0C0C0;border-bottom:1pt solid #C0C0C0">
                                                    <asp:Label ID="lblOtherProvinceDegree2" runat="server" />
                                                    <asp:DropDownList ID="ddlOtherProvinceDegree2" runat="server" CssClass="educationselect2" />
                                                    <asp:RequiredFieldValidator ID="rfvOtherProvinceDegree2" runat="server" ControlToValidate="ddlOtherProvinceDegree2"
                                                        InitialValue="" ValidationGroup="OtherDegree2Validation" ErrorMessage="<br />Province/State field is blank."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" Enabled="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
