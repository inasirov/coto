﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlOfflinePayment.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlOfflinePayment" %>
<%@ Import Namespace="COTO_RegOnly.Classes" %>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnPrintForm" />
            </Triggers>
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <%--<asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                Visible="false" ValidationGroup="PersonalValidation" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" width="100%" cellspacing="2" cellpadding="3">
                                <tr class="RowTitle">
                                    <td>
                                        <div>
                                            <asp:Label ID="lblPersonalInformationSectionTitle" CssClass="heading" runat="server"
                                            Text="Offline Payment" />
                                        </div>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <p>
                                            For offline payment, please print this <asp:LinkButton ID="lbtnPrintForm" runat="server" Text="form" OnClick="lbtnPrintFormClick" />&nbsp;
                                            and return to the College. If you have paid using online banking, 
                                            you are not required to submit this form. Payment must be received by
                                            May 31, <%# HardcodedValues.RenewalYear%>.
                                        </p>
                                        <p>
                                            Your tax receipt will be available for download on the College website once you have completed the annual renewal form 
                                            and your payment has been processed (may take up to 2 business days).
                                        </p>
                                        <p>
                                            If you wish to print a PDF report of your annual renewal information please&nbsp;
                                            <asp:HyperLink ID="hlPrintReport" runat="server" Text="click here" Target="_blank" NavigateUrl='<%# GetPrintReportUrl() %>' />
                                            &nbsp;- this may take a few minutes.
                                            <%--<asp:LinkButton ID="lbtnPrintReport" runat="server" Text="click here" OnClick="lbtnPrintReportClick" />--%>
                                        </p>
                                        <p>
                                            If you would like to provide feedback about your renewal
                                            experience, please 
                                            <asp:LinkButton ID="lbtnFeedbackLink" runat="server" Text="click here" OnClick="lbtnFeedbackLinkClick" />
                                        </p>
                                        <p style="font-weight: bold;">
                                            Thank you for completing your annual renewal!
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <asp:HiddenField ID="hfDocumentFields" runat="server" />
                                        <%--<asp:TextBox ID="txtPDF_Report_fields" runat="server" TextMode="MultiLine" Rows="10" Columns="50" />--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
