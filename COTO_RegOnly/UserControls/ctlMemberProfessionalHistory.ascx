﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberProfessionalHistory.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlMemberProfessionalHistory" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
    
    <style type="text/css">
        body
        {
            font-family: Arial, Helvetica, sans-serif;
            /*font-size: 13px;*/
        }
        .errMessage
        {
            width:300px;
            border: 1px solid;
            margin: 10px 0px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            position: relative;
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('images/info.png');
        }
    </style>
    
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="2" width="100%">
                    <tr class="HeaderTitle">
                        <td class="HeaderTitle" align="right">
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 7 of 12" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr class="RowTitle">
                        <td>
                            <asp:Label ID="lblPersonalEmploymentInformationTitle" runat="server" CssClass="heading"
                                Text="Professional Registration" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                                <asp:Label ID="lblDenotesRequiredTitle" runat="server"
                                    Text="* denotes required field" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" width="100%">
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblRegisteredPracticeOtherProvince" runat="server" Text="Are you currently registered/licensed to practise as an occupational therapist in another province, state or country? "
                                            Font-Bold="true" />&nbsp;
                                            <asp:ImageButton ID="ibRegPracticeOtherProvince" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibRegPracticeOtherProvinceHelpClick" />*&nbsp;
                                        <asp:DropDownList ID="ddlRegisteredPracticeOtherProvince" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRegisteredPracticeOtherProvinceSelectedIndexChanged">  
                                        </asp:DropDownList>&nbsp;
                                        <asp:RequiredFieldValidator ID="rfvRegisteredPracticeOtherProvince" runat="server" ControlToValidate="ddlRegisteredPracticeOtherProvince"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="Please provide your Registered Practice Status in other provinces/states/countries."
                                            Display="None" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <ajaxToolkit:TabContainer ID="tcMainContainer" CssClass="NewsTab" runat="server"
                                            AutoPostBack="false" TabStripPlacement="Top" ScrollBars="Auto" ActiveTabIndex="0">
                                            <ajaxToolkit:TabPanel ID="tpJur1" runat="server" HeaderText="Jurisdiction 1" ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table cellpadding="2" cellspacing="4" width="100%">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblRegulatoryBody1Title" runat="server" Text="Regulatory Body " />&nbsp;
                                                                <asp:ImageButton ID="ibRegulatoryBody1" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibRegulatoryBodyHelpClick" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlRegulatoryBody1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRegulatoryBody1SelectedIndexChanged" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="LeftTitle">
                                                                <asp:Label ID="lblRegulatoryBodyOther" runat="server" Text="Regulatory Body Other " />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegulatoryBodyOther1" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvinceState1Label" runat="server" Text="Province/State" />&nbsp;
                                                                <asp:ImageButton ID="ibProvinceState1" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibProvinceStateHelpClick" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince1" runat="server" ControlToValidate="ddlProvince1"
                                                                    InitialValue="" ValidationGroup="Jurisdiction1Validation" ErrorMessage="Please provide all information for Jurisdiction 1: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry1Label" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry1" runat="server" ControlToValidate="ddlCountry1"
                                                                    InitialValue="" ValidationGroup="Jurisdiction1Validation" ErrorMessage="Please provide all information for Jurisdiction 1: Country is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblLicense1Label" runat="server" Text="Registration/License #" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtLicense1" runat="server" Columns="12" MaxLength="12" />
                                                                <asp:RequiredFieldValidator ID="rfvLicense1" runat="server" ControlToValidate="txtLicense1" EnableClientScript="false"
                                                                   ValidationGroup="Jurisdiction1Validation" ErrorMessage="Please provide all information for Jurisdiction 1: Registration/License # is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblExpiryDate1Label" runat="server" Text="Expiry Date (DD/MM/YYYY)" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtExpiryDate1" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar1" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtExpiryDate1" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvExpiryDate1" runat="server" ControlToValidate="txtExpiryDate1" EnableClientScript="false"
                                                                    ValidationGroup="Jurisdiction1Validation" ErrorMessage="Please provide all information for Jurisdiction 1: Expiry Date is blank." Display="None" ForeColor="Red" />
                                                                &nbsp;
                                                                <asp:CheckBox ID="cbNoExpiration1" runat="server" Text="No Expiration" OnCheckedChanged="cbNoExpiration1CheckedChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                            <ajaxToolkit:TabPanel ID="tpJur2" runat="server" HeaderText="Jurisdiction 2"
                                                ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table cellpadding="2" cellspacing="4" width="100%">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblRegulatoryBody2" runat="server" Text="Regulatory Body " />&nbsp;
                                                                <asp:ImageButton ID="ibRegulatoryBody2" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibRegulatoryBodyHelpClick" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlRegulatoryBody2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRegulatoryBody2SelectedIndexChanged"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="LeftTitle">
                                                                <asp:Label ID="lblRegulatoryBodyOther2" runat="server" Text="Regulatory Body Other " />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegulatoryBodyOther2" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvinceState2" runat="server" Text="Province/State" />&nbsp;
                                                                <asp:ImageButton ID="ibProvinceState2" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibProvinceStateHelpClick" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince2" runat="server" ControlToValidate="ddlProvince2"
                                                                    InitialValue="" ValidationGroup="Jurisdiction2Validation" ErrorMessage="Please provide all information for Jurisdiction 2: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry2" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry2" runat="server" ControlToValidate="ddlCountry2"
                                                                    InitialValue="" ValidationGroup="Jurisdiction2Validation" ErrorMessage="Please provide all information for Jurisdiction 2: Country is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblLicense2" runat="server" Text="Registration/License #" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtLicense2" runat="server" Columns="12" MaxLength="12" />
                                                                <asp:RequiredFieldValidator ID="rfvLicense2" runat="server" ControlToValidate="txtLicense2" EnableClientScript="false"
                                                                   ValidationGroup="Jurisdiction2Validation" ErrorMessage="Please provide all information for Jurisdiction 2: Registration/License # is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblExpiryDate2" runat="server" Text="Expiry Date (DD/MM/YYYY)" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtExpiryDate2" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar2" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtExpiryDate2" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvExpiryDate2" runat="server" ControlToValidate="txtExpiryDate2" EnableClientScript="false"
                                                                    ValidationGroup="Jurisdiction2Validation" ErrorMessage="Please provide all information for Jurisdiction 2: Expiry Date is blank." Display="None" ForeColor="Red" />
                                                                &nbsp;
                                                                <asp:CheckBox ID="cbNoExpiration2" runat="server" Text="No Expiration" OnCheckedChanged="cbNoExpiration2CheckedChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                            <ajaxToolkit:TabPanel ID="tpJur3" runat="server" HeaderText="Jurisdiction 3"
                                                ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table cellpadding="2" cellspacing="4" width="100%">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblRegulatoryBody3" runat="server" Text="Regulatory Body " />&nbsp;
                                                                <asp:ImageButton ID="ibRegulatoryBody3" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibRegulatoryBodyHelpClick" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlRegulatoryBody3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRegulatoryBody3SelectedIndexChanged" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="LeftTitle">
                                                                <asp:Label ID="lbllblRegulatoryBodyOther3" runat="server" Text="Regulatory Body Other " />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegulatoryBodyOther3" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvince3" runat="server" Text="Province/State" />&nbsp;
                                                                <asp:ImageButton ID="ibProvinceState3" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibProvinceStateHelpClick" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince3" runat="server" ControlToValidate="ddlProvince3"
                                                                    InitialValue="" ValidationGroup="Jurisdiction3Validation" ErrorMessage="Please provide all information for Jurisdiction 3: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry3" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry3" runat="server" ControlToValidate="ddlCountry3"
                                                                    InitialValue="" ValidationGroup="Jurisdiction3Validation" ErrorMessage="Please provide all information for Jurisdiction 3: Country is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblLicense3" runat="server" Text="Registration/License #" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtLicense3" runat="server" Columns="12" MaxLength="12" />
                                                                <asp:RequiredFieldValidator ID="rfvLicense3" runat="server" ControlToValidate="txtLicense3" EnableClientScript="false"
                                                                   ValidationGroup="Jurisdiction3Validation" ErrorMessage="Please provide all information for Jurisdiction 3: Registration/License # is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblExpiryDate3" runat="server" Text="Expiry Date (DD/MM/YYYY)" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtExpiryDate3" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar3" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtExpiryDate3" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvExpiryDate3" runat="server" ControlToValidate="txtExpiryDate3" EnableClientScript="false"
                                                                    ValidationGroup="Jurisdiction3Validation" ErrorMessage="Please provide all information for Jurisdiction 3: Expiry Date is blank." Display="None" ForeColor="Red" />
                                                                &nbsp;
                                                                <asp:CheckBox ID="cbNoExpiration3" runat="server" Text="No Expiration" OnCheckedChanged="cbNoExpiration3CheckedChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                            <ajaxToolkit:TabPanel ID="tpJur4" runat="server" HeaderText="Jurisdiction 4"
                                                ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table cellpadding="2" cellspacing="4" width="100%">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblRegulatoryBody4" runat="server" Text="Regulatory Body " />&nbsp;
                                                                <asp:ImageButton ID="ibRegulatoryBody4" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibRegulatoryBodyHelpClick" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlRegulatoryBody4" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRegulatoryBody4SelectedIndexChanged"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="LeftTitle">
                                                                <asp:Label ID="lblRegulatoryBodyOther4" runat="server" Text="Regulatory Body Other " />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegulatoryBodyOther4" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvince4" runat="server" Text="Province/State" />&nbsp;
                                                                <asp:ImageButton ID="ibProvinceState4" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibProvinceStateHelpClick" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince4" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince4" runat="server" ControlToValidate="ddlProvince4"
                                                                    InitialValue="" ValidationGroup="Jurisdiction4Validation" ErrorMessage="Please provide all information for Jurisdiction 4: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry4" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry4" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry4" runat="server" ControlToValidate="ddlCountry4"
                                                                    InitialValue="" ValidationGroup="Jurisdiction4Validation" ErrorMessage="Please provide all information for Jurisdiction 4: Country is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblLicense4" runat="server" Text="Registration/License #" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtLicense4" runat="server" Columns="12" MaxLength="12" />
                                                                <asp:RequiredFieldValidator ID="rfvLicense4" runat="server" ControlToValidate="txtLicense4" EnableClientScript="false"
                                                                   ValidationGroup="Jurisdiction4Validation" ErrorMessage="Please provide all information for Jurisdiction 4: Registration/License # is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblExpiryDate4" runat="server" Text="Expiry Date (DD/MM/YYYY)" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtExpiryDate4" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar4" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtExpiryDate4" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvExpiryDate4" runat="server" ControlToValidate="txtExpiryDate4" EnableClientScript="false"
                                                                    ValidationGroup="Jurisdiction4Validation" ErrorMessage="Please provide all information for Jurisdiction 4: Expiry Date is blank." Display="None" ForeColor="Red" />
                                                                &nbsp;
                                                                <asp:CheckBox ID="cbNoExpiration4" runat="server" Text="No Expiration" OnCheckedChanged="cbNoExpiration4CheckedChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                        </ajaxToolkit:TabContainer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" width="100%">
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblRegisterOtherProfession" runat="server" Text="Are you currently registered/licensed to practise in a profession other than occupational therapy in Ontario or elsewhere?"
                                            Font-Bold="true" />&nbsp;
                                            <asp:ImageButton ID="ibRegisterOtherProfession" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibRegisterOtherProfessionHelpClick" />*&nbsp;
                                        <asp:DropDownList ID="ddlOtherProfession" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlOtherProfessionSelectedIndexChanged"> 
                                        </asp:DropDownList>&nbsp;
                                        <asp:RequiredFieldValidator ID="rfvOtherProfession" runat="server" ControlToValidate="ddlOtherProfession"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="Please provide your Registered Practice Status in another profession other than OT."
                                            Display="None" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <ajaxToolkit:TabContainer ID="TabContainer1" CssClass="NewsTab" runat="server"
                                            AutoPostBack="false" TabStripPlacement="Top" ScrollBars="Auto" ActiveTabIndex="0">
                                            <ajaxToolkit:TabPanel ID="tpProf1" runat="server" HeaderText="Profession 1" ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table cellpadding="2" cellspacing="4" width="100%">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblNameProfessionOP1" runat="server" Text="Name Of Profession " />&nbsp;
                                                                
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtNameProfessionOP1" runat="server" Columns="12" MaxLength="12" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="LeftTitle">
                                                                <asp:Label ID="lblRegulatoryBodyOP1" runat="server" Text="Regulatory Body" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegulatoryBodyOP1" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                                <asp:RequiredFieldValidator ID="rfvRegulatoryBodyOP1" runat="server" ControlToValidate="txtRegulatoryBodyOP1" EnableClientScript="false"
                                                                   ValidationGroup="Profession1Validation" ErrorMessage="Please provide all information for Profession 1: Regulatory Body is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvinceOP1" runat="server" Text="Province/State" />&nbsp;
                                                                <asp:ImageButton ID="ibProvinceStateOP1" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibProvinceStateHelpClick" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvinceOP1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvinceOP1" runat="server" ControlToValidate="ddlProvinceOP1"
                                                                    InitialValue="" ValidationGroup="Profession1Validation" ErrorMessage="Please provide all information for Profession 1: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountryOP1" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountryOP1" runat="server" />
                                                                 <asp:RequiredFieldValidator ID="rfvCountryOP1" runat="server" ControlToValidate="ddlCountryOP1"
                                                                    InitialValue="" ValidationGroup="Profession1Validation" ErrorMessage="Please provide all information for Profession 1: Country is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblLicenseOP1" runat="server" Text="Registration/License #" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtLicenseOP1" runat="server" Columns="12" MaxLength="12" />
                                                                <asp:RequiredFieldValidator ID="rfvLicenseOP1" runat="server" ControlToValidate="txtLicenseOP1" EnableClientScript="false"
                                                                   ValidationGroup="Profession1Validation" ErrorMessage="Please provide all information for Profession 1: Registration/License # is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblExpireDateOP1" runat="server" Text="Expiry Date (DD/MM/YYYY)" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtExpireDateOP1" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar5" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtExpireDateOP1" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvExpireDateOP1" runat="server" ControlToValidate="txtExpireDateOP1" EnableClientScript="false"
                                                                    ValidationGroup="Profession1Validation" ErrorMessage="Please provide all information for Profession 1: Expiry Date is blank." Display="None" ForeColor="Red" />
                                                                &nbsp;
                                                                <asp:CheckBox ID="cbNoExpirationOP1" runat="server" Text="No Expiration" OnCheckedChanged="cbNoExpirationOP1CheckedChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                            <ajaxToolkit:TabPanel ID="tpProf2" runat="server" HeaderText="Profession 2"
                                                ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table cellpadding="2" cellspacing="4" width="100%">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblNamePRofessionOP2" runat="server" Text="Name Of Profession " />&nbsp;
                                                                
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtNameProfessionOP2" runat="server" Columns="12" MaxLength="12" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="LeftTitle">
                                                                <asp:Label ID="lblRegulatoryBodyOP2" runat="server" Text="Regulatory Body" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegulatoryBodyOP2" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                                <asp:RequiredFieldValidator ID="rfvRegulatoryBodyOP2" runat="server" ControlToValidate="txtRegulatoryBodyOP2" EnableClientScript="false"
                                                                   ValidationGroup="Profession2Validation" ErrorMessage="Please provide all information for Profession 2: Regulatory Body is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvinceOP2" runat="server" Text="Province/State" />&nbsp;
                                                                <asp:ImageButton ID="ibProvinceStateOP2" ImageUrl="~/images/qmark.jpg" runat="server"  OnClick="ibProvinceStateHelpClick" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvinceOP2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvinceOP2" runat="server" ControlToValidate="ddlProvinceOP2"
                                                                    InitialValue="" ValidationGroup="Profession2Validation" ErrorMessage="Please provide all information for Profession 2: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountryOP2" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountryOP2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountryOP2" runat="server" ControlToValidate="ddlCountryOP2"
                                                                    InitialValue="" ValidationGroup="Profession2Validation" ErrorMessage="Please provide all information for Profession 2: Country is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblLicenseOP2" runat="server" Text="Registration/License #" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtLicenseOP2" runat="server" Columns="12" MaxLength="12" />
                                                                <asp:RequiredFieldValidator ID="rfvLicenseOP2" runat="server" ControlToValidate="txtLicenseOP2" EnableClientScript="false"
                                                                   ValidationGroup="Profession2Validation" ErrorMessage="Please provide all information for Profession 2: Registration/License # is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblExpireDateOP2" runat="server" Text="Expiry Date (DD/MM/YYYY)" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtExpireDateOP2" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar6" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtExpireDateOP2" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvExpireDateOP2" runat="server" ControlToValidate="txtExpireDateOP2" EnableClientScript="false"
                                                                    ValidationGroup="Profession2Validation" ErrorMessage="Please provide all information for Profession 2: Expiry Date is blank." Display="None" ForeColor="Red" />
                                                                &nbsp;
                                                                <asp:CheckBox ID="cbNoExpirationOP2" runat="server" Text="No Expiration" OnCheckedChanged="cbNoExpirationOP2CheckedChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                        </ajaxToolkit:TabContainer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" UseSubmitBehavior="true" TabIndex="0"/>--%>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="errorsPanel" runat="server"  Style="display: none; width: 750px;" CssClass="modalPopup" DefaultButton="okBtn">
                    <table width="100%" cellspacing="2" cellpadding="2">
                        <tr class="topHandleRed">
                            <td align="left" runat="server" id="tdCaption">
                                <asp:Label ID="lblCaption" runat="server" Text="Error Messages:" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="text-align: left">
                                    <asp:Label ID="lblErrorMessage" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="GeneralValidation" />
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Jurisdiction1Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary3" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Jurisdiction2Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary4" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Jurisdiction3Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary5" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Jurisdiction4Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary6" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Profession1Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary7" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Profession2Validation" />
                                    <div style="text-align: right">
                                        <asp:Button ID="okBtn" runat="server" Text="Ok" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="modalPopupEx" runat="server" PopupControlID="errorsPanel"
                    TargetControlID="invisibleTarget" CancelControlID="okBtn" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Label ID="invisibleTarget" runat="server" Style="display: none" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
