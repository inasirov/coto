﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Classes;
using System.Security.Cryptography;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberDeclaration : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;

        private string PrevStep = WebConfigItems.Step10;
        private string NextStep = WebConfigItems.Step12;
        private const int CurrentStep = 11;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

                if (string.IsNullOrEmpty((string)Session["ID"]))
                {
                    Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                    return;
                }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.RenewalStep = CurrentStep;
                BindLists();
                BindData();
            }
        }

        protected void btnBackClick(object sender, EventArgs e)
        {
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }
        protected void btnUpdateClick(object sender, EventArgs e)
        {
            var retMessage = CheckDeclaration();
            if (string.IsNullOrEmpty(retMessage))
            {
                UpdateUserDeclaration();
            }
            else
            {
                ShowMessage(string.Format("<font color='Red'>{0}</font>", retMessage), "Error:");
                return;
            }

            //Response.Redirect("MemberDisplay.aspx");

            string fullName = string.Empty;
            if (CurrentUser != null)
            {
                fullName = CurrentUser.FullName;
            }
            UpdateSteps(1);
            Response.Redirect(NextStep);
            //Response.Redirect(NextStep + "&ID=" + CurrentUserId + "&FULL_NAME=" + HttpUtility.UrlEncode(fullName));
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            var retMessage = CheckDeclaration();
            if (string.IsNullOrEmpty(retMessage))
            {
                UpdateUserDeclaration();
            }
            else
            {
                ShowMessage(string.Format("<font color='Red'>{0}</font>", retMessage), "Error:");
                return;
            }

            //Response.Redirect("MemberDisplay.aspx");

            string fullName = string.Empty;
            if (CurrentUser != null)
            {
                fullName = CurrentUser.FullName;
            }
            UpdateSteps(1);
            ///Response.Redirect(NextStep + "&ID=" + CurrentUserId + "&FULL_NAME=" + HttpUtility.UrlEncode(fullName));
            Response.Redirect(NextStep);
        }

        protected void ibLanguageServiceHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("Please indicate all language(s) in which you can competently provide OT services.", "Help Languages");
        }

        #endregion

        #region Methods

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        protected void UpdateUserDeclaration()
        {
            int currentYear = DateTime.Now.Year;
            User user = new User();

            user.Id = CurrentUserId;
            user.DeclarationInfo = new Classes.Declaration();

            user.DeclarationInfo.RegistrationDeclaration = ddlAgree.SelectedValue;
            user.DeclarationInfo.QADeclaration = ddlUnderstand.SelectedValue;

            var repository = new Repository();
            repository.UpdateUserDeclarationInfoLogged( CurrentUserId, user, currentYear);

            if (ddlUnderstand.SelectedValue.ToUpper() == "NO")
            {
                string message = string.Format("Member ID: {0}; ", CurrentUserId); ;
                 message += string.Format("Date: {0}" , DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

                 string emailSubject = "Registrant did not understand QA Declaration at Annual Renewal " + WebConfigItems.GetTestLabel;
                string emailTo = WebConfigItems.RegistrationQualityEmail;
                var tool = new Tools();
                tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);
            }

        }

        protected string CheckDeclaration()
        {
            string retValue = string.Empty;
            if (ddlAgree.SelectedValue.ToUpper() != "YES")
            {
                retValue = "You must answer 'Yes' to the Registration Declaration question to proceed with annual renewal.";
            }

            if (string.IsNullOrEmpty(ddlUnderstand.SelectedValue))
            {
                retValue += "You must answer the  Quality Assurance Declaration to proceed with renewal.";
            }

            return retValue;
        }

        protected void BindLists()
        {
            var list1 = new List<GenClass>();
            list1.Add(new GenClass { Code = "", Description = "" });
            list1.Add(new GenClass { Code = "Yes", Description = "Yes" });
            list1.Add(new GenClass { Code = "No", Description = "No" });

            ddlAgree.DataSource = list1;
            ddlAgree.DataValueField = "Code";
            ddlAgree.DataTextField = "Description";
            ddlAgree.DataBind();

            ddlUnderstand.DataSource = list1;
            ddlUnderstand.DataValueField = "Code";
            ddlUnderstand.DataTextField = "Description";
            ddlUnderstand.DataBind();

        }

        protected void BindData()
        {
            var repository = new Repository();
            int currentYear = DateTime.Now.Year;

            var user = repository.GetUserDeclarationInfo(CurrentUserId, currentYear);
            if (user != null)
            {
                if (user.DeclarationInfo != null)
                {
                    ddlAgree.SelectedValue = user.DeclarationInfo.RegistrationDeclaration;
                    ddlUnderstand.SelectedValue = user.DeclarationInfo.QADeclaration;
                }
            }

            var user2 = repository.GetUserInfo(CurrentUserId);
            if (user2 != null)
            {
                CurrentUser = user2;
                lblDeclarationTitle.Text = "Declaration for " + user2.FullName;
            }
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");
                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                if (SessionParameters.CurrentUser != null)
                {
                    return SessionParameters.CurrentUser;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUser = value;
            }
        }

        #endregion

        protected void lbtnOfflinePaymentClick(object sender, EventArgs e)
        {
            Response.Redirect(WebConfigItems.Step12_OfflinePayment);
        }

        protected void lbtnAnnualRegistrationSummaryClick(object sender, EventArgs e)
        {
            Response.Redirect(WebConfigItems.Step12_RegistrationSummary);
        }

        protected void lbtnRenewalFeedbackClick(object sender, EventArgs e)
        {
            Response.Redirect(WebConfigItems.Step12_Feedback);
        }
    }
}