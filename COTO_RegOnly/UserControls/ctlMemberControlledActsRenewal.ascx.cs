﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Classes;
using System.Security.Cryptography;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberControlledActsRenewal : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;
        private string PrevStep = WebConfigItems.Step6;
        private string NextStep = WebConfigItems.Step8;
        private const int CurrentStep = 7;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Request.QueryString.Count > 0)
                //securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.RenewalStep = CurrentStep;
                BindLists();
                BindData();
            }
            DataBind();
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(-1);
            Session["CustomAction"] = "renewal";
            Response.Redirect(PrevStep); // + "&ID=" + CurrentUserId + "&CustomAction=renewal");
        }

        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            Page.Validate("PersonalValidation");
            if (Page.IsValid)
            {
                UpdateUserInfo();
                //Response.Redirect("MemberDisplay.aspx");
                UpdateSteps(1);
                Session["CustomAction"] = "renewal";
                Response.Redirect(NextStep);
            }
        }

        #endregion

        #region Methods

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        protected void UpdateUserInfo()
        {
            int currentYear = DateTime.Now.Year;
            var act = new ControlledAct();

            act.Comm_Diagnosis_1 = ddlQuestion1.SelectedValue;
            act.Below_Dermis_2 = ddlQuestion2.SelectedValue;
            act.Setting_Casting_3 = ddlQuestion3.SelectedValue;
            act.Spinal_Manipulation_4 = ddlQuestion4.SelectedValue;
            act.Administer_Substance_5 = ddlQuestion5.SelectedValue;
            act.Instr_Hand_Finger_6 = ddlQuestion6.SelectedValue;
            act.Xrays_7 = ddlQuestion7.SelectedValue;
            act.Drugs_8 = ddlQuestion8.SelectedValue;
            act.Vision_9 = ddlQuestion9.SelectedValue;
            act.Psyhotherapy_10 = ddlQuestion10.SelectedValue;
            act.Acupuncture_11 = ddlQuestion11.SelectedValue;

            var repository = new Repository();
            repository.UpdateUserControlledActsInfoLogged(CurrentUserId, act, currentYear);

            //if (ddlQuestion1.SelectedValue.ToUpper() == "YES" || ddlQuestion2.SelectedValue.ToUpper() == "YES" || ddlQuestion3.SelectedValue.ToUpper() == "YES" 
            //    || ddlQuestion4.SelectedValue.ToUpper() == "YES" || ddlQuestion5.SelectedValue.ToUpper() == "YES" || ddlQuestion6.SelectedValue.ToUpper() == "YES" 
            //    || ddlQuestion7.SelectedValue.ToUpper() == "YES" || ddlQuestion8.SelectedValue.ToUpper() == "YES" || ddlQuestion9.SelectedValue.ToUpper() == "YES" 
            //    || ddlQuestion10.SelectedValue.ToUpper() == "YES" || ddlQuestion11.SelectedValue.ToUpper() == "YES")
            //{
            //    string message = string.Format("Member ID: {0}; ", CurrentUserId); ;
            //    message += string.Format("Date: {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

            //    string emailSubject = "Registrant Requires Controlled Act Review";
            //    string emailTo = WebConfigItems.RegistrationSupportEmail;
            //    var tool = new Tools();
            //    tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);
            //}
        }

        protected void BindLists()
        {
            var list1 = new List<GenClass>();
            //list1.Add(new GenClass { Code = "", Description = "" });
            list1.Add(new GenClass { Code = "No", Description = "No" });
            list1.Add(new GenClass { Code = "Yes", Description = "Yes" });

            ddlQuestion1.DataSource = list1;
            ddlQuestion1.DataValueField = "Code";
            ddlQuestion1.DataTextField = "Description";
            ddlQuestion1.DataBind();

            ddlQuestion2.DataSource = list1;
            ddlQuestion2.DataValueField = "Code";
            ddlQuestion2.DataTextField = "Description";
            ddlQuestion2.DataBind();

            ddlQuestion3.DataSource = list1;
            ddlQuestion3.DataValueField = "Code";
            ddlQuestion3.DataTextField = "Description";
            ddlQuestion3.DataBind();

            ddlQuestion4.DataSource = list1;
            ddlQuestion4.DataValueField = "Code";
            ddlQuestion4.DataTextField = "Description";
            ddlQuestion4.DataBind();

            ddlQuestion5.DataSource = list1;
            ddlQuestion5.DataValueField = "Code";
            ddlQuestion5.DataTextField = "Description";
            ddlQuestion5.DataBind();

            ddlQuestion6.DataSource = list1;
            ddlQuestion6.DataValueField = "Code";
            ddlQuestion6.DataTextField = "Description";
            ddlQuestion6.DataBind();

            ddlQuestion7.DataSource = list1;
            ddlQuestion7.DataValueField = "Code";
            ddlQuestion7.DataTextField = "Description";
            ddlQuestion7.DataBind();

            ddlQuestion8.DataSource = list1;
            ddlQuestion8.DataValueField = "Code";
            ddlQuestion8.DataTextField = "Description";
            ddlQuestion8.DataBind();

            ddlQuestion9.DataSource = list1;
            ddlQuestion9.DataValueField = "Code";
            ddlQuestion9.DataTextField = "Description";
            ddlQuestion9.DataBind();

            ddlQuestion10.DataSource = list1;
            ddlQuestion10.DataValueField = "Code";
            ddlQuestion10.DataTextField = "Description";
            ddlQuestion10.DataBind();

            ddlQuestion11.DataSource = list1;
            ddlQuestion11.DataValueField = "Code";
            ddlQuestion11.DataTextField = "Description";
            ddlQuestion11.DataBind();
        }

        protected void BindData()
        {
            var repository = new Repository();
            int currentYear = DateTime.Now.Year;

            var act = repository.GetUserControlledActInfo(CurrentUserId, currentYear);
            if (act != null)
            {
                var item1 = ddlQuestion1.Items.FindByValue(act.Comm_Diagnosis_1);
                if (item1!=null) ddlQuestion1.SelectedValue = act.Comm_Diagnosis_1;
                var item2 = ddlQuestion2.Items.FindByValue(act.Below_Dermis_2);
                if (item2 != null) ddlQuestion2.SelectedValue = act.Below_Dermis_2;
                var item3 = ddlQuestion3.Items.FindByValue(act.Setting_Casting_3);
                if (item3 != null) ddlQuestion3.SelectedValue = act.Setting_Casting_3;
                var item4 = ddlQuestion4.Items.FindByValue(act.Spinal_Manipulation_4);
                if (item4 != null) ddlQuestion4.SelectedValue = act.Spinal_Manipulation_4;
                var item5 = ddlQuestion5.Items.FindByValue(act.Administer_Substance_5);
                if (item5 != null) ddlQuestion5.SelectedValue = act.Administer_Substance_5;
                var item6 = ddlQuestion6.Items.FindByValue(act.Instr_Hand_Finger_6);
                if (item6 != null) ddlQuestion6.SelectedValue = act.Instr_Hand_Finger_6;
                var item7 = ddlQuestion7.Items.FindByValue(act.Xrays_7);
                if (item7 != null) ddlQuestion7.SelectedValue = act.Xrays_7;
                var item8 = ddlQuestion8.Items.FindByValue(act.Drugs_8);
                if (item8 != null) ddlQuestion8.SelectedValue = act.Drugs_8;
                var item9 = ddlQuestion9.Items.FindByValue(act.Vision_9);
                if (item9 != null) ddlQuestion9.SelectedValue = act.Vision_9;
                var item10 = ddlQuestion10.Items.FindByValue(act.Psyhotherapy_10);
                if (item10 != null) ddlQuestion10.SelectedValue = act.Psyhotherapy_10;
                var item11 = ddlQuestion11.Items.FindByValue(act.Acupuncture_11);
                if (item11 != null) ddlQuestion11.SelectedValue = act.Acupuncture_11;

            }

            var user2 = repository.GetUserInfo(CurrentUserId);
            if (user2 != null)
            {
                //CurrentUser = user2;
                lblControlledActsTitle.Text = "Controlled Acts Performed by " + user2.FullName;
            }
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }
        #endregion
    }
}