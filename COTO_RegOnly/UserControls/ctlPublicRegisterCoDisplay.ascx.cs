﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlPublicRegisterCoDisplay : System.Web.UI.UserControl
    {
        private const string VIEW_STATE_CURRENT_USER_ID = "CurrentUserId";

        protected void Page_Load(object sender, EventArgs e)
        {
            string CurrentUserId = string.Empty;

            if (Session[VIEW_STATE_CURRENT_USER_ID] != null)
            {
                CurrentUserId = (string)Session[VIEW_STATE_CURRENT_USER_ID];
                Session[VIEW_STATE_CURRENT_USER_ID] = null;
            }
            if (!string.IsNullOrEmpty(CurrentUserId))
            {
                BindCorporationInfo(CurrentUserId);
            }
        }

        protected void lbtnSearchAgainClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PublicRegister.aspx");
        }

        protected void BindCorporationInfo(string CurrentId)
        {
            var repository = new Repository();

            trCorporationEmptyRow.Visible = true;
            trCorporationNoEmptyRow.Visible = false;

            Corporation corp = repository.GetUserCorporationInfo(CurrentId);
            if (corp != null)
            {
                trCorporationEmptyRow.Visible = false;
                trCorporationNoEmptyRow.Visible = true;

                lblCorpNameText.Text = corp.FullName;
                lblCorpCertificateText.Text = corp.Certificate;
                lblCorpStatusText.Text = corp.Status;
                if (corp.StatusEffectiveDate != null)
                {
                    lblCorpStatusEffectiveDateText.Text = ((DateTime)corp.StatusEffectiveDate).ToString("MMMMM dd, yyyy");
                }
                lblCorpPracticeNameText.Text = corp.PracticeName;
                if (corp.BusinessAddress != null)
                {
                    if (!string.IsNullOrEmpty(corp.BusinessAddress.Address1))
                    {
                        lblCorpBusinessAddressText.Text = corp.BusinessAddress.Address1 + "<br />";
                    }
                    if (!string.IsNullOrEmpty(corp.BusinessAddress.Address2))
                    {
                        lblCorpBusinessAddressText.Text += corp.BusinessAddress.Address2 + "<br />";
                    }
                    if (!string.IsNullOrEmpty(corp.BusinessAddress.City))
                    {
                        lblCorpBusinessAddressText.Text += corp.BusinessAddress.City;
                    }
                    if (!string.IsNullOrEmpty(corp.BusinessAddress.Province))
                    {
                        lblCorpBusinessAddressText.Text += ", " + corp.BusinessAddress.Province;
                    }
                    lblCorpBusinessAddressText.Text += "<br />";
                    if (!string.IsNullOrEmpty(corp.BusinessAddress.PostalCode))
                    {
                        lblCorpBusinessAddressText.Text += corp.BusinessAddress.PostalCode;
                    }
                }
                lblCorpPhoneText.Text = corp.Phone;
                lblCorpEmailText.Text = corp.Email;
                if (corp.Shareholders != null && corp.Shareholders.Count > 0)
                {
                    foreach (var item in corp.Shareholders)
                    {
                        lblCorpShareholdersText.Text += string.Format("{0} {1} {2} <br />", item.FirstName, item.LastName, item.Registration);
                    }
                }
                else
                {
                    lblCorpShareholdersText.Text = "None";
                }
            }
        }

    }
}
