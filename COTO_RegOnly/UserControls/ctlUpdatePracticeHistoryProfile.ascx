﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlUpdatePracticeHistoryProfile.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlUpdatePracticeHistoryProfile" %>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="HeaderTitle" align="left">
                        <td colspan="2">
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Update Practice History Profile" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red" />
                        </td>   
                    </tr>
                    <tr>
                        <td style="text-align:left;" colspan="2">
                            <p>
                                Please provide details on the information that has changed in your practice history profile. 
                            </p>
                            <p>
                                Changes will be received and updated by the College and will not be reflected immediately on your online renewal. 
                                We may contact you if we require additional information.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="RightColumn">
                            <span style="font-weight:bold">Practice History Change Details:</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPracticeProfileChanges" runat="server" Rows="6" Columns="60" TextMode="MultiLine" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right" valign="middle" style="background-color: #ffffff" colspan="2">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/Images/btn_update.jpg" OnClick="btnSubmitClick" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>