﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlOT_DirectoryProcess : System.Web.UI.UserControl
    {

        #region Consts
        private const string VIEW_STATE_CURRENT_USER_ID = "CurrentUserId";
        private const string VIEW_STATE_CURRENT_FIRST_NAME = "CurrentFirstName";
        private const string VIEW_STATE_CURRENT_LAST_NAME = "CurrentLastName";
        private const string VIEW_STATE_CURRENT_MIDDLE_NAME = "CurrentFirstName";
        private const string VIEW_STATE_CURRENT_REGISTR_NUMBER = "CurrentMiddleName";
        private const string VIEW_STATE_CURRENT_CITY = "CurrentCity";
        private const string VIEW_STATE_CURRENT_POSTAL_CODE = "CurrentPostalCode";
        private const string VIEW_STATE_CURRENT_LANGUAGE = "CurrentLanguage";
        private const string VIEW_STATE_CURRENT_FUNDING = "CurrentFunding";
        private const string VIEW_STATE_CURRENT_MAJOR_SERVICE = "CurrentMajorService";
        private const string VIEW_STATE_CURRENT_CLIENT_AGE = "CurrentClientAge";
        private const string VIEW_STATE_CURRENT_PRACTICE_SETTING = "CurrentPracticeSetting";
        private const string VIEW_STATE_CURRENT_LETTER = "CurrentLetter";
        private const string VIEW_STATE_CURRENT_START_DISPLAY = "CurrentStartDisplay";
        private const int itemsOnPage = 25;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ReadSessionParameters();
                BindResult();
            }
        }

        protected void lbtnPreviousClick(object sender, EventArgs e)
        {
            CurrentStartDisplay = CurrentStartDisplay - itemsOnPage;
            //Response.Redirect("~/PublicRegisterProcess.aspx");
            BindResult();
        }

        protected void lbtnSearchAgainClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PublicRegister.aspx");
        }

        protected void lbtnNextClick(object sender, EventArgs e)
        {
            CurrentStartDisplay = CurrentStartDisplay + itemsOnPage;
            //Response.Redirect("~/PublicRegisterProcess.aspx");
            BindResult();
        }

        protected void lbtnLetterCommand(object sender, CommandEventArgs e)
        {
            if ((string)e.CommandArgument =="All")
            {
                CurrentLetter = string.Empty;
            }
            else
            {
                CurrentLetter = (string)e.CommandArgument;
            }
            CurrentStartDisplay = 1;
            BindResult();
        }

        protected void lvResultsCommand(object sender, ListViewCommandEventArgs e)
        {
            if (!string.IsNullOrEmpty((string)e.CommandArgument))
            {
                Session[VIEW_STATE_CURRENT_USER_ID] = (string)e.CommandArgument;
                Response.Redirect("~/PublicRegisterDisplay.aspx?UserID=" + Session[VIEW_STATE_CURRENT_USER_ID]);
            }
            
        }

        #endregion

        #region Methods

        public void ReadSessionParameters()
        {
            if (Session[VIEW_STATE_CURRENT_FIRST_NAME] != null)
            {
                CurrentFirstName = (string)Session[VIEW_STATE_CURRENT_FIRST_NAME];
                //Session[VIEW_STATE_CURRENT_FIRST_NAME] = null;
            }

            if (Session[VIEW_STATE_CURRENT_LAST_NAME] != null)
            {
                CurrentLastName = (string)Session[VIEW_STATE_CURRENT_LAST_NAME];
                //Session[VIEW_STATE_CURRENT_LAST_NAME] = null;
            }

            if (Session[VIEW_STATE_CURRENT_REGISTR_NUMBER] != null)
            {
                CurrentRegistration = (string)Session[VIEW_STATE_CURRENT_REGISTR_NUMBER];
                //Session[VIEW_STATE_CURRENT_REGISTR_NUMBER] = null;
            }

            if (Session[VIEW_STATE_CURRENT_CITY] != null)
            {
                CurrentCity = (string)Session[VIEW_STATE_CURRENT_CITY];
                //Session[VIEW_STATE_CURRENT_CITY] = null;
            }

            if (Session[VIEW_STATE_CURRENT_POSTAL_CODE] != null)
            {
                CurrentPostalCode = (string)Session[VIEW_STATE_CURRENT_POSTAL_CODE];
                //Session[VIEW_STATE_CURRENT_POSTAL_CODE] = null;
            }

            if (Session[VIEW_STATE_CURRENT_LANGUAGE] != null)
            {
                CurrentLanguage = (string)Session[VIEW_STATE_CURRENT_LANGUAGE];
                //Session[VIEW_STATE_CURRENT_LANGUAGE] = null;
            }

            if (Session[VIEW_STATE_CURRENT_FUNDING] != null)
            {
                CurrentFunding = (string)Session[VIEW_STATE_CURRENT_FUNDING];
                //Session[VIEW_STATE_CURRENT_FUNDING] = null;
            }

            if (Session[VIEW_STATE_CURRENT_MAJOR_SERVICE] != null)
            {
                CurrentMajorService = (string)Session[VIEW_STATE_CURRENT_MAJOR_SERVICE];
                //Session[VIEW_STATE_CURRENT_MAJOR_SERVICE] = null;
            }

            if (Session[VIEW_STATE_CURRENT_CLIENT_AGE] != null)
            {
                CurrentClientAge = (string)Session[VIEW_STATE_CURRENT_CLIENT_AGE];
                //Session[VIEW_STATE_CURRENT_CLIENT_AGE] = null;
            }

            if (Session[VIEW_STATE_CURRENT_PRACTICE_SETTING] != null)
            {
                CurrentPracticeSetting = (string)Session[VIEW_STATE_CURRENT_PRACTICE_SETTING];
                //Session[VIEW_STATE_CURRENT_PRACTICE_SETTING] = null;
            }

            //if (Session[VIEW_STATE_CURRENT_LETTER] != null)
            //{
            //    CurrentLetter = (string)Session[VIEW_STATE_CURRENT_LETTER];
            //}

            //if (Session[VIEW_STATE_CURRENT_START_DISPLAY] != null)
            //{
            //    CurrentStartDisplay = (int)Session[VIEW_STATE_CURRENT_START_DISPLAY];
            //}
        }

        public string GetFullName(string lastName, string firstName, string middleName, string informalName)
        {
            string retName = string.Format("{0}, {1} {2}", lastName, firstName, middleName);

            if (!string.IsNullOrEmpty(informalName) && informalName != firstName)
            {
                retName += string.Format(" ({0})", informalName);
            }
            return retName;
        }

        public string GetPreviousNames(string lastName, string legalLastName, string prevLegaLastName, List<Activity> listPrevs)
        {
            string retResult = string.Empty;

            if (!string.IsNullOrEmpty(legalLastName) && legalLastName != lastName)
            {
                retResult = legalLastName;
            }
            if (!string.IsNullOrEmpty(prevLegaLastName) && prevLegaLastName != lastName)
            {
                if (!string.IsNullOrEmpty(retResult)) retResult += ", ";
                retResult += prevLegaLastName;
            }

            foreach (var item in listPrevs)
            {
                if (!string.IsNullOrEmpty(item.UF_3) && item.UF_3 != legalLastName && item.UF_3 != prevLegaLastName)
                {
                    if (!string.IsNullOrEmpty(retResult)) retResult += ", ";
                    retResult += item.UF_3;
                }
            }


            return retResult;
        }
        public void BindResult()
        {
            string sqlquery = string.Empty;
            string sqlquery2 = string.Empty;
            var repository = new Repository();
            var list = repository.FindUserInfo(CurrentFirstName, CurrentLastName, CurrentRegistration, CurrentCity, CurrentPostalCode, CurrentLanguage, CurrentFunding,
                CurrentMajorService, CurrentClientAge, CurrentPracticeSetting, CurrentLetter, itemsOnPage, CurrentStartDisplay, out sqlquery);
            //var count0 = repository.FindUserInfoCount(CurrentFirstName, CurrentLastName, CurrentRegistration, CurrentCity, CurrentPostalCode, CurrentLanguage, CurrentFunding,
              //  CurrentMajorService, CurrentClientAge, CurrentPracticeSetting, CurrentLetter, out sqlquery2);

            var count0 = repository.FindUserInfoCountImprv(CurrentFirstName, CurrentLastName, CurrentRegistration, CurrentCity, CurrentPostalCode, CurrentLanguage, CurrentFunding,
                CurrentMajorService, CurrentClientAge, CurrentPracticeSetting, CurrentLetter);

            if (list.Count > 0)
            {
                if (count0 == 1)
                {
                    lblResultsMessage.Text = "There was 1 match";
                }
                else
                {
                    lblResultsMessage.Text = "There were " + count0.ToString() + " matches";
                }

                trShowResults.Visible = true;
                trNoResults.Visible = false;

                var list0 = list.Where(L => L.Row_Number < (CurrentStartDisplay + itemsOnPage)).ToList();
                list0.FindAll(I => I.CurrentStatusCode == "AF").ForEach(delegate(User user)
                {
                    user.CurrentStatus = "Inactive";
                });

                lvResults.DataSource = list0;
                lvResults.DataBind();
                if (CurrentStartDisplay > 1)
                {
                    lbtnPrevious.Enabled = true;
                    lbtnPrevious2.Enabled = true;
                }
                else
                {
                    lbtnPrevious.Enabled = false;
                    lbtnPrevious2.Enabled = false;
                }
                if (list.Count == (itemsOnPage + 1))
                {
                    lbtnNext.Enabled = true;
                    lbtnNext2.Enabled = true;
                }
                else
                {
                    lbtnNext.Enabled = false;
                    lbtnNext2.Enabled = false;
                }
            }
            else
            {
                //lblMessage2.Text = sqlquery;
                trShowResults.Visible = false;
                trNoResults.Visible = true;
            }
        }

        #endregion

        #region Properties

        public string CurrentFirstName
        {
            get
            {
                if (ViewState[VIEW_STATE_CURRENT_FIRST_NAME] != null)
                {
                    return (string)ViewState[VIEW_STATE_CURRENT_FIRST_NAME];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState[VIEW_STATE_CURRENT_FIRST_NAME] = value;
            }
        }

        public string CurrentLastName
        {
            get
            {
                if (ViewState[VIEW_STATE_CURRENT_LAST_NAME] != null)
                {
                    return (string)ViewState[VIEW_STATE_CURRENT_LAST_NAME];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState[VIEW_STATE_CURRENT_LAST_NAME] = value;
            }
        }

        public string CurrentRegistration
        {
            get
            {
                if (ViewState[VIEW_STATE_CURRENT_REGISTR_NUMBER] != null)
                {
                    return (string)ViewState[VIEW_STATE_CURRENT_REGISTR_NUMBER];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState[VIEW_STATE_CURRENT_REGISTR_NUMBER] = value;
            }
        }

        public string CurrentCity
        {
            get
            {
                if (ViewState[VIEW_STATE_CURRENT_CITY] != null)
                {
                    return (string)ViewState[VIEW_STATE_CURRENT_CITY];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState[VIEW_STATE_CURRENT_CITY] = value;
            }
        }

        public string CurrentPostalCode
        {
            get
            {
                if (ViewState[VIEW_STATE_CURRENT_POSTAL_CODE] != null)
                {
                    return (string)ViewState[VIEW_STATE_CURRENT_POSTAL_CODE];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState[VIEW_STATE_CURRENT_POSTAL_CODE] = value;
            }
        }

        public string CurrentLanguage
        {
            get
            {
                if (ViewState[VIEW_STATE_CURRENT_LANGUAGE] != null)
                {
                    return (string)ViewState[VIEW_STATE_CURRENT_LANGUAGE];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState[VIEW_STATE_CURRENT_LANGUAGE] = value;
            }
        }

        public string CurrentFunding
        {
            get
            {
                if (ViewState[VIEW_STATE_CURRENT_FUNDING] != null)
                {
                    return (string)ViewState[VIEW_STATE_CURRENT_FUNDING];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState[VIEW_STATE_CURRENT_FUNDING] = value;
            }
        }

        public string CurrentMajorService
        {
            get
            {
                if (ViewState[VIEW_STATE_CURRENT_MAJOR_SERVICE] != null)
                {
                    return (string)ViewState[VIEW_STATE_CURRENT_MAJOR_SERVICE];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState[VIEW_STATE_CURRENT_MAJOR_SERVICE] = value;
            }
        }

        public string CurrentClientAge
        {
            get
            {
                if (ViewState[VIEW_STATE_CURRENT_CLIENT_AGE] != null)
                {
                    return (string)ViewState[VIEW_STATE_CURRENT_CLIENT_AGE];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState[VIEW_STATE_CURRENT_CLIENT_AGE] = value;
            }
        }

        public string CurrentPracticeSetting
        {
            get
            {
                if (ViewState[VIEW_STATE_CURRENT_PRACTICE_SETTING] != null)
                {
                    return (string)ViewState[VIEW_STATE_CURRENT_PRACTICE_SETTING];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState[VIEW_STATE_CURRENT_PRACTICE_SETTING] = value;
            }
        }

        public string CurrentLetter
        {
            get
            {
                if (Session[VIEW_STATE_CURRENT_LETTER] != null)
                {
                    return (string)Session[VIEW_STATE_CURRENT_LETTER];
                }
                else
                    return string.Empty;
            }
            set
            {
                Session[VIEW_STATE_CURRENT_LETTER] = value;
            }
        }

        public int CurrentStartDisplay
        {
            get
            {
                if (Session[VIEW_STATE_CURRENT_START_DISPLAY] != null)
                {
                    return (int)Session[VIEW_STATE_CURRENT_START_DISPLAY];
                }
                else
                    return 1;
            }
            set
            {
                Session[VIEW_STATE_CURRENT_START_DISPLAY] = value;
            }
        }

        #endregion

    }
}