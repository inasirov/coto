﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberEmploymentRenewal.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlMemberEmploymentRenewal" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Src="~/UserControls/DialogBox.ascx" TagName="DialogBox" TagPrefix="uc" %>
<script src="../Scripts/MaskedEditFix.js" type="text/javascript"></script>
<script src="/Va_dotNet/Scripts/jBox/jBox.min.js"></script>
<link href="/Va_dotNet/Scripts/jBox/jBox.css" rel="stylesheet">

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
    
    <style type="text/css">
        body
        {
            font-family: Arial, Helvetica, sans-serif;
            /*font-size: 13px;*/
        }
        .errMessage
        {
            width:300px;
            border: 1px solid;
            margin: 10px 0px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            position: relative;
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('images/info.png');
        }
        .modalPopup{
            z-index: 6001 !important;
        }
        .modalBackground {
            z-index: 6000 !important;
        }
        input[type="radio"] {
            margin-right: 3px!important;
        }
        .pointer { cursor: pointer; }
        .dd-practice{
            max-width: 420px;
        }
    </style>
    <script type="text/javascript">
        function showMessage() {
            $('.errMessage').show();
            setTimeout('$(".errMessage").fadeOut(800)', 2000);
        }

        function valPhone(source, clientside_arguments) {
            clientside_arguments.IsValid = checkPhoneNumber
            (clientside_arguments.Value);
        }

        function checkPhoneNumber(phoneNo) {
            var phoneRE = /^(1\s*[-\/\.]?)?(\((\d{3})\)|(\d{3}))\s*[-\/\.]?\s*(\d{3})\s*[-\/\.]?\s*(\d{4})\s*(([xX]|[eE][xX][tT])\.?\s*(\d+))*$/;
            if (phoneNo.match(phoneRE)) {
                return true;
            } else {
                return false;
            }
        }

        function UpdateField() {
            var hfield = document.getElementById('<%=txtTelephone3.ClientID %>');
            if (hfield) {
                var oldValue = hfield.value;
                var pattern = /\_/g;
                var pattern2 = /\(\)/g;
                var newValue1 = oldValue.replace(pattern, "");
                var newValue2 = newValue1.replace(pattern2, "");
                if (newValue2 == ' -') {
                    newValue2 = "";
                }

                hfield.value = newValue2;
            }
        }

        function UpdateTextField(hfield) {
            //var hfield = document.getElementById('<%=txtTelephone3.ClientID %>');
            // var hfield = document.getElementById(controlID);

            if (hfield) {
                var oldValue = hfield.value;
                var pattern = /\_/g;
                var pattern2 = /\(\)/g;
                var newValue1 = oldValue.replace(pattern, "");
                var newValue2 = newValue1.replace(pattern2, "");
                if (newValue2 == ' -') {
                    newValue2 = "";
                }

                hfield.value = newValue2;
            }
        }

        jQuery(document).ready(function () {

            attachHelpMessages();
        });

        function attachHelpMessages() {

            // iCollegeMailingsHelp
            let message = jQuery('#hfCollegeMailingsHelp').val();
            new jBox('Modal', {
                attach: '#iCollegeMailingsHelp',
                width: 650,
                title: 'College Mailings',
                overlay: false,
                createOnInit: true,
                content: message,
                draggable: true,
                repositionOnOpen: false,
                repositionOnContent: false
            });

            // iStatus1Help
            let message2 = jQuery('#hfStatus1Help').val();
            new jBox('Modal', {
                attach: '#iStatus1Help',
                width: 650,
                title: 'Status',
                overlay: false,
                createOnInit: true,
                content: message2,
                draggable: true,
                repositionOnOpen: false,
                repositionOnContent: false
            });

            // iPostalCode1Help
            let message3 = jQuery('#hfPostalCode1Help').val();
            new jBox('Modal', {
                attach: '#iPostalCode1Help',
                width: 650,
                title: 'Postal Code',
                overlay: false,
                createOnInit: true,
                content: message3,
                draggable: true,
                repositionOnOpen: false,
                repositionOnContent: false
            });

            // iEmploymentRelationship1Help
            let message4 = jQuery('#hfEmploymentRelationship1Help').val();
            new jBox('Modal', {
                attach: '#iEmploymentRelationship1Help',
                width: 650,
                title: 'Employment Relationship',
                overlay: false,
                createOnInit: true,
                content: message4,
                draggable: true,
                repositionOnOpen: false,
                repositionOnContent: false
            });

            // iCasualStatus1Help
            let message5 = jQuery('#hfCasualStatus1Help').val();
            new jBox('Modal', {
                attach: '#iCasualStatus1Help',
                width: 650,
                title: 'Employment Status',
                overlay: false,
                createOnInit: true,
                content: message5,
                draggable: true,
                repositionOnOpen: false,
                repositionOnContent: false
            });

            // iPrimaryRole1Help
            let message6 = jQuery('#hfPrimaryRole1Help').val();
            new jBox('Modal', {
                attach: '#iPrimaryRole1Help',
                width: 650,
                title: 'Primary Role',
                overlay: false,
                createOnInit: true,
                content: message6,
                draggable: true,
                repositionOnOpen: false,
                repositionOnContent: false
            });

            // iPracticeSetting1Help
            let message7 = jQuery('#hfPracticeSetting1Help').val();
            new jBox('Modal', {
                attach: '#iPracticeSetting1Help',
                width: 650,
                title: 'Practice Setting',
                overlay: false,
                createOnInit: true,
                content: message7,
                draggable: true,
                repositionOnOpen: false,
                repositionOnContent: false
            });

            // iMajorServices1Help
            let message8 = jQuery('#hfMajorServices1Help').val();
            new jBox('Modal', {
                attach: '#iMajorServices1Help',
                width: 650,
                title: 'Major Services',
                overlay: false,
                createOnInit: true,
                content: message8,
                draggable: true,
                repositionOnOpen: false,
                repositionOnContent: false
            });

            // iClientAgeRange1Help
            let message9 = jQuery('#hfClientAgeRange1Help').val();
            new jBox('Modal', {
                attach: '#iClientAgeRange1Help',
                width: 650,
                title: 'Client Age Range',
                overlay: false,
                createOnInit: true,
                content: message9,
                draggable: true,
                repositionOnOpen: false,
                repositionOnContent: false
            });

            // iFundingSource1Help
            let message10 = jQuery('#hfFundingSource1Help').val();
            new jBox('Modal', {
                attach: '#iFundingSource1Help',
                width: 650,
                title: 'Funding Source',
                overlay: false,
                createOnInit: true,
                content: message10,
                draggable: true,
                repositionOnOpen: false,
                repositionOnContent: false
            });
          

        }
    </script>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="2" width="100%">
                    <tr class="HeaderTitle">
                        <td class="HeaderTitle" align="right">
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 6 of 12" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" UseSubmitBehavior="true" TabIndex="0"/>--%>
                        </td>
                    </tr>
                    <%--<tr>
                        <td align="right">
                            <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back" OnClick="btnBackClick" />&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Update" OnClick="btnUpdateClick"  />
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                            <%--<uc:DialogBox ID="dmb" runat="server" />--%>
                        </td>
                    </tr>
                    <tr class="RowTitle">
                        <td>
                            <div>
                                <asp:Label ID="lblPersonalEmploymentInformationTitle" runat="server" CssClass="heading"
                                Text="Practice Site Information" />
                            </div>
                            
                        </td>
                    </tr>
                    <%--<tr>
                        <td style="text-align: left;">
                            <p>
                                <asp:Label ID="lblDenotesRequiredTitle" runat="server"
                                    Text="* denotes required field" />
                            </p>
                        </td>
                    </tr>--%>
                    <tr>
                        <td style="text-align: right; font-weight:bold; padding-top: 10px; padding-bottom: 10px;">
                            <%--<a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/2020/2020_Annual_Renewal_Glossary_S6_PracticeSiteInformation_English.pdf" target="_blank">Glossary</a>&nbsp;/&nbsp;--%>
                            <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/2021/2021_Annual_Renewal_Glossary_S6_PracticeSiteInformation_French.pdf" target="_blank">Glossaire</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo">
                                <tr>
                                    <td class="RightColumn">
                                        <p>
                                            The information displayed below represents your data currently on file with the College. 
                                            If you are working as an OT in Ontario, all relevant fields must contain data.
                                        </p>
                                        <p>
                                            If you would like to remove your current employment information, please select 'No Longer Working Here'.
                                            If you would like to update your current employment information, please do so by updating the applicable field(s).
                                        </p>
                                        <p><strong>If you are unemployed, working outside the profession or working outside of Ontario, you cannot fill out this page.</strong></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblQuestion1Text" runat="server" Text="Are you practising at more than three practice sites or do you have more than three employers?"
                                            Font-Bold="true" />&nbsp;&nbsp;
                                        <asp:RadioButtonList ID="rblQuestion1Answers" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Yes" Value="1" Selected="False" />
                                            <asp:ListItem Text="No" Value="0" Selected="True" />
                                        </asp:RadioButtonList>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblCollegeMailings" runat="server" Text="College mailings are usually sent to the home address. Would you prefer the College to send mail to your primary employment address?"
                                         Font-Bold="true" />&nbsp;
                                        <input type="hidden" id="hfCollegeMailingsHelp" value='<p>Change this to <b>By default</b> all College mailings are sent to your home address. Would you prefer the College to send mail to your primary employment address?</p><p>If so, you must have primary employment address information entered, you can change your preferred mailing address at any time throughout the year.</p><br/>'/>
                                        <asp:Image ID="iCollegeMailingsHelp" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                        &nbsp;
                                        <asp:DropDownList ID="ddlCollegeMailings" runat="server" >
                                        </asp:DropDownList>&nbsp;
                                        <asp:RequiredFieldValidator ID="rfvCollegeMailings" runat="server" ControlToValidate="ddlCollegeMailings"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="College Mailing: Please select 'Yes' or 'No'"
                                            Display="None" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <ajaxToolkit:TabContainer ID="tcMainContainer" CssClass="NewsTab" runat="server" CssTheme="None"
                                            AutoPostBack="false" TabStripPlacement="Top" ScrollBars="Auto" ActiveTabIndex="0">
                                            <ajaxToolkit:TabPanel ID="tpEmpl1" runat="server" HeaderText="PRIMARY SITE" ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table cellpadding="2" cellspacing="4" width="100%">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblStatus1Title" runat="server" Text="Status" />&nbsp;
                                                                <input type="hidden" id="hfStatus1Help" value="<p><b>No longer working here</b><br/>Select this response if the employment displayed is no longer current.</p><p><b>Primary Site</b><br/>Refers to the employment with an employer, or in a self-employed arrangement, that is associate with the highest number of usual weekly hours worked.</p><p><b>Secondary site</b><br/>Refers to the employment associated with the second highest number of usual weekly hours worked, whether employed or self-employed.</p><p><b>Tertiary site</b><br/>Refers to the employment associated with the third highest number of usual weekly hours worked, whether employed or self-employed.</p>"/>
                                                                <asp:Image ID="iStatus1Help" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                                <%--<asp:ImageButton ID="ibStatus1Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibStatus1HelpClick" />--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlStatus1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus1SelectedIndexChanged" />
                                                                <asp:Label ID="lblStatus1Message" runat="server" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblStartDate1Label" runat="server" Text="Start Date" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtStartDate1" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar1" runat="server" AutoPostBack="False" BlankFieldText="mm dd yyyy"
                                                                    Format="mm dd yyyy" Separator="/" Control="txtStartDate1" From-Date="1950-01-01"
                                                                    MessageAlignment="RightCalendarControl" OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvStartDate1" runat="server" ControlToValidate="txtStartDate1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 1: Start Date is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer1Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblEndDate1Label" runat="server" Text="End Date" />&nbsp;
                                                                <%--<asp:ImageButton ID="ibEndDate1Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibEndDate1HelpClick" />--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtEndDate1" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar2" runat="server" AutoPostBack="False" BlankFieldText="mm dd yyyy"
                                                                    Format="mm dd yyyy" Separator="/" Control="txtEndDate1" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="LeftTitle">
                                                                <asp:Label ID="lblEmployerName1Label" runat="server" Text="Employer Name" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtEmployerName1" runat="server" TextMode="MultiLine" Rows="3" Columns="50" MaxLength="100" />
                                                                <asp:RequiredFieldValidator ID="rfvEmployerName1" runat="server" ControlToValidate="txtEmployerName1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 1: Employer name is blank."
                                                                    Display="None" ValidationGroup="Employer1Validation" EnableClientScript="false"
                                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblAddress1_1Label" runat="server" Text="Address" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAddress1_1" runat="server" MaxLength="50" />
                                                                <asp:RequiredFieldValidator ID="rfvAddress1_1" runat="server" ControlToValidate="txtAddress1_1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 1: Address is blank."
                                                                    Display="None" EnableClientScript="false" ValidationGroup="Employer1Validation"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAddress1_2" runat="server" MaxLength="50"  />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCity1Label" runat="server" Text="City" MaxLength="40" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtCity1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCity1" runat="server" ControlToValidate="txtCity1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 1: City is blank."
                                                                    Display="None" ValidationGroup="Employer1Validation" EnableClientScript="false"
                                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvince1Label" runat="server" Text="Province" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince1" runat="server" ControlToValidate="ddlProvince1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Province is blank."
                                                                    Display="None" EnableClientScript="false" Text="*" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPostalCode1Label" runat="server" Text="Postal Code" />&nbsp;
                                                                <input type="hidden" id="hfPostalCode1Help" value='<p>This data is used in annual trending to help identify OTs who typically work at multiple sites within the community, potentially some distance from an employer/business office location.</p><p><b>Yes</b> - Postal code reflects the site where service is delivered.</p><p><b>No</b> - Postal code does not reflect the site where service is delivered. The postal code provided refers to an employer or business office that is different than the site where service is delivered.</p>'/>
                                                                <asp:Image ID="iPostalCode1Help" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                                <%--<asp:ImageButton ID="ibPostalCode1Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibPostalCode1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtPostalCode1" runat="server" Columns="10" />
                                                                <ajaxToolkit:MaskedEditExtender ID="meePostalCode1" TargetControlID="txtPostalCode1"
                                                                Mask="LNLNLN" ClearMaskOnLostFocus="false" MaskType="None" ErrorTooltipEnabled="True" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvPostalCode1" runat="server" ControlToValidate="txtPostalCode1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 1: Postal Code is blank."
                                                                    Display="None" ValidationGroup="Employer1Validation" EnableClientScript="false"
                                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revPostalCode1" runat="server" EnableClientScript="false"
                                                                    ControlToValidate="txtPostalCode1" SetFocusOnError="True" ErrorMessage="Employer 1: Please provide a valid postal code without any spaces"
                                                                    Display="None" ValidationExpression="^([abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][a-zA-Z][0-9][a-zA-Z][0-9])$"
                                                                    ValidationGroup="Employer1Validation"  Text="*"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPostalCodeReflectPractice1Label" runat="server" Text="Does the postal code reflect site of practice?" />
                                                                <%--<asp:ImageButton ID="ibPostalCodeReflectionHelp" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibPostalCodeReflectionHelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPostalCodeReflectPractice1" runat="server" />
                                                                 <asp:RequiredFieldValidator ID="rfvPostalCodeReflectPractice1" runat="server" ControlToValidate="ddlPostalCodeReflectPractice1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Does the postal code reflect site of practice?"
                                                                    Display="None" EnableClientScript="false" Text="*" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry1Label" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry1" runat="server" ControlToValidate="ddlCountry1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Country is blank."
                                                                    Display="None" ForeColor="Red" EnableClientScript="false" />
                                                                <div id="message" class="errMessage" runat="server" style="display: none">
                                                                    You answered Yes to the question 'Does the postal code reflect site of practice?'.  Please note that the country field must be Canada.
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblTelephone1Label" runat="server" Text="Telephone" />
                                                                <%--<asp:ImageButton ID="ibPhone1Help" ImageUrl="~/images/qmark.jpg" runat="server" OnClick="ibPhone1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtTelephone1" runat="server" Columns="30" />
                                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtTelephone1"
                                                                    Mask="(999) 999-9999 x99999" MaskType="None" ClearMaskOnLostFocus="false" InputDirection="LeftToRight"
                                                                    ErrorTooltipEnabled="True" runat="server" Filtered="xX" />
                                                                <asp:RequiredFieldValidator ID="rfvTelephone1" runat="server" ControlToValidate="txtTelephone1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 1: Phone is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer1Validation" EnableClientScript="false" Text="*"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revTelephone1" runat="server" ControlToValidate="txtTelephone1" ErrorMessage="Please provide all information for Employer 1: Phone number is invalid." Display="None"
                                                                    EnableClientScript="false" Text="*" ValidationGroup="Employer1Validation" ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}( )?([x]?(\d+)?)?$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblFax1Label" runat="server" Text="Fax"  />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtFax1" runat="server" Columns="30" />
                                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtFax1"
                                                                    Mask="(999) 999-9999" ClearMaskOnLostFocus="false" Filtered="xX" MaskType="None"
                                                                    OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" InputDirection="LeftToRight"
                                                                    ErrorTooltipEnabled="True" runat="server" />
                                                                <asp:RegularExpressionValidator ID="revFax1" runat="server" ControlToValidate="txtFax1" ErrorMessage="Please provide all information for Employer 1: Fax number is invalid." Display="None"
                                                                    EnableClientScript="false" Text="*" ValidationGroup="Employer1Validation" ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblEmploymentRelationship1Label" runat="server" Text="Employment Relationship" />&nbsp;
                                                                <input type="hidden" id="hfEmploymentRelationship1Help" value='<p><b>Permanent employee</b><br />Status with employer is permanent with an indeterminate duration (no specified end date) of employment and guaranteed or fixed hours of work per week.</p><p><b>Temporary employee</b><br />Status with employer is temporary with a fixed duration of employment, based on a defined start and end date, and guaranteed or fixed hours of work per week.</p><p><b>Casual employee</b><br />Status with employer is on an as-needed basis, with employment that is not characterized by a guaranteed or fixed number of hours per week. There is no arrangement that between an employer and employee that the employee will be called to work on a regular basis.</p><p><b>Self-employed</b><br />A person who engages independently in the profession, operating his or her own economic enterprise. The individual may be the working owner of an incorporated or unincorporated business or professional practice, or an individual in a business relationship characterized by verbal or written agreement(s) in which the self-employed individual agrees to perform specific work for a payer in return for payment.</p>'/>
                                                                <asp:Image ID="iEmploymentRelationship1Help" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlEmploymentRelationship1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvEmploymentRelationship1" runat="server" ControlToValidate="ddlEmploymentRelationship1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Employment Relationship is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCasualStatus1Label" runat="server" Text="Employment Status" />&nbsp;
                                                                <input type="hidden" id="hfCasualStatus1Help" value='<p><b>Full time</b><br />Official status with employer is full-time or equivalent, or usual hours of practice are equal to or greater than 30 hours per week.</p><p><b>Part time</b><br />Official status with employer is part-time, or usual hours of practice are less than 30 hours per week.</p><p><b>Casual</b><br />Status with employer is on an as-needed basis, with employment that is not characterized by a guaranteed or fixed number of hours per week. There is no arrangement between employer and employee that the employee will be called to work on a regular basis.</p>'/>
                                                                <asp:Image ID="iCasualStatus1Help" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                                <%--<asp:ImageButton ID="ibCasualStatus1Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibCasualStatusHelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCasualStatus1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCasualStatus1SelectedIndexChanged"/>
                                                                <asp:RequiredFieldValidator ID="rfvCasualStatus1" runat="server" ControlToValidate="ddlCasualStatus1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Full / Part Time Status is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblAverageWeeklyHours1Label" runat="server" Text="Average Weekly Hours (no ranges, no decimal points)" />
                                                                <%--<asp:ImageButton ID="ibAverageWeeklyHours1Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibAverageWeeklyHours1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAverageWeeklyHours1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvAverageWeeklyHours1" runat="server" ControlToValidate="txtAverageWeeklyHours1"
                                                                    ForeColor="Red" ErrorMessage="Please provide all information for Employer 1: Average Weekly Hours is blank."
                                                                    Display="None" ValidationGroup="Employer1Validation" Text="*" EnableClientScript="false" />
                                                                <asp:RegularExpressionValidator ID="revAverageWeeklyHours1" runat="server" ControlToValidate="txtAverageWeeklyHours1"
                                                                    ForeColor="Red" ErrorMessage="Average Weekly Hours Employment 1: Please use whole numbers. Decimal points are not permitted." Display="None"
                                                                    ValidationExpression="^([0-9]*)$" ValidationGroup="Employer1Validation" EnableClientScript="false"
                                                                    Text="*"></asp:RegularExpressionValidator>
                                                                <asp:RangeValidator ID="rvAverageWeeklyHours1" runat="server" MaximumValue="168" MinimumValue="0"  ErrorMessage="Maximum number of hours in a week is 168" 
                                                                    ValidationGroup="Employer1Validation" ControlToValidate="txtAverageWeeklyHours1" Display="None" ForeColor="Red" Type="Integer" Text="*" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPrimaryRole1Label" runat="server" Text="Primary Role" />&nbsp;
                                                                <input type="hidden" id="hfPrimaryRole1Help" value='<p><b>Administrator</b><br />A person whose primary role is involved in administration, planning, organizing and managing.</p><p><b>Manager</b><br />Major role is in management of a particular team/group that delivers services (you have no responsibility of caseloads).</p><b>Owner/operator</b><br />An individual who is the owner of a practice site and who may or may not manage or supervise the operation at that site.</p><p><b>Service provider - direct care</b><br />Major role is in the direct delivery of occupational therapy services, including case management and/or consultation, related to direct client care.</p><p><b>Consultant (non-client care)</b><br />Major role is in the provision of expert guidance and consultation, without direct client-care, to a third party.</p><p><b>Instructor/educator</b><br />Major role is as an educator of occupational therapy for a particular target group.</p><b>Researcher</b><br />Major role is in knowledge development and dissemination of research.</p><b>Salesperson</b><br />Major role is in the sales of health related services and products.</p><b>Quality management specialist</b><br />Major role is the assurance and control of the quality of procedures and/or equipment.</p>'/>
                                                                <asp:Image ID="iPrimaryRole1Help" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPrimaryRole1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvPrimaryRole1" runat="server" ControlToValidate="ddlPrimaryRole1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Primary Role is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPracticeSetting1Label" runat="server" Text="Practice Setting" />&nbsp;
                                                                <input type="hidden" id="hfPracticeSetting1Help" value="<p>Select the descriptor that best identifies the practice setting of where you provide service (whether an employee or self-employed) for each place of employment/practice site. This is at the service delivery level. Service delivery level refers to the location where you are directly engaged in your occupational therapy practice.</p><p><b>General hospital</b><br/>Health care facility that offers a range of inpatient and outpatient health care services (for example, medical, surgical, psychiatry, etc.) available to the target population. Includes specialty hospitals not otherwise classified.</p><p><b>Rehabilitation facility/hospital</b><br/>Health care facility that has as its primary focus the post-acute, inpatient and outpatient rehabilitation of individuals.</p><p><b>Children Treatment Centre (CTC)</b><br/>This centre is a community-based organization that serves children with physical disabilities and multiple special needs. The centre provides physiotherapy, occupational therapy and speech therapy along with other additional services.</p><p><b>Mental health and addiction facility</b><br/>Health care facility that has as its primary focus the acute or post-acute, inpatient and/or outpatient, care of individuals with mental health issues and illness and/or addictions.</p><p><b>Residential/long-term care facility</b><br/>Licensed or regulated long-term care facility designed for people who require the availability of 24-hour nursing care and supervision within a secure setting. In general, long-term care facilities offer high levels of personal care and support. These facilities include nursing homes, municipal homes and charitable homes.</p><p><b>Assisted living residence/supportive housing</b><br/>Refers to a non-institutional community setting that integrates a shared living environment with varying degrees of supportive services of the following types: supervision, housekeeping, personal care, meal service, transportation, social and recreational opportunities, etc. May have limited medical/nursing services available. Includes group homes, retirement homes, community care homes, lodges, supportive housing and congregate living settings.</p><p><b>Community Health Centre (CHC)</b><br/>Community-based organization that may be the first-point of contact for clients, offering a range of primary health, social and/or other non-institutional-based services, including occupational therapy. CHCs emphasize health promotion, disease prevention and chronic disease management based on local population health needs. The organization must be recognized as a CHC and there are 75 CHCs throughout Ontario (2015).</p><p><b>Local Health Integration Network (LHIN) home and community care services</b><br/>LHINs arrange all government-funded services and work with health care providers to enhance access and co-ordination for people who need care in their own homes in the community, in supportive housing, or in a long-term care home. LHINs also provide information about local community support service agencies and can link people to these agencies to arrange services.</p><p><b>Visiting agency/client's environment</b><br />Community-based agency or group professional practice/business focused on delivering health services including occupational therapy. The professional travels to one or more sites that may be the client's home, school and/or workplace environment to provide services (for example, homecare or CCAC contracts).</p><p><b>Family Health Team (FHT)</b><br />Family Health Team is a group that includes physicians and other interdisciplinary providers, such as OTs, nurse practitioners, pharmacists, mental health workers, and dietitians. The FHT provides comprehensive primary health care (PHC) services. The FHT provides services on a 24/7 basis through a combination of regular office hours, after-hours services, and access to a registered nurse through the Telephone Health Advisory Service (THAS).The FHT emphasizes health promotion, disease prevention and chronic disease management based on local population health needs. The FHT must enroll patients. The group must be recognized as a FHT and there are 184 FHTs in Ontario (2015).</p><p><b>Independent Health Facility</b><br />Independent health facility Refers to a stand-alone facility or clinic offering specialized or broadly-based imaging services.</p><p><b>Nurse practitioner led clinic</b></br>This clinic is led by a nurse practitioner and provides primary health care in collaboration with family physicians, and other interdisciplinary health care providers. The focus of the clinic is on comprehensive primary health care services in areas where access to family health care is limited.</p><p><b>Group professional practice office/clinic</b><br />Community-based group professional practice/business or clinic (not already noted) organized around the delivery of primarily onsite health services, including occupational therapy, by a group of health professionals. Clients typically come to the professionals' location to receive services. Other support staff may also be involved, however, the health professionals are the focus of service provision.</p><p><b>Solo practice office</b><br />Community-based professional practice/business organized around the delivery of occupational therapy health services, by a single professional. Administrative support staff may also be involved, however, the health professional is the focus of service provision.</p><p><b>Post-secondary educational institution</b><br />Post-secondary institution, either a university or equivalent institution or a college or equivalent institution, with a primary focus on the delivery of occupational therapy education.</p><p><b>Preschool/school system/board of education</b><br />Primary, elementary or secondary school (or equivalent institution), or the associated school board (or equivalent entity) that has responsibility for the governance and management of education funding issued by provincial governments.</p><p><b>Health related business/industry</b><br />Business or industry whose focus of activities is not in the direct delivery of health care services, but rather the health of workers, health-related product development or the selling of health-related products (for example, medical device companies).</p><p><b>Group Health Centre (Sault Ste. Marie)</b><br />An interdisciplinary practice in Sault Ste. Marie that includes physicians, nurse practitioners, dietitians, pharmacists, physiotherapists, and many other providers. The group provides comprehensive primary health care (PHC) services. The services are provided on a 24/7 basis through a combination of regular office hours, after-hours services, and access to a registered nurse through the Telephone Health Advisory Service (THAS). The group emphasizes health promotion, disease prevention and chronic disease management based on local population health needs. The group must enroll patients.</p><p><b>Cancer Centre</b><br />Facility that specializes in services related to the treatment, prevention and research of cancer.</p><p><b>TeleHealth Ontario and Telephone Health Advisory Services</b><br />A program that provides free, confidential 24/7 service that provides Ontario residents with easy access to health information.</p><p><b>Board of Health/Public Health Laboratory/Public Health Unit</b><br />A public health laboratory or official health unit that administers health promotion and disease prevention programs to inform the public about healthy life-styles, communicable disease control including education in STDs/AIDS, immunization, food premises inspection, healthy growth and development including parenting education, health education for all age groups and selected screening services.</p><p><b>Association/government/para-government</b><br />Organization or government that deals with regulation, advocacy, policy development, program development, research and/or the protection of the public, at a national, provincial/territorial, regional or municipal level.</p><p><b>Correctional facility</b><br />Stand-alone organization/facility that has as its primary focus the treatment and rehabilitation of persons detained or on probation due to a criminal act.</p><p><b>Other place of work</b><br />Place of work is not otherwise described.</p>"/>
                                                                <asp:Image ID="iPracticeSetting1Help" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                                <%--<asp:ImageButton ID="ibPracticeSetting1Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibPracticeSetting1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPracticeSetting1" runat="server" CssClass="dd-practice"/>
                                                                <asp:RequiredFieldValidator ID="rfvPracticeSetting1" runat="server" ControlToValidate="ddlPracticeSetting1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Practice Setting is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblMajorServices1Label" runat="server" Text="Major Services" />&nbsp;
                                                                <input type="hidden" id="hfMajorServices1Help" value="<p>Select the descriptor that best represents the major focus of activities in which you primarily provide services for each practice site. It is common for OTs to work in a number of areas, however you are requested to select only one area that best represents the majority of your practice.</p><p><b>Mental health and addiction</b><br/>Services provided to clients with a variety of mental health issues or addictions that require interventions focusing on maintaining/optimizing the occupational performance of the life of an individual.</p><p><b>General service provision </b><br/>Services provided primarily to clients with a variety of general physical health issues requiring interventions focusing on maintaining/optimizing the occupational performance of the life of an individual.</p><p><b>Vocational rehabilitation</b><br/>Services provided with the purpose of enabling clients to participate in productive occupation(s).</p><p><b>Palliative care</b><br/>Services provided primarily to clients with the aim of relieving suffering and improving the quality of life for persons who are living with or dying from advanced illness or who are bereaved.</p><p><b>Public health </b><br/>Services are provided primarily with the purpose of improving the health of populations through the functions of health promotion, health protection, health surveillance and population health assessment.</p><p><b>Other areas of direct service/consultation </b><br/>Area of direct service/consultation not otherwise identified.</p><p><b>Administration </b><br/>Focus of activities is on the management of services, or the development of policy and/or programs.</p><p><b>Client service management </b><br/>Focus of activities is the management of client services across the health care continuum, specifically the coordination of multiple services as required for client care.</p><p><b>Consultation (medical/legal)</b><br/>Expert consultation is provided on the profession related to medical and/or legal matters, expert witness, associated with client care.</p><p><b>Post-secondary education </b><br/>Focus of activities is directed at providing post-secondary teaching to individuals registered in formal education programs.</p><p><b>Research </b><br/>Focus of activities is in knowledge development and dissemination of research including clinical and non-clinical.</p><p><b>Emergency </b><br/>Care provided to patients who have immediate medical problems, frequently before complete clinical or diagnostic information is available, in a comprehensive emergency department or an urgent care centre.</p><p><b>Infectious disease prevention and control </b><br/>Services are provided to primarily prevent and control health care associated infections and other epidemiological significant organisms. This includes providing services to reduce the risk, spread and incidence of infections in populations. This includes pandemic planning.</p><p><b>Chronic disease prevention and management </b><br/>Services are provided primarily to address chronic diseases early in the disease cycle to prevent disease progression and reduce potential health complications. Diseases can include diabetes, hypertension, congestive heart failure, asthma, chronic lung disease, renal failure, liver disease, and rheumatoid and osteoarthritis.</p><p><b>Cancer care </b><br/>Services provided primarily to clients with a variety of cancer and cancer related illnesses.</p><p><b>Comprehensive primary care </b><br/>Services provided primarily to a range of clients, possibly at first-contact, to identify, prevent, diagnose and/or treat health conditions (for example, oral care, foot care, etc.).</p><p><b>Sales </b><br/>Focus of activities is in the sales and/or service of health related apparatuses or equipment.</p><p><b>Quality management </b><br/>Focus of activities is on the assurance of the operational integrity, based on compliance with staffing, technical and organizational requirements.</p><p>If you select one of the services on the left of the chart, you are also required to select (only) one primary health condition as described on the right:</p><div class='row'><div class='col-sm-6' style='border: 1px solid black;'><p><b>Critical care:</b><br/>Services provided primarily to clients dealing with serious life- threatening and/or medically complex conditions who require constant care, observation and specialized monitoring and therapies.</p><p><b>Acute care:</b><br/>Services provided primarily to clients who have an acute medical condition or injury that is generally of short-duration.</p><p><b>Continuing care:</b><br/>Services provided primarily to clients with continuing health conditions for extended periods of time (e.g. long-term care or home care).</p><p><b>Geriatric care:</b><br/>Services provided primarily to care for elderly persons and to treat diseases associated with aging through short-term, intermediate or long-term treatment/interventions.</p></div><div class='col-sm-6' style='border: 1px solid black;'><p><b>Neurological:</b><br/>Services provided to clients with a variety of neurological health issues that require interventions focusing on maintaining/optimizing the occupational performance of the life of an individual.</p><p><b>Musculoskeletal:</b><br/>Services provided to clients with a variety of musculoskeletal health issues that require interventions focusing on maintaining/optimizing the occupational performance of the life of an individual.</p><p><b>Cardiovascular and respiratory:</b><br/>Services provided to clients with a variety of cardiovascular and/or respiratory health issues that require interventions focusing on maintaining/optimizing the occupational performance of the life of an individual.</p><p><b>Digestive/metabolic/endocrine:</b><br/>Services provided to clients with a variety of digestive, metabolic and/or endocrine related health issues that require interventions focusing on maintaining/optimizing the occupational performance of the life of an individual.</p></div></div><p><b>Other areas of practice</b><br/>Other area of employed activity not otherwise described.</p>"/>
                                                                <asp:Image ID="iMajorServices1Help" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                                <%--<asp:ImageButton ID="ibMajorServices1Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibMajorServices1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlMajorServices1" runat="server"  AutoPostBack="true" OnSelectedIndexChanged="ddlMajorServices1SelectedIndexChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvMajorServices1" runat="server" ControlToValidate="ddlMajorServices1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Major Service is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr id="trHealthCondition1" runat="server" visible="false">
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblHealthCondition1Label" runat="server" Text="Health Condition" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlHealthCondition1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvHealthCondition1" runat="server" ControlToValidate="ddlHealthCondition1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please select a Health Condition for Employer 1."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblClientAgeRange1Label" runat="server" Text="Client Age Range" />&nbsp;
                                                                <input type="hidden" id="hfClientAgeRange1Help" value="<p>Select the descriptor that best describes the client population that you most often work with for each employment profile/practice site.</p><p><b>Preschool age</b><br/>Preschool age clients that are between the ages of 0 and 4 years, inclusive.</p><p><b>School age</b><br/>School age clients that are between the ages of 5 and 17 years, inclusive.</p><p><b>Mixed paediatrics</b><br/>Range of clients that are between the ages of 0 and 17 years, inclusive.</p><p><b>Adults</b><br/>Adult clients that are between the ages of 18 and 64 years, inclusive.</p><p><b>Seniors</b><br/>Adult clients that are 65 years of age and older.</p><p><b>Mixed adults</b><br/>Range of clients that are 18 years and older, including seniors.</p><p><b>All ages</b><br/>Clients across all age ranges.</p><p><b>Other</b><br/>Direct service is not associated with one main age range of clients. Direct service is not associated with one main age range of clients.</p>"/>
                                                                <asp:Image ID="iClientAgeRange1Help" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlClientAgeRange1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvClientAgeRange1" runat="server" ControlToValidate="ddlClientAgeRange1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Client Age Range is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblFundingSource1Label" runat="server" Text="Funding Source" />&nbsp;
                                                                <input type="hidden" id="hfFundingSource1Help" value="<p>Select the descriptor that best identifies the major source of funding for each employment profile/practice site.</p><p><b>Public/government</b><br/>The public sector is the main source of funding for employed activities.</p><p><b>Private sector/individual client</b><br/>Private sector entity or an individual client is the primary source of funding for employed activities.</p><p><b>Public/private mix</b><br/>Funding for employed activities is derived from a mixture of public and private sources.</p><p><b>Other funding</b><br/>Funding source not otherwise described.</p><p><b>Auto insurance</b><br/> Funding source is through auto insurers.</p><p><b>Other insurance</b><br/>Funding source is through long-term disability, extended health or WSIB coverage.</p>"/>
                                                                <asp:Image ID="iFundingSource1Help" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlFundingSource1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvFundingSource1" runat="server" ControlToValidate="ddlFundingSource1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Funding Source is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                            <ajaxToolkit:TabPanel ID="tpEmpl2" runat="server" HeaderText="SECONDARY SITE"
                                                ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table cellpadding="2" cellspacing="4" width="100%">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblStatus2Title" runat="server" Text="Status" />&nbsp;
                                                                <%--<asp:ImageButton ID="ibStatus2Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibStatus1HelpClick" />--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlStatus2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus2SelectedIndexChanged" />
                                                                <asp:Label ID="lblStatus2Message" runat="server" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblStartDate2Label" runat="server" Text="Start Date" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtStartDate2" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar3" runat="server" AutoPostBack="False" BlankFieldText="mm dd yyyy"
                                                                    Format="mm dd yyyy" Separator="/" Control="txtStartDate2" From-Date="1950-01-01" 
                                                                    MessageAlignment="RightCalendarControl"  OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvStartDate2" runat="server" ControlToValidate="txtStartDate2"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 2: Start Date is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer2Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblEndDate2Label" runat="server" Text="End Date" />&nbsp;
                                                                <%--<asp:ImageButton ID="ibEndDate2Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibEndDate1HelpClick" />--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtEndDate2" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar4" runat="server" AutoPostBack="False" BlankFieldText="mm dd yyyy"
                                                                    Format="mm dd yyyy" Separator="/" Control="txtEndDate2" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="LeftTitle">
                                                                <asp:Label ID="lblEmployerName2Label" runat="server" Text="Employer Name" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtEmployerName2" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                                <asp:RequiredFieldValidator ID="rfvEmployerName2" runat="server" ControlToValidate="txtEmployerName2"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 2: Employer name is blank."
                                                                    Display="None" ValidationGroup="Employer2Validation" ForeColor="Red" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblAddress2_1Label" runat="server" Text="Address"  />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAddress2_1" runat="server" MaxLength="50"/>
                                                                <asp:RequiredFieldValidator ID="rfvAddress2_1" runat="server" ControlToValidate="txtAddress2_1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 2: Address is blank."
                                                                    Display="None" ValidationGroup="Employer2Validation" ForeColor="Red" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAddress2_2" runat="server" MaxLength="50"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCity2Label" runat="server" Text="City" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtCity2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCity2" runat="server" ControlToValidate="txtCity2"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 2: City is blank."
                                                                    Display="None" ValidationGroup="Employer2Validation" ForeColor="Red" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvince2Label" runat="server" Text="Province" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince2" runat="server" ControlToValidate="ddlProvince2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPostalCode2Label" runat="server" Text="Postal Code" />
                                                                <%--<asp:ImageButton ID="ibPostalCode2Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibPostalCode1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtPostalCode2" runat="server" Columns="10" />
                                                                <ajaxToolkit:MaskedEditExtender ID="mmePostalCode2" TargetControlID="txtPostalCode2"
                                                                Mask="LNLNLN" ClearMaskOnLostFocus="false" MaskType="None" ErrorTooltipEnabled="True" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvPostalCode2" runat="server" ControlToValidate="txtPostalCode2"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 2: Postal Code is blank."
                                                                    Display="None" ValidationGroup="Employer2Validation" ForeColor="Red" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revPostalCode2" runat="server" EnableClientScript="false"
                                                                    ControlToValidate="txtPostalCode2" SetFocusOnError="True" ErrorMessage="Employer 2: Please provide a valid postal code without any spaces"
                                                                    Display="None" ValidationExpression="^([abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][a-zA-Z][0-9][a-zA-Z][0-9])$"
                                                                    ValidationGroup="Employer2Validation" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPostalCodeReflectPractice2Label" runat="server" Text="Does the postal code reflect site of practice?" />
                                                                <%--<asp:ImageButton ID="ibPostalCodeReflection2Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibPostalCodeReflectionHelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPostalCodeReflectPractice2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvPostalCodeReflectPractice2" runat="server" ControlToValidate="ddlPostalCodeReflectPractice2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Does the postal code reflect site of practice?"
                                                                    Display="None" EnableClientScript="false" Text="*" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry2Label" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry2" runat="server" ControlToValidate="ddlCountry2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Country is blank."
                                                                    Display="None" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblTelephone2Label" runat="server" Text="Telephone" />
                                                                <%--<asp:ImageButton ID="ibPhone2Help" ImageUrl="~/images/qmark.jpg" runat="server" OnClick="ibPhone1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtTelephone2" runat="server" Columns="30" />
                                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3" TargetControlID="txtTelephone2"
                                                                    Mask="(999) 999-9999 x99999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                    OnInvalidCssClass="MaskedEditError" ClearMaskOnLostFocus="false" InputDirection="LeftToRight"
                                                                    ErrorTooltipEnabled="True" runat="server" Filtered="xX" MaskType="None"/>
                                                                <asp:RequiredFieldValidator ID="rfvTelephone2" runat="server" ControlToValidate="txtTelephone2"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 2: Phone is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer2Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revTelephone2" runat="server" ControlToValidate="txtTelephone2" ErrorMessage="Please provide all information for Employer 2: Phone number is invalid." Display="None"
                                                                EnableClientScript="false" Text="*" ValidationGroup="Employer2Validation" ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}( )?([x]?(\d+)?)?$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblFax2Label" runat="server" Text="Fax" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtFax2" runat="server" Columns="30" />
                                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender4" TargetControlID="txtFax2"
                                                                    Mask="(999) 999-9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                    OnInvalidCssClass="MaskedEditError" ClearMaskOnLostFocus="false" InputDirection="LeftToRight"
                                                                    ErrorTooltipEnabled="True" runat="server" Filtered="xX" MaskType="None"/>
                                                                <asp:RegularExpressionValidator ID="revFax2" runat="server" ControlToValidate="txtFax2" ErrorMessage="Please provide all information for Employer 2: Fax number is invalid." Display="None"
                                                                EnableClientScript="false" Text="*" ValidationGroup="Employer2Validation" ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblEmploymentRelationship2Label" runat="server" Text="Employment Relationship" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlEmploymentRelationship2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvEmploymentRelationship2" runat="server" ControlToValidate="ddlEmploymentRelationship2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Employment Relationship is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCasualStatus2Label" runat="server" Text="Full / Part Time/ Casual Status" />
                                                                <%--<asp:ImageButton ID="ibCasualStatus2Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibCasualStatusHelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCasualStatus2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCasualStatus2" runat="server" ControlToValidate="ddlCasualStatus2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Full / Part Time Status is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblAverageWeeklyHours2Label" runat="server" Text="Average Weekly Hours (no ranges, no decimal points)" />
                                                                <%--<asp:ImageButton ID="ibAverageWeeklyHours2Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibAverageWeeklyHours1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAverageWeeklyHours2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvAverageWeeklyHours2" runat="server" ControlToValidate="txtAverageWeeklyHours2"
                                                                    ForeColor="Red" ErrorMessage="Please provide all information for Employer 2: Average Weekly Hours is blank."
                                                                    Display="None" ValidationGroup="Employer2Validation" Text="*" EnableClientScript="false" />
                                                                <asp:RegularExpressionValidator ID="revAverageWeeklyHours2" runat="server" ControlToValidate="txtAverageWeeklyHours2"
                                                                    ForeColor="Red" ErrorMessage="Average Weekly Hours Employment 2: Please use whole numbers. Decimal points are not permitted." Display="None"
                                                                    ValidationExpression="^([0-9]*)$" ValidationGroup="Employer2Validation" Text="*" EnableClientScript="false"></asp:RegularExpressionValidator>
                                                                <asp:RangeValidator ID="rvAverageWeeklyHours2" runat="server" MaximumValue="168" MinimumValue="0"  ErrorMessage="Maximum number of hours in a week is 168" 
                                                                    ValidationGroup="Employer2Validation" ControlToValidate="txtAverageWeeklyHours2" Display="None" ForeColor="Red" Type="Integer" 
                                                                    Text="*" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPrimaryRole2Label" runat="server" Text="Primary Role" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPrimaryRole2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvPrimaryRole2" runat="server" ControlToValidate="ddlPrimaryRole2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Primary Role is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPracticeSetting2Label" runat="server" Text="Practice Setting" />
                                                                <%--<asp:ImageButton ID="ibPracticeSetting2Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibPracticeSetting1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPracticeSetting2" runat="server" CssClass="dd-practice"/>
                                                                <asp:RequiredFieldValidator ID="rfvPracticeSetting2" runat="server" ControlToValidate="ddlPracticeSetting2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Practice Setting is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblMajorServices2Label" runat="server" Text="Major Services" />
                                                                <%--<asp:ImageButton ID="ibMajorServices2Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibMajorServices1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlMajorServices2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMajorServices2SelectedIndexChanged"/>
                                                                <asp:RequiredFieldValidator ID="rfvMajorServices2" runat="server" ControlToValidate="ddlMajorServices2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Major Service is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr id="trHealthCondition2" runat="server" visible="false">
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblHealthCondition2Label" runat="server" Text="Health Condition" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlHealthCondition2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvHealthCondition2" runat="server" ControlToValidate="ddlHealthCondition2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please select a Health Condition for Employer 2."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblClientAgeRange2Label" runat="server" Text="Client Age Range" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlClientAgeRange2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvClientAgeRange2" runat="server" ControlToValidate="ddlClientAgeRange2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Client Age Range is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblFundingSource2Label" runat="server" Text="Funding Source" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlFundingSource2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvFundingSource2" runat="server" ControlToValidate="ddlFundingSource2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Funding Source is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                            <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="TERTIARY SITE"
                                                ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table cellpadding="2" cellspacing="4" width="100%">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblStatus3Title" runat="server" Text="Status" />&nbsp;
                                                                <%--<asp:ImageButton ID="ibStatus3Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibStatus1HelpClick" />--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlStatus3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus3SelectedIndexChanged" />
                                                                <asp:Label ID="lblStatus3Message" runat="server" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblStartDate3Label" runat="server" Text="Start Date" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtStartDate3" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar5" runat="server" AutoPostBack="False" BlankFieldText="mm dd yyyy"
                                                                    Format="mm dd yyyy" Separator="/" Control="txtStartDate3" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" From-Date="1950-01-01" />
                                                                <asp:RequiredFieldValidator ID="rfvStartDate3" runat="server" ControlToValidate="txtStartDate3"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 3: Start Date is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer3Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblEndDate3Label" runat="server" Text="End Date" />&nbsp;
                                                                <%--<asp:ImageButton ID="ibEndDate3Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibEndDate1HelpClick" />--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtEndDate3" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar6" runat="server" AutoPostBack="False" BlankFieldText="mm dd yyyy"
                                                                    Format="mm dd yyyy" Separator="/" Control="txtEndDate3" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="LeftTitle">
                                                                <asp:Label ID="lblEmployerName3Label" runat="server" Text="Employer Name" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtEmployerName3" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                                <asp:RequiredFieldValidator ID="rfvEmployerName3" runat="server" ControlToValidate="txtEmployerName3"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 3: Employer name is blank."
                                                                    Display="None" ValidationGroup="Employer3Validation" ForeColor="Red" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblAddress3_1Label" runat="server" Text="Address" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAddress3_1" runat="server" MaxLength="50"/>
                                                                <asp:RequiredFieldValidator ID="rfvAddress3_1" runat="server" ControlToValidate="txtAddress3_1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 3: Address is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer3Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAddress3_2" runat="server" MaxLength="50"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCity3Label" runat="server" Text="City" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtCity3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCity3" runat="server" ControlToValidate="txtCity3"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 3: City is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer3Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvince3Label" runat="server" Text="Province" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince3" runat="server" ControlToValidate="ddlProvince3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPostalCode3Label" runat="server" Text="Postal Code" />
                                                                <%--<asp:ImageButton ID="ibPostalCode3Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibPostalCode1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtPostalCode3" runat="server" Columns="10" />
                                                                <ajaxToolkit:MaskedEditExtender ID="meePostalCode3" TargetControlID="txtPostalCode3"
                                                                Mask="LNLNLN" ClearMaskOnLostFocus="false" MaskType="None" ErrorTooltipEnabled="True" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvPostalCode3" runat="server" ControlToValidate="txtPostalCode3"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 3: Postal Code is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer3Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revPostalCode3" runat="server" EnableClientScript="false"
                                                                    ControlToValidate="txtPostalCode3" SetFocusOnError="True" ErrorMessage="Employer 3: Please provide a valid postal code without any spaces"
                                                                    Display="None" ValidationExpression="^([abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][a-zA-Z][0-9][a-zA-Z][0-9])$"
                                                                    ValidationGroup="Employer3Validation" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPostalCodeReflectPractice3Label" runat="server" Text="Does the postal code reflect site of practice?" />
                                                                <%--<asp:ImageButton ID="ibPostalCodeReflection3Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibPostalCodeReflectionHelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPostalCodeReflectPractice3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvPostalCodeReflectPractice3" runat="server" ControlToValidate="ddlPostalCodeReflectPractice3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: Does the postal code reflect site of practice?"
                                                                    Display="None" EnableClientScript="false" Text="*" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry3Label" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry3" runat="server" ControlToValidate="ddlCountry3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: Country is blank."
                                                                    Display="None" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblTelephone3Label" runat="server" Text="Telephone" />
                                                                <%--<asp:ImageButton ID="ibPhone3Help" ImageUrl="~/images/qmark.jpg" runat="server" OnClick="ibPhone1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtTelephone3" runat="server" Columns="30" />
                                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender5" TargetControlID="txtTelephone3"
                                                                    Mask="(999) 999-9999 x99999" MaskType="None" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                    OnInvalidCssClass="MaskedEditError" ClearMaskOnLostFocus="false" InputDirection="LeftToRight" 
                                                                    runat="server" Filtered="xX" />
                                                                <asp:RequiredFieldValidator ID="rfvTelephone3" runat="server" ControlToValidate="txtTelephone3"  
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 3: Phone is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer3Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revTelephone3" runat="server" ControlToValidate="txtTelephone3" ErrorMessage="Please provide all information for Employer 3: Phone number is invalid." Display="None"
                                                                EnableClientScript="false" Text="*" ValidationGroup="Employer3Validation" ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}( )?([x]?(\d+)?)?$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblFax3Label" runat="server" Text="Fax" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtFax3" runat="server" Columns="30" />
                                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender6" TargetControlID="txtFax3"
                                                                    Mask="(999) 999-9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                    OnInvalidCssClass="MaskedEditError" ClearMaskOnLostFocus="false" InputDirection="LeftToRight"
                                                                    ErrorTooltipEnabled="True" runat="server" Filtered="xX" MaskType="None"/>
                                                                <asp:RegularExpressionValidator ID="revFax3" runat="server" ControlToValidate="txtFax3" ErrorMessage="Please provide all information for Employer 3: Fax number is invalid." Display="None"
                                                                EnableClientScript="false" Text="*" ValidationGroup="Employer3Validation" ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblEmploymentRelationship3Label" runat="server" Text="Employment Relationship" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlEmploymentRelationship3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvEmploymentRelationship3" runat="server" ControlToValidate="ddlEmploymentRelationship3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: Employment Relationship is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCasualStatus3Label" runat="server" Text="Full / Part Time/ Casual Status" />
                                                                <%--<asp:ImageButton ID="ImageButton1" ImageUrl="~/images/qmark.jpg" runat="server" OnClick="ibCasualStatusHelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCasualStatus3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCasualStatus3" runat="server" ControlToValidate="ddlCasualStatus3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 2: Full / Part Time Status is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblAverageWeeklyHours3Label" runat="server" Text="Average Weekly Hours (no ranges, no decimal points)" />
                                                                <%--<asp:ImageButton ID="ImageButton2" ImageUrl="~/images/qmark.jpg" runat="server" OnClick="ibAverageWeeklyHours1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAverageWeeklyHours3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvAverageWeeklyHours3" runat="server" ControlToValidate="txtAverageWeeklyHours3"
                                                                    ForeColor="Red" ErrorMessage="Please provide all information for Employer 3: Average Weekly Hours is blank."
                                                                    Display="None" ValidationGroup="Employer3Validation" Text="*" EnableClientScript="false" />
                                                                <asp:RegularExpressionValidator ID="revAverageWeeklyHours3" runat="server" ControlToValidate="txtAverageWeeklyHours3"
                                                                    ForeColor="Red" ErrorMessage="Average Weekly Hours Employment 3: Please use whole numbers. Decimal points are not permitted." Display="None"
                                                                    ValidationExpression="^([0-9]*)$" ValidationGroup="Employer3Validation" Text="*" EnableClientScript="false"></asp:RegularExpressionValidator>
                                                                <asp:RangeValidator ID="rvAverageWeeklyHours3" runat="server" MaximumValue="168" MinimumValue="0"  ErrorMessage="Maximum number of hours in a week is 168" ValidationGroup="Employer3Validation"
                                                                    ControlToValidate="txtAverageWeeklyHours3" Display="None" ForeColor="Red" Type="Integer" Text="*" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPrimaryRole3Label" runat="server" Text="Primary Role" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPrimaryRole3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvPrimaryRole3" runat="server" ControlToValidate="ddlPrimaryRole3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: Primary Role is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPracticeSetting3Label" runat="server" Text="Practice Setting" />
                                                                <%--<asp:ImageButton ID="ibPracticeSetting3Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibPracticeSetting1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPracticeSetting3" runat="server" CssClass="dd-practice" />
                                                                <asp:RequiredFieldValidator ID="rfvPracticeSetting3" runat="server" ControlToValidate="ddlPracticeSetting3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: Practice Setting is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblMajorServices3Label" runat="server" Text="Major Services" />
                                                                <%--<asp:ImageButton ID="ibMajorServices3Help" ImageUrl="~/images/qmark.jpg" runat="server"
                                                                    OnClick="ibMajorServices1HelpClick" />&nbsp;--%>
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlMajorServices3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMajorServices3SelectedIndexChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvMajorServices3" runat="server" ControlToValidate="ddlMajorServices3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: Major Service is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr id="trHealthCondition3" runat="server" visible="false">
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblHealthCondition3Label" runat="server" Text="Health Condition" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlHealthCondition3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvHealthCondition3" runat="server" ControlToValidate="ddlHealthCondition3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please select a Health Condition for Employer 3."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblClientAgeRange3Label" runat="server" Text="Client Age Range" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlClientAgeRange3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvClientAgeRange3" runat="server" ControlToValidate="ddlClientAgeRange3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: Client Age Range is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblFundingSource3Label" runat="server" Text="Funding Source" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlFundingSource3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvFundingSource3" runat="server" ControlToValidate="ddlFundingSource3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: Funding Source is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                        </ajaxToolkit:TabContainer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" UseSubmitBehavior="true" TabIndex="0"/>--%>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="errorsPanel" runat="server"  Style="display: none; width: 750px;" CssClass="modalPopup" DefaultButton="okBtn">
                    <table width="100%" cellspacing="2" cellpadding="2">
                        <tr class="topHandleRed">
                            <td align="left" runat="server" id="tdCaption" style="padding: 5px;">
                                <asp:Label ID="lblCaption" runat="server" Text="Error Messages:" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="text-align: left; padding: 8px;">
                                    <asp:Label ID="lblErrorMessage" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary4" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="GeneralValidation" />
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Employer1Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Employer2Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary3" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Employer3Validation" />
                                    <div style="text-align: right">
                                        <asp:Button ID="okBtn" runat="server" Text="Ok" Width="100px" /></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="modalPopupEx" runat="server" PopupControlID="errorsPanel"
                    TargetControlID="invisibleTarget" CancelControlID="okBtn" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Label ID="invisibleTarget" runat="server" Style="display: none" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
