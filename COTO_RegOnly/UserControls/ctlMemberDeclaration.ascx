﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberDeclaration.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlMemberDeclaration" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<div class="MainForm">
    <center>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr class="HeaderTitle" align="right">
                <td>
                    <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 11 of 12" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                    <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Next >" OnClick="btnUpdateClick"
                        ValidationGroup="PersonalValidation" />--%>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <uc:MessageBox ID="omb" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <table class="memberInfo" width="100%" cellspacing="2" cellpadding="3">
                        <tr class="RowTitle">
                            <td colspan="2">
                                <div>
                                    <asp:Label ID="lblDeclarationTitle" CssClass="heading" runat="server" Text="Declaration" />
                                </div>
                                
                            </td>
                        </tr>
                       <%-- <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblDeclarationText" runat="server" Font-Bold="true" Font-Size="Larger" Text="Once you complete this page you are no longer able to access
                                your online form. Please ensure that your form is complete before you proceed to the payment page." />
                            </td>
                        </tr>--%>
                        <%--<tr>
                            <td colspan="2" style="text-align: left;">
                                <p>
                                    <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" />
                                </p>
                            </td>
                        </tr>--%>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <div style="padding-top: 10px; padding-bottom: 10px;">
                                    <asp:Label ID="lblRegDeclarationTitle" runat="server" Text="Registration Declaration" Font-Bold="true"
                                    Font-Size="Larger" />
                                </div>
                                <br />
                                <p>
                                    I hereby certify that the statements made 
                                    by me in this renewal form are complete and correct to the best of my knowledge and belief. 
                                    I understand that the College reserves the right to verify any information I provide. 
                                    I understand that a false or misleading statement may be considered professional misconduct and/or result in revocation of my certificate of registration.
                                </p>
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>--%>
                        <tr>
                            <td style="width:60%; text-align: right;">
                                <asp:Label ID="lblAgreeTitle" runat="server" Text="I agree" Font-Bold="true" />
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlAgree" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblQualityAssuranceDeclarationTitle" runat="server" Text="Quality Assurance Declaration" Font-Bold="true"
                                    Font-Size="Larger" /><br />
                                <p>
                                    The <i>Regulated Health Professions Act, 1991</i>, requires the College to establish and maintain a <i>Quality Assurance Program</i>.
                                    Under <i>the Occupational Therapy Act, 1991</i>, every registrant must participate in the Quality Assurance Program and comply 
                                    with the requirements (O. Reg. 226/96: General).
                                </p>
                                <p>
                                    I hereby declare that I understand my obligation to participate in the Quality Assurance program and agree to complete 
                                    the mandatory requirements, including the Self-Assessment, Professional Development Plan, and Prescribed Regulatory 
                                    Education Program (PREP) by the defined due dates.
                                </p>
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>--%>
                        <tr>
                            <td style="width:60%; text-align: right;">
                                <asp:Label ID="lblUnderstandTitle" runat="server" Text="I agree" Font-Bold="true" />
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlUnderstand" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <%--<tr>
                            <td colspan="2">
                                <asp:LinkButton ID="lbtnOfflinePayment" runat="server" Text="Offline Payment" OnClick="lbtnOfflinePaymentClick" /><br />
                                <asp:LinkButton ID="lbtnAnnualRegistrationSummary" runat="server" Text="Annual Registration Summary" OnClick="lbtnAnnualRegistrationSummaryClick" /><br />
                                <asp:LinkButton ID="lbtnRenewalFeedback" runat="server" Text="Renewal Feedback" OnClick="lbtnRenewalFeedbackClick" /><br />
                            </td>
                        </tr>--%>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                </td>
            </tr>
        </table>
    </center>
</div>