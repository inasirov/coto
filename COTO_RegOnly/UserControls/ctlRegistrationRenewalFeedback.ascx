﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlRegistrationRenewalFeedback.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlRegistrationRenewalFeedback" %>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="HeaderTitle" align="left">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Renewal Feedback" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr id="trfeedbackMain" runat="server">
                        <td>
                            <table style="width: 100%">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <p>
                                            Feedback about your experience? We would love to hear it!
                                        </p>
                                        <p>
                                            Enter your feedback in the text field below and press &#39;Update&#39; when complete.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtFeedbackInfo" runat="server" Rows="6" Columns="60" TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff; text-align: right; vertical-align: middle">
                                        <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                                        <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/Images/btn_Update.jpg"
                                            OnClick="btnSubmitClick" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trfeedbackThankYou" runat="server" visible="false">
                        <td>
                            <table style="width: 100%">
                                <tr style="text-align: left">
                                    <td>
                                        <span>Thank you for providing us with feedback. </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff; text-align: right; vertical-align: middle">
                                        <asp:ImageButton ID="ibtnBack1" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
