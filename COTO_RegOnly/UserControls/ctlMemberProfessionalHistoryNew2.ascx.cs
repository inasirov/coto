﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;
using Classes;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberProfessionalHistoryNew2 : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;
        private string PrevStep = WebConfigItems.Step7;
        private string NextStep = WebConfigItems.Step9;

        private const int CurrentStep = 8;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.RenewalStep = CurrentStep;
                BindLists();
                BindData();
                
                if (!string.IsNullOrEmpty(WebConfigItems.Application_Step6_RegulatoryForm))
                {
                    var link = WebConfigItems.Application_Step6_RegulatoryForm;
                    //lbtnRegulatoryFormLink.Attributes.Add("onclick", string.Format("javascript:window.open('{0}', '_blank')", link));
                }
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            if ((ddlOutsideOntarioPractice.SelectedValue == "Yes" && ValidationCheck()) || ddlOutsideOntarioPractice.SelectedValue == "No")
            {
                UpdateUserPracticeHistory();
                UpdateSteps(1);
                Session["CustomAction"] = "renewal";
                Response.Redirect(NextStep);
            }
            else
            {
                if (Messages != null && Messages.Count > 0)
                {
                    string message = string.Empty;
                    foreach (var item in Messages)
                    {
                        message += ((PageMessage)item).Message + "<br />";
                    }
                    message = string.Format("<font color='Red'>{0}</font>", message);

                    Messages.Clear();
                    ShowMessage(message);
                }
            }
        }

        protected void ddlLastCountryOTPractice_SelectedIndexChanged(object sender, EventArgs e)
        {
            var countryCode = ddlLastCountryOTPractice.SelectedValue;
            if (countryCode == "USA" || countryCode == "CAN")
            {
                trOutsideON6.Visible = true;
                trOutsideON7.Visible = true;
            }
            else
            {
                trOutsideON6.Visible = false;
                trOutsideON7.Visible = false;
            }
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void lvJurisdictionsCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "EditRecord")
            {
                int empID = int.Parse((string)e.CommandArgument);
                BindJurisdiction(empID);
                mpext6.Show();
            }

            if (e.CommandName == "Remove")
            {
                int empID = int.Parse((string)e.CommandArgument);
                // Delete Juridiction, rebind the list;
                var repository = new Repository();
                var item = repository.GetAppProfessionalHistoryRecord(empID);
                if (item!= null)
                {
                    item.RegType = string.Empty;
                    repository.UpdateProfessionalHistoryDetailLogged(CurrentUserId, item);

                    BindData();
                }
            }
        }

        protected void lvOtherProfessionsCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "EditRecord")
            {
                int empID = int.Parse((string)e.CommandArgument);
                BindJurisdiction(empID);
                mpext6.Show();
            }

            if (e.CommandName == "Remove")
            {
                int empID = int.Parse((string)e.CommandArgument);
                // Delete Juridiction, rebind the list;
                var repository = new Repository();
                var item = repository.GetAppProfessionalHistoryRecord(empID);
                if (item != null)
                {
                    item.RegType = string.Empty;
                    repository.UpdateProfessionalHistoryDetailLogged(CurrentUserId, item);

                    BindData();
                }
            }
        }

        public void lbtnNewClick(object sender, EventArgs e)
        {
            ddlRegBody.Enabled = true;
            txtRegBodyOther.Enabled = true;
            ddlProvince.Enabled = true;
            ddlCountry.Enabled = true;
            ddlInitMonth.Enabled = true;
            ddlInitYear.Enabled = true;
            ddlNameProfession.Enabled = true;
            txtNameProfessionOther.Enabled = true;
            ddlOtherRegBody.Enabled = true;
            txtOtherRegBodyOther.Enabled = true;


            hfPracticeSeqn.Value = "0";
            hfRegType.Value = "OT";
            trNameProfession.Visible = false;
            trNameProfessionOther.Visible = false;
            trOtherRegBody.Visible = false;
            trOtherRegBodyOther.Visible = false;
            trRegBody.Visible = true;
            //ddlRegBody.SelectedIndex = 0;
            trRegBodyOther.Visible = false;
            cbNoExpiration.Checked = false;
            BindRegBodies();
            mpext6.Show();
        }

        public void lbtnNew2Click(object sender, EventArgs e)
        {
            ddlRegBody.Enabled = true;
            txtRegBodyOther.Enabled = true;
            ddlProvince.Enabled = true;
            ddlCountry.Enabled = true;
            ddlInitMonth.Enabled = true;
            ddlInitYear.Enabled = true;
            ddlNameProfession.Enabled = true;
            txtNameProfessionOther.Enabled = true;
            ddlOtherRegBody.Enabled = true;
            txtOtherRegBodyOther.Enabled = true;

            hfPracticeSeqn.Value = "0";
            hfRegType.Value = "OTHER";
            trRegBody.Visible = false;
            trRegBodyOther.Visible = false;
            trNameProfession.Visible = true;
            ddlNameProfession.SelectedIndex = 0;
            trNameProfessionOther.Visible = false;
            txtNameProfessionOther.Text = string.Empty;
            trOtherRegBody.Visible = true;
            trOtherRegBodyOther.Visible = false;
            txtOtherRegBodyOther.Text = string.Empty;
            cbNoExpiration.Checked = false;

            //BindRegBodies2();
            mpext6.Show();
        }

        protected void ibRegulatoryBodyHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("If the Regulatory Body is not listed, please select 'Other' from the bottom of the dropdown menu, and enter the name of the Regulatory body in the second field titled 'Regulatory Body Other'", "Help - Regulatory Body");
            //update.Update();
        }

        protected void ibProvinceStateHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("If the province/state is not listed, please select '1_Not Applicable' from the top of the dropdown menu.", "Help - Province / State");
            //update.Update();
        }

        protected void ddlRegBody_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlRegBody.SelectedValue))
            {
                if (ddlRegBody.SelectedItem.Text.ToUpper() == "OTHER")
                {
                    trRegBodyOther.Visible = true;
                }
                else
                {
                    trRegBodyOther.Visible = false;
                    txtRegBodyOther.Text = string.Empty;
                }
            }
            else
            {
                ClearJurisdictFields();
            }
            mpext6.Show();
        }

        protected void cbNoExpirationCheckedChanged(object sender, EventArgs e)
        {
            rfvExpiryDate.Enabled = true;
            if (cbNoExpiration.Checked)
            {
                txtExpiryDate.Text = string.Empty;
                rfvExpiryDate.Enabled = false;
            }
            mpext6.Show();
        }

        protected void ddlNameProfession_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlNameProfession.SelectedValue))
            {
                if (ddlNameProfession.SelectedItem.Text.ToUpper() == "OTHER")
                {
                    trNameProfessionOther.Visible = true;
                    ddlOtherRegBody.Items.Clear();
                    ddlOtherRegBody.DataBind();
                    ddlOtherRegBody.Items.Add("Other");
                    trOtherRegBodyOther.Visible = true;
                }
                else
                {
                    trNameProfessionOther.Visible = false;
                    txtNameProfessionOther.Text = string.Empty;

                    trOtherRegBodyOther.Visible = false;
                    txtOtherRegBodyOther.Text = string.Empty;

                    ddlOtherRegBody.Items.Clear();
                    var repository = new Repository();
                    var list = repository.GetGeneralList("REG_OTHER_PROF_BODIES");
                    var subList = ddlNameProfession.SelectedValue + "_";
                    list = list.Where(I => I.Code.StartsWith(subList)).OrderBy(I=>I.Description).ToList();

                    var history = repository.GetAppProfessionalHistoryRecords(CurrentUserId);
                    var list2 = history.Where(I => I.RegType == "OTHER").OrderBy(I=>I.OtherProfessionTypeDescription).ToList();
                    foreach(var item in list2)
                    {
                        string otRegBody = item.OtherRegulatoryBody;
                        var fItem = list.Find(I => I.Code == otRegBody);
                        if (fItem != null) list.Remove(fItem);
                    }

                    ddlOtherRegBody.DataSource = list;
                    ddlOtherRegBody.DataValueField = "Code";
                    ddlOtherRegBody.DataTextField = "Description";
                    ddlOtherRegBody.DataBind();
                    ddlOtherRegBody.Items.Insert(0, string.Empty);
                }
            }
            else
            {
                trNameProfessionOther.Visible = false;
                trOtherRegBodyOther.Visible = false;
                ClearProfFields();
            }
            mpext6.Show();
        }

        protected void ddlOtherRegBody_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOtherRegBody.SelectedItem.Text.ToUpper() == "OTHER")
            {
                trOtherRegBodyOther.Visible = true;
            }
            else
            {
                txtOtherRegBodyOther.Text = string.Empty;
                trOtherRegBodyOther.Visible = false;
            }
            mpext6.Show();
        }

        protected void btnSaveClick(object sender, EventArgs e)
        {
            var item = new ProfessionalHistoryPracticeDetailNew();
            int seqn = 0;
            int.TryParse(hfPracticeSeqn.Value, out seqn);
            item.SEQN = seqn;
            item.RegType = hfRegType.Value;
            if (ddlRegBody.SelectedItem!= null) item.RegulatoryBody = ddlRegBody.SelectedValue;
            item.RegulatoryBodyOther = txtRegBodyOther.Text;
            if (ddlNameProfession.SelectedItem != null) item.OtherProfessionType = ddlNameProfession.SelectedValue;
            item.OtherProfessionTypeOther = txtNameProfessionOther.Text;
            if (ddlOtherRegBody.SelectedItem != null) item.OtherRegulatoryBody = ddlOtherRegBody.SelectedValue;
            item.OtherRegulatoryBodyOther = txtOtherRegBodyOther.Text;
            item.Province = ddlProvince.SelectedValue;
            item.Country = ddlCountry.SelectedValue;
            item.RegNumber = txtLicense.Text;
            item.RegStatus = txtRegStatus.Text;
            item.InitMonth = ddlInitMonth.SelectedValue;
            item.InitYear = ddlInitYear.SelectedValue;

            var culture = new CultureInfo("en-CA");
            //CultureInfo provider = CultureInfo.InvariantCulture;
            if (!string.IsNullOrEmpty(txtExpiryDate.Text))
            {
                DateTime expDate = DateTime.ParseExact(txtExpiryDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                item.ExpiryDate = expDate;
            }
            else
            {
                item.ExpiryDate = DateTime.MinValue;
            }
            var repository = new Repository();
            repository.UpdateProfessionalHistoryDetailLogged(CurrentUserId, item);
            ClearJurisdictFields();
            ClearProfFields();
            mpext6.Hide();
            BindData();
        }

        protected void btnEditCancelCancelClick(object sender, EventArgs e)
        {
            ClearJurisdictFields();
            ClearProfFields();
            mpext6.Hide();
        }

        protected void txtExpiryDate_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtExpiryDate.Text))
            {
                rfvExpiryDate.Enabled = true;
                cbNoExpiration.Checked = false;
            }
            else
            {
                rfvExpiryDate.Enabled = false;
            }
            mpext6.Show();
        }

        protected void PopCalendar1_SelectionChanged(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(txtExpiryDate.Text))
            //{
            //    rfvExpiryDate.Enabled = true;
            //}
            //else
            //{
            //    rfvExpiryDate.Enabled = false;
            //}
            //mpext6.Show();
        }

        protected void ddlOutsideOntarioPracticeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOutsideOntarioPractice.SelectedValue == "Yes")
            {
                //trOutsideON1.Visible = true;
                trOutsideON2.Visible = true;
                trOutsideON3.Visible = true;
                trOutsideON4.Visible = true;
                trOutsideON5.Visible = true;
                trOutsideON6.Visible = true;
                trOutsideON7.Visible = true;
                //trOutsideON8.Visible = true;
            }
            else
            {
                //trOutsideON1.Visible = false;
                trOutsideON2.Visible = false;
                trOutsideON3.Visible = false;
                trOutsideON4.Visible = false;
                trOutsideON5.Visible = false;
                trOutsideON6.Visible = false;
                trOutsideON7.Visible = false;
                //trOutsideON8.Visible = false;
            }
        }

        #endregion

        #region Methods

        protected void BindRegBodies()
        {
            var repository = new Repository();

            var list0 = repository.GetGeneralList("REG_BODIES"); // get data for status field
            list0 = list0.OrderBy(I => I.Description).ToList();
            var history = repository.GetAppProfessionalHistoryRecords(CurrentUserId);

            var list1 = history.Where(I => I.RegType == "OT").OrderBy(I=>I.RegulatoryBodyDescription).ToList();
            
            foreach(var item in list1)
            {
                var regBody = item.RegulatoryBody;
                var fItem = list0.Find(I => I.Code == regBody);
                if (fItem != null) list0.Remove(fItem);
            }
            ddlRegBody.DataSource = list0;
            ddlRegBody.DataValueField = "Code";
            ddlRegBody.DataTextField = "Description";
            ddlRegBody.DataBind();
            //ddlRegBody.Items.Insert(0, string.Empty);
        }

        protected void UpdateUserPracticeHistory()
        {
            User user = new User();
            user.Id = CurrentUserId;

            user.PracticeHistory = new PracticeHistory();
            //user.PracticeHistory.FirstCountryPractice = ddlFirstCountryOTPractice.SelectedValue;
            //user.PracticeHistory.FirstProvinceStatePractice = ddlFirstStateOTPractice.SelectedValue;
            //user.PracticeHistory.FirstProvincePractice = ddlFirstProvinceOTPractice.SelectedValue;
            //user.PracticeHistory.FirstYearPractice = ddlFirstYearOTPractice.SelectedValue;
            //user.PracticeHistory.FirstYearCanadianPractice = ddlFirstYearCanadianOTPractice.SelectedValue;

            if (ddlOutsideOntarioPractice.SelectedIndex == 1)
            {
                user.PracticeHistory.LastYearOutsideOntarioPractice = ddlLastYearOutsideOntario.SelectedValue;
                user.PracticeHistory.LastCountryPractice = ddlLastCountryOTPractice.SelectedValue;
                //user.PracticeHistory.LastProvincePractice = ddlLastCanadianProvince.SelectedValue;
                
                if (user.PracticeHistory.LastCountryPractice == "USA" || user.PracticeHistory.LastCountryPractice == "CAN")
                {
                    user.PracticeHistory.LastProvinceStatePractice = ddlLastStateOrCanadianProvince.SelectedValue;
                }
            }
            else
            {
                user.PracticeHistory.LastCountryPractice = string.Empty;
                //user.PracticeHistory.LastProvincePractice = ddlLastCanadianProvince.SelectedValue;
                user.PracticeHistory.LastProvinceStatePractice = string.Empty;
                user.PracticeHistory.LastYearOutsideOntarioPractice = string.Empty;
            }
            var repository = new Repository();
            repository.UpdateUserPracticeHistoryLogged(CurrentUserId, user);
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        protected void BindLists()
        {
            var repository = new Repository();

            //var list0 = repository.GetGeneralList("REG_BODIES"); // get data for status field
            //ddlRegBody.DataSource = list0;
            //ddlRegBody.DataValueField = "Code";
            //ddlRegBody.DataTextField = "Description";
            //ddlRegBody.DataBind();
            //ddlRegBody.Items.Insert(0, string.Empty);

            var list1 = repository.GetGeneralList("COUNTRY");
            list1 = list1.OrderBy(I => I.Description).ToList();
            ddlCountry.DataSource = list1;
            ddlCountry.DataValueField = "Code";
            ddlCountry.DataTextField = "Description";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, string.Empty);

            //ddlFirstCountryOTPractice.DataSource = list1;
            //ddlFirstCountryOTPractice.DataValueField = "Code";
            //ddlFirstCountryOTPractice.DataTextField = "Description";
            //ddlFirstCountryOTPractice.DataBind();
            //ddlFirstCountryOTPractice.Items.Insert(0, string.Empty);

            ddlLastCountryOTPractice.DataSource = list1;
            ddlLastCountryOTPractice.DataValueField = "Code";
            ddlLastCountryOTPractice.DataTextField = "Description";
            ddlLastCountryOTPractice.DataBind();
            ddlLastCountryOTPractice.Items.Insert(0, string.Empty);

            var list2 = repository.GetGeneralList("PROVINCE_STATE");

            ddlProvince.DataSource = list2;
            ddlProvince.DataValueField = "Code";
            ddlProvince.DataTextField = "Description";
            ddlProvince.DataBind();
            ddlProvince.Items.Insert(0, string.Empty);

            //ddlFirstStateOTPractice.DataSource = list2;
            //ddlFirstStateOTPractice.DataValueField = "Code";
            //ddlFirstStateOTPractice.DataTextField = "Description";
            //ddlFirstStateOTPractice.DataBind();
            //ddlFirstStateOTPractice.Items.Insert(0, string.Empty);

            ddlLastStateOrCanadianProvince.DataSource = list2;
            ddlLastStateOrCanadianProvince.DataValueField = "Code";
            ddlLastStateOrCanadianProvince.DataTextField = "Description";
            ddlLastStateOrCanadianProvince.DataBind();
            ddlLastStateOrCanadianProvince.Items.Insert(0, string.Empty);

            var list3 = repository.GetGeneralList("REG_OTHER_MONTH");

            ddlInitMonth.DataSource = list3;
            ddlInitMonth.DataValueField = "Code";
            ddlInitMonth.DataTextField = "Description";
            ddlInitMonth.DataBind();
            ddlInitMonth.Items.Insert(0, string.Empty);

            var list4 = repository.GetGeneralList("REG_OTHER_YEAR").OrderByDescending(I => I.Code).ToList();

            ddlInitYear.DataSource = list4;
            ddlInitYear.DataValueField = "Code";
            ddlInitYear.DataTextField = "Description";
            ddlInitYear.DataBind();
            ddlInitYear.Items.Insert(0, string.Empty);

            var list5 = repository.GetGeneralList("REG_OTHER_PROF");
            list5 = list5.Where(I => I.Code != "OTHER").ToList();

            ddlNameProfession.DataSource = list5;
            ddlNameProfession.DataValueField = "Code";
            ddlNameProfession.DataTextField = "Description";
            ddlNameProfession.DataBind();
            ddlNameProfession.Items.Insert(0, string.Empty);

            var list6 = new List<GenClass>();
            //list6.Add(new GenClass { Code = "", Description = "" });
            list6.Add(new GenClass { Code = "No", Description = "No" });
            list6.Add(new GenClass { Code = "Yes", Description = "Yes" });

            ddlOutsideOntarioPractice.DataSource = list6;
            ddlOutsideOntarioPractice.DataValueField = "Code";
            ddlOutsideOntarioPractice.DataTextField = "Description";
            ddlOutsideOntarioPractice.DataBind();

            var list7 = repository.GetGeneralList("grad_year");
            try
            {
                list7 = list7.Where(I => I.Code != "1_NA").ToList();
                list7 = list7.Where(I => int.Parse(I.Code) <= DateTime.Now.Year).ToList();
            }
            catch { }

            ddlLastYearOutsideOntario.DataSource = list7;
            ddlLastYearOutsideOntario.DataValueField = "Code";
            ddlLastYearOutsideOntario.DataTextField = "Code";
            ddlLastYearOutsideOntario.DataBind();
            ddlLastYearOutsideOntario.Items.Insert(0, string.Empty);
        }

        protected void BindData()
        {
            var repository = new Repository();
            //lblDebugMessage.Text = "CurrentUserId = " + CurrentUserId;

            var history = repository.GetAppProfessionalHistoryRecords(CurrentUserId);
            
            var list1 = history.Where(I => I.RegType == "OT").OrderBy(I=>I.RegulatoryBodyDescription).ToList();
            lvJurisdictions.DataSource = list1;
            lvJurisdictions.DataBind();

            var list2 = history.Where(I => I.RegType == "OTHER").OrderBy(I=>I.OtherProfessionTypeDescription).ToList();
            lvOtherProfessions.DataSource = list2;
            lvOtherProfessions.DataBind();

            var user = repository.GetUserPracticeHistoryInfo(CurrentUserId);
            if (user != null)
            {
                if (user.PracticeHistory != null)
                {

                    if (!string.IsNullOrEmpty(user.PracticeHistory.LastYearOutsideOntarioPractice))
                    {
                        var findItem = ddlLastYearOutsideOntario.Items.FindByValue(user.PracticeHistory.LastYearOutsideOntarioPractice);
                        if (findItem!=null)  ddlLastYearOutsideOntario.SelectedValue = user.PracticeHistory.LastYearOutsideOntarioPractice;
                        ddlOutsideOntarioPractice.SelectedIndex = 1;
                        //trOutsideON1.Visible = true;
                        trOutsideON2.Visible = true;
                        trOutsideON3.Visible = true;
                        trOutsideON4.Visible = true;
                        trOutsideON5.Visible = true;

                        if (!string.IsNullOrEmpty(user.PracticeHistory.LastCountryPractice))
                        {
                            findItem = ddlLastCountryOTPractice.Items.FindByValue(user.PracticeHistory.LastCountryPractice);
                            if (findItem!=null) ddlLastCountryOTPractice.SelectedValue = user.PracticeHistory.LastCountryPractice;
                            if (user.PracticeHistory.LastCountryPractice == "USA" || user.PracticeHistory.LastCountryPractice == "CAN")
                            {
                                trOutsideON6.Visible = true;
                                trOutsideON7.Visible = true;
                                if (!string.IsNullOrEmpty(user.PracticeHistory.LastProvinceStatePractice))
                                {
                                    findItem = ddlLastStateOrCanadianProvince.Items.FindByValue(user.PracticeHistory.LastProvinceStatePractice);
                                    if (findItem!=null) ddlLastStateOrCanadianProvince.SelectedValue = user.PracticeHistory.LastProvinceStatePractice;
                                }
                            }
                        }
                    }
                }
            }
            //update.Update();
        }

        protected void BindJurisdiction(int recordSEQN)
        {
            var repository = new Repository();
            var item = repository.GetAppProfessionalHistoryRecord(recordSEQN);
            if (item!= null)
            {
                hfPracticeSeqn.Value = recordSEQN.ToString();
                hfRegType.Value = item.RegType;

                if (item.RegType =="OT")
                {
                    trNameProfession.Visible = false;
                    trNameProfessionOther.Visible = false;
                    trOtherRegBody.Visible = false;
                    trOtherRegBodyOther.Visible = false;

                    trRegBody.Visible = true;

                    var list0 = repository.GetGeneralList("REG_BODIES"); // get data for status field
                    list0 = list0.OrderBy(I => I.Description).ToList();
                    ddlRegBody.DataSource = list0;
                    ddlRegBody.DataValueField = "Code";
                    ddlRegBody.DataTextField = "Description";
                    ddlRegBody.DataBind();
                    ddlRegBody.Items.Insert(0, string.Empty);

                    var listItem = ddlRegBody.Items.FindByValue(item.RegulatoryBody);
                    if (listItem != null) ddlRegBody.SelectedValue = listItem.Value;

                    if (ddlRegBody.SelectedValue == "ZOTHER")
                    {
                        trRegBodyOther.Visible = true;
                        txtRegBodyOther.Text = item.RegulatoryBodyOther;
                    }
                    else
                    {
                        trRegBodyOther.Visible = false;
                        txtRegBodyOther.Text = string.Empty;
                    }
                }

                if (item.RegType == "OTHER")
                {
                    trRegBody.Visible = false;
                    trRegBodyOther.Visible = false;

                    trNameProfession.Visible = true;
                    trOtherRegBody.Visible = true;

                    var listItem = ddlNameProfession.Items.FindByValue(item.OtherProfessionType);
                    if (listItem != null) ddlNameProfession.SelectedValue = listItem.Value;

                    if (ddlNameProfession.SelectedItem.Text.ToUpper() == "OTHER")
                    {
                        trNameProfessionOther.Visible = true;
                        txtNameProfessionOther.Text = item.OtherProfessionTypeOther;
                        ddlOtherRegBody.Items.Clear();
                        ddlOtherRegBody.DataBind();
                        ddlOtherRegBody.Items.Add("Other");
                        trOtherRegBodyOther.Visible = true;
                        txtOtherRegBodyOther.Text = item.OtherRegulatoryBodyOther;
                    }
                    else
                    {
                        trNameProfessionOther.Visible = false;
                        txtNameProfessionOther.Text = string.Empty;
                        
                        txtOtherRegBodyOther.Text = string.Empty;

                        ddlOtherRegBody.Items.Clear();
                        //var repository = new Repository();
                        var list = repository.GetGeneralList("REG_OTHER_PROF_BODIES");
                        var subList = ddlNameProfession.SelectedValue + "_";
                        list = list.Where(I => I.Code.StartsWith(subList)).OrderBy(I=>I.Description).ToList();
                        ddlOtherRegBody.DataSource = list;
                        ddlOtherRegBody.DataValueField = "Code";
                        ddlOtherRegBody.DataTextField = "Description";
                        ddlOtherRegBody.DataBind();
                        ddlOtherRegBody.Items.Insert(0, string.Empty);

                        var listItem2 = ddlOtherRegBody.Items.FindByValue(item.OtherRegulatoryBody);
                        if (listItem2 != null) ddlOtherRegBody.SelectedValue = listItem2.Value;

                        if (ddlOtherRegBody.SelectedItem.Text.ToUpper() == "OTHER")
                        {
                            trOtherRegBodyOther.Visible = true;
                            txtOtherRegBodyOther.Text  = item.OtherRegulatoryBodyOther;
                        }
                    }
                }

                ddlProvince.SelectedValue = item.Province;
                ddlCountry.SelectedValue = item.Country;
                txtLicense.Text = item.RegNumber;

                txtRegStatus.Text = item.RegStatus;

                ddlInitMonth.SelectedValue = item.InitMonth;
                ddlInitYear.SelectedValue = item.InitYear;

                if (item.ExpiryDate != DateTime.MinValue)
                {
                    txtExpiryDate.Text = ((DateTime)item.ExpiryDate).ToString("MM/dd/yyyy");
                }
                else
                {
                    cbNoExpiration.Checked = true;
                    rfvExpiryDate.Enabled = false;
                }

                // make read only all fields except Reg.Licence/ Status and Expiry Date
                ddlRegBody.Enabled = false; rfvRegBody.Enabled = false;
                txtRegBodyOther.Enabled = false;
                ddlProvince.Enabled = false;
                ddlCountry.Enabled = false;
                ddlInitMonth.Enabled = false; rfvInitMonth.Enabled = false;
                ddlInitYear.Enabled = false; rfvInitYear.Enabled = false;
                ddlNameProfession.Enabled = false; rfvNameProfession.Enabled = false;
                txtNameProfessionOther.Enabled = false; rfvNameProfessionOther.Enabled = false;
                ddlOtherRegBody.Enabled = false; rfvOtherRegBody.Enabled = false;
                txtOtherRegBodyOther.Enabled = false; rfvOtherRegBodyOther.Enabled = false;
            }
        }

        protected void ClearJurisdictFields()
        {
            txtRegBodyOther.Text = string.Empty;
            ddlProvince.SelectedIndex = 0;
            ddlCountry.SelectedIndex = 0;
            txtLicense.Text = string.Empty;
            txtRegStatus.Text = string.Empty;
            ddlInitMonth.SelectedIndex = 0;
            ddlInitYear.SelectedIndex = 0;
            txtExpiryDate.Text = string.Empty;
            cbNoExpiration.Checked = false;
        }

        protected void ClearProfFields()
        {
            txtNameProfessionOther.Text = string.Empty;
            ddlOtherRegBody.Items.Clear();
            ddlOtherRegBody.Items.Insert(0, string.Empty);
            txtRegBodyOther.Text = string.Empty;
            ddlProvince.SelectedIndex = 0;
            ddlCountry.SelectedIndex = 0;
            txtLicense.Text = string.Empty;
            txtRegStatus.Text = string.Empty;
            ddlInitMonth.SelectedIndex = 0;
            ddlInitYear.SelectedIndex = 0;
            txtExpiryDate.Text = string.Empty;
            cbNoExpiration.Checked = false;
        }

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }
        }

        protected bool ValidationCheck()
        {

            Page.Validate("GeneralValidation");

            if (!Page.IsValid)
            {
                return false;
            }

            return true;
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        /// <summary>
        ///  Messages collection. Contains all custom warning or errors which will be populated on the page.
        /// </summary>
        public PageMessages Messages
        {
            get
            {
                return (PageMessages)SessionParameters.PageMessages;
            }
            set
            {
                SessionParameters.PageMessages = value;
            }
        }




        #endregion

        
    }
}