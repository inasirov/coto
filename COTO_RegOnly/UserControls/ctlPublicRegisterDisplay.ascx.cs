﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlPublicRegisterDisplay : System.Web.UI.UserControl
    {
        private const string VIEW_STATE_CURRENT_USER_ID = "CurrentUserId";

        protected void Page_Load(object sender, EventArgs e)
        {
            string CurrentUserId = string.Empty;

            if (Session[VIEW_STATE_CURRENT_USER_ID] != null)
            {
                CurrentUserId = (string)Session[VIEW_STATE_CURRENT_USER_ID];
                Session[VIEW_STATE_CURRENT_USER_ID] = null;
            }
            if (!string.IsNullOrEmpty(CurrentUserId))
            {
                BindUserInfo(CurrentUserId);
            }
        }

        protected void lvDisciplinesCommand(object sender, ListViewCommandEventArgs e)
        {
            if (!string.IsNullOrEmpty((string)e.CommandArgument))
            {
                string note = (string)e.CommandArgument;
                note = note.Replace(Convert.ToChar(13).ToString(), "<br />");
                note = note.Replace("§", "<br /><br />");
                note = note.Replace(Convert.ToChar(10).ToString(), "<br />");
                ShowMessage(note, "Hearing Summary");
            }
        }

        protected void lbtnSearchAgainClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PublicRegister.aspx");
        }

        protected void lbtnBackSearchResultsClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PublicRegisterProcess.aspx");
        }

        protected void ShowMessage(string Message, string Caption)
        {
            //omb.ShowBigMessage(Message, Caption);
            omb.ShowMessage(Message, Caption);
        }

        protected void BindUserInfo(string CurrentUserId)
        {
            var repository = new Repository();
            var userInfo = repository.GetUserGeneralInfo(CurrentUserId);
            lblNameText.Text = string.Format("{0} {1} {2}", userInfo.FirstName, userInfo.MiddleName, userInfo.LastName);
            var userPrevNames = repository.GetUserPreviousNames(CurrentUserId);

            int found = 0;
            //if (userPrevNames.Count == 0)
            //{
            //    lblFormerNamesEmpty.Text = "None on record";
            //    trFormerNameTitleRow.Visible = false;
            //    trFormerNameDataRow.Visible = false;
            //}
            //else
            //{
            lblLegalFirstNameText.Text = userInfo.LegalFirstName;
            lblLegalLastNameText.Text = userInfo.LegalLastName;
            lblLegalMiddleNameText.Text = userInfo.LegalMiddleName;
                lblFormerNamesEmpty.Text = string.Empty;
                trFormerNameTitleRow.Visible = true;
                trFormerNameDataRow.Visible = true;
                if (!string.IsNullOrEmpty(userInfo.LegalLastName) && userInfo.LegalLastName.Trim() != userInfo.LastName.Trim())
                {
                    lblPreviousFirstNameText.Text += "&nbsp;&nbsp;<br />";
                    lblPreviousMiddleNameText.Text += "&nbsp;&nbsp;<br />";
                    lblPreviousLastNameText.Text += userInfo.LegalLastName + "<br />";
                    found++;
                }
                if (!string.IsNullOrEmpty(userInfo.PreviousLegalLastName) && userInfo.PreviousLegalLastName.Trim() != userInfo.LastName.Trim())
                {
                    lblPreviousFirstNameText.Text += "&nbsp;&nbsp;<br />";
                    lblPreviousMiddleNameText.Text += "&nbsp;&nbsp;<br />";
                    lblPreviousLastNameText.Text += userInfo.PreviousLegalLastName + "<br />";
                    found++;
                }
                string thisName = string.Empty;
                foreach (var item in userPrevNames)
                {
                    if (thisName != item.UF_3)
                    {
                        if (string.IsNullOrEmpty(item.UF_1.Trim()) && string.IsNullOrEmpty(item.UF_2.Trim()) && item.UF_3 != userInfo.LegalLastName && item.UF_3 != userInfo.PreviousLegalLastName.Trim())
                        {
                            lblPreviousFirstNameText.Text += "&nbsp;" + item.UF_1.Trim() + "&nbsp;<br />";
                            lblPreviousMiddleNameText.Text += "&nbsp;" + item.UF_2.Trim() + "&nbsp;<br />";
                            lblPreviousLastNameText.Text += "&nbsp;" + item.UF_3.Trim() + "&nbsp;<br />";
                            found++;
                        }
                        else if (string.IsNullOrEmpty(item.UF_1.Trim()) || string.IsNullOrEmpty(item.UF_2.Trim()))
                        {
                            lblPreviousFirstNameText.Text += "&nbsp;" + item.UF_1.Trim() + "&nbsp;<br />";
                            lblPreviousMiddleNameText.Text += "&nbsp;" + item.UF_2.Trim() + "&nbsp;<br />";
                            if (item.UF_3 != userInfo.LegalLastName && item.UF_3 != userInfo.PreviousLegalLastName.Trim())
                            {
                                lblPreviousLastNameText.Text += "&nbsp;" + item.UF_3.Trim() + "&nbsp;<br />";
                            }
                            else
                            {
                                lblPreviousLastNameText.Text += "&nbsp;&nbsp;<br />";
                            }
                            found++;
                        }
                    }
                    thisName = item.UF_3;
                }

            //}

            if (found==0)   //userPrevNames.Count == 0
            {
                lblFormerNamesEmpty.Text = "None on record";
                trFormerNameTitleRow.Visible = false;
                trFormerNameDataRow.Visible = false;
            }

            var userPrevRegistrations = repository.GetUserPreviousRegistrationNumbers(CurrentUserId);
            foreach (var item in userPrevRegistrations)
            {
                lblPreviousRegistrationText.Text += item.Description + "<br />";
            }

            lblRegistrationText.Text = userInfo.Registration;
            lblCategoryText.Text = userInfo.Category;
            lblCurrentStatusText.Text = userInfo.CurrentStatus;
            if (userInfo.CurrentStatusCode == "AF")
            {
                lblCurrentStatusText.Text = "Inactive";
            }


            var userTerms = repository.GetUserTermsConditions(CurrentUserId);
            if (userTerms.Count > 0)
            {
                foreach (var item in userTerms)
                {
                    string note = item.Description;
                    note = note.Replace(Convert.ToChar(13).ToString(), "<br />");
                    note = note.Replace("§", "<br /><br />");
                    note = note.Replace(Convert.ToChar(10).ToString(), "<br />");
                    lblTermsConditionsText.Text += note + "<br />";
                }
            }
            else
            {
                lblTermsConditionsText.Text = "None";
            }



            if (userInfo.CurrencyLanguageServices != null)
            {
                if (!string.IsNullOrEmpty(userInfo.CurrencyLanguageServices.Lang_Service1))
                {
                    lblLanguageServiceText.Text = userInfo.CurrencyLanguageServices.Lang_Service1;
                }
                if (!string.IsNullOrEmpty(userInfo.CurrencyLanguageServices.Lang_Service2))
                {
                    lblLanguageServiceText.Text += ", " + userInfo.CurrencyLanguageServices.Lang_Service2;
                }
                if (!string.IsNullOrEmpty(userInfo.CurrencyLanguageServices.Lang_Service3))
                {
                    lblLanguageServiceText.Text += ", " + userInfo.CurrencyLanguageServices.Lang_Service3;
                }
                if (!string.IsNullOrEmpty(userInfo.CurrencyLanguageServices.Lang_Service4))
                {
                    lblLanguageServiceText.Text += ", " + userInfo.CurrencyLanguageServices.Lang_Service4;
                }
                if (!string.IsNullOrEmpty(userInfo.CurrencyLanguageServices.Lang_Service5))
                {
                    lblLanguageServiceText.Text += ", " + userInfo.CurrencyLanguageServices.Lang_Service5;
                }
            }

            var userDisciplines = repository.GetUserDisciplinesNew(CurrentUserId);
            lvDisciplines2.DataSource = userDisciplines;
            lvDisciplines2.DataBind();

            //var userDisciplines2 = repository.GetUserDisciplinesNew2(CurrentUserId);
            //lvDisciplines3.DataSource = userDisciplines2;
            //lvDisciplines3.DataBind();

            if (userDisciplines.Count == 0)
            {
                lblDisciplines.Text = "None";
            }

            if (userInfo.InitialRegistrationDate != null)
            {
                lblInitialRegistrationDateText.Text = ((DateTime)userInfo.InitialRegistrationDate).ToString("MMM dd, yyyy");
            }

            if (userInfo.CertificateExpiry != DateTime.MinValue)
            {
                lblCertificateExpiryText.Text = ((DateTime)userInfo.CertificateExpiry).ToString("MMM dd, yyyy");
            }

            if (!string.IsNullOrEmpty(userInfo.CurrentEmploymentStatus) && (userInfo.CurrentEmploymentStatus.Substring(0, 2) == "10" || userInfo.CurrentEmploymentStatus.Substring(0, 2) == "11") && (userInfo.CurrentStatusCode == "A" || userInfo.CurrentStatusCode == "AT" || userInfo.CurrentStatusCode == "AD") && userInfo.MemberType.ToUpper() == "REG")
            {
                Employment first = userInfo.EmploymentList.FirstOrDefault(E => E.Index == 1);
                Employment second = userInfo.EmploymentList.FirstOrDefault(E => E.Index == 2);
                Employment third = userInfo.EmploymentList.FirstOrDefault(E => E.Index == 3);

                if (first == null && second == null && third == null)
                {
                    trPracticeInformation.Visible = false;
                    trPracticeInformation1.Visible = false;
                }

                if (first != null && (!string.IsNullOrEmpty(first.EmployerName)) && userInfo.CurrentStatus != "R" && userInfo.CurrentStatus != "S")
                {
                    pnlPrimaryEmployment.Visible = true;
                    lblPrimaryEmployerNameText.Text = first.EmployerName;
                    if (first.Address != null && (!string.IsNullOrEmpty(first.Address.Address1)))
                    {
                        lblPrimaryEmployerAddressText.Text = first.Address.Address1;
                        trEmployment1Address1.Visible = true;
                    }
                    else
                    {
                        trEmployment1Address1.Visible = false;
                    }

                    if (first.Address != null && (!string.IsNullOrEmpty(first.Address.Address2)))
                    {
                        lblPrimaryEmployerAddress2Text.Text = first.Address.Address2;
                        trEmployment1Address2.Visible = true;
                    }
                    else
                    {
                        trEmployment1Address2.Visible = false;
                    }

                    trEmployment1Address3.Visible = false;

                    if (first.Address != null && (!string.IsNullOrEmpty(first.Address.City)))
                    {
                        lblPrimaryEmployerCityProvincePostalText.Text = first.Address.City;
                        trEmployment1Address3.Visible = true;
                    }

                    if (first.Address != null && (!string.IsNullOrEmpty(first.Address.Province)))
                    {
                        lblPrimaryEmployerCityProvincePostalText.Text += " " + first.Address.Province;
                        trEmployment1Address3.Visible = true;
                    }


                    if (first.Address != null && (!string.IsNullOrEmpty(first.Address.PostalCode)))
                    {
                        lblPrimaryEmployerCityProvincePostalText.Text += " " + first.Address.PostalCode;
                        trEmployment1Address3.Visible = true;
                    }


                    if (!string.IsNullOrEmpty(first.FundingSource))
                    {
                        string updFundingSource = first.FundingSource;
                        //updFundingSource = updFundingSource.Substring(updFundingSource.IndexOf(" ") + 1);
                        lblPrimaryEmploymentFundingSourceText.Text = updFundingSource; // first.FundingSource;
                    }
                    else
                    {
                        lblPrimaryEmploymentFundingSourceText.Text = "n/a";
                    }

                    if (!string.IsNullOrEmpty(first.PracticeSetting))
                    {
                        string updValue = first.PracticeSetting;
                        //updValue = updValue.Substring(updValue.IndexOf(" ") + 1);
                        lblPrimaryEmploymentPracticeSettingText.Text = updValue; // first.PracticeSetting;
                    }
                    else
                    {
                        lblPrimaryEmploymentPracticeSettingText.Text = "n/a";
                    }

                    if (!string.IsNullOrEmpty(first.MajorServices))
                    {
                        string updValue = first.MajorServices;
                        //updValue = updValue.Substring(updValue.IndexOf(" ") + 1);
                        lblPrimaryEmploymentMajorServiceText.Text = updValue;
                    }
                    else
                    {
                        lblPrimaryEmploymentMajorServiceText.Text = "n/a";
                    }

                    if (!string.IsNullOrEmpty(first.ClientAgeRange))
                    {
                        string updValue = first.ClientAgeRange;
                        //updValue = updValue.Substring(updValue.IndexOf(" ") + 1);
                        lblPrimaryEmploymentClientAgeRangeText.Text = updValue;
                    }
                    else
                    {
                        lblPrimaryEmploymentClientAgeRangeText.Text = "n/a";
                    }

                    if (!string.IsNullOrEmpty(first.Phone))
                    {
                        lblPrimaryEmployerTelephoneText.Text = first.Phone;
                    }

                }

                if (second != null && (!string.IsNullOrEmpty(second.EmployerName)) && userInfo.CurrentStatus != "R" && userInfo.CurrentStatus != "S")
                {
                    pnlSecondaryEmployment.Visible = true;
                    lblSecondaryEmployerNameText.Text = second.EmployerName;
                    if (second.Address != null && (!string.IsNullOrEmpty(second.Address.Address1)))
                    {
                        lblSecondaryEmployerAddressText.Text = second.Address.Address1;
                        trEmployment2Address1.Visible = true;
                    }
                    else
                    {
                        trEmployment2Address1.Visible = false;
                    }

                    if (second.Address != null && (!string.IsNullOrEmpty(second.Address.Address2)))
                    {
                        lblSecondaryEmployerAddress2Text.Text = second.Address.Address2;
                        trEmployment2Address2.Visible = true;
                    }
                    else
                    {
                        trEmployment2Address2.Visible = false;
                    }

                    trEmployment2Address3.Visible = false;

                    if (second.Address != null && (!string.IsNullOrEmpty(second.Address.City)))
                    {
                        lblSecondaryEmployerCityProvincePostalText.Text = second.Address.City;
                        trEmployment2Address3.Visible = true;
                    }

                    if (second.Address != null && (!string.IsNullOrEmpty(second.Address.Province)))
                    {
                        lblSecondaryEmployerCityProvincePostalText.Text += " " + second.Address.Province;
                        trEmployment2Address3.Visible = true;
                    }


                    if (second.Address != null && (!string.IsNullOrEmpty(second.Address.PostalCode)))
                    {
                        lblSecondaryEmployerCityProvincePostalText.Text += " " + second.Address.PostalCode;
                        trEmployment2Address3.Visible = true;
                    }


                    if (!string.IsNullOrEmpty(second.FundingSource))
                    {
                        string updValue = second.FundingSource;
                        //updValue = updValue.Substring(updValue.IndexOf(" ") + 1);
                        lblSecondaryEmployerFundingText.Text = updValue;
                    }
                    else
                    {
                        lblSecondaryEmployerFundingText.Text = "n/a";
                    }

                    if (!string.IsNullOrEmpty(second.PracticeSetting))
                    {
                        string updValue = second.PracticeSetting;
                        //updValue = updValue.Substring(updValue.IndexOf(" ") + 1);
                        lblSecondaryEmployerPracticeSettingText.Text = updValue;
                    }
                    else
                    {
                        lblSecondaryEmployerPracticeSettingText.Text = "n/a";
                    }

                    if (!string.IsNullOrEmpty(second.MajorServices))
                    {
                        string updValue = second.MajorServices;
                        //updValue = updValue.Substring(updValue.IndexOf(" ") + 1);
                        lblSecondaryEmployerMajorServiceText.Text = updValue;
                    }
                    else
                    {
                        lblSecondaryEmployerMajorServiceText.Text = "n/a";
                    }

                    if (!string.IsNullOrEmpty(second.ClientAgeRange))
                    {
                        string updValue = second.ClientAgeRange;
                        //updValue = updValue.Substring(updValue.IndexOf(" ") + 1);
                        lblSecondaryEmployerClientAgeRangeText.Text = updValue;
                    }
                    else
                    {
                        lblSecondaryEmployerClientAgeRangeText.Text = "n/a";
                    }

                    if (!string.IsNullOrEmpty(second.Phone))
                    {
                        lblSecondaryEmployerPhoneText.Text = second.Phone;
                    }
                }

                if (third != null && (!string.IsNullOrEmpty(third.EmployerName)) && userInfo.CurrentStatus != "R" && userInfo.CurrentStatus != "S")
                {
                    pnlTertiaryEmployment.Visible = true;
                    lblTertiaryEmployerNameText.Text = third.EmployerName;
                    if (third.Address != null && (!string.IsNullOrEmpty(third.Address.Address1)))
                    {
                        lblTertiaryEmployerAddressText.Text = third.Address.Address1;
                        trEmployment3Address1.Visible = true;
                    }
                    else
                    {
                        trEmployment3Address1.Visible = false;
                    }

                    if (third.Address != null && (!string.IsNullOrEmpty(third.Address.Address2)))
                    {
                        lblTertiaryEmployerAddress2Text.Text = third.Address.Address2;
                        trEmployment3Address2.Visible = true;
                    }
                    else
                    {
                        trEmployment3Address2.Visible = false;
                    }

                    trEmployment3Address3.Visible = false;

                    if (third.Address != null && (!string.IsNullOrEmpty(third.Address.City)))
                    {
                        lblTertiaryEmployerCityProvincePostalText.Text = third.Address.City;
                        trEmployment3Address3.Visible = true;
                    }

                    if (third.Address != null && (!string.IsNullOrEmpty(third.Address.Province)))
                    {
                        lblTertiaryEmployerCityProvincePostalText.Text += " " + third.Address.Province;
                        trEmployment3Address3.Visible = true;
                    }


                    if (third.Address != null && (!string.IsNullOrEmpty(third.Address.PostalCode)))
                    {
                        lblTertiaryEmployerCityProvincePostalText.Text += " " + third.Address.PostalCode;
                        trEmployment3Address3.Visible = true;
                    }


                    if (!string.IsNullOrEmpty(third.FundingSource))
                    {
                        string updValue = third.FundingSource;
                        //updValue = updValue.Substring(updValue.IndexOf(" ") + 1);
                        lblTertiaryEmployerFundingSourceText.Text = updValue;
                    }
                    else
                    {
                        lblTertiaryEmployerFundingSourceText.Text = "n/a";
                    }

                    if (!string.IsNullOrEmpty(third.PracticeSetting))
                    {
                        string updValue = third.PracticeSetting;
                        //updValue = updValue.Substring(updValue.IndexOf(" ") + 1);
                        lblTertiaryEmployerPracticeSettingText.Text = updValue;
                    }
                    else
                    {
                        lblTertiaryEmployerPracticeSettingText.Text = "n/a";
                    }

                    if (!string.IsNullOrEmpty(third.MajorServices))
                    {
                        string updValue = third.MajorServices;
                        //updValue = updValue.Substring(updValue.IndexOf(" ") + 1);
                        lblTertiaryEmployerMajorServiceText.Text = updValue;
                    }
                    else
                    {
                        lblTertiaryEmployerMajorServiceText.Text = "n/a";
                    }

                    if (!string.IsNullOrEmpty(third.ClientAgeRange))
                    {
                        string updValue = third.ClientAgeRange;
                        //updValue = updValue.Substring(updValue.IndexOf(" ") + 1);
                        lblTertiaryEmployerClientAgeRangeText.Text = updValue;
                    }
                    else
                    {
                        lblTertiaryEmployerClientAgeRangeText.Text = "n/a";
                    }

                    if (!string.IsNullOrEmpty(third.Phone))
                    {
                        lblTertiaryEmployerPhoneText.Text = third.Phone;
                    }
                }

            }
            else
            {
                trPracticeInformation.Visible = false;
                trPracticeInformation1.Visible = false;
            }

            if (!string.IsNullOrEmpty(userInfo.CoId))
            {
                trCorporationEmptyRow.Visible = false;
                trCorporationNoEmptyRow.Visible = true;
                var corp = repository.GetUserCorporationInfo(userInfo.CoId);
                if (corp != null)
                {
                    lblCorpNameText.Text = corp.FullName;
                    lblCorpCertificateText.Text = corp.Certificate;
                    lblCorpStatusText.Text = corp.Status;
                    if (corp.StatusEffectiveDate!=null)
                    {
                        lblCorpStatusEffectiveDateText.Text = ((DateTime)corp.StatusEffectiveDate).ToString("MMMMM dd, yyyy");
                    }
                    lblCorpPracticeNameText.Text = corp.PracticeName;
                    if (corp.BusinessAddress!=null)
                    {
                        if (!string.IsNullOrEmpty(corp.BusinessAddress.Address1))
                        {
                            lblCorpBusinessAddressText.Text = corp.BusinessAddress.Address1 + "<br />";
                        }
                        if (!string.IsNullOrEmpty(corp.BusinessAddress.Address2))
                        {
                            lblCorpBusinessAddressText.Text += corp.BusinessAddress.Address2 + "<br />";
                        }
                        if (!string.IsNullOrEmpty(corp.BusinessAddress.City))
                        {
                            lblCorpBusinessAddressText.Text += corp.BusinessAddress.City;
                        }
                        if (!string.IsNullOrEmpty(corp.BusinessAddress.Province))
                        {
                            lblCorpBusinessAddressText.Text += ", " + corp.BusinessAddress.Province;
                        }
                        lblCorpBusinessAddressText.Text += "<br />";
                        if (!string.IsNullOrEmpty(corp.BusinessAddress.PostalCode))
                        {
                            lblCorpBusinessAddressText.Text += corp.BusinessAddress.PostalCode;
                        }
                    }
                    lblCorpPhoneText.Text = corp.Phone;
                    lblCorpEmailText.Text = corp.Email;
                    if (corp.Shareholders != null && corp.Shareholders.Count > 0)
                    {
                        foreach (var item in corp.Shareholders)
                        {
                            lblCorpShareholdersText.Text += string.Format("{0} {1} {2} <br />", item.FirstName, item.LastName, item.Registration);
                        }
                    }
                    else
                    {
                        lblCorpShareholdersText.Text = "None";
                    }
                }
                else
                {
                    trCorporationEmptyRow.Visible = true;
                    trCorporationNoEmptyRow.Visible = false;
                }
            }
            else
            {
                trCorporationEmptyRow.Visible = true;
                trCorporationNoEmptyRow.Visible = false;
            }


        }
    }
}