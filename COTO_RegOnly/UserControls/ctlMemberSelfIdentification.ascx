﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberSelfIdentification.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlMemberSelfIdentification" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr class="HeaderTitle"  style="text-align: right;">
                <td>
                    <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 3 of 12" />
                </td>
            </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                        <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc:MessageBox ID="omb" runat="server" />
                    </td>
                </tr>
                <tr>
                <td>
                    <table class="memberInfo" width="100%" cellspacing="2" cellpadding="3">
                        <tr class="RowTitle">
                            <td colspan="2">
                                <div>
                                    <asp:Label ID="lblSelfIdentificationTitle" CssClass="heading" runat="server" Text="Self Identification of Indigenous Registrants" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="RightColumn">
                                <p>In 2015, the Truth and Reconciliation Commission (TRC) released its final report, which included 94 Calls to Action. 
                                   These Calls to Action offer direction on where systemic change is needed to further the reconciliation between Canadians and Indigenous Peoples. 
                                </p>
                                <p>
                                    Of relevance to the College of Occupational Therapists of Ontario (COTO) is the section on Health including Calls 
                                    to Action 18-24. These address the health disparities faced by Indigenous Peoples and acknowledge how poor health outcomes 
                                    are linked to the history of colonization in Canada. One recommendation for how to begin addressing these gaps include 
                                    increasing the number of Indigenous professionals working in healthcare. To contribute to this outcome, the College 
                                    would like to help increase the number of Indigenous occupational therapists in Ontario, starting by knowing how many 
                                    Indigenous occupational therapists are currently practising. This information will help us to monitor progress over time.
                                </p>
                                <p>
                                    Self-identification of this information is voluntary and confidential, and the data collected will be used in aggregate. 
                                    If clarification is needed regarding the information gathered, registrants may be contacted.  
                                </p>
                                <p>
                                    Please answer the following:
                                </p>

                                <ol>
                                    <li>
                                        <p>
                                            1.  Are you Indigenous? For the purposes of this answer, Indigenous refers to belonging to an Indigenous Nation that falls 
                                            under the three recognized groups of Aboriginal Peoples in Canada: First Nations (status, non-status), Métis or Inuit.
                                        </p>
                                    
                                        <asp:DropDownList ID="ddlQuestion1" runat="server" OnSelectedIndexChanged="ddlQuestion1_SelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion1" runat="server" ControlToValidate="ddlQuestion1"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select an option."
                                        Display="Dynamic" ForeColor="Red" />
                                    </li>
                                    <li id="liQuestion2" runat="server" visible="false">
                                        <p>
                                            2.  If you are Indigenous and answered "Yes" to Question 1, would you be interested in being contacted 
                                            by the College about future reconciliation initiatives? 
                                        </p>
                                        <asp:DropDownList ID="ddlQuestion2" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion2" runat="server" ControlToValidate="ddlQuestion2"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select an option."
                                        Display="Dynamic" ForeColor="Red" />
                                    </li>
                                </ol>

                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                <td style="text-align: right;">
                    <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                </td>
            </tr>
        </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </center>
</div>
