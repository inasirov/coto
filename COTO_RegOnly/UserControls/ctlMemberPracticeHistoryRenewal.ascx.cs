﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Globalization;
using Classes;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberPracticeHistoryRenewal : System.Web.UI.UserControl
    {

        #region Consts

        //private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;

        private string PrevStep = WebConfigItems.Step7;
        private string NextStep = WebConfigItems.Step9;
        private const int CurrentStep = 8;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.RenewalStep = CurrentStep;
                BindLists();
                BindData();
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            if ((ddlOutsideOntarioPractice.SelectedValue == "Yes" && ValidationCheck()) || ddlOutsideOntarioPractice.SelectedValue == "No")
            {
                UpdateUserPracticeHistory();
                UpdateSteps(1);
                Response.Redirect(NextStep);
            }
            else
            {
                if (Messages != null && Messages.Count > 0)
                {
                    string message = string.Empty;
                    foreach (var item in Messages)
                    {
                        message += ((PageMessage)item).Message + "<br />";
                    }
                    message = string.Format("<font color='Red'>{0}</font>", message);

                    Messages.Clear();
                    ShowMessage(message);
                }
            }
        }

        protected void ddlOutsideOntarioPracticeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOutsideOntarioPractice.SelectedValue == "Yes")
            {
                trOutsideON1.Visible = true;
                trOutsideON2.Visible = true;
                trOutsideON3.Visible = true;
                trOutsideON4.Visible = true;
                trOutsideON5.Visible = true;
                trOutsideON6.Visible = true;
                trOutsideON7.Visible = true;
                trOutsideON8.Visible = true;
            }
            else 
            {
                trOutsideON1.Visible = false;
                trOutsideON2.Visible = false;
                trOutsideON3.Visible = false;
                trOutsideON4.Visible = false;
                trOutsideON5.Visible = false;
                trOutsideON6.Visible = false;
                trOutsideON7.Visible = false;
                trOutsideON8.Visible = false;
            }
        }

        protected void lbtnCorrectDetailsClick(object sender, EventArgs e)
        {
            Response.Redirect(WebConfigItems.Step8_UpdatePracticeHistoryProfile);

        }

        #endregion

        #region Methods

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetUserPracticeHistoryInfo(CurrentUserId);
            if (user != null)
            {
                if (user.PracticeHistory != null)
                {
                    if (!string.IsNullOrEmpty(user.PracticeHistory.FirstCountryPractice))
                    {
                        ddlFirstCountryOTPractice.SelectedValue = user.PracticeHistory.FirstCountryPractice;
                    }

                    if (!string.IsNullOrEmpty(user.PracticeHistory.FirstProvinceStatePractice))
                    {
                        ddlFirstStateOTPractice.SelectedValue = user.PracticeHistory.FirstProvinceStatePractice;
                    }

                    if (!string.IsNullOrEmpty(user.PracticeHistory.FirstProvincePractice))
                    {
                        ddlFirstProvinceOTPractice.SelectedValue = user.PracticeHistory.FirstProvincePractice;
                    }

                    if (!string.IsNullOrEmpty(user.PracticeHistory.FirstYearPractice))
                    {
                        ddlFirstYearOTPractice.SelectedValue = user.PracticeHistory.FirstYearPractice;
                    }

                    if (!string.IsNullOrEmpty(user.PracticeHistory.FirstYearCanadianPractice))
                    {
                        ddlFirstYearCanadianOTPractice.SelectedValue = user.PracticeHistory.FirstYearCanadianPractice;
                    }

                    if (!string.IsNullOrEmpty(user.PracticeHistory.LastYearOutsideOntarioPractice))
                    {
                        ddlLastYearOutsideOntario.SelectedValue = user.PracticeHistory.LastYearOutsideOntarioPractice;
                        ddlOutsideOntarioPractice.SelectedIndex = 1;
                        trOutsideON1.Visible = true;
                        trOutsideON2.Visible = true;
                        trOutsideON3.Visible = true;
                        trOutsideON4.Visible = true;
                        trOutsideON5.Visible = true;
                        trOutsideON6.Visible = true;
                        trOutsideON7.Visible = true;
                        trOutsideON8.Visible = true;
                    }

                    if (!string.IsNullOrEmpty(user.PracticeHistory.LastCountryPractice))
                    {
                        ddlLastCountryOTPractice.SelectedValue = user.PracticeHistory.LastCountryPractice;
                    }

                    //if (!string.IsNullOrEmpty(user.PracticeHistory.LastProvincePractice))
                    //{
                    //    ddlLastCanadianProvince.SelectedValue = user.PracticeHistory.LastProvincePractice;
                    //}

                    if (!string.IsNullOrEmpty(user.PracticeHistory.LastProvinceStatePractice))
                    {
                        ddlLastStateOrCanadianProvince.SelectedValue = user.PracticeHistory.LastProvinceStatePractice;
                    }
                }
            }
            var user2 = repository.GetUserInfo(CurrentUserId);
            if (user2 != null)
            {
                CurrentUser = user2;
                lblPracticeHistoryTitle.Text = "OT Practice History for " + CurrentUser.FullName;
            }
        }

        protected void BindLists()
        {
            //var list2 = Countries.ListOfCountries;
            var repository = new Repository();
            var list1 = repository.GetGeneralList("COUNTRY");

            ddlFirstCountryOTPractice.DataSource = list1;
            ddlFirstCountryOTPractice.DataValueField = "Code";
            ddlFirstCountryOTPractice.DataTextField = "Description";
            ddlFirstCountryOTPractice.DataBind();
            ddlFirstCountryOTPractice.Items.Insert(0, string.Empty);

            ddlLastCountryOTPractice.DataSource = list1;
            ddlLastCountryOTPractice.DataValueField = "Code";
            ddlLastCountryOTPractice.DataTextField = "Description";
            ddlLastCountryOTPractice.DataBind();
            ddlLastCountryOTPractice.Items.Insert(0, string.Empty);

            var list2 = repository.GetGeneralList("PROVINCE_STATE");

            ddlFirstStateOTPractice.DataSource = list2;
            ddlFirstStateOTPractice.DataValueField = "Code";
            ddlFirstStateOTPractice.DataTextField = "Description";
            ddlFirstStateOTPractice.DataBind();
            ddlFirstStateOTPractice.Items.Insert(0, string.Empty);

            ddlLastStateOrCanadianProvince.DataSource = list2;
            ddlLastStateOrCanadianProvince.DataValueField = "Code";
            ddlLastStateOrCanadianProvince.DataTextField = "Description";
            ddlLastStateOrCanadianProvince.DataBind();
            ddlLastStateOrCanadianProvince.Items.Insert(0, string.Empty);

            var list3 = repository.GetGeneralList("FROM_PROVINCE");

            ddlFirstProvinceOTPractice.DataSource = list3;
            ddlFirstProvinceOTPractice.DataValueField = "Code";
            ddlFirstProvinceOTPractice.DataTextField = "Description";
            ddlFirstProvinceOTPractice.DataBind();
            ddlFirstProvinceOTPractice.Items.Insert(0, string.Empty);

            //ddlLastCanadianProvince.DataSource = list3;
            //ddlLastCanadianProvince.DataValueField = "Code";
            //ddlLastCanadianProvince.DataTextField = "Description";
            //ddlLastCanadianProvince.DataBind();
            //ddlLastCanadianProvince.Items.Insert(0, string.Empty);



            var list4 = repository.GetGeneralList("grad_year");

            ddlFirstYearOTPractice.DataSource = list4;
            ddlFirstYearOTPractice.DataValueField = "Code";
            ddlFirstYearOTPractice.DataTextField = "Code";
            ddlFirstYearOTPractice.DataBind();
            ddlFirstYearOTPractice.Items.Insert(0, string.Empty);

            ddlFirstYearCanadianOTPractice.DataSource = list4;
            ddlFirstYearCanadianOTPractice.DataValueField = "Code";
            ddlFirstYearCanadianOTPractice.DataTextField = "Code";
            ddlFirstYearCanadianOTPractice.DataBind();
            ddlFirstYearCanadianOTPractice.Items.Insert(0, string.Empty);

            // filter out not recent years
            try
            {
                list4 = list4.Where(I => I.Code != "1_NA").ToList();
                list4 = list4.Where(I => int.Parse(I.Code) <= DateTime.Now.Year).ToList();
            }
            catch   { }

            ddlLastYearOutsideOntario.DataSource = list4;
            ddlLastYearOutsideOntario.DataValueField = "Code";
            ddlLastYearOutsideOntario.DataTextField = "Code";
            ddlLastYearOutsideOntario.DataBind();
            ddlLastYearOutsideOntario.Items.Insert(0, string.Empty);


            var list5 = new List<GenClass>();
            //list5.Add(new GenClass { Code = "", Description = "" });
            list5.Add(new GenClass { Code = "No", Description = "No" });
            list5.Add(new GenClass { Code = "Yes", Description = "Yes" });

            ddlOutsideOntarioPractice.DataSource = list5;
            ddlOutsideOntarioPractice.DataValueField = "Code";
            ddlOutsideOntarioPractice.DataTextField = "Description";
            ddlOutsideOntarioPractice.DataBind();

        }

        protected void UpdateUserPracticeHistory()
        {
            User user = new User();
            user.Id = CurrentUserId;

            user.PracticeHistory = new PracticeHistory();
            user.PracticeHistory.FirstCountryPractice = ddlFirstCountryOTPractice.SelectedValue;
            user.PracticeHistory.FirstProvinceStatePractice = ddlFirstStateOTPractice.SelectedValue;
            user.PracticeHistory.FirstProvincePractice = ddlFirstProvinceOTPractice.SelectedValue;
            user.PracticeHistory.FirstYearPractice = ddlFirstYearOTPractice.SelectedValue;
            user.PracticeHistory.FirstYearCanadianPractice = ddlFirstYearCanadianOTPractice.SelectedValue;

            if (ddlOutsideOntarioPractice.SelectedIndex == 1)
            {
                user.PracticeHistory.LastCountryPractice = ddlLastCountryOTPractice.SelectedValue;
                //user.PracticeHistory.LastProvincePractice = ddlLastCanadianProvince.SelectedValue;
                user.PracticeHistory.LastProvinceStatePractice = ddlLastStateOrCanadianProvince.SelectedValue;
                user.PracticeHistory.LastYearOutsideOntarioPractice = ddlLastYearOutsideOntario.SelectedValue;
            }
            else
            {
                user.PracticeHistory.LastCountryPractice = string.Empty;
                //user.PracticeHistory.LastProvincePractice = ddlLastCanadianProvince.SelectedValue;
                user.PracticeHistory.LastProvinceStatePractice = string.Empty;
                user.PracticeHistory.LastYearOutsideOntarioPractice = string.Empty;
            }
            var repository = new Repository();
            repository.UpdateUserPracticeHistoryLogged( CurrentUserId, user);
        }

        protected bool ValidationCheck()
        {

            Page.Validate("GeneralValidation");

            if (!Page.IsValid)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        ///   Allow developer adds custom errors, warning into Messages Collection.
        /// </summary>
        /// <param name="message">Message information.</param>
        /// <param name="messageType">Message type.</param>
        public void AddMessage(string message, PageMessageType messageType)
        {
            this.Messages.Add(new PageMessage(message, messageType));
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        /// <summary>
        ///  Messages collection. Contains all custom warning or errors which will be populated on the page.
        /// </summary>
        public PageMessages Messages
        {
            get
            {
                return (PageMessages)SessionParameters.PageMessages;
            }
            set
            {
                SessionParameters.PageMessages = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                if (SessionParameters.CurrentUser != null)
                {
                    return SessionParameters.CurrentUser;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUser = value;
            }
        }

        #endregion

        

        
    }
}