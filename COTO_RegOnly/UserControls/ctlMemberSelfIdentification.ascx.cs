﻿using System;
using System.Web;
using System.Web.UI;
using System.Text;
using Classes;
using System.Linq;
using System.Security.Cryptography;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberSelfIdentification : System.Web.UI.UserControl
    {
        #region Consts
        private string _Key = "Yz7!~3";
        private string PrevStep = WebConfigItems.Step2;
        private string NextStep = WebConfigItems.Step4;
        private const int CurrentStep = 3;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) 
            {
                SessionParameters.RenewalStep = CurrentStep;
                BindLists();
                BindData();
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            UpdateUserInfo();
            UpdateSteps(1);
            Session["CustomAction"] = "renewal";
            Response.Redirect(NextStep); 
        }

        protected void ddlQuestion1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion1.SelectedValue.ToUpper().Contains("YES"))
            {
                liQuestion2.Visible = true;
            }
            else
            {
                liQuestion2.Visible = false;
            }
        }
        #endregion

        #region Methods

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        protected void BindLists()
        {
            var repository = new Repository();
            var list1 = repository.GetGeneralList("INDIGENOUS");

            var Yitem = list1.Find(i => i.Code == "YES");
            if (Yitem!= null)
            {
                list1.Remove(Yitem);
            }
            
            //list1.Insert(0, Yitem);
            ddlQuestion1.DataSource = list1;
            ddlQuestion1.DataValueField = "CODE";
            ddlQuestion1.DataTextField = "DESCRIPTION";
            ddlQuestion1.DataBind();
            ddlQuestion1.Items.Insert(0, string.Empty);

            var list2 = repository.GetGeneralList("INDIGENOUS_CONTACT");
            ddlQuestion2.DataSource = list2;
            ddlQuestion2.DataValueField = "CODE";
            ddlQuestion2.DataTextField = "DESCRIPTION";
            ddlQuestion2.DataBind();
            ddlQuestion2.Items.Insert(0, string.Empty);
        }

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetUserSelfIdentification(CurrentUserId);
            if (user != null)
            {
                if (!string.IsNullOrEmpty(user.SelfIdentificationQuestion1))
                {
                    var item = ddlQuestion1.Items.FindByValue(user.SelfIdentificationQuestion1);
                    if (item!= null)
                    {
                        ddlQuestion1.SelectedValue = item.Value;
                    }
                    if (user.SelfIdentificationQuestion1.ToUpper().Contains("YES"))
                    {
                        liQuestion2.Visible = true;
                    }
                }
                if (!string.IsNullOrEmpty(user.SelfIdentificationQuestion2))
                {
                    var item2 = ddlQuestion2.Items.FindByValue(user.SelfIdentificationQuestion2);
                    if (item2 != null)
                    {
                        ddlQuestion2.SelectedValue = item2.Value;
                    }
                }
            }

            var user2 = repository.GetUserInfo(CurrentUserId);
            if (user2 != null)
            {
                lblSelfIdentificationTitle.Text = "Self Identification of Indigenous Registrants for " + user2.FullName;
            }
        }

        protected void UpdateUserInfo()
        {
            User user = new User();
            user.Id = CurrentUserId;
            user.SelfIdentificationQuestion1 = ddlQuestion1.SelectedValue;
            user.SelfIdentificationQuestion2 = string.Empty;
            if (liQuestion2.Visible)
            {
                user.SelfIdentificationQuestion2 = ddlQuestion2.SelectedValue;
            }
            
            var repository = new Repository();
            repository.UpdateUserSelfIdentificationLogged(CurrentUserId, user);
            
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");
                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                if (SessionParameters.CurrentUser != null)
                {
                    return SessionParameters.CurrentUser;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUser = value;
            }
        }
        #endregion
       
    }
}