﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlRegistrationRenewalSummary : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }
        }

        protected void lbtnPrintReportClick(object sender, EventArgs e)
        {
            //Session["ReportID"] = CurrentUserId;
            Response.Redirect(WebConfigItems.Step12_PDF_Report);
        }

        protected void lbtnFeedbackLinkClick(object sender, EventArgs e)
        {
            Response.Redirect(WebConfigItems.Step12_Feedback);
        }

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }
    }
}