﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;
using Classes;

namespace UserControls
{
    public partial class ctlMemberEmployment : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;
        #endregion

        #region Events

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.ClientScript.IsStartupScriptRegistered(GetType(), "MaskedEditFix"))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "MaskedEditFix", String.Format("<script type='text/javascript' src='{0}'></script>", Page.ResolveUrl("~/Scripts/MaskedEditFix.js")));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            dmb.OkButtonPressed += dmbOkButtonPressed;
            dmb.CancelButtonPressed += dmbCancelButtonPressed;
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                BindLists();
                BindData();
            }
        }

        public void FixAjaxValidationSummaryErrorLabel_PreRender(object sender, EventArgs e)
        {
            string objId = ((Label)sender).ID;
            ScriptManager.RegisterStartupScript(this, this.GetType(), objId, ";", true);
        }

        protected void dmbOkButtonPressed(object sender, EventArgs args)
        {
            if (CurrentPrimaryEmploymentStatus != ddlStatus1.SelectedValue)
            {
                ClearEmpoyer1Fields();
                DisableEnableEmployer1Controls(false);
                CurrentPrimaryEmploymentStatus = ddlStatus1.SelectedValue;
                return;
            }
            if (CurrentSecondaryEmploymentStatus != ddlStatus2.SelectedValue)
            {
                ClearEmpoyer2Fields();
                DisableEnableEmployer2Controls(false);
                CurrentSecondaryEmploymentStatus = ddlStatus2.SelectedValue;
                return;
            }

            if (CurrentTertiaryEmploymentStatus != ddlStatus3.SelectedValue)
            {
                ClearEmpoyer3Fields();
                DisableEnableEmployer3Controls(false);
                CurrentTertiaryEmploymentStatus = ddlStatus3.SelectedValue;
                return;
            }
        }

        protected void dmbCancelButtonPressed(object sender, EventArgs args)
        {
            if (CurrentPrimaryEmploymentStatus != ddlStatus1.SelectedValue)
            {
                ddlStatus1.SelectedValue = CurrentPrimaryEmploymentStatus ;
                
                return;
            }
            if (CurrentSecondaryEmploymentStatus != ddlStatus2.SelectedValue)
            {
                 ddlStatus2.SelectedValue = CurrentSecondaryEmploymentStatus;
                return;
            }

            if (CurrentTertiaryEmploymentStatus != ddlStatus3.SelectedValue)
            {
                ddlStatus3.SelectedValue = CurrentTertiaryEmploymentStatus;
                return;
            }
        }

        protected void btnBackClick(object sender, EventArgs e)
        {
            Response.Redirect("MemberDisplay.aspx");
        }
        protected void btnUpdateClick(object sender, EventArgs e)
        {
            if (ValidationCheck())
            {
                UpdateUserEmployment();
                Response.Redirect("MemberDisplay.aspx");
            }
            else
            {
                if (Messages != null && Messages.Count > 0)
                {
                    string message = string.Empty;
                    foreach (var item in Messages)
                    {
                        message += ((PageMessage)item).Message + "<br />";
                    }
                    message = string.Format("<font color='Red'>{0}</font>", message);

                    Messages.Clear();
                    ShowErrorMessage(message);
                }

            }
        }

        protected void ibStatus1HelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("This represents your status for all OT employment.", "Status");
        }

        protected void ibPostalCode1HelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("Please provide a valid postal code without any spaces.", "Postal Code");
        }

        protected void ibPostalCodeReflectionHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("Answer 'Yes' to this question if your place of employment (employer/business office) is the location where you deliver service (e.g. hospital) and is located in Canada." + 
                "<br /><br />Answer 'No' to this question if you typically work at multiple sites within the community.", "Postal Code");
        }

        protected void ibPhone1HelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("Please include an extension number and represent it using a x. For example: (416) 214-1177 x224 ", "Phone");
        }

        protected void ibEndDate1HelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("If you have a known end-date of employment you may enter the date here and your file will be updated accordingly.", "Help - End Date");
        }

        protected void ibCasualStatusHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("<b>Full / Part Time Status</b><br /><br />" + 
            "1. Full time status is when the usual hours of work per week is equal to or greater than 30 hours.<br />" + 
            "2. Part-time status is when the usual hours per week is less than 30 hours per week. ", "Help - Full/Part Time Status");
        }

        protected void ibAverageWeeklyHours1HelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("If you work part time or on a casual basis, provide the <b>average</b> number of hours you work per week. " +
                "If you are on a leave of absence, provide the typical hours worked per week for this position. ", "Help - Average Weekly Hours");
        }

        protected void ibPracticeSetting1HelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("Only one selection can be made. Select the descriptor that best identifies the practice setting where you provide service. " + 
                "If your job is truly split 50/50 pick one descriptor and make additional profile selections according to this descriptor. ", "Help - Practice Setting");
        }

        protected void ibMajorServices1HelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("Only one selection can be made. Select the descriptor that best represents the major focus of activities " + 
            "in which you primarily provide services for each practice site. If your job is truly split 50/50 between " + 
            "two distinct areas of practice, pick one descriptor and make additional profile selections according to this descriptor. ", "Help - Major Services");
        }

        protected void PopCalendar1_SelectionChanged(object sender, EventArgs e)
        {

        }

        protected void ddlStatus1SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (string.IsNullOrEmpty(ddlStatus1.SelectedValue))
            {
                ShowDialog("All information for Primary Employment would be deleted. Are you sure?", "Confirmation");
                return;
            }
            if (ddlStatus1.SelectedValue == "10")
            {
                DisableEnableEmployer1Controls(false);
                
                // show warning message;
                lblStatus1Message.Text = "<br />If you select 'No Longer Working Here', your secondary employment will be moved to your primary employment position overnight, no other actions are required. If you have a new primary employment, please select 'Primary Employment' and enter your new employment information.";
            }
            if (ddlStatus1.SelectedValue == "20")
            {
                DisableEnableEmployer1Controls(true);
                // hide warning message
                lblStatus1Message.Text = string.Empty;
            }
           CurrentPrimaryEmploymentStatus = ddlStatus1.SelectedValue;
        }

        protected void ddlStatus2SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ddlStatus2.SelectedValue))
            {
                ShowDialog("All information for Secondary Employment would be deleted. Are you sure?", "Confirmation");
                return;
            }
            if (ddlStatus2.SelectedValue == "10")
            {
                DisableEnableEmployer2Controls(false);

                // show warning message;
                lblStatus2Message.Text = "<br />If you select 'No Longer Working Here', your tertiary employment will be moved to your secondary employment position overnight, no other actions are required. If you have a new secondary employment, please select 'Secondary Employment' and enter your new employment information.";
            }
            if (ddlStatus2.SelectedValue == "30")
            {
                DisableEnableEmployer2Controls(true);
                // hide warning message
                lblStatus2Message.Text = string.Empty;
            }
            CurrentSecondaryEmploymentStatus = ddlStatus2.SelectedValue;
        }

        protected void ddlStatus3SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ddlStatus3.SelectedValue))
            {
                ShowDialog("All information for Tertiary Employment would be deleted. Are you sure?", "Confirmation");
                return;
            }
            if (ddlStatus3.SelectedValue == "10")
            {
                DisableEnableEmployer3Controls(false);

                // show warning message;
                //lblStatus2Message.Text = "<br />If you select 'No Longer Working Here', your tertiary employment will be moved to your secondary employment position overnight, no other actions are required. If you have a new primary employment, please select 'Seondary Employment' and enter your new employment information.";
            }
            if (ddlStatus3.SelectedValue == "40")
            {
                DisableEnableEmployer3Controls(true);
                // hide warning message
                //lblStatus2Message.Text = string.Empty;
            }
            CurrentTertiaryEmploymentStatus = ddlStatus3.SelectedValue;
        }

        protected void ddlCurrentPracticeStatusSelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlCurrentPracticeStatus.SelectedIndex != 1 || ddlCurrentPracticeStatus.SelectedIndex != 2)
            //{
            //    ClearEmpoyer1Fields();
            //    ClearEmpoyer2Fields();
            //    ClearEmpoyer3Fields();
            //    ddlStatus1.SelectedIndex = 0;
            //    ddlStatus2.SelectedIndex = 0;
            //    ddlStatus3.SelectedIndex = 0;
            //    DisableEnableEmployer1Controls(false);
            //    DisableEnableEmployer2Controls(false);
            //    DisableEnableEmployer3Controls(false);
            //}
            if (!(ddlCurrentPracticeStatus.SelectedIndex == 1 || ddlCurrentPracticeStatus.SelectedIndex == 2))
            {
                ClearEmpoyer1Fields();
                ClearEmpoyer2Fields();
                ClearEmpoyer3Fields();
                ddlStatus1.SelectedIndex = 0; ddlStatus1.Enabled = false;
                ddlStatus2.SelectedIndex = 0; ddlStatus2.Enabled = false;
                ddlStatus3.SelectedIndex = 0; ddlStatus3.Enabled = false;
                DisableEnableEmployer1Controls(false);
                DisableEnableEmployer2Controls(false);
                DisableEnableEmployer3Controls(false);
                rblQuestion1Answers.SelectedIndex = -1; rblQuestion1Answers.Enabled = false;
                ddlFullTimePreference.Enabled = false; ddlFullTimePreference.SelectedIndex = 0; rfvFullTimePreference.Enabled = false;
                ddlNaturePractice.Enabled = false; ddlNaturePractice.SelectedIndex = 0; rfvNaturePractice.Enabled = false;
            }
            else
            {
                ddlStatus1.Enabled = true;
                ddlStatus2.Enabled = true;
                ddlStatus3.Enabled = true;

                //DisableEnableEmployer1Controls(true);
                //DisableEnableEmployer2Controls(true);
                //DisableEnableEmployer3Controls(true);

                rblQuestion1Answers.Enabled = true; rblQuestion1Answers.SelectedIndex = 1;
                ddlFullTimePreference.Enabled = true; rfvFullTimePreference.Enabled = true;
                ddlNaturePractice.Enabled = true; rfvNaturePractice.Enabled = true;
            }
        }

        protected void ddlMajorServices1SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlMajorServices1.SelectedIndex > 14 && ddlMajorServices1.SelectedIndex < 19)
            {
                trHealthCondition1.Visible = true;
            }
            else
            {
                trHealthCondition1.Visible = false;
            }
        }

        protected void ddlMajorServices2SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlMajorServices2.SelectedIndex > 14 && ddlMajorServices2.SelectedIndex < 19)
            {
                trHealthCondition2.Visible = true;
            }
            else
            {
                trHealthCondition2.Visible = false;
            }

        }

        protected void ddlMajorServices3SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlMajorServices3.SelectedIndex > 14 && ddlMajorServices3.SelectedIndex < 19)
            {
                trHealthCondition3.Visible = true;
            }
            else
            {
                trHealthCondition3.Visible = false;
            }

        }

        protected void ddlPostalCodeReflectPractice1SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedCountry = ddlCountry1.SelectedValue;
            //if (selectedCountry == "CAN")
            //{
            //    selectedCountry = "Canada";
            //}

            //if (selectedCountry == "United States")
            //{
            //    selectedCountry = "USA";
            //}

            if (ddlPostalCodeReflectPractice1.SelectedValue.ToLower() == "yes")
            {
                //var list = Countries.ListOfCountries;
                var repository = new Repository();
                var list = repository.GetGeneralList("COUNTRY");

                list = list.Where(L => L.Description.ToUpper().Contains("CANADA")).ToList();
                ddlCountry1.DataSource = list;
                ddlCountry1.DataValueField = "Code";
                ddlCountry1.DataTextField = "Description";
                ddlCountry1.DataBind();
                ddlCountry1.Items.Insert(0, new ListItem { Text = "", Value = "" });
                if (selectedCountry.ToUpper() == "CAN")
                {
                    ddlCountry1.SelectedValue = selectedCountry;
                }
                else
                {
                    ddlCountry1.SelectedIndex = 1;
                    ShowMessage(" You answered Yes to the question 'Does the postal code reflect site of practice?'. Please note that the country field must be Canada.", "Postal Code");
                }
            }
            else
            {
                //var list = Countries.ListOfCountries;
                var repository = new Repository();
                var list = repository.GetGeneralList("COUNTRY");

                ddlCountry1.DataSource = list;
                ddlCountry1.DataValueField = "Code";
                ddlCountry1.DataTextField = "Description";
                ddlCountry1.DataBind();
                ddlCountry1.Items.Insert(0, string.Empty);

                ddlCountry1.SelectedValue = selectedCountry;
            }
        }

        protected void ddlPostalCodeReflectPractice2SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedCountry = ddlCountry2.SelectedValue;
            //if (selectedCountry == "CAN")
            //{
            //    selectedCountry = "Canada";
            //}

            //if (selectedCountry == "United States")
            //{
            //    selectedCountry = "USA";
            //}

            if (ddlPostalCodeReflectPractice2.SelectedValue.ToLower() == "yes")
            {
                //var list = Countries.ListOfCountries;
                var repository = new Repository();
                var list = repository.GetGeneralList("COUNTRY");
                list = list.Where(L => L.Description.ToUpper().Contains("CANADA")).ToList();
                ddlCountry2.DataSource = list;
                ddlCountry2.DataValueField = "Code";
                ddlCountry2.DataTextField = "Description";
                ddlCountry2.DataBind();
                ddlCountry2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                if (selectedCountry.ToUpper() == "CAN")
                {
                    ddlCountry2.SelectedValue = selectedCountry;
                }
                else
                {
                    ddlCountry2.SelectedIndex = 1;
                    ShowMessage(" You answered Yes to the question 'Does the postal code reflect site of practice?'. Please note that the country field must be Canada.", "Postal Code");
                }
            }
            else
            {
                //var list = Countries.ListOfCountries;
                var repository = new Repository();
                var list = repository.GetGeneralList("COUNTRY");
                ddlCountry2.DataSource = list;
                ddlCountry2.DataValueField = "Code";
                ddlCountry2.DataTextField = "Description";
                ddlCountry2.DataBind();
                ddlCountry2.Items.Insert(0, string.Empty);
                ddlCountry2.SelectedValue = selectedCountry;
            }
        }

        protected void ddlPostalCodeReflectPractice3SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedCountry = ddlCountry3.SelectedValue;
            //if (selectedCountry == "CAN")
            //{
            //    selectedCountry = "Canada";
            //}

            //if (selectedCountry == "United States")
            //{
            //    selectedCountry = "USA";
            //}

            if (ddlPostalCodeReflectPractice3.SelectedValue.ToLower() == "yes")
            {
                //var list = Countries.ListOfCountries;
                var repository = new Repository();
                var list = repository.GetGeneralList("COUNTRY");
                list = list.Where(L => L.Description.ToUpper().Contains("CANADA")).ToList();
                ddlCountry3.DataSource = list;
                ddlCountry3.DataValueField = "Code";
                ddlCountry3.DataTextField = "Description";
                ddlCountry3.DataBind();
                ddlCountry3.Items.Insert(0, new ListItem { Value = "", Text = "" });
                if (selectedCountry.ToUpper() == "CAN")
                {
                    ddlCountry3.SelectedValue = selectedCountry;
                }
                else
                {
                    ddlCountry3.SelectedIndex = 1;
                    ShowMessage(" You answered Yes to the question 'Does the postal code reflect site of practice?'. Please note that the country field must be Canada.", "Postal Code");
                }
            }
            else
            {
                //var list = Countries.ListOfCountries;
                var repository = new Repository();
                var list = repository.GetGeneralList("COUNTRY");
                ddlCountry3.DataSource = list;
                ddlCountry3.DataValueField = "Code";
                ddlCountry3.DataTextField = "Description";
                ddlCountry3.DataBind();
                ddlCountry3.Items.Insert(0, string.Empty);
                ddlCountry3.SelectedValue = selectedCountry;
            }
        }
        #endregion

        #region Methods

        protected void DisableEnableEmployer1Controls(bool key)
        {
            txtEmployerName1.ReadOnly = !key;
            txtAddress1_1.ReadOnly = !key;
            txtAddress1_2.ReadOnly = !key;
            txtCity1.ReadOnly = !key;
            ddlProvince1.Enabled = key;
            txtPostalCode1.ReadOnly = !key;
            ddlPostalCodeReflectPractice1.Enabled = key;
            ddlCountry1.Enabled = key;
            txtTelephone1.ReadOnly = !key;
            txtFax1.ReadOnly = !key;
            txtStartDate1.ReadOnly = !key;
            PopCalendar1.Enabled = key;
            txtEndDate1.ReadOnly = !key;
            PopCalendar2.Enabled = key;
            ddlEmploymentRelationship1.Enabled = key;
            ddlCasualStatus1.Enabled = key;
            txtAverageWeeklyHours1.ReadOnly = !key;
            ddlPrimaryRole1.Enabled = key;
            ddlPracticeSetting1.Enabled = key;
            ddlMajorServices1.Enabled = key;
            ddlClientAgeRange1.Enabled = key;
            ddlFundingSource1.Enabled = key;
        }

        protected void DisableEnableEmployer2Controls(bool key)
        {
            txtEmployerName2.ReadOnly = !key;
            txtAddress2_1.ReadOnly = !key;
            txtAddress2_2.ReadOnly = !key;
            txtCity2.ReadOnly = !key;
            ddlProvince2.Enabled = key;
            txtPostalCode2.ReadOnly = !key;
            ddlPostalCodeReflectPractice2.Enabled = key;
            ddlCountry2.Enabled = key;
            txtTelephone2.ReadOnly = !key;
            txtFax2.ReadOnly = !key;
            txtStartDate2.ReadOnly = !key;
            txtEndDate2.ReadOnly = !key;
            PopCalendar3.Enabled = key;
            PopCalendar4.Enabled = key;
            ddlEmploymentRelationship2.Enabled = key;
            ddlCasualStatus2.Enabled = key;
            txtAverageWeeklyHours2.ReadOnly = !key;
            ddlPrimaryRole2.Enabled = key;
            ddlPracticeSetting2.Enabled = key;
            ddlMajorServices2.Enabled = key;
            ddlClientAgeRange2.Enabled = key;
            ddlFundingSource2.Enabled = key;
        }

        protected void DisableEnableEmployer3Controls(bool key)
        {
            txtEmployerName3.ReadOnly = !key;
            txtAddress3_1.ReadOnly = !key;
            txtAddress3_2.ReadOnly = !key;
            txtCity3.ReadOnly = !key;
            ddlProvince3.Enabled = key;
            txtPostalCode3.ReadOnly = !key;
            ddlPostalCodeReflectPractice3.Enabled = key;
            ddlCountry3.Enabled = key;
            txtTelephone3.ReadOnly = !key;
            txtFax3.ReadOnly = !key;
            txtStartDate3.ReadOnly = !key;
            txtEndDate3.ReadOnly = !key;
            PopCalendar5.Enabled = key;
            PopCalendar6.Enabled = key;
            ddlEmploymentRelationship3.Enabled = key;
            ddlCasualStatus3.Enabled = key;
            txtAverageWeeklyHours3.ReadOnly = !key;
            ddlPrimaryRole3.Enabled = key;
            ddlPracticeSetting3.Enabled = key;
            ddlMajorServices3.Enabled = key;
            ddlClientAgeRange3.Enabled = key;
            ddlFundingSource3.Enabled = key;
        }

        protected void ClearEmpoyer1Fields()
        {
            txtEmployerName1.Text = string.Empty;
            txtAddress1_1.Text = string.Empty;
            txtAddress1_2.Text = string.Empty;
            txtCity1.Text = string.Empty;
            ddlProvince1.SelectedIndex = 0;
            txtPostalCode1.Text = string.Empty;
            ddlPostalCodeReflectPractice1.SelectedIndex = 0;
            ddlCountry1.SelectedIndex = 0;
            txtTelephone1.Text = string.Empty;
            txtFax1.Text = string.Empty;
            txtStartDate1.Text = string.Empty;
            txtEndDate1.Text = string.Empty;
            ddlEmploymentRelationship1.SelectedIndex = 0;
            ddlCasualStatus1.SelectedIndex = 0;
            txtAverageWeeklyHours1.Text = "0";
            ddlPrimaryRole1.SelectedIndex = 0;
            ddlPracticeSetting1.SelectedIndex = 0;
            ddlMajorServices1.SelectedIndex = 0;
            ddlHealthCondition1.SelectedIndex = 0;
            ddlClientAgeRange1.SelectedIndex = 0;
            ddlFundingSource1.SelectedIndex = 0;
        }

        protected void ClearEmpoyer2Fields()
        {
            txtEmployerName2.Text = string.Empty;
            txtAddress2_1.Text = string.Empty;
            txtAddress2_2.Text = string.Empty;
            txtCity2.Text = string.Empty;
            ddlProvince2.SelectedIndex = 0;
            txtPostalCode2.Text = string.Empty;
            ddlPostalCodeReflectPractice2.SelectedIndex = 0;
            ddlCountry2.SelectedIndex = 0;
            txtTelephone2.Text = string.Empty;
            txtFax2.Text = string.Empty;
            txtStartDate2.Text = string.Empty;
            txtEndDate2.Text = string.Empty;
            ddlEmploymentRelationship2.SelectedIndex = 0;
            ddlCasualStatus2.SelectedIndex = 0;
            txtAverageWeeklyHours2.Text = "0";
            ddlPrimaryRole2.SelectedIndex = 0;
            ddlPracticeSetting2.SelectedIndex = 0;
            ddlMajorServices2.SelectedIndex = 0;
            ddlHealthCondition2.SelectedIndex = 0;
            ddlClientAgeRange2.SelectedIndex = 0;
            ddlFundingSource2.SelectedIndex = 0;
        }

        protected void ClearEmpoyer3Fields()
        {
            txtEmployerName3.Text = string.Empty;
            txtAddress3_1.Text = string.Empty;
            txtAddress3_2.Text = string.Empty;
            txtCity3.Text = string.Empty;
            ddlProvince3.SelectedIndex = 0;
            txtPostalCode3.Text = string.Empty;
            ddlPostalCodeReflectPractice3.SelectedIndex = 0;
            ddlCountry3.SelectedIndex = 0;
            txtTelephone3.Text = string.Empty;
            txtFax3.Text = string.Empty;
            txtStartDate3.Text = string.Empty;
            txtEndDate3.Text = string.Empty;
            ddlEmploymentRelationship3.SelectedIndex = 0;
            ddlCasualStatus3.SelectedIndex = 0;
            txtAverageWeeklyHours3.Text = "0";
            ddlPrimaryRole3.SelectedIndex = 0;
            ddlPracticeSetting3.SelectedIndex = 0;
            ddlMajorServices3.SelectedIndex = 0;
            ddlHealthCondition3.SelectedIndex = 0;
            ddlClientAgeRange3.SelectedIndex = 0;
            ddlFundingSource3.SelectedIndex = 0;
        }

        protected bool ValidationCheck()
        {
            bool validateEmp1 = (ddlStatus1.SelectedIndex > 1);
            bool validateEmp2 = (ddlStatus2.SelectedIndex > 1);
            bool validateEmp3 = (ddlStatus3.SelectedIndex > 1);

            Page.Validate("GeneralValidation");

            if (validateEmp1)
            {
                txtTelephone1.Text = txtTelephone1.Text.Replace("_", string.Empty);
                if (txtTelephone1.Text.Last() == 'x')
                {
                    txtTelephone1.Text = txtTelephone1.Text.Replace("x", string.Empty);
                }
                txtFax1.Text = txtFax1.Text.Replace("_", string.Empty);
                txtFax1.Text = txtFax1.Text.Replace("() -", string.Empty);
                Page.Validate("Employer1Validation");
            }
            if (validateEmp2)
            {
                txtTelephone2.Text = txtTelephone2.Text.Replace("_", string.Empty);
                if (txtTelephone2.Text.Last() == 'x')
                {
                    txtTelephone2.Text = txtTelephone2.Text.Replace("x", string.Empty);
                }
                txtFax2.Text = txtFax2.Text.Replace("_", string.Empty);
                txtFax2.Text = txtFax2.Text.Replace("() -", string.Empty);
                Page.Validate("Employer2Validation");
            }
            if (validateEmp3)
            {
                txtTelephone3.Text = txtTelephone3.Text.Replace("_", string.Empty);
                if (txtTelephone3.Text.Last() == 'x')
                {
                    txtTelephone3.Text = txtTelephone3.Text.Replace("x", string.Empty);
                }
                txtFax3.Text = txtFax3.Text.Replace("_", string.Empty);
                txtFax3.Text = txtFax3.Text.Replace("() -", string.Empty);

                Page.Validate("Employer3Validation");
            }
            
            if (!Page.IsValid)
            {
                lblErrorMessage.Text = string.Empty;
                update.Update();
                modalPopupEx.Show();
                return false;
            }

            bool valid = true;
            decimal  average1 = 0;
            decimal  average2 = 0;
            decimal  average3 = 0;

            if (!decimal.TryParse(txtAverageWeeklyHours1.Text, out average1))
            {
                AddMessage("Employer 1: Average weekly hours field contains non-numeric values", PageMessageType.UserError, string.Empty);
                valid = false;
            }

            if (!decimal.TryParse(txtAverageWeeklyHours2.Text, out average2))
            {
                AddMessage("Employer 2: Average weekly hours field contains non-numeric values", PageMessageType.UserError, string.Empty);
                valid = false;
            }

            if (!decimal.TryParse(txtAverageWeeklyHours3.Text, out average3))
            {
                AddMessage("Employer 3: Average weekly hours field contains non-numeric values", PageMessageType.UserError, string.Empty);
                valid = false;
            }

            if (ddlCurrentPracticeStatus.SelectedIndex == 1 || ddlCurrentPracticeStatus.SelectedIndex == 2)
            {
                if (ddlStatus1.SelectedIndex == 0)
                {
                    AddMessage("Please provide all information of your primary employment", PageMessageType.UserError, string.Empty);
                    valid = false;
                }
            }

            if (!valid)
            {
                return valid;
            }

            if (average1 < average2)
            {
                AddMessage("Average weekly hours - Primary must be greater than Secondary ", PageMessageType.UserError, string.Empty);
                valid = false;
            }

            if (average1 < average3)
            {
                AddMessage("Average weekly hours - Primary must be greater than Tertiary", PageMessageType.UserError, string.Empty);
                valid = false;
            }

            if (average2 < average3)
            {
                AddMessage("Average weekly hours - Secondary must be greater than Tertiary", PageMessageType.UserError, string.Empty);
                valid = false;
            }

            if (!valid)
            {
                return valid;
            }

            
            //DateTime startDate2 = DateTime.MinValue;
            //DateTime startDate3 = DateTime.MinValue;

            if (validateEmp1)
            {
                DateTime startDate1 = DateTime.MinValue;
                try
                {
                    if (!string.IsNullOrEmpty(txtStartDate1.Text))
                    {
                        var culture = new CultureInfo("en-CA");
                        startDate1 = Convert.ToDateTime(txtStartDate1.Text, culture);
                    }
                }
                catch
                {
                    AddMessage("Employer 1: Invalid Start Date entered.", PageMessageType.UserError, string.Empty);
                    valid = false;
                }
                if (!string.IsNullOrEmpty(txtEndDate1.Text))  // check for valid, and startdate should be no later than end date 
                {
                    DateTime endDate1 = DateTime.MinValue;
                    try
                    {
                        var culture = new CultureInfo("en-CA");
                        endDate1 = Convert.ToDateTime(txtEndDate1.Text, culture);
                        if (endDate1 < startDate1)
                        {
                            AddMessage("Employer 1: Start Date cannot be later than End Date.", PageMessageType.UserError, string.Empty);
                            valid = false;
                        }
                    }
                    catch
                    {
                        AddMessage("Employer 1: Invalid End Date entered.", PageMessageType.UserError, string.Empty);
                        valid = false;
                    }
                }

                if (trHealthCondition1.Visible && ddlHealthCondition1.SelectedIndex == 0)
                {
                    AddMessage("Please select a Health Condition for Employer 1.", PageMessageType.UserError, string.Empty);
                    valid = false;
                }
            }

            if (validateEmp2)
            {
                DateTime startDate2 = DateTime.MinValue;
                try
                {
                    if (!string.IsNullOrEmpty(txtStartDate2.Text))
                    {
                        var culture = new CultureInfo("en-CA");
                        startDate2 = Convert.ToDateTime(txtStartDate2.Text, culture);
                    }
                }
                catch
                {
                    AddMessage("Employer 2: Invalid Start Date entered.", PageMessageType.UserError, string.Empty);
                    valid = false;
                }
                if (!string.IsNullOrEmpty(txtEndDate2.Text))  // check for valid, and startdate should be no later than end date 
                {
                    DateTime endDate2 = DateTime.MinValue;
                    try
                    {
                        var culture = new CultureInfo("en-CA");
                        endDate2 = Convert.ToDateTime(txtEndDate2.Text, culture);
                        if (endDate2 < startDate2)
                        {
                            AddMessage("Employer 2: Start Date cannot be later than End Date.", PageMessageType.UserError, string.Empty);
                            valid = false;
                        }
                    }
                    catch
                    {
                        AddMessage("Employer 2: Invalid End Date entered.", PageMessageType.UserError, string.Empty);
                        valid = false;
                    }
                }

                if (trHealthCondition2.Visible && ddlHealthCondition2.SelectedIndex == 0)
                {
                    AddMessage("Please select a Health Condition for Employer 2.", PageMessageType.UserError, string.Empty);
                    valid = false;
                }
            }

            if (validateEmp3)
            {
                DateTime startDate3 = DateTime.MinValue;
                try
                {
                    if (!string.IsNullOrEmpty(txtStartDate3.Text))
                    {
                        var culture = new CultureInfo("en-CA");
                        startDate3 = Convert.ToDateTime(txtStartDate3.Text, culture);
                    }
                }
                catch
                {
                    AddMessage("Employer 3: Invalid Start Date entered.", PageMessageType.UserError, string.Empty);
                    valid = false;
                }
                if (!string.IsNullOrEmpty(txtEndDate3.Text))  // check for valid, and startdate should be no later than end date 
                {
                    DateTime endDate3 = DateTime.MinValue;
                    try
                    {
                        var culture = new CultureInfo("en-CA");
                        endDate3 = Convert.ToDateTime(txtEndDate3.Text, culture);
                        if (endDate3 < startDate3)
                        {
                            AddMessage("Employer 3: Start Date cannot be later than End Date.", PageMessageType.UserError, string.Empty);
                            valid = false;
                        }
                    }
                    catch
                    {
                        AddMessage("Employer 3: Invalid End Date entered.", PageMessageType.UserError, string.Empty);
                        valid = false;
                    }
                }
                if (trHealthCondition3.Visible && ddlHealthCondition3.SelectedIndex == 0)
                {
                    AddMessage("Please select a Health Condition for Employer 3.", PageMessageType.UserError, string.Empty);
                    valid = false;
                }
            }

            return valid;
        }

        /// <summary>
        ///   Allow developer adds custom errors, warning into Messages Collection.
        /// </summary>
        /// <param name="message">Message information.</param>
        /// <param name="messageType">Message type.</param>
        public void AddMessage(string message, PageMessageType messageType, string returnURL)
        {
            SessionParameters.ReturnUrl = returnURL;
            this.Messages.Add(new PageMessage(message, messageType));
        }


        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        protected void ShowDialog(string Message, string Caption)
        {
            dmb.ShowMessage(Message, Caption);
        }

        protected void ShowDialog(string Message)
        {
            dmb.ShowMessage(Message);
        }

        protected void ShowErrorMessage(string Message)
        {
            lblErrorMessage.Text = Message;
            update.Update();
            modalPopupEx.Show();
        }

        protected void UpdateUserEmployment()
        {
            User user = new User();
            user.Id = CurrentUserId;
            if (rblQuestion1Answers.SelectedIndex!= -1) 
            {
                user.MoreThreePractSites = (rblQuestion1Answers.SelectedValue == "1");
            }
            else user.MoreThreePractSites = null;
            user.CollegeMailings = ddlCollegeMailings.SelectedValue;

            user.CurrentEmploymentStatus = ddlCurrentPracticeStatus.SelectedValue;
            user.CurrentWorkPreference = ddlFullTimePreference.SelectedValue;
            user.NaturePractice = ddlNaturePractice.SelectedValue;

            user.EmploymentList = new List<Employment>();

            var employment1 = new Employment();
            employment1.Index = 1;

            employment1.Status = ddlStatus1.SelectedValue;
            employment1.EmployerName = txtEmployerName1.Text;
            employment1.Address = new Address();
            employment1.Address.Address1 = txtAddress1_1.Text;
            employment1.Address.Address2 = txtAddress1_2.Text;
            employment1.Address.City = txtCity1.Text;
            employment1.Address.Province = ddlProvince1.SelectedValue;
            employment1.Address.PostalCode = txtPostalCode1.Text;
            employment1.PostalCodeReflectPractice = ddlPostalCodeReflectPractice1.SelectedValue;
            employment1.Address.Country = ddlCountry1.SelectedValue;
            employment1.Phone = txtTelephone1.Text;
            employment1.Fax = txtFax1.Text;

            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;

            if (!string.IsNullOrEmpty(txtStartDate1.Text))
            {
                var culture = new CultureInfo("en-CA");
                startDate = Convert.ToDateTime(txtStartDate1.Text, culture);
                employment1.StartDate = startDate;
            }
            else
            {
                employment1.StartDate = null;
            }

            if (!string.IsNullOrEmpty(txtEndDate1.Text))
            {
                var culture = new CultureInfo("en-CA");
                endDate = Convert.ToDateTime(txtEndDate1.Text, culture);
                employment1.EndDate = endDate;
            }
            else
            {
                employment1.EndDate = null;
            }

            employment1.EmploymentRelationship = ddlEmploymentRelationship1.SelectedValue;
            employment1.CasualStatus = ddlCasualStatus1.SelectedValue;
            employment1.AverageWeeklyHours = double.Parse(txtAverageWeeklyHours1.Text);
            employment1.PrimaryRole = ddlPrimaryRole1.SelectedValue;
            employment1.PracticeSetting = ddlPracticeSetting1.SelectedValue;

            employment1.MajorServices = ddlMajorServices1.SelectedValue;
            employment1.HealthCondition = ddlHealthCondition1.SelectedValue;
            employment1.ClientAgeRange = ddlClientAgeRange1.SelectedValue;
            employment1.FundingSource = ddlFundingSource1.SelectedValue;

            user.EmploymentList.Add(employment1);

            var employment2 = new Employment();
            employment2.Index = 2;

            employment2.Status = ddlStatus2.SelectedValue;
            employment2.EmployerName = txtEmployerName2.Text;
            employment2.Address = new Address();
            employment2.Address.Address1 = txtAddress2_1.Text;
            employment2.Address.Address2 = txtAddress2_2.Text;
            employment2.Address.City = txtCity2.Text;
            employment2.Address.Province = ddlProvince2.SelectedValue;
            employment2.Address.PostalCode = txtPostalCode2.Text;
            employment2.PostalCodeReflectPractice = ddlPostalCodeReflectPractice2.SelectedValue;
            employment2.Address.Country = ddlCountry2.SelectedValue;
            employment2.Phone = txtTelephone2.Text;
            employment2.Fax = txtFax2.Text;

            startDate = DateTime.MinValue;
            endDate = DateTime.MinValue;

            if (!string.IsNullOrEmpty(txtStartDate2.Text))
            {
                var culture = new CultureInfo("en-CA");
                startDate = Convert.ToDateTime(txtStartDate2.Text, culture);
                employment2.StartDate = startDate;
            }
            else
            {
                employment2.StartDate = null;
            }

            if (!string.IsNullOrEmpty(txtEndDate2.Text))
            {
                var culture = new CultureInfo("en-CA");
                endDate = Convert.ToDateTime(txtEndDate2.Text, culture);
                employment2.EndDate = endDate;
            }
            else
            {
                employment2.EndDate = null;
            }

            employment2.EmploymentRelationship = ddlEmploymentRelationship2.SelectedValue;
            employment2.CasualStatus = ddlCasualStatus2.SelectedValue;
            employment2.AverageWeeklyHours = double.Parse(txtAverageWeeklyHours2.Text);
            employment2.PrimaryRole = ddlPrimaryRole2.SelectedValue;
            employment2.PracticeSetting = ddlPracticeSetting2.SelectedValue;

            employment2.MajorServices = ddlMajorServices2.SelectedValue;
            employment2.HealthCondition = ddlHealthCondition2.SelectedValue;
            employment2.ClientAgeRange = ddlClientAgeRange2.SelectedValue;
            employment2.FundingSource = ddlFundingSource2.SelectedValue;

            user.EmploymentList.Add(employment2);

            var employment3 = new Employment();
            employment3.Index = 3;

            employment3.Status = ddlStatus3.SelectedValue;
            employment3.EmployerName = txtEmployerName3.Text;
            employment3.Address = new Address();
            employment3.Address.Address1 = txtAddress3_1.Text;
            employment3.Address.Address2 = txtAddress3_2.Text;
            employment3.Address.City = txtCity3.Text;
            employment3.Address.Province = ddlProvince3.SelectedValue;
            employment3.Address.PostalCode = txtPostalCode3.Text;
            employment3.PostalCodeReflectPractice = ddlPostalCodeReflectPractice3.SelectedValue;
            employment3.Address.Country = ddlCountry3.SelectedValue;
            employment3.Phone = txtTelephone3.Text;
            employment3.Fax = txtFax3.Text;

            startDate = DateTime.MinValue;
            endDate = DateTime.MinValue;

            if (!string.IsNullOrEmpty(txtStartDate3.Text))
            {
                var culture = new CultureInfo("en-CA");
                startDate = Convert.ToDateTime(txtStartDate3.Text, culture);
                employment3.StartDate = startDate;
            }
            else
            {
                employment3.StartDate = null;
            }

            if (!string.IsNullOrEmpty(txtEndDate3.Text))
            {
                var culture = new CultureInfo("en-CA");
                endDate = Convert.ToDateTime(txtEndDate3.Text, culture);
                employment3.EndDate = endDate;
            }
            else
            {
                employment3.EndDate = null;
            }

            employment3.EmploymentRelationship = ddlEmploymentRelationship3.SelectedValue;
            employment3.CasualStatus = ddlCasualStatus3.SelectedValue;
            employment3.AverageWeeklyHours = double.Parse(txtAverageWeeklyHours3.Text);
            employment3.PrimaryRole = ddlPrimaryRole3.SelectedValue;
            employment3.PracticeSetting = ddlPracticeSetting3.SelectedValue;

            employment3.MajorServices = ddlMajorServices3.SelectedValue;
            employment3.HealthCondition = ddlHealthCondition3.SelectedValue;
            employment3.ClientAgeRange = ddlClientAgeRange3.SelectedValue;
            employment3.FundingSource = ddlFundingSource3.SelectedValue;

            user.EmploymentList.Add(employment3);

            var repository = new Repository();
            repository.UpdateUserEmploymentPersonalLogged(user);

            if (PreviousCollegeMailingValue != ddlCollegeMailings.SelectedValue)
            {
                bool preferred = (ddlCollegeMailings.SelectedValue == "Yes");
                repository.UpdateApplicationUserEmploymentAddressPreferred(CurrentUserId, preferred);
                PreviousCollegeMailingValue = ddlCollegeMailings.SelectedValue;
            }
        }

        protected void BindLists()
        {
            var repository = new Repository();

            var list0 = repository.GetGeneralList("EMPLOYMENT_STATUS"); // get data for status field
            ddlCurrentPracticeStatus.DataSource = list0;
            ddlCurrentPracticeStatus.DataValueField = "Code";
            ddlCurrentPracticeStatus.DataTextField = "Description";
            ddlCurrentPracticeStatus.DataBind();
            ddlCurrentPracticeStatus.Items.Insert(0, string.Empty);

            var list00 = repository.GetGeneralList("NATURE_PRACTICE"); // get data for nature of practice field
            list00 = list00.OrderBy(L => L.Substitute).ToList();
            ddlNaturePractice.DataSource = list00;
            ddlNaturePractice.DataValueField = "CODE";
            ddlNaturePractice.DataTextField = "DESCRIPTION";
            ddlNaturePractice.DataBind();
            ddlNaturePractice.Items.Insert(0, string.Empty);

            var list01 = repository.GetGeneralList("FULL_PT_STATUS"); // get data for nature of practice field

            ddlFullTimePreference.DataSource = list01;
            ddlFullTimePreference.DataValueField = "CODE";
            ddlFullTimePreference.DataTextField = "DESCRIPTION";
            ddlFullTimePreference.DataBind();
            ddlFullTimePreference.Items.Insert(0, string.Empty);

            var list1 = repository.GetGeneralList("EMPLOYMENT_SOURCE");
            var list1_1 = list1.Where(L => L.Code == "20" || L.Code=="10").ToList();
            ddlStatus1.DataSource = list1_1;
            ddlStatus1.DataValueField = "CODE";
            ddlStatus1.DataTextField = "DESCRIPTION";
            ddlStatus1.DataBind();
            ddlStatus1.Items.Insert(0, string.Empty);

            var list1_2 = list1.Where(L => L.Code == "30" || L.Code == "10").ToList();
            ddlStatus2.DataSource = list1_2;
            ddlStatus2.DataValueField = "CODE";
            ddlStatus2.DataTextField = "DESCRIPTION";
            ddlStatus2.DataBind();
            ddlStatus2.Items.Insert(0, string.Empty);

            var list1_3 = list1.Where(L => L.Code == "40" || L.Code == "10").ToList();
            ddlStatus3.DataSource = list1_3;
            ddlStatus3.DataValueField = "CODE";
            ddlStatus3.DataTextField = "DESCRIPTION";
            ddlStatus3.DataBind();
            ddlStatus3.Items.Insert(0, string.Empty);

            var list2 = repository.GetCanadianProvincesList();
            list2.Insert(0, new GenClass { Code = "", Description = "" });

            //var list2 = new List<GenClass>();
            //list2.Add(new GenClass { Code = "", Description = "" });
            //list2.Add(new GenClass { Code = "AB", Description = "Alberta" });
            //list2.Add(new GenClass { Code = "BC", Description = "British Columbia" });
            //list2.Add(new GenClass { Code = "MB", Description = "Manitoba" });
            //list2.Add(new GenClass { Code = "NB", Description = "New Brunswick" });
            //list2.Add(new GenClass { Code = "NL", Description = "Newfoundland" });
            //list2.Add(new GenClass { Code = "NS", Description = "Nova Scotia" });
            //list2.Add(new GenClass { Code = "NT", Description = "Northwest Territories" });
            //list2.Add(new GenClass { Code = "NV", Description = "Nunavut" });
            //list2.Add(new GenClass { Code = "ON", Description = "Ontario" });
            //list2.Add(new GenClass { Code = "OT", Description = "Other" });
            //list2.Add(new GenClass { Code = "PE", Description = "Prince Edward Island" });
            //list2.Add(new GenClass { Code = "QC", Description = "Quebec" });
            //list2.Add(new GenClass { Code = "SK", Description = "Saskatchewan" });
            //list2.Add(new GenClass { Code = "YK", Description = "Yukon" });

            ddlProvince1.DataSource = list2;
            ddlProvince1.DataValueField = "Code";
            ddlProvince1.DataTextField = "Description";
            ddlProvince1.DataBind();

            ddlProvince2.DataSource = list2;
            ddlProvince2.DataValueField = "Code";
            ddlProvince2.DataTextField = "Description";
            ddlProvince2.DataBind();

            ddlProvince3.DataSource = list2;
            ddlProvince3.DataValueField = "Code";
            ddlProvince3.DataTextField = "Description";
            ddlProvince3.DataBind();

            var list3 = new List<GenClass>();
            list3.Add(new GenClass { Code = "", Description = "" });
            list3.Add(new GenClass { Code = "No", Description = "No" });
            list3.Add(new GenClass { Code = "Yes", Description = "Yes" });

            ddlCollegeMailings.DataSource = list3;
            ddlCollegeMailings.DataValueField = "Code";
            ddlCollegeMailings.DataTextField = "Description";
            ddlCollegeMailings.DataBind();

            ddlPostalCodeReflectPractice1.DataSource = list3;
            ddlPostalCodeReflectPractice1.DataValueField = "Code";
            ddlPostalCodeReflectPractice1.DataTextField = "Description";
            ddlPostalCodeReflectPractice1.DataBind();

            ddlPostalCodeReflectPractice2.DataSource = list3;
            ddlPostalCodeReflectPractice2.DataValueField = "Code";
            ddlPostalCodeReflectPractice2.DataTextField = "Description";
            ddlPostalCodeReflectPractice2.DataBind();

            ddlPostalCodeReflectPractice3.DataSource = list3;
            ddlPostalCodeReflectPractice3.DataValueField = "Code";
            ddlPostalCodeReflectPractice3.DataTextField = "Description";
            ddlPostalCodeReflectPractice3.DataBind();


            //var list4 = Countries.ListOfCountries;
            var list4 = repository.GetGeneralList("COUNTRY");

            ddlCountry1.DataSource = list4;
            ddlCountry1.DataValueField = "Code";
            ddlCountry1.DataTextField = "Description";
            ddlCountry1.DataBind();
            ddlCountry1.Items.Insert(0, string.Empty);

            ddlCountry2.DataSource = list4;
            ddlCountry2.DataValueField = "Code";
            ddlCountry2.DataTextField = "Description";
            ddlCountry2.DataBind();
            ddlCountry2.Items.Insert(0, string.Empty);

            ddlCountry3.DataSource = list4;
            ddlCountry3.DataValueField = "Code";
            ddlCountry3.DataTextField = "Description";
            ddlCountry3.DataBind();
            ddlCountry3.Items.Insert(0, string.Empty);

            var list5 = repository.GetGeneralList("EMPLOY_CATEGORY");
            list5.Insert(0, new GenClass { Code = "", Description = "" });

            //var list5 = new List<GenClass>();
            //list5.Add(new GenClass { Code = "", Description = "" });
            //list5.Add(new GenClass { Code = "10 Permanent", Description = " 10 Permanent      " });
            //list5.Add(new GenClass { Code = "20 Temporary", Description = " 20 Temporary      " });
            //list5.Add(new GenClass { Code = "30 Casual", Description = " 30 Casual          " });
            //list5.Add(new GenClass { Code = "40 Self Employed", Description = " 40 Self Employed   " });

            ddlEmploymentRelationship1.DataSource = list5;
            ddlEmploymentRelationship1.DataValueField = "CODE";
            ddlEmploymentRelationship1.DataTextField = "DESCRIPTION";
            ddlEmploymentRelationship1.DataBind();

            ddlEmploymentRelationship2.DataSource = list5;
            ddlEmploymentRelationship2.DataValueField = "CODE";
            ddlEmploymentRelationship2.DataTextField = "DESCRIPTION";
            ddlEmploymentRelationship2.DataBind();

            ddlEmploymentRelationship3.DataSource = list5;
            ddlEmploymentRelationship3.DataValueField = "CODE";
            ddlEmploymentRelationship3.DataTextField = "DESCRIPTION";
            ddlEmploymentRelationship3.DataBind();

            var list6 = repository.GetGeneralList("FULL_PT_STATUS"); // get data for status field
            ddlCasualStatus1.DataSource = list6;
            ddlCasualStatus1.DataValueField = "CODE";
            ddlCasualStatus1.DataTextField = "DESCRIPTION";
            ddlCasualStatus1.DataBind();
            ddlCasualStatus1.Items.Insert(0, string.Empty);

            ddlCasualStatus2.DataSource = list6;
            ddlCasualStatus2.DataValueField = "CODE";
            ddlCasualStatus2.DataTextField = "DESCRIPTION";
            ddlCasualStatus2.DataBind();
            ddlCasualStatus2.Items.Insert(0, string.Empty);

            ddlCasualStatus3.DataSource = list6;
            ddlCasualStatus3.DataValueField = "CODE";
            ddlCasualStatus3.DataTextField = "DESCRIPTION";
            ddlCasualStatus3.DataBind();
            ddlCasualStatus3.Items.Insert(0, string.Empty);

            var list7 = repository.GetGeneralList("POSITION"); // get data for status field
            ddlPrimaryRole1.DataSource = list7;
            ddlPrimaryRole1.DataValueField = "CODE";
            ddlPrimaryRole1.DataTextField = "DESCRIPTION";
            ddlPrimaryRole1.DataBind();
            ddlPrimaryRole1.Items.Insert(0, string.Empty);

            ddlPrimaryRole2.DataSource = list7;
            ddlPrimaryRole2.DataValueField = "CODE";
            ddlPrimaryRole2.DataTextField = "DESCRIPTION";
            ddlPrimaryRole2.DataBind();
            ddlPrimaryRole2.Items.Insert(0, string.Empty);

            ddlPrimaryRole3.DataSource = list7;
            ddlPrimaryRole3.DataValueField = "CODE";
            ddlPrimaryRole3.DataTextField = "DESCRIPTION";
            ddlPrimaryRole3.DataBind();
            ddlPrimaryRole3.Items.Insert(0, string.Empty);

            var list8 = repository.GetGeneralList("EMPLOYER_TYPE"); // get data for status field
            ddlPracticeSetting1.DataSource = list8;
            ddlPracticeSetting1.DataValueField = "CODE";
            ddlPracticeSetting1.DataTextField = "DESCRIPTION";
            ddlPracticeSetting1.DataBind();
            ddlPracticeSetting1.Items.Insert(0, string.Empty);

            ddlPracticeSetting2.DataSource = list8;
            ddlPracticeSetting2.DataValueField = "CODE";
            ddlPracticeSetting2.DataTextField = "DESCRIPTION";
            ddlPracticeSetting2.DataBind();
            ddlPracticeSetting2.Items.Insert(0, string.Empty);

            ddlPracticeSetting3.DataSource = list8;
            ddlPracticeSetting3.DataValueField = "CODE";
            ddlPracticeSetting3.DataTextField = "DESCRIPTION";
            ddlPracticeSetting3.DataBind();
            ddlPracticeSetting3.Items.Insert(0, string.Empty);

            var list9 = repository.GetGeneralList("MAIN_AREA_PRACTICE"); // get data for status field
            ddlMajorServices1.DataSource = list9;
            ddlMajorServices1.DataValueField = "CODE";
            ddlMajorServices1.DataTextField = "DESCRIPTION";
            ddlMajorServices1.DataBind();
            ddlMajorServices1.Items.Insert(0, string.Empty);

            ddlMajorServices2.DataSource = list9;
            ddlMajorServices2.DataValueField = "CODE";
            ddlMajorServices2.DataTextField = "DESCRIPTION";
            ddlMajorServices2.DataBind();
            ddlMajorServices2.Items.Insert(0, string.Empty);

            ddlMajorServices3.DataSource = list9;
            ddlMajorServices3.DataValueField = "CODE";
            ddlMajorServices3.DataTextField = "DESCRIPTION";
            ddlMajorServices3.DataBind();
            ddlMajorServices3.Items.Insert(0, string.Empty);

            var list10 = repository.GetGeneralList("HEALTH_CONDITION"); // get data for status field
            ddlHealthCondition1.DataSource = list10;
            ddlHealthCondition1.DataValueField = "CODE";
            ddlHealthCondition1.DataTextField = "DESCRIPTION";
            ddlHealthCondition1.DataBind();
            ddlHealthCondition1.Items.Insert(0, string.Empty);

            ddlHealthCondition2.DataSource = list10;
            ddlHealthCondition2.DataValueField = "CODE";
            ddlHealthCondition2.DataTextField = "DESCRIPTION";
            ddlHealthCondition2.DataBind();
            ddlHealthCondition2.Items.Insert(0, string.Empty);

            ddlHealthCondition3.DataSource = list10;
            ddlHealthCondition3.DataValueField = "CODE";
            ddlHealthCondition3.DataTextField = "DESCRIPTION";
            ddlHealthCondition3.DataBind();
            ddlHealthCondition3.Items.Insert(0, string.Empty);

            var list11 = repository.GetGeneralList("CLIENT_AGE_RANGE"); // get data for status field
            ddlClientAgeRange1.DataSource = list11;
            ddlClientAgeRange1.DataValueField = "CODE";
            ddlClientAgeRange1.DataTextField = "DESCRIPTION";
            ddlClientAgeRange1.DataBind();
            ddlClientAgeRange1.Items.Insert(0, string.Empty);

            ddlClientAgeRange2.DataSource = list11;
            ddlClientAgeRange2.DataValueField = "CODE";
            ddlClientAgeRange2.DataTextField = "DESCRIPTION";
            ddlClientAgeRange2.DataBind();
            ddlClientAgeRange2.Items.Insert(0, string.Empty);

            ddlClientAgeRange3.DataSource = list11;
            ddlClientAgeRange3.DataValueField = "CODE";
            ddlClientAgeRange3.DataTextField = "DESCRIPTION";
            ddlClientAgeRange3.DataBind();
            ddlClientAgeRange3.Items.Insert(0, string.Empty);

            var list12 = repository.GetGeneralList("FUNDING"); // get data for status field
            ddlFundingSource1.DataSource = list12;
            ddlFundingSource1.DataValueField = "CODE";
            ddlFundingSource1.DataTextField = "DESCRIPTION";
            ddlFundingSource1.DataBind();
            ddlFundingSource1.Items.Insert(0, string.Empty);

            ddlFundingSource2.DataSource = list12;
            ddlFundingSource2.DataValueField = "CODE";
            ddlFundingSource2.DataTextField = "DESCRIPTION";
            ddlFundingSource2.DataBind();
            ddlFundingSource2.Items.Insert(0, string.Empty);

            ddlFundingSource3.DataSource = list12;
            ddlFundingSource3.DataValueField = "CODE";
            ddlFundingSource3.DataTextField = "DESCRIPTION";
            ddlFundingSource3.DataBind();
            ddlFundingSource3.Items.Insert(0, string.Empty);
        }

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetUserEmployment(CurrentUserId);
            if (user != null)
            {
                ddlCurrentPracticeStatus.SelectedValue = user.CurrentEmploymentStatus;
                ddlFullTimePreference.SelectedValue = user.CurrentWorkPreference;
                ddlNaturePractice.SelectedValue = user.NaturePractice;

                if (user.MoreThreePractSites != null)
                {
                    if (user.MoreThreePractSites == true)
                    {
                        rblQuestion1Answers.SelectedValue = "1";
                    }
                    else
                    {
                        rblQuestion1Answers.SelectedValue = "0";
                    }
                }
                else
                {
                    rblQuestion1Answers.SelectedIndex = -1;
                }

                if (!string.IsNullOrEmpty(user.CollegeMailings))
                {
                    ddlCollegeMailings.SelectedValue = user.CollegeMailings;
                    PreviousCollegeMailingValue = user.CollegeMailings;
                }

                var first = user.EmploymentList.First(E => E.Index == 1);
                if (user.EmploymentList != null && first != null)
                {
                    ddlStatus1.SelectedValue = first.Status;
                    CurrentPrimaryEmploymentStatus = ddlStatus1.SelectedValue;
                    txtEmployerName1.Text = first.EmployerName;
                    txtAddress1_1.Text = first.Address.Address1;
                    txtAddress1_2.Text = first.Address.Address2;
                    txtCity1.Text = first.Address.City;
                    ddlProvince1.SelectedValue = first.Address.Province;
                    txtPostalCode1.Text = first.Address.PostalCode.ToUpper();
                    ddlPostalCodeReflectPractice1.SelectedValue = first.PostalCodeReflectPractice;
                    
                    string selectedCountry = first.Address.Country;
                    //if (selectedCountry=="CAN")
                    //{
                    //    selectedCountry = "CAN";
                    //}

                    //if (selectedCountry == "United States")
                    //{
                    //    selectedCountry = "USA";
                    //}

                    if (first.PostalCodeReflectPractice.ToLower() == "yes")  // leave only Canada country in the list
                    {
                        //var list4 = Countries.ListOfCountries;
                        var list4 = repository.GetGeneralList("COUNTRY");
                        list4 = list4.Where(L => L.Code.Contains("CAN")).ToList();
                        ddlCountry1.DataSource = list4;
                        ddlCountry1.DataValueField = "Code";
                        ddlCountry1.DataTextField = "Description";
                        ddlCountry1.DataBind();
                        ddlCountry1.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        if (selectedCountry.ToUpper() == "CAN")
                        {
                            ddlCountry1.SelectedValue = selectedCountry;
                        }

                    }
                    else
                    {
                        ddlCountry1.SelectedValue = selectedCountry;
                    }

                    
                    txtTelephone1.Text = first.Phone;
                    txtFax1.Text = first.Fax;
                    if (first.StartDate != null)
                    {
                        txtStartDate1.Text = ((DateTime)first.StartDate).ToString("dd/MM/yyyy");
                    }

                    if (first.EndDate != null)
                    {
                        txtEndDate1.Text = ((DateTime)first.EndDate).ToString("dd/MM/yyyy");
                    }
                    ddlEmploymentRelationship1.SelectedValue = first.EmploymentRelationship;
                    ddlCasualStatus1.SelectedValue = first.CasualStatus;
                    txtAverageWeeklyHours1.Text = first.AverageWeeklyHours.ToString("N0");
                    ddlPrimaryRole1.SelectedValue = first.PrimaryRole;
                    ddlPracticeSetting1.SelectedValue = first.PracticeSetting;

                    ddlMajorServices1.SelectedValue = first.MajorServices;
                    ddlHealthCondition1.SelectedValue = first.HealthCondition;
                    ddlClientAgeRange1.SelectedValue = first.ClientAgeRange;
                    ddlFundingSource1.SelectedValue = first.FundingSource;
                    if (ddlStatus1.SelectedIndex <= 1)
                    {
                        DisableEnableEmployer1Controls(false);
                    }
                }

                var second = user.EmploymentList.First(E => E.Index == 2);
                if (user.EmploymentList != null && second != null)
                {
                    ddlStatus2.SelectedValue = second.Status;
                    CurrentSecondaryEmploymentStatus = ddlStatus2.SelectedValue;
                    txtEmployerName2.Text = second.EmployerName;
                    txtAddress2_1.Text = second.Address.Address1;
                    txtAddress2_2.Text = second.Address.Address2;
                    txtCity2.Text = second.Address.City;
                    ddlProvince2.SelectedValue = second.Address.Province;
                    txtPostalCode2.Text = second.Address.PostalCode.ToUpper();
                    
                    ddlPostalCodeReflectPractice2.SelectedValue = second.PostalCodeReflectPractice;
                    //ddlCountry2.SelectedValue = second.Address.Country;

                    string selectedCountry = second.Address.Country;
                    //if (selectedCountry == "CAN")
                    //{
                    //    selectedCountry = "Canada";
                    //}

                    //if (selectedCountry == "United States")
                    //{
                    //    selectedCountry = "USA";
                    //}

                    if (second.PostalCodeReflectPractice.ToLower() == "yes")  // leave only Canada country in the list
                    {
                        var list5 = repository.GetGeneralList("COUNTRY");
                        //var list5 = Countries.ListOfCountries;
                        list5 = list5.Where(L => L.Code == "CAN").ToList();
                        ddlCountry2.DataSource = list5;
                        ddlCountry2.DataValueField = "Code";
                        ddlCountry2.DataTextField = "Description";
                        ddlCountry2.DataBind();
                        ddlCountry2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        if (selectedCountry.ToUpper() == "CAN")
                        {
                            ddlCountry2.SelectedValue = selectedCountry;
                        }
                    }
                    else
                    {
                        ddlCountry2.SelectedValue = selectedCountry;
                    }
                    
                    txtTelephone2.Text = second.Phone;
                    txtFax2.Text = second.Fax;
                    if (second.StartDate != null)
                    {
                        txtStartDate2.Text = ((DateTime)second.StartDate).ToString("dd/MM/yyyy");
                    }

                    if (second.EndDate != null)
                    {
                        txtEndDate2.Text = ((DateTime)second.EndDate).ToString("dd/MM/yyyy");
                    }
                    ddlEmploymentRelationship2.SelectedValue = second.EmploymentRelationship;
                    ddlCasualStatus2.SelectedValue = second.CasualStatus;
                    txtAverageWeeklyHours2.Text = second.AverageWeeklyHours.ToString("N0");
                    ddlPrimaryRole2.SelectedValue = second.PrimaryRole;
                    ddlPracticeSetting2.SelectedValue = second.PracticeSetting;

                    ddlMajorServices2.SelectedValue = second.MajorServices;
                    ddlHealthCondition2.SelectedValue = second.HealthCondition;
                    ddlClientAgeRange2.SelectedValue = second.ClientAgeRange;
                    ddlFundingSource2.SelectedValue = second.FundingSource;
                    if (ddlStatus2.SelectedIndex <= 1)
                    {
                        DisableEnableEmployer2Controls(false);
                    }
                }

                var third = user.EmploymentList.First(E => E.Index == 3);
                if (user.EmploymentList != null && third != null)
                {
                    ddlStatus3.SelectedValue = third.Status;
                    CurrentTertiaryEmploymentStatus = ddlStatus3.SelectedValue;
                    txtEmployerName3.Text = third.EmployerName;
                    txtAddress3_1.Text = third.Address.Address1;
                    txtAddress3_2.Text = third.Address.Address2;
                    txtCity3.Text = third.Address.City;
                    ddlProvince3.SelectedValue = third.Address.Province;
                    txtPostalCode3.Text = third.Address.PostalCode.ToUpper();
                    ddlPostalCodeReflectPractice3.SelectedValue = third.PostalCodeReflectPractice;
                    //ddlCountry3.SelectedValue = third.Address.Country;

                    string selectedCountry = third.Address.Country;
                    //if (selectedCountry == "CAN")
                    //{
                    //    selectedCountry = "Canada";
                    //}

                    //if (selectedCountry == "United States")
                    //{
                    //    selectedCountry = "USA";
                    //}

                    if (third.PostalCodeReflectPractice.ToLower() == "yes")  // leave only Canada country in the list
                    {
                        var list6 = repository.GetGeneralList("COUNTRY");
                        //var list6 = Countries.ListOfCountries;
                        list6 = list6.Where(L => L.Code == "CAN").ToList();
                        ddlCountry3.DataSource = list6;
                        ddlCountry3.DataValueField = "Code";
                        ddlCountry3.DataTextField = "Description";
                        ddlCountry3.DataBind();
                        ddlCountry3.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        if (selectedCountry.ToUpper() == "CAN")
                        {
                            ddlCountry3.SelectedValue = selectedCountry;
                        }

                    }
                    else
                    {
                        ddlCountry3.SelectedValue = selectedCountry;
                    }

                    
                    txtTelephone3.Text = third.Phone;
                    txtFax3.Text = third.Fax;
                    if (third.StartDate != null)
                    {
                        txtStartDate3.Text = ((DateTime)third.StartDate).ToString("dd/MM/yyyy");
                    }

                    if (third.EndDate != null)
                    {
                        txtEndDate3.Text = ((DateTime)third.EndDate).ToString("dd/MM/yyyy");
                    }
                    ddlEmploymentRelationship3.SelectedValue = third.EmploymentRelationship;
                    ddlCasualStatus3.SelectedValue = third.CasualStatus;
                    txtAverageWeeklyHours3.Text = third.AverageWeeklyHours.ToString("N0");
                    ddlPrimaryRole3.SelectedValue = third.PrimaryRole;
                    ddlPracticeSetting3.SelectedValue = third.PracticeSetting;

                    ddlMajorServices3.SelectedValue = third.MajorServices;
                    ddlHealthCondition3.SelectedValue = third.HealthCondition;
                    ddlClientAgeRange3.SelectedValue = third.ClientAgeRange;
                    ddlFundingSource3.SelectedValue = third.FundingSource;
                    if (ddlStatus3.SelectedIndex <= 1)
                    {
                        DisableEnableEmployer3Controls(false);
                    }
                }

                if (!(ddlCurrentPracticeStatus.SelectedIndex == 1 || ddlCurrentPracticeStatus.SelectedIndex == 2))
                {
                    ClearEmpoyer1Fields();
                    ClearEmpoyer2Fields();
                    ClearEmpoyer3Fields();
                    ddlStatus1.SelectedIndex = 0; ddlStatus1.Enabled = false;
                    ddlStatus2.SelectedIndex = 0; ddlStatus2.Enabled = false;
                    ddlStatus3.SelectedIndex = 0; ddlStatus3.Enabled = false;
                    DisableEnableEmployer1Controls(false);
                    DisableEnableEmployer2Controls(false);
                    DisableEnableEmployer3Controls(false);
                    rblQuestion1Answers.Enabled = false;
                    ddlFullTimePreference.Enabled = false; rfvFullTimePreference.Enabled = false;
                    ddlNaturePractice.Enabled = false; rfvNaturePractice.Enabled = false;
                }
                else
                {
                    ddlStatus1.Enabled = true;
                    ddlStatus2.Enabled = true;
                    ddlStatus3.Enabled = true;

                    if (ddlStatus1.SelectedValue == "10")
                    {
                        DisableEnableEmployer1Controls(false);

                        // show warning message;
                        lblStatus1Message.Text = "<br />If you select 'No Longer Working Here', your secondary employment will be moved to your primary employment position overnight, no other actions are required. If you have a new primary employment, please select 'Primary Employment' and enter your new employment information.";
                    }

                    if (ddlStatus2.SelectedValue == "10")
                    {
                        DisableEnableEmployer2Controls(false);

                        // show warning message;
                        lblStatus2Message.Text = "<br />If you select 'No Longer Working Here', your tertiary employment will be moved to your secondary employment position overnight, no other actions are required. If you have a new secondary employment, please select 'Seondary Employment' and enter your new employment information.";
                    }

                    if (ddlStatus3.SelectedValue == "10")
                    {
                        DisableEnableEmployer3Controls(false);

                        // show warning message;
                        //lblStatus2Message.Text = "<br />If you select 'No Longer Working Here', your tertiary employment will be moved to your secondary employment position overnight, no other actions are required. If you have a new primary employment, please select 'Seondary Employment' and enter your new employment information.";
                    }

                    //DisableEnableEmployer1Controls(true);
                    //DisableEnableEmployer2Controls(true);
                    //DisableEnableEmployer3Controls(true);

                    rblQuestion1Answers.Enabled = true;
                    ddlFullTimePreference.Enabled = true; rfvFullTimePreference.Enabled = true;
                    ddlNaturePractice.Enabled = true; rfvNaturePractice.Enabled = true;
                }

            }
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public string CurrentPrimaryEmploymentStatus
        {
            get
            {
                if (ViewState["CurrentPrimaryEmploymentStatus"] != null)
                {
                    return (string)(ViewState["CurrentPrimaryEmploymentStatus"]);
                }
                return string.Empty;
            }
            set
            {
                ViewState["CurrentPrimaryEmploymentStatus"] = value;
            }
        }

        public string PreviousCollegeMailingValue
        {
            get
            {
                if (ViewState["PreviousCollegeMailingValue"] != null)
                {
                    return (string)(ViewState["PreviousCollegeMailingValue"]);
                }

                return "No";
            }
            set
            {
                ViewState["PreviousCollegeMailingValue"] = value;
            }
        }

        public string CurrentSecondaryEmploymentStatus
        {
            get
            {
                if (ViewState["CurrentSecondaryEmploymentStatus"] != null)
                {
                    return (string)(ViewState["CurrentSecondaryEmploymentStatus"]);
                }
                return string.Empty;
            }
            set
            {
                ViewState["CurrentSecondaryEmploymentStatus"] = value;
            }
        }

        public string CurrentTertiaryEmploymentStatus
        {
            get
            {
                if (ViewState["CurrentTertiaryEmploymentStatus"] != null)
                {
                    return (string)(ViewState["CurrentTertiaryEmploymentStatus"]);
                }
                return string.Empty;
            }
            set
            {
                ViewState["CurrentTertiaryEmploymentStatus"] = value;
            }
        }

        /// <summary>
        ///  Messages collection. Contains all custom warning or errors which will be populated on the page.
        /// </summary>
        public PageMessages Messages
        {
            get
            {
                return (PageMessages)SessionParameters.PageMessages;
            }
            set
            {
                SessionParameters.PageMessages = value;
            }
        }

        #endregion

    }
}