﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlUpdatePersonalInfo.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlUpdatePersonalInfo" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellspacing="2" cellpadding="3" width="100%">
                    <tr class="HeaderTitle" align="left">
                        <td colspan="2">
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Request to Update Commonly Used Names" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red" />
                        </td>   
                    </tr>
                    <tr>
                        <td colspan="2">
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left;" colspan="2">
                            <p>
                                Please provide the following information regarding your commonly used names if a change is required.
                            </p>
                        </td>
                    </tr>
                   <%-- <tr>
                        <td class="LeftLeftTitle">
                            <span style="font-weight:bold">Legal Last Name</span>
                            &nbsp;
                            <asp:ImageButton ID="ibtnLegalLastNameHelp" ImageUrl="~/images/qmark.jpg" runat="server"
                            OnClick="ibtnLegalLastNameHelpClick" />&nbsp*
                        </td>
                        <td style="text-align:left;">
                            <asp:TextBox ID="txtLegalLastName" runat="server" Columns="25" Enabled="false" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span style="font-weight:bold">Legal Middle Name</span>
                            &nbsp;
                            <asp:ImageButton ID="ibtnLegalMiddleName" ImageUrl="~/images/qmark.jpg" runat="server"
                            OnClick="ibtnLegalLastNameHelpClick" />&nbsp;
                        </td>
                        <td style="text-align:left;">
                            <asp:TextBox ID="txtLegalMiddleName" runat="server" Columns="25" Enabled="false" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span style="font-weight:bold">Legal First Name </span>
                            &nbsp;
                            <asp:ImageButton ID="ibtnLegalFistName" ImageUrl="~/images/qmark.jpg" runat="server"
                            OnClick="ibtnLegalLastNameHelpClick" />&nbsp;*
                        </td>
                        <td style="text-align:left;">
                            <asp:TextBox ID="txtLegalFistName" runat="server" Columns="25" Enabled="false"  />
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span style="font-weight:bold">Previous Legal Last Name </span>
                            &nbsp;
                            <asp:ImageButton ID="ibtnPreviousLegalLastName" ImageUrl="~/images/qmark.jpg" runat="server"
                            OnClick="ibtnPreviousLegalLastNameHelpClick" />&nbsp;
                        </td>
                        <td style="text-align:left;">
                            <asp:TextBox ID="txtPreviousLegalLastName" runat="server" Columns="25" Enabled="false" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span style="font-weight:bold">Previous Legal First Name </span>
                            &nbsp;
                            <asp:ImageButton ID="ibtnPreviousLegalFirstName" ImageUrl="~/images/qmark.jpg" runat="server"
                            OnClick="ibtnPreviousLegalLastNameHelpClick" />&nbsp;
                        </td>
                        <td style="text-align:left;">
                            <asp:TextBox ID="txtPreviousLegalFirstName" runat="server" Columns="25" Enabled="false"/>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span style="font-weight:bold">Commonly Used First Name in Practice </span>
                            &nbsp;
                            <asp:ImageButton ID="ibtnCommonlyUsedFirstName" ImageUrl="~/images/qmark.jpg" runat="server"
                            OnClick="ibtnCommonlyUsedFirstNameHelpClick" />&nbsp;
                        </td>
                        <td style="text-align:left;">
                            <asp:TextBox ID="txtCommonlyUsedFirstName" runat="server" Columns="25"  />
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span style="font-weight:bold">Commonly Used Middle Name in Practice </span>
                            &nbsp;
                            <asp:ImageButton ID="ibtnCommonlyUsedMiddleName" ImageUrl="~/images/qmark.jpg" runat="server"
                            OnClick="ibtnCommonlyUsedFirstNameHelpClick" />&nbsp;
                        </td>
                        <td style="text-align:left;">
                            <asp:TextBox ID="txtCommonlyUsedMiddleName" runat="server" Columns="25" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span style="font-weight:bold">Commonly Used Last Name in Practice </span>
                            &nbsp;
                            <asp:ImageButton ID="ibtnCommonlyUsedLastName" ImageUrl="~/images/qmark.jpg" runat="server"
                            OnClick="ibtnCommonlyUsedFirstNameHelpClick" />&nbsp;
                        </td>
                        <td style="text-align:left;">
                            <asp:TextBox ID="txtCommonlyUsedLastName" runat="server" Columns="25"  />
                        </td>
                    </tr>
                    <tr>
                        <td  colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:left;">
                            <p>
                                The information will be received and entered by College staff and will not appear automatically during your renewal.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right" valign="middle" style="background-color: #ffffff" colspan="2">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/Images/btn_Send.jpg" OnClick="btnSubmitClick" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>