﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;
using Classes;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlIntroduction : System.Web.UI.UserControl
    {

        #region Consts
        private string _Key = "Yz7!~3";
        private string NextStep = WebConfigItems.Step1;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Buffer = true;
            Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
            Response.Expires = -1500;
            Response.Cache.SetNoStore();
            Response.Cache.AppendCacheExtension("no-cache");

            if (Request.QueryString.Count > 1)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }
            if (!RenewalAccess())
            {
                omb.ShowMessage("Our records indicate that you have already renewed your registration and paid. Thank you very much.", "", WebConfigItems.GetCOTOWelcomePageUrl);
            }
            DataBind();
        }

        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            if (!cbAgree.Checked)
            {
                ShowErrorMessage("Please verify you have read and understood the renewal information.");
            }
            else
            {
                //lblDebug.Text += string.Format("<br/>Before redirect, Session['ID']={0}", Session["ID"]);
                Response.Redirect("~/MemberEditRenewal.aspx");
            }
        }

        protected void lbtnResignPageLinkClick(object sender, EventArgs e)
        {
            //string fullName = string.Empty;
            //if (CurrentUser != null)
            //{
            //    fullName = CurrentUser.FullName;
            //}
            Response.Redirect(WebConfigItems.Step0_ResignCertificate);
        }

        #endregion

        #region Methods

        private bool RenewalAccess()
        {
            bool retValue = true;

            var repository = new Repository();

            var user = repository.GetUserInfo(CurrentUserId);
            if (user != null)
            {
                if (user.CertificateExpiry != DateTime.MinValue)
                {
                    var amount_renewal = repository.GetRenewalFee("REN");
                    var amount_Paid = repository.GetRenewalFeePaidAmount(CurrentUserId, "REN", DateTime.Now.Year);
                    var user2 = repository.GetUserDeclarationInfo(CurrentUserId, DateTime.Now.Year);
                    //DateTime expiry = (DateTime)user.CertificateExpiry;
                    //if (expiry.Year == DateTime.Now.Year + 1)
                    //{
                    //    var user2 = repository.GetUserDeclarationInfo(CurrentUserId, DateTime.Now.Year);
                    //    if (user2 != null)
                    //    {
                    //        if (user2.DeclarationInfo.RegistrationDeclaration.ToUpper() == "YES")
                    //        {
                    //            retValue = false;
                    //        }
                    //    }
                    //}
                    if (amount_Paid>0 && amount_Paid< amount_renewal && user2 != null)
                    {
                        Response.Redirect(WebConfigItems.Step12);
                    }
                    else if (amount_Paid >= amount_renewal && user2 != null)
                    {
                        return false;
                    }
                }
                else
                {
                    trMain.Visible = false;
                    trNoAccess.Visible = true;
                    lblNoAccessMessage.Text = "Your record is missing information and you are unable to renew at this time. Please contact registration@coto.org to resolve this issue.";
                }
            }

            return retValue;
        }

        

        private void securityCheck()
        {
            try
            {
                //string hostName = Request.UserHostName;
                string timestamp = Request.QueryString["timestamp"];
                string memID = Request.QueryString["ID"];
                string CotoId = Request.QueryString["COTO_ID"];
                bool debug = false;
                if (Request.QueryString["debug"] != null)
                {
                    debug = true;
                }

                string toHash = memID + _Key + timestamp;
                string hash = getMd5Hash(toHash);

                DateTime dateNow = DateTime.Now;
                string year = dateNow.ToString("yyyy");
                string month = dateNow.ToString("MM");
                string day = dateNow.ToString("dd");
                string hour = dateNow.ToString("HH");
                string minute = dateNow.ToString("mm");

                //  string minute 
                string timestamp2 = year + month + day + hour + minute;

                //   double  timeValueLocal = 
                double timeValueLocal = Convert.ToDouble(timestamp2);
                double timeValuePassed = Convert.ToDouble(timestamp);

                double timeDifference = timeValuePassed - timeValueLocal;
                timeDifference = Math.Abs(timeDifference);

                if (debug)
                {
                    lblDebug.Text = string.Format("imisID={0}, CotoId={1}, hash={2}, Dev.Mode={3}, timeDifference={4}", memID, CotoId, hash, WebConfigItems.DevMode, timeDifference);
                }

                //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
                if (CotoId.ToLower() == hash.ToLower() && timeDifference < 120)
                {
                    Session["ID"] = memID;
                    //lblDebug.Text += string.Format("<br/>Session['ID']={0}", Session["ID"]);
                }
                else
                {
                    var tool = new Tools();
                    tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                    //tool.AddMessage("hash = " + hash.ToLower());
                    //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                    //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                    //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                    tool.AddMessage("Session Timeout.");
                    Response.Redirect("ErrorPage.aspx");

                    Session["ID"] = null;
                }
            }
            catch(Exception ex)
            {
                lblDebug.Text = string.Format("Error: {0}, inner:{1}", ex.Message, ex.InnerException);
            }
            
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        protected void ShowErrorMessage(string Message)
        {
            Message = string.Format("<font color='Red'>{0}</font>", Message);
            lblErrorMessage.Text = Message;
            update.Update();
            modalPopupEx.Show();
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        //public User CurrentUser
        //{
        //    get
        //    {
        //        if (SessionParameters.CurrentUser != null)
        //        {
        //            return SessionParameters.CurrentUser;
        //        }
        //        else
        //            return null;
        //    }
        //    set
        //    {
        //        SessionParameters.CurrentUser = value;
        //    }
        //}
        #endregion
    }
}