﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls.ctlMemberDisplay" Codebehind="ctlMemberDisplay.ascx.cs" %>
<div class="MainForm">
    <center>
        <table cellpadding="2" cellspacing="2" width="600">
            <tr class="HeaderTitle">
                <td >
                    <asp:Label ID="lblPageTitleLabel" runat="server" Text="College of Occupational Therapists of Ontario" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back" OnClick="btnBackClick" />&nbsp;&nbsp;
                    <asp:Button ID="btnEdit" CssClass="button" runat="server" Text="Edit" OnClick="btnEditClick" />&nbsp;&nbsp;
                    <asp:Button ID="btnEmployment" CssClass="button" runat="server" Text="Employment" OnClick="btnEmploymentClick" />
                </td>
            </tr>
            <tr id="trOthers" runat="server" visible="false">
                <td align="right">
                    <asp:Button ID="btnEditPersonal" CssClass="button" runat="server" Text="Personal" OnClick="btnEditPersonalClick" />&nbsp;&nbsp;
                    <asp:Button ID="btnEditLanguages" CssClass="button" runat="server" Text="Languages" OnClick="btnEditLanguagesClick" />&nbsp;&nbsp;
                    <asp:Button ID="btnEditEmployment" CssClass="button" runat="server" Text="Employment" OnClick="btnEditEmploymentClick" />&nbsp;&nbsp;
                    <asp:Button ID="btnEditConduct" CssClass="button" runat="server" Text="Conduct" OnClick="btnEditConductClick" />&nbsp;&nbsp;
                    <asp:Button ID="btnEditInsurance" CssClass="button" runat="server" Text="Insurance" OnClick="btnEditInsuranceClick" />&nbsp;&nbsp;
                    <asp:Button ID="btnEditDeclaration" CssClass="button" runat="server" Text="Declaration" OnClick="btnEditDeclarationClick" />
                </td>
            </tr>
            <tr>
                <td>
                    <table class="memberInfo" cellpadding="2" cellspacing="3" width="100%">
                        <tr class="RowTitle">
                            <td colspan="3">
                                <asp:Label ID="lblPersonalInformationSectionTitle" CssClass="heading" runat="server" Text="Personal Information" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftTitle" style="width: 30%">
                                <asp:Label ID="lblNameLabel" runat="server" Text="Name:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblNameText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftTitle">
                                <asp:Label ID="lblRegistrationLabel" runat="server" Text="Registration #:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblRegistrationText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftTitle">
                                <asp:Label ID="lblCategoryLabel" runat="server" Text="Category:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCategoryText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftTitle">
                                <asp:Label ID="lblCurrentStatusLabel" runat="server" Text="Current Status:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCurrentStatusText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftTitle">
                                <asp:Label ID="lblCertificateExpiryLabel" runat="server" Text="Certificate Expiry:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCertificateExpiryText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftTitle">
                                <asp:Label ID="lblEmailLabel" runat="server" Text="Email:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:HyperLink ID="hlEmail" runat="server"><asp:Label ID="lblEmailText" runat="server" /></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftTitle">
                                <asp:Label ID="lblHomePhoneLabel" runat="server" Text="Home Phone:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblHomePhoneText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="LeftTitle">
                                <asp:Label ID="lblHomeAddressLabel" runat="server" Text="Home Address:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblHomeAddressText" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </center>
</div>
