﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlPaymentConfirmation.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlPaymentConfirmation" %>
<%@ Import Namespace="COTO_RegOnly.Classes" %>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="RowTitle">
                        <td>
                            <asp:Label ID="lblPersonalInformationSectionTitle" CssClass="heading" runat="server"
                                Text="Payment confirmation" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">
                            <p>
                                This confirms that you have paid your Annual Registration Fee and answered all of the mandatory questions 
                                to renew your Certificate of Registration for the <%#  HardcodedValues.RenewalPeriod %>  registration year. 
                                Please print this page as confirmation. An email confirming completion of your Annual Registration will be sent. 
                                Your <%#  HardcodedValues.RenewalPeriod %> wallet card and tax receipt will be mailed to you by August 1, <%#  HardcodedValues.RenewalYear %>.
                            </p>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
