﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberConductRenewal.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlMemberConductRenewal" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion1" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion2" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion3" />

                <asp:AsyncPostBackTrigger ControlID="ddlQuestion4" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion5" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion11" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion12" />
            </Triggers>
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="HeaderTitle" align="right">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 8 of 11" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"  OnClick="ibtnNextClick" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" style="width: 100%; padding: 3px; border-spacing: 2px">
                                <tr class="RowTitle" >
                                    <td colspan="3" style="text-align: left">
                                        <asp:Label ID="lblConductTitle" CssClass="heading" runat="server" Text="Conduct" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left" colspan="2">
                                        <p>
                                            <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" />
                                        </p>
                                    </td>
                                    <td style="text-align: right; font-weight:bold;">
                                        <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/COTO%20Glossary%20of%20Terms%20-%20Conduct%202016%20ENG.pdf" target="_blank">Glossary</a>&nbsp;/&nbsp; <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/COTO%20Glossary%20of%20Terms%20-%20Conduct%202016%20FR.pdf" target="_blank">Glossaire</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left" colspan="3">
                                        <p>These questions pertain to occupational therapy practice, the practice of other professions, offences and other conduct.</p>
                                        <p>All questions must be answered. A response of "Yes" will require further explanation in the field provided. If you have missed a question, you will be prompted to provide an answer before you can proceed to the next page.</p>
                                        <p>For definitions of the terms on this page, please <strong>use the glossary at the top of the page</strong>. If you require more information about the questions on this page, please contact <a href="mailto:investigations@coto.org">investigations@coto.org</a></p>
                                    </td>
                                </tr>
                                <tr>
                                     <td colspan="3" style="vertical-align: top; text-align: left">
                                         <p><b><u>Professional Conduct</u></b></p>
                                     </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="lblQuestion1Title" runat="server" Text="1. Have you been refused registration, membership or a license with a regulatory body in Canada or elsewhere?  *" />
                                        <asp:HiddenField ID="hfReportedDate" runat="server" />
                                         <asp:HiddenField ID="hfReportedSEQN" runat="server"  Value="0"/>
                                    </td>
                                    <td class="RightColumn"  style="width: 20%">
                                        <asp:DropDownList ID="ddlQuestion1" runat="server" OnSelectedIndexChanged="ddlQuestion1SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion1" runat="server" ControlToValidate="ddlQuestion1" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion1Details" runat="server" visible="false">
                                    <td style="vertical-align: top; width: 15%">
                                        <asp:Label ID="lblQuestion1DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion1Details" TextMode="MultiLine" runat="server" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rvfQuestion1Details" runat="server" ControlToValidate="txtQuestion1Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="lblQuestion2Title" runat="server" Text="2. Have you had a finding of professional misconduct, incompetence, incapacity, or similar issue in Canada or elsewhere (you are not required to report findings made by the College of Occupational Therapists of Ontario)? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion2" runat="server" OnSelectedIndexChanged="ddlQuestion2SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion2" runat="server" ControlToValidate="ddlQuestion2" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion2Details" runat="server" visible="false">
                                    <td style="vertical-align: top;">
                                        <asp:Label ID="lblQuestion2DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion2Details" runat="server" TextMode="MultiLine" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion2Details" runat="server" ControlToValidate="txtQuestion2Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion3Title" runat="server" Text="3. Are you currently facing a proceeding (such as a hearing) for professional misconduct, incompetence, incapacity or a similar issue in Canada or elsewhere (you are not required to report proceedings initiated by the College of Occupational Therapists of Ontario)? *" />
                                        <p>Note: A proceeding includes an appeal process.  </p>
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion3" runat="server" OnSelectedIndexChanged="ddlQuestion3SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion3" runat="server" ControlToValidate="ddlQuestion3" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion3Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="lblQuestion3DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion3Details" runat="server" TextMode="MultiLine" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion3Details" runat="server" ControlToValidate="txtQuestion3Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion4Title" runat="server" Text="4. Have you ever had a finding of professional negligence or malpractice in Canada or elsewhere? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion4" runat="server" OnSelectedIndexChanged="ddlQuestion4SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion4" runat="server" ControlToValidate="ddlQuestion4" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion4Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="Label2" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion4Details" runat="server" TextMode="MultiLine" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion4Details" runat="server" ControlToValidate="txtQuestion4Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion5Title" runat="server" Text="5. Is there anything in your previous conduct that would give the College a reason to believe that you lack the knowledge, skill or judgement to practise safely and ethically? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion5" runat="server" OnSelectedIndexChanged="ddlQuestion5SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion5" runat="server" ControlToValidate="ddlQuestion5" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion5Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="Label4" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion5Details" runat="server" TextMode="MultiLine" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion5Details" runat="server" ControlToValidate="txtQuestion5Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                     <td colspan="3" style="vertical-align: top; text-align: left">
                                         <p><b><u>Offences and Bail Conditions</u></b></p>
                                     </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="lblQuestion11Title" runat="server" Text="1. Have you been found guilty by a court or other lawful authority of any offence in Canada or elsewhere that has not been previously reported to the College? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion11" runat="server" OnSelectedIndexChanged="ddlQuestion11SelectedIndexChanged" AutoPostBack="true" CausesValidation="false"/>
                                        <asp:RequiredFieldValidator ID="rfvQuestion11" runat="server" ControlToValidate="ddlQuestion11" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion11Details" runat="server" visible="false">
                                    <td style="vertical-align: top;">
                                        <asp:Label ID="lblQuestion11DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion11Details" runat="server" TextMode="MultiLine" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion11Details" runat="server" ControlToValidate="txtQuestion11Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion12Title" runat="server" Text="2. Are you currently subject to any conditions or restrictions (such as bail conditions) by a court (or similar authority) in Canada or elsewhere? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion12" runat="server" OnSelectedIndexChanged="ddlQuestion12SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion12" runat="server" ControlToValidate="ddlQuestion12" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion12Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="lblQuestion12DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion12Details" runat="server" TextMode="MultiLine" Rows="3"
                                            Columns="60" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion12Details" runat="server" ControlToValidate="txtQuestion12Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
