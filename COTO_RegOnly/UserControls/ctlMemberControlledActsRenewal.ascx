﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberControlledActsRenewal.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlMemberControlledActsRenewal" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>

<%@ Import Namespace="COTO_RegOnly.Classes" %>
<style type="text/css">
    .style1
    {
        height: 27px;
    }
</style>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion1" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion2" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion3" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion4" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion5" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion6" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion7" />
            </Triggers>
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="HeaderTitle" align="right">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 7 of 12" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" width="100%" cellspacing="2" cellpadding="3">
                                <tr class="RowTitle">
                                    <td colspan="2" class="style1">
                                        <div>
                                            <asp:Label ID="lblControlledActsTitle" CssClass="heading" runat="server" Text="Controlled Acts" />
                                        </div>
                                        
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td style="text-align:left" colspan="2">
                                        <p>
                                            <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" />
                                        </p>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td style="text-align:left" colspan="2">
                                        <p>
                                            Please select 'Yes' if you have <b>performed any</b> of the <b>controlled acts</b> listed below
                                            under delegation <b>since June 1,&nbsp;<%# HardcodedValues.PrevRenewalYear%></b>. If you have not performed any of the acts since June 1,&nbsp;<%# HardcodedValues.PrevRenewalYear%>&nbsp;please select 'No' and proceed to the next page.
                                        </p>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblQuestion1Title" runat="server" Text="Communicating to the individual or their personal representative a diagnosis 
                                        identifying a disease or disorder as the cause of symptoms of the individual in circumstances in which it is reasonably foreseeable 
                                        that the individual or their personal representative will rely on the diagnosis." />
                                    </td>
                                    <td class="RightColumn" style="width: 20%">
                                        <asp:DropDownList ID="ddlQuestion1" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion1" runat="server" ControlToValidate="ddlQuestion1" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblQuestion2Title" runat="server" Text="Performing a procedure on tissue below the dermis." />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion2" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion2" runat="server" ControlToValidate="ddlQuestion2" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblQuestion3Title" runat="server" Text="Setting or casting a fracture of a bone or a dislocation of a joint." />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion3" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion3" runat="server" ControlToValidate="ddlQuestion3" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblQuestion4Title" runat="server" Text="Moving the joints of the spine beyond the individual’s usual physiological range of motion using a fast, low amplitude thrust." />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion4" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion4" runat="server" ControlToValidate="ddlQuestion4" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblQuestion5Title" runat="server" Text="Administering substance by injection or inhalation" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion5" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion5" runat="server" ControlToValidate="ddlQuestion5" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        Putting an instrument, hand, or finger,
                                        <ol type="i">
                                            <li>beyond the external ear canal,</li>
                                            <li>beyond the point in the nasal passages where they normally narrow,</li>
                                            <li>beyond the larynx,</li>
                                            <li>beyond the opening of the urethra,</li>
                                            <li>beyond the labia majora,</li>
                                            <li>beyond the anal verge, or</li>
                                            <li>into an artificial opening into the body.</li>
                                        </ol> 
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion6" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion6" runat="server" ControlToValidate="ddlQuestion6" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblQuestion7Title" runat="server" Text="Applying or ordering the application of a form of energy prescribed by the regulations under the <i>Regulated Health Professions Act</i>." />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion7" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion7" runat="server" ControlToValidate="ddlQuestion7" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblQuestion8Title" runat="server" Text="Prescribing, dispensing, selling or compounding a drug as defined in the <i>Drug and Pharmacies Regulation Act</i>, or supervising the part of a pharmacy where such drugs are kept." />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion8" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion8" runat="server" ControlToValidate="ddlQuestion8" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblQuestion9Title" runat="server" Text="Prescribing or dispensing, for vision or eye problems, subnormal vision devices, contact lenses or eye glasses other than simple magnifiers." />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion9" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion9" runat="server" ControlToValidate="ddlQuestion9" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" class="RightColumn">
                                        <asp:Label ID="lblFollowingMessage" runat="server" Font-Bold="true" Text="The following two activities do not require delegation to occupational therapists. " />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="RightColumn">
                                        <p>
                                            1. Controlled act of psychotherapy. <br /><br />
                                            Note: The following elements must be present for a psychotherapy activity or intervention to fall within 
                                            the controlled act of psychotherapy: 
                                            <ol style="padding-left: 40px;list-style-type: decimal;">
                                                <li>You are treating a client </li>
                                                <li>You are applying a psychotherapy technique </li>
                                                <li>You have a therapeutic relationship with the client </li>
                                                <li>The client has a <strong>serious disorder</strong> of thought, cognition, mood, emotional regulation, perception or memory </li>
                                                <li>
                                                    This disorder may <strong>seriously impair</strong> the client’s judgment, insight, behaviour, communication or social functioning.
                                                </li>
                                            </ol>
                                            If you have questions about the controlled act of psychotherapy, please contact <a href="mailto:practice@coto.org">practice@coto.org</a>.  
                                            <p>
                                            </p>
                                            <p>
                                            </p>
                                            <p>
                                            </p>
                                        </p>
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion10" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion10" runat="server" ControlToValidate="ddlQuestion10" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblQuestion11Title" runat="server" Text="2. Acupuncture" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion11" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion11" runat="server" ControlToValidate="ddlQuestion11" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>