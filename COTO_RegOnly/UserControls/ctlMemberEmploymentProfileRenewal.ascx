﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberEmploymentProfileRenewal.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlMemberEmploymentProfileRenewal" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>

<%--<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>--%>
<script src="/Va_dotNet/Scripts/jBox/jBox.min.js"></script>
<link href="/Va_dotNet/Scripts/jBox/jBox.css" rel="stylesheet">

<style type="text/css">
    .style1
    {
        height: 22px;
    }

    .pointer { cursor: pointer; }
</style>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<script runat="server">

protected override void OnInit(EventArgs e)
{
    base.OnInit(e);
    string strScript = "caltotalpercent();";
    Page.ClientScript.RegisterStartupScript(this.GetType(), "OnLoadScript_" + this.ClientID, strScript, true);
}

</script>


<script type="text/javascript">

    jQuery(document).ready(function () {

        attachHelpMessages();

        //var options = {
        //    position: { x: 'right', y: 'center' },
        //    outside: 'x',
        //    theme: 'TooltipBorder',
        //    width: 450
        //};
        //var options2 = {
        //    theme: 'TooltipBorder',
        //    width: 300
        //};
        //$('.tooltip2').jBox('Tooltip', options);
        //$('.tooltip').jBox('Tooltip', options2);
    });

    function attachHelpMessages() {
        let message = jQuery('#hfCurrentPracticeStatusHelp').val();
        new jBox('Modal', {
            attach: '#iCurrentPracticeStatusHelp',
            width: 650,
            title: 'Current Practice Status',
            overlay: false,
            createOnInit: true,
            content: message,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        // iBestDescribePracticeHelp
        let message2 = jQuery('#hfBestDescribePracticeHelp').val();
        new jBox('Modal', {
            attach: '#iBestDescribePracticeHelp',
            width: 650,
            title: 'Nature of your Practice',
            overlay: false,
            createOnInit: true,
            content: message2,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        //iClinicalClientsHelp
        let message3 = jQuery('#hfClinicalClientsHelp').val();
        new jBox('Modal', {
            attach: '#iClinicalClientsHelp',
            width: 650,
            title: 'Clients',
            overlay: false,
            createOnInit: true,
            content: message3,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        // iWeeklyPracticeHoursHelp
        let message4 = jQuery('#hfWeeklyPracticeHoursHelp').val();
        new jBox('Modal', {
            attach: '#iWeeklyPracticeHoursHelp',
            width: 650,
            title: 'Weekly Practice hours',
            overlay: false,
            createOnInit: true,
            content: message4,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        // iWeeklyPracticeHoursPSHelp
        let message5 = jQuery('#hfWeeklyPracticeHoursPSHelp').val();
        new jBox('Modal', {
            attach: '#iWeeklyPracticeHoursPSHelp',
            width: 650,
            title: 'Time spent on direct professional services',
            overlay: false,
            createOnInit: true,
            content: message5,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        // iWeeklyPracticeHoursTHelp
        let message6 = jQuery('#hfWeeklyPracticeHoursTHelp').val();
        new jBox('Modal', {
            attach: '#iWeeklyPracticeHoursTHelp',
            width: 650,
            title: 'Time spent teaching',
            overlay: false,
            createOnInit: true,
            content: message6,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        // iWeeklyPracticeHoursCEHelp
        let message7 = jQuery('#hfWeeklyPracticeHoursCEHelp').val();
        new jBox('Modal', {
            attach: '#iWeeklyPracticeHoursCEHelp',
            width: 650,
            title: 'Time spent on clinical education',
            overlay: false,
            createOnInit: true,
            content: message7,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        // iWeeklyPracticeHoursRHelp
        let message8 = jQuery('#hfWeeklyPracticeHoursRHelp').val();
        new jBox('Modal', {
            attach: '#iWeeklyPracticeHoursRHelp',
            width: 650,
            title: 'Time spent on research',
            overlay: false,
            createOnInit: true,
            content: message8,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        // iWeeklyPracticeHoursAdmHelp
        let message9 = jQuery('#hfWeeklyPracticeHoursAdmHelp').val();
        new jBox('Modal', {
            attach: '#iWeeklyPracticeHoursAdmHelp',
            width: 650,
            title: 'Time spent on administration',
            overlay: false,
            createOnInit: true,
            content: message9,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        // iWeeklyPracticeHoursActHelp
        let message10 = jQuery('#hfWeeklyPracticeHoursActHelp').val();
        new jBox('Modal', {
            attach: '#iWeeklyPracticeHoursActHelp',
            width: 650,
            title: 'Time spent on all other activities',
            overlay: false,
            createOnInit: true,
            content: message10,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        // iFullTimePreferenceHelp
        let message11 = jQuery('#hfFullTimePreferenceHelp').val();
        new jBox('Modal', {
            attach: '#iFullTimePreferenceHelp',
            width: 650,
            title: 'Preferred Work Arrangement',
            overlay: false,
            createOnInit: true,
            content: message11,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });
    }

    function caltotalpercent() {
        
        var p1 = parseFloat(document.getElementById("<%= txtTimeSpentDirectProfessionalServices.ClientID %>").value);
        //  alert(p2);
        var p2 = parseFloat(document.getElementById("<%= txtTimeSpentTeaching.ClientID %>").value);
        //  alert(p2);
        var p3 = parseFloat(document.getElementById("<%= txtTimeSpentClinicalEducation.ClientID %>").value);
        //  alert(p2);
        var p4 = parseFloat(document.getElementById("<%= txtTimeSpentResearch.ClientID %>").value);
        //  alert(p2);
        var p5 = parseFloat(document.getElementById("<%= txtTimeSpentAdministration.ClientID %>").value);
        //  alert(p2);
        var p6 = parseFloat(document.getElementById("<%= txtTimeSpentOtherActivities.ClientID %>").value);


        if (isNaN(parseFloat(p1))) {
            p1 = 0;
            document.getElementById("<%= txtTimeSpentDirectProfessionalServices.ClientID %>").value = 0;
        }
        if (isNaN(parseFloat(p2))) {
            p2 = 0;
            document.getElementById("<%= txtTimeSpentTeaching.ClientID %>").value = 0;
        }
        if (isNaN(parseFloat(p3))) {
            p3 = 0;
            document.getElementById("<%= txtTimeSpentClinicalEducation.ClientID %>").value = 0;
        }
        if (isNaN(parseFloat(p4))) {
            p4 = 0;
            document.getElementById("<%= txtTimeSpentResearch.ClientID %>").value = 0;
        }
        if (isNaN(parseFloat(p5))) {
            p5 = 0;
            document.getElementById("<%= txtTimeSpentAdministration.ClientID %>").value = 0;
        }
        if (isNaN(parseFloat(p6))) {
            p6 = 0;
            document.getElementById("<%= txtTimeSpentOtherActivities.ClientID %>").value = 0;
        }

        var totalpercent = (p1) + (p2) + (p3) + (p4) + (p5) + (p6);
        document.getElementById("<%= lblTotalPercent.ClientID %>").innerHTML = parseFloat(totalpercent) + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%';

    }

</script>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlCurrentPracticeStatus" />
            </Triggers>
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="2" width="100%">
                    <tr class="HeaderTitle">
                        <td class="HeaderTitle" align="right">
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 5 of 12" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="GeneralValidation2"/>
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" UseSubmitBehavior="true" TabIndex="0"/>--%>
                        </td>
                    </tr>
                    <%--<tr>
                        <td align="right">
                            <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back" OnClick="btnBackClick" />&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Update" OnClick="btnUpdateClick"  />
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr class="RowTitle">
                        <td>
                            <div>
                                <asp:Label ID="lblPersonalEmploymentProfileInformationTitle" runat="server" CssClass="heading"
                                Text="Employment Profile" />
                            </div>
                            
                        </td>
                    </tr>
                    <%--<tr>
                        <td style="text-align: left;">
                            <p>
                                <asp:Label ID="lblDenotesRequiredTitle" runat="server"
                                    Text="* denotes required field" />
                            </p>
                        </td>
                    </tr>--%>
                    <tr>
                        <td style="text-align: right; font-weight:bold; padding-top: 10px; padding-bottom: 10px;">
                            <%--<a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/2020/2020_Annual_Renewal_Glossary_S5_EmploymentProfile_English.pdf" target="_blank">Glossary</a>&nbsp;/&nbsp;--%>
                            <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/2021/2021_Annual_Renewal_Glossary_S5_EmploymentProfile_French.pdf" target="_blank">Glossaire</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo">
                                <tr>
                                    <td colspan="2">
                                        <table style="width: 100%">
                                            <tr>
                                                <td class="RightColumn" style="width: 45%">
                                                    <asp:Label ID="lblCurrentPracticeStatus" runat="server" Text="What is your current practice status?"/>&nbsp;
                                                    <input type="hidden" id="hfCurrentPracticeStatusHelp" value="This represents your overall employment status. Select the status that represents your current situation as it exists today. If you are in a non-clinical or non-traditional occupational therapy role, indicate yourself as Employed in OT.<br /><br/><b>Employed in occupational therapy</b><br />Working as an occupational therapist in some capacity as an employee or self-employed professional.<br /><br/><b>Employed, on leave</b><br />Retained/employed as an employee of self-employed professional in occupational	therapy but currently not working as a result of a leave (for example, 	maternity/paternity leave or leave of absence).<br /><br/><b>Unemployed and seeking employment in occupational therapy</b><br />Not currently employed but you are seeking employment in occupational therapy at this time.<br /><br/>Note: It is your responsibility to update all employment profile changes that may occur throughout the year in the Registrants Only section of the College website within 30 days of any change occurring.<br /><br/><b>Unemployed and not seeking employment in occupational therapy</b><br />Not currently employed and not seeking employment in occupational therapy at this time. <br /><br/><b>Working outside the professional and seeking work in occupational therapy</b><br/><br />Currently working in a job unrelated to occupational therapy but you are actively searching for an occupational therapy job. If this applies to you, you are not required to provide Employment Profile details.<br/><br/><b>Working outside the profession and not seeking work in occupational therapy</b><br />Currently working in a job unrelated to occupational therapy and you are not actively pursuing a job in occupational therapy. If this applies to you, you are not required to provide Employment Profile details.<br/><br/><b>Working outside of Ontario </b><br />Currently working outside of Ontario in any job, including occupational therapy. If this applies to you, you are not required to provide Employment Profile details."/>
                                                    <asp:Image ID="iCurrentPracticeStatusHelp" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />

                                                </td>
                                                <td style="text-align: right; width: 55%">
                                                    <asp:DropDownList ID="ddlCurrentPracticeStatus" runat="server" OnSelectedIndexChanged="ddlCurrentPracticeStatusSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>&nbsp;
                                                    <asp:RequiredFieldValidator ID="rfvCurrentPracticeStatus" runat="server" ControlToValidate="ddlCurrentPracticeStatus"
                                                        InitialValue="" ValidationGroup="GeneralValidation2" ErrorMessage="<br />Please provide your practice status."
                                                        Display="Dynamic" ForeColor="Red" EnableClientScript="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="RightColumn" style="width: 75%">
                                        <asp:Label ID="lblFullTimePreference" runat="server" Text="What is your preferred work arrangement?" />&nbsp;
                                        <input type="hidden" id="hfFullTimePreferenceHelp" value="This question refers to your preferred work arrangement, which may be different from your current work arrangement."/>
                                        <asp:Image ID="iFullTimePreferenceHelp" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                    </td>
                                    <td style="text-align: right;">
                                         <asp:DropDownList ID="ddlFullTimePreference" runat="server" >
                                        </asp:DropDownList>&nbsp;
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlFullTimePreference"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please provide your preferred work arrangement."
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblStudentSupervisionLastYear" runat="server" 
                                            Text="Did you provide student supervision for more than three weeks in the last year?" />
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:DropDownList ID="ddlStudentSupervisionLastYear" runat="server" >
                                        </asp:DropDownList>&nbsp;
                                        <asp:RequiredFieldValidator ID="rfvFullTimePreference" runat="server" ControlToValidate="ddlStudentSupervisionLastYear"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please select 'YES' or 'NO'"
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblStudentSupervisionComingYear" runat="server"
                                            Text="Do you plan to provide student supervision this coming year?" />
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:DropDownList ID="ddlStudentSupervisionComingYear" runat="server">
                                        </asp:DropDownList>&nbsp;
                                        <asp:RequiredFieldValidator ID="rfvStudentSupervisionComingYear" runat="server" ControlToValidate="ddlStudentSupervisionComingYear"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please select 'YES' or 'NO'"
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblBestDescribePractice" runat="server" 
                                            Text="Please select which best describes the nature of your practice:" />
                                        <input type="hidden" id="hfBestDescribePracticeHelp" value="<b>Primarily clinical practice</b><br/>A clinical OT who uses occupational therapy knowledge and skills to provide direct service to clients/patients, including assessment, consultation and referral to other services.</br><br/><b>Mixed nature</b><br/>An OT with a mixed nature of practice has primarily a non-clinical nature of practice but provides direct service to clients as well.<br/><br/><b>Primarily non-clinical</b><br/>A non-clinical OT who uses occupational therapy knowledge and skill, and does not provide direct service to clients, who is accountable to the public through the College. If you provide direct care to one or more clients then you do not fall into this category.<br/>"/>
                                        <asp:Image ID="iBestDescribePracticeHelp" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:DropDownList ID="ddlBestDescribePractice" runat="server">
                                        </asp:DropDownList>&nbsp;
                                        <asp:RequiredFieldValidator ID="rfvBestDescribePractice" runat="server" ControlToValidate="ddlBestDescribePractice"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please provide which best describes the nature of your practice."
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblClinicalClientsTitle" runat="server" Text="Do you have clients, even if just one?" />
                                        <input type="hidden" id="hfClinicalClientsHelp" value='Select a response of "Yes" if you have at least one client, patient.<br/>'/>
                                        <asp:Image ID="iClinicalClientsHelp" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:DropDownList ID="ddlClinicalClients" runat="server">
                                        </asp:DropDownList>&nbsp;
                                        <asp:RequiredFieldValidator ID="rfvClinicalClients" runat="server" ControlToValidate="ddlClinicalClients"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please select 'YES' or 'NO'"
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <p style="margin-top:0px!important;">
                                            Enter the <b>number of weeks</b> you spent practising occupational therapy in the past 12 months across all your practice sites or jobs.
                                        </p>
                                    </td>
                                    <td style="text-align: right; vertical-align: top;">
                                        <asp:TextBox  ID="txtNumberWeeksPractices" runat="server" Columns="3" />&nbsp;
                                        <asp:RegularExpressionValidator ID="revNumberWeeksPractices" runat="server" ControlToValidate="txtNumberWeeksPractices" Display="Dynamic" EnableClientScript="false" ForeColor="Red" 
                                        ErrorMessage="<br />Please use whole numbers. Decimal points are not permitted." ValidationGroup="GeneralValidation" ValidationExpression="^(\d)?\d$" />
                                        <asp:RangeValidator ID="rvNumberWeeksPractices" runat="server" MaximumValue="52" MinimumValue="1" ErrorMessage="<br />Must be greater than 0, less than 53." ValidationGroup="GeneralValidation"
                                        ControlToValidate="txtNumberWeeksPractices" Display="Dynamic" EnableClientScript="false" ForeColor="Red" Type="Integer" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <p style="margin-top:0px!important;">
                                            Note: One practice day in any week = one week of practice. 
                                            Exclude your vacation, on-call, sick, and leave time greater than one week. There are 52 weeks in one calendar year.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <p style="margin-top:0px!important;">
                                            Enter the average <b>number of hours</b> (must be whole numbers, no decimals) you spent practising <b>per week</b> 
                                             in the past 12 months across all your practice sites or jobs. The maximum number that you may enter is 84 (equivalent to working 7 days a week, 12 hours per day). 
                                        </p>
                                        
                                    </td>
                                    <td style="text-align: right; vertical-align: top;">
                                        <asp:TextBox  ID="txtNumberHoursPractices" runat="server" Columns="4" />&nbsp;
                                        <%--<asp:RequiredFieldValidator ID="rfvNumberHoursPractices" runat="server" ControlToValidate="txtNumberHoursPractices"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please use whole numbers (integers) for average number of hours spent practising per week in the past 12 months across all your practice sites or jobs."
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red" />--%>
                                        <asp:RegularExpressionValidator ID="revNumberHoursPractices" runat="server" ControlToValidate="txtNumberHoursPractices" Display="Dynamic" EnableClientScript="false" ForeColor="Red" 
                                        ErrorMessage="<br />Please use whole numbers. Decimal points are not permitted." ValidationGroup="GeneralValidation" ValidationExpression="^(\d)?(\d)?\d$" />
                                        <asp:RangeValidator ID="rvNumberHoursPractices" runat="server" MaximumValue="84" MinimumValue="1"  ErrorMessage="<br />Must be greater than 0, less than 85." ValidationGroup="GeneralValidation"
                                        ControlToValidate="txtNumberHoursPractices" Display="Dynamic" EnableClientScript="false" ForeColor="Red" Type="Integer" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <p style="margin-top:0px!important;">
                                            Note: Must include all practice hours, including travel time between practice settings; preparation and service provision. Hours should <b>not include</b> commuting and any time spent volunteering outside of the profession.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                
                                <tr>
                                    <td class="RightColumn" colspan="2">
                                        <p style="margin-top:0px!important;">
                                            Of your weekly practice hours, please provide the percentage of time you spent on each activity, adding to 100%. 
                                            Please ensure that you enter a value for each box (i.e. enter '0' instead of leaving a box blank). Decimal points are not permitted.
                                            &nbsp;<input type="hidden" id="hfWeeklyPracticeHoursHelp" value='This information is collected by HealthForceOntario as a measure of how many hours are spent on each activity, giving an indication of availability of services.<br/>'/>
                                            <asp:Image ID="iWeeklyPracticeHoursHelp" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <table width="85%">
                                            <tr>
                                                <td style="width: 60%; text-align:left;vertical-align:top">
                                                    <span>Time spent on direct professional services</span>&nbsp;
                                                    <input type="hidden" id="hfWeeklyPracticeHoursPSHelp" value='Provide the percentage of time spent per week on direct health professional services across all practice sites (for example, conducting tests, client care, client charting, health promotion, etc.).<br/>'/>
                                                    <asp:Image ID="iWeeklyPracticeHoursPSHelp" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                    <%--<asp:ImageButton ID="ibtnDirectProfServices" ImageUrl="~/images/qmark.jpg" runat="server"
                                                    OnClick="ibtnDirectProfServicesClick" />--%>
                                                </td>
                                                <td style="width: 3%">
                                                    &nbsp;
                                                </td>
                                                <td style="text-align:left;">
                                                    <asp:TextBox ID="txtTimeSpentDirectProfessionalServices" runat="server" Columns="3" />&nbsp%
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTimeSpentDirectProfessionalServices" Display="Dynamic" 
                                                    ErrorMessage="<br />Please use whole numbers. Decimal points are not permitted. Each field requires a value and blanks are not permitted." ValidationExpression="^\d*$" ValidationGroup="GeneralValidation"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left;vertical-align:top">
                                                    <span>Time spent teaching</span>&nbsp;
                                                    <input type="hidden" id="hfWeeklyPracticeHoursTHelp" value='Provide the percentage of time spent per week across all practice sites on teaching to prepare students for a profession in occupational therapy excluding clinical education.<br/>'/>
                                                    <asp:Image ID="iWeeklyPracticeHoursTHelp" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                    <%--<asp:ImageButton ID="ibtnTimeSpentTeaching" ImageUrl="~/images/qmark.jpg" runat="server"
                                                    OnClick="ibtnTimeSpentTeachingClick" />--%>
                                                </td>
                                                <td style="width: 3%">
                                                    &nbsp;
                                                </td>
                                                <td style="text-align:left;">
                                                    <asp:TextBox ID="txtTimeSpentTeaching" runat="server" Columns="3"/>&nbsp%
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtTimeSpentTeaching" Display="Dynamic" 
                                                    ErrorMessage="<br />Please use whole numbers. Decimal points are not permitted. Each field requires a value and blanks are not permitted." ValidationExpression="^\d*$" ValidationGroup="GeneralValidation"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left;vertical-align:top">
                                                    <span>Time spent on clinical education</span>&nbsp;
                                                    <input type="hidden" id="hfWeeklyPracticeHoursCEHelp" value='Provide the percentage of time spent per week across all practice sites on clinical education only.<br/>'/>
                                                    <asp:Image ID="iWeeklyPracticeHoursCEHelp" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                    <%--<asp:ImageButton ID="ibtnTimeSpentClinicalEducation" ImageUrl="~/images/qmark.jpg" runat="server"
                                                    OnClick="ibtnTimeSpentClinicalEducationClick" />--%>
                                                </td>
                                                <td style="width: 3%">
                                                    &nbsp;
                                                </td>
                                                <td style="text-align:left;">
                                                    <asp:TextBox ID="txtTimeSpentClinicalEducation" runat="server" Columns="3"/>&nbsp%
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtTimeSpentClinicalEducation" Display="Dynamic" 
                                                    ErrorMessage="<br />Please use whole numbers. Decimal points are not permitted. Each field requires a value and blanks are not permitted." ValidationExpression="^\d*$" ValidationGroup="GeneralValidation"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left;vertical-align:top">
                                                    <span>Time spent on research</span>&nbsp;
                                                    <input type="hidden" id="hfWeeklyPracticeHoursRHelp" value='Provide the percentage of time spent per week across all practice sites for conducting research in the profession (for example, time spent preparing research proposals, planning or carrying out research activities, and analysing and documenting or disseminating results).<br/>'/>
                                                    <asp:Image ID="iWeeklyPracticeHoursRHelp" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                    <%--<asp:ImageButton ID="ibtnTimeSpentResearch" ImageUrl="~/images/qmark.jpg" runat="server"
                                                    OnClick="ibtnTimeSpentResearchClick" />--%>
                                                </td>
                                                <td style="width: 3%">
                                                    &nbsp;
                                                </td>
                                                <td style="text-align:left;">
                                                    <asp:TextBox ID="txtTimeSpentResearch" runat="server" Columns="3" />&nbsp%
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtTimeSpentResearch" Display="Dynamic" 
                                                    ErrorMessage="<br />Please use whole numbers. Decimal points are not permitted. Each field requires a value and blanks are not permitted." ValidationExpression="^\d*$" ValidationGroup="GeneralValidation"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left;vertical-align:top">
                                                    <span>Time spent on administration</span>&nbsp;
                                                    <input type="hidden" id="hfWeeklyPracticeHoursAdmHelp" value='Provide the percentage of time spent per week across all practice sites for administration activities in occupational therapy. Administration includes planning, organizing, management and paperwork (for example, completing stats, billing, etc.).<br/>'/>
                                                    <asp:Image ID="iWeeklyPracticeHoursAdmHelp" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                    <%--<asp:ImageButton ID="ibtnTimeSpentAdministration" ImageUrl="~/images/qmark.jpg" runat="server"
                                                    OnClick="ibtnTimeSpentAdministrationClick" />--%>
                                                </td>
                                                <td style="width: 3%">
                                                    &nbsp;
                                                </td>
                                                <td style="text-align:left;">
                                                    <asp:TextBox ID="txtTimeSpentAdministration" runat="server" Columns="3" />&nbsp%
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtTimeSpentAdministration" Display="Dynamic" 
                                                    ErrorMessage="<br />Please use whole numbers. Decimal points are not permitted. Each field requires a value and blanks are not permitted." ValidationExpression="^\d*$" ValidationGroup="GeneralValidation"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left; vertical-align:top">
                                                    <span>Time spent on all other activities</span>&nbsp;
                                                    <input type="hidden" id="hfWeeklyPracticeHoursActHelp" value='Provide the percentage of time spent per week across all practice sites on activities excluding direct professional services, teaching, research and administration.<br/>'/>
                                                    <asp:Image ID="iWeeklyPracticeHoursActHelp" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                                </td>
                                                <td style="width: 3%">
                                                    &nbsp;
                                                </td>
                                                <td style="text-align:left;">
                                                    <asp:TextBox ID="txtTimeSpentOtherActivities" runat="server" Columns="3" />&nbsp%
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtTimeSpentOtherActivities" Display="Dynamic" 
                                                    ErrorMessage="<br />Please use whole numbers. Decimal points are not permitted. Each field requires a value and blanks are not permitted." ValidationExpression="^\d*$" ValidationGroup="GeneralValidation"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"><hr /></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:right;">
                                                    <span style="font-weight:bold">Total Percent</span>
                                                </td>
                                                <td style="width: 3%">
                                                    &nbsp;
                                                </td>
                                                <td style="text-align:left;">
                                                    <asp:Label ID="lblTotalPercent" runat="server"  Columns="5"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="GeneralValidation2" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" UseSubmitBehavior="true" TabIndex="0"/>--%>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>