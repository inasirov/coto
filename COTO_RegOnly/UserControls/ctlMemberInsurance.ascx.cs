﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Classes;
using System.Security.Cryptography;
using System.Globalization;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberInsurance : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;

        private string PrevStep = WebConfigItems.Step9;
        private string NextStep = WebConfigItems.Step11;
        private const int CurrentStep = 10;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

                if (string.IsNullOrEmpty((string)Session["ID"]))
                {
                    Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                     return;
                }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.RenewalStep = CurrentStep;
                BindLists();
                BindData();
            }
        }

        protected void btnBackClick(object sender, EventArgs e)
        {
            UpdateSteps(-1);
            //Response.Redirect("MemberDisplay.aspx");
            Response.Redirect(PrevStep);
        }
        protected void btnUpdateClick(object sender, EventArgs e)
        {
            Page.Validate("PersonalValidation");
            if (Page.IsValid)
            {
                UpdateUserInsuranceInfo();
                UpdateSteps(1);
                //Response.Redirect("MemberDisplay.aspx");
                Response.Redirect(NextStep);
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            UpdateSteps(-1);
            //Response.Redirect("MemberDisplay.aspx");
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            Page.Validate("PersonalValidation");
            if (Page.IsValid)
            {
                UpdateUserInsuranceInfo();
                UpdateSteps(1);
                //Response.Redirect("MemberDisplay.aspx");
                Response.Redirect(NextStep);
            }
        }

        protected void ibLanguageServiceHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("Please indicate all language(s) in which you can competently provide OT services.", "Help Languages");
        }

        protected void ddlPlanHeldWithSelectedIndexChanged(object sender, EventArgs e)
        {
            //txtExpiryDate.ReadOnly = false;
            //PopCalendar1.Visible = true;
            if (ddlPlanHeldWith.SelectedValue.ToLower() == "ot")
            {
                trOtherInsurance.Visible = true;
                //txtCertificate.MaxLength = 10;
                //revCertificate.Enabled = false;
            }
            else
            {
                txtOther.Text = string.Empty;
                trOtherInsurance.Visible = false;
                //txtCertificate.MaxLength = 6;
                //revCertificate.Enabled = true;
                //Page.Validate("PersonalValidation");
                //if (ddlPlanHeldWith.SelectedValue == "CAOT" || ddlPlanHeldWith.SelectedValue == "OSOT")
                //{
                //    DateTime expiryDate = new DateTime(DateTime.Now.Year, 10, 1);
                //    txtExpiryDate.Text = expiryDate.ToString("dd/MM/yyyy");
                //    txtExpiryDate.ReadOnly = true;
                //    PopCalendar1.Visible = false;
                //}
            }
        }

        #endregion

        #region Methods

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }
        
        protected void UpdateUserInsuranceInfo()
        {
            int currentYear = DateTime.Now.Year;
            User user = new User();

            user.Id = CurrentUserId;
            user.InsuranceInfo = new Classes.Insurance();

            user.InsuranceInfo.PlanHeld = ddlPlanHeldWith.SelectedValue;
            user.InsuranceInfo.OtherInsuranceProvider = txtOther.Text;

            if (!string.IsNullOrEmpty(txtExpiryDate.Text))
            {
                DateTime complDate = DateTime.MinValue;
                CultureInfo provider = CultureInfo.InvariantCulture;
                string format = "dd/MM/yyyy";
                DateTime.TryParseExact(txtExpiryDate.Text, format, provider, DateTimeStyles.None, out complDate);
                //var culture = new CultureInfo("en-CA");
                //var expiryDate = Convert.ToDateTime(txtExpiryDate.Text, culture);
                user.InsuranceInfo.ExpiryDate = complDate;
            }
            else
            {
                user.InsuranceInfo.ExpiryDate = DateTime.MinValue;
            }
             
            user.InsuranceInfo.CertificateNumber = txtCertificate.Text;

            
            var repository = new Repository();
            repository.UpdateUserInsuranceInfoLogged( CurrentUserId, user, currentYear);
            //ShowMessage("Insurance dropdown valie is " + ddlPlanHeldWith.SelectedValue.ToUpper());
            if (ddlPlanHeldWith.SelectedValue.ToUpper() == "OT")
            {
                 string message = string.Format("Member ID: {0}; ", CurrentUserId); ;
                 message += string.Format("Date: {0}<br />" , DateTime.Now.ToString("yyyy-MM-dd HH:mm"));


                 message += "You have selected \"Other\" as your insurance provider. The insurance policy you have selected must meet the College’s requirements " +
                 "for professional liability insurance and include a sexual abuse therapy and counseling fund endorsement.<br /><br />" +
                 "You are required to submit a copy of the insurance policy content and certificate to the College for review and approval. " +
                 "Please submit a copy to Clara Lau, Manager, Registration via email  clau@coto.org or by fax  416-214-0851. <br /><br />" +
                 "If you have any questions please contact Clara Lau via email  clau@coto.org or by telephone  416-214-1177 (toll free 1-800-890-6570) x229 ";

                 string emailSubject = "Registrant Requires Insurance Review " + WebConfigItems.GetTestLabel;
                string emailTo = WebConfigItems.RegistrationManagerContactEmail;
                //if (!string.IsNullOrEmpty(CurrentUserEmail))
                //{
                //    emailTo += "; " + CurrentUserEmail;
                //}
                var tool = new Tools();
                tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, CurrentUserEmail);
            }
        }

        protected void BindData()
        {
            //txtExpiryDate.ReadOnly = false;
            //PopCalendar1.Visible = true;

            var repository = new Repository();
            int currentYear = DateTime.Now.Year;
            var user = repository.GetUserInsuranceInfo(CurrentUserId, currentYear);
            if (user != null)
            {
                if (user.InsuranceInfo != null)
                {
                    ddlPlanHeldWith.SelectedValue = user.InsuranceInfo.PlanHeld;
                    if (ddlPlanHeldWith.SelectedValue.ToLower() == "ot")
                    {
                        txtOther.Text = user.InsuranceInfo.OtherInsuranceProvider;
                        trOtherInsurance.Visible = true;
                        //txtCertificate.MaxLength = 10;
                        //revCertificate.Enabled = false;

                        //if (user.InsuranceInfo.ExpiryDate != DateTime.MinValue)
                        //{
                        //    txtExpiryDate.Text = user.InsuranceInfo.ExpiryDate.ToString("dd/MM/yyyy");
                        //}

                    }
                    else
                    {
                        trOtherInsurance.Visible = false;

                        //if (ddlPlanHeldWith.SelectedValue == "CAOT" || ddlPlanHeldWith.SelectedValue == "OSOT")
                        //{
                        //    DateTime expiryDate = new DateTime(DateTime.Now.Year, 10, 1);
                        //    txtExpiryDate.Text = expiryDate.ToString("dd/MM/yyyy");
                        //    txtExpiryDate.ReadOnly = true;
                        //    PopCalendar1.Visible = false;
                        //}
                    }
                    if (user.InsuranceInfo.ExpiryDate != DateTime.MinValue)
                    {
                        txtExpiryDate.Text = user.InsuranceInfo.ExpiryDate.ToString("dd/MM/yyyy");
                    }
                    
                    txtCertificate.Text = user.InsuranceInfo.CertificateNumber;
                }
            }
            var user2 = repository.GetUserInfo(CurrentUserId);
            if (user2 != null)
            {
                //CurrentUser = user2;
                CurrentUserEmail = user2.Email;
                lblInsuranceTitle.Text = "Insurance for " + user2.FullName;
            }
        }

        protected void BindLists()
        {
            var repository = new Repository();

            var list1 = repository.GetGeneralList("INSURANCE"); // get data for status field
            var item = list1.Find(I => I.Code == "NONE");
            if (item != null)
            {
                list1.Remove(item);
            }
            ddlPlanHeldWith.DataSource = list1;
            ddlPlanHeldWith.DataValueField = "CODE";
            ddlPlanHeldWith.DataTextField = "DESCRIPTION";
            ddlPlanHeldWith.DataBind();
            ddlPlanHeldWith.Items.Insert(0, string.Empty);
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public string CurrentUserEmail
        {
            get
            {
                if (ViewState["CURRENT_USER_EMAIL"] != null)
                {
                    return (string)ViewState["CURRENT_USER_EMAIL"];
                }
                else
                    return string.Empty; ;
            }
            set
            {
                ViewState["CURRENT_USER_EMAIL"] = value;
            }
        }
        #endregion
    }
}