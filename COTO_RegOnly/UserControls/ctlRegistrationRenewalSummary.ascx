﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlRegistrationRenewalSummary.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlRegistrationRenewalSummary" %>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="RowTitle">
                        <td>
                            <asp:Label ID="lblPersonalInformationSectionTitle" CssClass="heading" runat="server"
                                Text="Annual Registration Renewal Summary" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">
                            <p style="font-weight: bold;">
                                Thank you for completing your annual registration renewal online.
                            </p>
                            <p>
                                If you wish to print a PDF report of your online annual renewal information please&nbsp;
                                <asp:LinkButton ID="lbtnPrintReport" runat="server" Text="click here" OnClick="lbtnPrintReportClick" />
                            </p>
                            <p>
                                If you would like to provide feedback with regard to your online registration renewal
                                experience, please click
                                <asp:LinkButton ID="lbtnFeedbackLink" runat="server" Text="click here" OnClick="lbtnFeedbackLinkClick" />
                            </p>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
