﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlPublicRegisterDisplay.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlPublicRegisterDisplay" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<div class="MainForm">
    <center>
        <table cellpadding="2" cellspacing="2" width="600">
            <tr class="HeaderTitle">
                <td>
                    <asp:Label ID="lblPageTitleLabel" runat="server" Text="OT Directory Search Results" />
                </td>
            </tr>
            <tr>
                <td>
                    <uc:MessageBox ID="omb" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <table class="memberInfo" cellpadding="2" cellspacing="3" width="100%">
                        <tr class="RowTitle2">
                            <td colspan="3">
                                <asp:Label ID="lblPersonalInformationSectionTitle" CssClass="heading2" runat="server"
                                    Text="OT GENERAL INFORMATION" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%">
                                <asp:Label ID="lblNameLabel" runat="server" Text="Name:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblNameText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%">
                                <asp:Label ID="lblLegalFirstNameTitle" runat="server" Text="Legal First Name:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblLegalFirstNameText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%">
                                <asp:Label ID="lblLegalMiddleNameTitle" runat="server" Text="Legal Middle Name:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblLegalMiddleNameText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%">
                                <asp:Label ID="lblLegalLastNameTitle" runat="server" Text="Legal Last Name:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblLegalLastNameText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle">
                                <asp:Label ID="lblFormerNamesTitle" runat="server" Text="Former / AKA Names:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblFormerNamesEmpty" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table width="100%" cellpadding="3">
                                    <tr id="trFormerNameTitleRow" runat="server" visible="false">
                                        <td class="LeftSubTitle">
                                            <span>First Name</span>
                                        </td>
                                        <td class="LeftSubTitle">
                                            <span>Middle Name</span>
                                        </td>
                                        <td class="LeftSubTitle">
                                            <span>Last Name</span>
                                        </td>
                                    </tr>
                                    <tr id="trFormerNameDataRow" runat="server" visible="false">
                                        <td class="RightColumn">
                                            <asp:Label ID="lblPreviousFirstNameText" runat="server" />
                                        </td>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblPreviousMiddleNameText" runat="server" />
                                        </td>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblPreviousLastNameText" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle">
                                <asp:Label ID="lblRegistrationLabel" runat="server" Text="Registration Number:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblRegistrationText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" valign="top">
                                <asp:Label ID="lblPreviousRegistrationLabel" runat="server" Text="Previous Registration #'s:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblPreviousRegistrationText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle">
                                <asp:Label ID="lblCategoryLabel" runat="server" Text="Category:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCategoryText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle">
                                <asp:Label ID="lblCurrentStatusLabel" runat="server" Text="Current Status:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCurrentStatusText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" valign="top">
                                <span>Terms, Conditions and Limitations:</span>
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblTermsConditionsText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle">
                                <span>Language(s) of Service:</span>
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblLanguageServiceText" runat="server" />
                            </td>
                        </tr>
                        <tr class="RowTitle2">
                            <td colspan="3">
                                <asp:Label ID="Label1" CssClass="heading2" runat="server" Text="DISCIPLINE INFORMATION" />
                            </td>
                        </tr>
                        <%--<tr id="trDisciplineEmptyRow">
                            <td class="LeftLeftTitle" colspan="3">
                                <span>None</span>
                            </td>
                        </tr>--%>
                        <tr>
                            <td colspan="3">
                                <%--<asp:ListView ID="lvDisciplines" runat="server" OnItemCommand="lvDisciplinesCommand">
                                    <LayoutTemplate>
                                        <table width="100%">
                                            <tr id="ItemPlaceHolder" runat="server" />
                                        </table>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td class="LeftLeftTitle">
                                                    <span>None</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td class="LeftLeftTitle" style="width: 40%">
                                                            <asp:Label ID="lblDecisionMadeTitle" runat="server" Text="Date of Referral to Discipline Committee:" />
                                                        </td>
                                                        <td style="width: 3%">
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="lblDecisionMadeText" runat="server" Text='<%# ((DateTime)Eval("DecisionMade")).ToString("MMMMM dd, yyyy") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle">
                                                            <asp:Label ID="lblSummaryAllegationsTitle" runat="server" Text="Summary of Allegations:" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="lblSummaryAllegationsText" runat="server" Text='<%# (string)Eval("HearAllegations") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle">
                                                            <asp:Label ID="lblDateDisciplineHearingTitle" runat="server" Text="Date of Discipline Hearing: " />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="lblDateDisciplineHearingText" runat="server" Text='<%# ((DateTime)Eval("DiscHearingDate")).ToString("MMMMM dd, yyyy") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle">
                                                            <asp:Label ID="lblPenaltyHearingDateTitle" runat="server" Text="Penalty Hearing Date: " />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="lblPenaltyHearingDateText" runat="server" Text='<%# ((DateTime)Eval("PenaltyHearingDate")).ToString("MMMMM dd, yyyy") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle">
                                                            <asp:Label ID="lblHearOutcomeSummaryTitle" runat="server" Text="Hearing Summary: " />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:LinkButton ID="lbtnViewSummary" runat="server" CommandArgument='<%# (string)Eval("HearOutcomeSummary") %>'
                                                                CommandName="View">View hearing summary</asp:LinkButton>
                                                            <asp:Label ID="lblHearOutcomeSummaryText" runat="server" Text='<%# (string)Eval("HearOutcomeSummary") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle">
                                                            <asp:Label ID="lblAdditionalInfoTitle" runat="server" Text="Additional Info: " />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="lblAdditionalInfoText" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <ItemSeparatorTemplate>
                                        <tr>
                                            <td align="right">
                                                <hr style="width: 520px;" />
                                            </td>
                                        </tr>
                                    </ItemSeparatorTemplate>
                                </asp:ListView>--%>
                                <table width="100%">
                                    <tr>
                                        <td class="LeftLeftTitle" style="width: 40%">
                                            <asp:Label ID="lblDisciplines" runat="server" />
                                        </td>
                                        <td style="width: 3%">
                                        </td>
                                        <td class="RightColumn">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <asp:ListView ID="lvDisciplines2" runat="server">
                                    <LayoutTemplate>
                                        <table width="100%">
                                            <tr id="ItemPlaceHolder" runat="server" />
                                        </table>
                                    </LayoutTemplate>
                                    <%--<EmptyDataTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td class="LeftLeftTitle">
                                                    <span>None</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>--%>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td class="LeftLeftTitle" style="width: 40%; vertical-align: top">
                                                            <asp:Label ID="lblDecisionMadeTitle" runat="server" Text="Date of Referral to Discipline Committee:" />
                                                        </td>
                                                        <td style="width: 3%">
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="lblDecisionMadeText" runat="server" Text='<%# ((DateTime)Eval("RefToDiscComm")).ToString("MMMM dd, yyyy") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle" style="vertical-align: top">
                                                            <asp:Label ID="lblSummaryAllegationsTitle" runat="server" Text="Summary of Allegations:" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="lblSummaryAllegationsText" runat="server" Text='<%# (string)Eval("SummaryAllegation") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle"  style="vertical-align: top">
                                                            <asp:Label ID="lblDateDisciplineHearingTitle" runat="server" Text="Date of Discipline Hearing: " />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="lblDateDisciplineHearingText" runat="server" Text='<%# (string)Eval("DiscHearingDate") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle"  style="vertical-align: top">
                                                            <asp:Label ID="lblPenaltyHearingDateTitle" runat="server" Text="Penalty Hearing Date: " />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="lblPenaltyHearingDateText" runat="server" Text='<%# (string)Eval("PenaltyHearingDate") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle"  style="vertical-align: top">
                                                            <asp:Label ID="lblHearingPanelMembersTitle" runat="server" Text="Hearing Panel Members: " />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="lblHearingPanelMembers" runat="server" Text='<%# (string)Eval("HearingPanelMembers") %>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table id="trDecisionFinding" runat="server" width="100%" visible='<%# (DateTime)Eval("DecisionFinding") != DateTime.MinValue %>'>
                                                    <tr>
                                                        <td class="LeftLeftTitle" style="width: 40%; vertical-align: top">
                                                            <asp:Label ID="Label8" runat="server" Text="Date of Decision on Finding: " />
                                                        </td>
                                                        <td style="width: 3%">
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="Label9" runat="server" Text='<%# ((DateTime)Eval("DecisionFinding")).ToString("MMMM dd, yyyy") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle" style="vertical-align: top">
                                                            <asp:Label ID="Label10" runat="server" Text="Terms of Publication Ban: " />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="Label11" runat="server" Text='<%# (string)Eval("PublicationBanTerms") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle" style="vertical-align: top">
                                                            <asp:Label ID="Label12" runat="server" Text="Discipline Hearing Outcome: " />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="Label13" runat="server" Text='<%# (string)Eval("HearingOutcome") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle" style="vertical-align: top">
                                                            <asp:Label ID="lblHearOutcomeSummaryTitle" runat="server" Text="Hearing Summary: " />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <%--<asp:LinkButton ID="lbtnViewSummary" runat="server" CommandArgument='<%# (string)Eval("HearingSummary") %>'
                                                                CommandName="View">View hearing summary</asp:LinkButton>--%>
                                                            <asp:Label ID="lblHearingSummaryText" runat="server" Text='<%# (string)Eval("HearingSummary") %>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <ItemSeparatorTemplate>
                                        <tr>
                                            <td align="right">
                                                <hr style="width: 520px;" />
                                            </td>
                                        </tr>
                                    </ItemSeparatorTemplate>
                                </asp:ListView>
                                <%--<asp:ListView ID="lvDisciplines3" runat="server">
                                    <LayoutTemplate>
                                        <table width="100%">
                                            <tr id="ItemPlaceHolder" runat="server" />
                                        </table>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td class="LeftLeftTitle">
                                                    <span>None</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td class="LeftLeftTitle" style="width: 40%; vertical-align: top">
                                                            <asp:Label ID="lblDecisionMadeTitle" runat="server" Text="Date of Decision on Finding: " />
                                                        </td>
                                                        <td style="width: 3%">
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="lblDecisionMadeText" runat="server" Text='<%# ((DateTime)Eval("DecisionFinding")).ToString("MMMM dd, yyyy") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle" style="vertical-align: top">
                                                            <asp:Label ID="lblSummaryAllegationsTitle" runat="server" Text="Terms of Publication Ban: " />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="lblSummaryAllegationsText" runat="server" Text='<%# (string)Eval("PublicationBanTerms") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle" style="vertical-align: top">
                                                            <asp:Label ID="lblDateDisciplineHearingTitle" runat="server" Text="Discipline Hearing Outcome: " />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:Label ID="lblDateDisciplineHearingText" runat="server" Text='<%# (string)Eval("HearingOutcome") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="LeftLeftTitle" style="vertical-align: top">
                                                            <asp:Label ID="lblHearOutcomeSummaryTitle" runat="server" Text="Hearing Summary: " />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td class="RightColumn">
                                                            <asp:LinkButton ID="lbtnViewSummary" runat="server" CommandArgument='<%# (string)Eval("HearingSummary") %>'
                                                                CommandName="View">View hearing summary</asp:LinkButton>
                                                            <asp:Label ID="lblHearingSummaryText" runat="server" Text='<%# (string)Eval("HearingSummary") %>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <ItemSeparatorTemplate>
                                        <tr>
                                            <td align="right">
                                                <hr style="width: 520px;" />
                                            </td>
                                        </tr>
                                    </ItemSeparatorTemplate>
                                </asp:ListView>--%>
                            </td>
                        </tr>
                        <tr class="RowTitle2">
                            <td colspan="3">
                                <asp:Label ID="Label2" CssClass="heading2" runat="server" Text="FITNESS TO PRACTISE" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" colspan="3">
                                <span>None</span>
                            </td>
                        </tr>
                        <tr class="RowTitle2">
                            <td colspan="3">
                                <asp:Label ID="Label3" CssClass="heading2" runat="server" Text="PROFESSIONAL NEGLIGENCE / MALPRACTICE" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" colspan="3">
                                <span>None</span>
                            </td>
                        </tr>
                        <tr class="RowTitle2">
                            <td colspan="3">
                                <asp:Label ID="Label4" CssClass="heading2" runat="server" Text="CERTIFICATE OF REGISTRATION HISTORY" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle">
                                <asp:Label ID="Label7" runat="server" Text="Initial Registration Date:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblInitialRegistrationDateText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle">
                                <asp:Label ID="lblCertificateExpiryLabel" runat="server" Text="Expiry Date:" />
                            </td>
                            <td>
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCertificateExpiryText" runat="server" />
                            </td>
                        </tr>
                        <tr class="RowTitle2" id="trPracticeInformation" runat="server">
                            <td colspan="3">
                                <asp:Label ID="Label5" CssClass="heading2" runat="server" Text="PRACTICE INFORMATION" />
                            </td>
                        </tr>
                        <tr id="trPracticeInformation1" runat="server">
                            <td colspan="3">
                                <asp:Panel ID="pnlPrimaryEmployment" runat="server" Visible="false">
                                    <table width="100%">
                                        <tr>
                                            <td class="LeftSubTitle" colspan="3">
                                                <asp:Label ID="lblPrimaryEmployerTitle" runat="server" Text="PRIMARY EMPLOYER" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LeftLeftTitle" style="width: 40%">
                                                <asp:Label ID="lblPrimaryEmployerNameTitle" runat="server" Text="Name:" />
                                            </td>
                                            <td style="width: 3%">
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblPrimaryEmployerNameText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment1Address1" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblPrimaryEmployerAddressTitle" runat="server" Text="Address:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblPrimaryEmployerAddressText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment1Address2" runat="server">
                                            <td class="LeftLeftTitle">
                                                &nbsp;
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblPrimaryEmployerAddress2Text" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment1Address3" runat="server">
                                            <td class="LeftLeftTitle">
                                                &nbsp;
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblPrimaryEmployerCityProvincePostalText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment1Phone" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="Label14" runat="server" Text="Telephone:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblPrimaryEmployerTelephoneText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LeftSubTitle" colspan="3">
                                                <asp:Label ID="lblPrimaryEmploymentProfileTitle" runat="server" Text="Employment Profile" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment1FundingSource" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblEPrimarymploymentFundingSourceTitle" runat="server" Text="Funding Source:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblPrimaryEmploymentFundingSourceText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment1PracticeSetting" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblPrimaryEmploymentPracticeSettingTitle" runat="server" Text="Practice Setting Type:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblPrimaryEmploymentPracticeSettingText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment1MajorService" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblPrimaryEmploymentMajorServiceTitle" runat="server" Text="Major Service Provided:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblPrimaryEmploymentMajorServiceText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment1ClientAgeRange" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblPrimaryEmploymentClientAgeRangeTitle" runat="server" Text="Client Age Range:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblPrimaryEmploymentClientAgeRangeText" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlSecondaryEmployment" runat="server" Visible="false">
                                    <table width="100%">
                                        <tr>
                                            <td class="LeftSubTitle" colspan="3">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LeftSubTitle" colspan="3">
                                                <hr style="width: 520px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LeftSubTitle" colspan="3">
                                                <asp:Label ID="lblSecondaryEmployerTitle" runat="server" Text="SECONDARY  EMPLOYER" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LeftLeftTitle" style="width: 40%">
                                                <asp:Label ID="lblSecondaryEmployerNameTitle" runat="server" Text="Name:" />
                                            </td>
                                            <td style="width: 3%">
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblSecondaryEmployerNameText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment2Address1" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblSecondaryEmployerAddressTitle" runat="server" Text="Address:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblSecondaryEmployerAddressText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment2Address2" runat="server">
                                            <td class="LeftLeftTitle">
                                                &nbsp;
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblSecondaryEmployerAddress2Text" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment2Address3" runat="server">
                                            <td class="LeftLeftTitle">
                                                &nbsp;
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblSecondaryEmployerCityProvincePostalText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment2Phone" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblSecondaryEmployerPhoneTitle" runat="server" Text="Telephone:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblSecondaryEmployerPhoneText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LeftSubTitle" colspan="3">
                                                <asp:Label ID="Label18" runat="server" Text="Employment Profile" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment2FundingSource" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblSecondaryEmployerFundingTitle" runat="server" Text="Funding Source:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblSecondaryEmployerFundingText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment2PracticeSetting" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblSecondaryEmployerPracticeSettingTitle" runat="server" Text="Practice Setting Type:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblSecondaryEmployerPracticeSettingText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment2MajorService" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblSecondaryEmployerMajorServiceTitle" runat="server" Text="Major Service Provided:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblSecondaryEmployerMajorServiceText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment2CientAgeRange" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblSecondaryEmployerClientAgeRangeTitle" runat="server" Text="Client Age Range:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblSecondaryEmployerClientAgeRangeText" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlTertiaryEmployment" runat="server" Visible="false">
                                    <table width="100%">
                                        <tr>
                                            <td class="LeftSubTitle" colspan="3">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LeftSubTitle" colspan="3">
                                                <hr style="width: 520px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LeftSubTitle" colspan="3">
                                                <asp:Label ID="lblTertiaryEmployerTitle" runat="server" Text="TERTIARY EMPLOYER" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LeftLeftTitle" style="width: 40%">
                                                <asp:Label ID="lblTertiaryEmployerNameTitle" runat="server" Text="Name:" />
                                            </td>
                                            <td style="width: 3%">
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblTertiaryEmployerNameText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment3Address1" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblTertiaryEmployerAddressTitle" runat="server" Text="Address:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblTertiaryEmployerAddressText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment3Address2" runat="server">
                                            <td class="LeftLeftTitle">
                                                &nbsp;
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblTertiaryEmployerAddress2Text" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment3Address3" runat="server">
                                            <td class="LeftLeftTitle">
                                                &nbsp;
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblTertiaryEmployerCityProvincePostalText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment3Phone" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblTertiaryEmployerPhoneTitle" runat="server" Text="Telephone:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblTertiaryEmployerPhoneText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LeftSubTitle" colspan="3">
                                                <asp:Label ID="lblTertiaryEmployerProfileTitle" runat="server" Text="Employment Profile" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment3FudingSource" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblTertiaryEmployerFundingSourceTitle" runat="server" Text="Funding Source:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblTertiaryEmployerFundingSourceText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment3PracticeSetting" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblTertiaryEmployerPracticeSettingTitle" runat="server" Text="Practice Setting Type:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblTertiaryEmployerPracticeSettingText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment3MajorService" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblTertiaryEmployerMajorServiceTitle" runat="server" Text="Major Service Provided:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblTertiaryEmployerMajorServiceText" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="trEmployment3ClientAgeRange" runat="server">
                                            <td class="LeftLeftTitle">
                                                <asp:Label ID="lblTertiaryEmployerClientAgeRangeTitle" runat="server" Text="Client Age Range:" />
                                            </td>
                                            <td>
                                            </td>
                                            <td class="RightColumn">
                                                <asp:Label ID="lblTertiaryEmployerClientAgeRangeText" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr class="RowTitle2">
                            <td colspan="3">
                                <asp:Label ID="Label6" CssClass="heading2" runat="server" Text="PROFESSIONAL CORPORATIONS" />
                            </td>
                        </tr>
                        <tr id="trCorporationEmptyRow" runat="server">
                            <td class="LeftLeftTitle" colspan="3">
                                <span>None</span>
                            </td>
                        </tr>
                        <tr id="trCorporationNoEmptyRow" runat="server">
                            <td colspan="3">
                                <table width="100%">
                                    <tr>
                                        <td class="LeftLeftTitle" style="width: 40%; vertical-align: top">
                                            <asp:Label ID="lblCorpNameTitle" runat="server" Text="Corporation Name:" />
                                        </td>
                                        <td style="width: 3%">
                                        </td>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblCorpNameText" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="LeftLeftTitle" style="width: 40%">
                                            <asp:Label ID="lblCorpCertificateTitle" runat="server" Text="Certificate of Authorization #:" />
                                        </td>
                                        <td style="width: 3%">
                                        </td>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblCorpCertificateText" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="LeftLeftTitle" style="width: 40%">
                                            <asp:Label ID="lblCorpStatusTitle" runat="server" Text="Status:" />
                                        </td>
                                        <td style="width: 3%">
                                        </td>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblCorpStatusText" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="LeftLeftTitle" style="width: 40%">
                                            <asp:Label ID="lblCorpStatusEffectiveDateTitle" runat="server" Text="Status Effective Date:" />
                                        </td>
                                        <td style="width: 3%">
                                        </td>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblCorpStatusEffectiveDateText" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="LeftLeftTitle" style="width: 40%; vertical-align: top">
                                            <asp:Label ID="lblCorpPracticeNameTitle" runat="server" Text="Practice Name:" />
                                        </td>
                                        <td style="width: 3%">
                                        </td>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblCorpPracticeNameText" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="LeftLeftTitle" style="width: 40%; vertical-align: top">
                                            <asp:Label ID="lblCorpBusinessAddressTitle" runat="server" Text="Business Address:" />
                                        </td>
                                        <td style="width: 3%">
                                        </td>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblCorpBusinessAddressText" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="LeftLeftTitle" style="width: 40%">
                                            <asp:Label ID="lblCorpPhoneTitle" runat="server" Text="Telephone Number:" />
                                        </td>
                                        <td style="width: 3%">
                                        </td>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblCorpPhoneText" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="LeftLeftTitle" style="width: 40%">
                                            <asp:Label ID="lblCorpEmailTitle" runat="server" Text="Email Address:" />
                                        </td>
                                        <td style="width: 3%">
                                        </td>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblCorpEmailText" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="LeftLeftTitle" style="width: 40%; vertical-align: top">
                                            <asp:Label ID="lblCorpShareholdersTitle" runat="server" Text="Shareholder(s):" />
                                        </td>
                                        <td style="width: 3%">
                                        </td>
                                        <td class="RightColumn">
                                            <asp:Label ID="lblCorpShareholdersText" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:LinkButton ID="lbtnBackSearchResults" runat="server" Text="Back to Search Results" OnClick="lbtnBackSearchResultsClick" />&nbsp;&nbsp;
                    <asp:LinkButton ID="lbtnSearchAgain3" runat="server" Text="Search Again" OnClick="lbtnSearchAgainClick" />
                </td>
            </tr>
        </table>
    </center>
</div>
