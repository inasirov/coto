﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberInsurance.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlMemberInsurance" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>

<div class="MainForm">
    <center>
    <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:PostBackTrigger ControlID="ddlPlanHeldWith" />
            </Triggers>
            <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr class="HeaderTitle" align="right">
                <td>
                    <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 10 of 12" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <uc:MessageBox ID="omb" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <table class="memberInfo" width="100%" cellspacing="2" cellpadding="3">
                        <tr class="RowTitle">
                            <td colspan="2">
                                <asp:Label ID="lblInsuranceTitle" CssClass="heading"  runat="server" Text="Insurance" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblProfessionalLiabilityTitle" runat="server" Text="Professional Liability Insurance" Font-Bold="true"
                                    Font-Size="Medium" /><br /><br />
                                <asp:Label ID="lblProfessionalLiabilityDesc" runat="server" Text="To renew your certificate of registration you are required 
                                to hold professional liability insurance as prescribed by the College Bylaws, Section 20." />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width:50%; text-align: left;">
                                <asp:Label ID="lblPlanHeldWithTitle" CssClass="LeftTitle" runat="server" Text="Plan Held With*" />
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlPlanHeldWith" runat="server" OnSelectedIndexChanged="ddlPlanHeldWithSelectedIndexChanged" AutoPostBack="true" CausesValidation="true" />
                                <asp:RequiredFieldValidator ID="rfvPlanHeldWith" runat="server" ControlToValidate="ddlPlanHeldWith" EnableClientScript="false"
                                InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select a value" Display="Dynamic" ForeColor="Red" />
                            </td>
                        </tr>
                        <tr id="trOtherInsurance" runat="server" visible="false">
                            <td style="text-align: left; vertical-align: top;">
                                <asp:Label ID="lblOtherTitle" runat="server" CssClass="LeftTitle" Text="Other (name of insurance provider if applicable)" />
                            </td>
                            <td class="RightColumn">
                                <asp:TextBox ID="txtOther" runat="server" Columns="60" Width="240px" />
                                <asp:RequiredFieldValidator ID="rfvOther" runat="server" ControlToValidate="txtOther" EnableClientScript="false"
                                ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide a name of insurance provider" Display="Dynamic" ForeColor="Red" />
                                <br />
                                <asp:Label ID="lblSendFaxMessage" runat="server" Font-Bold="true" Text="Please fax a copy of your insurance policy to the College at 416-214-0851 for approval by June 1, 2015." />

                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                <asp:Label ID="lblExpiryDateTitle" runat="server" CssClass="LeftTitle" Text="Expiry Date (DD/MM/YYYY) *" />
                            </td>
                            <td class="RightColumn">
                                <asp:TextBox ID="txtExpiryDate" runat="server" />&nbsp;
                                <rjs:PopCalendar ID="PopCalendar1" runat="server" AutoPostBack="False" BlankFieldText="dd/MM/yyyy" Separator="/" Control="txtExpiryDate"
                                                                    MessageAlignment="RightCalendarControl" Culture="en-CA English (Canada)" From-Today="true" />
                                <asp:RequiredFieldValidator ID="rfvExpiryDate" runat="server" ControlToValidate="txtExpiryDate" EnableClientScript="false"
                                ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select Expiry Date" Display="Dynamic" ForeColor="Red" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                <asp:Label ID="lblCertificate" runat="server" CssClass="LeftTitle" Text="Certificate #*" />
                            </td>
                            <td class="RightColumn">
                                <asp:TextBox ID="txtCertificate" runat="server" MaxLength="20" />
                                <asp:RequiredFieldValidator ID="rfvCertificate" runat="server" ControlToValidate="txtCertificate" EnableClientScript="false"
                                ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide Certificate #" Display="Dynamic" ForeColor="Red" />
                                <%--<asp:RegularExpressionValidator ID="revCertificate" runat="server" ControlToValidate="txtCertificate" EnableClientScript="false" 
                                ValidationGroup="PersonalValidation" ErrorMessage="<br />Certificate # can have only 6 digits" ValidationExpression="^\d(\d)?(\d)?(\d)?(\d)?(\d)?$"  Display="Dynamic" ForeColor="Red" />--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                </td>
            </tr>
        </table>
        </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
