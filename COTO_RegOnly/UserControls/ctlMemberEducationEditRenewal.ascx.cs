﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Globalization;
using Classes;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberEducationEditRenewal : System.Web.UI.UserControl
    {
        #region Consts

        //private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;

        private string PrevStep = WebConfigItems.Step3;
        private string NextStep = WebConfigItems.Step5;
        private const int CurrentStep = 4;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Request.QueryString.Count > 0)
            //    securityCheck();

            if (Request.QueryString["common"] != null && Request.QueryString["common"] == "updated")
            {
                trEditConfirmation.Visible = true;
            }
            else
            {
                trEditConfirmation.Visible = false;
            }

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack)
            {
                SessionParameters.RenewalStep = CurrentStep;
                
                BindData();
            }
        }
        
        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
            
        }

        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            if (ValidationCheck())
            {
                UpdateUserEducation();
                UpdateSteps(1);
                Response.Redirect(NextStep);
            }
            else
            {
                if (Messages != null && Messages.Count > 0)
                {
                    string message = string.Empty;
                    foreach (var item in Messages)
                    {
                        message += ((PageMessage)item).Message + "<br />";
                    }
                    message = string.Format("<font color='Red'>{0}</font>", message);

                    Messages.Clear();
                    ShowErrorMessage(message);
                }
            }
        }

        //protected void ibCanadianUniversityClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("If you have graduated from a Canadian university that is not on the list, please select 'Other'" + 
        //    " and provide the details in the 'Other University (not listed above)' field. ", "University");
        //}

        //protected void ibOtherUniversityClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("Choose from this list if the university you attended is not in Canada. If you do not find your university" + 
        //        " on the list, please select 'Other' and provide the details in the 'Other University (not listed above)' field. ", "Help - University - Other");
        //}

        //protected void ibProvinceStateClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("If you have graduated from a program outside of North America, please select 'Other'.", "Help - University Province/State");
        //}

        //protected void ibtnFieldOfStudyHelpClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("If your field of study is not listed please choose 'Other field of study'.", "Help - Field of Study");
        //}
        
        //protected void ibtnCanadianUniversityOtherClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("If you have graduated from a Canadian university that is not on the list, please select 'Other' and provide the details in the 'Other University (not listed above)' field. ", "University");
        //}

        protected void ddlCountryGradEntrySelectIndexChanged(object sender, EventArgs e)
        {
            //RefreshOTEducationMessage();
            if (ddlCountryGradEntry.SelectedValue == "CAN")
            {
                ddlProvinceEntry.Visible = true;
                ddlProvinceEntry.Enabled = true;
                rfvProvinceEntry.Enabled = true;
                var repository = new Repository();
                var list1 = repository.GetCanadianProvincesListNew();

                list1.Insert(0, new GenClass { Code = "", Description = "" });

                ddlProvinceEntry.DataSource = list1;
                ddlProvinceEntry.DataValueField = "Code";
                ddlProvinceEntry.DataTextField = "Description";
                ddlProvinceEntry.DataBind();

                //txtProvinceEntry.Visible = false;
                //rfvProvinceEntry_2.Enabled = false;
            }
            if (ddlCountryGradEntry.SelectedValue == "USA")
            {
                ddlProvinceEntry.Visible = true;
                ddlProvinceEntry.Enabled = true;
                rfvProvinceEntry.Enabled = true;
                var repository = new Repository();
                var list1 = repository.GetUSStatesList();

                list1.Insert(0, new GenClass { Code = "", Description = "" });

                ddlProvinceEntry.DataSource = list1;
                ddlProvinceEntry.DataValueField = "Code";
                ddlProvinceEntry.DataTextField = "Description";
                ddlProvinceEntry.DataBind();

                //txtProvinceEntry.Visible = false;
                //rfvProvinceEntry_2.Enabled = false;
            }
            if (ddlCountryGradEntry.SelectedValue != "CAN" && ddlCountryGradEntry.SelectedValue != "USA")
            {
                ddlProvinceEntry.Visible = true;
                rfvProvinceEntry.Enabled = false;
                var repository = new Repository();
                var list1 = repository.GetGeneralList("PROVINCE_STATE");
                list1.Insert(0, new GenClass { Code = "", Description = "" });
                var item = list1.Find(I => I.Code == "98");
                ddlProvinceEntry.DataSource = list1;
                ddlProvinceEntry.DataValueField = "Code";
                ddlProvinceEntry.DataTextField = "Description";
                ddlProvinceEntry.DataBind();
                if (item != null)
                {
                    ddlProvinceEntry.SelectedValue = "98";
                }
                ddlProvinceEntry.Enabled = false;
                //txtProvinceEntry.Visible = true; txtProvinceEntry.Text = "Not Applicable"; txtProvinceEntry.Enabled = false;
                //rfvProvinceEntry_2.Enabled = false;
            }
        }

        protected void ddlCountryGradDegree2SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCountryGradDegree2.SelectedValue == "CAN")
            {
                ddlProvinceDegree2.Visible = true;
                ddlProvinceDegree2.Enabled = true;
                rfvProvinceDegree2.Enabled = true;
                var repository = new Repository();
                var list1 = repository.GetCanadianProvincesListNew();

                list1.Insert(0, new GenClass { Code = "", Description = "" });

                ddlProvinceDegree2.DataSource = list1;
                ddlProvinceDegree2.DataValueField = "Code";
                ddlProvinceDegree2.DataTextField = "Description";
                ddlProvinceDegree2.DataBind();

                //txtProvinceDegree2.Visible = false;
                //rfvProvinceDegree2_2.Enabled = false;
            }
            if (ddlCountryGradDegree2.SelectedValue == "USA")
            {
                ddlProvinceDegree2.Visible = true;
                ddlProvinceDegree2.Enabled = true;
                rfvProvinceDegree2.Enabled = true;
                var repository = new Repository();
                var list1 = repository.GetUSStatesList();

                list1.Insert(0, new GenClass { Code = "", Description = "" });

                ddlProvinceDegree2.DataSource = list1;
                ddlProvinceDegree2.DataValueField = "Code";
                ddlProvinceDegree2.DataTextField = "Description";
                ddlProvinceDegree2.DataBind();

                //txtProvinceDegree2.Visible = false;
                //rfvProvinceDegree2_2.Enabled = false;
            }
            if (ddlCountryGradDegree2.SelectedValue != "CAN" && ddlCountryGradDegree2.SelectedValue != "USA")
            {
                ddlProvinceDegree2.Visible = true;
                rfvProvinceDegree2.Enabled = false;
                var repository = new Repository();
                var list1 = repository.GetGeneralList("PROVINCE_STATE");
                list1.Insert(0, new GenClass { Code = "", Description = "" });
                var item = list1.Find(I => I.Code == "98");
                ddlProvinceDegree2.DataSource = list1;
                ddlProvinceDegree2.DataValueField = "Code";
                ddlProvinceDegree2.DataTextField = "Description";
                ddlProvinceDegree2.DataBind();
                if (item != null)
                {
                    ddlProvinceDegree2.SelectedValue = "98";
                }
                ddlProvinceDegree2.Enabled = false;
                //txtProvinceDegree2.Visible = true; txtProvinceDegree2.Text = "Not Applicable"; txtProvinceDegree2.Enabled = false;
                //rfvProvinceDegree2_2.Enabled = false;
            }
        }

        protected void ddlCountryGradDegree3SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCountryGradDegree3.SelectedValue == "CAN")
            {
                ddlProvinceDegree3.Visible = true;
                ddlProvinceDegree3.Enabled = true;
                rfvProvinceDegree3.Enabled = true;
                var repository = new Repository();
                var list1 = repository.GetCanadianProvincesListNew();

                list1.Insert(0, new GenClass { Code = "", Description = "" });

                ddlProvinceDegree3.DataSource = list1;
                ddlProvinceDegree3.DataValueField = "Code";
                ddlProvinceDegree3.DataTextField = "Description";
                ddlProvinceDegree3.DataBind();

                //txtProvinceDegree3.Visible = false;
                //rfvProvinceDegree3_2.Enabled = false;
            }
            if (ddlCountryGradDegree3.SelectedValue == "USA")
            {
                ddlProvinceDegree3.Visible = true;
                ddlProvinceDegree3.Enabled = true;
                rfvProvinceDegree3.Enabled = true;
                var repository = new Repository();
                var list1 = repository.GetUSStatesList();

                list1.Insert(0, new GenClass { Code = "", Description = "" });

                ddlProvinceDegree3.DataSource = list1;
                ddlProvinceDegree3.DataValueField = "Code";
                ddlProvinceDegree3.DataTextField = "Description";
                ddlProvinceDegree3.DataBind();

                //txtProvinceDegree3.Visible = false;
                //rfvProvinceDegree3_2.Enabled = false;
            }
            if (ddlCountryGradDegree3.SelectedValue != "CAN" && ddlCountryGradDegree3.SelectedValue != "USA")
            {
                ddlProvinceDegree3.Visible = true;
                rfvProvinceDegree3.Enabled = false;
                var repository = new Repository();
                var list1 = repository.GetGeneralList("PROVINCE_STATE");
                list1.Insert(0, new GenClass { Code = "", Description = "" });
                var item = list1.Find(I => I.Code == "98");
                ddlProvinceDegree3.DataSource = list1;
                ddlProvinceDegree3.DataValueField = "Code";
                ddlProvinceDegree3.DataTextField = "Description";
                ddlProvinceDegree3.DataBind();
                if (item != null)
                {
                    ddlProvinceDegree3.SelectedValue = "98";
                }
                ddlProvinceDegree3.Enabled = false;
                //txtProvinceDegree3.Visible = true; txtProvinceDegree3.Text = "Not Applicable"; txtProvinceDegree3.Enabled = false;
                //rfvProvinceDegree3_2.Enabled = false;
            }
        }

        protected void ddlOtherCountryDegree1SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOtherCountryDegree1.SelectedValue == "CAN")
            {
                ddlOtherProvinceDegree1.Visible = true;
                ddlOtherProvinceDegree1.Enabled = true;
                rfvOtherProvinceDegree1.Enabled = true;
                var repository = new Repository();
                var list1 = repository.GetCanadianProvincesListNew();

                list1.Insert(0, new GenClass { Code = "", Description = "" });

                ddlOtherProvinceDegree1.DataSource = list1;
                ddlOtherProvinceDegree1.DataValueField = "Code";
                ddlOtherProvinceDegree1.DataTextField = "Description";
                ddlOtherProvinceDegree1.DataBind();

                //txtOtherProvinceDegree1.Visible = false;
                //rfvOtherProvinceDegree1_2.Enabled = false;
            }
            if (ddlOtherCountryDegree1.SelectedValue == "USA")
            {
                ddlOtherProvinceDegree1.Visible = true;
                ddlOtherProvinceDegree1.Enabled = true;
                rfvOtherProvinceDegree1.Enabled = true;
                var repository = new Repository();
                var list1 = repository.GetUSStatesList();

                list1.Insert(0, new GenClass { Code = "", Description = "" });

                ddlOtherProvinceDegree1.DataSource = list1;
                ddlOtherProvinceDegree1.DataValueField = "Code";
                ddlOtherProvinceDegree1.DataTextField = "Description";
                ddlOtherProvinceDegree1.DataBind();

                //txtOtherProvinceDegree1.Visible = false;
                //rfvOtherProvinceDegree1_2.Enabled = false;
            }
            if (ddlOtherCountryDegree1.SelectedValue != "CAN" && ddlOtherCountryDegree1.SelectedValue != "USA")
            {
                ddlOtherProvinceDegree1.Visible = true;
                rfvOtherProvinceDegree1.Enabled = false;
                var repository = new Repository();
                var list1 = repository.GetGeneralList("PROVINCE_STATE");
                list1.Insert(0, new GenClass { Code = "", Description = "" });
                var item = list1.Find(I => I.Code == "98");
                ddlOtherProvinceDegree1.DataSource = list1;
                ddlOtherProvinceDegree1.DataValueField = "Code";
                ddlOtherProvinceDegree1.DataTextField = "Description";
                ddlOtherProvinceDegree1.DataBind();
                if (item != null)
                {
                    ddlOtherProvinceDegree1.SelectedValue = "98";
                }
                ddlOtherProvinceDegree1.Enabled = false;
                //txtOtherProvinceDegree1.Visible = true; txtOtherProvinceDegree1.Text = "Not Applicable"; txtOtherProvinceDegree1.Enabled = false;
                //rfvOtherProvinceDegree1_2.Enabled = false;
            }
        }

        protected void ddlOtherCountryDegree2SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOtherCountryDegree2.SelectedValue == "CAN")
            {
                ddlOtherProvinceDegree2.Visible = true;
                ddlOtherProvinceDegree2.Enabled = true;
                rfvOtherProvinceDegree2.Enabled = true;
                var repository = new Repository();
                var list1 = repository.GetCanadianProvincesListNew();

                list1.Insert(0, new GenClass { Code = "", Description = "" });

                ddlOtherProvinceDegree2.DataSource = list1;
                ddlOtherProvinceDegree2.DataValueField = "Code";
                ddlOtherProvinceDegree2.DataTextField = "Description";
                ddlOtherProvinceDegree2.DataBind();

                //txtOtherProvinceDegree2.Visible = false;
                //rfvOtherProvinceDegree2_2.Enabled = false;
            }
            if (ddlOtherCountryDegree2.SelectedValue == "USA")
            {
                ddlOtherProvinceDegree2.Visible = true;
                ddlOtherProvinceDegree2.Enabled = true;
                rfvOtherProvinceDegree2.Enabled = true;
                var repository = new Repository();
                var list1 = repository.GetUSStatesList();

                list1.Insert(0, new GenClass { Code = "", Description = "" });

                ddlOtherProvinceDegree2.DataSource = list1;
                ddlOtherProvinceDegree2.DataValueField = "Code";
                ddlOtherProvinceDegree2.DataTextField = "Description";
                ddlOtherProvinceDegree2.DataBind();

                //txtOtherProvinceDegree2.Visible = false;
                //rfvOtherProvinceDegree2_2.Enabled = false;
            }
            if (ddlOtherCountryDegree2.SelectedValue != "CAN" && ddlOtherCountryDegree2.SelectedValue != "USA")
            {
                ddlOtherProvinceDegree2.Visible = true;
                //ddlOtherProvinceDegree2.Enabled = false;
                rfvOtherProvinceDegree2.Enabled = false;
                var repository = new Repository();
                var list1 = repository.GetGeneralList("PROVINCE_STATE");
                list1.Insert(0, new GenClass { Code = "", Description = "" });
                var item = list1.Find(I => I.Code == "98");
                ddlOtherProvinceDegree2.DataSource = list1;
                ddlOtherProvinceDegree2.DataValueField = "Code";
                ddlOtherProvinceDegree2.DataTextField = "Description";
                ddlOtherProvinceDegree2.DataBind();
                if (item != null)
                {
                    ddlOtherProvinceDegree2.SelectedValue = "98";
                }
                ddlOtherProvinceDegree2.Enabled = false;
                //txtOtherProvinceDegree2.Visible = true; txtOtherProvinceDegree2.Text = "Not Applicable"; txtOtherProvinceDegree2.Enabled = false;
                //rfvOtherProvinceDegree2_2.Enabled = false;
            }
        }

        protected void ddlDiplomaEntrySelectedIndexChanged(object sender, EventArgs e)
        {
            ClearEntryDegree();
            ClearDegree2();
            ClearDegree3();

            if (!string.IsNullOrEmpty(ddlDiplomaEntry.SelectedValue))
            {
                EnableDisableEntryDegree(true);
                ddlDiplomaDegree2.Enabled = true;
            }
            else
            {
                EnableDisableEntryDegree(false);
                ddlDiplomaDegree2.Enabled = false;
            }
            ddlProvinceEntry.SelectedIndex = 0;
            ddlProvinceEntry.Enabled = false;
            rfvProvinceEntry.Enabled = false;
            //txtProvinceEntry.Visible = true;
            //rfvProvinceEntry_2.Enabled = true;
            trOtherUniversityNotListed.Visible = false;
            ddlDiplomaDegree2.SelectedIndex = 0;
            EnableDisableDegree2(false);
            ddlDiplomaDegree3.SelectedIndex = 0; ddlDiplomaDegree3.Enabled = false;
            EnableDisableDegree3(false);

        }

        protected void ddlDiplomaDegree2SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOtherUniversityEntry.SelectedItem != null && ddlOtherUniversityEntry.SelectedItem.Text.ToUpper() != "OTHER")
            {
                trOtherUniversityNotListed.Visible = false;
            }
            ClearDegree2();
            ClearDegree3();

            if (!string.IsNullOrEmpty(ddlDiplomaDegree2.SelectedValue))
            {
                EnableDisableDegree2(true);
                ddlDiplomaDegree3.Enabled = true;
            }
            else
            {
                EnableDisableDegree2(false);
                ddlDiplomaDegree3.Enabled = false;
            }
            ddlProvinceDegree2.SelectedIndex = 0;
            ddlProvinceDegree2.Enabled = false;
            rfvProvinceDegree2.Enabled = false;
            //txtProvinceDegree2.Visible = true;
            //rfvProvinceDegree2_2.Enabled = true;
            ddlDiplomaDegree3.SelectedIndex = 0;
            EnableDisableDegree3(false);
        }

        protected void ddlDiplomaDegree3SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((ddlOtherUniversityEntry.SelectedItem != null && ddlOtherUniversityEntry.SelectedItem.Text.ToUpper() != "OTHER") && (ddlOtherUniversityDegree2.SelectedItem != null && ddlOtherUniversityDegree2.SelectedItem.Text.ToUpper() != "OTHER"))
            {
                trOtherUniversityNotListed.Visible = false;
            }
            ClearDegree3();
            if (!string.IsNullOrEmpty(ddlDiplomaDegree3.SelectedValue))
            {
                EnableDisableDegree3(true);
            }
            else
            {
                EnableDisableDegree3(false);
            }
            ddlProvinceDegree3.SelectedIndex = 0;
            ddlProvinceDegree3.Enabled = false;
            rfvProvinceDegree3.Enabled = false;
            //txtProvinceDegree3.Visible = true;
            //rfvProvinceDegree3_2.Enabled = true;
        }

        protected void ddlOtherDiplomaDegree1SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearOtherDegree1();
            ClearOtherDegree2();

            if (!string.IsNullOrEmpty(ddlOtherDiplomaDegree1.SelectedValue))
            {
                EnableDisableOtherDegree1(true);
                ddlOtherDiplomaDegree2.Enabled = true;
            }
            else
            {
                EnableDisableOtherDegree1(false);
                ddlOtherDiplomaDegree2.Enabled = false;
            }
            ddlOtherProvinceDegree1.SelectedIndex = 0;
            ddlOtherProvinceDegree1.Enabled = false;
            rfvOtherProvinceDegree1.Enabled = false;
            //txtOtherProvinceDegree1.Visible = true;
            //rfvOtherProvinceDegree1_2.Enabled = true;
            //trOtherOtherUniversityNotListed.Visible = false;

            ddlOtherDiplomaDegree2.SelectedIndex = 0;
            EnableDisableOtherDegree2(false);
        }

        protected void ddlOtherDiplomaDegree2SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlOtherOtherUniversityDegree1.SelectedItem != null && ddlOtherOtherUniversityDegree1.SelectedItem.Text.ToUpper() != "OTHER")
            //{
            //    trOtherOtherUniversityNotListed.Visible = false;
            //}
            ClearOtherDegree2();

            if (!string.IsNullOrEmpty(ddlOtherDiplomaDegree2.SelectedValue))
            {
                EnableDisableOtherDegree2(true);
            }
            else
            {
                EnableDisableOtherDegree2(false);
            }
            ddlOtherProvinceDegree2.SelectedIndex = 0;
            ddlOtherProvinceDegree2.Enabled = false;
            rfvOtherProvinceDegree2.Enabled = false;
            //txtOtherProvinceDegree2.Visible = true;
            //rfvOtherProvinceDegree2_2.Enabled = true;
        }

        // old methods

        protected void ddlOtherUniversityDegree2SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (ddlOtherUniversityDegree2.SelectedValue.ToUpper() == "OTHER")
            {
                trOtherUniversityNotListed.Visible = true;
                txtOtherNotListedDegree2.Visible = true;
                rfvOtherNotListedDegree2.Enabled = true;
                //lblOtherNotListedDegree2.Visible = true;
            }
            else
            {
                rfvOtherNotListedDegree2.Enabled = false;
                if (!((ddlOtherUniversityDegree3.Visible && ddlOtherUniversityDegree3.SelectedValue.ToUpper() == "OTHER") || (lblOtherUniversityDegree3.Visible && lblOtherUniversityDegree3.Text.ToUpper() == "OTHER")))
                {
                    trOtherUniversityNotListed.Visible = false;
                    txtOtherNotListedDegree2.Text = string.Empty;
                    lblOtherNotListedDegree2.Text = string.Empty;
                    txtOtherNotListedDegree3.Text = string.Empty;
                    lblOtherNotListedDegree3.Text = string.Empty;
                }
                else
                {
                    txtOtherNotListedDegree2.Text = string.Empty;
                    lblOtherNotListedDegree2.Text = string.Empty;
                    txtOtherNotListedDegree2.Visible = false;
                }
            }
        }

        protected void ddlOtherUniversityDegree3SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOtherUniversityDegree3.SelectedValue.ToUpper() == "OTHER")
            {
                trOtherUniversityNotListed.Visible = true;
                txtOtherNotListedDegree3.Visible = true;
                rfvOtherNotListedDegree3.Enabled = true;
            }
            else
            {
                rfvOtherNotListedDegree3.Enabled = false;
                if (!((ddlOtherUniversityDegree2.Visible && ddlOtherUniversityDegree2.SelectedValue.ToUpper() == "OTHER") || (lblOtherUniversityDegree2.Visible && lblOtherUniversityDegree2.Text.ToUpper() == "OTHER")))
                {
                    trOtherUniversityNotListed.Visible = false;
                    txtOtherNotListedDegree2.Text = string.Empty;
                    lblOtherNotListedDegree2.Text = string.Empty;
                    txtOtherNotListedDegree3.Text = string.Empty;
                    lblOtherNotListedDegree3.Text = string.Empty;
                }
                else
                {
                    txtOtherNotListedDegree3.Text = string.Empty;
                    lblOtherNotListedDegree3.Text = string.Empty;
                    txtOtherNotListedDegree3.Visible = false;
                }
            }
        }

        protected void ddlOtherUniversityDegree3aSelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void ddlOtherOtherUniversityDegree1SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOtherOtherUniversityDegree1.SelectedValue.ToUpper() == "OTHER")
            {
                trOtherOtherUniversityNotListed.Visible = true;
                txtOtherOtherUniversityNotListedDegree1.Visible = true; rfvOtherOtherUniversityNotListedDegree1.Enabled = true;
                txtOtherOtherUniversityNotListedDegree1.Text = string.Empty;
            }
            else
            {
                txtOtherOtherUniversityNotListedDegree1.Visible = false; txtOtherOtherUniversityNotListedDegree1.Text = string.Empty;
                rfvOtherOtherUniversityNotListedDegree1.Enabled = false;
                if (!txtOtherOtherUniversityNotListedDegree2.Visible && !lblOtherOtherUniversityNotListedDegree2.Visible)
                {
                    trOtherOtherUniversityNotListed.Visible = false;
                }
            }
        }

        protected void ddlOtherOtherUniversityDegree2SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOtherOtherUniversityDegree2.SelectedValue.ToUpper() == "OTHER")
            {
                trOtherOtherUniversityNotListed.Visible = true;
                txtOtherOtherUniversityNotListedDegree2.Visible = true; txtOtherOtherUniversityNotListedDegree2.Text = string.Empty;
                rfvOtherOtherUniversityNotListedDegree2.Enabled = true;
            }
            else
            {
                txtOtherOtherUniversityNotListedDegree2.Text = string.Empty;
                txtOtherOtherUniversityNotListedDegree2.Visible = false; rfvOtherOtherUniversityNotListedDegree2.Enabled = false;
                //lblOtherOtherUniversityNotListedDegree2.Visible = false;
                if (!txtOtherOtherUniversityNotListedDegree1.Visible && !lblOtherOtherUniversityNotListedDegree1.Visible)
                {
                    trOtherOtherUniversityNotListed.Visible = false;
                }
            }
        }

        protected void lbtnUpdateEducationClick(object sender, EventArgs e)
        {
            Response.Redirect(WebConfigItems.Step3_UpdateEducationProfile);
        }

        protected void ddlCanadianUniversityEntryDegreeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCanadianUniversityEntryDegree.SelectedItem.Text) && ddlCanadianUniversityEntryDegree.SelectedItem.Text.ToUpper() != "OTHER")
            {
                ddlOtherUniversityEntry.SelectedIndex = 0;
                ddlOtherUniversityEntry.Visible = false;
                rfvOtherUniversityEntry.Enabled = false;
                lblOtherUniversityEntry.Text = string.Empty;

                if (!string.IsNullOrEmpty(ddlCanadianUniversityEntryDegree.SelectedItem.Text))
                {
                    var canadaItem = ddlCountryGradEntry.Items.FindByValue("CAN");
                    if (canadaItem != null)
                    {
                        ddlCountryGradEntry.SelectedValue = canadaItem.Value;
                        ddlCountryGradEntrySelectIndexChanged(null, null);
                    }
                }
                
            }
            else
            {
                ddlOtherUniversityEntry.Visible = true;
                rfvOtherUniversityEntry.Enabled = true;
            }
        }

        protected void ddlCanadianUniversityDegree2SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCanadianUniversityDegree2.SelectedItem.Text) && ddlCanadianUniversityDegree2.SelectedItem.Text.ToUpper() != "OTHER")
            {
                ddlOtherUniversityDegree2.SelectedIndex = 0;
                ddlOtherUniversityDegree2.Visible = false;
                rfvOtherUniversityDegree2.Enabled = false;
                lblOtherUniversityDegree2.Text = string.Empty;
                rfvOtherNotListedDegree2.Enabled = false;

                if (!string.IsNullOrEmpty(ddlCanadianUniversityDegree2.SelectedItem.Text))
                {
                    var canadaItem = ddlCountryGradDegree2.Items.FindByValue("CAN");
                    if (canadaItem != null)
                    {
                        ddlCountryGradDegree2.SelectedValue = canadaItem.Value;
                        ddlCountryGradDegree2SelectedIndexChanged(null, null);
                    }
                }
            }
            else
            {
                ddlOtherUniversityDegree2.Visible = true;
                ddlOtherUniversityDegree2.SelectedIndex = 0;
                rfvOtherUniversityDegree2.Enabled = true;
            }
        }

        protected void ddlCanadianUniversityDegree3SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCanadianUniversityDegree3.SelectedItem.Text) && ddlCanadianUniversityDegree3.SelectedItem.Text.ToUpper() != "OTHER")
            {
                ddlOtherUniversityDegree3.SelectedIndex = 0;
                ddlOtherUniversityDegree3.Visible = false;
                rfvOtherUniversityDegree3.Enabled = false;
                lblOtherUniversityDegree3.Text = string.Empty;
                rfvOtherNotListedDegree3.Enabled = false;

                if (!string.IsNullOrEmpty(ddlCanadianUniversityDegree3.SelectedItem.Text))
                {
                    var canadaItem = ddlCountryGradDegree3.Items.FindByValue("CAN");
                    if (canadaItem != null)
                    {
                        ddlCountryGradDegree3.SelectedValue = canadaItem.Value;
                        ddlCountryGradDegree3SelectedIndexChanged(null, null);
                    }
                }
            }
            else
            {
                ddlOtherUniversityDegree3.Visible = true;
                rfvOtherUniversityDegree3.Enabled = true;
                ddlOtherUniversityDegree3.SelectedIndex = 0;
            }
        }

        protected void ddlOtherCanadianUniversityDegree1SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(ddlOtherCanadianUniversityDegree1.SelectedItem.Text) && ddlOtherCanadianUniversityDegree1.SelectedItem.Text.ToUpper() != "OTHER")
            //if (ddlOtherCanadianUniversityDegree1.SelectedItem.Text.ToUpper() == "OTHER CANADIAN UNIVERSITY" || ddlOtherCanadianUniversityDegree1.SelectedItem.Text.ToUpper() == "OUT OF COUNTRY")
            if ((!string.IsNullOrEmpty(ddlOtherCanadianUniversityDegree1.SelectedItem.Text) && (ddlOtherCanadianUniversityDegree1.SelectedItem.Text.ToUpper().Contains("OTHER") || ddlOtherCanadianUniversityDegree1.SelectedItem.Text.ToUpper().Contains("COUNTRY") || ddlOtherCanadianUniversityDegree1.SelectedItem.Text.ToUpper().Contains("PRIVATE"))))
            {
                ddlOtherOtherUniversityDegree1.Enabled = true;
                rfvOtherOtherUniversityDegree1.Enabled = true;
            }
            else
            {
                ddlOtherOtherUniversityDegree1.SelectedIndex = 0;
                ddlOtherOtherUniversityDegree1.Enabled = false; rfvOtherOtherUniversityDegree1.Enabled = false;
                txtOtherOtherUniversityNotListedDegree1.Text = string.Empty; rfvOtherOtherUniversityNotListedDegree1.Enabled = false;
                if (!txtOtherOtherUniversityNotListedDegree2.Visible && !lblOtherOtherUniversityNotListedDegree2.Visible)
                {
                    trOtherOtherUniversityNotListed.Visible = false;
                }
                if (!string.IsNullOrEmpty(ddlOtherCanadianUniversityDegree1.SelectedItem.Text))
                {
                    var canadaItem = ddlOtherCountryDegree1.Items.FindByValue("CAN");
                    if (canadaItem!=null)
                    {
                        ddlOtherCountryDegree1.SelectedValue = canadaItem.Value;
                        ddlOtherCountryDegree1SelectedIndexChanged(null, null);
                    }
                }
            }
        }

        protected void ddlOtherCanadianUniversityDegree2SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlOtherCanadianUniversityDegree2.SelectedItem.Text) && (ddlOtherCanadianUniversityDegree2.SelectedItem.Text.ToUpper().Contains("OTHER") || ddlOtherCanadianUniversityDegree2.SelectedItem.Text.ToUpper().Contains("COUNTRY") || ddlOtherCanadianUniversityDegree2.SelectedItem.Text.ToUpper().Contains("PRIVATE")))
            {
                ddlOtherOtherUniversityDegree2.Enabled = true; rfvOtherOtherUniversityDegree2.Enabled = true;
            }
            else
            {
                ddlOtherOtherUniversityDegree2.SelectedIndex = 0;
                ddlOtherOtherUniversityDegree2.Enabled = false; rfvOtherOtherUniversityDegree2.Enabled = false;
                if (!lblOtherCanadianUniversityDegree1.Visible && !lblOtherOtherUniversityNotListedDegree1.Visible)
                {
                    trOtherOtherUniversityNotListed.Visible = false;
                }
                if (!string.IsNullOrEmpty(ddlOtherCanadianUniversityDegree2.SelectedItem.Text))
                {
                    var canadaItem = ddlOtherCountryDegree2.Items.FindByValue("CAN");
                    if (canadaItem != null)
                    {
                        ddlOtherCountryDegree2.SelectedValue = canadaItem.Value;
                        ddlOtherCountryDegree2SelectedIndexChanged(null, null);
                    }
                }
            }
        }

        #endregion

        #region Methods

        protected void EnableDisableEntryDegree(bool enabled)
        {
            ddlCanadianUniversityEntryDegree.Enabled = enabled;
            rfvCanadianUniversityEntryDegree.Enabled = enabled;
            ddlOtherUniversityEntry.Enabled = enabled;
            rfvOtherUniversityEntry.Enabled = enabled;
            //txtOtherNotListedEntryDegree.Enabled = enabled;
            //rfvOtherNotListedEntryDegree.Enabled = enabled;
            ddlCountryGradEntry.Enabled = enabled;
            rfvCountryGradEntry.Enabled = enabled;
            ddlProvinceEntry.Enabled = enabled;
            rfvProvinceEntry.Enabled = enabled;
            //txtProvinceEntry.Enabled = enabled;
            //rfvProvinceEntry_2.Enabled = enabled;
            ddlYearGradEntry.Enabled = enabled;
            rfvYearGradEntry.Enabled = enabled;
        }

        protected void EnableDisableDegree2(bool enabled)
        {
            //ddlDiplomaDegree2.Enabled = enabled;

            ddlCanadianUniversityDegree2.Enabled = enabled;
            rfvCanadianUniversityDegree2.Enabled = enabled;
            ddlOtherUniversityDegree2.Enabled = enabled;
            rfvOtherUniversityDegree2.Enabled = enabled;
            txtOtherNotListedDegree2.Enabled = enabled;
            rfvOtherNotListedDegree2.Enabled = enabled;
            ddlCountryGradDegree2.Enabled = enabled;
            rfvCountryGradDegree2.Enabled = enabled;
            ddlProvinceDegree2.Enabled = enabled;
            rfvProvinceDegree2.Enabled = enabled;
            //txtProvinceDegree2.Enabled = enabled;
            //rfvProvinceDegree2_2.Enabled = enabled;
            ddlYearGradDegree2.Enabled = enabled;
            rfvYearGradDegree2.Enabled = enabled;
        }

        protected void EnableDisableDegree3(bool enabled)
        {
            //ddlDiplomaDegree3.Enabled = enabled;

            ddlCanadianUniversityDegree3.Enabled = enabled;
            rfvCanadianUniversityDegree3.Enabled = enabled;
            ddlOtherUniversityDegree3.Enabled = enabled;
            rfvOtherUniversityDegree3.Enabled = enabled;
            txtOtherNotListedDegree3.Enabled = enabled;
            rfvOtherNotListedDegree3.Enabled = enabled;
            ddlCountryGradDegree3.Enabled = enabled;
            rfvCountryGradDegree3.Enabled = enabled;
            ddlProvinceDegree3.Enabled = enabled;
            rfvProvinceDegree3.Enabled = enabled;
            //txtProvinceDegree3.Enabled = enabled;
            //rfvProvinceDegree3_2.Enabled = enabled;
            ddlYearGradDegree3.Enabled = enabled;
            rfvYearGradDegree3.Enabled = enabled;
        }

        protected void EnableDisableOtherDegree1(bool enabled)
        {
            //ddlOtherDiplomaDegree1.Enabled = enabled;

            ddlOtherCanadianUniversityDegree1.Enabled = enabled;
            rfvOtherCanadianUniversityDegree1.Enabled = enabled;
            //ddlOtherOtherUniversityDegree1.Enabled = enabled;
            //rfvOtherOtherUniversityDegree1.Enabled = enabled;
            //txtOtherOtherUniversityNotListedDegree1.Enabled = enabled;
            //rfvOtherOtherUniversityNotListedDegree1.Enabled = enabled;
            ddlOtherFieldStudyDegree1.Enabled = enabled;
            rfvOtherFieldStudyDegree1.Enabled = enabled;
            ddlOtherYearGradDegree1.Enabled = enabled;
            rfvOtherYearGradDegree1.Enabled = enabled;
            ddlOtherCountryDegree1.Enabled = enabled;
            rfvOtherCountryDegree1.Enabled = enabled;
            ddlOtherProvinceDegree1.Enabled = enabled;
            rfvOtherProvinceDegree1.Enabled = enabled;
            //txtOtherProvinceDegree1.Enabled = enabled;
            //rfvOtherProvinceDegree1_2.Enabled = enabled;
        }

        protected void EnableDisableOtherDegree2(bool enabled)
        {
            //ddlOtherDiplomaDegree2.Enabled = enabled;

            ddlOtherCanadianUniversityDegree2.Enabled = enabled;
            rfvOtherCanadianUniversityDegree2.Enabled = enabled;
            //ddlOtherOtherUniversityDegree2.Enabled = enabled;
            //rfvOtherOtherUniversityDegree2.Enabled = enabled;
            //txtOtherOtherUniversityNotListedDegree2.Enabled = enabled;
            //rfvOtherOtherUniversityNotListedDegree2.Enabled = enabled;
            ddlOtherFieldStudyDegree2.Enabled = enabled;
            rfvOtherFieldStudyDegree2.Enabled = enabled;
            ddlOtherYearGradDegree2.Enabled = enabled;
            rfvOtherYearGradDegree2.Enabled = enabled;
            ddlOtherCountryDegree2.Enabled = enabled;
            rfvOtherCountryDegree2.Enabled = enabled;
            ddlOtherProvinceDegree2.Enabled = enabled;
            rfvOtherProvinceDegree2.Enabled = enabled;
            //txtOtherProvinceDegree2.Enabled = enabled;
            //rfvOtherProvinceDegree2_2.Enabled = enabled;
        }

        protected void ClearEntryDegree()
        {
            ddlCanadianUniversityEntryDegree.SelectedIndex = 0;
            ddlOtherUniversityEntry.Visible = true; ddlOtherUniversityEntry.SelectedIndex = 0;
            //txtOtherNotListedEntryDegree.Text = string.Empty;
            ddlCountryGradEntry.SelectedIndex = 0;
            if (ddlProvinceEntry.Items.Count > 0) ddlProvinceEntry.SelectedIndex = 0;
            //txtProvinceEntry.Text = string.Empty;
            ddlYearGradEntry.SelectedIndex = 0;
        }

        protected void ClearDegree2()
        {
            //ddlDiplomaDegree2.SelectedIndex = 0;
            ddlCanadianUniversityDegree2.SelectedIndex = 0;
            ddlOtherUniversityDegree2.Visible = true; ddlOtherUniversityDegree2.SelectedIndex = 0;
            txtOtherNotListedDegree2.Text = string.Empty;
            ddlCountryGradDegree2.SelectedIndex = 0;
            if (ddlProvinceDegree2.Items.Count > 0) ddlProvinceDegree2.SelectedIndex = 0;
            //txtProvinceDegree2.Text = string.Empty;
            ddlYearGradDegree2.SelectedIndex = 0;
        }

        protected void ClearDegree3()
        {
            //ddlDiplomaDegree3.SelectedIndex = 0;
            ddlCanadianUniversityDegree3.SelectedIndex = 0;
            ddlOtherUniversityDegree3.Visible = true; ddlOtherUniversityDegree3.SelectedIndex = 0;
            txtOtherNotListedDegree3.Text = string.Empty;
            ddlCountryGradDegree3.SelectedIndex = 0;
            if (ddlProvinceDegree3.Items.Count > 0) ddlProvinceDegree3.SelectedIndex = 0;
            //txtProvinceDegree3.Text = string.Empty;
            ddlYearGradDegree3.SelectedIndex = 0;
        }

        protected void ClearOtherDegree1()
        {
            //ddlOtherDiplomaDegree1.SelectedIndex = 0;
            ddlOtherCanadianUniversityDegree1.SelectedIndex = 0;
            ddlOtherOtherUniversityDegree1.SelectedIndex = 0; ddlOtherOtherUniversityDegree1.Enabled = false;
            txtOtherOtherUniversityNotListedDegree1.Text = string.Empty;
            txtOtherOtherUniversityNotListedDegree1.Visible = false;
            rfvOtherOtherUniversityNotListedDegree1.Enabled = false;
            trOtherOtherUniversityNotListed.Visible = false;
            

            ddlOtherFieldStudyDegree1.SelectedIndex = 0;
            ddlOtherYearGradDegree1.SelectedIndex = 0;
            ddlOtherCountryDegree1.SelectedIndex = 0;
            if (ddlOtherProvinceDegree1.Items.Count > 0) ddlOtherProvinceDegree1.SelectedIndex = 0;
            //txtOtherProvinceDegree1.Text = string.Empty;
        }

        protected void ClearOtherDegree2()
        {
            //ddlOtherDiplomaDegree2.SelectedIndex = 0;
            ddlOtherCanadianUniversityDegree2.SelectedIndex = 0;
            ddlOtherOtherUniversityDegree2.SelectedIndex = 0; ddlOtherOtherUniversityDegree2.Enabled = false;

            txtOtherOtherUniversityNotListedDegree2.Text = string.Empty;
            txtOtherOtherUniversityNotListedDegree2.Visible = false;
            rfvOtherOtherUniversityNotListedDegree2.Enabled = false;
            if (txtOtherOtherUniversityNotListedDegree1.Visible == false && lblOtherOtherUniversityNotListedDegree1.Visible == false) trOtherOtherUniversityNotListed.Visible = false;

            ddlOtherFieldStudyDegree2.SelectedIndex = 0;
            ddlOtherYearGradDegree2.SelectedIndex = 0;
            ddlOtherCountryDegree2.SelectedIndex = 0;
            if (ddlOtherProvinceDegree2.Items.Count > 0) ddlOtherProvinceDegree2.SelectedIndex = 0;
            //txtOtherProvinceDegree2.Text = string.Empty;
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        protected void ShowErrorMessage(string Message)
        {
            lblErrorMessages.Text = Message;
            update.Update();
            //lblErrorMessage.Text = Message;
            //update.Update();
            //modalPopupEx.Show();
        }

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetUserEducationInfo(CurrentUserId);
            if (user != null)
            {
                if (user.Education != null)
                {
                    EnableDisableEntryDegree(false);
                    EnableDisableDegree2(false); ddlDiplomaDegree2.Enabled = false;
                    EnableDisableDegree3(false); ddlDiplomaDegree3.Enabled = false;
                    EnableDisableOtherDegree1(false);
                    EnableDisableOtherDegree2(false); ddlOtherDiplomaDegree2.Enabled = false;

                    if (user.Education.EntryDegree != null)
                    {
                        if (!string.IsNullOrEmpty(user.Education.EntryDegree.DiplomaName.Trim()))
                        {
                            lblDiplomaEntry.Text = repository.GetGeneralName("DEGREE_OT_ENTRY", user.Education.EntryDegree.DiplomaName);
                            ddlDiplomaEntry.Visible = false;
                            //EnableDisableDegree2(true);
                            //EnableDisableEntryDegree(true);
                            ddlDiplomaDegree2.Enabled = true;
                        }
                        else
                        {
                            lblDiplomaEntry.Text = string.Empty; lblDiplomaEntry.Visible = false;
                            ddlDiplomaEntry.Visible = true;
                            var list = repository.GetGeneralList("DEGREE_OT_ENTRY");  //DEGREE
                            ddlDiplomaEntry.DataSource = list;
                            ddlDiplomaEntry.DataValueField = "CODE";
                            ddlDiplomaEntry.DataTextField = "DESCRIPTION";
                            ddlDiplomaEntry.DataBind();
                            ddlDiplomaEntry.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (!string.IsNullOrEmpty(user.Education.EntryDegree.CanadianUniversity.Trim()))
                        {
                            lblCanadianUniversityEntryDegree.Text = user.Education.EntryDegree.CanadianUniversity;
                            ddlCanadianUniversityEntryDegree.Visible = false;
                        }
                        else
                        {
                            lblCanadianUniversityEntryDegree.Text = string.Empty; lblCanadianUniversityEntryDegree.Visible = false;
                            ddlCanadianUniversityEntryDegree.Visible = true;
                            var list = repository.GetGeneralList("UNIV_OT");
                            ddlCanadianUniversityEntryDegree.DataSource = list;
                            ddlCanadianUniversityEntryDegree.DataValueField = "SUBSTITUTE";
                            ddlCanadianUniversityEntryDegree.DataTextField = "DESCRIPTION";
                            ddlCanadianUniversityEntryDegree.DataBind();
                            //ddlCanadianUniversityEntryDegree.Items.Insert(0, new ListItem { Value = "Other", Text = "Other" });
                            ddlCanadianUniversityEntryDegree.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (string.IsNullOrEmpty(user.Education.EntryDegree.CanadianUniversity.Trim()) || user.Education.EntryDegree.CanadianUniversity.ToUpper() == "OTHER")
                        {

                            if (!string.IsNullOrEmpty(user.Education.EntryDegree.OtherUniversity.Trim()))
                            {
                                lblOtherUniversityEntry.Text = user.Education.EntryDegree.OtherUniversity;
                                ddlOtherUniversityEntry.Visible = false;
                                rfvOtherUniversityEntry.Enabled = false;
                            }
                            else
                            {
                                lblOtherUniversityEntry.Text = string.Empty; lblOtherUniversityEntry.Visible = false;
                                ddlOtherUniversityEntry.Visible = true;
                                rfvOtherUniversityEntry.Enabled = true;
                                var list = repository.GetGeneralList("UNIV_INTL");
                                ddlOtherUniversityEntry.DataSource = list;
                                ddlOtherUniversityEntry.DataValueField = "CODE";
                                ddlOtherUniversityEntry.DataTextField = "DESCRIPTION";
                                ddlOtherUniversityEntry.DataBind();
                                ddlOtherUniversityEntry.Items.Insert(1, new ListItem { Value = "Other", Text = "Other" });
                                //ddlOtherUniversityEntry.Items.Insert(0, new ListItem { Value = "", Text = "" });
                            }
                        }
                        else
                        {
                            lblOtherUniversityEntry.Text = string.Empty;
                            ddlOtherUniversityEntry.Visible = false;
                            rfvOtherUniversityEntry.Enabled = false;
                        }

                        if (!string.IsNullOrEmpty(user.Education.EntryDegree.Country.Trim()))
                        {
                            lblCountryGradEntry.Text = repository.GetGeneralNameSubstImprv("COUNTRY", user.Education.EntryDegree.Country);
                            ddlCountryGradEntry.Visible = false;
                        }
                        else
                        {
                            lblCountryGradEntry.Text = string.Empty; lblCountryGradEntry.Visible = false;
                            ddlCountryGradEntry.Visible = true;
                            var list = repository.GetGeneralList("COUNTRY");
                            list = list.OrderBy(I => I.Description).ToList();
                            ddlCountryGradEntry.DataSource = list;
                            ddlCountryGradEntry.DataValueField = "Code";
                            ddlCountryGradEntry.DataTextField = "DESCRIPTION";
                            ddlCountryGradEntry.DataBind();
                            ddlCountryGradEntry.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (!string.IsNullOrEmpty(user.Education.EntryDegree.ProvinceState.Trim()))
                        {
                            lblProvinceEntry.Text = repository.GetGeneralNameImprv("PROVINCE_STATE", user.Education.EntryDegree.ProvinceState);
                            ddlProvinceEntry.Visible = false; rfvProvinceEntry.Enabled = false;
                            //txtProvinceEntry.Visible = false; rfvProvinceEntry_2.Enabled = false;
                        }
                        else
                        {
                            lblProvinceEntry.Text = string.Empty; lblProvinceEntry.Visible = false;
                            ddlProvinceEntry.Visible = true;
                            var list = repository.GetGeneralList("PROVINCE_STATE");
                            ddlProvinceEntry.DataSource = list;
                            ddlProvinceEntry.DataValueField = "CODE";
                            ddlProvinceEntry.DataTextField = "DESCRIPTION";
                            ddlProvinceEntry.DataBind();
                            ddlProvinceEntry.Items.Insert(0, new ListItem { Value = "", Text = "" });
                            ddlProvinceEntry.Enabled = false;
                        }

                        if (!string.IsNullOrEmpty(user.Education.EntryDegree.GraduationYear.Trim()))
                        {
                            lblYearGradEntry.Text = user.Education.EntryDegree.GraduationYear;
                            ddlYearGradEntry.Visible = false;
                        }
                        else
                        {
                            lblYearGradEntry.Text = string.Empty; lblYearGradEntry.Visible = false;
                            ddlYearGradEntry.Visible = true;
                            var list = repository.GetGeneralList("GRAD_YEAR");
                            list = list.Where(I => I.Code != "1_NA").ToList();
                            list = list.Where(I => int.Parse(I.Code) <= DateTime.Now.Year).OrderByDescending(I => I.Description).ToList();
                            ddlYearGradEntry.DataSource = list;
                            ddlYearGradEntry.DataValueField = "CODE";
                            ddlYearGradEntry.DataTextField = "DESCRIPTION";
                            ddlYearGradEntry.DataBind();
                            ddlYearGradEntry.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }
                    }

                    if (user.Education.Degree2 != null)
                    {
                        if (!string.IsNullOrEmpty(user.Education.Degree2.DiplomaName.Trim()))
                        {
                            lblDiplomaDegree2.Text = repository.GetGeneralName("DEGREE_OT_POST_ENTRY", user.Education.Degree2.DiplomaName); 
                            ddlDiplomaDegree2.Visible = false;
                            //EnableDisableDegree3(true);
                            ddlDiplomaDegree3.Enabled = true;
                        }
                        else
                        {
                            lblDiplomaDegree2.Text = string.Empty; lblDiplomaDegree2.Visible = false;
                            ddlDiplomaDegree2.Visible = true;
                            var list = repository.GetGeneralList("DEGREE_OT_POST_ENTRY"); // DEGREE
                            ddlDiplomaDegree2.DataSource = list;
                            ddlDiplomaDegree2.DataValueField = "CODE";
                            ddlDiplomaDegree2.DataTextField = "DESCRIPTION";
                            ddlDiplomaDegree2.DataBind();
                            ddlDiplomaDegree2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (!string.IsNullOrEmpty(user.Education.Degree2.CanadianUniversity.Trim()))
                        {
                            lblCanadianUniversityDegree2.Text = user.Education.Degree2.CanadianUniversity;
                            ddlCanadianUniversityDegree2.Visible = false;
                        }
                        else
                        {
                            lblCanadianUniversityDegree2.Text = string.Empty; lblCanadianUniversityDegree2.Visible = false;
                            ddlCanadianUniversityDegree2.Visible = true;
                            var list = repository.GetGeneralList("UNIV_OT");
                            ddlCanadianUniversityDegree2.DataSource = list;
                            ddlCanadianUniversityDegree2.DataValueField = "SUBSTITUTE";
                            ddlCanadianUniversityDegree2.DataTextField = "DESCRIPTION";
                            ddlCanadianUniversityDegree2.DataBind();
                            ddlCanadianUniversityDegree2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (string.IsNullOrEmpty(user.Education.Degree2.CanadianUniversity.Trim()) || user.Education.Degree2.CanadianUniversity.ToUpper() == "OTHER")
                        {
                            if (!string.IsNullOrEmpty(user.Education.Degree2.OtherUniversity.Trim()))
                            {
                                lblOtherUniversityDegree2.Text = user.Education.Degree2.OtherUniversity;
                                ddlOtherUniversityDegree2.Visible = false;
                                rfvOtherUniversityDegree2.Enabled = false;
                                if (user.Education.Degree2.OtherUniversity.ToUpper() == "OTHER")
                                {
                                    trOtherUniversityNotListed.Visible = true;
                                    //txtOtherNotListedDegree2.Visible = true;
                                    //txtOtherNotListedDegree2.Text = user.Education.Degree2.OtherUniversityNotListed;
                                    //if (!string.IsNullOrEmpty(txtOtherNotListedDegree2.Text))
                                    //{
                                    //    txtOtherNotListedDegree2.Enabled = false;
                                    //}

                                    if (!string.IsNullOrEmpty(user.Education.Degree2.OtherUniversityNotListed))
                                    {
                                        lblOtherNotListedDegree2.Text = user.Education.Degree2.OtherUniversityNotListed;
                                    }
                                    else
                                    {
                                        txtOtherNotListedDegree2.Visible = true;
                                    }
                                }
                            }
                            else
                            {
                                lblOtherUniversityDegree2.Text = string.Empty; lblOtherUniversityDegree2.Visible = false;
                                ddlOtherUniversityDegree2.Visible = true;
                                rfvOtherUniversityDegree2.Enabled = true;
                                var list = repository.GetGeneralList("UNIV_INTL");
                                ddlOtherUniversityDegree2.DataSource = list;
                                ddlOtherUniversityDegree2.DataValueField = "CODE";
                                ddlOtherUniversityDegree2.DataTextField = "DESCRIPTION";
                                ddlOtherUniversityDegree2.DataBind();
                                ddlOtherUniversityDegree2.Items.Insert(1, new ListItem { Value = "Other", Text = "Other" });
                                //ddlOtherUniversityDegree2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                            }
                        }
                        else
                        {
                            lblOtherUniversityDegree2.Text = string.Empty;
                            ddlOtherUniversityDegree2.Visible = false;
                            rfvOtherUniversityDegree2.Enabled = false;
                        }

                        if (!string.IsNullOrEmpty(user.Education.Degree2.Country.Trim()))
                        {
                            lblCountryGradDegree2.Text = repository.GetGeneralNameSubstImprv("COUNTRY", user.Education.Degree2.Country);
                            ddlCountryGradDegree2.Visible = false;
                        }
                        else
                        {
                            lblCountryGradDegree2.Text = string.Empty; lblCountryGradDegree2.Visible = false;
                            ddlCountryGradDegree2.Visible = true;
                            var list = repository.GetGeneralList("COUNTRY");
                            list = list.OrderBy(I => I.Description).ToList();
                            ddlCountryGradDegree2.DataSource = list;
                            ddlCountryGradDegree2.DataValueField = "Code";
                            ddlCountryGradDegree2.DataTextField = "DESCRIPTION";
                            ddlCountryGradDegree2.DataBind();
                            ddlCountryGradDegree2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (!string.IsNullOrEmpty(user.Education.Degree2.ProvinceState.Trim()))
                        {
                            lblProvinceDegree2.Text = repository.GetGeneralNameImprv("PROVINCE_STATE", user.Education.Degree2.ProvinceState);
                            ddlProvinceDegree2.Visible = false;
                        }
                        else
                        {
                            lblProvinceDegree2.Text = string.Empty; lblProvinceDegree2.Visible = false;
                            ddlProvinceDegree2.Visible = true;
                            var list = repository.GetGeneralList("PROVINCE_STATE");
                            ddlProvinceDegree2.DataSource = list;
                            ddlProvinceDegree2.DataValueField = "CODE";
                            ddlProvinceDegree2.DataTextField = "DESCRIPTION";
                            ddlProvinceDegree2.DataBind();
                            ddlProvinceDegree2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                            ddlProvinceDegree2.Enabled = false;
                        }

                        if (!string.IsNullOrEmpty(user.Education.Degree2.GraduationYear.Trim()))
                        {
                            lblYearGradDegree2.Text = user.Education.Degree2.GraduationYear;
                            ddlYearGradDegree2.Visible = false;
                        }
                        else
                        {
                            lblYearGradDegree2.Text = string.Empty; lblYearGradDegree2.Visible = false;
                            ddlYearGradDegree2.Visible = true;
                            var list = repository.GetGeneralList("GRAD_YEAR");
                            list = list.Where(I => I.Code != "1_NA").ToList();
                            list = list.Where(I => int.Parse(I.Code) <= DateTime.Now.Year).OrderByDescending(I => I.Description).ToList();
                            ddlYearGradDegree2.DataSource = list;
                            ddlYearGradDegree2.DataValueField = "CODE";
                            ddlYearGradDegree2.DataTextField = "DESCRIPTION";
                            ddlYearGradDegree2.DataBind();
                            ddlYearGradDegree2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }
                    }

                    if (user.Education.Degree3 != null)
                    {
                        if (!string.IsNullOrEmpty(user.Education.Degree3.DiplomaName.Trim()))
                        {
                            lblDiplomaDegree3.Text = repository.GetGeneralName("DEGREE_OT_POST_ENTRY", user.Education.Degree3.DiplomaName); 
                            ddlDiplomaDegree3.Visible = false;
                        }
                        else
                        {
                            lblDiplomaDegree3.Text = string.Empty; lblDiplomaDegree3.Visible = false;
                            ddlDiplomaDegree3.Visible = true;
                            var list = repository.GetGeneralList("DEGREE_OT_POST_ENTRY"); //DEGREE
                            ddlDiplomaDegree3.DataSource = list;
                            ddlDiplomaDegree3.DataValueField = "CODE";
                            ddlDiplomaDegree3.DataTextField = "DESCRIPTION";
                            ddlDiplomaDegree3.DataBind();
                            ddlDiplomaDegree3.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (!string.IsNullOrEmpty(user.Education.Degree3.CanadianUniversity.Trim()))
                        {
                            lblCanadianUniversityDegree3.Text = user.Education.Degree3.CanadianUniversity;
                            ddlCanadianUniversityDegree3.Visible = false;
                        }
                        else
                        {
                            lblCanadianUniversityDegree3.Text = string.Empty; lblCanadianUniversityDegree3.Visible = false;
                            ddlCanadianUniversityDegree3.Visible = true;
                            var list = repository.GetGeneralList("UNIV_OT");
                            ddlCanadianUniversityDegree3.DataSource = list;
                            ddlCanadianUniversityDegree3.DataValueField = "SUBSTITUTE";
                            ddlCanadianUniversityDegree3.DataTextField = "DESCRIPTION";
                            ddlCanadianUniversityDegree3.DataBind();
                            ddlCanadianUniversityDegree3.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (string.IsNullOrEmpty(user.Education.Degree3.CanadianUniversity.Trim()) || user.Education.Degree3.CanadianUniversity.ToUpper() == "OTHER")
                        {
                            if (!string.IsNullOrEmpty(user.Education.Degree3.OtherUniversity.Trim()))
                            {
                                lblOtherUniversityDegree3.Text = user.Education.Degree3.OtherUniversity;
                                ddlOtherUniversityDegree3.Visible = false; rfvOtherUniversityDegree3.Enabled = false;
                                if (user.Education.Degree3.OtherUniversity.ToUpper() == "OTHER")
                                {
                                    trOtherUniversityNotListed.Visible = true;
                                    //txtOtherNotListedDegree3.Visible = true;
                                    //txtOtherNotListedDegree3.Text = user.Education.Degree3.OtherUniversityNotListed;
                                    //if (!string.IsNullOrEmpty(txtOtherNotListedDegree3.Text))
                                    //{
                                    //    txtOtherNotListedDegree3.Enabled = false;
                                    //}
                                    if (!string.IsNullOrEmpty(user.Education.Degree3.OtherUniversityNotListed))
                                    {
                                        lblOtherNotListedDegree3.Text = user.Education.Degree3.OtherUniversityNotListed;
                                    }
                                    else
                                    {
                                        txtOtherNotListedDegree3.Visible = true; rfvOtherNotListedDegree3.Enabled = true;
                                    }
                                }
                            }
                            else
                            {
                                lblOtherUniversityDegree3.Text = string.Empty; lblOtherUniversityDegree3.Visible = false;
                                ddlOtherUniversityDegree3.Visible = true; rfvOtherUniversityDegree3.Enabled = true;
                                var list = repository.GetGeneralList("UNIV_INTL");
                                ddlOtherUniversityDegree3.DataSource = list;
                                ddlOtherUniversityDegree3.DataValueField = "CODE";
                                ddlOtherUniversityDegree3.DataTextField = "DESCRIPTION";
                                ddlOtherUniversityDegree3.DataBind();
                                ddlOtherUniversityDegree3.Items.Insert(1, new ListItem { Value = "Other", Text = "Other" });
                                //ddlOtherUniversityDegree3.Items.Insert(0, new ListItem { Value = "", Text = "" });
                            }
                        }
                        else
                        {
                            lblOtherUniversityDegree3.Text = string.Empty;
                            ddlOtherUniversityDegree3.Visible = false; rfvOtherUniversityDegree3.Enabled = false;
                        }

                        if (!string.IsNullOrEmpty(user.Education.Degree3.Country.Trim()))
                        {
                            lblCountryGradDegree3.Text = repository.GetGeneralNameSubstImprv("COUNTRY", user.Education.Degree3.Country);
                            ddlCountryGradDegree3.Visible = false;
                        }
                        else
                        {
                            lblCountryGradDegree3.Text = string.Empty; lblCountryGradDegree3.Visible = false;
                            ddlCountryGradDegree3.Visible = true;
                            var list = repository.GetGeneralList("COUNTRY");
                            list = list.OrderBy(I => I.Description).ToList();
                            ddlCountryGradDegree3.DataSource = list;
                            ddlCountryGradDegree3.DataValueField = "Code";
                            ddlCountryGradDegree3.DataTextField = "DESCRIPTION";
                            ddlCountryGradDegree3.DataBind();
                            ddlCountryGradDegree3.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (!string.IsNullOrEmpty(user.Education.Degree3.ProvinceState.Trim()))
                        {
                            lblProvinceDegree3.Text = repository.GetGeneralNameImprv("PROVINCE_STATE", user.Education.Degree3.ProvinceState);
                            ddlProvinceDegree3.Visible = false; rfvProvinceDegree3.Enabled = false;
                            //txtProvinceDegree3.Visible = false; rfvProvinceDegree3_2.Enabled = false;
                        }
                        else
                        {
                            lblProvinceDegree3.Text = string.Empty; lblProvinceDegree3.Visible = false;
                            ddlProvinceDegree3.Visible = true;
                            var list = repository.GetGeneralList("PROVINCE_STATE");
                            ddlProvinceDegree3.DataSource = list;
                            ddlProvinceDegree3.DataValueField = "CODE";
                            ddlProvinceDegree3.DataTextField = "DESCRIPTION";
                            ddlProvinceDegree3.DataBind();
                            ddlProvinceDegree3.Items.Insert(0, new ListItem { Value = "", Text = "" });
                            ddlProvinceDegree3.Enabled = false;
                        }

                        if (!string.IsNullOrEmpty(user.Education.Degree3.GraduationYear.Trim()))
                        {
                            lblYearGradDegree3.Text = user.Education.Degree3.GraduationYear;
                            ddlYearGradDegree3.Visible = false;
                        }
                        else
                        {
                            lblYearGradDegree3.Text = string.Empty; lblYearGradDegree3.Visible = false;
                            ddlYearGradDegree3.Visible = true;
                            var list = repository.GetGeneralList("GRAD_YEAR");
                            list = list.Where(I => I.Code != "1_NA").ToList();
                            list = list.Where(I => int.Parse(I.Code) <= DateTime.Now.Year).OrderByDescending(I => I.Description).ToList();
                            ddlYearGradDegree3.DataSource = list;
                            ddlYearGradDegree3.DataValueField = "CODE";
                            ddlYearGradDegree3.DataTextField = "DESCRIPTION";
                            ddlYearGradDegree3.DataBind();
                            ddlYearGradDegree3.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }
                    }

                    if (user.Education.OtherDegree1 != null)
                    {
                        if (!string.IsNullOrEmpty(user.Education.OtherDegree1.DiplomaName.Trim()))
                        {
                            lblOtherDiplomaDegree1.Text = repository.GetGeneralName("DEGREE_OTHER", user.Education.OtherDegree1.DiplomaName); 
                            ddlOtherDiplomaDegree1.Visible = false;
                            //EnableDisableOtherDegree2(true);
                            ddlOtherDiplomaDegree2.Enabled = true;
                        }
                        else
                        {
                            lblOtherDiplomaDegree1.Text = string.Empty; lblOtherDiplomaDegree1.Visible = false;
                            ddlOtherDiplomaDegree1.Visible = true;
                            var list = repository.GetGeneralList("DEGREE_OTHER");  //DEGREE
                            ddlOtherDiplomaDegree1.DataSource = list;
                            ddlOtherDiplomaDegree1.DataValueField = "CODE";
                            ddlOtherDiplomaDegree1.DataTextField = "DESCRIPTION";
                            ddlOtherDiplomaDegree1.DataBind();
                            ddlOtherDiplomaDegree1.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (!string.IsNullOrEmpty(user.Education.OtherDegree1.CanadianUniversity.Trim()))
                        {
                            lblOtherCanadianUniversityDegree1.Text = user.Education.OtherDegree1.CanadianUniversity;
                            ddlOtherCanadianUniversityDegree1.Visible = false;
                        }
                        else
                        {
                            lblOtherCanadianUniversityDegree1.Text = string.Empty; lblOtherCanadianUniversityDegree1.Visible = false;
                            ddlOtherCanadianUniversityDegree1.Visible = true;
                            var list = repository.GetGeneralList("UNIV_CDN");
                            ddlOtherCanadianUniversityDegree1.DataSource = list;
                            ddlOtherCanadianUniversityDegree1.DataValueField = "SUBSTITUTE";
                            ddlOtherCanadianUniversityDegree1.DataTextField = "SUBSTITUTE";
                            ddlOtherCanadianUniversityDegree1.DataBind();
                            ddlOtherCanadianUniversityDegree1.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (string.IsNullOrEmpty(user.Education.OtherDegree1.CanadianUniversity.Trim()) || user.Education.OtherDegree1.CanadianUniversity.ToUpper() == "OTHER")
                        {
                            if (!string.IsNullOrEmpty(user.Education.OtherDegree1.OtherUniversity.Trim()))
                            {
                                lblOtherOtherUniversityDegree1.Text = user.Education.OtherDegree1.OtherUniversity;
                                ddlOtherOtherUniversityDegree1.Visible = false; rfvOtherOtherUniversityDegree1.Enabled = false;
                                if (user.Education.OtherDegree1.OtherUniversity.ToUpper() == "OTHER")
                                {
                                    trOtherOtherUniversityNotListed.Visible = true;
                                    //txtOtherOtherUniversityNotListedDegree1.Visible = true;
                                    //txtOtherOtherUniversityNotListedDegree1.Text = user.Education.OtherDegree1.OtherUniversityNotListed;
                                    //if (!string.IsNullOrEmpty(txtOtherOtherUniversityNotListedDegree1.Text))
                                    //{
                                    //    txtOtherOtherUniversityNotListedDegree1.Enabled = false;
                                    //}
                                    if (!string.IsNullOrEmpty(user.Education.OtherDegree1.OtherUniversityNotListed))
                                    {
                                        lblOtherOtherUniversityNotListedDegree1.Text = user.Education.OtherDegree1.OtherUniversityNotListed;
                                        lblOtherOtherUniversityNotListedDegree1.Visible = true;
                                    }
                                    else
                                    {
                                        txtOtherOtherUniversityNotListedDegree1.Visible = true; rfvOtherOtherUniversityNotListedDegree1.Enabled = true;
                                    }
                                }
                            }
                            else
                            {
                                lblOtherOtherUniversityDegree1.Text = string.Empty; lblOtherOtherUniversityDegree1.Visible = false;
                                ddlOtherOtherUniversityDegree1.Visible = true; rfvOtherOtherUniversityDegree1.Enabled = true;
                                var list = repository.GetGeneralList("UNIV_INTL");
                                ddlOtherOtherUniversityDegree1.DataSource = list;
                                ddlOtherOtherUniversityDegree1.DataValueField = "CODE";
                                ddlOtherOtherUniversityDegree1.DataTextField = "DESCRIPTION";
                                ddlOtherOtherUniversityDegree1.DataBind();
                                ddlOtherOtherUniversityDegree1.Items.Insert(1, new ListItem { Value = "Other", Text = "Other" });
                                //ddlOtherOtherUniversityDegree1.Items.Insert(0, new ListItem { Value = "", Text = "" });
                            }
                        }
                        else
                        {
                            lblOtherOtherUniversityDegree1.Text = string.Empty;
                            ddlOtherOtherUniversityDegree1.Visible = false; rfvOtherOtherUniversityDegree1.Enabled = false;
                        }

                        if (!string.IsNullOrEmpty(user.Education.OtherDegree1.StudyField.Trim()))
                        {
                            lblOtherFieldStudyDegree1.Text = repository.GetGeneralNameImprv("FIELD_OF_STUDY", user.Education.OtherDegree1.StudyField);
                            ddlOtherFieldStudyDegree1.Visible = false;
                        }
                        else
                        {
                            lblOtherFieldStudyDegree1.Text = string.Empty; lblOtherFieldStudyDegree1.Visible = false;
                            ddlOtherFieldStudyDegree1.Visible = true;
                            var list = repository.GetGeneralList("FIELD_OF_STUDY");
                            ddlOtherFieldStudyDegree1.DataSource = list;
                            ddlOtherFieldStudyDegree1.DataValueField = "CODE";
                            ddlOtherFieldStudyDegree1.DataTextField = "DESCRIPTION";
                            ddlOtherFieldStudyDegree1.DataBind();
                            ddlOtherFieldStudyDegree1.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (!string.IsNullOrEmpty(user.Education.OtherDegree1.Country.Trim()))
                        {
                            lblOtherCountryDegree1.Text = repository.GetGeneralNameSubstImprv("COUNTRY", user.Education.OtherDegree1.Country);
                            ddlOtherCountryDegree1.Visible = false;
                        }
                        else
                        {
                            lblOtherCountryDegree1.Text = string.Empty; lblOtherCountryDegree1.Visible = false;
                            ddlOtherCountryDegree1.Visible = true;
                            var list = repository.GetGeneralList("COUNTRY");
                            list = list.OrderBy(I => I.Description).ToList();
                            ddlOtherCountryDegree1.DataSource = list;
                            ddlOtherCountryDegree1.DataValueField = "Code";
                            ddlOtherCountryDegree1.DataTextField = "DESCRIPTION";
                            ddlOtherCountryDegree1.DataBind();
                            ddlOtherCountryDegree1.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (!string.IsNullOrEmpty(user.Education.OtherDegree1.ProvinceState.Trim()))
                        {
                            lblOtherProvinceDegree1.Text = repository.GetGeneralNameImprv("PROVINCE_STATE", user.Education.OtherDegree1.ProvinceState);
                            ddlOtherProvinceDegree1.Visible = false; rfvOtherProvinceDegree1.Enabled = false;
                            //txtOtherProvinceDegree1.Visible = false; rfvOtherProvinceDegree1_2.Enabled = false;
                        }
                        else
                        {
                            lblOtherProvinceDegree1.Text = string.Empty; lblOtherProvinceDegree1.Visible = false;
                            ddlOtherProvinceDegree1.Visible = true;
                            var list = repository.GetGeneralList("PROVINCE_STATE");
                            ddlOtherProvinceDegree1.DataSource = list;
                            ddlOtherProvinceDegree1.DataValueField = "CODE";
                            ddlOtherProvinceDegree1.DataTextField = "DESCRIPTION";
                            ddlOtherProvinceDegree1.DataBind();
                            ddlOtherProvinceDegree1.Items.Insert(0, new ListItem { Value = "", Text = "" });
                            ddlOtherProvinceDegree1.Enabled = false;
                        }

                        if (!string.IsNullOrEmpty(user.Education.OtherDegree1.GraduationYear.Trim()))
                        {
                            lblOtherYearGradDegree1.Text = user.Education.OtherDegree1.GraduationYear;
                            ddlOtherYearGradDegree1.Visible = false;
                        }
                        else
                        {
                            lblOtherYearGradDegree1.Text = string.Empty; lblOtherYearGradDegree1.Visible = false;
                            ddlOtherYearGradDegree1.Visible = true;
                            var list = repository.GetGeneralList("GRAD_YEAR");
                            list = list.Where(I => I.Code != "1_NA").ToList();
                            list = list.Where(I => int.Parse(I.Code) <= DateTime.Now.Year).OrderByDescending(I => I.Description).ToList();
                            //list = list.Where(I => int.Parse(I.Code) <= DateTime.Now.Year).ToList();
                            ddlOtherYearGradDegree1.DataSource = list;
                            ddlOtherYearGradDegree1.DataValueField = "CODE";
                            ddlOtherYearGradDegree1.DataTextField = "DESCRIPTION";
                            ddlOtherYearGradDegree1.DataBind();
                            ddlOtherYearGradDegree1.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }
                    }

                    if (user.Education.OtherDegree2 != null)
                    {
                        if (!string.IsNullOrEmpty(user.Education.OtherDegree2.DiplomaName.Trim()))
                        {
                            lblOtherDiplomaDegree2.Text = repository.GetGeneralName("DEGREE_OTHER", user.Education.OtherDegree2.DiplomaName); 
                            ddlOtherDiplomaDegree2.Visible = false;
                        }
                        else
                        {
                            lblOtherDiplomaDegree2.Text = string.Empty; lblOtherDiplomaDegree2.Visible = false;
                            ddlOtherDiplomaDegree2.Visible = true;
                            var list = repository.GetGeneralList("DEGREE_OTHER"); //DEGREE
                            ddlOtherDiplomaDegree2.DataSource = list;
                            ddlOtherDiplomaDegree2.DataValueField = "CODE";
                            ddlOtherDiplomaDegree2.DataTextField = "DESCRIPTION";
                            ddlOtherDiplomaDegree2.DataBind();
                            ddlOtherDiplomaDegree2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (!string.IsNullOrEmpty(user.Education.OtherDegree2.CanadianUniversity.Trim()))
                        {
                            lblOtherCanadianUniversityDegree2.Text = user.Education.OtherDegree2.CanadianUniversity;
                            ddlOtherCanadianUniversityDegree2.Visible = false;
                        }
                        else
                        {
                            lblOtherCanadianUniversityDegree2.Text = string.Empty; lblOtherCanadianUniversityDegree2.Visible = false;
                            ddlOtherCanadianUniversityDegree2.Visible = true;
                            var list = repository.GetGeneralList("UNIV_CDN");
                            ddlOtherCanadianUniversityDegree2.DataSource = list;
                            ddlOtherCanadianUniversityDegree2.DataValueField = "SUBSTITUTE";
                            ddlOtherCanadianUniversityDegree2.DataTextField = "SUBSTITUTE";
                            ddlOtherCanadianUniversityDegree2.DataBind();
                            ddlOtherCanadianUniversityDegree2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (string.IsNullOrEmpty(user.Education.OtherDegree2.CanadianUniversity.Trim()) || user.Education.OtherDegree2.CanadianUniversity.ToUpper() == "OTHER")
                        {
                            if (!string.IsNullOrEmpty(user.Education.OtherDegree2.OtherUniversity.Trim()))
                            {
                                lblOtherOtherUniversityDegree2.Text = user.Education.OtherDegree2.OtherUniversity;
                                ddlOtherOtherUniversityDegree2.Visible = false; rfvOtherOtherUniversityDegree2.Enabled = false; 
                                if (user.Education.OtherDegree2.OtherUniversity.ToUpper() == "OTHER")
                                {
                                    trOtherOtherUniversityNotListed.Visible = true;
                                    //txtOtherOtherUniversityNotListedDegree2.Visible = true;
                                    //txtOtherOtherUniversityNotListedDegree2.Text = user.Education.OtherDegree2.OtherUniversityNotListed;
                                    //if (!string.IsNullOrEmpty(txtOtherOtherUniversityNotListedDegree2.Text))
                                    //{
                                    //    txtOtherOtherUniversityNotListedDegree2.Enabled = false;
                                    //}
                                    if (!string.IsNullOrEmpty(user.Education.OtherDegree2.OtherUniversityNotListed))
                                    {
                                        lblOtherOtherUniversityNotListedDegree2.Text = user.Education.OtherDegree2.OtherUniversityNotListed;
                                        lblOtherOtherUniversityNotListedDegree2.Visible = true;
                                    }
                                    else
                                    {
                                        txtOtherOtherUniversityNotListedDegree2.Visible = true; rfvOtherOtherUniversityNotListedDegree2.Enabled = true;
                                    }
                                }
                            }
                            else
                            {
                                lblOtherOtherUniversityDegree2.Text = string.Empty; lblOtherOtherUniversityDegree2.Visible = false;
                                ddlOtherOtherUniversityDegree2.Visible = true; rfvOtherOtherUniversityDegree2.Enabled = true;
                                var list = repository.GetGeneralList("UNIV_INTL");
                                ddlOtherOtherUniversityDegree2.DataSource = list;
                                ddlOtherOtherUniversityDegree2.DataValueField = "CODE";
                                ddlOtherOtherUniversityDegree2.DataTextField = "DESCRIPTION";
                                ddlOtherOtherUniversityDegree2.DataBind();
                                ddlOtherOtherUniversityDegree2.Items.Insert(1, new ListItem { Value = "Other", Text = "Other" });
                                //ddlOtherOtherUniversityDegree2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                            }
                        }
                        else
                        {
                            lblOtherOtherUniversityDegree2.Text = string.Empty;
                            ddlOtherOtherUniversityDegree2.Visible = false; rfvOtherOtherUniversityDegree2.Enabled = false;
                        }

                        if (!string.IsNullOrEmpty(user.Education.OtherDegree2.StudyField.Trim()))
                        {
                            lblOtherFieldStudyDegree2.Text = repository.GetGeneralNameImprv("FIELD_OF_STUDY", user.Education.OtherDegree2.StudyField);
                            ddlOtherFieldStudyDegree2.Visible = false;
                        }
                        else
                        {
                            lblOtherFieldStudyDegree2.Text = string.Empty; lblOtherFieldStudyDegree2.Visible = false;
                            ddlOtherFieldStudyDegree2.Visible = true;
                            var list = repository.GetGeneralList("FIELD_OF_STUDY");
                            ddlOtherFieldStudyDegree2.DataSource = list;
                            ddlOtherFieldStudyDegree2.DataValueField = "CODE";
                            ddlOtherFieldStudyDegree2.DataTextField = "DESCRIPTION";
                            ddlOtherFieldStudyDegree2.DataBind();
                            ddlOtherFieldStudyDegree2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (!string.IsNullOrEmpty(user.Education.OtherDegree2.Country.Trim()))
                        {
                            lblOtherCountryDegree2.Text = repository.GetGeneralNameSubstImprv("COUNTRY", user.Education.OtherDegree2.Country);
                            ddlOtherCountryDegree2.Visible = false;
                        }
                        else
                        {
                            lblOtherCountryDegree2.Text = string.Empty; lblOtherCountryDegree2.Visible = false;
                            ddlOtherCountryDegree2.Visible = true;
                            var list = repository.GetGeneralList("COUNTRY");
                            list = list.OrderBy(I => I.Description).ToList();
                            ddlOtherCountryDegree2.DataSource = list;
                            ddlOtherCountryDegree2.DataValueField = "Code";
                            ddlOtherCountryDegree2.DataTextField = "DESCRIPTION";
                            ddlOtherCountryDegree2.DataBind();
                            ddlOtherCountryDegree2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }

                        if (!string.IsNullOrEmpty(user.Education.OtherDegree2.ProvinceState.Trim()))
                        {
                            lblOtherProvinceDegree2.Text = repository.GetGeneralNameImprv("PROVINCE_STATE", user.Education.OtherDegree2.ProvinceState);
                            ddlOtherProvinceDegree2.Visible = false; rfvOtherProvinceDegree2.Enabled = false;
                            //txtOtherProvinceDegree2.Visible = false; rfvOtherProvinceDegree2_2.Enabled = false;
                        }
                        else
                        {
                            lblOtherProvinceDegree2.Text = string.Empty; lblOtherProvinceDegree2.Visible = false;
                            ddlOtherProvinceDegree2.Visible = true;
                            var list = repository.GetGeneralList("PROVINCE_STATE");
                            ddlOtherProvinceDegree2.DataSource = list;
                            ddlOtherProvinceDegree2.DataValueField = "CODE";
                            ddlOtherProvinceDegree2.DataTextField = "DESCRIPTION";
                            ddlOtherProvinceDegree2.DataBind();
                            ddlOtherProvinceDegree2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                            ddlOtherProvinceDegree2.Enabled = false;
                        }

                        if (!string.IsNullOrEmpty(user.Education.OtherDegree2.GraduationYear.Trim()))
                        {
                            lblOtherYearGradDegree2.Text = user.Education.OtherDegree2.GraduationYear;
                            ddlOtherYearGradDegree2.Visible = false;
                        }
                        else
                        {
                            lblOtherYearGradDegree2.Text = string.Empty; lblOtherYearGradDegree2.Visible = false;
                            ddlOtherYearGradDegree2.Visible = true;
                            var list = repository.GetGeneralList("GRAD_YEAR");
                            list = list.Where(I => I.Code != "1_NA").ToList();
                            list = list.Where(I => int.Parse(I.Code) <= DateTime.Now.Year).OrderByDescending(I => I.Description).ToList();
                            //list = list.Where(I => int.Parse(I.Code) <= DateTime.Now.Year).ToList();
                            ddlOtherYearGradDegree2.DataSource = list;
                            ddlOtherYearGradDegree2.DataValueField = "CODE";
                            ddlOtherYearGradDegree2.DataTextField = "DESCRIPTION";
                            ddlOtherYearGradDegree2.DataBind();
                            ddlOtherYearGradDegree2.Items.Insert(0, new ListItem { Value = "", Text = "" });
                        }
                    }
                }
            }
            var user2 = repository.GetUserInfo(CurrentUserId);
            if (user2 != null)
            {
                CurrentUser = user2;
                lblPersonalInformationSectionTitle.Text = "Education for " + CurrentUser.FullName;
            }
        }

        protected bool ValidationCheck()
        {
            bool validEducation = true;

            bool validateEntryDegree = false;
            bool validateDegree2 = false;
            bool validateDegree3 = false;
            bool validateOtherDegree1 = false;
            bool validateOtherDegree2 = false;

            validateEntryDegree = !string.IsNullOrEmpty(ddlDiplomaEntry.SelectedValue);
            validateDegree2 = !string.IsNullOrEmpty(ddlDiplomaDegree2.SelectedValue);
            validateDegree3 = !string.IsNullOrEmpty(ddlDiplomaDegree3.SelectedValue);
            validateOtherDegree1 = !string.IsNullOrEmpty(ddlOtherDiplomaDegree1.SelectedValue);
            validateOtherDegree2 = !string.IsNullOrEmpty(ddlOtherDiplomaDegree2.SelectedValue);

            if (validateEntryDegree)
            {
                Page.Validate("EntryDegreeValidation");
            }
            if (validateDegree2)
            {
                Page.Validate("Degree2Validation");
            }
            if (validateDegree3)
            {
                Page.Validate("Degree3Validation");
            }
            if (validateOtherDegree1)
            {
                Page.Validate("OtherDegree1Validation");
            }
            if (validateOtherDegree2)
            {
                Page.Validate("OtherDegree2Validation");
            }

            if (ddlDiplomaEntry.Visible && string.IsNullOrEmpty(ddlDiplomaEntry.SelectedValue))
            {
                AddMessage("Please provide all information of your entry degree", PageMessageType.UserError);
                validEducation = false;
            }

            int yearEntry = 0;
            int yearDegree2 = 0;
            int yearDegree3 = 0;
            int yearOtherDegree1 = 0;
            int yearOtherDegree2 = 0;

            if (validateDegree2 && ((validateEntryDegree && int.TryParse(ddlYearGradEntry.SelectedValue, out yearEntry)) || (!validateEntryDegree && int.TryParse(lblYearGradEntry.Text, out yearEntry))) && int.TryParse(ddlYearGradDegree2.SelectedValue, out yearDegree2))
            {
                if (yearEntry >= yearDegree2)
                {
                    AddMessage("Degree 2 year of graduation should be later than Entry degree year of graduation", PageMessageType.UserError);
                    validEducation = false;
                }
            }
            if (validateDegree3 && ((validateDegree2 && int.TryParse(ddlYearGradDegree2.SelectedValue, out yearDegree2)) || (!validateDegree2 && int.TryParse(lblYearGradDegree2.Text, out yearDegree2))) && int.TryParse(ddlYearGradDegree3.SelectedValue, out yearDegree3))
            {
                if (yearDegree2 >= yearDegree3)
                {
                    AddMessage("Degree 3 year of graduation should be later than Degree 2 year of graduation", PageMessageType.UserError);
                    validEducation = false;
                }
            }

            if (validateOtherDegree2 && ((validateOtherDegree1 && int.TryParse(ddlOtherYearGradDegree1.SelectedValue, out yearOtherDegree1)) || (!validateOtherDegree1 && int.TryParse(lblOtherYearGradDegree1.Text, out yearOtherDegree1))) && int.TryParse(ddlOtherYearGradDegree2.SelectedValue, out yearOtherDegree2))
            {
                if (yearOtherDegree1 >= yearOtherDegree2)
                {
                    AddMessage("Degree 2 year of graduation should be later than degree 1 year of graduation", PageMessageType.UserError);
                    validEducation = false;
                }
            }
            if (!Page.IsValid)
            {
                lblErrorMessages.Text = string.Empty;
                update.Update();
                validEducation = false;
            }
            return validEducation;
        }

        public void AddMessage(string message, PageMessageType messageType)
        {
            this.Messages.Add(new PageMessage(message, messageType));
        }

        protected void UpdateUserEducation()
        {
            User user = new User();
            user.Id = CurrentUserId;

            user.Education = new UserEducation();
            user.Education.EntryDegree = new Education();
            user.Education.Degree2 = new Education();
            user.Education.Degree3 = new Education();
            user.Education.OtherDegree1 = new Education();
            user.Education.OtherDegree2 = new Education();

            if (ddlDiplomaEntry.Visible && !string.IsNullOrEmpty(ddlDiplomaEntry.SelectedValue))
            {
                user.Education.EntryDegree.DiplomaName = ddlDiplomaEntry.SelectedValue;
            }

            if (ddlDiplomaDegree2.Visible && !string.IsNullOrEmpty(ddlDiplomaDegree2.SelectedValue))
            {
                user.Education.Degree2.DiplomaName = ddlDiplomaDegree2.SelectedValue;
            }

            if (ddlDiplomaDegree3.Visible && !string.IsNullOrEmpty(ddlDiplomaDegree3.SelectedValue))
            {
                user.Education.Degree3.DiplomaName = ddlDiplomaDegree3.SelectedValue;
            }

            if (ddlCanadianUniversityEntryDegree.Visible && !string.IsNullOrEmpty(ddlCanadianUniversityEntryDegree.SelectedValue))
            {
                user.Education.EntryDegree.CanadianUniversity = ddlCanadianUniversityEntryDegree.SelectedValue;
            }


            if (ddlCanadianUniversityDegree2.Visible && !string.IsNullOrEmpty(ddlCanadianUniversityDegree2.SelectedValue))
            {
                user.Education.Degree2.CanadianUniversity = ddlCanadianUniversityDegree2.SelectedValue;
            }

            if (ddlCanadianUniversityDegree3.Visible && !string.IsNullOrEmpty(ddlCanadianUniversityDegree3.SelectedValue))
            {
                user.Education.Degree3.CanadianUniversity = ddlCanadianUniversityDegree3.SelectedValue;
            }

            if (ddlOtherUniversityEntry.Visible && !string.IsNullOrEmpty(ddlOtherUniversityEntry.SelectedValue))
            {
                user.Education.EntryDegree.OtherUniversity = ddlOtherUniversityEntry.SelectedValue;
            }

            if (ddlOtherUniversityDegree2.Visible && !string.IsNullOrEmpty(ddlOtherUniversityDegree2.SelectedValue))
            {
                user.Education.Degree2.OtherUniversity = ddlOtherUniversityDegree2.SelectedValue;
            }

            if (ddlOtherUniversityDegree3.Visible && !string.IsNullOrEmpty(ddlOtherUniversityDegree3.SelectedValue))
            {
                user.Education.Degree3.OtherUniversity = ddlOtherUniversityDegree3.SelectedValue;
            }

            if (txtOtherNotListedDegree2.Visible && !string.IsNullOrEmpty(txtOtherNotListedDegree2.Text))
            {
                user.Education.Degree2.OtherUniversityNotListed = txtOtherNotListedDegree2.Text;
            }

            if (txtOtherNotListedDegree3.Visible && !string.IsNullOrEmpty(txtOtherNotListedDegree3.Text))
            {
                user.Education.Degree3.OtherUniversityNotListed = txtOtherNotListedDegree3.Text;
            }
            
            if (ddlProvinceEntry.Visible && !string.IsNullOrEmpty(ddlProvinceEntry.SelectedValue))
            {
                user.Education.EntryDegree.ProvinceState = ddlProvinceEntry.SelectedValue;
            }

            if (ddlProvinceDegree2.Visible && !string.IsNullOrEmpty(ddlProvinceDegree2.SelectedValue))
            {
                user.Education.Degree2.ProvinceState = ddlProvinceDegree2.SelectedValue;
            }

            if (ddlProvinceDegree3.Visible && !string.IsNullOrEmpty(ddlProvinceDegree3.SelectedValue))
            {
                user.Education.Degree3.ProvinceState = ddlProvinceDegree3.SelectedValue;
            }

            if (ddlCountryGradEntry.Visible && !string.IsNullOrEmpty(ddlCountryGradEntry.SelectedValue))
            {
                user.Education.EntryDegree.Country = ddlCountryGradEntry.SelectedValue;
            }

            if (ddlCountryGradDegree2.Visible && !string.IsNullOrEmpty(ddlCountryGradDegree2.SelectedValue))
            {
                user.Education.Degree2.Country = ddlCountryGradDegree2.SelectedValue;
            }

            if (ddlCountryGradDegree3.Visible && !string.IsNullOrEmpty(ddlCountryGradDegree3.SelectedValue))
            {
                user.Education.Degree3.Country = ddlCountryGradDegree3.SelectedValue;
            }

            if (ddlYearGradEntry.Visible && !string.IsNullOrEmpty(ddlYearGradEntry.SelectedValue))
            {
                user.Education.EntryDegree.GraduationYear = ddlYearGradEntry.SelectedValue;
            }

            if (ddlYearGradDegree2.Visible && !string.IsNullOrEmpty(ddlYearGradDegree2.SelectedValue))
            {
                user.Education.Degree2.GraduationYear = ddlYearGradDegree2.SelectedValue;
            }

            if (ddlYearGradDegree3.Visible && !string.IsNullOrEmpty(ddlYearGradDegree3.SelectedValue))
            {
                user.Education.Degree3.GraduationYear = ddlYearGradDegree3.SelectedValue;
            }

            /// ----------------

            if (ddlOtherDiplomaDegree1.Visible && !string.IsNullOrEmpty(ddlOtherDiplomaDegree1.SelectedValue))
            {
                user.Education.OtherDegree1.DiplomaName = ddlOtherDiplomaDegree1.SelectedValue;
            }

            if (ddlOtherDiplomaDegree2.Visible && !string.IsNullOrEmpty(ddlOtherDiplomaDegree2.SelectedValue))
            {
                user.Education.OtherDegree2.DiplomaName = ddlOtherDiplomaDegree2.SelectedValue;
            }

            if (ddlOtherCanadianUniversityDegree1.Visible && !string.IsNullOrEmpty(ddlOtherCanadianUniversityDegree1.SelectedValue))
            {
                user.Education.OtherDegree1.CanadianUniversity = ddlOtherCanadianUniversityDegree1.SelectedValue;
            }

            if (ddlOtherCanadianUniversityDegree2.Visible && !string.IsNullOrEmpty(ddlOtherCanadianUniversityDegree2.SelectedValue))
            {
                user.Education.OtherDegree2.CanadianUniversity = ddlOtherCanadianUniversityDegree2.SelectedValue;
            }

            if (ddlOtherOtherUniversityDegree1.Visible && !string.IsNullOrEmpty(ddlOtherOtherUniversityDegree1.SelectedValue))
            {
                user.Education.OtherDegree1.OtherUniversity = ddlOtherOtherUniversityDegree1.SelectedValue;
            }

            if (ddlOtherOtherUniversityDegree2.Visible && !string.IsNullOrEmpty(ddlOtherOtherUniversityDegree2.SelectedValue))
            {
                user.Education.OtherDegree2.OtherUniversity = ddlOtherOtherUniversityDegree2.SelectedValue;
            }

            if (txtOtherOtherUniversityNotListedDegree1.Visible && !string.IsNullOrEmpty(txtOtherOtherUniversityNotListedDegree1.Text))
            {
                user.Education.OtherDegree1.OtherUniversityNotListed = txtOtherOtherUniversityNotListedDegree1.Text;
            }

            if (txtOtherOtherUniversityNotListedDegree2.Visible && !string.IsNullOrEmpty(txtOtherOtherUniversityNotListedDegree2.Text))
            {
                user.Education.OtherDegree2.OtherUniversityNotListed = txtOtherOtherUniversityNotListedDegree2.Text;
            }


            if (ddlOtherFieldStudyDegree1.Visible && !string.IsNullOrEmpty(ddlOtherFieldStudyDegree1.SelectedValue))
            {
                user.Education.OtherDegree1.StudyField = ddlOtherFieldStudyDegree1.SelectedValue;
            }

            if (ddlOtherFieldStudyDegree2.Visible && !string.IsNullOrEmpty(ddlOtherFieldStudyDegree2.SelectedValue))
            {
                user.Education.OtherDegree2.StudyField = ddlOtherFieldStudyDegree2.SelectedValue;
            }

            if (ddlOtherProvinceDegree1.Visible && !string.IsNullOrEmpty(ddlOtherProvinceDegree1.SelectedValue))
            {
                user.Education.OtherDegree1.ProvinceState = ddlOtherProvinceDegree1.SelectedValue;
            }

            if (ddlOtherProvinceDegree2.Visible && !string.IsNullOrEmpty(ddlOtherProvinceDegree2.SelectedValue))
            {
                user.Education.OtherDegree2.ProvinceState = ddlOtherProvinceDegree2.SelectedValue;
            }

            if (ddlOtherCountryDegree1.Visible && !string.IsNullOrEmpty(ddlOtherCountryDegree1.SelectedValue))
            {
                user.Education.OtherDegree1.Country = ddlOtherCountryDegree1.SelectedValue;
            }

            if (ddlOtherCountryDegree2.Visible && !string.IsNullOrEmpty(ddlOtherCountryDegree2.SelectedValue))
            {
                user.Education.OtherDegree2.Country = ddlOtherCountryDegree2.SelectedValue;
            }

            if (ddlOtherYearGradDegree1.Visible && !string.IsNullOrEmpty(ddlOtherYearGradDegree1.SelectedValue))
            {
                user.Education.OtherDegree1.GraduationYear = ddlOtherYearGradDegree1.SelectedValue;
            }

            if (ddlOtherYearGradDegree2.Visible && !string.IsNullOrEmpty(ddlOtherYearGradDegree2.SelectedValue))
            {
                user.Education.OtherDegree2.GraduationYear = ddlOtherYearGradDegree2.SelectedValue;
            }

            /// ---------------
            var repository = new Repository();
            repository.UpdateUserEducationLogged( CurrentUserId, user);
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        /// <summary>
        ///  Messages collection. Contains all custom warning or errors which will be populated on the page.
        /// </summary>
        public PageMessages Messages
        {
            get
            {
                return (PageMessages)SessionParameters.PageMessages;
            }
            set
            {
                SessionParameters.PageMessages = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                if (SessionParameters.CurrentUser != null)
                {
                    return SessionParameters.CurrentUser;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUser = value;
            }
        }

        #endregion

       
    }
}