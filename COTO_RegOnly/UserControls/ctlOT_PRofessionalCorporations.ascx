﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlOT_PRofessionalCorporations.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlOT_PRofessionalCorporations" %>
<style type="text/css">
    thead
    {
        font-weight: bold;
    }
    
    /* Table hover style */
    div.AltRow tr th
    {
        background-color: #dadada;
        font-weight: bold;
    }
    
    /* Table odd rows style */
    div.AltRow tr:nth-child(odd)
    {
        background-color: #ffffff;
    }
    
    /* Table even rows style */
    div.AltRow tr:nth-child(even)
    {
        background-color: #efefef;
    }
    
    /* Table hover style */
    div.AltRow tr:hover
    {
        background-color: #dadada;
    }
</style>

<div class="MainForm">
    <center>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr class="RowTitle">
                <td>
                    <asp:Label ID="lblPageTitle" CssClass="heading" runat="server" Text="Professional Corporations" />
                </td>
            </tr>
            <tr id="trShowResults" runat="server">
                <td>
                    <table width="100%">
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblResultsMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <p>Click on any of the names below for more information on the Professional Corporation listed.</p>
                                <p>
                                    All Occupational Therapy Professional Corporations must hold an active Certificate of Authorization 
                                    (denoted by the Registration Number) with the College in order to practice the profession of OT 
                                    under the corporation. All shareholders of the corporation must hold an active certificate of 
                                    registration with the College. The certificate of authorization must be renewed yearly on 
                                    the anniversary of the status effective date. 
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListView ID="lvResults" runat="server" OnItemCommand="lvResultsCommand">
                                    <LayoutTemplate>
                                        <div class="AltRow">
                                            <table border="0" cellpadding="3" cellspacing="0" style="width: 100%; text-align: center;">
                                                <tr bgcolor="#EBEBEB">
                                                    <th style="text-align: left; width: 50%;">
                                                        <asp:Label ID="lblOccupationalTherapist" runat="server" Text="Professional Corporation" />
                                                    </th>
                                                    <th style="width: 25%; text-align: center;">
                                                        <asp:Label ID="lblRegistrationNumber" runat="server" Text="Registration Number" />
                                                    </th>
                                                    <th style="width: 25%; text-align: center;">
                                                        <asp:Label ID="lblStatus" runat="server" Text="Status" />
                                                    </th>
                                                </tr>
                                                <tr id="ItemPlaceHolder" runat="server">
                                                </tr>
                                            </table>
                                        </div>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <table border="0" cellpadding="3" cellspacing="0" style="width: 100%; text-align: center;">
                                            <tr bgcolor="#EBEBEB">
                                                <td style="text-align: left; width: 50%;">
                                                    <asp:Label ID="lblOccupationalTherapist" runat="server" Text="Professional Corporation" />
                                                </td>
                                                <td style="width: 25%; text-align: center;">
                                                    <asp:Label ID="lblRegistrationNumber" runat="server" Text="Registration Number" />
                                                </td>
                                                <td style="width: 25%; text-align: center;">
                                                    <asp:Label ID="lblStatus" runat="server" Text="Status" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td colspan="3">
                                                    <span>Empty List</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="text-align: left;">
                                                <asp:LinkButton ID="lbtnFullName" runat="server" Text='<%# (string)Eval("FullName") %>' CommandArgument='<%# (string)Eval("Id") %>' CommandName="View"/>
                                            </td>
                                            <td style="text-align: center;">
                                                <asp:Label ID="lblRegistrationnumber" runat="server" Text='<%# (string)Eval("Registration") %>' />
                                            </td>
                                            <td style="text-align: center;">
                                                <asp:Label ID="lblStatus" runat="server" Text='<%# (string)Eval("CurrentStatus") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:LinkButton ID="LinkButton1" runat="server" Text="Previous" Enabled="false" />&nbsp;•&nbsp;
                                <asp:LinkButton ID="LinkButton2" runat="server" Text="Search Again" OnClick="lbtnSearchAgainClick"/>&nbsp;•&nbsp;
                                <asp:LinkButton ID="LinkButton3" runat="server" Text="Next" Enabled="false" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trNoResults" runat="server">
                <td>
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <p>
                                    We are sorry but your search request did not return any matches. Please click "search
                                    again" and try broadening your search by using "all" in some of the areas of choice.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:LinkButton ID="lbtnPrevious3" runat="server" Text="Previous" Enabled="false" />&nbsp;•&nbsp;
                                <asp:LinkButton ID="lbtnSearchAgain3" runat="server" Text="Search Again" OnClick="lbtnSearchAgainClick" />&nbsp;•&nbsp;
                                <asp:LinkButton ID="lbtnNext3" runat="server" Text="Next" Enabled="false" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </center>
</div>
