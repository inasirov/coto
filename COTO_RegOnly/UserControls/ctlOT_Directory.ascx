﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ctlOT_Directory.ascx.cs"
    Inherits="WCM_iParts_ctlOT_Directory" %>
<style type="text/css">
    .memberInfo td select
    {
        padding: 0px !important;
        font-size: 12px !important;
    }
    .button
    {
        border: 1px solid #ccc;
        padding: 4px 4px 2px;
        color: #587390;
        text-decoration: none;
        font-size: 14px;
        line-height: 20px;
        background: #ddd;
        border-radius: 6px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        box-shadow: 1px 1px 2px rgba(0,0,0,.5);
        -webkit-box-shadow: 1px 1px 2px rgba(0,0,0,.5);
        -moz-box-shadow: 1px 1px 2px rgba(0,0,0,.5);
        text-shadow: #fff 0px 1px 1px;
        background: -webkit-gradient(linear, left top, left bottom, from(#eeeeee), to(#cccccc));
        background: -moz-linear-gradient(top,  #eeeeee,  #cccccc);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#eeeeee', endColorstr='#cccccc');
    }
    
    .button:active
    {
        box-shadow: 0px 0px 0px rgba(0,0,0,.5);
        -webkit-box-shadow: 0px 0px 0px rgba(0,0,0,.5);
        -moz-box-shadow: 0px 0px 0px rgba(0,0,0,.5);
        position: relative;
        top: 1px;
        left: 1px;
    }
    
    body
    {
        font: 12px/19px Arial,Verdana,Helvetica,sans-serif;
    }
    
    div .MainForm
    {
        background: url("/coto_test/images/member/section_bg.png") repeat scroll 0 0;
        border: 1px solid #F4F4F4;
        box-shadow: 0 0 10px #C4C4C4;
        margin: 10px auto 10px;
        overflow: hidden;
        padding: 15px 15px 25px 15px;
        width: 540px;
    }
    
    input.groovybutton
    {
        font-size: 14px;
        font-family: Arial,Helvetica,sans-serif;
        color: #FFFFFF;
        background-color: #808EAB;
        border-style: none;
        -moz-box-shadow: 1px 1px 2px #000000;
        -webkit-box-shadow: 1px 1px 2px #000000;
        box-shadow: 1px 1px 2px #000000;
    }
    input.groovybuttondisabled
    {
        font-size: 14px;
        font-family: Arial,Helvetica,sans-serif;
        color: InactiveCaptionText;
        background-color: #808EAB;
        border-style: none;
        -moz-box-shadow: 1px 1px 2px #000000;
        -webkit-box-shadow: 1px 1px 2px #000000;
        box-shadow: 1px 1px 2px #000000;
    }
    
    .HeaderTitle
    {
        color: Black;
        font-family: Arial,Helvetica,sans-serif;
        font-size: 12pt;
        font-weight: bold;
    }
    
    .RowTitle td
    {
        background-color: #3b5a6f;
        border-bottom: 2px none #FFFFFF;
        color: black;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16pt;
        padding: 5px 5px 5px 5px;
        text-align: left;
    }
    
    .RowTitle2 td
    {
        background-color: #3b5a6f;
        border-bottom: 2px none #FFFFFF;
        color: black;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14pt;
        padding: 3px 3px 3px 3px;
        text-align: left;
    }
    
    .heading
    {
        color: #FFFFFF;
        font-family: Arial, Verdana, Helvetica,sans-serif;
        font-size: 17px;
        font-weight: bold;
        letter-spacing: 0px;
        line-height: normal;
        font-style: normal;
        font-variant: normal;
    }
    
    .heading2
    {
        color: #FFFFFF;
        font-family: Arial, Verdana, Helvetica,sans-serif;
        font-size: 14px;
        font-weight: bold;
        letter-spacing: 0px;
        line-height: normal;
        font-style: normal;
        font-variant: normal;
    }
    
    .LeftTitle
    {
        font-family: Arial, Verdana, Helvetica,sans-serif;
        font-size: 9pt;
        font-weight: bold;
        text-align: right;
        vertical-align: top;
    }
    
    .LeftLeftTitle
    {
        font-family: Arial, Verdana, Helvetica,sans-serif;
        font-size: 9pt;
        font-weight: bold;
        text-align: left;
    }
    
    .LeftSubTitle
    {
        color: #3B5A6F;
        font-family: Arial, Verdana, Helvetica,sans-serif;
        font-size: 9pt;
        font-weight: bold;
        text-align: left;
    }
    
    
    .RightColumn
    {
        text-align: left;
        vertical-align: middle;
    }
    
    .memberInfo td
    {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 9pt;
        padding: 3px;
        border-spacing: 3px;
    }
    
    /*.memberInfo
{
    padding: 2px;
    border-spacing: 2px;
    font: Arial, Helvetica, sans-serif;
    font-size: 9pt;
    width: 100%;
}*/
    
    .TabHeader .ajax__tab_outer
    {
        height: 29px;
    }
    
    .TabHeader .ajax__tab_inner
    {
        padding-left: 3px;
    }
    
    .TabHeader .ajax__tab_body
    {
        font-family: Arial,Helvetica,sans-serif;
        font-size: 10pt;
        border: 1px solid #999999;
        border-top: 0;
        padding: 5px;
        background-color: #ffffff;
    }
    
    
    /*.NewsTab .ajax__tab_header
{
    color: #aa976b;
    font-size: 13px;
    font-weight: bold;
    margin-left: 10px;
}*/
    
    .NewsTab .ajax__tab_header
    {
        font-family: Arial,Helvetica,sans-serif;
        font-size: 13px;
        font-weight: bold;
        letter-spacing: 0.2em;
        line-height: 14px;
        display: block;
    }
    
    .NewsTab .ajax__tab_outer
    {
        border: 1px solid #999999;
    }
    .NewsTab .ajax__tab_inner
    {
        padding: 6px;
        margin-right: 1px;
        margin-left: 1px;
        margin-top: 1px;
        margin-bottom: 1px;
    }
    /* #f8f6ea; */
    .NewsTab .ajax__tab_active .ajax__tab_outer
    {
        -moz-box-shadow: 1px 0px 2px #000000;
        -webkit-box-shadow: 1px 0px 2px #000000;
        box-shadow: 1px 0px 2px #000000;
    }
    .NewsTab .ajax__tab_active .ajax__tab_inner
    {
        color: White;
        background-color: #3B5A6F;
    }
    
    .NewsTab .ajax__tab_active .ajax__tab_inner a:link, .NewsTab .ajax__tab_active .ajax__tab_inner a:visited
    {
        color: White; /*cursor: pointer;*/
    }
    
    .NewsTab .ajax__tab_body
    {
        padding: 10px 10px 10px 10px;
        border-style: solid;
        border-width: thin;
        -moz-box-shadow: 2px 2px 2px #000000;
        -webkit-box-shadow: 2px 2px 2px #000000;
        box-shadow: 2px 2px 2px #000000;
    }
    
    input.mapButton
    {
        font-size: 14px;
        font-family: Arial,Helvetica,sans-serif; /*color: White; */
        background-color: #808EAB;
        border-style: none;
        -moz-box-shadow: 1px 1px 1px #000000;
        -webkit-box-shadow: 1px 1px 1px #000000;
        box-shadow: 1px 1px 1px #000000;
    }
    
    .modalBackground
    {
        background-color: Gray;
        filter: alpha(opacity=80);
        opacity: 0.8;
    }
    
    .modalPopup
    {
        background-color: #ffffdd;
        border-width: 1px;
        -moz-border-radius: 5px;
        border-style: solid;
        border-color: Gray;
        min-width: 250px;
        max-width: 600px;
    }
    
    .topHandle
    {
        background-color: #97bae6;
        color: White;
        font-weight: bold;
    }
    
    .topHandleRed
    {
        background-color: Red; /* #97bae6;*/
        color: White;
        font-weight: bold;
    }
    
    #dvMessageText
    {
        max-width: 400px;
        max-height: 400px;
    }
    
    .memberInfo .RightColumn select, .memberInfo .RightColumn input, .memberInfo .RightColumn textarea
    {
        background-color: whitesmoke; /*#EDFAFF; #DCDCC2; ivory*/
        border-left: 1pt solid #C0C0C0;
        border-right: 1pt solid #C0C0C0;
        border-top: 1pt solid #C0C0C0;
        border-bottom: 1pt solid #C0C0C0;
        font: normal 13px Arial, Helvetica, sans-serif;
    }
    
    .copyright
    {
        font: normal 11px/19px Arial, Verdana, Helvetica, sans-serif;
        color: #585858;
    }
    
    a.copyrightLink
    {
        text-decoration: underline;
        font: 11px/19px Arial, Helvetica, sans-serif;
        color: #C3C2C2;
    }
    
    a.copyrightLink:visited
    {
        text-decoration: underline;
        font: 11px/19px Arial, Helvetica, sans-serif;
        color: #C3C2C2;
    }
    
    a.copyrightLink:hover
    {
        text-decoration: none;
        font: 11px/19px Arial, Helvetica, sans-serif;
        color: #C3C2C2;
    }
    
    a.copyrightLink:active
    {
        text-decoration: none;
        font: 11px/19px Arial, Helvetica, sans-serif;
        color: #C3C2C2;
    }
    
    .educationselect
    {
        width: 180px;
    }
    
    .educationselect2
    {
        width: 260px;
    }
    
    .ETD
    {
        background-color: #F0F0F0;
        font-family: Arial,Helvetica,sans-serif;
        font-size: 10pt;
        text-align: left;
    }
    
    .style1
    {
        height: 36px;
    }
</style>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnPublicRegisterDetailsDoc" />
            </Triggers>
            <ContentTemplate>
                <asp:Panel DefaultButton="btnSearch" runat="server">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class="RowTitle">
                            <td>
                                <asp:Label ID="lblPageTitle" CssClass="heading" runat="server" Text="OT Directory" />
                            </td>
                        </tr>
                        <%--<tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>--%>
                        <tr>
                            <td style="text-align: left">
                                <p>
                                    In accordance with Section 23 of the Health Professions Procedural Code, (Schedule
                                    2 of the <em>Regulated Health Professions Act, 1991</em>), the College is required
                                    to have a Public Register. All persons using the title of occupational therapist
                                    must hold an active registration certificate with the College. The Public Register
                                    contains all of the publicly available information of registered occupational therapists
                                    in Ontario.
                                </p>
                                <p>
                                    <b>How to use the OT Directory:</b><br />
                                    1. To confirm the registration status of your OT
                                    <br />
                                    2. To conduct a search for an OT in your area
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    Fill in one or more of the fields below. If your search does not pull up any results,
                                    try refining your search.
                                </p>
                                <p>
                                    In addition to information on individual registrants, the College is required to
                                    publish specific information related to Professional Corporations.<br />
                                    <b>To search Professional Corporations,</b>&nbsp;
                                    <asp:LinkButton ID="lbtnCorporationsSearch" runat="server" Text="click here" OnClick="lbtnCorporationsSearchClick" />
                                    <br />
                                    <br />
                                    <asp:LinkButton ID="lbtnPublicRegisterDetailsDoc" runat="server" Text="Click here"
                                        OnClick="lbtnPublicRegisterDetailsDocClick" />
                                    &nbsp;to review all the details of the information that is made available as public
                                    information.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="background-color: #d9d8b4;">
                                <table style="border: 1px solid #d9d8b4; padding: 5px; border-spacing: 1px; width: 100%"
                                    class="memberInfo">
                                    <tr>
                                        <td align="left" valign="top" style="background-color: #fbfbf7">
                                            <table width="100%" style="border: 1px solid #d9d8b4;">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <asp:Label ID="lblLastName" runat="server" Text="Last Name" />
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:TextBox ID="txtLastName" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <asp:Label ID="lblFirstName" runat="server" Text="First Name" />
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:TextBox ID="txtFirstName" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" class="style1">
                                                        <asp:Label ID="lblRegNumber" runat="server" Text="Registration #" />
                                                    </td>
                                                    <td align="left" valign="top" class="style1">
                                                        <asp:TextBox ID="txtRegNumber" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="middle" style="background-color: #ffffff">
                                            <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/Images/member/search.gif"
                                                OnClick="btnSearchClick" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <p>
                                    NOTE: The College provides the criteria to search an OT as shown below. The College
                                    does not endorse the services of any occupational therapist.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="5" cellspacing="1" width="100%">
                                    <tr>
                                        <td valign="top" align="left">
                                            <table border="0" cellpadding="0" cellspacing="10">
                                                <tr>
                                                    <td class="subheading" valign="top" align="left">
                                                        Advanced Search:
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="1" cellpadding="5" style="border: 1px solid #d9d8b4;"
                                    class="memberInfo">
                                    <tr>
                                        <td align="left" valign="top" style="background-color: #fbfbf7">
                                            <table style="width: 100%; border: 1px solid #d9d8b4;">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <asp:Label ID="lblCity" runat="server" Text="City/Town" />
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:TextBox ID="txtCity" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <asp:Label ID="lblPostalCode" runat="server" Text="Postal Code" />
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:TextBox ID="txtPostalCode" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <asp:Label ID="lblLanguage" runat="server" Text="Language" />
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:DropDownList ID="ddlLanguage" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <asp:Label ID="lblFundingSource" runat="server" Text="Funding Source" />
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:DropDownList ID="ddlFundingSource" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <asp:Label ID="lblMajorService" runat="server" Text="Major Service Provided" />
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:DropDownList ID="ddlMajorService" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <asp:Label ID="lblAgeRange" runat="server" Text="Age Range of Clients Served " />
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:DropDownList ID="ddlAgeRange" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <asp:Label ID="lblPracticeSetting" runat="server" Text="Practice Setting" />
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:DropDownList ID="ddlPracticeSetting" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="middle" style="background-color: #ffffff">
                                            <asp:ImageButton ID="btnSearch2" runat="server" ImageUrl="~/Images/member/search.gif"
                                                OnClick="btnSearchClick" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
