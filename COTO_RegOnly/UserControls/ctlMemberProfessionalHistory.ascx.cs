﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;
using Classes;
using COTO_RegOnly.Classes;


namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberProfessionalHistory : System.Web.UI.UserControl
    {

        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;
        private string PrevStep = WebConfigItems.Step6;
        private string NextStep = WebConfigItems.Step8;

        private const int CurrentStep = 7;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.RenewalStep = CurrentStep;
                BindLists();
                BindData();
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            if (ValidationCheck())
            {
                UpdateUserProfessionalHistory();
                //Response.Redirect("MemberDisplay.aspx");
                //string fullName = string.Empty;
                //if (CurrentUser != null)
                //{
                //    fullName = CurrentUser.FullName;
                //}
                UpdateSteps(1);
                Session["CustomAction"] = "renewal";
                Response.Redirect(NextStep); // + "&ID=" + CurrentUserId + "&FULL_NAME=" + HttpUtility.UrlEncode(fullName) + "&CustomAction=renewal");
                
                //UpdateSteps(-1);
                //Response.Redirect(PrevStep); // + "&ID=" + CurrentUserId + "&FULL_NAME=" + HttpUtility.UrlEncode(fullName) + "&CustomAction=renewal");
            }
            else
            {
                if (Messages != null && Messages.Count > 0)
                {
                    string message = string.Empty;
                    foreach (var item in Messages)
                    {
                        message += ((PageMessage)item).Message + "<br />";
                    }
                    message = string.Format("<font color='Red'>{0}</font>", message);

                    Messages.Clear();
                    ShowErrorMessage(message);
                }
            }
        }

        protected void PopCalendar1_SelectionChanged(object sender, EventArgs e)
        {
            
        }

        protected void cbNoExpiration1CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoExpiration1.Checked) txtExpiryDate1.Text = string.Empty;
        }

        protected void cbNoExpiration2CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoExpiration2.Checked) txtExpiryDate2.Text = string.Empty;
        }

        protected void cbNoExpiration3CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoExpiration3.Checked) txtExpiryDate3.Text = string.Empty;
        }

        protected void cbNoExpiration4CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoExpiration4.Checked) txtExpiryDate4.Text = string.Empty;
        }

        protected void cbNoExpirationOP1CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoExpirationOP1.Checked) txtExpireDateOP1.Text = string.Empty;
        }

        protected void cbNoExpirationOP2CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoExpirationOP2.Checked) txtExpireDateOP2.Text = string.Empty;
        }

        protected void ibRegPracticeOtherProvinceHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("Membership information with professional associations, (CAOT, OSOT,AOTA, AIOTA) is not required. ", "Help - Other Province");
        }

        protected void ibRegisterOtherProfessionHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("Only include those professions where you are required, by law, to hold a registration / license to practice that profession.", "Help - Other Profession Province");
        }

        protected void ibRegulatoryBodyHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("If the Regulatory Body is not listed, please select 'Other' from the bottom of the dropdown menu, and enter the name of the Regulatory body in the second field titled 'Regulatory Body Other'", "Help - Regulatory Body");
        }

        protected void ibProvinceStateHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("If the province/state is not listed, please select '1_Not Applicable' from the top of the dropdown menu.", "Help - Province / State");
        }

        protected void ddlRegisteredPracticeOtherProvinceSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRegisteredPracticeOtherProvince.SelectedValue.ToUpper() == "YES")
            {
                DisableEnableJurisdiction1Controls(true);
                if (!string.IsNullOrEmpty(ddlRegulatoryBody1.SelectedValue))
                {
                    DisableEnableJurisdiction2Controls(true);
                }
                if (!string.IsNullOrEmpty(ddlRegulatoryBody2.SelectedValue))
                {
                    DisableEnableJurisdiction3Controls(true);
                }
                if (!string.IsNullOrEmpty(ddlRegulatoryBody3.SelectedValue))
                {
                    DisableEnableJurisdiction4Controls(true);
                }
                //DisableEnableJurisdiction2Controls(true);
                //DisableEnableJurisdiction3Controls(true);
                //DisableEnableJurisdiction4Controls(true);
            }
            else         //if (ddlRegisteredPracticeOtherProvince.SelectedValue.ToUpper() =="NO")
            {
                ClearJurisdict1Fields();
                DisableEnableJurisdiction1Controls(false);
                ClearJurisdict2Fields();
                DisableEnableJurisdiction2Controls(false);
                ClearJurisdict3Fields();
                DisableEnableJurisdiction3Controls(false);
                ClearJurisdict4Fields();
                DisableEnableJurisdiction4Controls(false);
            }
            
        }

        protected void ddlOtherProfessionSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOtherProfession.SelectedValue.ToUpper() == "YES")
            {
                DisableEnableProfession1Controls(true);
                DisableEnableProfession2Controls(true);
            }
            else       
            {
                ClearProf1Fields();
                DisableEnableProfession1Controls(false);
                ClearProf2Fields();
                DisableEnableProfession2Controls(false);
            }
            
        }

        protected void ddlRegulatoryBody1SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlRegulatoryBody1.SelectedValue))
            {
                DisableEnableJurisdiction2Controls(true);
                if (ddlRegulatoryBody1.SelectedItem.Text.ToUpper() == "OTHER")
                {
                    txtRegulatoryBodyOther1.ReadOnly = false;
                }
                else
                {
                    txtRegulatoryBodyOther1.Text = string.Empty;
                    txtRegulatoryBodyOther1.ReadOnly = true;
                }
            }
            else
            {
                ClearJurisdict1Fields();
                ClearJurisdict2Fields();
                DisableEnableJurisdiction2Controls(false);
                ClearJurisdict3Fields();
                DisableEnableJurisdiction3Controls(false);
                ClearJurisdict4Fields();
                DisableEnableJurisdiction4Controls(false);
            }
        }

        protected void ddlRegulatoryBody2SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlRegulatoryBody2.SelectedValue))
            {
                DisableEnableJurisdiction3Controls(true);
                if (ddlRegulatoryBody2.SelectedItem.Text.ToUpper() == "OTHER")
                {
                    txtRegulatoryBodyOther2.ReadOnly = false;
                }
                else
                {
                    txtRegulatoryBodyOther2.Text = string.Empty;
                    txtRegulatoryBodyOther2.ReadOnly = true;
                }
            }
            else
            {
                ClearJurisdict2Fields();
                ClearJurisdict3Fields();
                DisableEnableJurisdiction3Controls(false);
                ClearJurisdict4Fields();
                DisableEnableJurisdiction4Controls(false);
            }
        }

        protected void ddlRegulatoryBody3SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlRegulatoryBody3.SelectedValue))
            {
                DisableEnableJurisdiction4Controls(true);
                if (ddlRegulatoryBody3.SelectedItem.Text.ToUpper() == "OTHER")
                {
                    txtRegulatoryBodyOther3.ReadOnly = false;
                }
                else
                {
                    txtRegulatoryBodyOther3.Text = string.Empty;
                    txtRegulatoryBodyOther3.ReadOnly = true;
                }
            }
            else
            {
                ClearJurisdict3Fields();
                ClearJurisdict4Fields();
                DisableEnableJurisdiction4Controls(false);
            }
        }

        protected void ddlRegulatoryBody4SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ddlRegulatoryBody4.SelectedValue))
            {
                ClearJurisdict4Fields();
                if (ddlRegulatoryBody4.SelectedItem.Text.ToUpper() == "OTHER")
                {
                    txtRegulatoryBodyOther4.ReadOnly = false;
                }
                else
                {
                    txtRegulatoryBodyOther4.Text = string.Empty;
                    txtRegulatoryBodyOther4.ReadOnly = true;
                }
            }
        }
        
        #endregion

        #region Methods

        protected void ClearProf1Fields()
        {
            txtNameProfessionOP1.Text = string.Empty;
            txtRegulatoryBodyOP1.Text = string.Empty;
            ddlProvinceOP1.SelectedIndex = 0;
            ddlCountryOP1.SelectedIndex = 0;
            txtLicenseOP1.Text = string.Empty;
            txtExpireDateOP1.Text = string.Empty;
        }

        protected void ClearProf2Fields()
        {
            txtNameProfessionOP2.Text = string.Empty;
            txtRegulatoryBodyOP2.Text = string.Empty;
            ddlProvinceOP2.SelectedIndex = 0;
            ddlCountryOP2.SelectedIndex = 0;
            txtLicenseOP2.Text = string.Empty;
            txtExpireDateOP2.Text = string.Empty;
        }

        protected void ClearJurisdict1Fields()
        {
            ddlRegulatoryBody1.SelectedIndex = 0;
            txtRegulatoryBodyOther1.Text = string.Empty;
            ddlProvince1.SelectedIndex = 0;
            ddlCountry1.SelectedIndex = 0;
            txtLicense1.Text = string.Empty;
            txtExpiryDate1.Text = string.Empty;
            cbNoExpiration1.Checked = false;
        }

        protected void ClearJurisdict2Fields()
        {
            ddlRegulatoryBody2.SelectedIndex = 0;
            txtRegulatoryBodyOther2.Text = string.Empty;
            ddlProvince2.SelectedIndex = 0;
            ddlCountry2.SelectedIndex = 0;
            txtLicense2.Text = string.Empty;
            txtExpiryDate2.Text = string.Empty;
        }

        protected void ClearJurisdict3Fields()
        {
            ddlRegulatoryBody3.SelectedIndex = 0;
            txtRegulatoryBodyOther3.Text = string.Empty;
            ddlProvince3.SelectedIndex = 0;
            ddlCountry3.SelectedIndex = 0;
            txtLicense3.Text = string.Empty;
            txtExpiryDate3.Text = string.Empty;
        }

        protected void ClearJurisdict4Fields()
        {
            ddlRegulatoryBody4.SelectedIndex = 0;
            txtRegulatoryBodyOther4.Text = string.Empty;
            ddlProvince4.SelectedIndex = 0;
            ddlCountry4.SelectedIndex = 0;
            txtLicense4.Text = string.Empty;
            txtExpiryDate4.Text = string.Empty;
        }

        protected void DisableEnableJurisdiction1Controls(bool key)
        {
            ddlRegulatoryBody1.Enabled = key;
            txtRegulatoryBodyOther1.ReadOnly = !key;
            ddlProvince1.Enabled = key;
            ddlCountry1.Enabled = key;
            txtLicense1.ReadOnly = !key;
            txtExpiryDate1.ReadOnly = !key;
            PopCalendar1.Enabled = key;
            cbNoExpiration1.Enabled = key;
        }

        protected void DisableEnableJurisdiction2Controls(bool key)
        {
            ddlRegulatoryBody2.Enabled = key;
            txtRegulatoryBodyOther2.ReadOnly = !key;
            ddlProvince2.Enabled = key;
            ddlCountry2.Enabled = key;
            txtLicense2.ReadOnly = !key;
            txtExpiryDate2.ReadOnly = !key;
            PopCalendar2.Enabled = key;
            cbNoExpiration2.Enabled = key;
        }

        protected void DisableEnableJurisdiction3Controls(bool key)
        {
            ddlRegulatoryBody3.Enabled = key;
            txtRegulatoryBodyOther3.ReadOnly = !key;
            ddlProvince3.Enabled = key;
            ddlCountry3.Enabled = key;
            txtLicense3.ReadOnly = !key;
            txtExpiryDate3.ReadOnly = !key;
            PopCalendar3.Enabled = key;
            cbNoExpiration3.Enabled = key;
        }

        protected void DisableEnableJurisdiction4Controls(bool key)
        {
            ddlRegulatoryBody4.Enabled = key;
            txtRegulatoryBodyOther4.ReadOnly = !key;
            ddlProvince4.Enabled = key;
            ddlCountry4.Enabled = key;
            txtLicense4.ReadOnly = !key;
            txtExpiryDate4.ReadOnly = !key;
            PopCalendar4.Enabled = key;
            cbNoExpiration4.Enabled = key;
        }

        protected void DisableEnableProfession1Controls(bool key)
        {
            txtNameProfessionOP1.ReadOnly = !key;
            txtRegulatoryBodyOP1.ReadOnly = !key;
            ddlProvinceOP1.Enabled = key;
            ddlCountryOP1.Enabled = key;
            txtLicenseOP1.ReadOnly = !key;
            txtExpireDateOP1.ReadOnly = !key;
            PopCalendar5.Enabled = key;
            cbNoExpirationOP1.Enabled = key;
        }

        protected void DisableEnableProfession2Controls(bool key)
        {
            txtNameProfessionOP2.ReadOnly = !key;
            txtRegulatoryBodyOP2.ReadOnly = !key;
            ddlProvinceOP2.Enabled = key;
            ddlCountryOP2.Enabled = key;
            txtLicenseOP2.ReadOnly = !key;
            txtExpireDateOP2.ReadOnly = !key;
            PopCalendar6.Enabled = key;
            cbNoExpirationOP2.Enabled = key;
        }

        protected bool ValidationCheck()
        {
            Page.Validate("GeneralValidation");
            bool valid = true;

            if (ddlRegisteredPracticeOtherProvince.SelectedValue.ToUpper() == "YES")
            {
                if (ddlRegulatoryBody1.SelectedIndex == 0)
                {
                    AddMessage("Please provide all information of your Jurisdiction 1", PageMessageType.UserError, string.Empty);
                    valid = false;
                    return valid;
                }

                bool validateJur1 = (ddlRegulatoryBody1.SelectedIndex > 0);
                bool validateJur2 = (ddlRegulatoryBody2.SelectedIndex > 0);
                bool validateJur3 = (ddlRegulatoryBody3.SelectedIndex > 0);
                bool validateJur4 = (ddlRegulatoryBody4.SelectedIndex > 0);

                if (validateJur1)
                {
                    if (cbNoExpiration1.Checked)
                    {
                        rfvExpiryDate1.Enabled = false;
                    }
                    else
                    {
                        rfvExpiryDate1.Enabled = true;
                    }

                    if (!string.IsNullOrEmpty(txtExpiryDate1.Text))
                    {
                        try
                        {
                            CultureInfo provider = CultureInfo.InvariantCulture;
                            DateTime expiry1 = DateTime.ParseExact(txtExpiryDate1.Text, "dd/MM/yyyy", provider);
                            if (expiry1 < DateTime.Now)
                            {
                                AddMessage("Jurisdiction 1: Expiry Date cannot be less than the current date", PageMessageType.UserError, string.Empty);
                                valid = false;
                                return valid;
                            }
                        }
                        catch 
                        {
                            AddMessage("Jurisdiction 1: Expiry Date format error", PageMessageType.UserError, string.Empty);
                            valid = false;
                            return valid;
                        }
                    }
                    if (ddlRegulatoryBody1.SelectedValue.ToUpper() == "ZOTHER" && string.IsNullOrEmpty(txtRegulatoryBodyOther1.Text))
                    {
                        AddMessage("Please provide all information of your Jurisdiction 1: Regulatory Body Other is empty", PageMessageType.UserError, string.Empty);
                        valid = false;
                        return valid;
                    }
                    Page.Validate("Jurisdiction1Validation");
                }

                if (validateJur2)
                {
                    if (cbNoExpiration2.Checked)
                    {
                        rfvExpiryDate2.Enabled = false;
                    }
                    else
                    {
                        rfvExpiryDate2.Enabled = true;
                    }

                    if (!string.IsNullOrEmpty(txtExpiryDate2.Text))
                    {
                        try
                        {
                            CultureInfo provider = CultureInfo.InvariantCulture;
                            DateTime expiry2 = DateTime.ParseExact(txtExpiryDate2.Text, "dd/MM/yyyy", provider);
                            if (expiry2 < DateTime.Now)
                            {
                                AddMessage("Jurisdiction 2: Expiry Date cannot be less than the current date", PageMessageType.UserError, string.Empty);
                                valid = false;
                                return valid;
                            }
                        }
                        catch 
                        {
                            AddMessage("Jurisdiction 2: Expiry Date format error", PageMessageType.UserError, string.Empty);
                            valid = false;
                            return valid;
                        }
                    }
                    if (ddlRegulatoryBody2.SelectedValue.ToUpper() == "ZOTHER" && string.IsNullOrEmpty(txtRegulatoryBodyOther2.Text))
                    {
                        AddMessage("Please provide all information of your Jurisdiction 2: Regulatory Body Other is empty", PageMessageType.UserError, string.Empty);
                        valid = false;
                        return valid;
                    }
                    Page.Validate("Jurisdiction2Validation");
                }

                if (validateJur3)
                {
                    if (cbNoExpiration3.Checked)
                    {
                        rfvExpiryDate3.Enabled = false;
                    }
                    else
                    {
                        rfvExpiryDate3.Enabled = true;
                    }

                    if (!string.IsNullOrEmpty(txtExpiryDate3.Text))
                    {
                        try
                        {
                            CultureInfo provider = CultureInfo.InvariantCulture;
                            DateTime expiry3 = DateTime.ParseExact(txtExpiryDate3.Text, "dd/MM/yyyy", provider);
                            if (expiry3 < DateTime.Now)
                            {
                                AddMessage("Jurisdiction 3: Expiry Date cannot be less than the current date", PageMessageType.UserError, string.Empty);
                                valid = false;
                                return valid;
                            }
                        }
                        catch 
                        {
                            AddMessage("Jurisdiction 3: Expiry Date format error", PageMessageType.UserError, string.Empty);
                            valid = false;
                            return valid;
                        }
                    }
                    if (ddlRegulatoryBody3.SelectedValue.ToUpper() == "ZOTHER" && string.IsNullOrEmpty(txtRegulatoryBodyOther3.Text))
                    {
                        AddMessage("Please provide all information of your Jurisdiction 3: Regulatory Body Other is empty", PageMessageType.UserError, string.Empty);
                        valid = false;
                        return valid;
                    }
                    Page.Validate("Jurisdiction3Validation");
                }

                if (validateJur4)
                {
                    if (cbNoExpiration4.Checked)
                    {
                        rfvExpiryDate4.Enabled = false;
                    }
                    else
                    {
                        rfvExpiryDate4.Enabled = true;
                    }

                    if (!string.IsNullOrEmpty(txtExpiryDate4.Text))
                    {
                        
                        try
                        {
                            CultureInfo provider = CultureInfo.InvariantCulture;
                            DateTime expiry4 = DateTime.ParseExact(txtExpiryDate4.Text, "dd/MM/yyyy", provider);
                            if (expiry4 < DateTime.Now)
                            {
                                AddMessage("Jurisdiction 4: Expiry Date cannot be less than the current date", PageMessageType.UserError, string.Empty);
                                valid = false;
                                return valid;
                            }
                        }
                        catch 
                        {
                            AddMessage("Jurisdiction 4: Expiry Date format error", PageMessageType.UserError, string.Empty);
                            valid = false;
                            return valid;
                        }
                    }
                    if (ddlRegulatoryBody4.SelectedValue.ToUpper() == "ZOTHER" && string.IsNullOrEmpty(txtRegulatoryBodyOther4.Text))
                    {
                        AddMessage("Please provide all information of your Jurisdiction 4: Regulatory Body Other is empty", PageMessageType.UserError, string.Empty);
                        valid = false;
                        return valid;
                    }
                    Page.Validate("Jurisdiction4Validation");
                }
            }

            if (ddlOtherProfession.SelectedValue.ToUpper() == "YES")
            {
                
                if (string.IsNullOrEmpty(txtNameProfessionOP1.Text))
                {
                    AddMessage("Please provide all information of your Profession 1", PageMessageType.UserError, string.Empty);
                    valid = false;
                    return valid;
                }

                bool validateProf1 = (!string.IsNullOrEmpty(txtNameProfessionOP1.Text));
                bool validateProf2 = (!string.IsNullOrEmpty(txtNameProfessionOP2.Text));


                if (validateProf1)
                {
                    if (cbNoExpirationOP1.Checked)
                    {
                        rfvExpireDateOP1.Enabled = false;
                    }
                    else
                    {
                        rfvExpireDateOP1.Enabled = true;
                    }

                    if (!string.IsNullOrEmpty(txtExpireDateOP1.Text))
                    {
                        
                        try
                        {
                            CultureInfo provider = CultureInfo.InvariantCulture;
                            DateTime expiry1 = DateTime.ParseExact(txtExpireDateOP1.Text, "dd/MM/yyyy", provider);
                            if (expiry1 < DateTime.Now)
                            {
                                AddMessage("Profession 1: Expiry Date cannot be less than the current date", PageMessageType.UserError, string.Empty);
                                valid = false;
                                return valid;
                            }
                        }
                        catch 
                        {
                            AddMessage("Profession 1: Expiry Date format error", PageMessageType.UserError, string.Empty);
                            valid = false;
                            return valid;
                        }
                    }
                    Page.Validate("Profession1Validation");
                    
                }

                if (validateProf2)
                {
                    if (cbNoExpirationOP2.Checked)
                    {
                        rfvExpireDateOP2.Enabled = false;
                    }
                    else
                    {
                        rfvExpireDateOP2.Enabled = true;
                    }

                    if (!string.IsNullOrEmpty(txtExpireDateOP2.Text))
                    {
                        
                        try
                        {
                            CultureInfo provider = CultureInfo.InvariantCulture;
                            DateTime expiry2 = DateTime.ParseExact(txtExpireDateOP2.Text, "dd/MM/yyyy", provider);
                            if (expiry2 < DateTime.Now)
                            {
                                AddMessage("Profession 2: Expiry Date cannot be less than the current date", PageMessageType.UserError, string.Empty);
                                valid = false;
                                return valid;
                            }
                        }
                        catch 
                        {
                            AddMessage("Profession 2: Expiry Date format error", PageMessageType.UserError, string.Empty);
                            valid = false;
                            return valid;
                        }
                    }
                    Page.Validate("Profession2Validation");
                    
                }
            }

            if (!Page.IsValid)
            {
                lblErrorMessage.Text = string.Empty;
                update.Update();
                modalPopupEx.Show();
                return false;
            }

            return valid;
        }
        
        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        protected void ShowErrorMessage(string Message)
        {
            lblErrorMessage.Text = Message;
            update.Update();
            modalPopupEx.Show();
        }

        public void AddMessage(string message, PageMessageType messageType, string returnURL)
        {
            SessionParameters.ReturnUrl = returnURL;
            this.Messages.Add(new PageMessage(message, messageType));
        }

        protected void BindLists()
        {

            var list3 = new List<GenClass>();
            list3.Add(new GenClass { Code = "", Description = "" });
            list3.Add(new GenClass { Code = "No", Description = "No" });
            list3.Add(new GenClass { Code = "Yes", Description = "Yes" });

            ddlRegisteredPracticeOtherProvince.DataSource = list3;
            ddlRegisteredPracticeOtherProvince.DataValueField = "Code";
            ddlRegisteredPracticeOtherProvince.DataTextField = "Description";
            ddlRegisteredPracticeOtherProvince.DataBind();

            ddlOtherProfession.DataSource = list3;
            ddlOtherProfession.DataValueField = "Code";
            ddlOtherProfession.DataTextField = "Description";
            ddlOtherProfession.DataBind();

            var repository = new Repository();

            var list0 = repository.GetGeneralList("REG_BODIES"); // get data for status field
            ddlRegulatoryBody1.DataSource = list0;
            ddlRegulatoryBody1.DataValueField = "Code";
            ddlRegulatoryBody1.DataTextField = "Description";
            ddlRegulatoryBody1.DataBind();
            ddlRegulatoryBody1.Items.Insert(0, string.Empty);

            ddlRegulatoryBody2.DataSource = list0;
            ddlRegulatoryBody2.DataValueField = "Code";
            ddlRegulatoryBody2.DataTextField = "Description";
            ddlRegulatoryBody2.DataBind();
            ddlRegulatoryBody2.Items.Insert(0, string.Empty);

            ddlRegulatoryBody3.DataSource = list0;
            ddlRegulatoryBody3.DataValueField = "Code";
            ddlRegulatoryBody3.DataTextField = "Description";
            ddlRegulatoryBody3.DataBind();
            ddlRegulatoryBody3.Items.Insert(0, string.Empty);

            ddlRegulatoryBody4.DataSource = list0;
            ddlRegulatoryBody4.DataValueField = "Code";
            ddlRegulatoryBody4.DataTextField = "Description";
            ddlRegulatoryBody4.DataBind();
            ddlRegulatoryBody4.Items.Insert(0, string.Empty);

            var list1 = repository.GetGeneralList("COUNTRY"); 
            ddlCountry1.DataSource = list1;
            ddlCountry1.DataValueField = "Code";
            ddlCountry1.DataTextField = "Description";
            ddlCountry1.DataBind();
            ddlCountry1.Items.Insert(0, string.Empty);

            ddlCountry2.DataSource = list1;
            ddlCountry2.DataValueField = "Code";
            ddlCountry2.DataTextField = "Description";
            ddlCountry2.DataBind();
            ddlCountry2.Items.Insert(0, string.Empty);

            ddlCountry3.DataSource = list1;
            ddlCountry3.DataValueField = "Code";
            ddlCountry3.DataTextField = "Description";
            ddlCountry3.DataBind();
            ddlCountry3.Items.Insert(0, string.Empty);

            ddlCountry4.DataSource = list1;
            ddlCountry4.DataValueField = "Code";
            ddlCountry4.DataTextField = "Description";
            ddlCountry4.DataBind();
            ddlCountry4.Items.Insert(0, string.Empty);

            ddlCountryOP1.DataSource = list1;
            ddlCountryOP1.DataValueField = "Code";
            ddlCountryOP1.DataTextField = "Description";
            ddlCountryOP1.DataBind();
            ddlCountryOP1.Items.Insert(0, string.Empty);

            ddlCountryOP2.DataSource = list1;
            ddlCountryOP2.DataValueField = "Code";
            ddlCountryOP2.DataTextField = "Description";
            ddlCountryOP2.DataBind();
            ddlCountryOP2.Items.Insert(0, string.Empty);

            var list2 = repository.GetGeneralList("PROVINCE_STATE");

            ddlProvince1.DataSource = list2;
            ddlProvince1.DataValueField = "Code";
            ddlProvince1.DataTextField = "Description";
            ddlProvince1.DataBind();
            ddlProvince1.Items.Insert(0, string.Empty);

            ddlProvince2.DataSource = list2;
            ddlProvince2.DataValueField = "Code";
            ddlProvince2.DataTextField = "Description";
            ddlProvince2.DataBind();
            ddlProvince2.Items.Insert(0, string.Empty);

            ddlProvince3.DataSource = list2;
            ddlProvince3.DataValueField = "Code";
            ddlProvince3.DataTextField = "Description";
            ddlProvince3.DataBind();
            ddlProvince3.Items.Insert(0, string.Empty);

            ddlProvince4.DataSource = list2;
            ddlProvince4.DataValueField = "Code";
            ddlProvince4.DataTextField = "Description";
            ddlProvince4.DataBind();
            ddlProvince4.Items.Insert(0, string.Empty);

            ddlProvinceOP1.DataSource = list2;
            ddlProvinceOP1.DataValueField = "Code";
            ddlProvinceOP1.DataTextField = "Description";
            ddlProvinceOP1.DataBind();
            ddlProvinceOP1.Items.Insert(0, string.Empty);

            ddlProvinceOP2.DataSource = list2;
            ddlProvinceOP2.DataValueField = "Code";
            ddlProvinceOP2.DataTextField = "Description";
            ddlProvinceOP2.DataBind();
            ddlProvinceOP2.Items.Insert(0, string.Empty);

        }

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetUserProfessionalHistoryInfo(CurrentUserId);
            if (user != null)
            {
                if (user.ProfessionalHistoryInfo != null)
                {
                    ddlRegisteredPracticeOtherProvince.SelectedValue = user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProvince;
                    ddlOtherProfession.SelectedValue = user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProfession;

                    if (ddlRegisteredPracticeOtherProvince.SelectedValue.ToUpper() != "YES")
                    {
                        DisableEnableJurisdiction1Controls(false);
                        DisableEnableJurisdiction2Controls(false);
                        DisableEnableJurisdiction3Controls(false);
                        DisableEnableJurisdiction4Controls(false);
                    }
                    if (ddlOtherProfession.SelectedValue.ToUpper() != "YES")
                    {
                        DisableEnableProfession1Controls(false);
                        DisableEnableProfession2Controls(false);
                    }
                }

                var first = user.ProfessionalHistoryInfo.PracticeDetail1;
                if (first != null)
                {
                    ddlRegulatoryBody1.SelectedValue = first.RegulatoryBody;
                    
                    if (ddlRegulatoryBody1.SelectedItem.Text.ToUpper() == "OTHER")
                    {
                        txtRegulatoryBodyOther1.Text = first.RegulatoryBodyOther;
                        txtRegulatoryBodyOther1.ReadOnly = false;
                    }
                    else
                    {
                        txtRegulatoryBodyOther1.Text = string.Empty;
                        txtRegulatoryBodyOther1.ReadOnly = true;
                    }
                    
                    ddlProvince1.SelectedValue = first.Province;
                    ddlCountry1.SelectedValue = first.Country;
                    txtLicense1.Text = first.License;

                    if (first.ExpiryDate != null)
                    {
                        txtExpiryDate1.Text = ((DateTime)first.ExpiryDate).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ddlRegulatoryBody1.SelectedValue)) cbNoExpiration1.Checked = true;
                    }

                    if (string.IsNullOrEmpty(first.RegulatoryBody))
                    {
                        DisableEnableJurisdiction2Controls(false);
                    }
                }

                var second = user.ProfessionalHistoryInfo.PracticeDetail2;
                if (second != null)
                {
                    ddlRegulatoryBody2.SelectedValue = second.RegulatoryBody;
                    if (ddlRegulatoryBody2.SelectedItem.Text.ToUpper() == "OTHER")
                    {
                        txtRegulatoryBodyOther2.Text = second.RegulatoryBodyOther;
                        txtRegulatoryBodyOther2.ReadOnly = false;
                    }
                    else
                    {
                        txtRegulatoryBodyOther2.Text = string.Empty;
                        txtRegulatoryBodyOther2.ReadOnly = true;
                    }
                    //txtRegulatoryBodyOther2.Text = second.RegulatoryBodyOther;
                    ddlProvince2.SelectedValue = second.Province;
                    ddlCountry2.SelectedValue = second.Country;
                    txtLicense2.Text = second.License;

                    if (second.ExpiryDate != null)
                    {
                        txtExpiryDate2.Text = ((DateTime)second.ExpiryDate).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ddlRegulatoryBody2.SelectedValue))  cbNoExpiration2.Checked = true;
                    }

                    if (string.IsNullOrEmpty(second.RegulatoryBody))
                    {
                        DisableEnableJurisdiction3Controls(false);
                    }
                }

                var third = user.ProfessionalHistoryInfo.PracticeDetail3;
                if (third != null)
                {
                    ddlRegulatoryBody3.SelectedValue = third.RegulatoryBody;
                    if (ddlRegulatoryBody3.SelectedItem.Text.ToUpper() == "OTHER")
                    {
                        txtRegulatoryBodyOther3.Text = third.RegulatoryBodyOther;
                        txtRegulatoryBodyOther3.ReadOnly = false;
                    }
                    else
                    {
                        txtRegulatoryBodyOther3.Text = string.Empty;
                        txtRegulatoryBodyOther3.ReadOnly = true;
                    }
                    //txtRegulatoryBodyOther3.Text = third.RegulatoryBodyOther;
                    ddlProvince3.SelectedValue = third.Province;
                    ddlCountry3.SelectedValue = third.Country;
                    txtLicense3.Text = third.License;

                    if (third.ExpiryDate != null)
                    {
                        txtExpiryDate3.Text = ((DateTime)third.ExpiryDate).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ddlRegulatoryBody3.SelectedValue))  cbNoExpiration3.Checked = true;
                    }

                    if (string.IsNullOrEmpty(third.RegulatoryBody))
                    {
                        DisableEnableJurisdiction4Controls(false);
                    }
                }

                var fourth = user.ProfessionalHistoryInfo.PracticeDetail4;
                if (fourth != null)
                {
                    ddlRegulatoryBody4.SelectedValue = fourth.RegulatoryBody;
                    if (ddlRegulatoryBody4.SelectedItem.Text.ToUpper() == "OTHER")
                    {
                        txtRegulatoryBodyOther4.Text = fourth.RegulatoryBodyOther;
                        txtRegulatoryBodyOther4.ReadOnly = false;
                    }
                    else
                    {
                        txtRegulatoryBodyOther4.Text = string.Empty;
                        txtRegulatoryBodyOther4.ReadOnly = true;
                    }
                    //txtRegulatoryBodyOther4.Text = fourth.RegulatoryBodyOther;
                    ddlProvince4.SelectedValue = fourth.Province;
                    ddlCountry4.SelectedValue = fourth.Country;
                    txtLicense4.Text = fourth.License;

                    if (fourth.ExpiryDate != null)
                    {
                        txtExpiryDate4.Text = ((DateTime)fourth.ExpiryDate).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ddlRegulatoryBody4.SelectedValue)) cbNoExpiration4.Checked = true;
                    }
                }

                var firstProf = user.ProfessionalHistoryInfo.ProfessionDetail1;
                if (firstProf != null)
                {
                    txtNameProfessionOP1.Text = firstProf.ProfessionName;
                    txtRegulatoryBodyOP1.Text = firstProf.RegulatoryBody;
                    ddlProvinceOP1.SelectedValue = firstProf.Province;
                    ddlCountryOP1.SelectedValue = firstProf.Country;
                    txtLicenseOP1.Text = firstProf.License;

                    if (firstProf.ExpiryDate != null)
                    {
                        txtExpireDateOP1.Text = ((DateTime)firstProf.ExpiryDate).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(txtNameProfessionOP1.Text)) cbNoExpirationOP1.Checked = true;
                    }
                }

                var secondProf = user.ProfessionalHistoryInfo.ProfessionDetail2;
                if (secondProf != null)
                {
                    txtNameProfessionOP2.Text = secondProf.ProfessionName;
                    txtRegulatoryBodyOP2.Text = secondProf.RegulatoryBody;
                    ddlProvinceOP2.SelectedValue = secondProf.Province;
                    ddlCountryOP2.SelectedValue = secondProf.Country;
                    txtLicenseOP2.Text = secondProf.License;

                    if (secondProf.ExpiryDate != null)
                    {
                        txtExpireDateOP2.Text = ((DateTime)secondProf.ExpiryDate).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(txtNameProfessionOP2.Text)) cbNoExpirationOP2.Checked = true;
                    }
                }
            }

            var user2 = repository.GetUserInfo(CurrentUserId);
            if (user2 != null)
            {
                //CurrentUser = user2;
                lblPersonalEmploymentInformationTitle.Text = "Professional Registration for " + user2.FullName;
            }
        }

        protected void UpdateUserProfessionalHistory()
        {
            var repository = new Repository();
            User user = new User();
            user.Id = CurrentUserId;
            user.ProfessionalHistoryInfo = new Classes.ProfessionalHistory();
            user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProvince = ddlRegisteredPracticeOtherProvince.SelectedValue;
            user.ProfessionalHistoryInfo.RegisteredPractiseInOtherProfession = ddlOtherProfession.SelectedValue;

            user.ProfessionalHistoryInfo.PracticeDetail1 = new Classes.ProfessionalHistoryPracticeDetail();
            user.ProfessionalHistoryInfo.PracticeDetail2 = new Classes.ProfessionalHistoryPracticeDetail();
            user.ProfessionalHistoryInfo.PracticeDetail3 = new Classes.ProfessionalHistoryPracticeDetail();
            user.ProfessionalHistoryInfo.PracticeDetail4 = new Classes.ProfessionalHistoryPracticeDetail();
            user.ProfessionalHistoryInfo.ProfessionDetail1 = new Classes.ProfessionalHistoryProfessionDetail();
            user.ProfessionalHistoryInfo.ProfessionDetail2 = new Classes.ProfessionalHistoryProfessionDetail();

            user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBody = ddlRegulatoryBody1.SelectedValue;
            user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBody = ddlRegulatoryBody2.SelectedValue;
            user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBody = ddlRegulatoryBody3.SelectedValue;
            user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBody = ddlRegulatoryBody4.SelectedValue;

            user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyText = ddlRegulatoryBody1.SelectedItem.Text;
            user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyText = ddlRegulatoryBody2.SelectedItem.Text;
            user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyText = ddlRegulatoryBody3.SelectedItem.Text;
            user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyText = ddlRegulatoryBody4.SelectedItem.Text;

            user.ProfessionalHistoryInfo.PracticeDetail1.RegulatoryBodyOther = txtRegulatoryBodyOther1.Text;
            user.ProfessionalHistoryInfo.PracticeDetail2.RegulatoryBodyOther = txtRegulatoryBodyOther2.Text;
            user.ProfessionalHistoryInfo.PracticeDetail3.RegulatoryBodyOther = txtRegulatoryBodyOther3.Text;
            user.ProfessionalHistoryInfo.PracticeDetail4.RegulatoryBodyOther = txtRegulatoryBodyOther4.Text;

            user.ProfessionalHistoryInfo.PracticeDetail1.Province = ddlProvince1.SelectedValue;
            user.ProfessionalHistoryInfo.PracticeDetail2.Province = ddlProvince2.SelectedValue;
            user.ProfessionalHistoryInfo.PracticeDetail3.Province = ddlProvince3.SelectedValue;
            user.ProfessionalHistoryInfo.PracticeDetail4.Province = ddlProvince4.SelectedValue;

            user.ProfessionalHistoryInfo.PracticeDetail1.Country = ddlCountry1.SelectedValue;
            user.ProfessionalHistoryInfo.PracticeDetail2.Country = ddlCountry2.SelectedValue;
            user.ProfessionalHistoryInfo.PracticeDetail3.Country = ddlCountry3.SelectedValue;
            user.ProfessionalHistoryInfo.PracticeDetail4.Country = ddlCountry4.SelectedValue;

            user.ProfessionalHistoryInfo.PracticeDetail1.License = txtLicense1.Text;
            user.ProfessionalHistoryInfo.PracticeDetail2.License = txtLicense2.Text;
            user.ProfessionalHistoryInfo.PracticeDetail3.License = txtLicense3.Text;
            user.ProfessionalHistoryInfo.PracticeDetail4.License = txtLicense4.Text;

            var culture = new CultureInfo("en-CA");

            if (!string.IsNullOrEmpty(txtExpiryDate1.Text))
            {
                DateTime expDate = Convert.ToDateTime(txtExpiryDate1.Text, culture);
                user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate = expDate;
            }
            else
            {
                user.ProfessionalHistoryInfo.PracticeDetail1.ExpiryDate = null;
            }


            if (!string.IsNullOrEmpty(txtExpiryDate2.Text))
            {
                DateTime expDate = Convert.ToDateTime(txtExpiryDate2.Text, culture);
                user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate = expDate;
            }
            else
            {
                user.ProfessionalHistoryInfo.PracticeDetail2.ExpiryDate = null;
            }


            if (!string.IsNullOrEmpty(txtExpiryDate3.Text))
            {
                DateTime expDate = Convert.ToDateTime(txtExpiryDate3.Text, culture);
                user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate = expDate;
            }
            else
            {
                user.ProfessionalHistoryInfo.PracticeDetail3.ExpiryDate = null;
            }


            if (!string.IsNullOrEmpty(txtExpiryDate4.Text))
            {
                DateTime expDate = Convert.ToDateTime(txtExpiryDate4.Text, culture);
                user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate = expDate;
            }
            else
            {
                user.ProfessionalHistoryInfo.PracticeDetail4.ExpiryDate = null;
            }

            user.ProfessionalHistoryInfo.ProfessionDetail1.ProfessionName = txtNameProfessionOP1.Text;
            user.ProfessionalHistoryInfo.ProfessionDetail1.RegulatoryBody = txtRegulatoryBodyOP1.Text;
            user.ProfessionalHistoryInfo.ProfessionDetail1.Province = ddlProvinceOP1.SelectedValue;
            user.ProfessionalHistoryInfo.ProfessionDetail1.Country = ddlCountryOP1.SelectedValue;
            user.ProfessionalHistoryInfo.ProfessionDetail1.License = txtLicenseOP1.Text;
            
            if (!string.IsNullOrEmpty(txtExpireDateOP1.Text))
            {
                DateTime expDate = Convert.ToDateTime(txtExpireDateOP1.Text, culture);
                user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate = expDate;
            }
            else
            {
                user.ProfessionalHistoryInfo.ProfessionDetail1.ExpiryDate = null;
            }

            user.ProfessionalHistoryInfo.ProfessionDetail2.ProfessionName = txtNameProfessionOP2.Text;
            user.ProfessionalHistoryInfo.ProfessionDetail2.RegulatoryBody = txtRegulatoryBodyOP2.Text;
            user.ProfessionalHistoryInfo.ProfessionDetail2.Province = ddlProvinceOP2.SelectedValue;
            user.ProfessionalHistoryInfo.ProfessionDetail2.Country = ddlCountryOP2.SelectedValue;
            user.ProfessionalHistoryInfo.ProfessionDetail2.License = txtLicenseOP2.Text;

            if (!string.IsNullOrEmpty(txtExpireDateOP2.Text))
            {
                DateTime expDate = Convert.ToDateTime(txtExpireDateOP2.Text, culture);
                user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate = expDate;
            }
            else
            {
                user.ProfessionalHistoryInfo.ProfessionDetail2.ExpiryDate = null;
            }

            
            repository.UpdateUserProfessionalHistoryLogged( CurrentUserId, user);

        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        /// <summary>
        ///  Messages collection. Contains all custom warning or errors which will be populated on the page.
        /// </summary>
        public PageMessages Messages
        {
            get
            {
                return (PageMessages)SessionParameters.PageMessages;
            }
            set
            {
                SessionParameters.PageMessages = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                if (SessionParameters.CurrentUser != null)
                {
                    return SessionParameters.CurrentUser;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUser = value;
            }
        }

        #endregion
    }
}