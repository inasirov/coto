﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Globalization;
using Classes;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberEmploymentProfileRenewal : System.Web.UI.UserControl
    {

        #region Consts

        //private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;

        private string PrevStep = WebConfigItems.Step4;
        private string NextStep = WebConfigItems.Step6;
        private const int CurrentStep = 5;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Request.QueryString.Count > 0)
            //    securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.RenewalStep = CurrentStep;
                BindLists();
                BindData();
            }
            txtTimeSpentDirectProfessionalServices.Attributes.Add("onmouseout", "caltotalpercent();");
            txtTimeSpentDirectProfessionalServices.Attributes.Add("onblur", "caltotalpercent();");

            txtTimeSpentAdministration.Attributes.Add("onmouseout", "caltotalpercent();");
            txtTimeSpentAdministration.Attributes.Add("onblur", "caltotalpercent();");

            txtTimeSpentClinicalEducation.Attributes.Add("onmouseout", "caltotalpercent();");
            txtTimeSpentClinicalEducation.Attributes.Add("onblur", "caltotalpercent();");

            txtTimeSpentOtherActivities.Attributes.Add("onmouseout", "caltotalpercent();");
            txtTimeSpentOtherActivities.Attributes.Add("onblur", "caltotalpercent();");

            txtTimeSpentResearch.Attributes.Add("onmouseout", "caltotalpercent();");
            txtTimeSpentResearch.Attributes.Add("onblur", "caltotalpercent();");

            txtTimeSpentTeaching.Attributes.Add("onmouseout", "caltotalpercent();");
            txtTimeSpentTeaching.Attributes.Add("onblur", "caltotalpercent();");


        }

        //protected void ibtnCurrentPracticeStatusClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("This represents your status for all OT employment.", "Status");
        //}

        //protected void ibtnBestDescribePracticeClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("<p><b>Primarily clinical practice</b> - A clinical OT is a registered OT who uses OT knowledge " +
        //        " and skill to provide direct service to clients/patients, including assessment, consultation and " +
        //        " referral to other services.</p>" +
        //        "<p><b>Mixed Nature</b> - An OT with a mixed nature of practice has primarily a non-clinical nature " +
        //        " of practice but provides direct service to clients as well.</p>" +
        //        "<p><b>Primarily non-clinical</b> - A non-clinical OT is a registered OT who uses OT knowledge and skill, and does not provide direct service " +
        //        "to clients, who is accountable to the public through the College. If you provide direct care to one or more clients " +
        //        "then you do not fall into this category.</p>", "Help - Nature of Your Practice");
        //}

        //protected void ibtnDirectProfServicesClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("Time spent per week on direct health professional services, e.g. administering assessments, client care, " +
        //        "charting on patients, health promotion. ", "Help");
        //}

        //protected void ibtnTimeSpentTeachingClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("Time spent per week teaching to prepare students for the OT health profession, excluding clinical education. ", "Help");
        //}

        //protected void ibtnTimeSpentClinicalEducationClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("Time spent per week providing or attending on clinical education only.", "Help - Clinical Education");
        //}

        //protected void ibtnTimeSpentResearchClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("Provide the percentage of time spent per week across all practice sites for conducting research in the profession (e.g. time spent " +
        //        "preparing research proposals, planning or carrying out research activities, and analyzing and documenting or disseminating results).", "Help - Research");
        //}

        //protected void ibtnTimeSpentAdministrationClick(object sender, ImageClickEventArgs e)
        //{
        //    ShowMessage("Time includes planning, organizing, management, paperwork - stats, billing. ", "Help - Admin");
        //}

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            string selectedStatus = ddlCurrentPracticeStatus.SelectedValue;
            Page.Validate("GeneralValidation2");
            if (!Page.IsValid)
            {
                return;
            }

            bool needValidation = selectedStatus.Contains("10") || selectedStatus.Contains("11");
            if (!needValidation || ValidationCheck())
            {
                UpdateUserEmploymentProfile();
                UpdateSteps(1);
                Response.Redirect(NextStep);
            }
            else
            {
                if (Messages != null && Messages.Count > 0)
                {
                    string message = string.Empty;
                    foreach (var item in Messages)
                    {
                        message += ((PageMessage)item).Message + "<br />";
                    }
                    message = string.Format("<font color='Red'>{0}</font>", message);

                    Messages.Clear();
                    ShowMessage(message);
                }
            }
        }

        protected void ddlCurrentPracticeStatusSelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = ddlCurrentPracticeStatus.SelectedItem.Value;
            if (selected.Contains("20") || selected.Contains("30"))
            {
                ClearAndDisableAllControls();
                ShowMessage("By selecting <b>\"Unemployed in OT\"</b> you will not be required to fill out the Practice Site Information and " +
                "Employment Profile sections of this page, and your current information will be deleted.<br />" +
                "If you are <b>\"Unemployed in OT\"</b> and wish to proceed with this selection please click OK and proceed to the next page.<br />" +
                "If you are <b>\"Employed in OT\"</b> or <b>\"Employed, on Leave\"</b>, please click OK and select the appropriate field from the drop down menu.", "Help");
            }
            else if (selected.Contains("40") || selected.Contains("50") || selected.Contains("70"))
            {
                ClearAndDisableAllControls();
                ShowMessage("By selecting <b>\"Working outside the profession\"</b> or <b>\"Working outside of Ontario\"</b> you will not be required to fill out the Practice Site Information and " +
                "Employment Profile sections of this page, and your current information will be deleted.<br />" +
                "If you are <b>\"Working outside the profession\"</b> or <b>\"Working outside of Ontario\"</b> and wish to proceed with this selection please click OK and proceed to the next page.<br />" +
                "If you are <b>\"Employed in OT\"</b> or <b>\"Employed, on Leave\"</b>, please click OK and select the appropriate field from the drop down menu.", "Help");
            }
            else
            {
                EnableAllControls();
            }
            if (selected.Contains("10"))
            {
                rvNumberWeeksPractices.MinimumValue = "1";
                rvNumberWeeksPractices.ErrorMessage = "<br />Must be greater than 0, less than 53";
                rvNumberHoursPractices.MinimumValue = "1";
                rvNumberHoursPractices.ErrorMessage = "<br />Must be greater than 0, less than 85";
            }
            if (selected.Contains("11"))
            {
                rvNumberWeeksPractices.MinimumValue = "1";
                rvNumberWeeksPractices.ErrorMessage = "<br />Must be greater than 0, less than 53 ";
                rvNumberHoursPractices.MinimumValue = "1";
                rvNumberHoursPractices.ErrorMessage = "<br />Must be greater than 0, less than 85";
            }
        }

        #endregion

        #region Methods

        protected void ClearAndDisableAllControls()
        {
            ddlFullTimePreference.SelectedIndex = 0;
            ddlFullTimePreference.Enabled = false;
            ddlStudentSupervisionLastYear.SelectedIndex = 0;
            ddlStudentSupervisionLastYear.Enabled = false;
            ddlStudentSupervisionComingYear.SelectedIndex = 0;
            ddlStudentSupervisionComingYear.Enabled = false;
            ddlClinicalClients.SelectedIndex = 0;
            ddlClinicalClients.Enabled = false;
            ddlBestDescribePractice.SelectedIndex = 0;
            ddlBestDescribePractice.Enabled = false;
            txtNumberHoursPractices.Text = "0";
            txtNumberHoursPractices.Enabled = false;
            txtNumberWeeksPractices.Text = "0";
            txtNumberWeeksPractices.Enabled = false;
            txtTimeSpentAdministration.Text = "0";
            txtTimeSpentAdministration.Enabled = false;
            txtTimeSpentClinicalEducation.Text = "0";
            txtTimeSpentClinicalEducation.Enabled = false;
            txtTimeSpentDirectProfessionalServices.Text = "0";
            txtTimeSpentDirectProfessionalServices.Enabled = false;
            txtTimeSpentOtherActivities.Text = "0";
            txtTimeSpentOtherActivities.Enabled = false;
            txtTimeSpentResearch.Text = "0";
            txtTimeSpentResearch.Enabled = false;
            txtTimeSpentTeaching.Text = "0";
            txtTimeSpentTeaching.Enabled = false;
            lblTotalPercent.Text = "0   %";
        }

        protected void EnableAllControls()
        {
            ddlFullTimePreference.Enabled = true;
            ddlStudentSupervisionLastYear.Enabled = true;
            ddlStudentSupervisionComingYear.Enabled = true;
            ddlClinicalClients.Enabled = true;
            ddlBestDescribePractice.Enabled = true;
            txtNumberHoursPractices.Enabled = true;
            txtNumberWeeksPractices.Enabled = true;
            txtTimeSpentAdministration.Enabled = true;
            txtTimeSpentClinicalEducation.Enabled = true;
            txtTimeSpentDirectProfessionalServices.Enabled = true;
            txtTimeSpentOtherActivities.Enabled = true;
            txtTimeSpentResearch.Enabled = true;
            txtTimeSpentTeaching.Enabled = true;
        }

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetUserEmploymentProfileInfo(CurrentUserId);
            if (user != null)
            {
                if (user.EmploymentProfile != null)
                {


                    //if (!string.IsNullOrEmpty(user.EmploymentProfile.CollegeMailings))
                    //{
                    //    ddlCollegeMailings.SelectedValue = user.EmploymentProfile.CollegeMailings;
                    //}

                    if (!string.IsNullOrEmpty(user.EmploymentProfile.CurrentWorkPreference))
                    {
                        ddlFullTimePreference.SelectedValue = user.EmploymentProfile.CurrentWorkPreference;
                    }

                    if (!string.IsNullOrEmpty(user.EmploymentProfile.StudentSupervisionLastYear))
                    {
                        ddlStudentSupervisionLastYear.SelectedValue = user.EmploymentProfile.StudentSupervisionLastYear;
                    }

                    if (!string.IsNullOrEmpty(user.EmploymentProfile.StudentSupervisionComingYear))
                    {
                        ddlStudentSupervisionComingYear.SelectedValue = user.EmploymentProfile.StudentSupervisionComingYear;
                    }

                    if (!string.IsNullOrEmpty(user.EmploymentProfile.HaveClinicalClients))
                    {
                        ddlClinicalClients.SelectedValue = user.EmploymentProfile.HaveClinicalClients;
                    }

                    if (!string.IsNullOrEmpty(user.EmploymentProfile.NaturePractice))
                    {
                        ddlBestDescribePractice.SelectedValue = user.EmploymentProfile.NaturePractice;
                    }

                    txtNumberWeeksPractices.Text = user.EmploymentProfile.PracticingNumberWeeks.ToString();
                    txtNumberHoursPractices.Text = user.EmploymentProfile.PracticingNumberHoursPerWeek.ToString();
                    txtTimeSpentDirectProfessionalServices.Text = user.EmploymentProfile.TimeSpentProfServices.ToString();
                    txtTimeSpentTeaching.Text = user.EmploymentProfile.TimeSpentTeaching.ToString();
                    txtTimeSpentClinicalEducation.Text = user.EmploymentProfile.TimeSpentClinicalEducation.ToString();
                    txtTimeSpentAdministration.Text = user.EmploymentProfile.TimeSpentAdministration.ToString();
                    txtTimeSpentResearch.Text = user.EmploymentProfile.TimeSpentResearch.ToString();
                    txtTimeSpentOtherActivities.Text = user.EmploymentProfile.TimeSpentOtherActivities.ToString();
                    lblTotalPercent.Text = (user.EmploymentProfile.TimeSpentProfServices + user.EmploymentProfile.TimeSpentTeaching + user.EmploymentProfile.TimeSpentClinicalEducation +
                        user.EmploymentProfile.TimeSpentAdministration + user.EmploymentProfile.TimeSpentResearch + user.EmploymentProfile.TimeSpentOtherActivities).ToString() + "   %";

                    if (!string.IsNullOrEmpty(user.EmploymentProfile.PracticeStatus))
                    {
                        ddlCurrentPracticeStatus.SelectedValue = user.EmploymentProfile.PracticeStatus;

                        string selected = ddlCurrentPracticeStatus.SelectedValue;
                        if (selected.Contains("20") || selected.Contains("30") || selected.Contains("40") || selected.Contains("50") || selected.Contains("70"))
                        {
                            ClearAndDisableAllControls();
                        }
                        else
                        {
                            EnableAllControls();
                        }
                        if (selected.Contains("10"))
                        {
                            rvNumberWeeksPractices.MinimumValue = "1";
                            rvNumberWeeksPractices.ErrorMessage = "<br />Must be greater than 0, less than 53";
                            rvNumberHoursPractices.MinimumValue = "1";
                            rvNumberHoursPractices.ErrorMessage = "<br />Must be greater than 0, less than 85";
                        }
                        if (selected.Contains("11"))
                        {
                            rvNumberWeeksPractices.MinimumValue = "0";
                            rvNumberWeeksPractices.ErrorMessage = "<br />Must be greater than 0, less than 53 ";
                            rvNumberHoursPractices.MinimumValue = "0";
                            rvNumberHoursPractices.ErrorMessage = "<br />Must be greater than 0, less than 85";
                        }
                    }
                }
            }
            var user2 = repository.GetUserInfo(CurrentUserId);
            if (user2 != null)
            {
                CurrentUser = user2;
                lblPersonalEmploymentProfileInformationTitle.Text = "Employment Profile for " + CurrentUser.FullName;
            }
        }

        protected void BindLists()
        {
            //var list2 = Countries.ListOfCountries;
            var repository = new Repository();

            var list01 = repository.GetGeneralList("FULL_PT_STATUS");

            ddlFullTimePreference.DataSource = list01;
            ddlFullTimePreference.DataValueField = "CODE";
            ddlFullTimePreference.DataTextField = "DESCRIPTION";
            ddlFullTimePreference.DataBind();
            ddlFullTimePreference.Items.Insert(0, string.Empty);

            var list2 = repository.GetGeneralList("EMPLOYMENT_STATUS");

            ddlCurrentPracticeStatus.DataSource = list2;
            ddlCurrentPracticeStatus.DataValueField = "Code";
            ddlCurrentPracticeStatus.DataTextField = "Description";
            ddlCurrentPracticeStatus.DataBind();
            ddlCurrentPracticeStatus.Items.Insert(0, string.Empty);

            var list3 = new List<GenClass>();
            list3.Add(new GenClass { Code = "", Description = "" });
            list3.Add(new GenClass { Code = "No", Description = "No" });
            list3.Add(new GenClass { Code = "Yes", Description = "Yes" });

            //ddlCollegeMailings.DataSource = list3;
            //ddlCollegeMailings.DataValueField = "Code";
            //ddlCollegeMailings.DataTextField = "Description";
            //ddlCollegeMailings.DataBind();

            ddlStudentSupervisionLastYear.DataSource = list3;
            ddlStudentSupervisionLastYear.DataValueField = "Code";
            ddlStudentSupervisionLastYear.DataTextField = "Description";
            ddlStudentSupervisionLastYear.DataBind();

            ddlStudentSupervisionComingYear.DataSource = list3;
            ddlStudentSupervisionComingYear.DataValueField = "Code";
            ddlStudentSupervisionComingYear.DataTextField = "Description";
            ddlStudentSupervisionComingYear.DataBind();

            ddlClinicalClients.DataSource = list3;
            ddlClinicalClients.DataValueField = "Code";
            ddlClinicalClients.DataTextField = "Description";
            ddlClinicalClients.DataBind();

            var list4 = repository.GetGeneralList("NATURE_PRACTICE");
            list4 = list4.Where(i => i.Code != "NOT_APPLICABLE").ToList();
            ddlBestDescribePractice.DataSource = list4;
            ddlBestDescribePractice.DataValueField = "Code";
            ddlBestDescribePractice.DataTextField = "Description";
            ddlBestDescribePractice.DataBind();
            ddlBestDescribePractice.Items.Insert(0, string.Empty);
        }

        protected void UpdateUserEmploymentProfile()
        {
            User user = new User();
            user.Id = CurrentUserId;

            user.EmploymentProfile = new EmploymentProfile();
            user.EmploymentProfile.PracticeStatus = ddlCurrentPracticeStatus.SelectedValue;
            //user.EmploymentProfile.CollegeMailings = ddlCollegeMailings.SelectedValue;
            user.EmploymentProfile.StudentSupervisionLastYear = ddlStudentSupervisionLastYear.SelectedValue;
            user.EmploymentProfile.StudentSupervisionComingYear = ddlStudentSupervisionComingYear.SelectedValue;
            user.EmploymentProfile.HaveClinicalClients = ddlClinicalClients.SelectedValue;
            user.EmploymentProfile.NaturePractice = ddlBestDescribePractice.SelectedValue;
            user.EmploymentProfile.CurrentWorkPreference = ddlFullTimePreference.SelectedValue;

            int number = user.EmploymentProfile.PracticingNumberWeeks;
            if (int.TryParse(txtNumberWeeksPractices.Text, out number))
            {
                user.EmploymentProfile.PracticingNumberWeeks = number;
            }

            if (int.TryParse(txtNumberHoursPractices.Text, out number))
            {
                user.EmploymentProfile.PracticingNumberHoursPerWeek = number;
            }

            if (int.TryParse(txtTimeSpentDirectProfessionalServices.Text, out number))
            {
                user.EmploymentProfile.TimeSpentProfServices = number;
            }
            if (int.TryParse(txtTimeSpentTeaching.Text, out number))
            {
                user.EmploymentProfile.TimeSpentTeaching = number;
            }
            if (int.TryParse(txtTimeSpentClinicalEducation.Text, out number))
            {
                user.EmploymentProfile.TimeSpentClinicalEducation = number;
            }
            if (int.TryParse(txtTimeSpentAdministration.Text, out number))
            {
                user.EmploymentProfile.TimeSpentAdministration = number;
            }
            if (int.TryParse(txtTimeSpentResearch.Text, out number))
            {
                user.EmploymentProfile.TimeSpentResearch = number;
            }
            if (int.TryParse(txtTimeSpentOtherActivities.Text, out number))
            {
                user.EmploymentProfile.TimeSpentOtherActivities = number;
            }

            var repository = new Repository();
            repository.UpdateUserEmploymentProfileLogged( CurrentUserId, user);

        }

        protected bool ValidationCheck()
        {

            Page.Validate("GeneralValidation");

            if (!Page.IsValid)
            {
                return false;
            }

            bool valid = true;

            int number = 0;
            if (!int.TryParse(txtNumberWeeksPractices.Text, out number))
            {
                AddMessage("Please use whole numbers. Decimal points are not permitted.", PageMessageType.UserError);
                valid = false;
            }


            if (!int.TryParse(txtNumberHoursPractices.Text, out number))
            {
                AddMessage("Please use whole numbers. Decimal points are not permitted.", PageMessageType.UserError);
                valid = false;
            }
            

            int time1 = 0;
            int time2 = 0;
            int time3 = 0;
            int time4 = 0;
            int time5 = 0;
            int time6 = 0;

            if (!int.TryParse(txtTimeSpentDirectProfessionalServices.Text, out time1))
            {
                AddMessage("Please use whole numbers. Decimal points are not permitted. Each field requires a value and blanks are not permitted.", PageMessageType.UserError);
                valid = false;
            }
            if (!int.TryParse(txtTimeSpentTeaching.Text, out time2))
            {
                AddMessage("Please use whole numbers. Decimal points are not permitted. Each field requires a value and blanks are not permitted.", PageMessageType.UserError);
                valid = false;
            }
            if (!int.TryParse(txtTimeSpentClinicalEducation.Text, out time3))
            {
                AddMessage("Please use whole numbers. Decimal points are not permitted. Each field requires a value and blanks are not permitted.", PageMessageType.UserError);
                valid = false;
            }
            if (!int.TryParse(txtTimeSpentResearch.Text, out time4))
            {
                AddMessage("Please use whole numbers. Decimal points are not permitted. Each field requires a value and blanks are not permitted.", PageMessageType.UserError);
                valid = false;
            }
            if (!int.TryParse(txtTimeSpentAdministration.Text, out time5))
            {
                AddMessage("Please use whole numbers. Decimal points are not permitted. Each field requires a value and blanks are not permitted.", PageMessageType.UserError);
                valid = false;
            }
            if (!int.TryParse(txtTimeSpentOtherActivities.Text, out time6))
            {
                AddMessage("Please use whole numbers. Decimal points are not permitted. Each field requires a value and blanks are not permitted.", PageMessageType.UserError);
                valid = false;
            }

            if ((time1 + time2 + time3 + time4 + time5 + time6) != 100 && ddlBestDescribePractice.SelectedItem.Text != "Not Applicable")
            {
                AddMessage(string.Format("Please ensure total percentages entered equals 100%. Your total currently equals: {0} %<br /> " +
                "Each field requires a value and blanks are not permitted.", (time1 + time2 + time3 + time4 + time5 + time6)), PageMessageType.UserError);
                valid = false;
            }

            if (time1 > 0 && !(ddlBestDescribePractice.SelectedItem.Text == "Clinical Practice" || ddlBestDescribePractice.SelectedItem.Text == "Mixed Nature"))
            {
                AddMessage(string.Format(" Since you have specified that you provide direct professional services your options for \"Nature of Practice\" are \"Mixed Nature\" or \"Clinical Practice\".", (time1 + time2 + time3 + time4 + time5 + time6)), PageMessageType.UserError);
                valid = false;
            }

            if ((ddlBestDescribePractice.SelectedItem.Text == "Non-Clinical" && ddlClinicalClients.SelectedItem.Text == "Yes"))
            {
                AddMessage(string.Format("If you provide direct care to one or more clients, your nature of practice must be clinical or mixed."), PageMessageType.UserError);
                valid = false;
            }

            return valid;
        }

        /// <summary>
        ///   Allow developer adds custom errors, warning into Messages Collection.
        /// </summary>
        /// <param name="message">Message information.</param>
        /// <param name="messageType">Message type.</param>
        public void AddMessage(string message, PageMessageType messageType)
        {
            this.Messages.Add(new PageMessage(message, messageType));
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        /// <summary>
        ///  Messages collection. Contains all custom warning or errors which will be populated on the page.
        /// </summary>
        public PageMessages Messages
        {
            get
            {
                return (PageMessages)SessionParameters.PageMessages;
            }
            set
            {
                SessionParameters.PageMessages = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                if (SessionParameters.CurrentUser != null)
                {
                    return SessionParameters.CurrentUser;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUser = value;
            }
        }

        #endregion


    }
}