﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberCurrencyLanguage.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlMemberCurrencyLanguage" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<div class="MainForm">
    <center>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr class="HeaderTitle" align="right">
                <td>
                    <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 2 of 12" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                    <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Next >" OnClick="btnUpdateClick"
                        ValidationGroup="PersonalValidation" />--%>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <uc:MessageBox ID="omb" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <table class="memberInfo" width="100%" cellspacing="2" cellpadding="3">
                        <tr class="RowTitle">
                            <td colspan="2">
                                <div>
                                    <asp:Label ID="lblCurrencyLanguageTitle" CssClass="heading" runat="server"
                                    Text="Currency and Languages of Service" />
                                </div>
                                
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="2" align="left">
                                <p>
                                    <asp:Label ID="lblDenotesRequiredTitle" runat="server"  Text="* denotes required field" />
                                </p>
                            </td>
                        </tr>--%>
                        <tr>
                            <td colspan="2" class="RightColumn">
                                <div style="padding-top: 10px; padding-bottom: 10px;">
                                    <asp:Label ID="lblCurrencyHoursTitle" runat="server"
                                    Text="Currency Hours" Font-Bold="true"  Font-Size="Medium" />
                                </div>
                                <br />
                                <asp:Label ID="lblCurrencyHoursDesc" runat="server" 
                                Text="Please select the option that applies to you from the following list."/><br />
                                <p>Please note, once registered, new and returning OTs 
                                    (including <b>new grads</b> and <b>OTs who have recently re-registered</b>) 
                                    will have <b>three years from the date of registration</b> to complete at least 600 practice hours.
                                </p>
                                <p>
                                    If you select "I do not meet the currency requirements and require a review", someone from the College will contact you directly.
                                    If you are unsure if you meet currency, please review our <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/2021/2021_MeetingCurrencyRequirements.pdf" target="_blank">Meeting Currency Requirements</a> page.
                                </p>    
                                <asp:DropDownList ID="ddlCurrencyHours" runat="server" />
                                <asp:RequiredFieldValidator ID="rfvCurrencyHours" runat="server" ControlToValidate="ddlCurrencyHours"
                                 InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select a currency option."
                                 Display="Dynamic" ForeColor="Red" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left; padding-top: 15px !important;" >
                                <asp:Label ID="lblLanguageOfServiceTitle" runat="server" Font-Bold="true" Font-Size="Medium"
                                    Text="Languages of Service" />&nbsp;
                                <%--<asp:ImageButton ID="ibLanguageServiceHelp" ImageUrl="~/images/qmark.jpg" runat="server" OnClick="ibLanguageServiceHelpClick" />--%>
                                <br />
                                <asp:Label ID="lblLanguageOfServiceDesc" runat="server" Text="Provide up to five languages in which you can personally and competently provide professional services." />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:40%" class="LeftLeftTitle">
                                <asp:Label ID="lblService1Title" runat="server"
                                    Text="Language of Service 1" />
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlService1" runat="server" />
                                <asp:RequiredFieldValidator ID="rfvService1" runat="server" ControlToValidate="ddlService1"
                                 InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Language of Service 1 is blank."
                                 Display="Dynamic" ForeColor="Red" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle"> 
                                <asp:Label ID="lblService2Title" runat="server"
                                    Text="Language of Service 2" />
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlService2" runat="server" OnSelectedIndexChanged="ddlService2_SelectedIndexChanged" AutoPostBack="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle">
                                <asp:Label ID="lblService3Title" runat="server"
                                    Text="Language of Service 3" />
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlService3" runat="server" OnSelectedIndexChanged="ddlService3_SelectedIndexChanged" AutoPostBack="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle">
                                <asp:Label ID="lblService4Title" runat="server"
                                    Text="Language of Service 4" />
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlService4" runat="server" OnSelectedIndexChanged="ddlService4_SelectedIndexChanged" AutoPostBack="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle">
                                <asp:Label ID="lblService5Title" runat="server"
                                    Text="Language of Service 5" />
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlService5" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                    <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Next >" OnClick="btnUpdateClick"
                        ValidationGroup="PersonalValidation" />--%>
                </td>
            </tr>
        </table>
    </center>
</div>
