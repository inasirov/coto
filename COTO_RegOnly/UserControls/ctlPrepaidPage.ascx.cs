﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlPrepaidPage1 : System.Web.UI.UserControl
    {
        private string PrevStep = WebConfigItems.Step11;

        protected void Page_Load(object sender, EventArgs e)
        {
            DataBind();
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(PrevStep);
        }

        protected void lbtnPrintReportClick(object sender, EventArgs e)
        {
            //Session["ReportID"] = CurrentUserId;
            //lblMessage.Text = "User ID: " + CurrentUserId;
            Response.Redirect(WebConfigItems.Step12_PDF_Report);
        }

        protected void lbtnFeedbackLinkClick(object sender, EventArgs e)
        {
            SessionParameters.ReturnUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            //lblMessage.Text = "Page url : " + HttpContext.Current.Request.Url.AbsoluteUri;
            Response.Redirect(WebConfigItems.Step12_Feedback);
        }

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }
    }
}