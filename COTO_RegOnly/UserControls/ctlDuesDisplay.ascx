﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlDuesDisplay.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlDuesDisplay" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<%@ Import Namespace="COTO_RegOnly.Classes" %>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="HeaderTitle" align="right">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 11 of 11" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                Visible="false" ValidationGroup="PersonalValidation" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" width="100%" cellspacing="2" cellpadding="3">
                                <tr class="RowTitle">
                                    <td>
                                        <asp:Label ID="lblPersonalInformationSectionTitle" CssClass="heading" runat="server"
                                            Text="Payment" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <p>
                                            Thank you for completing your Online Annual Registration Renewal Form.
                                        </p>
                                        
                                        <p>
                                            The total Annual Registration Renewal Fee for payments received by the College on
                                            or before June 1, <%# HardcodedValues.RenewalYear%> is $<%#  HardcodedValues.AnnualRegistration_Fee_Total %>(Annual Registration Fee $<%#  HardcodedValues.AnnualRegistration_Fee %>+ $<%#  HardcodedValues.AnnualRegistration_Fee_HST %>HST).
                                        </p>
                                        <p>
                                            The late fee of $<%#  HardcodedValues.AnnualRegistration_Late_Fee_With_HST %>(incl. HST) will be applied to all payments received by
                                            the College after June 1, <%#  HardcodedValues.RenewalYear %>.
                                        </p>
                                        <p>
                                            An NSF fee of $<%#  HardcodedValues.AnnualRegistration_NSF_Fee %>will be charged to all payments that are returned NSF or credit
                                            card payments that are processed by the College and declined.
                                        </p>
                                        <p>
                                            Please note that partial payments and post dated cheques are not permitted.
                                        </p>
                                        <p>
                                            Payment Method:
                                        </p>
                                        <ol>
                                            <li>Online credit card payment (Visa/MasterCard/Amex) - select add to basket </li>
                                            <li>Offline payment (Online banking/cheque/money order/credit card) -&nbsp;
                                                <asp:LinkButton ID="lbtnOfflinePayment" runat="server" Text="click here" OnClick="lbtnOfflinePaymentClick" />&nbsp;
                                                <asp:ImageButton ID="ibtnOfflinePaymentHelp" runat="server" OnClick="ibtnOfflinePaymentHelpClick"
                                                    ImageUrl="~/images/qmark.jpg" />
                                            </li>
                                        </ol>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ListView ID="lvSubscriptions" runat="server">
                                            <LayoutTemplate>
                                                <table border="0" cellpadding="3" cellspacing="0" style="width: 100%; text-align: center;">
                                                    <tr bgcolor="#EBEBEB">
                                                        <td style="width: 35px;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="text-align: left;">
                                                            <asp:Label ID="lblItemHeader" runat="server" Text="Item" />
                                                        </td>
                                                        <td style="width: 25px;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 100px; text-align: left; padding-right: 4px;">
                                                            <asp:Label ID="lblCostHeader" runat="server" Text="Cost" />
                                                        </td>
                                                    </tr>
                                                    <tr id="ItemPlaceHolder" runat="server">
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td style='text-align: right;'>
                                                            <asp:Label ID="lblTotalText" runat="server" Text="Total" Font-Bold="true" />&nbsp;
                                                        </td>
                                                        <td style='text-align: right; border-bottom: 1px solid black; border-left: 1px solid black;'>
                                                            $US
                                                        </td>
                                                        <td style='text-align: right; border-bottom: 1px solid black;'>
                                                            <asp:Label ID="lblTotalSummaryValue" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </LayoutTemplate>
                                            <EmptyDataTemplate>
                                                <table border="0" cellpadding="3" cellspacing="0" style="width: 100%; text-align: center;">
                                                    <tr bgcolor="#EBEBEB">
                                                        <td style="width: 35px;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="text-align: left;">
                                                            <asp:Label ID="lblItemHeader" runat="server" Text="Item" />
                                                        </td>
                                                        <td style="width: 25px;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 100px; text-align: right; padding-right: 4px;">
                                                            <asp:Label ID="lblCostHeader" runat="server" Text="Cost" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td colspan="3">
                                                            <span>Empty List</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td style='text-align: right;'>
                                                            <asp:Label ID="lblTotalText" runat="server" Text="Total" Font-Bold="true" />&nbsp;
                                                        </td>
                                                        <td style='text-align: right;'>
                                                            $US
                                                        </td>
                                                        <td style='text-align: right'>
                                                            <asp:Label ID="lblFundChartSummaryValue" runat="server" Text="0.00" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EmptyDataTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="border-bottom: 1px solid black;">
                                                        <asp:Image ID="imgCheckBox" runat="server" ImageUrl="~/images/checkbox.gif" />
                                                    </td>
                                                    <td style="white-space: nowrap; text-align: left; border-bottom: 1px solid black;">
                                                        <asp:Label ID="lblSubscriptionItem" runat="server" Text='<%# Eval("DuesProduct.Title") %>' />
                                                    </td>
                                                    <td style="text-align: right; border-bottom: 1px solid black; border-left: 1px solid black;">
                                                        $US
                                                    </td>
                                                    <td style="white-space: nowrap; text-align: right; border-bottom: 1px solid black;">
                                                        <asp:Label ID="lblSubscriptionItemCost" runat="server" Text='<%# ((decimal)Eval("AmountBilled")).ToString("#,#0.00;") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                ValidationGroup="PersonalValidation" Visible="false" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" ValidationGroup="PersonalValidation" />--%>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
