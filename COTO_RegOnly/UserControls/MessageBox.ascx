﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageBox.ascx.cs" Inherits="UserControls.MessageBox" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%--<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>--%>
<ajax:ModalPopupExtender ID="mpext" runat="server" BackgroundCssClass="modalBackground"
    TargetControlID="pnlPopup" PopupControlID="pnlPopup">
</ajax:ModalPopupExtender> <%----%>
<asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none;"  DefaultButton="btnOk">
    <table width="100%">
        <tr class="topHandle">
            <td colspan="2" align="left" runat="server" id="tdCaption">
                <asp:Label ID="lblCaption" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="midHandle">
            <td style="width: 60px" valign="middle" align="center">
                <asp:Image ID="imgInfo" runat="server" ImageUrl="~/Images/Info-48x48.png" />
            </td>
            <td valign="middle" align="left">
                <div style="overflow:auto; max-height: 600px; max-width: 400px;" >
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>    
                </div>
                <asp:HiddenField ID="hfRedirect" runat="server" />
            </td>
        </tr>
        <tr class="midHandle">
            <td colspan="2" align="right">
                <asp:Button ID="btnOk" CssClass="mapButton" runat="server" Text="OK" OnClick="btnOk_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>

<script type="text/javascript">
    function fnClickOK(sender, e)
    {
        __doPostBack(sender,e);
    }
</script>
<link href="/VA_dotNet/Styles/MessageBox.css" rel="stylesheet" type="text/css" />