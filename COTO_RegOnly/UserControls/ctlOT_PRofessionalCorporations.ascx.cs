﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlOT_PRofessionalCorporations : System.Web.UI.UserControl
    {
        
        private const string VIEW_STATE_CURRENT_USER_ID = "CurrentUserId";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindResult();
            }
        }

        public void BindResult()
        {
            var repository = new Repository();
            var list = repository.FindProfessionalCorporations();
            
            if (list.Count > 0)
            {
                trShowResults.Visible = true;
                trNoResults.Visible = false;
                
                lvResults.DataSource = list;
                lvResults.DataBind();
            }
            else
            {
                trShowResults.Visible = false;
                trNoResults.Visible = true;
            }
        }

        protected void lbtnSearchAgainClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PublicRegister.aspx");
        }

        protected void lvResultsCommand(object sender, ListViewCommandEventArgs e)
        {
            if (!string.IsNullOrEmpty((string)e.CommandArgument))
            {
                Session[VIEW_STATE_CURRENT_USER_ID] = (string)e.CommandArgument;
                Response.Redirect("~/PublicRegisterCoDisplay.aspx");
            }

        }
    }
}