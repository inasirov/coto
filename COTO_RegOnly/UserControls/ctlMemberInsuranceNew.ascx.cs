﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Classes;
using System.Security.Cryptography;
using System.Globalization;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlMemberInsuranceNew : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";

        private string PrevStep = WebConfigItems.Step9;
        private string NextStep = WebConfigItems.Step11;
        private const int CurrentStep = 10;

        protected DateTime OSOT_EspiryDate = new DateTime( DateTime.Now.Year, 10, 1);
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.RenewalStep = CurrentStep;
                BindLists();
                BindData();
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }

        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            lblGeneralMessage.Text = string.Empty;
            var repository = new Repository();
            var insurance = repository.GetRenewalUserInsuranceInfoNew(CurrentUserId);
            if (insurance==null)
            {
                lblGeneralMessage.Text = "You must provide details about your current professional liability insurance";
                return;
            }
            //else
            //{
            //    //if (insurance.InsuranceInfo != null)
            //    //{
            //        CheckRequirements(insurance.InsuranceInfo.PlanHeld);
            //    //}
            //}
            UpdateSteps(1);
            Response.Redirect(NextStep);
        }

        protected void ddlPlanHeldWithSelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshInsuranceSection();
        }

        protected void btnUpdatePolicy_Click(object sender, EventArgs e)
        {
            mpext6.Show();
        }

        protected void btnSaveClick(object sender, EventArgs e)
        {
            lblGeneralMessage.Text = string.Empty;
            if (ValidationCheck())  // Page.IsValid
            {
                UpdateUserInsuranceInfo();
                BindData();
                mpext6.Hide();
                update.Update();
            }
            else
            {
                if (Messages != null && Messages.Count > 0)
                {
                    string message = string.Empty;
                    foreach (var item in Messages)
                    {
                        message += ((PageMessage)item).Message + "<br />";
                    }
                    message = string.Format("<font color='Red'>{0}</font>", message);

                    Messages.Clear();
                    ShowErrorMessage(message);
                }
            }
        }

        protected void btnEditCancelCancelClick(object sender, EventArgs e)
        {
            lblGeneralMessage.Text = string.Empty;
            mpext6.Hide();
        }
        #endregion

        #region Methods

        protected bool ValidationCheck()
        {
            lblErrorMessages.Text = string.Empty;
            bool valid = true;
            DateTime expiryDate = DateTime.MinValue;
            DateTime startDate = DateTime.MinValue;
            CultureInfo provider2 = CultureInfo.InvariantCulture;
            string format2 = "MM/dd/yyyy";
            if (!string.IsNullOrEmpty(txtExpiryDate.Text) && !DateTime.TryParseExact(txtExpiryDate.Text, format2, provider2, DateTimeStyles.None, out expiryDate))
            {
                AddMessage("Expiry Date has invalid format", PageMessageType.UserError);
                valid = false;
            }

            if (!string.IsNullOrEmpty(txtStartDate.Text) && !DateTime.TryParseExact(txtStartDate.Text, format2, provider2, DateTimeStyles.None, out startDate))
            {
                AddMessage("Start Date has invalid format", PageMessageType.UserError);
                valid = false;
            }

            if (expiryDate != DateTime.MinValue)
            {
                if (expiryDate <= DateTime.Today)
                {
                    AddMessage("The insurance Expiry Date must be greater than today", PageMessageType.UserError);
                    valid = false;
                }
            }

            if (startDate != DateTime.MinValue)
            {
                if (startDate > DateTime.Today)
                {
                    AddMessage("The insurance Start Date must be current day or less", PageMessageType.UserError);
                    valid = false;
                }
            }

            if (expiryDate != DateTime.MinValue && startDate != DateTime.MinValue)
            {
                if (expiryDate <= startDate)
                {
                    AddMessage("The insurance Expiry Date must be greater than Start Date", PageMessageType.UserError);
                    valid = false;
                }
            }

            Page.Validate("PersonalValidation");

            if (!Page.IsValid)
            {
                lblErrorMessages.Text = string.Empty;
                valid = false;
            }
            return valid;
        }

        protected void RefreshInsuranceSection()
        {
            //txtExpiryDate.ReadOnly = false;
            //ceExpiryDate.Enabled = true;
            if (ddlPlanHeldWith.SelectedValue.ToUpper() == "NONE")
            {
                trCertificate.Visible = false;
                trExpiryDate.Visible = false;
            }
            else
            {
                trCertificate.Visible = true;
                trExpiryDate.Visible = true;
            }
            if (ddlPlanHeldWith.SelectedValue.ToLower() == "ot")
            {
                trOtherInsurance2.Visible = true;
                //txtCertificate.MaxLength = 10;
                //revCertificate.Enabled = false;
            }
            else
            {
                txtOther.Text = string.Empty;
                trOtherInsurance2.Visible = false;
                
            }

            if (ddlPlanHeldWith.SelectedValue.ToLower() == "osot" || ddlPlanHeldWith.SelectedValue.ToLower() == "caot")
            {
                txtExpiryDate.Text = OSOT_EspiryDate.ToString("MM/dd/yyyy");
                txtExpiryDate.Enabled = false;
            }
            else
            {
                txtExpiryDate.Enabled = true;
            }


        }

        protected void UpdateUserInsuranceInfo()
        {
            //int currentYear = DateTime.Now.Year;
            //if (DateTime.Now.Month <= 5)
            //{
            //    currentYear = currentYear - 1;
            //}
            User user = new User();

            user.Id = CurrentUserId;
            user.InsuranceInfo = new Classes.Insurance();
            int insurSEQN = 0;
            if (!string.IsNullOrEmpty(hfInsuranceSEQN.Value))
            {
                int.TryParse(hfInsuranceSEQN.Value, out insurSEQN);
                user.InsuranceInfo.SEQN = insurSEQN;
            }
            user.InsuranceInfo.PlanHeld = ddlPlanHeldWith.SelectedValue;
            user.InsuranceInfo.OtherInsuranceProvider = txtOther.Text;

            CultureInfo provider = CultureInfo.InvariantCulture;
            if (!string.IsNullOrEmpty(txtStartDate.Text))
            {
                var startDate = DateTime.ParseExact(txtStartDate.Text, "MM/dd/yyyy", provider);
                user.InsuranceInfo.StartDate = startDate;
            }
            else
            {
                user.InsuranceInfo.StartDate = DateTime.MinValue;
            }
            if (trExpiryDate.Visible && !string.IsNullOrEmpty(txtExpiryDate.Text))
            {
                var expiryDate = DateTime.ParseExact(txtExpiryDate.Text, "MM/dd/yyyy", provider);
                user.InsuranceInfo.ExpiryDate = expiryDate;
            }
            else
            {
                user.InsuranceInfo.ExpiryDate = DateTime.MinValue;
            }
            if (trCertificate.Visible)
            {
                user.InsuranceInfo.CertificateNumber = txtCertificate.Text;
            }
            else
            {
                user.InsuranceInfo.CertificateNumber = string.Empty;
            }

            var repository = new Repository();
            int newSEN = repository.UpdateApplicationUserInsuranceInfoLoggedNew(CurrentUserId, user);
            hfInsuranceSEQN.Value = newSEN.ToString();

            
        }

        //protected void CheckRequirements(string InsuranceTypeCode)
        //{
        //    if (InsuranceTypeCode.ToUpper() == "OT")
        //    {
        //        string message = string.Format("Member ID: {0}; ", CurrentUserId); ;
        //        message += string.Format("Date: {0}<br />", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

        //        message += "You have selected \"Other\" as your insurance provider. The insurance policy you have selected must meet the College’s requirements " +
        //        "for professional liability insurance and include a sexual abuse therapy and counseling fund endorsement.<br /><br />" +
        //        "You are required to submit a copy of the insurance policy content and certificate to the College for review and approval. " +
        //        "Please submit a copy to Brandi Park, Manager, Registration via email  bpark@coto.org or by fax  416-214-0851. <br /><br />" +
        //        "If you have any questions please contact Brandi Park via email  bpark@coto.org or by telephone  416-214-1177 (toll free 1-800-890-6570) x229 ";

        //        string emailSubject = "Registrant Requires Insurance Review ";  // + WebConfigItems.GetTestLabel;
        //        string emailTo = WebConfigItems.RegistrationManagerContactEmail;
        //        string emailToCC = string.Empty;
        //        if (!string.IsNullOrEmpty(CurrentUserEmail))
        //        {
        //            emailToCC = CurrentUserEmail;
        //        }
        //        var tool = new Tools();
        //        tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, emailToCC);
        //    }
        //}

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetRenewalUserInsuranceInfoNew(CurrentUserId);
            if (user != null)
            {
                if (user.InsuranceInfo != null)
                {
                    //hfInsuranceSEQN.Value = user.InsuranceInfo.SEQN.ToString();

                    //ddlPlanHeldWith.SelectedValue = user.InsuranceInfo.PlanHeld;
                    lblPlanHeldWith.Text = user.InsuranceInfo.PlanHeldName;

                    if (user.InsuranceInfo.StartDate != DateTime.MinValue)
                    {
                        //txtStartDate.Text = user.InsuranceInfo.StartDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        lblStartDate.Text = user.InsuranceInfo.StartDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                    }

                    if (user.InsuranceInfo.ExpiryDate != DateTime.MinValue)
                    {
                        //txtExpiryDate.Text = user.InsuranceInfo.ExpiryDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        lblExpiryDate.Text = user.InsuranceInfo.ExpiryDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                    }

                    if (user.InsuranceInfo.PlanHeld.ToUpper() == "OT")
                    {
                        //txtOther.Text = user.InsuranceInfo.OtherInsuranceProvider;
                        lblOther.Text = user.InsuranceInfo.OtherInsuranceProvider;
                        trOtherInsurance.Visible = true;
                    }
                    else
                    {
                        trOtherInsurance.Visible = false;
                    }
                    //txtCertificate.Text = user.InsuranceInfo.CertificateNumber;
                    lblCertificate.Text = user.InsuranceInfo.CertificateNumber;
                }
                //RefreshInsuranceSection();
            }
            else
            {
                hfInsuranceSEQN.Value = "0";
            }

            var user2 = repository.GetUserInfo(CurrentUserId);
            if (user2 != null)
            {
                CurrentUserEmail = user2.Email;
                lblInsuranceTitle.Text = "Insurance for " + user2.FullName;
            }
        }

        protected void BindLists()
        {
            var repository = new Repository();

            var list1 = repository.GetGeneralList("INSURANCE"); // get data for status field
            var item = list1.Find(I => I.Code == "NONE");
            if (item != null)
            {
                list1.Remove(item);
            }

            // Feb 04, 2020, removed "OSOT" option
            var item2 = list1.Find(I => I.Code == "OSOT");
            if (item2 != null)
            {
                list1.Remove(item2);
            }

            ddlPlanHeldWith.DataSource = list1;
            ddlPlanHeldWith.DataValueField = "CODE";
            ddlPlanHeldWith.DataTextField = "DESCRIPTION";
            ddlPlanHeldWith.DataBind();
            ddlPlanHeldWith.Items.Insert(0, string.Empty);
        }

        public void AddMessage(string message, PageMessageType messageType)
        {
            this.Messages.Add(new PageMessage(message, messageType));
        }

        protected void ShowErrorMessage(string Message)
        {
            lblErrorMessages.Text = Message;
            //update.Update();
        }

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public string CurrentUserEmail
        {
            get
            {
                if (ViewState["CURRENT_USER_EMAIL"] != null)
                {
                    return (string)ViewState["CURRENT_USER_EMAIL"];
                }
                else
                    return string.Empty; ;
            }
            set
            {
                ViewState["CURRENT_USER_EMAIL"] = value;
            }
        }

        /// <summary>
        ///  Messages collection. Contains all custom warning or errors which will be populated on the page.
        /// </summary>
        public PageMessages Messages
        {
            get
            {
                return (PageMessages)SessionParameters.PageMessages;
            }
            set
            {
                SessionParameters.PageMessages = value;
            }
        }

        #endregion

        
    }
}