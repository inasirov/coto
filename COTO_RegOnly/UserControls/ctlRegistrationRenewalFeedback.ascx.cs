﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlRegistrationRenewalFeedback : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }
        }

        protected void btnSubmitClick(object sender, ImageClickEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFeedbackInfo.Text))
            {
                var repository = new Repository();
                var user2 = repository.GetUserInfo(CurrentUserId);

                string message = string.Format("Member ID: {0}<br />", CurrentUserId);
                message += string.Format("Full Name: {0}<br />", user2.FullName);
                message += string.Format("Feedback: {0}<br /><br />", txtFeedbackInfo.Text);
                message += string.Format("Date: {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));


                string emailSubject = string.Format("coto comments for {0} ({1}) {2}", user2.FullName, CurrentUserId, WebConfigItems.GetTestLabel);
                string emailTo = WebConfigItems.RegistrationManagerContactEmail;
                var tool = new Tools();
                tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);
                //Response.Redirect(string.Format("{0}?common=updated", WebConfigItems.Step3));
                trfeedbackMain.Visible = false;
                trfeedbackThankYou.Visible = true;
            }
            else
            {
                lblMessage.Text = "Information was not provided";
                lblMessage.Visible = true;
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            if (SessionParameters.ReturnUrl != null && !string.IsNullOrEmpty(SessionParameters.ReturnUrl))
            {
                //lblMessage.Text = "Return Url : " + SessionParameters.ReturnUrl;
                Response.Redirect(SessionParameters.ReturnUrl);
            }
            else
            {
                //lblMessage.Text = "Return Url : empty";
                Response.Redirect(WebConfigItems.Step12_PaymentConfirmation);
            }
            
        }

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }
    }
}