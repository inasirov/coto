﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using Classes;

namespace UserControls
{
    public partial class ctlMemberDisplay : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 1)
                securityCheck();

            string Others = Request.QueryString["SOB"];
            if (!string.IsNullOrEmpty(Others))
            {
                if (Others == "1")
                {
                    SessionParameters.ShowSteps = true;
                }
                else if (Others == "0")
                {
                    SessionParameters.ShowSteps = false;
                }
            }
            if (SessionParameters.ShowSteps)
            {
                trOthers.Visible = true;
            }
            else
            {
                trOthers.Visible = false;
            }



            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                BindData();
                
            }
        }

        protected void btnBackClick(object sender, EventArgs e)
        {
            Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
        }

        protected void btnEditClick(object sender, EventArgs e)
        {
            Response.Redirect("MemberEdit.aspx");
        }

        protected void btnEditPersonalClick(object sender, EventArgs e)
        {
            Response.Redirect("MemberEditRenewal.aspx");
        }

        protected void btnEditLanguagesClick(object sender, EventArgs e)
        {
            Response.Redirect("MemberEditLanguages.aspx");
        }

        protected void btnEditEmploymentClick(object sender, EventArgs e)
        {
            Response.Redirect("MemberEmploymentRenewal.aspx");
        }

        protected void btnEditConductClick(object sender, EventArgs e)
        {
            Response.Redirect("MemberEditConduct.aspx");
        }

        protected void btnEmploymentClick(object sender, EventArgs e)
        {
            Response.Redirect("MemberEmployment.aspx");
        }

        protected void btnEditInsuranceClick(object sender, EventArgs e)
        {
            Response.Redirect("MemberLiabilityInsurance.aspx");
        }

        protected void btnEditDeclarationClick(object sender, EventArgs e)
        {
            Response.Redirect("MemberDeclaration.aspx");
        }
        
        #endregion

        #region Methods

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetUserInfo(CurrentUserId);
            if (user != null)
            {
                lblNameText.Text = user.FullName;
                lblRegistrationText.Text = user.Registration;
                lblCategoryText.Text = user.Category;
                lblCurrentStatusText.Text = user.CurrentStatus;
                if (user.CertificateExpiry != DateTime.MinValue)
                    lblCertificateExpiryText.Text = ((DateTime)user.CertificateExpiry).ToString("dd/MM/yyyy");
                lblEmailText.Text = user.Email;
                if (!string.IsNullOrEmpty(user.Email))
                {
                    hlEmail.NavigateUrl = string.Format("mailto: {0}", user.Email);
                }
                
                lblHomeAddressText.Text = user.FullFormattedAddress;
                lblHomePhoneText.Text = user.HomePhone;
            }

        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (CotoId.ToLower() == hash.ToLower() && timeDifference < 120)
            //if (timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }
        #endregion

    }
}