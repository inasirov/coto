﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Asi;
//using Asi.Web;
//using Asi.iBO.ContactManagement;
//using Asi.iBO;
//using Asi.iBO.Commerce;
//using Asi.Soa.ClientServices;

namespace COTO_RegOnly.UserControls
{
    public partial class ctlDuesDisplay : System.Web.UI.UserControl
    {

        #region Consts
        private const string VIEW_STATE_CURRENT_IMIS_ID = "GetCurrentImisId";
        private const string CHECKOUT_BASKET_PAGE = "~/BasketCheckout.aspx";
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(CurrentIMISID))
            //{
            //    //CurrentIMISID = newId;
            //    BindDues();
            //    //CheckExistingItemsInBasket();
            //}
        }

        #endregion

        #region Methods

        //public void BindDues()
        //{

        //    var contact = GetContactByID(CurrentIMISID);
        //    //lblResultInfo.Text = "Ucontact: " + contact.ToString();
        //    if (contact != null)
        //    {
        //        List<CSubscription> outstandingSubs = contact.Subscriptions.ToList();
        //        if (outstandingSubs != null && outstandingSubs.Count > 0)
        //        {
        //            lvSubscriptions.DataSource = outstandingSubs;
        //            lvSubscriptions.DataBind();

        //            var sum = outstandingSubs.Sum(L => L.Balance);
        //            var sumLabel = lvSubscriptions.FindControl("lblTotalSummaryValue") as Label;
        //            if (sumLabel != null)
        //            {
        //                sumLabel.Text = sum.ToString("#,#0.00;");
        //            }
        //        }
        //    }
        //}

        //public CContact GetContactByID(string iMIS_ID)
        //{
        //    Asi.iBO.ContactManagement.CContact contact;
        //    IiMISUser user;
        //    //user = CStaffUser.LoginByUserId("manager");
        //    user = CContactUser.LoginByWebLogin(Asi.Security.AppPrincipal.CurrentIdentity.UserId);
        //    user.ThrowExceptionOnError = true;
        //    contact = new Asi.iBO.ContactManagement.CContact(user, iMIS_ID);
        //    return contact;
        //}

        public void CheckExistingItemsInBasket()
        {
            //string userName = "manager";

            ////string password = "manager.dev";

            //EntityManager entityManager = new EntityManager(userName);

            //Asi.Soa.ClientServices.CartManager manager = new CartManager(entityManager, GetCurrentiMISID());

            //if (manager != null && manager.Cart != null)
            //{
            //    if (!manager.CartIsEmpty)  //&& manager.Cart.ComboOrder.Invoices.Select(S=>S.Description.Contains("Membership")).FirstOrDefault();
            //    {
            //        var items = manager.Cart.ComboOrder.Invoices.Where(I => I.Description.Contains("Membership") || I.Description.Contains("Renewal")).FirstOrDefault();
            //        if (items != null)
            //        {
            //            Response.Redirect(CHECKOUT_BASKET_PAGE);
            //        }
            //    }
            //}
        }

        #endregion

        protected void lbtnOfflinePaymentClick(object sender, EventArgs e)
        {

        }

        #region Properties

        //public string CurrentIMISID
        //{
        //    get
        //    {
        //        if (ViewState[VIEW_STATE_CURRENT_IMIS_ID] != null)
        //        {
        //            return (string)(ViewState[VIEW_STATE_CURRENT_IMIS_ID]);
        //        }
        //        else
        //        {
        //            string newId = GetCurrentiMISID();
        //            ViewState[VIEW_STATE_CURRENT_IMIS_ID] = newId;
        //            return newId;
        //        }
        //    }
        //    set
        //    {
        //        ViewState[VIEW_STATE_CURRENT_IMIS_ID] = value;
        //    }
        //}

        //public string GetCurrentiMISID()
        //{
        //    string _id = "";
        //    try
        //    {
        //        if (Asi.Security.AppPrincipal.CurrentIdentity.IsAuthenticated && Asi.Security.Utility.SecurityHelper.LoggedInImisId != "")
        //        {
        //            _id = Asi.Security.Utility.SecurityHelper.LoggedInImisId;
        //        }
        //    }
        //    catch { }
        //    return _id;
        //}

        #endregion

    }
}