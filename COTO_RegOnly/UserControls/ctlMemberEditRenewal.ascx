﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberEditRenewal.ascx.cs"  Inherits="COTO_RegOnly.UserControls.ctlMemberEditRenewal" %>

<script src="Scripts/MaskedEditFix.js" type="text/javascript"></script>
<%--<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>--%>
<script src="/Va_dotNet/Scripts/jBox/jBox.min.js"></script>

<link href="/Va_dotNet/Scripts/jBox/jBox.css" rel="stylesheet">

    <style type="text/css">
        .auto-style1 {
            height: 27px;
        }
        .pointer { cursor: pointer; }
    </style>


<script type="text/javascript">
    jQuery(document).ready(function () {
        attachHelpMessages();
    });


    function attachHelpMessages() {

        let message = jQuery('#hfLegalLastNameTitle').val();
        new jBox('Modal', {
            attach: '#iLegalLastNameTitle',
            width: 650,
            title: 'Legal Last Name',
            overlay: false,
            createOnInit: true,
            content: message,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        // iPreviousLegalLastName
        let message2 = jQuery('#hfPreviousLegalLastName').val();
        new jBox('Modal', {
            attach: '#iPreviousLegalLastName',
            width: 650,
            title: 'Previous Legal Last Name',
            overlay: false,
            createOnInit: true,
            content: message2,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        // iPreviousLegalFirstName
        let message3 = jQuery('#hfPreviousLegalFirstName').val();
        new jBox('Modal', {
            attach: '#iPreviousLegalFirstName',
            width: 650,
            title: 'Previous Legal First Name',
            overlay: false,
            createOnInit: true,
            content: message3,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        //iCommonlyUsedLastName
        let message4 = jQuery('#hfCommonlyUsedLastName').val();
        new jBox('Modal', {
            attach: '#iCommonlyUsedLastName',
            width: 650,
            title: 'Commonly Used Last Name',
            overlay: false,
            createOnInit: true,
            content: message4,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });

        //iHomeAddress
        let message5 = jQuery('#hfHomeAddress').val();
        new jBox('Modal', {
            attach: '#iHomeAddress',
            width: 650,
            title: 'Home Address',
            overlay: false,
            createOnInit: true,
            content: message5,
            draggable: true,
            repositionOnOpen: false,
            repositionOnContent: false
        });
    }
</script>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="HeaderTitle" align="right">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 1 of 12" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" ValidationGroup="PersonalValidation" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                            <asp:Label ID="lblDebug" runat="server" />
                        </td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" width="100%" cellspacing="2" cellpadding="3">
                                <tr class="RowTitle">
                                    <td colspan="2">
                                        <div>
                                            <asp:Label ID="lblPersonalInformationSectionTitle" CssClass="heading" runat="server"
                                            Text="Personal Information" />
                                        </div>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <%--<asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" />--%>
                                    </td>
                                    <td style="text-align: right; font-weight:bold;">
                                        <p>
                                            <%--<a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/2020/2020_Annual_Renewal_Glossary_S1_PersonalInformation_English.pdf" target="_blank">Glossary</a>&nbsp;/&nbsp; --%>
                                            <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/2021/2021_Annual_Renewal_Glossary_S1_PersonalInformation_French.pdf" target="_blank">Glossaire</a>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <p>
                                            <asp:Label ID="lblDescription" runat="server" Text="If the information regarding your commonly used names is incorrect, please provide the correct details. If you need to make changes to your <b>legal name</b>, please " />
                                            <a href="https://www.coto.org/docs/default-source/PDFs/request-for-change-of-legal-name.pdf?sfvrsn=2" target="_blank">complete this form</a>
                                            <%--<asp:LinkButton ID="btnEditProfile" runat="server" Text="complete this form " OnClick="btnEditProfileClick" />--%>
                                            <asp:Label ID="lblDescription2" runat="server" Text=" and forward to the College along with appropriate documentation, (e.g. marriage certificate). Please ensure proper use of upper and lower case letters (for example, street names, cities, postal codes, etc). " />
                                        </p>
                                    </td>
                                </tr>
                                <tr id="trEditConfirmation" runat="server" visible="false">
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="lblUpdateConfirmation" runat="server" Text="Thank you. The information on common names has been received and will be updated by the College."
                                            ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle" style="width: 50%">
                                        <asp:Label ID="lblLegalLastNameTitle" runat="server" Text="Legal Last Name" />
                                        &nbsp;<asp:Image ID="iLegalLastNameTitle" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                       <input type="hidden" id="hfLegalLastNameTitle" value="Your full legal name is required to be on file with the College but it does not necessarily have to be the name that you use in a professional practice. This is the name that appears on your wall certificate." />
                                        <%-- <asp:ImageButton ID="ibLegalLastName" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibLegalLastNameClick" />--%>
                                    </td>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblLegalLastName" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblLegalMiddleNameTitle" runat="server" Text="Legal Middle Name" />
                                        <%--<asp:ImageButton ID="ibLegalMiddleName" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibLegalLastNameClick" />--%>
                                    </td>
                                    <td class="auto-style1">
                                        <asp:Label ID="lblLegalMiddleName" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblLegalFirstNameTitle" runat="server" Text="Legal First Name" />
                                        <%--<asp:ImageButton ID="ibLegalFirstName" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibLegalLastNameClick" />--%>
                                    </td>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblLegalFirstName" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblPreviousLegalLastNameTitle" runat="server" Text="Previous Legal Last Name" />
                                        &nbsp;<asp:Image ID="iPreviousLegalLastName" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                        <input type="hidden" id="hfPreviousLegalLastName" value="If you are currently registered under a different legal name than the name that you had when you graduated with your OT entry degree, please provide this information if you have not already done so." />
                                        
                                        <%--<asp:ImageButton ID="ibPreviousLegalLastName" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibPreviousLegalLastNameClick" />--%>
                                    </td>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblPreviousLegalLastName" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="ibPreviousLegalFirstNameTitle" runat="server" Text="Previous Legal First Name" />
                                        &nbsp;<asp:Image ID="iPreviousLegalFirstName" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer"/>
                                        <input type="hidden" id="hfPreviousLegalFirstName" value="If you are currently registered under a different legal name than the name that you had when you graduated with your OT entry degree, please provide this information if you have not already done so." />
                                        <%--<asp:ImageButton ID="ibPreviousLegalFirstName" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibPreviousLegalLastNameClick" />--%>
                                    </td>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblPreviousLegalFirstName" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblCommonlyUsedLastNameTitle" runat="server" Text="Commonly Used Last Name in Practice" />
                                        &nbsp;<asp:Image ID="iCommonlyUsedLastName" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                        <input type="hidden" id="hfCommonlyUsedLastName" value="The name that is listed on the public register with the College. This must be the same name that you use in your professional practice. This may or not may be your full legal name." />
                                        <%--<asp:ImageButton ID="ibCommonlyUsedLastName" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibCommonlyUsedLastNameClick" />--%>
                                    </td>
                                    <td class="RightColumn">
                                        <%--<asp:Label ID="lblCommonlyUsedLastName" runat="server" />--%>
                                        <asp:TextBox ID="txtCommonlyUsedLastName" runat="server" MaxLength="30"/>
                                        <asp:RequiredFieldValidator ID="rfvCommonlyUsedLastName" runat="server" ControlToValidate="txtCommonlyUsedLastName"
                                            SetFocusOnError="True" ErrorMessage="<br />Please provide Commonly Used Last Name: Field is blank."
                                            Display="Dynamic" ValidationGroup="PersonalValidation" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblCommonlyUsedFirstNameTitle" runat="server" Text="Commonly Used First Name in Practice" />
                                        <%--<asp:ImageButton ID="ibCommonlyUsedFirstName" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibCommonlyUsedLastNameClick" />--%>
                                    </td>
                                    <td class="RightColumn">
                                        <%--<asp:Label ID="lblCommonlyUsedFirstName" runat="server" />--%>
                                        <asp:TextBox ID="txtCommonlyUsedFirstName" runat="server" MaxLength="20" />
                                        <asp:RequiredFieldValidator ID="rfvCommonlyUsedFirstName" runat="server" ControlToValidate="txtCommonlyUsedFirstName"
                                            SetFocusOnError="True" ErrorMessage="<br />Please provide Commonly Used First Name: Field is blank."
                                            Display="Dynamic" ValidationGroup="PersonalValidation" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblEmailLabel" runat="server" Text="Email:" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtEmailText" runat="server" Columns="35" MaxLength="100"/>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmailText"
                                            SetFocusOnError="True" ErrorMessage="<br />Please provide Email: Field is blank."
                                            Display="Dynamic" ValidationGroup="PersonalValidation" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailText"
                                            ErrorMessage="<br />Please provide a valid email" Display="Dynamic" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                            ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblBirthDate" runat="server" Text="Birth Date:" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtBirthDate" runat="server" ReadOnly="true" />
                                        <%--<rjs:PopCalendar ID="PopCalendar1" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                            Format="dd/mm/yyyy" Separator="/" Control="txtBirthDate" From-Date="1900-01-01" Enabled="false"
                                            ValidationGroup="PersonalValidation" MessageAlignment="RightCalendarControl" />--%>
                                        <%--<asp:RequiredFieldValidator ID="rfvBirthDate" runat="server" ControlToValidate="txtBirthDate"
                                            ErrorMessage="<br />Please provide Birth Date: field is blank." Display="Dynamic"
                                            ForeColor="Red" ValidationGroup="PersonalValidation"></asp:RequiredFieldValidator>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblHomePhoneLabel" runat="server" Text="Home Phone:" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtHomePhoneText" runat="server" MaxLength="25" />
                                        <ajaxToolkit:MaskedEditExtender ID="meeHomePhone" TargetControlID="txtHomePhoneText"
                                            Mask="(999) 999-9999" ClearMaskOnLostFocus="false" MaskType="None" OnFocusCssClass="MaskedEditFocus"
                                            OnInvalidCssClass="MaskedEditError" InputDirection="LeftToRight" ErrorTooltipEnabled="True"
                                            runat="server" Enabled="false" />
                                        <ajaxToolkit:MaskedEditValidator ID="mevHomePhone" runat="server" ControlToValidate="txtHomePhoneText" ControlExtender="meeHomePhone"
                                            InitialValue="(___) ___-____"  ValidationExpression="\([0-9]{3}\)\s[0-9]{3}\-[0-9]{4}"
                                            InvalidValueMessage="<br />Please provide Home Phone" Display="Dynamic" ForeColor="Red" ValidationGroup="PersonalValidation"
                                            IsValidEmpty="false" EmptyValueMessage="<br />Please provide Home Phone: Field is blank" Enabled="false" />
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td rowspan="3" valign="top" class="LeftLeftTitle">
                                        <asp:Label ID="lblHomeAddressLabel" runat="server" Text="Home Address:" />
                                        &nbsp;<asp:Image ID="iHomeAddress" ImageUrl="~/images/info2.png" runat="server" ClientIDMode="Static" CssClass="pointer" />
                                        <input type="hidden" id="hfHomeAddress" value="As per College bylaws, your home address must be provided to the College. The standard procedure for College mailings is to send to the home address, unless you indicate that your preferred mailing address is your employment address. You can change your preferred mailing address to your Primary (Employment) Site address in Step 4, Employment Profile." />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtHomeAddress1Text" runat="server" MaxLength="40" />
                                        <asp:RequiredFieldValidator ID="rfvAddress1" runat="server" ControlToValidate="txtHomeAddress1Text"
                                            ForeColor="Red" ErrorMessage="<br />Please provide Address: Field is blank."
                                            Display="Dynamic" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtHomeAddress2Text" runat="server" MaxLength="40" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtHomeAddress3Text" runat="server" MaxLength="40" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblCountry" runat="server" Text="Country:" />
                                        <%--<asp:ImageButton ID="ibCountryHelp" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibCountryHelpClick" />--%>&nbsp;
                                    </td>
                                    <td class="RightColumn">
                                        <%--<asp:TextBox ID="txtCountry" runat="server" />--%>
                                        <asp:DropDownList ID="ddlCountry" runat="server" OnSelectedIndexChanged="ddlCountrySelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry"
                                            InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide Country: Field is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblProvince" runat="server" Text="Province/State:" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtProvince" runat="server" Visible="false" Columns="20" MaxLength="15" />
                                        <asp:DropDownList ID="ddlProvince" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvProvince" runat="server" ControlToValidate="ddlProvince"
                                            InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide Province: Field is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblCityLabel" runat="server" Text="City:" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtCityText" runat="server" MaxLength="40" />
                                        <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCityText"
                                            ForeColor="Red" ErrorMessage="<br />Please provide City: Field is blank." Display="Dynamic"
                                            ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblPostalCode" runat="server" Text="Postal Code/Zip:" />&nbsp;
                                        <%--<asp:ImageButton ID="ibPostalCodeHelp" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibPostalCodeHelpClick" />--%>
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtPostalCode" runat="server" MaxLength="10" Width="100px"/>
                                        <asp:RequiredFieldValidator ID="rfvPostalCode" runat="server" ControlToValidate="txtPostalCode"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Postal Code Required" Display="Dynamic"
                                        ForeColor="Red" ></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revPostalCodeCA" runat="server" ControlToValidate="txtPostalCode"
                                            ErrorMessage="<br />Please provide a valid postal/zip code" Display="Dynamic"
                                            ValidationExpression="^([abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][a-zA-Z](\s)?[0-9][a-zA-Z][0-9])$"
                                            ValidationGroup="PersonalValidation" ForeColor="Red" Enabled="false" /> 
                                        <asp:RegularExpressionValidator ID="revPostalCodeUS" runat="server" ControlToValidate="txtPostalCode"
                                            ErrorMessage="<br />Please provide a valid postal/zip code" Display="Dynamic"
                                            ValidationExpression="^\d{5}((-|\s|_){1}\d{4})?$"
                                            ValidationGroup="PersonalValidation" ForeColor="Red" Enabled="false" />
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" ValidationGroup="PersonalValidation" />--%>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
