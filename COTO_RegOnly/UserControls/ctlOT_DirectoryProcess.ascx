﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlOT_DirectoryProcess.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlOT_DirectoryProcess" %>

<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Collections.Generic" %>

<style type="text/css">
    thead
    {
        font-weight: bold;
    }
    
    /* Table hover style */
    div.AltRow tr th
    {
        background-color: #dadada;
        font-weight: bold;
    }
    
    /* Table odd rows style */
    div.AltRow tr:nth-child(odd)
    {
        background-color: #ffffff;
    }
    
    /* Table even rows style */
    div.AltRow tr:nth-child(even)
    {
        background-color: #efefef;
    }
    
    /* Table hover style */
    div.AltRow tr:hover
    {
        background-color: #dadada;
    }
</style>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<div class="MainForm">
    <center>
        <asp:UpdateProgress runat="server" id="PageUpdateProgress">
            <ProgressTemplate>
            <table style="background-color: white; border-bottom: #eeeeee 1px solid; border-left: #eeeeee 1px solid; border-right: #eeeeee 1px solid; border-top: #eeeeee 1px solid; font-family: Verdana;" border="0" cellpadding="0" cellspacing="0"><tbody>  
                <tr><td>  
                    <img src="Images/blue_spin.gif" /> Loading... </td></tr>  
                </tbody></table>
                
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="RowTitle">
                        <td>
                            <asp:Label ID="lblPageTitle" CssClass="heading" runat="server" Text="OT Directory Search Results" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr id="trShowResults" runat="server">
                        <td>
                            <table width="100%">
                                <tr>
                                    <td style="text-align: left; font-weight:bold">
                                        <asp:Label ID="lblResultsMessage" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        Click on any of the names below for more information on the Occupational Therapist
                                        listed.
                                        <ul>
                                            <li>The College registration renewal is June 1 every year. All General Practising (G)
                                                certificates expire on May 31. </li>
                                            <li>Provisional Practising (P) Certificates expire May 31 or as otherwise specified
                                            </li>
                                            <li>Temporary Certificates(T) are valid for up to 4 months from the Effective Date on
                                                which the certificate was issued. </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:LinkButton ID="lbtnPrevious" runat="server" Text="Previous" OnClick="lbtnPreviousClick" />&nbsp;•&nbsp;
                                        <asp:LinkButton ID="lbtnSearchAgain" runat="server" Text="Search Again"  OnClick="lbtnSearchAgainClick" />&nbsp;•&nbsp;
                                        <asp:LinkButton ID="lbtnNext" runat="server" Text="Next" OnClick="lbtnNextClick"  />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lbtnLetterAll" runat="server" Text="All" CommandArgument="All" OnCommand="lbtnLetterCommand" />&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterA" runat="server" Text="A" CommandArgument="A" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterB" runat="server" Text="B" CommandArgument="B" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterC" runat="server" Text="C" CommandArgument="C" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterD" runat="server" Text="D" CommandArgument="D" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterE" runat="server" Text="E" CommandArgument="E" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterF" runat="server" Text="F" CommandArgument="F" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterG" runat="server" Text="G" CommandArgument="G" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterH" runat="server" Text="H" CommandArgument="H" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterI" runat="server" Text="I" CommandArgument="I" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterJ" runat="server" Text="J" CommandArgument="J" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterK" runat="server" Text="K" CommandArgument="K" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterL" runat="server" Text="L" CommandArgument="L" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterM" runat="server" Text="M" CommandArgument="M" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterN" runat="server" Text="N" CommandArgument="N" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterO" runat="server" Text="O" CommandArgument="O" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterP" runat="server" Text="P" CommandArgument="P" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterQ" runat="server" Text="Q" CommandArgument="Q" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterR" runat="server" Text="R" CommandArgument="R" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterS" runat="server" Text="S" CommandArgument="S" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterT" runat="server" Text="T" CommandArgument="T" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterU" runat="server" Text="U" CommandArgument="U" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterV" runat="server" Text="V" CommandArgument="V" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterW" runat="server" Text="W" CommandArgument="W" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterX" runat="server" Text="X" CommandArgument="X" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterY" runat="server" Text="Y" CommandArgument="Y" OnCommand="lbtnLetterCommand"/>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lbtnLetterZ" runat="server" Text="Z" CommandArgument="Z" OnCommand="lbtnLetterCommand"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ListView ID="lvResults" runat="server" OnItemCommand="lvResultsCommand">
                                            <LayoutTemplate>
                                                <div class="AltRow">
                                                    <table border="0" cellpadding="3" cellspacing="0" style="width: 100%; text-align: center;">
                                                        <tr bgcolor="#EBEBEB">
                                                            <th style="text-align: left; width: 30%;">
                                                                <asp:Label ID="lblOccupationalTherapist" runat="server" Text="Occupational Therapist" />
                                                            </th>
                                                            <th style="width: 25%; width: 25%; text-align: center">
                                                                <asp:Label ID="lblFormerNames" runat="server" Text="Former /AKA Names " />
                                                            </th>
                                                            <th style="width: 25%; text-align: center;">
                                                                <asp:Label ID="lblRegistrationNumber" runat="server" Text="Registration Number" />
                                                            </th>
                                                            <th style="width: 20%; text-align: center;">
                                                                <asp:Label ID="lblStatus" runat="server" Text="Status" />
                                                            </th>
                                                        </tr>
                                                        <tr id="ItemPlaceHolder" runat="server">
                                                        </tr>
                                                    </table>
                                                </div>
                                            </LayoutTemplate>
                                            <EmptyDataTemplate>
                                                <table border="0" cellpadding="3" cellspacing="0" style="width: 100%; text-align: center;">
                                                    <tr bgcolor="#EBEBEB">
                                                        <td style="text-align: left; width: 30%;">
                                                            <asp:Label ID="lblOccupationalTherapist" runat="server" Text="Occupational Therapist" />
                                                        </td>
                                                        <td style="width: 25%; width: 25%; text-align: center">
                                                            <asp:Label ID="lblFormerNames" runat="server" Text="Former /AKA Names " />
                                                        </td>
                                                        <td style="width: 25%; text-align: center;">
                                                            <asp:Label ID="lblRegistrationNumber" runat="server" Text="Registration Number" />
                                                        </td>
                                                        <td style="width: 20%; text-align: center;">
                                                            <asp:Label ID="lblStatus" runat="server" Text="Status" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td colspan="3">
                                                            <span>Empty List</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EmptyDataTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: left;">
                                                        <asp:LinkButton ID="lbtnFullName" runat="server" Text='<%# GetFullName((string)Eval("LastName") , (string)Eval("FirstName"), (string)Eval("MiddleName"), (string)Eval("InformalName") ) %>' CommandArgument='<%# (string)Eval("Id") %>' CommandName="View" />
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:Label ID="lblPreviousName" runat="server" Text='<%# GetPreviousNames((string)Eval("LastName"), (string)Eval("LegalLastName"), (string)Eval("PreviousLegalLastName"), (List<COTO_RegOnly.Classes.Activity>)Eval("PreviousLastNames")) %>' />
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:Label ID="lblRegistrationnumber" runat="server" Text='<%# (string)Eval("Registration") %>' />
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# (string)Eval("CurrentStatus") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:LinkButton ID="lbtnPrevious2" runat="server" Text="Previous" OnClick="lbtnPreviousClick"/>&nbsp;•&nbsp;
                                        <asp:LinkButton ID="lbtnSearchAgain2" runat="server" Text="Search Again" OnClick="lbtnSearchAgainClick"/>&nbsp;•&nbsp;
                                        <asp:LinkButton ID="lbtnNext2" runat="server" Text="Next" OnClick="lbtnNextClick" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trNoResults" runat="server">
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        <p>
                                            We are sorry but your search request did not return any matches. Please click "search
                                            again" and try broadening your search by using "all" in some of the areas of choice.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; font-weight:bold">
                                        <asp:Label ID="lblMessage2" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:LinkButton ID="lbtnPrevious3" runat="server" Text="Previous" Enabled="false" />&nbsp;•&nbsp;
                                        <asp:LinkButton ID="lbtnSearchAgain3" runat="server" Text="Search Again" OnClick="lbtnSearchAgainClick"/>&nbsp;•&nbsp;
                                        <asp:LinkButton ID="lbtnNext3" runat="server" Text="Next" Enabled="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
