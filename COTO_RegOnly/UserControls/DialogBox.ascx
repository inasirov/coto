﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DialogBox.ascx.cs" Inherits="COTO_RegOnly.UserControls.DialogBox" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<ajax:ModalPopupExtender ID="mpext5" runat="server" BackgroundCssClass="modalBackground"
    TargetControlID="pnlPopup5" PopupControlID="pnlPopup5" >
</ajax:ModalPopupExtender>

<%--<input id="dummy" type="button" style="display: none" runat="server" />--%>
<%----%> <%----%>
<asp:Panel ID="pnlPopup5" runat="server" CssClass="modalPopup"  Style="display: none;"
    DefaultButton="btnClose">
    <table style="width: 100%; padding: 0px; border-spacing: 0px;">
        <tr class="topHandle">
            <td style="text-align: left; vertical-align: text-top; padding: 8px;" colspan="2">
                <asp:Label ID="lblWarningTitle" runat="server" Font-Bold="true" Font-Size="16px"
                    Text="Warning - Please Confirm"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 80px" valign="middle" align="center">
                <asp:Image ID="imgInfo" runat="server" ImageUrl="~/Images/mark.png" />
            </td>
            <td style="text-align: left; vertical-align: text-top; padding: 16px;">
                <asp:Label ID="lblConfirmationMessage" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center; padding: 5px; vertical-align: top">
                <asp:Button ID="btnConfirm" CssClass="mapButton" runat="server" Text="Confirm" OnClick="btnConfirm_Click"/>&nbsp;&nbsp;
                <asp:Button ID="btnClose" CssClass="mapButton" runat="server" Text="Cancel" OnClick="btnClose_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>

<script type="text/javascript">
    function fnClickOK(sender, e) {
        __doPostBack(sender, e);
    }
</script>
<link href="../Styles/MessageBox.css" rel="stylesheet" type="text/css" />