﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit; 

namespace UserControls
{
    public partial class MessageBox : System.Web.UI.UserControl
    {
        public string _RedirectUrl
        {
            get
            {
                if (hfRedirect != null && hfRedirect.Value!=null)
                {
                    return (string)(hfRedirect.Value);
                }
                else
                    return string.Empty;
            }
            set
            {
                hfRedirect.Value = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            btnOk.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnOk.UniqueID, "");
        }

        public void ShowMessage(string Message)
        {
            lblMessage.Text = Message;
            lblCaption.Text = "";
            tdCaption.Visible = false;
            mpext.Show();
        }

        public void ShowMessage(string Message, string Caption)
        {
            lblMessage.Text = Message;
            lblCaption.Text = Caption;
            tdCaption.Visible = true;
            mpext.Show();
        }
 
        public void ShowMessage(string Message, string Caption, string RedirtectUrl)
        {
            lblMessage.Text = Message;
            lblCaption.Text = Caption;
            tdCaption.Visible = true;
            _RedirectUrl = RedirtectUrl;
            mpext.Show();
        }
 
        private void Hide()
        {
            lblMessage.Text = "";
            lblCaption.Text = "";
            mpext.Hide();
        }

        public void btnOk_Click(object sender, EventArgs e)
        {
            OnOkButtonPressed(e);
            if (!string.IsNullOrEmpty(_RedirectUrl))
            {
                Response.Redirect(_RedirectUrl);
            }
        }

        public delegate void OkButtonPressedHandler(object sender, EventArgs args);
        public event OkButtonPressedHandler OkButtonPressed;
        protected virtual void OnOkButtonPressed(EventArgs e)
        {
            if (OkButtonPressed != null)
                OkButtonPressed(btnOk, e);
        }


    }
}