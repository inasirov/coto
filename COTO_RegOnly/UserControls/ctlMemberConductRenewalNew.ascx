﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMemberConductRenewalNew.ascx.cs" Inherits="COTO_RegOnly.UserControls.ctlMemberConductRenewalNew" %>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion1" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion2" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion3" />

                <asp:AsyncPostBackTrigger ControlID="ddlQuestion4" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion5" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion6" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion7" />
            </Triggers>
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="HeaderTitle" align="right">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Annual Registration Renewal Step 9 of 12" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"  OnClick="ibtnNextClick" />
                        </td>
                    </tr>
                    <%--<tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            <table class="memberInfo" style="width: 100%; padding: 3px; border-spacing: 2px">
                                <tr class="RowTitle" >
                                    <td colspan="3" style="text-align: left">
                                        <div>
                                            <asp:Label ID="lblConductTitle" CssClass="heading" runat="server" Text="Suitability to Practise" />
                                        </div>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left">
                                        <%--<p>
                                            <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" />
                                        </p>--%>
                                    </td>
                                    <td style="text-align: right; font-weight:bold; padding-top: 10px!important; padding-bottom: 10px!important;" colspan="2">
                                        <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/2021/2021_Annual_Renewal_Glossary_S9_AdditionalInformation_SuitabilitytoPractise_English.pdf" target="_blank">Additional Information</a>&nbsp;&nbsp;
                                        <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/2021/2021_Annual_Renewal_Glossary_S9_AdditionalInformation_SuitabilitytoPractise_French.pdf" target="_blank">Renseignements supplémentaires</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left" colspan="3">
                                        <p>These questions pertain to occupational therapy practice, the practice of other professions, offences and other conduct. 
                                            Occupational therapists (OTs) are required answer these questions annually at renewal. If a change occurs to any of this 
                                            information throughout the year, for example, if you have been charged with an offence, you are legally required to report 
                                            it to the College within 30 days of the charge being laid against you. You can make the report by logging into the College 
                                            website and selecting "Self-Report".</p>
                                        <p>All questions must be answered. A response of "Yes" will require further explanation in the field provided. If you have missed 
                                            a question, you will be prompted to provide an answer before you can proceed to the next page.</p>
                                        <p>An answer of yes to any of these questions may not result in action from the College. The Office of the Registrar will consider 
                                            the content of the information and initiate a more detailed review as required.</p>
                                        <p><strong>Before you answer the questions below, please click on the link in the top right corner to review additional information.</strong></p>
                                        <p>If you have any questions or are unsure whether you are to report information, please contact Aoife Coghlan, Manager, Investigations &amp; Resolutions at <a href='mailto:acoghlan@coto.org'>acoghlan@coto.org</a> or by phone at 1.800.890.6570 ext. 223.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 30%">&nbsp;</td>
                                    <td style="width: 50%">&nbsp;</td>
                                    <td style="width: 20%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="lblQuestion1Title" runat="server" Text="1. Are you currently facing a proceeding (such as a hearing) for professional misconduct, incompetence, incapacity or a similar issue in Canada or elsewhere that has not been reported to the College?" />
                                        <asp:HiddenField ID="hfReportedDate" runat="server" />
                                         <asp:HiddenField ID="hfReportedSEQN" runat="server"  Value="0"/>
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion1" runat="server" OnSelectedIndexChanged="ddlQuestion1SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion1" runat="server" ControlToValidate="ddlQuestion1" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion1Details" runat="server" visible="false">
                                    <td style="vertical-align: top;">
                                        <asp:Label ID="lblQuestion1DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion1Details" TextMode="MultiLine" runat="server" Rows="3" Width="98%"  />
                                        <asp:RequiredFieldValidator ID="rvfQuestion1Details" runat="server" ControlToValidate="txtQuestion1Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="lblQuestion2Title" runat="server" Text="2. Have you had a finding of professional misconduct, incompetence, incapacity, or similar issue in Canada or elsewhere that has not been reported to the College?" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion2" runat="server" OnSelectedIndexChanged="ddlQuestion2SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion2" runat="server" ControlToValidate="ddlQuestion2" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion2Details" runat="server" visible="false">
                                    <td style="vertical-align: top;">
                                        <asp:Label ID="lblQuestion2DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion2Details" runat="server" TextMode="MultiLine" Rows="3" Width="98%" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion2Details" runat="server" ControlToValidate="txtQuestion2Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion3Title" runat="server" Text="3. Have you had a finding of professional negligence or malpractice in Canada or elsewhere that has not been reported to the College?" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion3" runat="server" OnSelectedIndexChanged="ddlQuestion3SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion3" runat="server" ControlToValidate="ddlQuestion3" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion3Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left;">
                                        <asp:Label ID="lblQuestion3DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion3Details" runat="server" TextMode="MultiLine" Rows="3" Width="98%" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion3Details" runat="server" ControlToValidate="txtQuestion3Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion4Title" runat="server" Text="4. Have you been charged with any offence in Canada or elsewhere that has not been reported to the College?" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion4" runat="server" OnSelectedIndexChanged="ddlQuestion4SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion4" runat="server" ControlToValidate="ddlQuestion4" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion4Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left;">
                                        <asp:Label ID="Label2" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion4Details" runat="server" TextMode="MultiLine" Rows="3" Width="98%" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion4Details" runat="server" ControlToValidate="txtQuestion4Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion5Title" runat="server" Text="5. Are you currently subject to any conditions or restrictions (such as a bail condition) by a court (or similar authority) in Canada or elsewhere that have not been previously reported to the College?" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion5" runat="server" OnSelectedIndexChanged="ddlQuestion5SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion5" runat="server" ControlToValidate="ddlQuestion5" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion5Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left;">
                                        <asp:Label ID="Label4" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion5Details" runat="server" TextMode="MultiLine" Rows="3" Width="98%" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion5Details" runat="server" ControlToValidate="txtQuestion5Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion6Title" runat="server" Text="6. Have you been found guilty by a court or other lawful authority of any offence in Canada or elsewhere that has not been previously reported to the College?" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion6" runat="server" OnSelectedIndexChanged="ddlQuestion6SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion6" runat="server" ControlToValidate="ddlQuestion6" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion6Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left;">
                                        <asp:Label ID="Label3" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion6Details" runat="server" TextMode="MultiLine" Rows="3" Width="98%" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion6Details" runat="server" ControlToValidate="txtQuestion6Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion7Title" runat="server" Text="7. Are there any other events or circumstances that would provide reasonable grounds for the belief that you will not or are not able to practise occupational therapy in a safe and professional manner?" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion7" runat="server" OnSelectedIndexChanged="ddlQuestion7SelectedIndexChanged" AutoPostBack="true" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion7" runat="server" ControlToValidate="ddlQuestion7" EnableClientScript="false"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select 'Yes' or 'No'" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trQuestion7Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left;">
                                        <asp:Label ID="lblQuestion7DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td colspan="2" class="RightColumn">
                                        <asp:TextBox ID="txtQuestion7Details" runat="server" TextMode="MultiLine" Rows="3" Width="98%" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion7Details" runat="server" ControlToValidate="txtQuestion7Details" EnableClientScript="false"
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Details field is empty" Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
