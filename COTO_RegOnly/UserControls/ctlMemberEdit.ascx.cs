﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using Classes;
using System.Globalization;


namespace UserControls
{
    public partial class ctlMemberEdit : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;
        #endregion

        #region Events

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.ClientScript.IsStartupScriptRegistered(GetType(), "MaskedEditFix"))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "MaskedEditFix", String.Format("<script type='text/javascript' src='{0}'></script>", Page.ResolveUrl("~/Scripts/MaskedEditFix.js")));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                BindLists();
                BindData();
            }
        }

        protected void btnBackClick(object sender, EventArgs e)
        {
            Response.Redirect("MemberDisplay.aspx");
        }
        protected void btnUpdateClick(object sender, EventArgs e)
        {
            if (UpdateUserInfo()) Response.Redirect("MemberDisplay.aspx");
        }

        protected void ibPostalCodeHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("Please use the appropriate format for postal codes, i.e. capital letters without any spaces (ANANAN). ", "Postal Code");
        }

        protected void ibCountryHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("If you reside in Canada, this field can be left blank.", "Country");
        }

        protected void ddlCountrySelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCountry.SelectedItem.Text.ToUpper() != "CANADA")
            {
                meePostalCode.Enabled = false;
                revPostalCode.Enabled = false;
            }
            else
            {
                meePostalCode.Enabled = true;
                revPostalCode.Enabled = true;
            }

            var repository = new Repository();

            meePostalCode.Enabled = false;
            revPostalCode.Enabled = false;
            revPostalCode.Enabled = false;

            if (ddlCountry.SelectedItem.Text.ToUpper().Contains("CANADA") || ddlCountry.SelectedItem.Text.ToUpper().Contains("UNITED STATES"))
            {
                txtProvince.Visible = false;
                txtProvince.Text = string.Empty;
                txtPostalCode.Text = string.Empty;
                ddlProvince.Visible = true;
                ddlProvince.SelectedIndex = 0;
                rfvProvince.Enabled = true;
                meeHomePhone.Enabled = true;
                //revHomePhone.Enabled = true;
                if (ddlCountry.SelectedItem.Text.ToUpper().Contains("CANADA"))
                {
                    var list0 = repository.GetCanadianProvincesListNew();

                    list0.Insert(0, new GenClass { Code = "", Description = "" });

                    ddlProvince.DataSource = list0;
                    ddlProvince.DataValueField = "Code";
                    ddlProvince.DataTextField = "Description";
                    ddlProvince.DataBind();

                    meePostalCode.Enabled = true;
                    revPostalCode.Enabled = true;
                }
                if (ddlCountry.SelectedItem.Text.ToUpper().Contains("UNITED STATES"))
                {
                    var list1 = repository.GetUSStatesList();

                    list1.Insert(0, new GenClass { Code = "", Description = "" });

                    ddlProvince.DataSource = list1;
                    ddlProvince.DataValueField = "Code";
                    ddlProvince.DataTextField = "Description";
                    ddlProvince.DataBind();
                }

            }
            else
            {
                txtProvince.Visible = true;
                ddlProvince.Visible = false;
                rfvProvince.Enabled = false;
                meeHomePhone.Enabled = false;
                //revHomePhone.Enabled = false;
            }
        }
        #endregion

        #region Methods

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        protected bool UpdateUserInfo()
        {
            User user = new User();
            user.Email = txtEmailText.Text;
            //if (!string.IsNullOrEmpty(hfBirthDate.Value))
            //{
            //    var culture = new CultureInfo("en-CA");
            //    var birthDate = Convert.ToDateTime(hfBirthDate.Value, culture);
            //    user.BirthDate = birthDate;
            //}
            //else
            //{
            //    user.BirthDate = DateTime.MinValue;
            //}
            user.Id = CurrentUserId;

            //txtHomePhoneText.Text = txtHomePhoneText.Text.Replace("_", string.Empty);
            //txtHomePhoneText.Text = txtHomePhoneText.Text.Replace("() -", string.Empty);
            user.HomePhone = txtHomePhoneText.Text;
            user.HomeAddress = new Address();
            user.HomeAddress.Address1 = txtHomeAddress1Text.Text;
            user.HomeAddress.Address2 = txtHomeAddress2Text.Text;
            user.HomeAddress.Address3 = txtHomeAddress3Text.Text;

            user.HomeAddress.City = txtCityText.Text;
            user.HomeAddress.Country = ddlCountry.SelectedItem.Text;
            user.HomeAddress.PostalCode = txtPostalCode.Text.ToUpper();

            if (ddlProvince.Visible)
            {
                user.HomeAddress.Province = ddlProvince.SelectedValue;
            }
            else
            {
                user.HomeAddress.Province = txtProvince.Text;
            }
            var repository = new Repository();
            var message = repository.UpdateUserInfoProfileLogged(CurrentUserId, user);
            if (!string.IsNullOrEmpty(message))
            {
                ShowMessage(message);
                return false;
            }
            return true;
        }

        protected void BindLists()
        {
            var repository = new Repository();

            var list1 = repository.GetCanadianProvincesListNew();

            list1.Insert(0, new GenClass { Code = "", Description = "" });

            ddlProvince.DataSource = list1;
            ddlProvince.DataValueField = "Code";
            ddlProvince.DataTextField = "Description";
            ddlProvince.DataBind();

            //var list2 = Countries.ListOfCountries;

            var list2 = repository.GetGeneralList("COUNTRY");
            //var list2 = repository.GetCountriesNew();

            list2 = list2.OrderBy(I => I.Description).ToList();
            ddlCountry.DataSource = list2;
            ddlCountry.DataValueField = "Code";
            ddlCountry.DataTextField = "Description";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, string.Empty);
        }

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetUserInfo(CurrentUserId);
            if (user != null)
            {
                txtEmailText.Text = user.Email;
                txtHomePhoneText.Text = user.HomePhone;
                //if (user.BirthDate != DateTime.MinValue)
                //{
                //    hfBirthDate.Value = user.BirthDate.ToString("dd/MM/yyyy");
                //}

                if (user.HomeAddress != null)
                {
                    txtHomeAddress1Text.Text = user.HomeAddress.Address1;
                    txtHomeAddress2Text.Text = user.HomeAddress.Address2;
                    txtHomeAddress3Text.Text = user.HomeAddress.Address3;

                    txtCityText.Text = user.HomeAddress.City;

                    if (string.IsNullOrEmpty(user.HomeAddress.Country))
                    {
                        user.HomeAddress.Country = "Canada";
                    }

                    var foundCountry = ddlCountry.Items.FindByText(user.HomeAddress.Country);
                    if (foundCountry != null)
                    {
                        ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(foundCountry);
                    }

                    meePostalCode.Enabled = false;
                    revPostalCode.Enabled = false;

                    if (user.HomeAddress.Country.ToUpper().Contains("CANADA") || user.HomeAddress.Country.ToUpper().Contains("UNITED STATES"))
                    {
                        txtProvince.Visible = false;
                        ddlProvince.Visible = true;
                        rfvProvince.Enabled = true;
                        //meeHomePhone.Enabled = true;

                        //ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByText(user.HomeAddress.Country));
                        if (user.HomeAddress.Country.ToUpper().Contains("CANADA"))
                        {
                            meePostalCode.Enabled = true;
                            revPostalCode.Enabled = true;
                        }
                        if (user.HomeAddress.Country.ToUpper().Contains("UNITED STATES"))
                        {
                            var list1 = repository.GetUSStatesList();

                            list1.Insert(0, new GenClass { Code = "", Description = "" });

                            ddlProvince.DataSource = list1;
                            ddlProvince.DataValueField = "Code";
                            ddlProvince.DataTextField = "Description";
                            ddlProvince.DataBind();
                        }

                        if (ddlProvince.Items.FindByValue(user.HomeAddress.Province) != null)
                        {
                            ddlProvince.SelectedValue = user.HomeAddress.Province;
                        }
                    }
                    else
                    {
                        txtProvince.Visible = true;
                        txtProvince.Text = user.HomeAddress.Province;
                        ddlProvince.Visible = false;
                        rfvProvince.Enabled = false;
                        //meeHomePhone.Enabled = false;
                    }

                    txtPostalCode.Text = user.HomeAddress.PostalCode;
                }
            }
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }
        #endregion

        

    }
}