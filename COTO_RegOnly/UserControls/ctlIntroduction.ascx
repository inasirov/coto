﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlIntroduction.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlIntroduction" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<%@ Import Namespace="COTO_RegOnly.Classes" %>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="HeaderTitle" align="left">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text='<%#  "Annual Renewal " + HardcodedValues.RenewalYear %> ' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                            <asp:Label ID="lblDebug" runat="server" />
                        </td>
                    </tr>
                    <tr id="trNoAccess" runat="server" visible="false">
                        <td style="text-align:left;">
                            <asp:Label ID="lblNoAccessMessage" runat="server" />
                        </td>
                    </tr>
                    <tr id="trMain" runat="server">
                        <td>
                            <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left">
                                        
                                        <p>
                                            Your certificate of registration expires on <b>May 31,&nbsp;<%# HardcodedValues.RenewalYear%></b>. 
                                            The deadline to renew or resign your certificate is <b>May 31,&nbsp;<%# HardcodedValues.RenewalYear%></b>.
                                            If you log in and make payment after 11:59 p.m. on May 31,&nbsp;<%# HardcodedValues.RenewalYear%>&nbsp;a late penalty fee of $100.00 + HST will automatically be added to your renewal fee. 
                                        </p>
                                        <span style="font-weight: bold; text-decoration: underline">Data Collection</span>
                                        <p>
                                            The College of Occupational Therapists of Ontario is working with the Ministry of Health and Long-Term Care and the Canadian Institute for Health Information (CIHI) 
                                            to learn more about occupational therapists by collecting and sharing demographic, geographic, educational, and employment information. 
                                            This data collection is part of the province&#39;s health human resources strategy.
                                        </p>
                                        <p>
                                            You are required to provide this information under the <i>Regulated Health Professions Act</i> (1991), the Occupational Therapy Act and the regulations and bylaws thereunder.
                                            To protect your privacy, the data the College submits will be anonymized.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <span style="font-weight: bold; text-decoration: underline">Reminders</span>
                                        <ol style="list-style: disc">
                                            <li>Do not stay logged in for extended periods of time without entering information as the system will time out.</li>
                                        </ol>

                                        <span style="font-weight: bold; text-decoration: underline">Resigning Your Certificate of Registration</span><br /><br />
                                        If you will not be working or using the title occupational therapist or OT in Ontario after May 31,&nbsp;<%# HardcodedValues.RenewalYear%>&nbsp;you may choose to resign your certificate of registration. 
                                        To <b>resign</b> your certificate of registration, click&nbsp;<asp:LinkButton ID="lbtnResignPageLink" runat="server" OnClick="lbtnResignPageLinkClick" Text="here" />. <br /><br />
                                        
                                        <span style="font-weight: bold; text-decoration: underline">Renewal Directions</span><br />
                                        <ol style="list-style: disc">
                                            <li>Answer all questions on each page. Clicking on the 'Next' button will save data entered on the current page and bring you to the next page.</li>
                                            <li>All information appearing in your profile must be reviewed and updated accordingly.</li>
                                            <li>A more <strong>detailed description of the terms</strong> can be found on the page by hovering over the text. 
                                                A French version of the descriptions can be found in <strong>the Glossary</strong> on the top right hand corner of the page.</li>
                                            <li>Payment options include:
                                                <ul>
                                                    <li>online banking </li>
                                                    <li>credit card (Visa, Mastercard, Amex)</li>
                                                    <li>cheque/money order payable to "COTO"</li>
                                                </ul>
                                            </li>
                                            <li>You may print a copy of the renewal form for your records at the end of the process. </li>
                                            <li>The College is not responsible for technology issues beyond its control. Please note that the online system is optimized for use with minimum requirements of Mozilla Firefox version 84 and 85, Google Chrome versions 87 and 88, Safari and Microsoft Edge. All other web browsers may not provide the optimum user experience.
                                            </li>
                                            <li>College office hours are Monday - Friday 8:30 a.m. - 4:30 p.m. Renewal questions
                                                can be sent by email to <a href="mailto:registration@coto.org">registration@coto.org</a>.
                                            </li>
                                        </ol>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <asp:CheckBox ID="cbAgree" runat="server" />&nbsp; <span>I have read and understood
                                            the above information.</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                            OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="errorsPanel" runat="server" Style="display: none; width: 750px;" CssClass="modalPopup"
                                DefaultButton="okBtn">
                                <table width="100%" cellspacing="2" cellpadding="2">
                                    <tr class="topHandleRed">
                                        <td align="left" runat="server" id="tdCaption">
                                            <asp:Label ID="lblCaption" runat="server" Text="Error Messages:"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="text-align: left; padding: 3px;">
                                                <asp:Label ID="lblErrorMessage" runat="server" />
                                                <asp:ValidationSummary ID="ValidationSummary" runat="server" DisplayMode="BulletList"
                                                    ShowSummary="true" ValidationGroup="GeneralValidation" />
                                                <div style="text-align: right">
                                                    <asp:Button ID="okBtn" runat="server" Text="Ok" Width="120px" /></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <ajaxToolkit:ModalPopupExtender ID="modalPopupEx" runat="server" PopupControlID="errorsPanel"
                                TargetControlID="invisibleTarget" CancelControlID="okBtn" BackgroundCssClass="modalBackground">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Label ID="invisibleTarget" runat="server" Style="display: none" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
