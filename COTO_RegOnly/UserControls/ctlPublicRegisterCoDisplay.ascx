﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlPublicRegisterCoDisplay.ascx.cs"
    Inherits="COTO_RegOnly.UserControls.ctlPublicRegisterCoDisplay" %>
<div class="MainForm">
    <center>
        <table cellpadding="2" cellspacing="2" width="600">
            <tr class="HeaderTitle">
                <td>
                    <asp:Label ID="lblPageTitleLabel" runat="server" Text="OT Directory Search Results" />
                </td>
            </tr>
            <tr class="RowTitle2">
                <td colspan="3">
                    <asp:Label ID="Label6" CssClass="heading2" runat="server" Text="PROFESSIONAL CORPORATION INFORMATION" />
                </td>
            </tr>
            <tr id="trCorporationEmptyRow" runat="server">
                <td colspan="3">
                    <span>We are sorry but your search request did not return any matches. Please click "search again" 
                    and try broadening your search by using "all" in some of the areas of choice. </span>
                </td>
            </tr>
            <tr id="trCorporationNoEmptyRow" runat="server">
                <td colspan="3">
                    <table width="100%">
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%; vertical-align: top">
                                <asp:Label ID="lblCorpNameTitle" runat="server" Text="Name:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCorpNameText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%">
                                <asp:Label ID="lblCorpCertificateTitle" runat="server" Text="Registration #:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCorpCertificateText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%">
                                <asp:Label ID="lblCorpStatusTitle" runat="server" Text="Status:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCorpStatusText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%">
                                <asp:Label ID="lblCorpStatusEffectiveDateTitle" runat="server" Text="Status Effective Date:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCorpStatusEffectiveDateText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%; vertical-align: top">
                                <asp:Label ID="lblCorpCategoryTitle" runat="server" Text="Category:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCorpCategoryText" runat="server" Text="Certificate of Authorization"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%; vertical-align: top">
                                <asp:Label ID="lblCorpPracticeNameTitle" runat="server" Text="Practice Name:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCorpPracticeNameText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%; vertical-align: top">
                                <asp:Label ID="lblCorpBusinessAddressTitle" runat="server" Text="Address:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCorpBusinessAddressText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%">
                                <asp:Label ID="lblCorpPhoneTitle" runat="server" Text="Telephone Number:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCorpPhoneText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%">
                                <asp:Label ID="lblCorpEmailTitle" runat="server" Text="Email Address:" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCorpEmailText" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="LeftLeftTitle" style="width: 40%; vertical-align: top">
                                <asp:Label ID="lblCorpShareholdersTitle" runat="server" Text="Shareholder(s):" />
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td class="RightColumn">
                                <asp:Label ID="lblCorpShareholdersText" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:LinkButton ID="lbtnSearchAgain3" runat="server" Text="Search Again" OnClick="lbtnSearchAgainClick"/>
                </td>
            </tr>
        </table>
    </center>
</div>
