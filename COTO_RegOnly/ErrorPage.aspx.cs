﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;

namespace COTO_RegOnly
{
    public partial class ErrorPage : System.Web.UI.Page
    {
        #region Constants
        private const string VIEW_STATE_IS_SESSION_TIMEOUT = "IsSessionTimeout";
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //if (string.IsNullOrEmpty(SessionParameters.ReturnUrl))
                //{
                //    btnGoBack.OnClientClick = "javascript:window.close(); return false;";
                //}
                PopulateActionButton();
                PopulateMessages();
            }
        }

        protected void lsMessages_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            PageMessage message;
            //System.Web.UI.WebControls.Image imgMessage;
            Label lblMessage;

            message = (PageMessage)e.Item.DataItem;
            //imgMessage = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgMessage");
            lblMessage = (Label)e.Item.FindControl("lblMessage");

            lblMessage.Text = message.Message;
        }

        protected void BtnGoBack_Click(object sender, EventArgs e)
        {
            if (!IsSessionTimeout)
            {
                // Ignore the error happened page.
                if (!string.IsNullOrEmpty(SessionParameters.ReturnUrl))
                {
                    try
                    {
                        //string newURL = System.IO.Path.GetFullPath(SessionParameters.ReturnUrl);
                        string newUrl = SessionParameters.ReturnUrl;
                        SessionParameters.ReturnUrl = null; //clear returnurl from session
                        Response.Redirect(newUrl);
                    }
                    catch
                    {
                        Server.ClearError();
                    }
                }
                else
                {
                    Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                }
            }
            else
            {
                // If system's session already timeout.
                // Redirct to the top level page.
                // will be implemented later
                try
                {
                    Response.Redirect(WebConfigItems.GetCOTOWelcomePageUrl);
                    // Response.Redirect("~/Default.aspx");
                }
                catch
                {
                    Server.ClearError();
                }
            }
        }

        #endregion

        #region Methods

        public void PopulateActionButton()
        {

            if (SessionParameters.CurrentUserId != null &&
                !string.IsNullOrEmpty(SessionParameters.CurrentUserId)
                )
            {
                IsSessionTimeout = false;
                btnGoBack.Text = "Go Back";
            }
            else
            {
                // If session is time out, show go to login page.
                IsSessionTimeout = true;
                btnGoBack.Text = "Close";
            }
        }
        public void PopulateMessages()
        {
            // Messages might be null
            if (Messages != null)
            {
                if (this.Messages.Count > 0)
                {
                    lsMessages.DataSource = this.Messages;
                    lsMessages.DataBind();
                    this.Messages.Clear();
                }
            }

            if (Request.QueryString["aspxerrorpath"]!=null)
            {
                string pageURL = Request.QueryString["aspxerrorpath"];
                Messages = new PageMessages();
                Messages.Add(new PageMessage(string.Format("Sorry, \"{0}\" page not found", pageURL), PageMessageType.UserError));

                lsMessages.DataSource = this.Messages;
                lsMessages.DataBind();
            }
        }

        #endregion

        #region Properties

        /// <summary>
        ///  Default value = false.
        ///  If Current user session is gone, value = true.
        ///  System will redirect to login page.
        /// </summary>
        protected bool IsSessionTimeout
        {
            get
            {
                if (ViewState[VIEW_STATE_IS_SESSION_TIMEOUT] != null)
                {
                    try
                    {
                        return (bool)ViewState[VIEW_STATE_IS_SESSION_TIMEOUT];
                    }
                    catch
                    {
                        return true;
                    }
                }
                else
                    return false;
            }
            set
            {
                ViewState[VIEW_STATE_IS_SESSION_TIMEOUT] = value;
            }
        }

        /// <summary>
        ///  Messages collection. Contains all custom warning or errors which will be populated on the page.
        /// </summary>
        protected PageMessages Messages
        {
            get
            {
                return (PageMessages)SessionParameters.PageMessages;
            }
            set
            {
                SessionParameters.PageMessages = value;
            }
        }
        #endregion



    }
}