﻿using Autofac;
using Autofac.Core;
//using Autofac.Integration.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services.Description;
using System.Web.SessionState;

namespace COTO_RegOnly
{
    public class Global : System.Web.HttpApplication
    {
        //static IContainerProvider _containerProvider;

        ///// <summary>
        ///// Gets the container.
        ///// </summary>
        //public IContainerProvider ContainerProvider
        //{
        //    get { return _containerProvider; }
        //}

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            var builder = new ContainerBuilder();
            builder.RegisterType<Component>().As<Autofac.Core.Service>().InstancePerRequest();
            // ... continue registering dependencies and then build the
            // container provider...
            //_containerProvider = new ContainerProvider(builder.Build());
        }


        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
