﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlPersonalInformation.ascx.cs" Inherits="COTO_RegOnly.Application.UserControls.ctlPersonalInformation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<script src="../Scripts/MaskedEditFix.js" type="text/javascript"></script>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="border: 0; border-spacing: 0; padding: 0; width: 100%">
                    <tr class="HeaderTitle" style="text-align:right;">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Registration Application Step 1 of 11" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <%--<asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;--%>
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" width="100%" cellspacing="2" cellpadding="3">
                                <tr class="RowTitle">
                                    <td  colspan="2">
                                        <div>
                                            <asp:Label ID="lblRegistrationApplicationSectionTitle" CssClass="heading" runat="server"
                                            Text="Registration Application" />
                                        </div>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td  colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn"  colspan="2">
                                        <span>
                                            This is the official application for individuals applying for registration with the College of Occupational Therapists of Ontario.
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td  colspan="2">
                                        <p><strong>Confidentiality and Public Access to Information </strong></p>
                                        <p>
                                            In the course of carrying out its regulatory activities, the College of Occupational Therapists of Ontario collects, 
                                            uses and discloses personal information in accordance with the <i>Regulated Health Professions Act, 1991</i>, 
                                            and the <i>Occupational Therapy Act, 1991</i>, and the regulations and bylaws made under those statutes. 
                                            While these regulatory activities are not of a commercial nature and therefore not subjected to the federal 
                                            <i>Personal Information Protection and Electronic Documents Act</i>, the College promotes the privacy 
                                            of personal information in a manner consistent with its Privacy Code.
                                        </p>
                                        <p>
                                            The purpose for collecting the information on this application is to assist the College in pursuing its regulatory activities, 
                                            for example, knowing where people work if a complaint comes in, planning quality assurance initiatives that will 
                                            best assist members, and providing professional information such as registration status, work contact information 
                                            to members of the public, and for national and provincial reporting for the purpose of health human resource planning. 
                                        </p>
                                        <p>
                                            While most information in the hands of the College is strictly confidential, the College is required and/or permitted by 
                                            the <i>Regulated Health Professions Act, 1991</i> (Section 2, Section 23) and the College bylaws (Section 17) 
                                            to make certain information about registrants available to the public. Health human resource planning information 
                                            is anonymized before it is shared.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="RightColumn">
                                        <asp:Label ID="lblPersonalInformationSectionTitle" runat="server" Text="Personal Information" Font-Bold="true"  Font-Size="Medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" />
                                    </td>
                                    <td style="text-align: right; font-weight:bold; padding-right: 20px;">
                                        <p>
                                            <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/Glossary-PersonalInformation.pdf" target="_blank">Glossary</a>
                                            <%--&nbsp;/&nbsp;<a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/Glossary-PersonalInformationFR.pdf" target="_blank">Glossaire</a>--%>
                                        </p>
                                    </td>
                                </tr>
                                <tr id="trDescriptionNew" runat="server">
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="lblEnterNamesTitle" runat="server" Text="Please enter your legal and commonly used names in practice" Font-Bold="true" />
                                    </td>
                                </tr>
                                <tr id="trDescription" runat="server">
                                    <td colspan="2" style="text-align: left;">
                                        <p>
                                            If details regarding your commonly used name are incorrect, please provide correct details. 
                                            If you need to make changes to legal names, please forward your request in writing to the College along with appropriate documentation, (e.g. marriage certificate).
                                        </p>
                                    </td>
                                </tr>
                                <tr id="trLegalLastName" runat="server" style="vertical-align: top;">
                                    <td class="LeftLeftTitle" style="width: 50%">
                                        <asp:Label ID="lblLegalLastNameTitle" runat="server" Text="Legal Last Name" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblLegalLastName" runat="server" />
                                        <asp:TextBox ID="txtLegalLastName" runat="server" Visible="false" />
                                        <asp:RequiredFieldValidator ID="rfvLegalLastName" runat="server" ControlToValidate="txtLegalLastName" Display="Dynamic"
                                        ErrorMessage="<br />Please provide Legal Last Name: Field is blank."  ValidationGroup="PersonalValidation" 
                                        ForeColor="Red" Enabled="false" />
                                    </td>
                                </tr>
                                <tr id="trLegalMiddleName" runat="server">
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblLegalMiddleNameTitle" runat="server" Text="Legal Middle Name" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblLegalMiddleName" runat="server" />
                                        <asp:TextBox ID="txtLegalMiddleName" runat="server" Visible="false" />
                                    </td>
                                </tr>
                                <tr id="trLegalFirstName" runat="server" style="vertical-align: top;">
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblLegalFirstNameTitle" runat="server" Text="Legal First Name" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblLegalFirstName" runat="server" />
                                        <asp:TextBox ID="txtLegalFirstName" runat="server" Visible="false" />
                                        <asp:RequiredFieldValidator ID="rfvLegalFirstName" runat="server" ControlToValidate="txtLegalFirstName" Display="Dynamic"
                                        ErrorMessage="<br />Please provide Legal First Name: Field is blank."  ValidationGroup="PersonalValidation" 
                                        ForeColor="Red" Enabled="false" />
                                    </td>
                                </tr>
                                <tr id="trPrevLegalLastName" runat="server">
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblPreviousLegalLastNameTitle" runat="server" Text="Previous Legal Last Name" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblPreviousLegalLastName" runat="server" />
                                        <asp:TextBox ID="txtPreviousLegalLastName" runat="server" Visible="false" />
                                        <%--<asp:RequiredFieldValidator ID="rfvPreviousLegalLastName" runat="server" ControlToValidate="txtPreviousLegalLastName" SetFocusOnError="True" 
                                        ErrorMessage="<br />Please provide Previous Legal Last Name: Field is blank." Display="Dynamic" ValidationGroup="PersonalValidation" 
                                        ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>--%>
                                    </td>
                                </tr>
                                <tr id="trPrevLegalFirstName" runat="server">
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="ibPreviousLegalFirstNameTitle" runat="server" Text="Previous Legal First Name" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblPreviousLegalFirstName" runat="server" />
                                        <asp:TextBox ID="txtPreviousLegalFirstName" runat="server" Visible="false" />
                                        <%--<asp:RequiredFieldValidator ID="rfvPreviousLegalFirstName" runat="server" ControlToValidate="txtPreviousLegalFirstName" SetFocusOnError="True" 
                                        ErrorMessage="<br />Please provide Previous Legal First Name: Field is blank." Display="Dynamic" ValidationGroup="PersonalValidation" 
                                        ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblCommonlyUsedLastNameTitle" runat="server" Text="Commonly Used Last Name in Practice" />&nbsp;*
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtCommonlyUsedLastName" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvCommonlyUsedLastName" runat="server" ControlToValidate="txtCommonlyUsedLastName" SetFocusOnError="True" 
                                        ErrorMessage="<br />Please provide Commonly Used Last Name: Field is blank." Display="Dynamic" ValidationGroup="PersonalValidation" 
                                        ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblCommonlyUsedMiddleNameTitle" runat="server" Text="Commonly Used Middle Name in Practice" />&nbsp;
                                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/qmark.jpg" runat="server"
                                            OnClick="ibCommonlyUsedLastNameClick" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtCommonlyUsedMiddleName" runat="server" />
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblCommonlyUsedFirstNameTitle" runat="server" Text="Commonly Used First Name in Practice" />&nbsp;*
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtCommonlyUsedFirstName" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvCommonlyUsedFirstName" runat="server" ControlToValidate="txtCommonlyUsedFirstName" SetFocusOnError="True" 
                                        ErrorMessage="<br />Please provide Commonly Used First Name: Field is blank." Display="Dynamic" ValidationGroup="PersonalValidation" 
                                        ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                
                                <tr id="trEditConfirmation" runat="server" visible="false">
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="lblUpdateConfirmation" runat="server" Text="Thank you. The information on common names has been received and will be updated by the College."
                                            ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblEmailLabel" runat="server" Text="Email: *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtEmailText" runat="server" Columns="35" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmailText"
                                            SetFocusOnError="True" ErrorMessage="<br />Please provide Email: Field is blank."
                                            Display="Dynamic" ValidationGroup="PersonalValidation" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailText"
                                            ErrorMessage="<br />Please provide a valid email" Display="Dynamic" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                            ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle" style="vertical-align:top;">
                                        <asp:Label ID="lblBirthDate" runat="server" Text="Birth Date: (MM/DD/YYYY) *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtBirthDate" runat="server" Enabled="false"/>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtBirthDate" Format="MM/dd/yyyy" />
                                        <%--<rjs:PopCalendar ID="PopCalendar1" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                            Format="dd/mm/yyyy" Separator="/" Control="txtBirthDate" From-Date="1900-01-01" Enabled="true"
                                            ValidationGroup="PersonalValidation" MessageAlignment="RightCalendarControl" />--%>
                                        <asp:RequiredFieldValidator ID="rfvBirthDate" runat="server" ControlToValidate="txtBirthDate"
                                            ErrorMessage="<br />Please provide Birth Date: field is blank." Display="Dynamic"
                                            ForeColor="Red" ValidationGroup="PersonalValidation" Enabled="false"></asp:RequiredFieldValidator>
                                        <asp:Label ID="lblBirthDateConvertError" runat="server" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblGender" runat="server" Text="Gender to which you identify: *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlGender" runat="server" OnSelectedIndexChanged="ddlGender_SelectedIndexChanged" AutoPostBack="true"/>
                                        <asp:RequiredFieldValidator ID="rfvGender" runat="server" ControlToValidate="ddlGender"
                                            InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide Gender information: Field is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trGenderSelfDescribe" runat="server" visible="false">
                                    <td class="LeftLeftTitle">
                                        &nbsp;
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtGenderSelfDescribe" runat="server" MaxLength="30" Columns="30" />
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="3" valign="top" class="LeftLeftTitle">
                                        <asp:Label ID="lblHomeAddressLabel" runat="server" Text="Home Address: *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtHomeAddress1Text" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvAddress1" runat="server" ControlToValidate="txtHomeAddress1Text"
                                            ForeColor="Red" ErrorMessage="<br />Please provide Address: Field is blank."
                                            Display="Dynamic" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtHomeAddress2Text" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtHomeAddress3Text" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblCountry" runat="server" Text="Country:" />&nbsp;*
                                    </td>
                                    <td class="RightColumn">
                                        <%--<asp:TextBox ID="txtCountry" runat="server" />--%>
                                        <asp:DropDownList ID="ddlCountry" runat="server" OnSelectedIndexChanged="ddlCountrySelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry"
                                            InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide Country: Field is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblCityLabel" runat="server" Text="City: *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtCityText" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCityText"
                                            ForeColor="Red" ErrorMessage="<br />Please provide City: Field is blank." Display="Dynamic"
                                            ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblProvince" runat="server" Text="State/Province: *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtProvince" runat="server" Visible="false" Columns="20" MaxLength="15" />
                                        <asp:DropDownList ID="ddlProvince" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvProvince" runat="server" ControlToValidate="ddlProvince"
                                            InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide State/Province: Field is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblPostalCode" runat="server" Text="Postal Code:" />&nbsp;*
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtPostalCode" runat="server" MaxLength="10" />
                                        <ajaxToolkit:MaskedEditExtender ID="meePostalCode" TargetControlID="txtPostalCode"
                                            Mask="LNLNLN" ClearMaskOnLostFocus="false" MaskType="None" ErrorTooltipEnabled="True"
                                            runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvPostalCode" runat="server" ControlToValidate="txtPostalCode"
                                            SetFocusOnError="True" ErrorMessage="<br />Please provide Postal Code: Field is blank."
                                            Display="Dynamic" ValidationGroup="PersonalValidation" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revPostalCode" runat="server" ControlToValidate="txtPostalCode"
                                            ErrorMessage="<br />Please provide a valid postal code without any spaces" Display="Dynamic"
                                            ValidationExpression="^([abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][a-zA-Z][0-9][a-zA-Z][0-9])$"
                                            ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblHomePhoneLabel" runat="server" Text="Home Phone: *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:TextBox ID="txtHomePhoneText" runat="server" MaxLength="24" />
                                        <ajaxToolkit:MaskedEditExtender ID="meeHomePhone" TargetControlID="txtHomePhoneText"
                                            Mask="(999) 999-9999" ClearMaskOnLostFocus="false" MaskType="None" InputDirection="LeftToRight"
                                            ErrorTooltipEnabled="True" runat="server" Filtered="xX"/>
                                        <asp:RequiredFieldValidator ID="rfvHomePhone" runat="server" ControlToValidate="txtHomePhoneText"
                                            ErrorMessage="<br />Please provide Home Phone: field is blank." Display="Dynamic"
                                            ForeColor="Red" ValidationGroup="PersonalValidation"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revHomePhone" runat="server" ControlToValidate="txtHomePhoneText" ErrorMessage="<br />Please provide Home Phone: Phone number is invalid." 
                                            ForeColor="Red" Display="Dynamic" ValidationGroup="PersonalValidation" ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}$"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <%--<asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                OnClick="ibtnBackClick" />&nbsp;&nbsp;--%>
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>