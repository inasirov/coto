﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlCurrencyLanguage.ascx.cs"
    Inherits="COTO_RegOnly.Application.UserControls.ctlCurrencyLanguage" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="border: 0; border-spacing: 0; padding: 0; width: 100%">
                    <tr class="HeaderTitle" style="text-align: right;">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Registration Application Step 3 of 11" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" style="border: 0; border-spacing: 3px; padding: 2px; width: 100%">
                                <tr class="RowTitle">
                                    <td colspan="2">
                                        <div>
                                            <asp:Label ID="lblCurrencyLanguageTitle" CssClass="heading" runat="server"
                                            Text="Currency and Language" />
                                        </div>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align:left;">
                                        <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" /><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="RightColumn">
                                        <asp:Label ID="lblCurrencyHoursTitle" runat="server"
                                            Text="Currency Hours *" Font-Bold="true"  Font-Size="Medium" /><br />
                                        <asp:Label ID="lblCurrencyHoursDesc" runat="server" 
                                        Text="Please select the criterion that applies to you *:"/><br /><br />
                                        <asp:DropDownList ID="ddlCurrencyHours" runat="server" OnSelectedIndexChanged="ddlCurrencyHoursSelectedIndexChanged" AutoPostBack="true" Width="630px"  />
                                        <%--<ajax:ComboBox ID="ddlCurrencyHours" runat="server" DropDownStyle="DropDownList"  OnSelectedIndexChanged="ddlCurrencyHoursSelectedIndexChanged" AutoPostBack="true" Width="700px"  />--%>
                                        <asp:RequiredFieldValidator ID="rfvCurrencyHours" runat="server" ControlToValidate="ddlCurrencyHours"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Currency Hours is blank."
                                        Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="RightColumn">
                                        <asp:Label ID="lblCurrencyHoursDescription" runat="server" ForeColor="DarkRed" Font-Bold="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align:left;">
                                        <asp:Label ID="lblLanguageOfServiceTitle" runat="server" Font-Bold="true" Font-Size="Medium"  Text="Language" />&nbsp;
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td style="width:40%" class="LeftLeftTitle">
                                        <asp:Label ID="lblFirstLanguageTitle" runat="server"
                                            Text="First Language: *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlFirstLanguage" runat="server" OnSelectedIndexChanged="ddlFirstLanguageSelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvFirstLanguage" runat="server" ControlToValidate="ddlFirstLanguage"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />First Language is blank."
                                        Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:40%" class="LeftLeftTitle">
                                        <asp:Label ID="lblEducationInstructionTitle" runat="server"
                                            Text="Language of OT Education Instruction: *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlEducationLanguage" runat="server" OnSelectedIndexChanged="ddlEducationLanguageSelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvEducationalLanguage" runat="server" ControlToValidate="ddlEducationLanguage"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Language of OT Educational Instruction is blank."
                                        Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:40%" class="LeftLeftTitle">
                                        <asp:Label ID="lblPreferredDocumentationLanguageTitle" runat="server"
                                            Text="Preferred Language for Documentation from the College: *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlPreferredDocumentationLanguage" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvPreferredDocumentationLanguage" runat="server" ControlToValidate="ddlPreferredDocumentationLanguage"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Preferred Language for Documentation from the College is blank."
                                        Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" id="trMessage" runat="server" class="RightColumn">
                                        <asp:Label ID="lblMessage" runat="server" Font-Bold="true" ForeColor="DarkRed" Text="Proof of language fluency is required if your first language is not English or French, or if the language of occupational therapy instruction was not English or French. Please refer to the following link for the College's language fluency policy for approved language tests: " /><br/>
                                        <asp:LinkButton ID="lbtnLanguageRequirements" runat="server" Text="(8-81) Language Fluency Requirement" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="RightColumn">
                                        <asp:Label ID="lblLanguageOfServiceDesc" runat="server" Text="Please provide the languages in which you can personally and competently provide professional services. (Up to five, as applicable)" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:40%" class="LeftLeftTitle">
                                        <asp:Label ID="lblService1Title" runat="server"
                                            Text="Language of Service 1*" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlService1" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvService1" runat="server" ControlToValidate="ddlService1"
                                        InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Language of Service 1 is blank."
                                        Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle"> 
                                        <asp:Label ID="lblService2Title" runat="server"
                                            Text="Language of Service 2" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlService2" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblService3Title" runat="server"
                                            Text="Language of Service 3" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlService3" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblService4Title" runat="server"
                                            Text="Language of Service 4" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlService4" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LeftLeftTitle">
                                        <asp:Label ID="lblService5Title" runat="server"
                                            Text="Language of Service 5" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlService5" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right;">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Next >" OnClick="btnUpdateClick"
                                ValidationGroup="PersonalValidation" />--%>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
