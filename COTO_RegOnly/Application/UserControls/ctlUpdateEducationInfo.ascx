﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlUpdateEducationInfo.ascx.cs" Inherits="COTO_RegOnly.Application.UserControls.ctlUpdateEducationInfo" %>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="border: 0; border-spacing: 0px; padding: 0px; width: 100%">
                    <tr class="HeaderTitle" style="text-align:left;">
                        <td colspan="2">
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Update Education Profile" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red" />
                        </td>   
                    </tr>
                    <tr>
                        <td style="text-align:left;" colspan="2">
                            <p>
                                Please provide details on the information that has changed in your education profile. 
                            </p>
                            <p>
                                Changes will be received and updated by the College. 
                                We may contact you if we require additional information.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span style="font-weight:bold">Education Change Details:</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEducationProfileChanges" runat="server" Rows="6" Columns="60" TextMode="MultiLine" />
                        </td>
                    </tr>
                    <%--<tr>
                        <td colspan="2" style="text-align:left;">
                            <p>
                                The information will be received and entered by College staff and will not appear automatically during your renewal.
                            </p>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:right; vertical-align: middle; background-color: #ffffff" colspan="2">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="btnSubmit" runat="server" ImageUrl="~/Images/btn_update.jpg" OnClick="btnSubmitClick" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>