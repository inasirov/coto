﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlInsurance.ascx.cs" Inherits="COTO_RegOnly.Application.UserControls.ctlInsurance" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>

<style type="text/css">
    .style1
    {
        height: 23px;
    }
</style>

<div class="MainForm">
    <center>
    <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:PostBackTrigger ControlID="ddlPlanHeldWith" />
            </Triggers>
            <ContentTemplate>
        <table style="border: 0; border-spacing: 0; padding: 0; width: 100%">
            <tr class="HeaderTitle" style="text-align: right;">
                <td>
                    <asp:Label ID="lblPageTitleLabel" runat="server" Text="Registration Application Step 9 of 11" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <uc:MessageBox ID="omb" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <table class="memberInfo" style="border-spacing: 2px; padding: 3px; width: 100%">
                        <tr class="RowTitle">
                            <td colspan="2">
                                <div>
                                    <asp:Label ID="lblInsuranceTitle" CssClass="heading"  runat="server" Text="Professional Liability Insurance and Examination" />
                                </div>
                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblErrorMessages" runat="server" ForeColor="Red" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblProfessionalLiabilityTitle" runat="server" Text="Professional Liability Insurance" Font-Bold="true"
                                    Font-Size="Medium" /><br /><br />
                                <p>Professional liability insurance with a sexual abuse therapy and counseling fund endorsement is mandatory for registration. 
                                    The requirements for professional liability insurance are set out in <a href='https://www.coto.org/docs/default-source/bylaws/2017_bylaws.pdf?sfvrsn=2' target="_blank">Part 19 of the College By-law</a>. 
                                    The College is not affiliated with and does not endorse any insurance provider.</p>
                                <p>The College has confirmed that the insurance coverage offered (as of October 1, 2017) by the Canadian Association of Occupational Therapists (CAOT), 
                                    the Ontario Society of Occupational Therapists (OSOT) and Aon Healthcare Advantage (Insurance Solutions for Occupational Therapists) 
                                    meets the College requirements. </p>
                                <p>You are required to send a copy of your insurance certificate which confirms the purchase of the insurance and includes the policy number and expiry date. </p>
                                <%--<asp:Label ID="lblProfessionalLiabilityDesc" runat="server" Text="Documentation verifying purchase of insurance stating the certificate number and expiry date is required for all applications. If your insurance is not with the Canadian Association of Occupational Therapists (CAOT), the Ontario Society of Occupational Therapists (OSOT) or Aon Healthcare Advantage you must also forward a complete policy description. If you do not know your certificate number, please leave this field blank and send the College a copy of the policy once available.  A certificate of registration will not be issued until acceptable proof of insurance is received." />--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width:30%; text-align: left;">
                                <asp:Label ID="lblPlanHeldWithTitle" CssClass="LeftTitle" runat="server" Text="Insurer Name: *" />
                                <asp:HiddenField ID="hfInsuranceSEQN" runat="server" />
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlPlanHeldWith" runat="server" OnSelectedIndexChanged="ddlPlanHeldWithSelectedIndexChanged" AutoPostBack="true" />
                                <asp:RequiredFieldValidator ID="rfvPlanHeldWith" runat="server" ControlToValidate="ddlPlanHeldWith" EnableClientScript="false"
                                InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select a value" Display="Dynamic" ForeColor="Red" />
                            </td>
                        </tr>
                        <tr id="trOtherInsurance" runat="server" visible="false">
                            <td style="text-align: left; vertical-align: top;">
                                <asp:Label ID="lblOtherTitle" runat="server" CssClass="LeftTitle" Text="Other (name of insurance provider if applicable): " />
                            </td>
                            <td class="RightColumn">
                                <asp:TextBox ID="txtOther" runat="server" Columns="60" Width="240px" />
                                <asp:RequiredFieldValidator ID="rfvOther" runat="server" ControlToValidate="txtOther" EnableClientScript="false"
                                ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide a name of insurance provider" Display="Dynamic" ForeColor="Red" />
                                <br />
                                <asp:Label ID="lblSendFaxMessage" runat="server" Font-Bold="true" ForeColor="DarkRed" Text="You must submit a complete policy description to the College for review." />

                            </td>
                        </tr>
                        <tr id="trCertificate" runat="server">
                            <td style="text-align: left;">
                                <asp:Label ID="lblCertificate" runat="server" CssClass="LeftTitle" Text="Policy Number:" />
                            </td>
                            <td class="RightColumn">
                                <asp:TextBox ID="txtCertificate" runat="server" MaxLength="10" />
                                <%--<asp:RequiredFieldValidator ID="rfvCertificate" runat="server" ControlToValidate="txtCertificate" EnableClientScript="false"
                                ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide Certificate #" Display="Dynamic" ForeColor="Red" />
                                <asp:RegularExpressionValidator ID="revCertificate" runat="server" ControlToValidate="txtCertificate" EnableClientScript="false" 
                                ValidationGroup="PersonalValidation" ErrorMessage="<br />Certificate # can have only 6 digits" ValidationExpression="^\d(\d)?(\d)?(\d)?(\d)?(\d)?$"  Display="Dynamic" ForeColor="Red" />--%>
                            </td>
                        </tr>
                        <tr id="trStartDate" runat="server">
                            <td style="text-align: left;">
                                <asp:Label ID="lblStartDate" runat="server" CssClass="LeftTitle" Text="Start Date (MM/DD/YYYY): *" />
                            </td>
                            <td class="RightColumn">
                                <asp:TextBox ID="txtStartDate" runat="server" />&nbsp;
                                <ajaxToolkit:CalendarExtender ID="ceStartDate" runat="server" TargetControlID="txtStartDate" Format="MM/dd/yyyy" />
                                <%--<rjs:PopCalendar ID="rjsExpiryDate" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtExpiryDate" From-Date="1990-01-01"
                                                                    MessageAlignment="RightCalendarControl" />--%>
                                <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="txtStartDate" EnableClientScript="false"
                                ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select Start Date" Display="Dynamic" ForeColor="Red" />
                            </td>
                        </tr>
                        <tr id="trExpiryDate" runat="server">
                            <td style="text-align: left;">
                                <asp:Label ID="lblExpiryDateTitle" runat="server" CssClass="LeftTitle" Text="Expiration Date (MM/DD/YYYY): *" />
                            </td>
                            <td class="RightColumn">
                                <asp:TextBox ID="txtExpiryDate" runat="server" />&nbsp;
                                <ajaxToolkit:CalendarExtender ID="ceExpiryDate" runat="server" TargetControlID="txtExpiryDate" Format="MM/dd/yyyy" />
                                <%--<rjs:PopCalendar ID="rjsExpiryDate" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtExpiryDate" From-Date="1990-01-01"
                                                                    MessageAlignment="RightCalendarControl" />--%>
                                <asp:RequiredFieldValidator ID="rfvExpiryDate" runat="server" ControlToValidate="txtExpiryDate" EnableClientScript="false"
                                ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select Expiry Date" Display="Dynamic" ForeColor="Red" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblExaminationTitle" runat="server" Text="Examination" Font-Bold="true"
                                    Font-Size="Medium" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                <asp:Label ID="lblSelectCategoryTitle" runat="server" CssClass="LeftTitle" Text="Please select the category from dropdown that applies to you: *" />
                            </td>
                            <td  class="RightColumn" style="vertical-align: top;">
                                <asp:DropDownList ID="ddlExaminationCategory" runat="server" OnSelectedIndexChanged="ddlExaminationCategorySelectedIndexChanged" AutoPostBack="true" style="width: 500px;" />
                                <asp:RequiredFieldValidator ID="rfvExaminationCategory" runat="server" ControlToValidate="ddlExaminationCategory" EnableClientScript="false"
                                InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select the category" Display="Dynamic" ForeColor="Red" />
                            </td>
                        </tr>
                        <tr id="trCAOTExamCompleted" runat="server">
                            <td style="text-align: left;">
                                <asp:Label ID="lblDateCompletionTitle" runat="server" CssClass="LeftTitle" Text="Date of Completion (MM/DD/YYYY)*" />
                            </td>
                            <td  class="RightColumn">
                                <asp:TextBox ID="txtDateExamCompletion" runat="server" />&nbsp;
                                <ajaxToolkit:CalendarExtender ID="ceDateExamCompletion" runat="server" TargetControlID="txtDateExamCompletion" Format="MM/dd/yyyy" EndDate='<%# DateTime.Now.AddDays(-1) %>' />
                                <%--<rjs:PopCalendar ID="rjsDateExamCompletion" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy" Format="dd/mm/yyyy" Separator="/" Control="txtDateExamCompletion" From-Date="1990-01-01"
                                MessageAlignment="RightCalendarControl" />--%>
                                <asp:RequiredFieldValidator ID="rfvDateExamCompletion" runat="server" ControlToValidate="txtDateExamCompletion" EnableClientScript="false"
                                ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select Date of Completion" Display="Dynamic" ForeColor="Red" />
                            </td>
                        </tr>
                        <tr  id="trCAOTExamCompleted2" runat="server">
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblCAOTExamCompletedDescr" runat="server" ForeColor="DarkRed" Font-Bold="true" Text="You must forward a copy of your exam results to the College, if not already on file with the College." />
                            </td>
                        </tr>
                        <tr id="trCAOTExamRegistered" runat="server">
                            <td style="text-align: left;">
                                <asp:Label ID="lblDateRegistrationTitle" runat="server" CssClass="LeftTitle" Text="Date of Exam *" />
                            </td>
                            <td class="RightColumn">
                                <%--<asp:TextBox ID="txtDateExamRegistration" runat="server" />&nbsp;--%>
                                <%--<ajaxToolkit:CalendarExtender ID="ceDateExamRegistration" runat="server" TargetControlID="txtDateExamRegistration" Format="dd/MM/yyyy" StartDate='<%# DateTime.Now.AddDays(1) %>' />--%>
                                <asp:DropDownList ID="ddlDateExamRegistration" runat="server" /> 
                                <%--<rjs:PopCalendar ID="rjsDateExamRegistration" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtDateExamRegistration" From-Date="1990-01-01"
                                                                    MessageAlignment="RightCalendarControl" />--%>
                                <%--<asp:RequiredFieldValidator ID="rfvDateExamRegistration" runat="server" ControlToValidate="txtDateExamRegistration" EnableClientScript="false"
                                ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select Date of Exam" Display="Dynamic" ForeColor="Red" />--%>
                                <asp:RequiredFieldValidator ID="rfvDateExamRegistration" runat="server" ControlToValidate="ddlDateExamRegistration" EnableClientScript="false"
                                ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select Date of Exam" Display="Dynamic" ForeColor="Red"  />
                            </td>
                        </tr>
                        <tr id="trCAOTExamRegistered2" runat="server">
                            <td colspan="2" style="text-align: left;" class="style1">
                                <asp:Label ID="lblCAOTExamRegisteredDescr" runat="server" ForeColor="DarkRed" Font-Bold="true" Text="You must forward proof of registration for the CAOT exam to the College. An email or letter from CAOT confirming registration is considered acceptable proof." />
                            </td>
                        </tr>
                        <tr id="trLMSA" runat="server">
                            <td colspan="2" style="text-align: left;">
                                <p style="color: DarkRed; font-weight:bold">If you wish to apply under the Labour Mobility Support Agreement, you must arrange for completion 
                                    of the 
                                    <asp:LinkButton ID="lbtnLaborMobilitySupportAgreementFormLink" runat="server" Text="Labour Mobility Support Agreement Confirmation Form" />&nbsp;by the regulatory organization with which you are currently regulated. 
                                    A 
                                    <asp:LinkButton ID="lbtnRegulatoryHistoryFormLink" runat="server" Text="Regulatory History Form" />&nbsp;confirming your current regulatory history is also required.
                                </p>
                                <p style="color: DarkRed; font-weight:bold">
                                    Complete the Authorization for Release of Information portion of the forms, send them to your current regulatory organization(s) 
                                    and instruct them to return the completed forms directly to the College of Occupational Therapists on Ontario.
                                </p>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                </td>
            </tr>
        </table>
        </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>