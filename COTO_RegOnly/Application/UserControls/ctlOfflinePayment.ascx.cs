﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlOfflinePayment : System.Web.UI.UserControl
    {
        private string PrevStep = WebConfigItems.Application_Step11;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.Application_Step0);
                return;
            }
            lbtnPrintReport.Attributes.Add("onclick", string.Format("javascript:window.open('{0}?ReportID={1}', '_blank')", WebConfigItems.Application_Step11_PDF_Report, CurrentUserId));
            //ReadPdfDoc();
            //BindData();
            //DataBind();
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(PrevStep);
        }

        protected void lbtnPrintFormClick(object sender, EventArgs e)
        {
            
            var repository = new Repository();
            var user = repository.GetUserInfo(CurrentUserId);
            if (user != null)
            {
                
                string fullName = user.FirstName + " " + user.LastName;
                string major_Key = user.Registration;

                // new version
                string fileName = HttpContext.Current.Server.MapPath("~/Report/Online_Application_Offline_Payment_Form.pdf");

                
                MemoryStream ms = PopulatePDF(fileName, fullName);

                //HttpContext.Current.Response.ClearContent();
                //HttpContext.Current.Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=Payment_Form.pdf");
                HttpContext.Current.Response.BinaryWrite(ms.ToArray());
                //HttpContext.Current.Response.End();
                ms.Dispose();
                Response.Buffer = true;
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.ContentType = ContentType;
                Response.Flush();

                // end of new version
            }
        }

        private MemoryStream PopulatePDF(string fileName, string FullName)
        {
            MemoryStream ms = new MemoryStream();
            PdfStamper Stamper = null;
            PdfReader Reader = new PdfReader(fileName);
            try
            {
                PdfCopyFields Copier = new PdfCopyFields(ms);
                Copier.AddDocument(Reader);
                Copier.Close();

                PdfReader docReader = new PdfReader(ms.ToArray());
                ms = new MemoryStream();
                
                //PdfReader docReader = new PdfReader(ms.ToArray());
                Stamper = new PdfStamper(docReader, ms);
                AcroFields Fields = Stamper.AcroFields;

                ////fill form fields here                      
                //Fields.SetField("topmostSubform[0].Page1[0].Full_Name[0]", FullName);
                //Fields.SetField("topmostSubform[0].Page1[0].Registration_Number[0]", Registration);
                //Fields.SetField("topmostSubform[0].Page1[0].TextField1[0]", CurrentUserId);
                var repository = new Repository();
                decimal Amount = 0;
                var subscriptions = repository.GetUserApplicationSubscriptions(CurrentUserId);
                if (subscriptions != null)
                {
                    var mainFee = subscriptions.Find(I => !I.ProductCode.ToUpper().Contains("HST"));
                    var hstFee = subscriptions.Find(I => I.ProductCode.ToUpper().Contains("HST"));
                    Amount = subscriptions.Sum(I => I.Amount);
                }

                Fields.SetField("Text Field 1 - Name", FullName); // conf
                Fields.SetField("Text Field 2 - ID", CurrentUserId); 
                Fields.SetField("Text Field 3 - Amount", Amount.ToString("C2"));
                Fields.SetField("Text1", Amount.ToString("C2"));
                Fields.SetField("Text Field 76", FullName);  //conf
                //Fields.SetField("topmostSubform[0].Page1[0].TextField1[4]", Registration);
                //Fields.SetField("topmostSubform[0].Page1[0].TextField1[5]", "05");
                //Fields.SetField("topmostSubform[0].Page1[0].TextField6[0]", CurrentUserId);
                //Fields.SetField("topmostSubform[0].Page1[0].TextField7[0]", CurrentUserId);
                //Fields.SetField("topmostSubform[0].Page1[0].TextField8[0]", CurrentUserId);
                //Fields.SetField("topmostSubform[0].Page1[0].SignatureField1[0]", "SF00");

                //foreach(var item in Fields.Fields)
                //{
                //    lblTest.Text += string.Format("Field: key={0}, value={1}<br />", item.Key, item.Value);
                //}

                Stamper.FormFlattening = false;
            }
            finally
            {
                if (Stamper != null)
                {
                    Stamper.Close();
                }
                //Reader.Close();
            }
            return ms;
        }

        protected void ReadPdfDoc()
        {
            var repository = new Repository();
            var user = repository.GetUserInfo(CurrentUserId);
            if (user != null)
            {

                string fullName = user.FirstName + " " + user.LastName;
                string fileName = HttpContext.Current.Server.MapPath("~/Report/Online_Application_Offline_Payment_Form.pdf");

                MemoryStream ms = new MemoryStream();
                PdfStamper Stamper = null;
                PdfReader Reader = new PdfReader(fileName);
                try
                {
                    PdfCopyFields Copier = new PdfCopyFields(ms);
                    Copier.AddDocument(Reader);
                    Copier.Close();

                    PdfReader docReader = new PdfReader(ms.ToArray());
                    ms = new MemoryStream();

                    Stamper = new PdfStamper(docReader, ms);
                    AcroFields Fields = Stamper.AcroFields;

                    foreach (var item in Fields.Fields)
                    {
                        lblTest.Text += string.Format("Field: key={0}, value={1}<br />", item.Key, item.Value);
                    }
                    Stamper.FormFlattening = false;
                }
                finally
                {
                    if (Stamper != null)
                    {
                        Stamper.Close();
                    }
                    ms.Dispose();
                }
            }
        }

        protected void BindData()
        {
            //hlContact.NavigateUrl = string.Format("mailto:{0}", WebConfigItems.ApplicationSupportEmail);
            var repository = new Repository();

            var user2 = repository.GetApplicationUserInfo(CurrentUserId);
            if (user2 != null)
            {
                lblPersonalInformationSectionTitle.Text = string.Format("Offline Payment for {0} #{1}", user2.FullName, CurrentUserId);
            }
        }

        protected void lbtnPrintReportClick(object sender, EventArgs e)
        {
            //Session["ReportID"] = CurrentUserId;
            //Response.Redirect(WebConfigItems.Application_Step11_PDF_Report);
        }

        protected void lbtnFeedbackLinkClick(object sender, EventArgs e)
        {
            SessionParameters.ReturnUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            Response.Redirect(WebConfigItems.Application_Step11_Feedback);
        }

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        #endregion

    }
}