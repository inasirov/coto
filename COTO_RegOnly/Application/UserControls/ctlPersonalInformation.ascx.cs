﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlPersonalInformation : System.Web.UI.UserControl
    {

        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;

        //private string PrevStep = WebConfigItems.Application_Step0;
        private string NextStep = WebConfigItems.Application_Step2;
        private const int CurrentStep = 1;

        #endregion

        #region Events

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.ClientScript.IsStartupScriptRegistered(GetType(), "MaskedEditFix"))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "MaskedEditFix", String.Format("<script type='text/javascript' src='{0}'></script>", Page.ResolveUrl("~/Scripts/MaskedEditFix.js")));
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "MaskedEditFix", String.Format("<script type='text/javascript' src='{0}'></script>", Page.ResolveUrl("~/Scripts/MaskedEditFix.js")), true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Buffer = true;
            Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
            Response.Expires = -1500;
            Response.Cache.SetNoStore();
            Response.Cache.AppendCacheExtension("no-cache");
            //lblBirthDateConvertError.Text = string.Empty;
            if (Request.QueryString["common"] != null && Request.QueryString["common"] == "updated")
            {
                trEditConfirmation.Visible = true;
            }
            else
            {
                trEditConfirmation.Visible = false;
            }

            if (Request.QueryString.Count > 1)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.Application_Step0);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.ApplicationStep = CurrentStep;
                BindLists();
                BindData();
            }
        }

        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            if (ValidationCheck())
            {
                if (UpdateUserInfo())
                {
                    //Response.Redirect("MemberDisplay.aspx");
                    UpdateSteps(1);
                    Response.Redirect(NextStep);
                }
            }
        }

        protected void btnEditProfileClick(object sender, EventArgs e)
        {
            Response.Redirect(WebConfigItems.Application_Step2_UpdatePersonalInfo);
        }

        protected void ddlProvinceSelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void ddlCountrySelectedIndexChanged(object sender, EventArgs e)
        {
            var repository = new Repository();

            meePostalCode.Enabled = false;
            revPostalCode.Enabled = false;
            revPostalCode.Enabled = false;

            if (ddlCountry.SelectedItem.Text.ToUpper().Contains("CANADA") || ddlCountry.SelectedItem.Text.ToUpper().Contains("UNITED STATES"))
            {
                txtProvince.Visible = false;
                txtProvince.Text = string.Empty;
                txtPostalCode.Text = string.Empty;
                ddlProvince.Visible = true;
                ddlProvince.SelectedIndex = 0;
                rfvProvince.Enabled = true;
                meeHomePhone.Enabled = true;
                revHomePhone.Enabled = true;
                if (ddlCountry.SelectedItem.Text.ToUpper().Contains("CANADA"))
                {
                    var list0 = repository.GetCanadianProvincesListNew();

                    list0.Insert(0, new GenClass { Code = "", Description = "" });

                    ddlProvince.DataSource = list0;
                    ddlProvince.DataValueField = "Code";
                    ddlProvince.DataTextField = "Description";
                    ddlProvince.DataBind();

                    meePostalCode.Enabled = true;
                    revPostalCode.Enabled = true;
                }
                if (ddlCountry.SelectedItem.Text.ToUpper().Contains("UNITED STATES"))
                {
                    var list1 = repository.GetUSStatesList();

                    list1.Insert(0, new GenClass { Code = "", Description = "" });

                    ddlProvince.DataSource = list1;
                    ddlProvince.DataValueField = "Code";
                    ddlProvince.DataTextField = "Description";
                    ddlProvince.DataBind();
                }
            }
            else
            {
                txtProvince.Visible = true;
                ddlProvince.Visible = false;
                rfvProvince.Enabled = false;
                meeHomePhone.Enabled = false;
                revHomePhone.Enabled = false;
            }
        }

        #endregion

        #region Methods

        protected bool ValidationCheck()
        {
            lblBirthDateConvertError.Text = string.Empty;
            bool valid = true;

            // if birth date text field is enabled - we need to check if it's older than 18 years
            if (txtBirthDate.Enabled)
            {
                DateTime birthDate = DateTime.MinValue;
                CultureInfo provider = CultureInfo.InvariantCulture;
                string format = "MM/dd/yyyy";
                if (!string.IsNullOrEmpty(txtBirthDate.Text) && DateTime.TryParseExact(txtBirthDate.Text, format, provider, DateTimeStyles.None, out birthDate))
                {
                    TimeSpan Span = DateTime.Today - birthDate;
                    DateTime Age = DateTime.MinValue + Span;
                    int Years = Age.Year - 1;
                    if (Years < 18)
                    {
                        lblBirthDateConvertError.Text = "<br />Applicant must be 18 years of age or older.";
                        valid = false;
                    }
                }
                else
                {
                    lblBirthDateConvertError.Text = "<br />Birth date has invalid format";
                    valid = false;
                }
            }
            return valid;
        }

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.ApplicationStep != 0)
            {
                SessionParameters.ApplicationStep += diff;
            }
        }

        protected bool UpdateUserInfo()
        {
            User user = new User();
            user.Email = txtEmailText.Text;

            user.Id = CurrentUserId;
            txtHomePhoneText.Text = txtHomePhoneText.Text.Replace("_", string.Empty);
            if (txtHomePhoneText.Text.Last() == 'x')
            {
                txtHomePhoneText.Text = txtHomePhoneText.Text.Replace("x", string.Empty);
            }
            user.HomePhone = txtHomePhoneText.Text;
            user.HomeAddress = new Address();
            user.HomeAddress.Address1 = txtHomeAddress1Text.Text;
            user.HomeAddress.Address2 = txtHomeAddress2Text.Text;
            user.HomeAddress.Address3 = txtHomeAddress3Text.Text;

            string status = CurrentUser.CurrentStatus.ToUpper().Contains("FORMER") ? "Returning" : "New";
            user.CurrentStatus = status;
            if (status == "New")
            {
                
                user.PreviousLegalFirstName = txtPreviousLegalFirstName.Text;
                user.PreviousLegalLastName = txtPreviousLegalLastName.Text;
            }

            user.CommonlyUsedFirstName = txtCommonlyUsedFirstName.Text;
            user.CommonlyUsedLastName = txtCommonlyUsedLastName.Text;
            //user.CommonlyUsedMiddleName = txtCommonlyUsedMiddleName.Text;

            if (txtLegalLastName.Visible)
            {
                user.LegalLastName = txtLegalLastName.Text;
            }
            else
            {
                user.LegalLastName = lblLegalLastName.Text;
            }

            if (txtLegalFirstName.Visible)
            {
                user.LegalFirstName = txtLegalFirstName.Text;
            }
            else
            {
                user.LegalFirstName = lblLegalFirstName.Text;
            }

            if (txtLegalMiddleName.Visible)
            {
                user.LegalMiddleName = txtLegalMiddleName.Text;
            }
            else
            {
                user.LegalMiddleName = lblLegalMiddleName.Text;
            }

            bool saveBirthDate = false;

            if (txtBirthDate.Enabled)
            {
                saveBirthDate = true;

                if (!string.IsNullOrEmpty(txtBirthDate.Text))
                {
                    DateTime birthDate;
                    CultureInfo provider = CultureInfo.InvariantCulture;
                    string format = "MM/dd/yyyy";
                    if (DateTime.TryParseExact(txtBirthDate.Text, format, provider, DateTimeStyles.None, out birthDate))
                    {
                        user.BirthDate = birthDate;
                    }
                    else
                    {
                        lblBirthDateConvertError.Text = "<br/>Birth date has invalid format";
                        return false;
                    }
                }
                else
                {
                    lblBirthDateConvertError.Text = "<br />Birth date has invalid format";
                    return false;
                }
            }

            user.HomeAddress.City = txtCityText.Text;
            //user.HomeAddress.Province = ddlProvince.SelectedValue;
            //user.HomeAddress.Province = txtProvince.Text;
            user.HomeAddress.PostalCode = txtPostalCode.Text.ToUpper();
            user.HomeAddress.Country = ddlCountry.SelectedItem.Text; 
            
            if (ddlProvince.Visible)
            {
                user.HomeAddress.Province = ddlProvince.SelectedValue;
            }
            else
            {
                user.HomeAddress.Province = txtProvince.Text;
            }

            user.Gender = ddlGender.SelectedValue;
            if (user.Gender == "SelfDescribe")
            {
                user.GenderSelfDescribe = txtGenderSelfDescribe.Text;
            }
            

            var repository = new Repository();
            string message = repository.UpdateApplicationUserInfoLogged(CurrentUserId, user, saveBirthDate);
            if (!string.IsNullOrEmpty(message))
            {
                ShowMessage(message);
                return false;
            }

            //// save all changes to name_log table
            //if (CurrentUser != null)
            //{
            //    if (CurrentUser.Email != user.Email)
            //    {
            //        // repository.AddLogEntry( DateTime.Now, "Change", 
            //    }
            //}
            return true;
        }

        //protected bool ValidationCheck()
        //{

        //}

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        protected void BindLists()
        {
            var repository = new Repository();

            var list1 = repository.GetCanadianProvincesListNew();

            list1.Insert(0, new GenClass { Code = "", Description = "" });

            ddlProvince.DataSource = list1;
            ddlProvince.DataValueField = "Code";
            ddlProvince.DataTextField = "Description";
            ddlProvince.DataBind();

            //var list2 = Countries.ListOfCountries;

            var list2 = repository.GetGeneralList("COUNTRY");
            //var list2 = repository.GetCountriesNew();

            ddlCountry.DataSource = list2;
            ddlCountry.DataValueField = "Code";
            ddlCountry.DataTextField = "Description";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, string.Empty);

            var list3 = new List<GenClass>();
            list3.Insert(0, new GenClass { Code = "", Description = "" });
            list3.Add(new GenClass { Code = "Female", Description = "Female" });
            list3.Add(new GenClass { Code = "Male", Description = "Male" });
            list3.Add(new GenClass { Code = "SelfDescribe", Description = "Prefer to self-describe" });

            ddlGender.DataSource = list3;
            ddlGender.DataValueField = "Code";
            ddlGender.DataTextField = "Description";
            ddlGender.DataBind();

        }

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetApplicationUserInfo(CurrentUserId);
            if (user != null)
            {
                CurrentUser = user; // saved previous information about user to viewstate

                //lblPersonalInformationSectionTitle.Text = string.Format("Personal Information for {0} #{1}", user.FullName, user.Id);
                lblRegistrationApplicationSectionTitle.Text = string.Format("Registration Application for {0} #{1}", user.FullName, user.Id);
                txtEmailText.Text = user.Email;
                txtHomePhoneText.Text = user.HomePhone;
                string status = string.Empty;
                if (user.CurrentStatus.ToUpper().Contains("FORMER"))
                {
                    trDescription.Visible = true;
                    status = "Returning";
                    trDescriptionNew.Visible = false;
                }
                else
                {
                    trDescription.Visible = false;
                    status = "New";
                    trDescriptionNew.Visible = true;
                }


                if (!string.IsNullOrEmpty(user.LegalLastName))
                {
                    lblLegalLastName.Text = user.LegalLastName;
                }
                else
                {
                    lblLegalLastName.Text = string.Empty;
                    txtLegalLastName.Visible = true;
                    rfvLegalLastName.Enabled = true;
                }

                if (!string.IsNullOrEmpty(user.LegalFirstName))
                {
                    lblLegalFirstName.Text = user.LegalFirstName;
                }
                else
                {
                    lblLegalFirstName.Text = string.Empty;
                    txtLegalFirstName.Visible = true;
                    rfvLegalFirstName.Enabled = true;
                }

                if (!string.IsNullOrEmpty(user.LegalMiddleName))
                {
                    lblLegalMiddleName.Text = user.LegalMiddleName;
                }
                else
                {
                    lblLegalMiddleName.Text = string.Empty;
                    if (string.IsNullOrEmpty(user.LegalFirstName) || string.IsNullOrEmpty(user.LegalLastName))
                    {
                        txtLegalMiddleName.Visible = true;
                    }
                }

                if (status == "New")
                {
                    txtPreviousLegalLastName.Text = user.PreviousLegalLastName;
                    txtPreviousLegalLastName.Visible = true;
                    //rfvPreviousLegalLastName.Enabled = true;

                    txtPreviousLegalFirstName.Text = user.PreviousLegalFirstName;
                    txtPreviousLegalFirstName.Visible = true;
                    //rfvPreviousLegalFirstName.Enabled = true;
                }
                else
                {
                    if (!string.IsNullOrEmpty(user.PreviousLegalLastName))
                    {
                        lblPreviousLegalLastName.Text = user.PreviousLegalLastName;
                    }
                    else
                    {
                        trPrevLegalLastName.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(user.PreviousLegalFirstName))
                    {
                        lblPreviousLegalFirstName.Text = user.PreviousLegalFirstName;
                    }
                    else
                    {
                        trPrevLegalFirstName.Visible = false;
                    }
                }
                
                
                txtCommonlyUsedLastName.Text = user.CommonlyUsedLastName;
                txtCommonlyUsedFirstName.Text = user.CommonlyUsedFirstName;
                //txtCommonlyUsedMiddleName.Text = user.CommonlyUsedMiddleName;

                if (user.BirthDate != DateTime.MinValue && user.BirthDate < DateTime.Today)
                {
                    txtBirthDate.Text = user.BirthDate.ToString("MM/dd/yyyy");

                    TimeSpan Span = DateTime.Today - user.BirthDate;
                    DateTime Age = DateTime.MinValue + Span;
                    int Years = Age.Year - 1;
                    if (Years < 18)
                    {
                        txtBirthDate.Enabled = true;
                        rfvBirthDate.Enabled = true;
                    }
                }
                else
                {
                    txtBirthDate.Enabled = true;
                    rfvBirthDate.Enabled = true;
                }

                if (!string.IsNullOrEmpty(user.Gender))
                {
                    ddlGender.SelectedValue = user.Gender;
                    if (user.Gender == "SelfDescribe")
                    {
                        trGenderSelfDescribe.Visible = true;
                        txtGenderSelfDescribe.Text = user.GenderSelfDescribe;
                    }
                    else
                    {
                        trGenderSelfDescribe.Visible = false;
                        txtGenderSelfDescribe.Text = string.Empty;
                    }
                }

                if (user.HomeAddress != null)
                {
                    txtHomeAddress1Text.Text = user.HomeAddress.Address1;
                    txtHomeAddress2Text.Text = user.HomeAddress.Address2;
                    txtHomeAddress3Text.Text = user.HomeAddress.Address3;

                    txtCityText.Text = user.HomeAddress.City;
                    //ddlCountry.SelectedValue = user.HomeAddress.Country;
                    var foundCoutnry = ddlCountry.Items.FindByText(user.HomeAddress.Country);
                    if (foundCoutnry != null)
                    {
                        ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(foundCoutnry);
                    }
                    
                    txtPostalCode.Text = user.HomeAddress.PostalCode;
                    //ddlProvince.SelectedValue = user.HomeAddress.Province;
                    //txtProvince.Text = user.HomeAddress.Province;
                    
                    //txtCountry.Text = user.HomeAddress.Country;
                    meePostalCode.Enabled = false;
                    revPostalCode.Enabled = false;

                    if (user.HomeAddress.Country.ToUpper().Contains("CANADA") || user.HomeAddress.Country.ToUpper().Contains("UNITED STATES"))
                    {
                        txtProvince.Visible = false;
                        ddlProvince.Visible = true;
                        rfvProvince.Enabled = true;
                        meeHomePhone.Enabled = true;
                        revHomePhone.Enabled = true;

                        //ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByText(user.HomeAddress.Country));
                        if (user.HomeAddress.Country.ToUpper().Contains("CANADA"))
                        {
                            meePostalCode.Enabled = true;
                            revPostalCode.Enabled = true;
                        }
                        if (user.HomeAddress.Country.ToUpper().Contains("UNITED STATES"))
                        {
                            var list1 = repository.GetUSStatesList();

                            list1.Insert(0, new GenClass { Code = "", Description = "" });

                            ddlProvince.DataSource = list1;
                            ddlProvince.DataValueField = "Code";
                            ddlProvince.DataTextField = "Description";
                            ddlProvince.DataBind();
                        }

                        if (ddlProvince.Items.FindByValue(user.HomeAddress.Province) != null)
                        {
                            ddlProvince.SelectedValue = user.HomeAddress.Province;
                        }
                    }
                    else
                    {
                        txtProvince.Visible = true;
                        txtProvince.Text = user.HomeAddress.Province;
                        ddlProvince.Visible = false;
                        rfvProvince.Enabled = false;
                        meeHomePhone.Enabled = false;
                        revHomePhone.Enabled = false;
                    }
                }
            }
        }

        private void securityCheck()
        {
            //string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                if (ViewState["CURRENT_PREV_USER_INFO"] != null)
                {
                    return (User)ViewState["CURRENT_PREV_USER_INFO"];
                }
                else
                    return null; ;
            }
            set
            {
                ViewState["CURRENT_PREV_USER_INFO"] = value;
            }
        }

        #endregion

        protected void ddlGender_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlGender.SelectedItem.Value  == "SelfDescribe")
            {
                trGenderSelfDescribe.Visible = true;
            }
            else
            {
                trGenderSelfDescribe.Visible = false;
                txtGenderSelfDescribe.Text = string.Empty;
            }
        }
    }
}