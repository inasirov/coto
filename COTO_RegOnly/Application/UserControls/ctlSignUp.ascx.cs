﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
//using Asi;
//using Asi.Web;
//using Asi.iBO;
//using Asi.iBO.ContactManagement;
using System.Web.Security;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlSignUp : System.Web.UI.UserControl
    {

        #region Consts
        private const string VIEW_STATE_CURRENT_IMIS_ID = "GetCurrentImisId";
        private const string Reset_Password_Url = "/IMIS15/COTO/AsiCommon/Controls/Shared/FormsAuthentication/RecoverPassword.aspx";
        private string ReturnPageUrl = WebConfigItems.Application_Step1;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //BindLists();
                //var newId = GetCurrentiMISID();
                if (!string.IsNullOrEmpty(CurrentUserId))
                {
                    //CurrentUserId = newId;
                    //BindExistingUserInfo();
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //if (ValidationRegistration())
            //{

            //    //if (string.IsNullOrEmpty(CurrentUserId) && string.IsNullOrEmpty(hfUserID.Value))
            //    //{
            //    //    SavedPassword = txtPwrd.Text;
            //    //    SavedVerifyPassword = txtVerifyPwrd.Text;
            //    //}

            //    //var repository = new Repository();

            //    string newId = string.Empty;

            //    string error = string.Empty;


            //    var user = new SavedMember();

            //    user.LoginName = txtLogin.Text;
            //    user.Password = txtPwrd.Text;

            //    user.UserID = CurrentUserId;
            //    user.Prefix = ddlPrefix.SelectedValue;
            //    user.FirstName = txtFirstName.Text;
            //    user.MiddleName = txtMiddleName.Text;
            //    user.LastName = txtLastName.Text;
            //    user.PreferredEmail = txtPreferredEmail.Text;

            //    user.HomeAddress = txtHomeAddress1.Text;
            //    user.HomeAddress1 = txtHomeAddress1.Text;
            //    user.HomeAddress += " " + txtHomeAddress2.Text;
            //    user.HomeAddress2 = txtHomeAddress2.Text;
            //    user.HomeAddress += " " + txtHomeAddress3.Text;
            //    user.HomeAddress3 = txtHomeAddress3.Text;

            //    user.HomeCity = txtHomeCity.Text;
            //    user.HomeCountry = ddlHomeCountry.SelectedValue;
            //    user.HomePostalCode = txtHomePostalCode.Text;

            //    if (trHomeProvinceDropdown.Visible)
            //    {
            //        user.HomeProvince = ddlHomeProvince.SelectedItem.Text;
            //        user.HomeProvinceValue = ddlHomeProvince.SelectedValue;
            //    }
            //    else
            //    {
            //        user.HomeProvince = txtHomeProvince.Text;
            //        user.HomeProvinceValue = txtHomeProvince.Text;
            //    }
            //    user.HomePhone = txtHomePhone.Text;
            //    user.HomeFax = txtHomeFax.Text;

            //    //DoRegistration(user);
            //    //return;

            //    Response.Redirect(ReturnPageUrl);
            //}
            //else
            //{
            //    if (!string.IsNullOrEmpty(SavedPassword)) txtPwrd.Text = SavedPassword;
            //    if (!string.IsNullOrEmpty(SavedVerifyPassword)) txtVerifyPwrd.Text = SavedVerifyPassword;
            //}
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ddlPrefix.SelectedIndex = 0;
            if (txtFirstName.Enabled)
            {
                txtFirstName.Text = string.Empty;
            }

            if (txtMiddleName.Enabled)
            {
                txtMiddleName.Text = string.Empty;
            }

            if (txtLastName.Enabled)
            {
                txtLastName.Text = string.Empty;
            }

            txtPreferredEmail.Text = string.Empty;

            txtLogin.Text = string.Empty;
            txtPwrd.Attributes["value"] = string.Empty;
            txtVerifyPwrd.Attributes["value"] = string.Empty;

            txtHomeAddress1.Text = string.Empty;
            txtHomeAddress2.Text = string.Empty;
            txtHomeAddress3.Text = string.Empty;
            txtHomeCity.Text = string.Empty;
            ddlHomeCountry.SelectedIndex = 0;
            ddlHomeProvince.SelectedIndex = 0;
            txtHomeProvince.Text = string.Empty;
            trHomeProvinceDropdown.Visible = false;
            trHomeProvinceTextBox.Visible = true;
            txtHomePostalCode.Text = string.Empty;
            txtHomePhone.Text = string.Empty;
            txtHomeFax.Text = string.Empty;
        }

        protected void ddlHomeCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            string homeCountry = ddlHomeCountry.SelectedValue.ToUpper();
            if (homeCountry == "CANADA" || homeCountry == "USA" || homeCountry == "UNITED STATES")
            {
                trHomeProvinceDropdown.Visible = true;
                trHomeProvinceTextBox.Visible = false;
            }
            else
            {
                trHomeProvinceDropdown.Visible = false;
                trHomeProvinceTextBox.Visible = true;
            }
            if (homeCountry == "CANADA" || homeCountry == "USA" || homeCountry == "UNITED STATES")
            {
                lblHomePostalCodeTitle.Text = "Postal Code/Zip: *";
            }
            else
            {
                lblHomePostalCodeTitle.Text = "Postal Code/Zip:";
            }
        }

        #endregion

        #region Methods

        //public void BindExistingUserInfo()
        //{
        //    var repository = new Repository();
        //    var contact = repository.GetContactByID(CurrentUserId);
        //    if (contact != null)
        //    {
        //        hfUserID.Value = CurrentUserId;
        //        ddlPrefix.SelectedValue = contact.Prefix;
        //        txtFirstName.Text = contact.FirstName;
        //        txtFirstName.Enabled = false;
        //        txtMiddleName.Text = contact.MiddleName;
        //        txtMiddleName.Enabled = false;
        //        txtLastName.Text = contact.LastName;
        //        txtLastName.Enabled = false;
        //        txtPreferredEmail.Text = contact.EmailAddress;

        //        trLoginDetails1.Visible = false;
        //        trLoginDetails2.Visible = false;
        //        trLoginDetails3.Visible = false;
        //        trLoginDetails4.Visible = false;
        //        trLoginDetails5.Visible = false;
        //        trLoginDetails6.Visible = false;
        //        trLoginDetails7.Visible = false;

        //        btnReset.Visible = false;
        //        var homeAddress = contact.GetAddressByPurpose("Home");
        //        if (homeAddress != null)
        //        {
        //            txtHomeAddress1.Text = homeAddress.Address1;
        //            txtHomeAddress2.Text = homeAddress.Address2;
        //            txtHomeAddress3.Text = homeAddress.Address3;
        //            txtHomeCity.Text = homeAddress.City;
        //            txtHomePostalCode.Text = homeAddress.PostalCode;

        //            ddlHomeCountry.SelectedValue = homeAddress.Country;

        //            string homeCountry = homeAddress.Country.ToUpper();
        //            if (homeCountry == "CANADA" || homeCountry == "USA" || homeCountry == "UNITED STATES")
        //            {
        //                trHomeProvinceDropdown.Visible = true;
        //                trHomeProvinceTextBox.Visible = false;
        //                ddlHomeProvince.SelectedValue = homeAddress.StateProvince;
        //            }
        //            else
        //            {
        //                trHomeProvinceDropdown.Visible = false;
        //                trHomeProvinceTextBox.Visible = true;
        //                txtHomeProvince.Text = homeAddress.StateProvince;
        //            }
        //            txtHomePhone.Text = homeAddress.Phone;
        //            txtHomeFax.Text = homeAddress.Fax;

        //        }
        //    }
        //}

        //public void BindLists()
        //{
        //    var repository = new Repository();

        //    var list1 = repository.GetPrefixes(); //.GetGeneralList("PREFIX"); // get data for prefix field
        //    ddlPrefix.DataSource = list1;
        //    ddlPrefix.DataValueField = "CODE";
        //    ddlPrefix.DataTextField = "DESCRIPTION";
        //    ddlPrefix.DataBind();
        //    ddlPrefix.Items.Insert(0, string.Empty);

        //    var list4 = repository.GetCountryNames();
        //    var result = list4.Where(L => L.Code.ToUpper() != "CANADA" && L.Code.ToUpper() != "USA").ToList();

        //    ddlHomeCountry.DataSource = result;
        //    ddlHomeCountry.DataValueField = "CODE";
        //    ddlHomeCountry.DataTextField = "DESCRIPTION";
        //    ddlHomeCountry.DataBind();
        //    ddlHomeCountry.Items.Insert(0, string.Empty);
        //    ddlHomeCountry.Items.Insert(1, "Canada");
        //    ddlHomeCountry.Items.Insert(2, "United States");

        //    var list5 = repository.GetStatesProvinces();

        //    ddlHomeProvince.DataSource = list5;
        //    ddlHomeProvince.DataValueField = "CODE";
        //    ddlHomeProvince.DataTextField = "DESCRIPTION";
        //    ddlHomeProvince.DataBind();
        //    ddlHomeProvince.Items.Insert(0, string.Empty);
        //}

        //public bool ValidationRegistration()
        //{
        //    bool retValue = true;

        //    ResetErrorMessages();

        //    Page.Validate("GeneralValidation");


        //    //if (cbHomeBilling.Checked || cbHomeMailing.Checked)
        //    //{
        //    if (ddlHomeCountry.SelectedValue.ToUpper() != "CANADA")
        //    {
        //        revHomePostalCode.Enabled = false;
        //        if (ddlHomeCountry.SelectedItem.Text.ToUpper() != "UNITED STATES")
        //        {
        //            revHomePostalCodeUS.Enabled = false;
        //            rfvHomePostalCode.Enabled = false;
        //            rfvHomeProvince2.Enabled = false;
        //        }
        //        else
        //        {
        //            revHomePostalCodeUS.Enabled = true;
        //            rfvHomePostalCode.Enabled = true;
        //        }
        //        if (!string.IsNullOrEmpty(txtHomePostalCode.Text) && (txtHomePostalCode.Text.Length < 3 || txtHomePostalCode.Text.Length > 10))
        //        {
        //            lblHomePostalCodeErrorMessage.Text = "*";
        //            lblPageErrorMessage.Text += "<li>Home Postal Code must be between 3 and 10 characters in length.</li>";
        //            retValue = false;
        //        }
        //    }
        //    else
        //    {
        //        if ((txtHomePostalCode.Text.Length == 6) && (txtHomePostalCode.Text.IndexOf(" ") < 0))
        //        {
        //            txtHomePostalCode.Text = txtHomePostalCode.Text.Substring(0, 3) + " " + txtHomePostalCode.Text.Substring(3, txtHomePostalCode.Text.Length - 3);
        //        }
        //        revHomePostalCodeUS.Enabled = false;
        //        revHomePostalCode.Enabled = true;
        //        rfvHomePostalCode.Enabled = true;
        //    }
        //    Page.Validate("HomeAddressValidation");
        //    //}

        //    if (string.IsNullOrEmpty(CurrentUserId))
        //    {
        //        if (txtPwrd.Text != txtVerifyPwrd.Text)
        //        {
        //            lblVerifyPasswordErrorMessage.Text = "*";
        //            lblPageErrorMessage.Text = "<li>The passwords entered do not match. Please retype your password </li>";
        //            retValue = false;
        //        }
        //        if (txtPwrd.Text.Length < 6 || txtPwrd.Text.Length > 15)
        //        {
        //            lblPasswordErrorMessage.Text = "*";
        //            lblPageErrorMessage.Text += "<li>Password must consists of at least 6 characters and not more than 15 characters </li>";
        //            retValue = false;
        //        }
        //    }

        //    var repository = new Repository();
        //    if (string.IsNullOrEmpty(CurrentUserId) && !string.IsNullOrEmpty(txtLogin.Text) && repository.CheckUserNameIfExists(txtLogin.Text))
        //    {
        //        lblLoginErrorMessage.Text = "*";
        //        lblPageErrorMessage.Text += string.Format("<li>The login '{0}' is already in use. Please choose a different login </li>", txtLogin.Text);
        //        retValue = false;
        //    }
        //    if (string.IsNullOrEmpty(CurrentUserId) && (!string.IsNullOrEmpty(txtPreferredEmail.Text)) && repository.CheckUserEmailExists(txtPreferredEmail.Text))
        //    {
        //        lblPreferredEmailErrorMessage.Text = "*";
        //        lblPageErrorMessage.Text += string.Format("<li>Sorry, the email address you have entered is already on record indicating you have previously registered with the COTO." +
        //        "If you have forgotten your login or password <a href='{0}'>click here to reset your password and obtain your login</a>.</li>", Reset_Password_Url);
        //        retValue = false;
        //    }

        //    if (!retValue || !Page.IsValid)
        //    {
        //        if (!string.IsNullOrEmpty(lblPageErrorMessage.Text)) lblPageErrorMessage.Text = "<ul>" + lblPageErrorMessage.Text + "</ul>";
        //        lblPageErrorMessage.Text = "<b>Please complete all required fields!</b> <br />" + lblPageErrorMessage.Text;
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "SetFocus", "<script>document.getElementById('" + lblPageErrorMessage.ClientID + "').focus();</script>");
        //        //upMainForm.Update();
        //        return false;
        //    }

        //    return retValue;
        //}

        //public string GetCurrentiMISID()
        //{
        //    string _id = "";
        //    try
        //    {
        //        if (Asi.Security.AppPrincipal.CurrentIdentity.IsAuthenticated && Asi.Security.Utility.SecurityHelper.GetSelectedImisId() != "")
        //        {
        //            _id = Asi.Security.Utility.SecurityHelper.GetSelectedImisId();
        //        }
        //    }
        //    catch { }
        //    return _id;
        //}


        private static object GetSession(string SessionName)
        {
            if (HttpContext.Current != null &&
                HttpContext.Current.Session != null &&
                HttpContext.Current.Session[SessionName] != null)
                return HttpContext.Current.Session[SessionName];
            return null;
        }

        private static void SetSession(string SessionName, Object Value)
        {
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Session != null)
                {
                    HttpContext.Current.Session[SessionName] = Value;
                }
            }
        }

        public void ResetErrorMessages()
        {
            lblPageErrorMessage.Text = string.Empty;
            lblLoginErrorMessage.Text = string.Empty;
            lblPasswordErrorMessage.Text = string.Empty;
            lblVerifyPasswordErrorMessage.Text = string.Empty;
            lblHomeCountryErrorMessage.Text = string.Empty;
            lblPreferredEmailErrorMessage.Text = string.Empty;
            lblHomePostalCodeErrorMessage.Text = string.Empty;
        }

        public void DoRegistration()
        {

        }

        //public void DoRegistration(SavedMember user)
        //{
        //    var repository = new Repository();
        //    string newId = string.Empty;

        //    string error = string.Empty;

        //    if (!string.IsNullOrEmpty(CurrentUserId))
        //    {
        //        newId = repository.UpdateContact(CurrentUserId, user.Prefix, user.FirstName, user.MiddleName, user.LastName, user.PreferredEmail);
        //        newId = CurrentUserId;
        //    }
        //    else
        //    {
        //        newId = repository.CreateNewContact(user.Prefix, user.FirstName, user.MiddleName, user.LastName, user.PreferredEmail, user.LoginName, user.Password, "AP");
        //        //CurrentIMISID = newId;

        //        try
        //        {
        //            //if (CurrentRegistrantType == RegistrantType.IIBA)
        //            //{
        //            string contactLogin = user.LoginName;
        //            using (Asi.Security.SecurityContext.Impersonate(contactLogin))
        //            {
        //                FormsAuthentication.SignOut();
        //                Asi.Security.SecurityContext.LogonByUserId(contactLogin);
        //                Session["LoginUser"] = CContactUser.LoginByWebLogin(contactLogin);
        //                FormsAuthentication.SetAuthCookie(Asi.Security.AppPrincipal.CurrentIdentity.Name, false);
        //                Asi.Web.Security.EstablishAppContext(Context);
        //            }
        //            //}
        //        }
        //        catch (Exception ex)
        //        {
        //            error = ex.Message;
        //        }
        //    }

        //    if (!string.IsNullOrEmpty(newId))
        //    {
        //        if (!string.IsNullOrEmpty(user.HomeAddress) || !string.IsNullOrEmpty(user.HomeCity) || !string.IsNullOrEmpty(user.HomeCountry)
        //            || !string.IsNullOrEmpty(user.HomeProvince) || !string.IsNullOrEmpty(user.HomePostalCode) || !string.IsNullOrEmpty(user.HomePhone)
        //            || !string.IsNullOrEmpty(user.HomeFax))
        //        {
        //            repository.UpdateAddress(newId, "Home", user.HomeAddress1, user.HomeAddress2, user.HomeAddress3, user.HomeCity,
        //            user.HomeCountry, user.HomeProvinceValue, user.HomePostalCode, user.HomePhone, user.HomeFax,
        //            true, true, true);
        //        }
        //    }

        //    Session["ID"] = newId;

        //}
        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public static string SavedVerifyPassword
        {
            get
            {
                if (GetSession("SavedVerifyPassword") == null)
                {
                    SetSession("SavedVerifyPassword", string.Empty);
                }
                return (string)GetSession("SavedVerifyPassword");
            }
            set
            {
                SetSession("SavedVerifyPassword", value);
            }
        }

        public static string SavedPassword
        {
            get
            {
                if (GetSession("SavedPassword") == null)
                {
                    SetSession("SavedPassword", string.Empty);
                }
                return (string)GetSession("SavedPassword");
            }
            set
            {
                SetSession("SavedPassword", value);
            }
        }
        #endregion

    }
}