﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlRegistrationApplication : System.Web.UI.UserControl
    {

        #region Consts
        private string _Key = "Yz7!~3";
        private string PrevStep = WebConfigItems.Application_Step1;
        private string NextStep = WebConfigItems.Application_Step3;
        private const int CurrentStep = 2;
        #endregion
        
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            lblExpectedStartDateErrorMessage.Text = string.Empty;
            if (Request.QueryString.Count > 1)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.Application_Step0);
                return;
            }
            
            if (!IsPostBack) // first time loading 
            {
                SessionParameters.ApplicationStep = CurrentStep;
                BindLists();
                BindData();
            }
        }

        protected void ddlRegistrationCategorySelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshCertificateDescription();
        }

        protected void ddlCitizenshipCategorySelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshCitizenshipInfo();
        }

        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            DateTime expectedDate = DateTime.MinValue;
            CultureInfo provider = CultureInfo.InvariantCulture;
            string format = "MM/dd/yyyy";
            if (!string.IsNullOrEmpty(txtExpectedStartDate.Text) && !DateTime.TryParseExact(txtExpectedStartDate.Text, format, provider, DateTimeStyles.None, out expectedDate))
            {
                lblExpectedStartDateErrorMessage.Text = "<br />Expected Start Date has invalid format";
                return;
            }

            if (expectedDate < DateTime.Today.AddDays(7))
            {
                lblExpectedStartDateErrorMessage.Text = "<br />Expected Start Date must be at least 7 days from date application is being filled out";
                return;
            }

            if (UpdateUserInfo(expectedDate))
            {
                //Response.Redirect("MemberDisplay.aspx");
                UpdateSteps(1);
                ApplicationRegistrationCategory = ddlRegistrationCategory.SelectedValue;
                ApplicationRegistrationCitizenship = ddlCitizenshipCategory.SelectedValue;
                Response.Redirect(NextStep);
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(PrevStep);
            UpdateSteps(-1);
        }
        #endregion

        #region Methods

        protected void RefreshCertificateDescription()
        {
            lblRegistrationCategoryDescription.Text = string.Empty;
            if (ddlRegistrationCategory.SelectedValue == "GC")
            {
                lblRegistrationCategoryDescription.Text = "General Practising Certificates of Registration are issued to individuals who meet all of the registration requirements including passing the CAOT exam. " +
                    "General Practising Registrants are able to practice without restriction.";
            }
            if (ddlRegistrationCategory.SelectedValue == "P")
            {
                lblRegistrationCategoryDescription.Text = "Provisional Practising Certificates of Registration are issued to individuals who have not yet met the examination requirement. " +
                    "To register in this category, applicants must meet all of the registration requirements, with the exception of the examination. " +
                    "In addition, Provisional Practising Registrants must be registered to write the first available sitting of the exam, " +
                    "and have an offer of employment where they will be supervised by a Registrant who has held a General Practising Certificate of Registration " +
                    string.Format("for at least one year. Applicants in this category must submit a signed <a href='{0}' target='_blank'>Provisional Registration Supervision Agreement</a>.", WebConfigItems.Application_Step2_EmployerForm);
            }
        }

        protected void RefreshCitizenshipInfo()
        {
            lblCitizenshipCategoryDescription.Text = string.Empty;
            if (ddlCitizenshipCategory.SelectedValue.Contains("requirement"))  // "I do not yet meet this requirement"
            {
                lblCitizenshipCategoryDescription.Text = "Forward a copy of your documentation of eligibility to work in Canada once available.";
            }
            if (ddlCitizenshipCategory.SelectedValue.Contains("Citizenship"))  // "Canadian Citizenship"
            {
                lblCitizenshipCategoryDescription.Text = "Forward a copy of your Canadian birth certificate, valid Canadian passport or Canadian citizenship card.";
            }
            if (ddlCitizenshipCategory.SelectedValue.Contains("Permanent"))  // "Permanent Resident"
            {
                lblCitizenshipCategoryDescription.Text = "Forward a copy of your Permanent Resident Card (front and back).";
            }
            if (ddlCitizenshipCategory.SelectedValue.Contains("Landed"))  // "Landed Immigrant"
            {
                lblCitizenshipCategoryDescription.Text = "Forward a copy of your Record of Landing.";
            }
            if (ddlCitizenshipCategory.SelectedValue.Contains("Employment"))   // "Work Permit"; new type is "Employment Authorization"
            {
                lblCitizenshipCategoryDescription.Text = "Forward a copy of your Employment Authorization under the Immigration and Refugee Protection Act. This includes work permits. The Authorization must allow you to work as an occupational therapist, work within healthcare and/or with children.";
            }
        }

        protected void BindData()
        {
            ceExpectedStartDate.StartDate = DateTime.Now.AddDays(7);
            var repository = new Repository();
            var contact = repository.GetUserRegistrationInfo(CurrentUserId);
            if (contact != null)
            {
                if (contact.CurrentStatus.ToUpper().Contains("FORMER"))
                {
                    lblCitizenshipRequirementsMessage.Text = "If you have previously submitted proof of citizenship to the College, you are not required to forward another copy to the College.";
                }

                //lblRegistrationApplicationSectionTitle.Text = string.Format("Registration Application for {0}", contact.FullName);
                if (contact.Category != null && !string.IsNullOrEmpty(contact.Category))
                {
                    ddlRegistrationCategory.SelectedValue = contact.Category;
                }

                if (contact.RegisterBy != DateTime.MinValue)
                {
                    txtExpectedStartDate.Text = contact.RegisterBy.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                }

                if (!string.IsNullOrEmpty(contact.Citizenship))
                {
                    if (ddlCitizenshipCategory.Items.FindByValue(contact.Citizenship) != null)
                    {
                        ddlCitizenshipCategory.SelectedValue = contact.Citizenship;
                    }
                }
                RefreshCertificateDescription();
                RefreshCitizenshipInfo();
            }
            var user2 = repository.GetApplicationUserInfo(CurrentUserId);
            if (user2 != null)
            {
                //CurrentUser = user2;
                lblSectionTitle.Text = string.Format("Registration Category, Expected Start Date and Work Eligibility for {0} #{1}", user2.FullName, user2.Id);
            }
        }

        protected void BindLists()
        {
            var repository = new Repository();
            
            var list1 = new List<GenClass>();
            list1.Insert(0, new GenClass { Code = "", Description = "" });
            list1.Add(new GenClass { Code = "GC", Description = "General Practising Certificate" });
            list1.Add(new GenClass { Code = "P", Description = "Provisional Practising Certificate" });
            
            ddlRegistrationCategory.DataSource = list1;
            ddlRegistrationCategory.DataValueField = "Code";
            ddlRegistrationCategory.DataTextField = "Description";
            ddlRegistrationCategory.DataBind();

            var list2 = new List<GenClass>();
            list2 = repository.GetGeneralList("Citizenship");
            list2.Insert(0, new GenClass { Substitute = string.Empty, Description = string.Empty });
            //list2.Add(new GenClass { Code = "Canadian Citizenship", Description = "Canadian Citizen" });
            //list2.Add(new GenClass { Code = "Permanent Resident", Description = "Permanent Resident / Landed Immigrant" });
            //list2.Add(new GenClass { Code = "Work Permit", Description = "Work Permit" });
            //list2.Add(new GenClass { Code = "I do not yet meet this requirement", Description = "I do not yet meet this requirement" });

            ddlCitizenshipCategory.DataSource = list2;
            ddlCitizenshipCategory.DataValueField = "Substitute";
            ddlCitizenshipCategory.DataTextField = "Description";
            ddlCitizenshipCategory.DataBind();
        }

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.ApplicationStep != 0)
            {
                SessionParameters.ApplicationStep += diff;
            }
        }

        protected bool UpdateUserInfo( DateTime expectedStartDate)
        {
            User user = new User();
            user.Id = CurrentUserId;
            user.Category = ddlRegistrationCategory.SelectedValue;
            user.Citizenship = ddlCitizenshipCategory.SelectedValue;
            user.RegisterBy = expectedStartDate;
            var repository = new Repository();
            repository.UpdateApplicationUserRegistrationApplicationLogged(CurrentUserId, user);

            return true;
        }

        private void securityCheck()
        {
            //string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public string ApplicationRegistrationCategory
        {
            get
            {
                if (SessionParameters.ApplicationRegistrationCategory != null)
                {
                    return SessionParameters.ApplicationRegistrationCategory;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.ApplicationRegistrationCategory = value;
            }
        }

        public string ApplicationRegistrationCitizenship
        {
            get
            {
                if (SessionParameters.ApplicationRegistrationCitizenship != null)
                {
                    return SessionParameters.ApplicationRegistrationCitizenship;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.ApplicationRegistrationCitizenship = value;
            }
        }
        #endregion
    }
}