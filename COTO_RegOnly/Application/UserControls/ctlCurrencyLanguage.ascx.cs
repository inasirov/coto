﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using System.Security.Cryptography;
using System.Text;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlCurrencyLanguage : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;

        private string PrevStep = WebConfigItems.Application_Step2;
        private string NextStep = WebConfigItems.Application_Step4;

        private const int CurrentStep = 3;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //lblMessage.Text = string.Empty;
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.Application_Step0);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.ApplicationStep = CurrentStep;

                BindLists();
                BindData();

                if (!string.IsNullOrEmpty(WebConfigItems.Application_Step3_LanguageRequirementLink))
                {
                    var link = WebConfigItems.Application_Step3_LanguageRequirementLink;
                    lbtnLanguageRequirements.Attributes.Add("onclick", string.Format("javascript: window.open('{0}', '_blank')", link));
                }
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            UpdateUserInfo();
            UpdateSteps(1);
            Response.Redirect(NextStep); // + "&ID=" + CurrentUserId + "&FULL_NAME=" + HttpUtility.UrlEncode(fullName) + "&CustomAction=renewal");
        }

        protected void ddlCurrencyHoursSelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshCurrencyDescription();
        }

        protected void ddlFirstLanguageSelectedIndexChanged(object sender, EventArgs e)
        {
            if (CheckLanguageRequirement())
            {
                trMessage.Visible = true;
            }
            else
            {
                trMessage.Visible = false;
            }
        }

        protected void ddlEducationLanguageSelectedIndexChanged(object sender, EventArgs e)
        {
            if (CheckLanguageRequirement())
            {
                trMessage.Visible = true;
            }
            else
            {
                trMessage.Visible = false;
            }
        }

        //protected void lbtnLanguageRequirementsClick(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(WebConfigItems.Application_Step3_LanguageRequirementLink))
        //    {
        //        Response.Redirect(WebConfigItems.Application_Step3_LanguageRequirementLink);
        //    }
        //}
        #endregion

        #region Methods

        protected bool CheckLanguageRequirement()
        {
            bool retValue = true;
            if (ddlFirstLanguage.SelectedItem.Text.Contains("English") || ddlFirstLanguage.SelectedItem.Text.Contains("French") || ddlEducationLanguage.SelectedItem.Text.Contains("English") || ddlEducationLanguage.SelectedItem.Text.Contains("French"))
            {
                retValue = false;
            }

            if (string.IsNullOrEmpty(ddlFirstLanguage.SelectedValue) && string.IsNullOrEmpty(ddlEducationLanguage.SelectedValue))
            {
                retValue = false;
            }
            return retValue;
        }

        protected void RefreshCurrencyDescription()
        {
            lblCurrencyHoursDescription.Text = string.Empty;
            if (ddlCurrencyHours.SelectedValue == "A_600_O")
            {
                lblCurrencyHoursDescription.Text = "You are required to forward documentation to the College to demonstrate that you meet the currency requirement. " +
                    "Please forward a letter from your most recent employer confirming the employment period and hours worked. The letter must be on official letterhead and include the employer name, address and phone number. ";
            }
            if (ddlCurrencyHours.SelectedValue == "A_LMSA")
            {
                lblCurrencyHoursDescription.Text = "<p>If you wish to apply under the Labour Mobility Support Agreement, you must arrange for completion of " +
                    string.Format("<a href='{0}' target='_blank'>the Labour Mobility Support Agreement Confirmation Form</a> by the regulatory organization with which you are currently registered. ", WebConfigItems.Application_Step3_LMSA_Link) +
                   string.Format("A <a href='{0}' target='_blank'>Regulatory History Form</a> confirming your current regulatory history is also required.<p>", WebConfigItems.Application_Step6_RegulatoryForm);
                lblCurrencyHoursDescription.Text += "<p>Complete the Authorization for Release of Information portion of the forms, send them to your current regulatory organization(s) and " +
                    "instruct them to return the completed forms directly to the College of Occupational Therapists of Ontario.</p>";
            }
            if (ddlCurrencyHours.SelectedValue.ToUpper() == "A_REVIEW")
            {
                lblCurrencyHoursDescription.Text = "<p>If you have not worked 600 hours in the past 3 years and do not meet the currency requirement, " +
                    "you will be required to complete a refresher program.</p>";
                lblCurrencyHoursDescription.Text += "You must provide a detailed account of your occupational therapy experience for the past 10 years, " +
                    "including dates of employment, hours worked per week, volunteer work and/or continuing education courses completed within the scope of occupational therapy. " +
                    string.Format("To initiate this process, please fill out the <a href='{0}' target='_blank'>Additional Currency Data Sheet</a> and forward it to the College.", WebConfigItems.Application_Step3_NotMeetCurrencyRequirementLink);
            }
        }

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.ApplicationStep != 0)
            {
                SessionParameters.ApplicationStep += diff;
            }
        }

        protected void UpdateUserInfo()
        {
            User user = new User();
            user.Id = CurrentUserId;
            user.CurrencyLanguageServices = new Classes.CurrencyLanguage();
            user.CurrencyLanguageServices.Currency = ddlCurrencyHours.SelectedValue;
            user.CurrencyLanguageServices.Lang_Service1 = ddlService1.SelectedValue;
            user.CurrencyLanguageServices.Lang_Service2 = ddlService2.SelectedValue;
            user.CurrencyLanguageServices.Lang_Service3 = ddlService3.SelectedValue;
            user.CurrencyLanguageServices.Lang_Service4 = ddlService4.SelectedValue;
            user.CurrencyLanguageServices.Lang_Service5 = ddlService5.SelectedValue;
            user.CurrencyLanguageServices.First_Language = ddlFirstLanguage.SelectedValue;
            user.CurrencyLanguageServices.Lang_OT_Instr = ddlEducationLanguage.SelectedValue;
            user.CurrencyLanguageServices.Lang_Preferred_Doc = ddlPreferredDocumentationLanguage.SelectedValue;


            var repository = new Repository();
            repository.UpdateApplicationUserCurrencyLanguageLogged(CurrentUserId, user);
            int currentYear = DateTime.Now.Year;
            if (DateTime.Now.Month <= 5)
            {
                currentYear = currentYear - 1;
            }
            repository.UpdateApplicationUserCurrencyHoursInfoLogged(CurrentUserId, user, currentYear);

            //if (ddlCurrencyHours.SelectedValue.ToUpper() == "REVIEW")
            //{
            //    string message = string.Format("Member ID: {0}; ", CurrentUserId); ;
            //    message += string.Format("Date: {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

            //    string emailSubject = "Registrant Requires Currency Review";
            //    string emailTo = WebConfigItems.RegistrationManagerContactEmail;
            //    var tool = new Tools();
            //    tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);
            //}
        }

        protected void BindLists()
        {
            var repository = new Repository();

            var list1 = repository.GetGeneralList("CURRENCY_REQ"); // get data for status field
            list1 = list1.Where(I => I.Code.StartsWith("A_")).ToList();
            //foreach (var item in list1)
            //{
            //    item.Code = item.Code.Replace("A_", "");
            //}
            ddlCurrencyHours.DataSource = list1;
            ddlCurrencyHours.DataValueField = "CODE";
            ddlCurrencyHours.DataTextField = "DESCRIPTION";
            ddlCurrencyHours.DataBind();
            ddlCurrencyHours.Items.Insert(0, string.Empty);

            var list2 = repository.GetGeneralList("LANGUAGE"); // get data for nature of practice field
            ddlService1.DataSource = list2;
            ddlService1.DataValueField = "CODE";
            ddlService1.DataTextField = "DESCRIPTION";
            ddlService1.DataBind();
            ddlService1.Items.Insert(0, string.Empty);


            var list3 = repository.GetGeneralList("LANGUAGE_INST_AH");
            var list4 = repository.GetGeneralList("LANGUAGE1");

            list3 = list3.OrderBy(I => I.Description).ToList();
            
            ddlFirstLanguage.DataSource = list3;
            ddlFirstLanguage.DataValueField = "CODE";
            ddlFirstLanguage.DataTextField = "DESCRIPTION";
            ddlFirstLanguage.DataBind();
            ddlFirstLanguage.Items.Insert(0, string.Empty);

            ddlEducationLanguage.DataSource = list4;
            ddlEducationLanguage.DataValueField = "CODE";
            ddlEducationLanguage.DataTextField = "DESCRIPTION";
            ddlEducationLanguage.DataBind();
            ddlEducationLanguage.Items.Insert(0, string.Empty);

            ddlPreferredDocumentationLanguage.DataSource = list2;
            ddlPreferredDocumentationLanguage.DataValueField = "CODE";
            ddlPreferredDocumentationLanguage.DataTextField = "DESCRIPTION";
            ddlPreferredDocumentationLanguage.DataBind();
            ddlPreferredDocumentationLanguage.Items.Insert(0, string.Empty);

            ddlService2.DataSource = list3;
            ddlService2.DataValueField = "CODE";
            ddlService2.DataTextField = "DESCRIPTION";
            ddlService2.DataBind();
            ddlService2.Items.Insert(0, string.Empty);

            ddlService3.DataSource = list3;
            ddlService3.DataValueField = "CODE";
            ddlService3.DataTextField = "DESCRIPTION";
            ddlService3.DataBind();
            ddlService3.Items.Insert(0, string.Empty);

            ddlService4.DataSource = list3;
            ddlService4.DataValueField = "CODE";
            ddlService4.DataTextField = "DESCRIPTION";
            ddlService4.DataBind();
            ddlService4.Items.Insert(0, string.Empty);

            ddlService5.DataSource = list3;
            ddlService5.DataValueField = "CODE";
            ddlService5.DataTextField = "DESCRIPTION";
            ddlService5.DataBind();
            ddlService5.Items.Insert(0, string.Empty);

        }

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetApplicationUserCurrencyLanguage(CurrentUserId);
            if (user != null)
            {
                if (user.CurrencyLanguageServices != null)
                {
                    //ddlCurrencyHours.SelectedValue = user.CurrencyLanguageServices.Currency;
                    ddlService1.SelectedValue = user.CurrencyLanguageServices.Lang_Service1;
                    ddlService2.SelectedValue = user.CurrencyLanguageServices.Lang_Service2;
                    ddlService3.SelectedValue = user.CurrencyLanguageServices.Lang_Service3;
                    ddlService4.SelectedValue = user.CurrencyLanguageServices.Lang_Service4;
                    ddlService5.SelectedValue = user.CurrencyLanguageServices.Lang_Service5;
                    ddlFirstLanguage.SelectedValue = user.CurrencyLanguageServices.First_Language;
                    ddlEducationLanguage.SelectedValue = user.CurrencyLanguageServices.Lang_OT_Instr;
                    ddlPreferredDocumentationLanguage.SelectedValue = user.CurrencyLanguageServices.Lang_Preferred_Doc;

                    if (CheckLanguageRequirement())
                    {
                        trMessage.Visible = true;
                    }
                    else
                    {
                        trMessage.Visible = false;
                    }
                }
            }
            int currentYear = DateTime.Now.Year;
            if (DateTime.Now.Month <= 5)
            {
                currentYear = currentYear - 1;
            }
            var user0 = repository.GetApplicationUserCurrencyHoursInfo(CurrentUserId, currentYear);
            if (user0 != null)
            {
                if (user0.CurrencyLanguageServices != null)
                {
                    ddlCurrencyHours.SelectedValue = user0.CurrencyLanguageServices.Currency;
                    RefreshCurrencyDescription();
                }
            }

            var user2 = repository.GetApplicationUserInfo(CurrentUserId);
            if (user2 != null)
            {
                //CurrentUser = user2;
                lblCurrencyLanguageTitle.Text = "Currency and Language for " + user2.FullName + " #" + user2.Id ;
            }
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                if (SessionParameters.CurrentUser != null)
                {
                    return SessionParameters.CurrentUser;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUser = value;
            }
        }

        #endregion

        

    }
}