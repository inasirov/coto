﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using COTO_RegOnly.Classes;
using System.Security.Cryptography;
using System.Text;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlPracticeHistory : System.Web.UI.UserControl
    {

        #region Consts

        private string _Key = "Yz7!~3";
        private string PrevStep = WebConfigItems.Application_Step6;
        private string NextStep = WebConfigItems.Application_Step8;
        private const int CurrentStep = 7;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            lblFirstYearCanadianPracticeErrorMessage.Text = string.Empty;
            lblLastYearOutsideOntarioErrorMessage.Text = string.Empty;

            if (Request.QueryString.Count > 1 && Request.QueryString["timestamp"]!=null)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.Application_Step0);
                return;
            }
            if (Request.QueryString["common"] != null && Request.QueryString["common"] == "updated")
            {
                trEditConfirmation.Visible = true;
            }
            else
            {
                trEditConfirmation.Visible = false;
            }
            if (!IsPostBack) // first time loading 
            {
                SessionParameters.ApplicationStep = CurrentStep;
                BindLists();
                BindData();
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            //if ((ddlOutsideOntarioPractice.SelectedValue == "Yes" && ValidationCheck()) || ddlOutsideOntarioPractice.SelectedValue == "No")
            if (ValidationCheck())
            {
                UpdateUserPracticeHistory();
                UpdateSteps(1);
                Response.Redirect(NextStep);
            }
            else
            {
                if (Messages != null && Messages.Count > 0)
                {
                    string message = string.Empty;
                    foreach (var item in Messages)
                    {
                        message += ((PageMessage)item).Message + "<br />";
                    }
                    message = string.Format("<font color='Red'>{0}</font>", message);

                    Messages.Clear();
                    ShowMessage(message);
                }
            }
        }

        //protected void ddlOutsideOntarioPracticeSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlOutsideOntarioPractice.SelectedValue == "Yes")
        //    {
        //        trMemberInfo.Visible = true;   
        //    }
        //    else
        //    {
        //        trMemberInfo.Visible = false;
        //    }
        //}

        protected void lbtnCorrectDetailsClick(object sender, EventArgs e)
        {
            Response.Redirect(WebConfigItems.Application_Step7_UpdatePracticeHistory);
        }

        protected void ddlFirstYearOTPracticeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFirstYearOTPractice.SelectedItem.Text == "Not Applicable")
            {
                DisableAllLists();
            }
            else
            {
                if (PreviousInitialFirstYear == "Not Applicable")
                {
                    ddlFirstCountryOTPractice.Enabled = true;
                    ddlFirstStateOTPractice.Enabled = true;

                    ddlFirstYearCanadianOTPractice.Enabled = true;
                    ddlFirstProvinceOTPractice.Enabled = true;

                    ddlLastYearOutsideOntario.Enabled = true;
                    ddlLastCountryOTPractice.Enabled = true;
                    ddlLastStateOrCanadianProvince.Enabled = true;
                }
            }
            PreviousInitialFirstYear = ddlFirstYearOTPractice.SelectedItem.Text;
        }

        protected void ddlFirstCountryOTPracticeSelectedIndexChanged(object sender, EventArgs e)
        {
            ddlFirstStateOTPractice.SelectedIndex = 0;
            if (ddlFirstCountryOTPractice.SelectedValue == "CAN")
            {
                ddlFirstProvinceOTPractice.SelectedIndex = 0;
                ddlFirstYearCanadianOTPractice.SelectedValue = ddlFirstYearOTPractice.SelectedValue;
                ddlFirstYearCanadianOTPractice.Enabled = false;
                //ddlFirstProvinceOTPractice.Enabled = false;

                var repository = new Repository();
                var list3 = repository.GetGeneralList("FROM_PROVINCE");

                ddlFirstStateOTPractice.DataSource = list3;
                ddlFirstStateOTPractice.DataValueField = "Code";
                ddlFirstStateOTPractice.DataTextField = "Description";
                ddlFirstStateOTPractice.DataBind();
                ddlFirstStateOTPractice.Items.Insert(0, string.Empty);
                ddlFirstStateOTPractice.SelectedIndex = 0;
            }
            else
            {
                ddlFirstYearCanadianOTPractice.Enabled = true;
                ddlFirstProvinceOTPractice.Enabled = true;
                //ddlFirstProvinceOTPractice.Enabled = true;
                var repository = new Repository();
                var list2 = repository.GetGeneralList("PROVINCE_STATE");

                ddlFirstStateOTPractice.DataSource = list2;
                ddlFirstStateOTPractice.DataValueField = "Code";
                ddlFirstStateOTPractice.DataTextField = "Description";
                ddlFirstStateOTPractice.DataBind();
                ddlFirstStateOTPractice.Items.Insert(0, string.Empty);
                ddlFirstStateOTPractice.SelectedIndex = 0;
            }
        }

        protected void ddlFirstStateOTPracticeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFirstCountryOTPractice.SelectedValue == "CAN")
            {
                if (!string.IsNullOrEmpty(ddlFirstStateOTPractice.SelectedItem.Value))
                {
                    ddlFirstProvinceOTPractice.SelectedValue = ddlFirstStateOTPractice.SelectedItem.Value;
                    ddlFirstProvinceOTPractice.Enabled = false;
                }
                else
                {
                    ddlFirstProvinceOTPractice.SelectedIndex = 0;
                    ddlFirstProvinceOTPractice.Enabled = true;
                }
            }
            //else
            //{
            //    ddlFirstProvinceOTPractice.Enabled = true;
            //}
        }

        protected void ddlFirstYearCanadianOTPracticeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFirstYearCanadianOTPractice.SelectedItem.Text == "Not Applicable")
            {
                var item = ddlFirstProvinceOTPractice.Items.FindByText("Not Applicable");
                if (item != null)
                {
                    ddlFirstProvinceOTPractice.SelectedValue = item.Value;
                }
                ddlFirstProvinceOTPractice.Enabled = false;
            }
            else
            {
                ddlFirstProvinceOTPractice.Enabled = true;
            }
        }

        protected void ddlLastYearOutsideOntarioSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLastYearOutsideOntario.SelectedItem.Text == "Not Applicable")
            {
                var item = ddlLastCountryOTPractice.Items.FindByText("Not Applicable");
                if (item != null)
                {
                    ddlLastCountryOTPractice.SelectedValue = item.Value;
                    ddlLastCountryOTPractice.Enabled = false;
                }
                var item1 = ddlLastStateOrCanadianProvince.Items.FindByText("Not Applicable");
                if (item1 != null)
                {
                    ddlLastStateOrCanadianProvince.SelectedValue = item1.Value;
                    ddlLastStateOrCanadianProvince.Enabled = false;
                }
            }
            else
            {
                ddlLastCountryOTPractice.Enabled = true;
                ddlLastStateOrCanadianProvince.Enabled = true;
            }
        }

        #endregion

        #region Methods

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetApplicationUserPracticeHistoryInfo(CurrentUserId);
            if (user != null)
            {
                if (user.PracticeHistory != null)
                {
                    if (!string.IsNullOrEmpty(user.PracticeHistory.FirstCountryPractice))
                    {
                        ddlFirstCountryOTPractice.SelectedValue = user.PracticeHistory.FirstCountryPractice;
                    }

                    if (!string.IsNullOrEmpty(user.PracticeHistory.FirstProvinceStatePractice))
                    {
                        var item = ddlFirstStateOTPractice.Items.FindByValue(user.PracticeHistory.FirstProvinceStatePractice);
                        if (item != null) ddlFirstStateOTPractice.SelectedValue = item.Value;
                    }

                    if (!string.IsNullOrEmpty(user.PracticeHistory.FirstProvincePractice))
                    {
                        var item2 = ddlFirstProvinceOTPractice.Items.FindByValue(user.PracticeHistory.FirstProvincePractice);
                        if (item2 != null) ddlFirstProvinceOTPractice.SelectedValue = item2.Value;
                    }

                    if (!string.IsNullOrEmpty(user.PracticeHistory.FirstYearPractice))
                    {
                        var item3 = ddlFirstYearOTPractice.Items.FindByValue(user.PracticeHistory.FirstYearPractice);
                        if (item3 != null) ddlFirstYearOTPractice.SelectedValue = item3.Value;
                    }

                    if (!string.IsNullOrEmpty(user.PracticeHistory.FirstYearCanadianPractice))
                    {
                        var item4 = ddlFirstYearCanadianOTPractice.Items.FindByValue(user.PracticeHistory.FirstYearCanadianPractice);
                        if (item4 != null) ddlFirstYearCanadianOTPractice.SelectedValue = item4.Value;
                    }

                    if (!string.IsNullOrEmpty(user.PracticeHistory.LastYearOutsideOntarioPractice))
                    {
                        var item5 = ddlLastYearOutsideOntario.Items.FindByValue(user.PracticeHistory.LastYearOutsideOntarioPractice);
                        if (item5 != null) ddlLastYearOutsideOntario.SelectedValue = item5.Value;
                    }

                    // disabling dropdowns if some conditions met

                    // condition #1
                    if (!string.IsNullOrEmpty(user.PracticeHistory.LastCountryPractice))
                    {
                        var item6 = ddlLastCountryOTPractice.Items.FindByValue(user.PracticeHistory.LastCountryPractice);
                        if (item6 != null) ddlLastCountryOTPractice.SelectedValue = item6.Value;
                    }

                    if (!string.IsNullOrEmpty(user.PracticeHistory.LastProvinceStatePractice))
                    {
                        var item7 = ddlLastStateOrCanadianProvince.Items.FindByValue(user.PracticeHistory.LastProvinceStatePractice);
                        if (item7 != null) ddlLastStateOrCanadianProvince.SelectedValue = item7.Value;
                    }
                    if (ddlFirstYearOTPractice.SelectedItem.Text == "Not Applicable")
                    {
                        DisableAllLists();
                    }
                    PreviousInitialFirstYear = ddlFirstYearOTPractice.SelectedItem.Text;

                    // condition #2
                    if (ddlFirstCountryOTPractice.SelectedValue == "CAN")
                    {
                        //ddlFirstProvinceOTPractice.SelectedIndex = 0;
                        //ddlFirstYearCanadianOTPractice.SelectedValue = ddlFirstYearOTPractice.SelectedValue;
                        ddlFirstYearCanadianOTPractice.Enabled = false;
                        //ddlFirstProvinceOTPractice.Enabled = false;

                        //var repository = new Repository();
                        var list3 = repository.GetGeneralList("FROM_PROVINCE");

                        ddlFirstStateOTPractice.DataSource = list3;
                        ddlFirstStateOTPractice.DataValueField = "Code";
                        ddlFirstStateOTPractice.DataTextField = "Description";
                        ddlFirstStateOTPractice.DataBind();
                        ddlFirstStateOTPractice.Items.Insert(0, string.Empty);
                        ddlFirstStateOTPractice.SelectedIndex = 0;
                        if (!string.IsNullOrEmpty(user.PracticeHistory.FirstProvinceStatePractice))
                        {
                            var item1 = ddlFirstStateOTPractice.Items.FindByValue(user.PracticeHistory.FirstProvinceStatePractice);
                            if (item1 != null) ddlFirstStateOTPractice.SelectedValue = item1.Value;
                        }

                        if (!string.IsNullOrEmpty(ddlFirstStateOTPractice.SelectedItem.Value))
                        {
                            var item2 = ddlFirstProvinceOTPractice.Items.FindByValue(ddlFirstStateOTPractice.SelectedItem.Value);
                            if (item2!=null)  ddlFirstProvinceOTPractice.SelectedValue = item2.Value;
                            ddlFirstProvinceOTPractice.Enabled = false;
                        }
                        else
                        {
                            ddlFirstProvinceOTPractice.SelectedIndex = 0;
                            ddlFirstProvinceOTPractice.Enabled = true;
                            if (!string.IsNullOrEmpty(user.PracticeHistory.FirstProvincePractice))
                            {
                                var item3 = ddlFirstProvinceOTPractice.Items.FindByValue(user.PracticeHistory.FirstProvincePractice);
                                if (item3 != null) ddlFirstProvinceOTPractice.SelectedValue = item3.Value; // user.PracticeHistory.FirstProvincePractice;
                            }
                        }
                    }

                    // condition #3
                    if (ddlFirstYearCanadianOTPractice.SelectedItem.Text == "Not Applicable")
                    {
                        var item = ddlFirstProvinceOTPractice.Items.FindByText("Not Applicable");
                        if (item != null)
                        {
                            ddlFirstProvinceOTPractice.SelectedValue = item.Value;
                        }
                        ddlFirstProvinceOTPractice.Enabled = false;
                    }

                    // condition #4 
                    if (ddlLastYearOutsideOntario.SelectedItem.Text == "Not Applicable")
                    {
                        var item = ddlLastCountryOTPractice.Items.FindByText("Not Applicable");
                        if (item != null)
                        {
                            ddlLastCountryOTPractice.SelectedValue = item.Value;
                            ddlLastCountryOTPractice.Enabled = false;
                        }
                        var item1 = ddlLastStateOrCanadianProvince.Items.FindByText("Not Applicable");
                        if (item1 != null)
                        {
                            ddlLastStateOrCanadianProvince.SelectedValue = item1.Value;
                            ddlLastStateOrCanadianProvince.Enabled = false;
                        }
                    }
                }
            }
            var user2 = repository.GetApplicationUserInfo(CurrentUserId);
            if (user2 != null)
            {
                CurrentUser = user2;
                lblPracticeHistoryTitle.Text = string.Format("OT Practice History for {0} #{1}", CurrentUser.FullName, CurrentUser.Id);

                if (user2.CurrentStatus.ToUpper().Contains("FORMER"))
                {
                    ddlFirstYearOTPractice.Enabled = false;
                    rfvFirstYearOTPractice.Enabled = false;

                    ddlFirstCountryOTPractice.Enabled = false;
                    rfvFirstCountryOTPractice.Enabled = false;

                    ddlFirstStateOTPractice.Enabled = false;
                    rfvFirstStateOTPractice.Enabled = false;

                    ddlFirstYearCanadianOTPractice.Enabled = false;
                    rfvFirstYearCanadianOTPractice.Enabled = false;

                    ddlFirstProvinceOTPractice.Enabled = false;
                    rfvFirstProvinceOTPractice.Enabled = false;

                    trDescription.Visible = true;
                    trDescriptionNew.Visible = false;
                }
                else
                {
                    trDescription.Visible = false;
                    trDescriptionNew.Visible = true;
                }
            }
        }

        protected void BindLists()
        {
            //var list2 = Countries.ListOfCountries;
            var repository = new Repository();
            var list1 = repository.GetGeneralList("COUNTRY");
            list1 = list1.OrderBy(I => I.Description).ToList();
            ddlFirstCountryOTPractice.DataSource = list1;
            ddlFirstCountryOTPractice.DataValueField = "Code";
            ddlFirstCountryOTPractice.DataTextField = "Description";
            ddlFirstCountryOTPractice.DataBind();
            ddlFirstCountryOTPractice.Items.Insert(0, string.Empty);

            ddlLastCountryOTPractice.DataSource = list1;
            ddlLastCountryOTPractice.DataValueField = "Code";
            ddlLastCountryOTPractice.DataTextField = "Description";
            ddlLastCountryOTPractice.DataBind();
            ddlLastCountryOTPractice.Items.Insert(0, string.Empty);

            var list2 = repository.GetGeneralList("PROVINCE_STATE");

            ddlFirstStateOTPractice.DataSource = list2;
            ddlFirstStateOTPractice.DataValueField = "Code";
            ddlFirstStateOTPractice.DataTextField = "Description";
            ddlFirstStateOTPractice.DataBind();
            ddlFirstStateOTPractice.Items.Insert(0, string.Empty);

            ddlLastStateOrCanadianProvince.DataSource = list2;
            ddlLastStateOrCanadianProvince.DataValueField = "Code";
            ddlLastStateOrCanadianProvince.DataTextField = "Description";
            ddlLastStateOrCanadianProvince.DataBind();
            ddlLastStateOrCanadianProvince.Items.Insert(0, string.Empty);

            var list3 = repository.GetGeneralList("FROM_PROVINCE");

            ddlFirstProvinceOTPractice.DataSource = list3;
            ddlFirstProvinceOTPractice.DataValueField = "Code";
            ddlFirstProvinceOTPractice.DataTextField = "Description";
            ddlFirstProvinceOTPractice.DataBind();
            ddlFirstProvinceOTPractice.Items.Insert(0, string.Empty);

            //ddlLastCanadianProvince.DataSource = list3;
            //ddlLastCanadianProvince.DataValueField = "Code";
            //ddlLastCanadianProvince.DataTextField = "Description";
            //ddlLastCanadianProvince.DataBind();
            //ddlLastCanadianProvince.Items.Insert(0, string.Empty);
            var list4 = repository.GetGeneralList("grad_year");

            try
            {
                list4 = list4.Where(I => I.Code != "9999").ToList();
                list4 = list4.Where(I => int.Parse(I.Code) <= DateTime.Now.Year).ToList();
            }
            catch { }

            list4 = list4.OrderByDescending(I => I.Code).ToList();
            list4.Insert(0, new GenClass { Code = string.Empty, Description = string.Empty });
            list4.Insert(1, new GenClass { Code = "N/A", Description = "Not Applicable" });


            ddlFirstYearOTPractice.DataSource = list4;
            ddlFirstYearOTPractice.DataValueField = "Code";
            ddlFirstYearOTPractice.DataTextField = "Description";
            ddlFirstYearOTPractice.DataBind();
            //ddlFirstYearOTPractice.Items.Insert(0, string.Empty);

            ddlFirstYearCanadianOTPractice.DataSource = list4;
            ddlFirstYearCanadianOTPractice.DataValueField = "Code";
            ddlFirstYearCanadianOTPractice.DataTextField = "Description";
            ddlFirstYearCanadianOTPractice.DataBind();
            //ddlFirstYearCanadianOTPractice.Items.Insert(0, string.Empty);

            // filter out not recent years
            ddlLastYearOutsideOntario.DataSource = list4;
            ddlLastYearOutsideOntario.DataValueField = "Code";
            ddlLastYearOutsideOntario.DataTextField = "Description";
            ddlLastYearOutsideOntario.DataBind();
            //ddlLastYearOutsideOntario.Items.Insert(0, string.Empty);


            //var list5 = new List<GenClass>();
            ////list5.Add(new GenClass { Code = "", Description = "" });
            //list5.Add(new GenClass { Code = "No", Description = "No" });
            //list5.Add(new GenClass { Code = "Yes", Description = "Yes" });

            //ddlOutsideOntarioPractice.DataSource = list5;
            //ddlOutsideOntarioPractice.DataValueField = "Code";
            //ddlOutsideOntarioPractice.DataTextField = "Description";
            //ddlOutsideOntarioPractice.DataBind();
        }

        protected void DisableAllLists()
        {
            var item = ddlFirstCountryOTPractice.Items.FindByText("Not Applicable");
            if (item != null)
            {
                ddlFirstCountryOTPractice.SelectedValue = item.Value;
                ddlFirstCountryOTPractice.Enabled = false;
            }
            var item2 = ddlFirstStateOTPractice.Items.FindByText("Not Applicable");
            if (item2 != null)
            {
                ddlFirstStateOTPractice.SelectedValue = item2.Value;
                ddlFirstStateOTPractice.Enabled = false;
            }

            var item3 = ddlFirstYearCanadianOTPractice.Items.FindByText("Not Applicable");
            if (item3 != null)
            {
                ddlFirstYearCanadianOTPractice.SelectedValue = item3.Value;
                ddlFirstYearCanadianOTPractice.Enabled = false;
            }
            var item4 = ddlFirstProvinceOTPractice.Items.FindByText("Not Applicable");
            if (item4 != null)
            {
                ddlFirstProvinceOTPractice.SelectedValue = item4.Value;
                ddlFirstProvinceOTPractice.Enabled = false;
            }

            var item5 = ddlLastYearOutsideOntario.Items.FindByText("Not Applicable");
            if (item5 != null)
            {
                ddlLastYearOutsideOntario.SelectedValue = item5.Value;
                ddlLastYearOutsideOntario.Enabled = false;
            }
            var item6 = ddlLastCountryOTPractice.Items.FindByText("Not Applicable");
            if (item6 != null)
            {
                ddlLastCountryOTPractice.SelectedValue = item6.Value;
                ddlLastCountryOTPractice.Enabled = false;
            }
            var item7 = ddlLastStateOrCanadianProvince.Items.FindByText("Not Applicable");
            if (item7 != null)
            {
                ddlLastStateOrCanadianProvince.SelectedValue = item7.Value;
                ddlLastStateOrCanadianProvince.Enabled = false;
            }
        }

        protected void UpdateUserPracticeHistory()
        {
            User user = new User();
            user.Id = CurrentUserId;

            user.PracticeHistory = new PracticeHistory();
            user.PracticeHistory.FirstCountryPractice = ddlFirstCountryOTPractice.SelectedValue;
            user.PracticeHistory.FirstProvinceStatePractice = ddlFirstStateOTPractice.SelectedValue;
            user.PracticeHistory.FirstProvincePractice = ddlFirstProvinceOTPractice.SelectedValue;
            user.PracticeHistory.FirstYearPractice = ddlFirstYearOTPractice.SelectedValue;
            user.PracticeHistory.FirstYearCanadianPractice = ddlFirstYearCanadianOTPractice.SelectedValue;

            //if (ddlOutsideOntarioPractice.SelectedIndex == 1)
            //{
                user.PracticeHistory.LastCountryPractice = ddlLastCountryOTPractice.SelectedValue;
                //user.PracticeHistory.LastProvincePractice = ddlLastCanadianProvince.SelectedValue;
                user.PracticeHistory.LastProvinceStatePractice = ddlLastStateOrCanadianProvince.SelectedValue;
                user.PracticeHistory.LastYearOutsideOntarioPractice = ddlLastYearOutsideOntario.SelectedValue;
            //}
            //else
            //{
            //    user.PracticeHistory.LastCountryPractice = string.Empty;
            //    //user.PracticeHistory.LastProvincePractice = ddlLastCanadianProvince.SelectedValue;
            //    user.PracticeHistory.LastProvinceStatePractice = string.Empty;
            //    user.PracticeHistory.LastYearOutsideOntarioPractice = string.Empty;
            //}
            var repository = new Repository();
            repository.UpdateApplicationUserPracticeHistoryLogged(CurrentUserId, user);
        }

        protected bool ValidationCheck()
        {

            Page.Validate("GeneralValidation");

            if (!Page.IsValid)
            {
                return false;
            }
            int initPracticeFirstYear = 0;
            int canadPractice = 0;
            int mostRecent = 0;
            int.TryParse(ddlFirstYearOTPractice.SelectedValue, out initPracticeFirstYear);
            int.TryParse(ddlFirstYearCanadianOTPractice.SelectedValue, out canadPractice);
            int.TryParse(ddlLastYearOutsideOntario.SelectedValue, out mostRecent);

            if (initPracticeFirstYear != 0 && canadPractice != 0 && initPracticeFirstYear > canadPractice)
            {
                lblFirstYearCanadianPracticeErrorMessage.Text = "<br />First year practiced occupational therapy in Canada cannot be earlier than initial year of practice";
                return false;
            }

            if (initPracticeFirstYear != 0 && mostRecent != 0 && initPracticeFirstYear > mostRecent)
            {
                lblLastYearOutsideOntarioErrorMessage.Text = "<br />Most recent year practiced occupational therapy outside of Ontario cannot be earlier than initial year of practice";
                return false;
            }

            return true;
        }

        /// <summary>
        ///   Allow developer adds custom errors, warning into Messages Collection.
        /// </summary>
        /// <param name="message">Message information.</param>
        /// <param name="messageType">Message type.</param>
        public void AddMessage(string message, PageMessageType messageType)
        {
            this.Messages.Add(new PageMessage(message, messageType));
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        /// <summary>
        ///  Messages collection. Contains all custom warning or errors which will be populated on the page.
        /// </summary>
        public PageMessages Messages
        {
            get
            {
                return (PageMessages)SessionParameters.PageMessages;
            }
            set
            {
                SessionParameters.PageMessages = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                if (SessionParameters.CurrentUser != null)
                {
                    return SessionParameters.CurrentUser;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUser = value;
            }
        }

        protected string PreviousInitialFirstYear
        {
            get
            {
                if (ViewState["PreviousInitialFirstYear"] != null)
                {
                    return (string)ViewState["PreviousInitialFirstYear"];
                }
                else
                    return string.Empty;
            }
            set
            {
                ViewState["PreviousInitialFirstYear"] = value;
            }
        }
        #endregion
       
    }
}