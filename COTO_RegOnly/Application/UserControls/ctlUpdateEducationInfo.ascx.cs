﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlUpdateEducationInfo : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.Application_Step0);
                return;
            }
        }

        protected void btnSubmitClick(object sender, ImageClickEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtEducationProfileChanges.Text))
            {
                var repository = new Repository();
                var user2 = repository.GetApplicationUserInfo(CurrentUserId);

                string message = string.Format("Member ID: {0}<br />", CurrentUserId);
                message += string.Format("Full Name: {0}<br />", user2.FullName);
                message += string.Format("Education Change Request Details: {0}<br /><br />", txtEducationProfileChanges.Text);
                message += string.Format("Date: {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));


                string emailSubject = string.Format("Update Education Information for {0} ({1})", user2.FullName, CurrentUserId);
                string emailTo = WebConfigItems.ApplicationSupportEmail;
                var tool = new Tools();
                tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);
                Response.Redirect(string.Format("{0}?common=updated", WebConfigItems.Application_Step4));
            }
            else
            {
                lblMessage.Text = "The Details field is empty";
                lblMessage.Visible = true;
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(WebConfigItems.Application_Step4);
        }

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }
    }
}