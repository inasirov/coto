﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlSignUp.ascx.cs" Inherits="COTO_RegOnly.Application.UserControls.ctlSignUp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="upMainForm" runat="server">
            <ContentTemplate>
                <table id="tbEditMode" runat="server" style="border-spacing: 0; padding: 0; width: 100%">
                    <tr class="HeaderTitle" style="text-align:right;">
                        <td colspan="2">
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Registration Application - New member registration" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr class="RowTitle">
                        <td colspan="2">
                            <asp:Label ID="lblPersonalInformationSectionTitle" CssClass="heading" runat="server"
                                            Text="Personal Contact Details" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left">
                            <span>*-denotes required fields</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="LeftLeftTitle">
                            <asp:Label ID="lblPageErrorMessage" runat="server" ForeColor="Red" />
                            <asp:ValidationSummary ID="vsSummary" runat="server" ValidationGroup="GeneralValidation" DisplayMode="BulletList" ShowSummary="true" EnableClientScript="false" CssClass="valSummary" />
                            <asp:ValidationSummary ID="vsSummaryHomeAddress" runat="server" ValidationGroup="HomeAddressValidation" DisplayMode="BulletList" ShowSummary="true" EnableClientScript="false" CssClass="valSummary" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span>Prefix:</span>
                        </td>
                        <td class="RightColumn" style="width:60%">
                            <asp:DropDownList ID="ddlPrefix" runat="server" />
                            <asp:HiddenField ID="hfUserID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span>First Name:*</span>
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtFirstName" runat="server" Width="160px" MaxLength="20" />
                            <asp:RequiredFieldValidator ID="rfvFirstname" runat="server" ControlToValidate="txtFirstname"
                                ErrorMessage="First name is blank." Text="*" ValidationGroup="GeneralValidation"
                                EnableClientScript="false" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span>Middle Name:</span>
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtMiddleName" runat="server" Width="160px" MaxLength="20" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span>Last Name:*</span>
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtLastName" runat="server" Width="160px" MaxLength="30" />
                            <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                                ErrorMessage="Last name is blank." Text="*" ValidationGroup="GeneralValidation"
                                EnableClientScript="false" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span>Preferred Email:*</span>
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtPreferredEmail" runat="server" Width="160px" MaxLength="50" />
                            <asp:RequiredFieldValidator ID="rfvPreferredEmail" runat="server" ControlToValidate="txtPreferredEmail"
                                EnableClientScript="false" ErrorMessage="Preferred Email is blank." Text="*"
                                ValidationGroup="GeneralValidation" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revPreferredEmail" runat="server" ControlToValidate="txtPreferredEmail"
                                ErrorMessage="Please provide a valid preferred email" Text="*"
                                EnableClientScript="false" ForeColor="Red" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                ValidationGroup="GeneralValidation" />
                            <asp:Label ID="lblPreferredEmailErrorMessage" runat="server" ForeColor="Red" />
                        </td>
                    </tr>
                    <tr id="trLoginDetails1" runat="server" class="RightColumn">
                        <td colspan="2">
                            <h4>Login Details</h4>
                        </td>
                    </tr>
                    <tr id="trLoginDetails2" runat="server">
                        <td colspan="2" class="ETD2">
                            ATTENTION: Please remember the login and password you have entered as these are
                            required for logging in to the COTO website
                        </td>
                    </tr>
                    <tr id="trLoginDetails3" runat="server">
                        <td class="LeftLeftTitle">
                            <span>Login:*</span>
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtLogin" runat="server" Width="160px" MaxLength="30" />
                            <asp:Label ID="lblLoginErrorMessage" runat="server" ForeColor="Red" />
                            <asp:RequiredFieldValidator ID="rfvLogin" runat="server" ControlToValidate="txtLogin" 
                                ErrorMessage="Login field is blank." ValidationGroup="GeneralValidation" Text="*"
                                EnableClientScript="false" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="trLoginDetails4" runat="server">
                        <td class="LeftLeftTitle">
                            <span>Choose a Password:*</span>
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtPwrd" runat="server" Width="160px" TextMode="Password" MaxLength="15" />
                            <asp:Label ID="lblPasswordErrorMessage" runat="server" ForeColor="Red"/>
                       </td>
                    </tr>
                    <tr id="trLoginDetails5" runat="server">
                        <td class="LeftLeftTitle">
                            <span>Verify Password:*</span>
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtVerifyPwrd" runat="server" Width="160px" TextMode="Password" MaxLength="15" />
                            <asp:Label ID="lblVerifyPasswordErrorMessage" runat="server" ForeColor="Red" />
                        </td>
                    </tr>
                    <tr id="trLoginDetails6" runat="server">
                        <td>
                            &nbsp;
                        </td>
                        <td class="RightColumn">
                            <span>Must be 6 characters (not case sensitive)</span>
                        </td>
                    </tr>
                    <tr id="trLoginDetails7" runat="server">
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="RightColumn">
                            <h4>Home Address</h4>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="LeftLeftTitle">
                            <asp:Label ID="lblHomeAddressTitle" runat="server" Text="Address:" />
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtHomeAddress1" runat="server" Width="160px" MaxLength="40"/>
                            <asp:RequiredFieldValidator ID="rfvHomeAddress1" runat="server" ControlToValidate="txtHomeAddress1"
                                ErrorMessage="Home Address is blank." ValidationGroup="HomeAddressValidation"
                                EnableClientScript="false" ForeColor="Red" Text="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            &nbsp;
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtHomeAddress2" runat="server" Width="160px" MaxLength="40" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            &nbsp;
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtHomeAddress3" runat="server" Width="160px" MaxLength="40" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <asp:Label ID="lblHomeCityTitle" runat="server" Text="City:" />
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtHomeCity" runat="server" Width="160px" MaxLength="40"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvHomeCity" runat="server" ControlToValidate="txtHomeCity"
                                ErrorMessage="Home City is blank." ValidationGroup="HomeAddressValidation"
                                EnableClientScript="false" ForeColor="Red" Text="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <asp:Label ID="lblHomeCountryTitle" runat="server" Text="Country:" />
                        </td>
                        <td class="RightColumn">
                            <asp:DropDownList ID="ddlHomeCountry" runat="server" 
                                onselectedindexchanged="ddlHomeCountry_SelectedIndexChanged" AutoPostBack="true" />
                            <asp:Label ID="lblHomeCountryErrorMessage" runat="server" ForeColor="Red" />
                            <asp:RequiredFieldValidator ID="rfvHomeCountry" runat="server" ControlToValidate="ddlHomeCountry"
                                InitialValue="" ValidationGroup="HomeAddressValidation" ErrorMessage="Please select Home Country."
                                EnableClientScript="false" ForeColor="Red" Text="*"/>
                        </td>
                    </tr>
                    <tr id="trHomeProvinceDropdown"  runat="server">
                        <td class="LeftLeftTitle">
                            <asp:Label ID="lblHomeProvinceTitle" runat="server" Text="Province/State:" />
                        </td>
                        <td class="RightColumn">
                            <asp:DropDownList ID="ddlHomeProvince" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvHomeProvince" runat="server" ControlToValidate="ddlHomeProvince"
                                InitialValue="" ValidationGroup="HomeAddressValidation" ErrorMessage="Please select Home Province."
                                 EnableClientScript="false" ForeColor="Red" Text="*"/>
                        </td>
                    </tr>
                    <tr id="trHomeProvinceTextBox"  runat="server" visible="false">
                        <td class="LeftLeftTitle">
                            <asp:Label ID="lblHomeProvince2Title" runat="server" Text="Province/State:" />
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtHomeProvince" runat="server" Width="160px" MaxLength="15"/>
                            <asp:RequiredFieldValidator ID="rfvHomeProvince2" runat="server" ControlToValidate="txtHomeProvince"
                                ValidationGroup="HomeAddressValidation" ErrorMessage="Home Province is blank."
                                EnableClientScript="false" ForeColor="Red" Text="*"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <asp:Label ID="lblHomePostalCodeTitle" runat="server" Text="Postal Code/Zip:" />
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtHomePostalCode" runat="server" Width="160px" MaxLength="10" />
                            <asp:RequiredFieldValidator ID="rfvHomePostalCode" runat="server" ControlToValidate="txtHomePostalCode"
                                ErrorMessage="Home Postal Code/Zip is blank." ValidationGroup="HomeAddressValidation"
                                EnableClientScript="false" ForeColor="Red" Text="*"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revHomePostalCode" runat="server" ControlToValidate="txtHomePostalCode"
                                            ErrorMessage="Please provide a valid Home Postal Code" EnableClientScript="false"
                                            ValidationExpression="^([abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][a-zA-Z](\s)?[0-9][a-zA-Z][0-9])$"
                                            ValidationGroup="HomeAddressValidation" ForeColor="Red" Text="*"></asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator ID="revHomePostalCodeUS" runat="server" ControlToValidate="txtHomePostalCode"
                                            ErrorMessage="Please provide a valid Home Postal Code" EnableClientScript="false"
                                            ValidationExpression="(^\d{5}(-\d{4})?)$"
                                            ValidationGroup="HomeAddressValidation" ForeColor="Red" Text="*"/>
                            &nbsp;<asp:Label ID="lblHomePostalCodeErrorMessage" runat="server" ForeColor="Red" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <asp:Label ID="lblHomePhoneTitle" runat="server" Text="Phone:" />
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtHomePhone" runat="server" Width="160px" MaxLength="25"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvHomePhone" runat="server" ControlToValidate="txtHomePhone"
                                ErrorMessage="Home Phone is blank." ValidationGroup="HomeAddressValidation"
                                EnableClientScript="false" ForeColor="Red" Text="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="LeftLeftTitle">
                            <span>Fax:</span>
                        </td>
                        <td class="RightColumn">
                            <asp:TextBox ID="txtHomeFax" runat="server" Width="160px" MaxLength="25"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="VAU_Button" OnClick="btnSubmit_Click" OnClientClick="this.disabled = true; this.value = 'Submitting...';" UseSubmitBehavior="false"/>
                            &nbsp;
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="VAU_Button" OnClick="btnReset_Click" />
                        </td>
                    </tr>
                </table>

                </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSubmit" />
                <asp:AsyncPostBackTrigger ControlID="ddlHomeCountry" />
            </Triggers>
        </asp:UpdatePanel>
    </center>
</div>