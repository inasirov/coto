﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlDeclaration.ascx.cs" Inherits="COTO_RegOnly.Application.UserControls.ctlDeclaration" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<div class="MainForm">
    <center>
        <table style="border: 0; border-spacing: 0; padding: 0; width: 100%">
            <tr class="HeaderTitle" align="right">
                <td>
                    <asp:Label ID="lblPageTitleLabel" runat="server" Text="Registration Application Step 10 of 11" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <uc:MessageBox ID="omb" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <table class="memberInfo" style="border-spacing: 2px; padding: 3px; width: 100%">
                        <tr class="RowTitle">
                            <td colspan="2">
                                <div>
                                    <asp:Label ID="lblDeclarationTitle" CssClass="heading" runat="server" Text="Registration Declaration" />
                                </div>
                                
                            </td>
                        </tr>
                       <%-- <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblDeclarationText" runat="server" Font-Bold="true" Font-Size="Larger" Text="Once you complete this page you are no longer able to access
                                your online form. Please ensure that your form is complete before you proceed to the payment page." />
                            </td>
                        </tr>--%>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblRegAuthorizationTitle" runat="server" Text="Authorization Declaration" Font-Bold="true"
                                    Font-Size="Larger" /><br /><br />
                                <asp:Label ID="lblRegDeclarationDesc" runat="server" Text="I hereby authorize the College of Occupational Therapists of Ontario to obtain 
                                information from other regulatory organizations, educational institutions, present and former employers, and any other sources for the purposes 
                                related to my registration and qualifications. Answering 'yes' to this statement is my sufficient and irrevocable authority 
                                for these persons or entities to release this information to the College." />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; text-align: right;">
                                <asp:Label ID="lblAgreeAuthorizationTitle" runat="server" Text="I agree*" Font-Bold="true" />
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlAgreeAuthorization" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblRegDeclarationTitle" runat="server" Text="Registration Declaration" Font-Bold="true"
                                    Font-Size="Larger" /><br /><br />
                                <asp:Label ID="lblQualityAssuranceDeclarationDesc" runat="server" Text="I hereby certify that the statements made 
                                by me on this application are complete and correct to the best of my knowledge and belief. 
                                I understand that a false or misleading statement may disqualify me from registration or may be 
                                cause for revocation of any registration which may be granted to me." />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; text-align: right;">
                                <asp:Label ID="lblAgreeDeclaration" runat="server" Text="I agree*" Font-Bold="true" />
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlAgreeDeclaration" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblPaymentApplicationFeeTitle" runat="server" Text="Payment of Application Fee" Font-Bold="true"
                                    Font-Size="Larger" /><br /><br />
                                <asp:Label ID="lblPaymentApplicationFeeDescription" runat="server" Text="I understand that my application will not be considered complete and will not be reviewed 
                                until I have submitted the application fee on the next page or offline." />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; text-align: right;">
                                <asp:Label ID="lblPaymentAgreement" runat="server" Text="I agree*" Font-Bold="true" />
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlPaymentFee" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <asp:Label ID="lblNewslettersUpdates" runat="server" Text="Newsletter Updates" Font-Bold="true"
                                    Font-Size="Larger" /><br /><br />
                                <asp:Label ID="lblNewslettersDescription" runat="server" Text="I would like to sign up to receive electronic newsletter updates from the College of Occupational Therapists of Ontario." />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; text-align: right;">
                                <asp:Label ID="Label3" runat="server" Text="I agree*" Font-Bold="true" />
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlNewsletterUpdates" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <span>By saying yes, you are consenting to receive electronic newsletter updates from the College of Occupational Therapists. 
                                    You can revoke your consent to receive emails at any time by using the "Unsubscribe®" link, found at the bottom of every email. 
                                    <u>Emails are serviced by Constant Contact.</u>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" colspan="2">
                                <asp:Label ID="lblSatisfactionQuestion" runat="server" Text="How satisfied were you with the online application tool?" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; text-align: right;">
                                <b>*</b>
                            </td>
                            <td class="RightColumn">
                                <asp:DropDownList ID="ddlSatisfaction" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" colspan="2">
                                <asp:Label ID="lblSatisfactionMessage" runat="server" Text="If you were at all dissatisfied with the online application tool, please provide additional details below:" /><br />
                                <asp:TextBox ID="txtSatisfactionNotes" runat="server" MaxLength="1000" Rows="3" 
                                    Width="500px" TextMode="MultiLine" />
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="2">
                                <asp:LinkButton ID="lbtnOfflinePayment" runat="server" Text="Offline Payment" OnClick="lbtnOfflinePaymentClick" /><br />
                                <asp:LinkButton ID="lbtnAnnualRegistrationSummary" runat="server" Text="Annual Registration Summary" OnClick="lbtnAnnualRegistrationSummaryClick" /><br />
                                <asp:LinkButton ID="lbtnRenewalFeedback" runat="server" Text="Renewal Feedback" OnClick="lbtnRenewalFeedbackClick" /><br />
                            </td>
                        </tr>--%>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                </td>
            </tr>
        </table>
    </center>
</div>