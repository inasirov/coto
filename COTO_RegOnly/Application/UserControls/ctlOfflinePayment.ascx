﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlOfflinePayment.ascx.cs" Inherits="COTO_RegOnly.Application.UserControls.ctlOfflinePayment" %>

<%@ Import Namespace="COTO_RegOnly.Classes" %>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnPrintForm" />
            </Triggers>
            <ContentTemplate>
                <table style="border: 0; border-spacing: 0; padding: 0; width:100%;">
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <%--<asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                Visible="false" ValidationGroup="PersonalValidation" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" style="border-spacing: 2px; padding: 3px; width:100%;">
                                <tr class="RowTitle">
                                    <td>
                                        <asp:Label ID="lblPersonalInformationSectionTitle" CssClass="heading" runat="server"
                                            Text="Offline Payment" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <p>
                                            For offline payment, please print this&nbsp;
                                            <asp:LinkButton ID="lbtnPrintForm" runat="server" Text="form" OnClick="lbtnPrintFormClick" />&nbsp;
                                            and return to the College.
                                        </p>
                                        <p>
                                            If you wish to print a PDF report of your registration information please&nbsp;
                                            <%--<asp:HyperLink ID="hlPrintReport" runat="server" Text="click here" Target="_blank" NavigateUrl='<%# GetPrintReportUrl() %>' />--%>
                                            <asp:LinkButton ID="lbtnPrintReport" runat="server" Text="click here" />&nbsp; - this may take a few minutes.
                                        </p>
                                        <p>
                                            If you would like to provide feedback with regard to your online application experience, please&nbsp;
                                            <asp:LinkButton ID="lbtnFeedbackLink" runat="server" Text="click here" OnClick="lbtnFeedbackLinkClick" />
                                        </p>
                                        
                                        <%--<p>
                                           <asp:Label ID="lblApplicationFees"  runat="server" />
                                        </p>--%>
                                        <p style="font-weight: bold;">
                                            Thank you for completing your registration application online.
                                        </p>
                                        <asp:Label ID="lblTest" runat="server" CssClass="hidden-class"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>

<style type="text/css">
    .hidden-class{
        visibility:hidden;
    }

</style>