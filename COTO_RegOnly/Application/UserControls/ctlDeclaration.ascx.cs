﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using System.Text;
using System.Security.Cryptography;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlDeclaration : System.Web.UI.UserControl
    {

        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;

        private string PrevStep = WebConfigItems.Application_Step9;
        private string NextStep = WebConfigItems.Application_Step11;
        private const int CurrentStep = 10;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.Application_Step0);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.ApplicationStep = CurrentStep;
                BindLists();
                BindData();
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            var retMessage = CheckDeclaration();
            if (string.IsNullOrEmpty(retMessage))
            {
                UpdateUserDeclaration();
            }
            else
            {
                ShowMessage(string.Format("<font color='Red'>{0}</font>", retMessage), "Error:");
                return;
            }

            //Response.Redirect("MemberDisplay.aspx");

            string fullName = string.Empty;
            if (CurrentUser != null)
            {
                fullName = CurrentUser.FullName;
            }
            UpdateSteps(1);
            Response.Redirect(NextStep);
        }

        #endregion

        #region Methods

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.ApplicationStep != 0)
            {
                SessionParameters.ApplicationStep += diff;
            }
        }

        protected void UpdateUserDeclaration()
        {
            int currentYear = DateTime.Now.Year;
            if (DateTime.Now.Month <= 5)
            {
                currentYear = currentYear - 1;
            }
            User user = new User();

            user.Id = CurrentUserId;
            user.DeclarationInfo = new Classes.Declaration();

            user.DeclarationInfo.ApplicationDeclarationDate = DateTime.Today;
            user.DeclarationInfo.ApplicationAuthorizationDate = DateTime.Today;

            double satisfaction = 0;
            double.TryParse(ddlSatisfaction.SelectedValue, out satisfaction);
            user.DeclarationInfo.Satisfaction = satisfaction;
           
            user.DeclarationInfo.SatisfactionNotes = txtSatisfactionNotes.Text;

            user.DeclarationInfo.PaymentApplicationFee = (ddlPaymentFee.SelectedValue.ToUpper()=="YES");
            user.DeclarationInfo.NewsletterUpdates = (ddlNewsletterUpdates.SelectedValue.ToUpper() == "YES");

            var repository = new Repository();
            repository.UpdateApplicationUserDeclarationInfoLogged(CurrentUserId, user, currentYear);

            
            var user2 = repository.GetApplicationUserInfo(CurrentUserId);
            
            var user0 = repository.GetApplicationUserCurrencyHoursInfo(CurrentUserId, currentYear);
            var list1 = repository.GetGeneralList("CURRENCY_REQ"); // get data for status field
            //list1 = list1.Where(I => I.Code.StartsWith("A_")).ToList();

            //foreach (var item in list1)
            //{
            //    item.Code = item.Code.Replace("A_", "");
            //}

            var currency = list1.Find(I => I.Code == user0.CurrencyLanguageServices.Currency);

            string message = string.Format("Full Name: {0}<br />", user2.FullName);
            message += string.Format("iMIS ID#: {0}<br />", CurrentUserId);
            //message += string.Format("Member ID: {0}<br />", CurrentUserId);
            if (currency != null)
            {
                message += string.Format("Currency Hours: {0}<br />", currency.Description);
            }

            var user3 = repository.GetApplicationUserEducationInfo(CurrentUserId);
            var country = user3.Education.EntryDegree.Country;
            country = repository.GetGeneralNameSubstImprv("COUNTRY", country);

            if (!string.IsNullOrEmpty(country))
            {
                message += string.Format("Country of OT Education: {0}<br />", country);
            }

            if (user2.RegisterBy != DateTime.MinValue)
            {
                message += string.Format("Register By: {0}<br />", user2.RegisterBy.ToString("yyyy-MM-dd"));
            }

            message += string.Format("Date: {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

            string emailSubject = string.Format("New Application Received, iMIS ID# :{0}, Name:{1}", CurrentUserId, user2.FullName);
            string emailTo = WebConfigItems.ApplicationSupportEmail;
            var tool = new Tools();
            tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);


            //// if there is the dissatisfaction details provide from user, send email to Brandi
            //if (!string.IsNullOrEmpty(txtSatisfactionNotes.Text))
            //{
            //    message = string.Format("Satisfaction rate selected: {0}<br /> ", ddlSatisfaction.SelectedItem.Text);
            //    message += string.Format("Feedback notes from Declaration page: {0} ", txtSatisfactionNotes.Text);
            //    emailSubject = string.Format("New Application Received, iMIS ID# :{0}, Name:{1}", CurrentUserId, user2.FullName);
            //    emailTo = WebConfigItems.RegistrationManagerContactEmail;
            //    tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);
            //}
        }

        protected string CheckDeclaration()
        {
            string retValue = string.Empty;
            if (ddlAgreeAuthorization.SelectedValue.ToUpper() != "YES")
            {
                retValue = "You must answer 'Yes' to the Authorization Declaration question in order to proceed with the registration application.";
            }

            if (ddlAgreeDeclaration.SelectedValue.ToUpper() != "YES")
            {
                retValue += "You must answer 'Yes' to the Registration Declaration question in order to proceed with the registration application.";
            }

            if (ddlPaymentFee.SelectedValue.ToUpper() != "YES")
            {
                retValue += "You must answer 'Yes' to the Payment of Application Fee question in order to proceed with the registration application.";
            }

            if (ddlNewsletterUpdates.SelectedValue == "")
            {
                retValue += "You must answer to the Newsletter Updates question in order to proceed with the registration application.";
            }

            if (ddlSatisfaction.SelectedValue == "")
            {
                retValue += "You must answer to the Satisfaction question in order to proceed with the registration application.";
            }

            return retValue;
        }

        protected void BindLists()
        {
            var list1 = new List<GenClass>();
            list1.Add(new GenClass { Code = "", Description = "" });
            list1.Add(new GenClass { Code = "Yes", Description = "Yes" });

            ddlPaymentFee.DataSource = list1;
            ddlPaymentFee.DataValueField = "Code";
            ddlPaymentFee.DataTextField = "Description";
            ddlPaymentFee.DataBind();

            list1.Add(new GenClass { Code = "No", Description = "No" });

            ddlAgreeAuthorization.DataSource = list1;
            ddlAgreeAuthorization.DataValueField = "Code";
            ddlAgreeAuthorization.DataTextField = "Description";
            ddlAgreeAuthorization.DataBind();

            ddlAgreeDeclaration.DataSource = list1;
            ddlAgreeDeclaration.DataValueField = "Code";
            ddlAgreeDeclaration.DataTextField = "Description";
            ddlAgreeDeclaration.DataBind();

            ddlNewsletterUpdates.DataSource = list1;
            ddlNewsletterUpdates.DataValueField = "Code";
            ddlNewsletterUpdates.DataTextField = "Description";
            ddlNewsletterUpdates.DataBind();

            //var list2 = new List<GenClass>();
            var repository = new Repository();
            var list2 = repository.GetGeneralList("SATISFACTION");
            list2 = list2.OrderByDescending(I => I.Code).ToList();
            list2.Insert(0,new GenClass { Code = "", Description = "" });
            //list2.Add(new GenClass { Code = "6", Description = "7 Very Satisfied" });
            //list2.Add(new GenClass { Code = "6", Description = "6 Satisfied" });
            //list2.Add(new GenClass { Code = "5", Description = "5 Somewhat Satisfied" });
            //list2.Add(new GenClass { Code = "4", Description = "4 Neutral" });
            //list2.Add(new GenClass { Code = "3", Description = "3 Somewhat Dissatisfied" });
            //list2.Add(new GenClass { Code = "2", Description = "2 Dissatisfied" });
            //list2.Add(new GenClass { Code = "1", Description = "1 Very Dissatisfied" });

            ddlSatisfaction.DataSource = list2;
            ddlSatisfaction.DataValueField = "Code";
            ddlSatisfaction.DataTextField = "Description";
            ddlSatisfaction.DataBind();
        }

        protected void BindData()
        {
            var repository = new Repository();
            int currentYear = DateTime.Now.Year;
            if (DateTime.Now.Month <= 5)
            {
                currentYear = currentYear - 1;
            }

            var user = repository.GetApplicationUserDeclarationInfo(CurrentUserId, currentYear);
            if (user != null)
            {
                if (user.DeclarationInfo != null)
                {
                    if (user.DeclarationInfo.ApplicationDeclarationDate != null)
                    {
                        ddlAgreeDeclaration.SelectedValue = user.DeclarationInfo.ApplicationDeclaration ? "Yes" : "No";
                    }
                    else
                    {
                        ddlAgreeDeclaration.SelectedValue = string.Empty;
                    }

                    if (user.DeclarationInfo.ApplicationAuthorizationDate != null)
                    {
                        ddlAgreeAuthorization.SelectedValue = user.DeclarationInfo.ApplicationAuthorization ? "Yes" : "No";
                    }
                    else
                    {
                        ddlAgreeAuthorization.SelectedValue = string.Empty;
                    }
                    if (user.DeclarationInfo.PaymentApplicationFee)
                    {
                        ddlPaymentFee.SelectedValue = "Yes";
                    }
                    if (user.DeclarationInfo.NewsletterUpdates)
                    {
                        ddlNewsletterUpdates.SelectedValue = "Yes";
                    }
                    else
                    {
                        ddlNewsletterUpdates.SelectedValue = "No";
                    }
                    if (user.DeclarationInfo.Satisfaction > 0)
                    {
                        var item = ddlSatisfaction.Items.FindByValue(user.DeclarationInfo.Satisfaction.ToString());
                        if (item != null)
                        {
                            ddlSatisfaction.SelectedValue = item.Value;
                        }
                    }
                    txtSatisfactionNotes.Text = user.DeclarationInfo.SatisfactionNotes;
                }
            }

            var user2 = repository.GetApplicationUserInfo(CurrentUserId);
            if (user2 != null)
            {
                CurrentUser = user2;
                lblDeclarationTitle.Text = "Declaration for " + user2.FullName + " #" + user2.Id;
            }
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");
                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                if (SessionParameters.CurrentUser != null)
                {
                    return SessionParameters.CurrentUser;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUser = value;
            }
        }

        #endregion
    }
}