﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlRegistrationApplication.ascx.cs" Inherits="COTO_RegOnly.Application.UserControls.ctlRegistrationApplication" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>


<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="border: 0; border-spacing: 0; padding: 0; width: 100%">
                    <tr class="HeaderTitle" style="text-align:right;">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Registration Application Step 2 of 11" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right;">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" style="width: 100%; border-spacing: 2px; padding: 3px;">
                                 <tr class="RowTitle">
                                    <td>
                                        <div>
                                            <asp:Label ID="lblSectionTitle" CssClass="heading" runat="server"
                                            Text="Registration Category, Expected Start Date and Work Eligibility" />
                                        </div>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblRegistrationCategoryTitle" runat="server" Text="Registration Category" Font-Bold="true"  Font-Size="Medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;">
                                        <asp:Label ID="lblApplyingFor" runat="server" Text="I am applying for a:" />&nbsp;
                                        <asp:DropDownList ID="ddlRegistrationCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRegistrationCategorySelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="rfvRegistrationCategory" runat="server" ControlToValidate="ddlRegistrationCategory"
                                            InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide Registration Category: Field is blank."
                                            Display="Dynamic" ForeColor="Red" /><br />
                                        <asp:Label ID="lblRegistrationCategoryDescription" runat="server" ForeColor="DarkRed" Font-Bold="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblExpectedStartDateTitle" runat="server" Text="Expected Start Date" Font-Bold="true"  Font-Size="Medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;">
                                        <asp:Label ID="lblExpectedStartDate" runat="server" Text="What date would you like to be registered by: *" />&nbsp;
                                        <asp:TextBox ID="txtExpectedStartDate" runat="server" />&nbsp;
                                        <ajaxToolkit:CalendarExtender ID="ceExpectedStartDate" runat="server" TargetControlID="txtExpectedStartDate" Format="MM/dd/yyyy" />
                                        <asp:RequiredFieldValidator ID="rfvExpectedStartDate" runat="server" ControlToValidate="txtExpectedStartDate" 
                                        ValidationGroup="PersonalValidation" ErrorMessage="<br />Please select Expected Start Date" Display="Dynamic" ForeColor="Red" />
                                        <asp:Label ID="lblExpectedStartDateErrorMessage" runat="server" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;"> 
                                        <span>Please note, applications for registration take a minimum of 7 business days to process.</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblCitizenshipTitle" runat="server" Text="Work Eligibility" Font-Bold="true"  Font-Size="Medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;">
                                        <asp:Label ID="lblCitizenshipRequirementsMessage" runat="server" Text="You will be required to forward documentation to the College to demonstrate you meet this requirement.
                                            If you do not currently meet this requirement you can still proceed with completing the other sections of the application." />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;">
                                        <asp:Label ID="lblCitizenshipCategoryTitle" runat="server" Text="Please select the category that applies to you:" />&nbsp;
                                        <asp:DropDownList ID="ddlCitizenshipCategory" runat="server" OnSelectedIndexChanged="ddlCitizenshipCategorySelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="rfvCitizenshipCategory" runat="server" ControlToValidate="ddlCitizenshipCategory"
                                            InitialValue="" ValidationGroup="PersonalValidation" ErrorMessage="<br />Please provide Citizenship Category: Field is blank."
                                            Display="Dynamic" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td  style="white-space:normal; text-align:left;">
                                        <asp:Label ID="lblCitizenshipCategoryDescription" runat="server" Font-Bold="true" ForeColor="DarkRed" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right;">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>