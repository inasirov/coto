﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using System.Globalization;
using System.Text;
using System.Security.Cryptography;
using COTO_RegOnly.Classes;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlProfessionalHistory : System.Web.UI.UserControl
    {

        #region Consts

        private string _Key = "Yz7!~3";
        private string PrevStep = WebConfigItems.Application_Step5;
        private string NextStep = WebConfigItems.Application_Step7;
        private const int CurrentStep = 6;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(WebConfigItems.Application_Step0);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                SessionParameters.ApplicationStep = CurrentStep;
                BindLists();
                BindData();
                if (!string.IsNullOrEmpty(WebConfigItems.Application_Step6_RegulatoryForm))
                {
                    var link =  WebConfigItems.Application_Step6_RegulatoryForm;
                    //lbtnRegulatoryFormLink.Attributes.Add("onclick", string.Format("javascript:window.open('{0}', '_blank')",link ));
                }
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(-1);
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            if (ValidationCheck())
            {
                string message = UpdateUserProfessionalHistory();

                if (string.IsNullOrEmpty(message))
                {
                    UpdateSteps(1);
                    Session["CustomAction"] = "renewal";
                    Response.Redirect(NextStep); // + "&ID=" + CurrentUserId + "&FULL_NAME=" + HttpUtility.UrlEncode(fullName) + "&CustomAction=renewal");
                }
                else
                {
                    lblErrorMessage.Text = message;
                }
            }
            else
            {
                if (Messages != null && Messages.Count > 0)
                {
                    string message = string.Empty;
                    foreach (var item in Messages)
                    {
                        message += ((PageMessage)item).Message + "<br />";
                    }
                    message = string.Format("<font color='Red'>{0}</font>", message);

                    Messages.Clear();
                    ShowErrorMessage(message);
                }
            }
        }

        protected void PopCalendar1_SelectionChanged(object sender, EventArgs e)
        {

        }

        protected void ddlRegBody1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlRegBody1.SelectedValue))
            {
                DisableEnableJurisdiction2Controls(true);
                if (ddlRegBody1.SelectedItem.Text.ToUpper() == "OTHER")
                {
                    trRegBodyOther1.Visible = true;
                }
                else
                {
                    trRegBodyOther1.Visible = false;
                    txtRegBodyOther1.Text = string.Empty;
                }
            }
            else
            {
                ClearJurisdict1Fields();
                ClearJurisdict2Fields();
                DisableEnableJurisdiction2Controls(false);
                ClearJurisdict3Fields();
                DisableEnableJurisdiction3Controls(false);
                ClearJurisdict4Fields();
                DisableEnableJurisdiction4Controls(false);
            }
        }

        protected void ddlRegBody2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlRegBody2.SelectedValue))
            {
                DisableEnableJurisdiction3Controls(true);
                if (ddlRegBody2.SelectedItem.Text.ToUpper() == "OTHER")
                {
                    trRegBodyOther2.Visible = true;
                }
                else
                {
                    trRegBodyOther2.Visible = false;
                    txtRegBodyOther2.Text = string.Empty;
                }
            }
            else
            {
                ClearJurisdict2Fields();
                ClearJurisdict3Fields();
                DisableEnableJurisdiction3Controls(false);
                ClearJurisdict4Fields();
                DisableEnableJurisdiction4Controls(false);
            }
        }

        protected void ddlRegBody3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRegBody3.SelectedItem.Text.ToUpper() == "OTHER")
            {
                trRegBodyOther3.Visible = true;
            }
            else
            {
                trRegBodyOther3.Visible = false;
                txtRegBodyOther3.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(ddlRegBody3.SelectedValue))
            {
                DisableEnableJurisdiction4Controls(true);
                
            }
            else
            {
                ClearJurisdict3Fields();
                ClearJurisdict4Fields();
                DisableEnableJurisdiction4Controls(false);
            }
        }

        protected void ddlRegBody4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRegBody4.SelectedItem.Text.ToUpper() == "OTHER")
            {
                trRegBodyOther4.Visible = true;
            }
            else
            {
                trRegBodyOther4.Visible = false;
                txtRegBodyOther4.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(ddlRegBody4.SelectedValue))
            {
                //   
            }
            else
            {
                ClearJurisdict4Fields();
            }
            
        }

        protected void cbNoExpiration1CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoExpiration1.Checked) txtExpiryDate1.Text = string.Empty;
        }

        protected void cbNoExpiration2CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoExpiration2.Checked) txtExpiryDate2.Text = string.Empty;
        }

        protected void cbNoExpiration3CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoExpiration3.Checked) txtExpiryDate3.Text = string.Empty;
        }

        protected void cbNoExpiration4CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoExpiration4.Checked) txtExpiryDate4.Text = string.Empty;
        }

        protected void cbNoExpirationOP1CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoExpirationOP1.Checked) txtExpireDateOP1.Text = string.Empty;
        }

        protected void cbNoExpirationOP2CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoExpirationOP2.Checked) txtExpireDateOP2.Text = string.Empty;
        }

        protected void ddlNameProfessionOP1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlNameProfessionOP1.SelectedValue))
            {
                DisableEnableProfession2Controls(true);
                if (ddlNameProfessionOP1.SelectedItem.Text.ToUpper() == "OTHER")
                {
                    trNameProfessionOtherOP1.Visible = true;
                    ddlRegBodyOP1.Items.Clear();
                    ddlRegBodyOP1.DataBind();
                    ddlRegBodyOP1.Items.Add("Other");
                    trRegBodyOtherOP1.Visible = true;
                }
                else
                {
                    trNameProfessionOtherOP1.Visible = false;
                    txtNameProfessionOtherOP1.Text = string.Empty;

                    trRegBodyOtherOP1.Visible = false;
                    txtRegBodyOtherOP1.Text = string.Empty;

                    ddlRegBodyOP1.Items.Clear();
                    //string newLookup = "REG_OTHER_PROF_" + ddlNameProfessionOP1.SelectedValue;
                    //var repository = new Repository();
                    //var list = repository.GetGeneralList(newLookup);

                    var repository = new Repository();
                    var list = repository.GetGeneralList("REG_OTHER_PROF_BODIES"); 
                    var subList = ddlNameProfessionOP1.SelectedValue + "_";
                    list = list.Where(I => I.Code.StartsWith(subList)).ToList();
                    ddlRegBodyOP1.DataSource = list;
                    ddlRegBodyOP1.DataValueField = "Code";
                    ddlRegBodyOP1.DataTextField = "Description";
                    ddlRegBodyOP1.DataBind();
                    ddlRegBodyOP1.Items.Insert(0, string.Empty);
                    //ddlRegBodyOP1.Items.Add(new )
                }
            }
            else
            {
                trNameProfessionOtherOP1.Visible = false;
                trRegBodyOtherOP1.Visible = false;

                ClearProf1Fields();
                ClearProf2Fields();
                DisableEnableProfession2Controls(false);
            }
        }

        protected void ddlNameProfessionOP2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlNameProfessionOP2.SelectedValue))
            {
                if (ddlNameProfessionOP2.SelectedItem.Text.ToUpper() == "OTHER")
                {
                    trNameProfessionOtherOP2.Visible = true;
                    ddlRegBodyOP2.Items.Clear();
                    ddlRegBodyOP2.DataBind();
                    ddlRegBodyOP2.Items.Add("Other");
                    trRegBodyOtherOP2.Visible = true;
                }
                else
                {
                    trNameProfessionOtherOP2.Visible = false;
                    txtNameProfessionOtherOP2.Text = string.Empty;

                    trRegBodyOtherOP2.Visible = false;
                    txtRegBodyOtherOP2.Text = string.Empty;

                    ddlRegBodyOP2.Items.Clear();
                    //string newLookup = "REG_OTHER_PROF_" + ddlNameProfessionOP2.SelectedValue;
                    //var repository = new Repository();
                    //var list = repository.GetGeneralList(newLookup);
                    var repository = new Repository();
                    var list = repository.GetGeneralList("REG_OTHER_PROF_BODIES");
                    var subList = ddlNameProfessionOP2.SelectedValue + "_";
                    list = list.Where(I => I.Code.StartsWith(subList)).ToList();
                    ddlRegBodyOP2.DataSource = list;
                    ddlRegBodyOP2.DataValueField = "Code";
                    ddlRegBodyOP2.DataTextField = "Description";
                    ddlRegBodyOP2.DataBind();
                    ddlRegBodyOP2.Items.Insert(0, string.Empty);
                }
            }
            else
            {
                trNameProfessionOtherOP2.Visible = false;
                trRegBodyOtherOP2.Visible = false;
                ClearProf2Fields();
            }
        }

        protected void ddlRegBodyOP1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRegBodyOP1.SelectedItem.Text.ToUpper() == "OTHER")
            {
                trRegBodyOtherOP1.Visible = true;
            }
            else
            {
                txtRegBodyOtherOP1.Text = string.Empty;
                trRegBodyOtherOP1.Visible = false;
            }
        }

        protected void ddlRegBodyOP2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRegBodyOP2.SelectedItem.Text.ToUpper() == "OTHER")
            {
                trRegBodyOtherOP2.Visible = true;
            }
            else
            {
                txtRegBodyOtherOP2.Text = string.Empty;
                trRegBodyOtherOP2.Visible = false;
            }
        }

        #endregion

        #region Methods

        protected void ClearProf1Fields()
        {
            //ddlNameProfessionOP1.SelectedIndex = 0;
            txtNameProfessionOtherOP1.Text = string.Empty;
            //ddlRegBodyOP1.SelectedIndex = 0;
            ddlRegBodyOP1.Items.Clear();
            ddlRegBodyOP1.Items.Insert(0, string.Empty);
            txtRegBodyOtherOP1.Text = string.Empty;
            ddlProvinceOP1.SelectedIndex = 0;
            ddlCountryOP1.SelectedIndex = 0;
            txtLicenseOP1.Text = string.Empty;
            txtRegStatusOP1.Text = string.Empty;
            ddlInitMonthOP1.SelectedIndex = 0;
            ddlInitYearOP1.SelectedIndex = 0;
            txtExpireDateOP1.Text = string.Empty;
        }

        protected void ClearProf2Fields()
        {
            ddlNameProfessionOP2.SelectedIndex = 0;
            txtNameProfessionOtherOP2.Text = string.Empty;
            ddlRegBodyOP2.Items.Clear();
            ddlRegBodyOP2.Items.Insert(0, string.Empty);
            //ddlRegBodyOP2.SelectedIndex = 0;
            txtRegBodyOtherOP2.Text = string.Empty;
            ddlProvinceOP2.SelectedIndex = 0;
            ddlCountryOP2.SelectedIndex = 0;
            txtLicenseOP2.Text = string.Empty;
            txtRegStatusOP2.Text = string.Empty;
            ddlInitMonthOP2.SelectedIndex = 0;
            ddlInitYearOP2.SelectedIndex = 0;
            txtExpireDateOP2.Text = string.Empty;
        }

        protected void ClearJurisdict1Fields()
        {
            //ddlRegBody1.SelectedIndex = 0;
            txtRegBodyOther1.Text = string.Empty;
            ddlProvince1.SelectedIndex = 0;
            ddlCountry1.SelectedIndex = 0;
            txtLicense1.Text = string.Empty;
            txtRegStatus1.Text = string.Empty;
            ddlInitMonth1.SelectedIndex = 0;
            ddlInitYear1.SelectedIndex = 0;
            txtExpiryDate1.Text = string.Empty;
        }

        protected void ClearJurisdict2Fields()
        {
            ddlRegBody2.SelectedIndex = 0;
            txtRegBodyOther2.Text = string.Empty;
            ddlProvince2.SelectedIndex = 0;
            ddlCountry2.SelectedIndex = 0;
            txtLicense2.Text = string.Empty;
            txtRegStatus2.Text = string.Empty;
            ddlInitMonth2.SelectedIndex = 0;
            ddlInitYear2.SelectedIndex = 0;
            txtExpiryDate2.Text = string.Empty;
        }

        protected void ClearJurisdict3Fields()
        {
            ddlRegBody3.SelectedIndex = 0;
            txtRegBodyOther3.Text = string.Empty;
            ddlProvince3.SelectedIndex = 0;
            ddlCountry3.SelectedIndex = 0;
            txtLicense3.Text = string.Empty;
            txtRegStatus3.Text = string.Empty;
            ddlInitMonth3.SelectedIndex = 0;
            ddlInitYear3.SelectedIndex = 0;
            txtExpiryDate3.Text = string.Empty;
        }

        protected void ClearJurisdict4Fields()
        {
            ddlRegBody4.SelectedIndex = 0;
            txtRegBodyOther4.Text = string.Empty;
            ddlProvince4.SelectedIndex = 0;
            ddlCountry4.SelectedIndex = 0;
            txtLicense4.Text = string.Empty;
            txtRegStatus4.Text = string.Empty;
            ddlInitMonth4.SelectedIndex = 0;
            ddlInitYear4.SelectedIndex = 0;
            txtExpiryDate4.Text = string.Empty;
        }

        protected void DisableEnableJurisdiction1Controls(bool key)
        {
            //ddlRegBody1.Enabled = key;
            txtRegBodyOther1.ReadOnly = !key;
            ddlProvince1.Enabled = key;
            ddlCountry1.Enabled = key;
            txtLicense1.ReadOnly = !key;
            txtRegStatus1.ReadOnly = !key;
            txtExpiryDate1.ReadOnly = !key;
            ddlInitMonth1.Enabled = key;
            ddlInitYear1.Enabled = key;
            txtExpiryDate1.ReadOnly = !key;
            PopCalendar1.Enabled = key;
            cbNoExpiration1.Enabled = key;
        }

        protected void DisableEnableJurisdiction2Controls(bool key)
        {
            ddlRegBody2.Enabled = key;
            txtRegBodyOther2.ReadOnly = !key;
            ddlProvince2.Enabled = key;
            ddlCountry2.Enabled = key;
            txtLicense2.ReadOnly = !key;
            txtRegStatus2.ReadOnly = !key;
            txtExpiryDate2.ReadOnly = !key;
            ddlInitMonth2.Enabled = key;
            ddlInitYear2.Enabled = key;
            PopCalendar2.Enabled = key;
            cbNoExpiration2.Enabled = key;
        }

        protected void DisableEnableJurisdiction3Controls(bool key)
        {
            ddlRegBody3.Enabled = key;
            txtRegBodyOther3.ReadOnly = !key;
            ddlProvince3.Enabled = key;
            ddlCountry3.Enabled = key;
            txtLicense3.ReadOnly = !key;
            txtRegStatus3.ReadOnly = !key;
            txtExpiryDate3.ReadOnly = !key;
            ddlInitMonth3.Enabled = key;
            ddlInitYear3.Enabled = key;
            txtExpiryDate3.ReadOnly = !key;
            PopCalendar3.Enabled = key;
            cbNoExpiration3.Enabled = key;
        }

        protected void DisableEnableJurisdiction4Controls(bool key)
        {
            ddlRegBody4.Enabled = key;
            txtRegBodyOther4.ReadOnly = !key;
            ddlProvince4.Enabled = key;
            ddlCountry4.Enabled = key;
            txtLicense4.ReadOnly = !key;
            txtRegStatus4.ReadOnly = !key;
            txtExpiryDate4.ReadOnly = !key;
            ddlInitMonth4.Enabled = key;
            ddlInitYear4.Enabled = key;
            txtExpiryDate4.ReadOnly = !key;
            PopCalendar4.Enabled = key;
            cbNoExpiration4.Enabled = key;
        }

        protected void DisableEnableProfession1Controls(bool key)
        {
            //ddlNameProfessionOP1.Enabled = key;
            txtNameProfessionOtherOP1.ReadOnly = !key;
            ddlRegBodyOP1.Enabled = key;
            txtRegBodyOtherOP1.ReadOnly = !key;
            ddlProvinceOP1.Enabled = key;
            ddlCountryOP1.Enabled = key;
            txtLicenseOP1.ReadOnly = !key;
            txtRegStatusOP1.ReadOnly = !key;
            ddlInitMonthOP1.Enabled = key;
            ddlInitYearOP1.Enabled = key;
            txtExpireDateOP1.ReadOnly = !key;
            PopCalendar5.Enabled = key;
            cbNoExpirationOP1.Enabled = key;
        }

        protected void DisableEnableProfession2Controls(bool key)
        {
            ddlNameProfessionOP2.Enabled = key;
            txtNameProfessionOtherOP2.ReadOnly = !key;
            ddlRegBodyOP2.Enabled = key;
            txtRegBodyOtherOP2.ReadOnly = !key;
            ddlProvinceOP2.Enabled = key;
            ddlCountryOP2.Enabled = key;
            txtLicenseOP2.ReadOnly = !key;
            txtRegStatusOP2.ReadOnly = !key;
            ddlInitMonthOP2.Enabled = key;
            ddlInitYearOP2.Enabled = key;
            txtExpireDateOP2.ReadOnly = !key;
            PopCalendar6.Enabled = key;
            cbNoExpirationOP2.Enabled = key;
        }

        protected bool ValidationCheck()
        {
            Page.Validate("GeneralValidation");
            bool valid = true;

            bool validateJur1 = (ddlRegBody1.SelectedIndex > 0);
            bool validateJur2 = (ddlRegBody2.SelectedIndex > 0);
            bool validateJur3 = (ddlRegBody3.SelectedIndex > 0);
            bool validateJur4 = (ddlRegBody4.SelectedIndex > 0);

            if (validateJur1)
            {
                if (cbNoExpiration1.Checked)
                {
                    rfvExpiryDate1.Enabled = false;
                }
                else
                {
                    rfvExpiryDate1.Enabled = true;
                }

                if (!string.IsNullOrEmpty(txtExpiryDate1.Text))
                {
                    try
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime expiry1 = DateTime.ParseExact(txtExpiryDate1.Text, "MM/dd/yyyy", provider);
                    }
                    catch
                    {
                        AddMessage("Jurisdiction 1: Expiry Date format error", PageMessageType.UserError, string.Empty);
                        valid = false;
                        return valid;
                    }
                }

                if (ddlRegBody1.SelectedValue.ToUpper() == "ZOTHER" && string.IsNullOrEmpty(txtRegBodyOther1.Text))
                {
                    AddMessage("Please provide all information of your Jurisdiction 1: Regulatory Body Other is empty", PageMessageType.UserError, string.Empty);
                    valid = false;
                    return valid;
                }
                Page.Validate("Jurisdiction1Validation");
            }

            if (validateJur2)
            {
                if (cbNoExpiration2.Checked)
                {
                    rfvExpiryDate2.Enabled = false;
                }
                else
                {
                    rfvExpiryDate2.Enabled = true;
                }

                if (!string.IsNullOrEmpty(txtExpiryDate2.Text))
                {
                    try
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime expiry2 = DateTime.ParseExact(txtExpiryDate2.Text, "MM/dd/yyyy", provider);
                    }
                    catch
                    {
                        AddMessage("Jurisdiction 2: Expiry Date format error", PageMessageType.UserError, string.Empty);
                        valid = false;
                        return valid;
                    }
                }
                if (ddlRegBody2.SelectedValue.ToUpper() == "ZOTHER" && string.IsNullOrEmpty(txtRegBodyOther2.Text))
                {
                    AddMessage("Please provide all information of your Jurisdiction 2: Regulatory Body Other is empty", PageMessageType.UserError, string.Empty);
                    valid = false;
                    return valid;
                }
                Page.Validate("Jurisdiction2Validation");
            }

            if (validateJur3)
            {
                if (cbNoExpiration3.Checked)
                {
                    rfvExpiryDate3.Enabled = false;
                }
                else
                {
                    rfvExpiryDate3.Enabled = true;
                }

                if (!string.IsNullOrEmpty(txtExpiryDate3.Text))
                {
                    try
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime expiry3 = DateTime.ParseExact(txtExpiryDate3.Text, "MM/dd/yyyy", provider);
                    }
                    catch
                    {
                        AddMessage("Jurisdiction 3: Expiry Date format error", PageMessageType.UserError, string.Empty);
                        valid = false;
                        return valid;
                    }
                }
                if (ddlRegBody3.SelectedValue.ToUpper() == "ZOTHER" && string.IsNullOrEmpty(txtRegBodyOther3.Text))
                {
                    AddMessage("Please provide all information of your Jurisdiction 3: Regulatory Body Other is empty", PageMessageType.UserError, string.Empty);
                    valid = false;
                    return valid;
                }
                Page.Validate("Jurisdiction3Validation");
            }

            if (validateJur4)
            {
                if (cbNoExpiration4.Checked)
                {
                    rfvExpiryDate4.Enabled = false;
                }
                else
                {
                    rfvExpiryDate4.Enabled = true;
                }

                if (!string.IsNullOrEmpty(txtExpiryDate4.Text))
                {
                    try
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime expiry4 = DateTime.ParseExact(txtExpiryDate4.Text, "MM/dd/yyyy", provider);
                    }
                    catch
                    {
                        AddMessage("Jurisdiction 4: Expiry Date format error", PageMessageType.UserError, string.Empty);
                        valid = false;
                        return valid;
                    }
                }
                if (ddlRegBody4.SelectedValue.ToUpper() == "ZOTHER" && string.IsNullOrEmpty(txtRegBodyOther4.Text))
                {
                    AddMessage("Please provide all information of your Jurisdiction 4: Regulatory Body Other is empty", PageMessageType.UserError, string.Empty);
                    valid = false;
                    return valid;
                }
                Page.Validate("Jurisdiction4Validation");
            }

            bool validateProf1 = ddlNameProfessionOP1.SelectedIndex > 0;
            bool validateProf2 = ddlNameProfessionOP2.SelectedIndex > 0;
            if (validateProf1)
            {
                if (cbNoExpirationOP1.Checked)
                {
                    rfvExpireDateOP1.Enabled = false;
                }
                else
                {
                    rfvExpireDateOP1.Enabled = true;
                }
                if (!string.IsNullOrEmpty(txtExpireDateOP1.Text))
                {
                    try
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime expiry1 = DateTime.ParseExact(txtExpireDateOP1.Text, "MM/dd/yyyy", provider);
                    }
                    catch
                    {
                        AddMessage("Profession 1: Expiry Date format error", PageMessageType.UserError, string.Empty);
                        valid = false;
                        return valid;
                    }
                }
                Page.Validate("Profession1Validation");
            }

            if (validateProf2)
            {
                if (cbNoExpirationOP2.Checked)
                {
                    rfvExpireDateOP2.Enabled = false;
                }
                else
                {
                    rfvExpireDateOP2.Enabled = true;
                }

                if (!string.IsNullOrEmpty(txtExpireDateOP2.Text))
                {
                    try
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime expiry2 = DateTime.ParseExact(txtExpireDateOP2.Text, "MM/dd/yyyy", provider);
                    }
                    catch
                    {
                        AddMessage("Profession 2: Expiry Date format error", PageMessageType.UserError, string.Empty);
                        valid = false;
                        return valid;
                    }
                }
                Page.Validate("Profession2Validation");
            }

            if (!Page.IsValid)
            {
                lblErrorMessage.Text = string.Empty;
                update.Update();
                modalPopupEx.Show();
                return false;
            }
            return valid;
        }

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.RenewalStep != 0)
            {
                SessionParameters.RenewalStep += diff;
            }
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        protected void ShowErrorMessage(string Message)
        {
            lblErrorMessage.Text = Message;
            update.Update();
            modalPopupEx.Show();
        }

        public void AddMessage(string message, PageMessageType messageType, string returnURL)
        {
            SessionParameters.ReturnUrl = returnURL;
            this.Messages.Add(new PageMessage(message, messageType));
        }

        protected void BindLists()
        {

            //var list3 = new List<GenClass>();
            //list3.Add(new GenClass { Code = "", Description = "" });
            //list3.Add(new GenClass { Code = "No", Description = "No" });
            //list3.Add(new GenClass { Code = "Yes", Description = "Yes" });

            //ddlRegisteredPracticeOtherProvince.DataSource = list3;
            //ddlRegisteredPracticeOtherProvince.DataValueField = "Code";
            //ddlRegisteredPracticeOtherProvince.DataTextField = "Description";
            //ddlRegisteredPracticeOtherProvince.DataBind();

            //ddlOtherProfession.DataSource = list3;
            //ddlOtherProfession.DataValueField = "Code";
            //ddlOtherProfession.DataTextField = "Description";
            //ddlOtherProfession.DataBind();

            var repository = new Repository();

            var list0 = repository.GetGeneralList("REG_BODIES"); // get data for status field
            ddlRegBody1.DataSource = list0;
            ddlRegBody1.DataValueField = "Code";
            ddlRegBody1.DataTextField = "Description";
            ddlRegBody1.DataBind();
            ddlRegBody1.Items.Insert(0, string.Empty);

            ddlRegBody2.DataSource = list0;
            ddlRegBody2.DataValueField = "Code";
            ddlRegBody2.DataTextField = "Description";
            ddlRegBody2.DataBind();
            ddlRegBody2.Items.Insert(0, string.Empty);

            ddlRegBody3.DataSource = list0;
            ddlRegBody3.DataValueField = "Code";
            ddlRegBody3.DataTextField = "Description";
            ddlRegBody3.DataBind();
            ddlRegBody3.Items.Insert(0, string.Empty);

            ddlRegBody4.DataSource = list0;
            ddlRegBody4.DataValueField = "Code";
            ddlRegBody4.DataTextField = "Description";
            ddlRegBody4.DataBind();
            ddlRegBody4.Items.Insert(0, string.Empty);

            var list1 = repository.GetGeneralList("COUNTRY");
            list1 = list1.OrderBy(I => I.Description).ToList();
            ddlCountry1.DataSource = list1;
            ddlCountry1.DataValueField = "Code";
            ddlCountry1.DataTextField = "Description";
            ddlCountry1.DataBind();
            ddlCountry1.Items.Insert(0, string.Empty);

            ddlCountry2.DataSource = list1;
            ddlCountry2.DataValueField = "Code";
            ddlCountry2.DataTextField = "Description";
            ddlCountry2.DataBind();
            ddlCountry2.Items.Insert(0, string.Empty);

            ddlCountry3.DataSource = list1;
            ddlCountry3.DataValueField = "Code";
            ddlCountry3.DataTextField = "Description";
            ddlCountry3.DataBind();
            ddlCountry3.Items.Insert(0, string.Empty);

            ddlCountry4.DataSource = list1;
            ddlCountry4.DataValueField = "Code";
            ddlCountry4.DataTextField = "Description";
            ddlCountry4.DataBind();
            ddlCountry4.Items.Insert(0, string.Empty);

            ddlCountryOP1.DataSource = list1;
            ddlCountryOP1.DataValueField = "Code";
            ddlCountryOP1.DataTextField = "Description";
            ddlCountryOP1.DataBind();
            ddlCountryOP1.Items.Insert(0, string.Empty);

            ddlCountryOP2.DataSource = list1;
            ddlCountryOP2.DataValueField = "Code";
            ddlCountryOP2.DataTextField = "Description";
            ddlCountryOP2.DataBind();
            ddlCountryOP2.Items.Insert(0, string.Empty);

            var list2 = repository.GetGeneralList("PROVINCE_STATE");

            ddlProvince1.DataSource = list2;
            ddlProvince1.DataValueField = "Code";
            ddlProvince1.DataTextField = "Description";
            ddlProvince1.DataBind();
            ddlProvince1.Items.Insert(0, string.Empty);

            ddlProvince2.DataSource = list2;
            ddlProvince2.DataValueField = "Code";
            ddlProvince2.DataTextField = "Description";
            ddlProvince2.DataBind();
            ddlProvince2.Items.Insert(0, string.Empty);

            ddlProvince3.DataSource = list2;
            ddlProvince3.DataValueField = "Code";
            ddlProvince3.DataTextField = "Description";
            ddlProvince3.DataBind();
            ddlProvince3.Items.Insert(0, string.Empty);

            ddlProvince4.DataSource = list2;
            ddlProvince4.DataValueField = "Code";
            ddlProvince4.DataTextField = "Description";
            ddlProvince4.DataBind();
            ddlProvince4.Items.Insert(0, string.Empty);

            ddlProvinceOP1.DataSource = list2;
            ddlProvinceOP1.DataValueField = "Code";
            ddlProvinceOP1.DataTextField = "Description";
            ddlProvinceOP1.DataBind();
            ddlProvinceOP1.Items.Insert(0, string.Empty);

            ddlProvinceOP2.DataSource = list2;
            ddlProvinceOP2.DataValueField = "Code";
            ddlProvinceOP2.DataTextField = "Description";
            ddlProvinceOP2.DataBind();
            ddlProvinceOP2.Items.Insert(0, string.Empty);

            var list3 = repository.GetGeneralList("REG_OTHER_MONTH");

            ddlInitMonth1.DataSource = list3;
            ddlInitMonth1.DataValueField = "Code";
            ddlInitMonth1.DataTextField = "Description";
            ddlInitMonth1.DataBind();
            ddlInitMonth1.Items.Insert(0, string.Empty);

            ddlInitMonth2.DataSource = list3;
            ddlInitMonth2.DataValueField = "Code";
            ddlInitMonth2.DataTextField = "Description";
            ddlInitMonth2.DataBind();
            ddlInitMonth2.Items.Insert(0, string.Empty);

            ddlInitMonth3.DataSource = list3;
            ddlInitMonth3.DataValueField = "Code";
            ddlInitMonth3.DataTextField = "Description";
            ddlInitMonth3.DataBind();
            ddlInitMonth3.Items.Insert(0, string.Empty);

            ddlInitMonth4.DataSource = list3;
            ddlInitMonth4.DataValueField = "Code";
            ddlInitMonth4.DataTextField = "Description";
            ddlInitMonth4.DataBind();
            ddlInitMonth4.Items.Insert(0, string.Empty);

            ddlInitMonthOP1.DataSource = list3;
            ddlInitMonthOP1.DataValueField = "Code";
            ddlInitMonthOP1.DataTextField = "Description";
            ddlInitMonthOP1.DataBind();
            ddlInitMonthOP1.Items.Insert(0, string.Empty);

            ddlInitMonthOP2.DataSource = list3;
            ddlInitMonthOP2.DataValueField = "Code";
            ddlInitMonthOP2.DataTextField = "Description";
            ddlInitMonthOP2.DataBind();
            ddlInitMonthOP2.Items.Insert(0, string.Empty);

            var list4 = repository.GetGeneralList("REG_OTHER_YEAR").OrderByDescending(I => I.Code).ToList();

            ddlInitYear1.DataSource = list4;
            ddlInitYear1.DataValueField = "Code";
            ddlInitYear1.DataTextField = "Description";
            ddlInitYear1.DataBind();
            ddlInitYear1.Items.Insert(0, string.Empty);

            ddlInitYear2.DataSource = list4;
            ddlInitYear2.DataValueField = "Code";
            ddlInitYear2.DataTextField = "Description";
            ddlInitYear2.DataBind();
            ddlInitYear2.Items.Insert(0, string.Empty);

            ddlInitYear3.DataSource = list4;
            ddlInitYear3.DataValueField = "Code";
            ddlInitYear3.DataTextField = "Description";
            ddlInitYear3.DataBind();
            ddlInitYear3.Items.Insert(0, string.Empty);

            ddlInitYear4.DataSource = list4;
            ddlInitYear4.DataValueField = "Code";
            ddlInitYear4.DataTextField = "Description";
            ddlInitYear4.DataBind();
            ddlInitYear4.Items.Insert(0, string.Empty);

            ddlInitYearOP1.DataSource = list4;
            ddlInitYearOP1.DataValueField = "Code";
            ddlInitYearOP1.DataTextField = "Description";
            ddlInitYearOP1.DataBind();
            ddlInitYearOP1.Items.Insert(0, string.Empty);

            ddlInitYearOP2.DataSource = list4;
            ddlInitYearOP2.DataValueField = "Code";
            ddlInitYearOP2.DataTextField = "Description";
            ddlInitYearOP2.DataBind();
            ddlInitYearOP2.Items.Insert(0, string.Empty);

            var list5 = repository.GetGeneralList("REG_OTHER_PROF");

            var other_item = list5.Find(i => i.Code == "OTHER");
            if (other_item!=null)
            {
                list5.Remove(other_item);
            }
            ddlNameProfessionOP1.DataSource = list5;
            ddlNameProfessionOP1.DataValueField = "Code";
            ddlNameProfessionOP1.DataTextField = "Description";
            ddlNameProfessionOP1.DataBind();
            ddlNameProfessionOP1.Items.Insert(0, string.Empty);

            ddlNameProfessionOP2.DataSource = list5;
            ddlNameProfessionOP2.DataValueField = "Code";
            ddlNameProfessionOP2.DataTextField = "Description";
            ddlNameProfessionOP2.DataBind();
            ddlNameProfessionOP2.Items.Insert(0, string.Empty);

        }

        protected void BindData()
        {
            var repository = new Repository();
            hfPracticeSeqn1.Value = "0";
            hfPracticeSeqn2.Value = "0";
            hfPracticeSeqn3.Value = "0";
            hfPracticeSeqn4.Value = "0";

            hfPracticeSeqnOP1.Value = "0";
            hfPracticeSeqnOP2.Value = "0";

            //DisableEnableJurisdiction1Controls(false);
            DisableEnableJurisdiction2Controls(false);
            DisableEnableJurisdiction3Controls(false);
            DisableEnableJurisdiction4Controls(false);

            //DisableEnableProfession1Controls(false);
            DisableEnableProfession2Controls(false);

            var history = repository.GetAppProfessionalHistoryInfoNew(CurrentUserId);
            if (history!= null)
            {
                if (history.PracticeDetails!= null)
                {
                    var first = history.PracticeDetails.Find(I => I.OrderNum == 1);
                    if (first != null)
                    {
                        hfPracticeSeqn1.Value = first.SEQN.ToString();

                        var listItem = ddlRegBody1.Items.FindByValue(first.RegulatoryBody);
                        if (listItem != null) ddlRegBody1.SelectedValue = listItem.Value;

                        if (ddlRegBody1.SelectedValue == "ZOTHER")
                        {
                            trRegBodyOther1.Visible = true;
                            txtRegBodyOther1.Text = first.RegulatoryBodyOther;
                            //txtRegBodyOther1.ReadOnly = false;
                        }
                        else
                        {
                            trRegBodyOther1.Visible = false;
                            txtRegBodyOther1.Text = string.Empty;
                            //txtRegBodyOther1.ReadOnly = true;
                        }

                        ddlProvince1.SelectedValue = first.Province;
                        ddlCountry1.SelectedValue = first.Country;
                        txtLicense1.Text = first.RegNumber;

                        txtRegStatus1.Text = first.RegStatus;

                        ddlInitMonth1.SelectedValue = first.InitMonth;
                        ddlInitYear1.SelectedValue = first.InitYear;

                        if (first.ExpiryDate != null)
                        {
                            txtExpiryDate1.Text = ((DateTime)first.ExpiryDate).ToString("MM/dd/yyyy");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ddlRegBody1.SelectedValue)) cbNoExpiration1.Checked = true;
                        }

                        if (!string.IsNullOrEmpty(first.RegulatoryBody))
                        {
                            DisableEnableJurisdiction2Controls(true);
                        }
                    }
                    

                    var second = history.PracticeDetails.Find(I => I.OrderNum == 2);
                    if (second != null)
                    {
                        hfPracticeSeqn2.Value = second.SEQN.ToString();
                        var listItem = ddlRegBody2.Items.FindByValue(second.RegulatoryBody);
                        if (listItem != null) ddlRegBody2.SelectedValue = listItem.Value;

                        if (ddlRegBody2.SelectedValue == "ZOTHER")
                        {
                            trRegBodyOther2.Visible = true;
                            txtRegBodyOther2.Text = second.RegulatoryBodyOther;
                            //txtRegBodyOther1.ReadOnly = false;
                        }
                        else
                        {
                            trRegBodyOther2.Visible = false;
                            txtRegBodyOther2.Text = string.Empty;
                            //txtRegBodyOther1.ReadOnly = true;
                        }

                        ddlProvince2.SelectedValue = second.Province;
                        ddlCountry2.SelectedValue = second.Country;
                        txtLicense2.Text = second.RegNumber;

                        txtRegStatus2.Text = second.RegStatus;

                        ddlInitMonth2.SelectedValue = second.InitMonth;
                        ddlInitYear2.SelectedValue = second.InitYear;

                        if (second.ExpiryDate != null)
                        {
                            txtExpiryDate2.Text = ((DateTime)second.ExpiryDate).ToString("MM/dd/yyyy");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ddlRegBody2.SelectedValue)) cbNoExpiration2.Checked = true;
                        }

                        if (!string.IsNullOrEmpty(second.RegulatoryBody))
                        {
                            DisableEnableJurisdiction3Controls(true);
                        }
                    }
                    else
                    {
                        DisableEnableJurisdiction3Controls(false);
                        DisableEnableJurisdiction4Controls(false);
                    }

                    var third = history.PracticeDetails.Find(I => I.OrderNum == 3);
                    if (third != null)
                    {
                        hfPracticeSeqn3.Value = third.SEQN.ToString();
                        var listItem = ddlRegBody3.Items.FindByValue(third.RegulatoryBody);
                        if (listItem != null) ddlRegBody3.SelectedValue = listItem.Value;

                        if (ddlRegBody3.SelectedValue == "ZOTHER")
                        {
                            trRegBodyOther3.Visible = true;
                            txtRegBodyOther3.Text = third.RegulatoryBodyOther;
                            //txtRegBodyOther1.ReadOnly = false;
                        }
                        else
                        {
                            trRegBodyOther3.Visible = false;
                            txtRegBodyOther3.Text = string.Empty;
                            //txtRegBodyOther1.ReadOnly = true;
                        }

                        ddlProvince3.SelectedValue = third.Province;
                        ddlCountry3.SelectedValue = third.Country;
                        txtLicense3.Text = third.RegNumber;

                        txtRegStatus3.Text = third.RegStatus;

                        ddlInitMonth3.SelectedValue = third.InitMonth;
                        ddlInitYear3.SelectedValue = third.InitYear;

                        if (third.ExpiryDate != null)
                        {
                            txtExpiryDate3.Text = ((DateTime)third.ExpiryDate).ToString("MM/dd/yyyy");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ddlRegBody3.SelectedValue)) cbNoExpiration3.Checked = true;
                        }

                        if (!string.IsNullOrEmpty(third.RegulatoryBody))
                        {
                            DisableEnableJurisdiction4Controls(true);
                        }
                    }
                    else
                    {
                        DisableEnableJurisdiction4Controls(false);
                    }

                    var forth = history.PracticeDetails.Find(I => I.OrderNum == 4);
                    if (forth != null)
                    {
                        hfPracticeSeqn4.Value = forth.SEQN.ToString();
                        var listItem = ddlRegBody4.Items.FindByValue(forth.RegulatoryBody);
                        if (listItem != null) ddlRegBody4.SelectedValue = listItem.Value;

                        if (ddlRegBody4.SelectedValue == "ZOTHER")
                        {
                            trRegBodyOther4.Visible = true;
                            txtRegBodyOther4.Text = forth.RegulatoryBodyOther;
                            //txtRegBodyOther1.ReadOnly = false;
                        }
                        else
                        {
                            trRegBodyOther4.Visible = false;
                            txtRegBodyOther4.Text = string.Empty;
                            //txtRegBodyOther1.ReadOnly = true;
                        }

                        ddlProvince4.SelectedValue = forth.Province;
                        ddlCountry4.SelectedValue = forth.Country;
                        txtLicense4.Text = forth.RegNumber;

                        txtRegStatus4.Text = forth.RegStatus;

                        ddlInitMonth4.SelectedValue = forth.InitMonth;
                        ddlInitYear4.SelectedValue = forth.InitYear;

                        if (forth.ExpiryDate != null)
                        {
                            txtExpiryDate4.Text = ((DateTime)forth.ExpiryDate).ToString("MM/dd/yyyy");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ddlRegBody4.SelectedValue)) cbNoExpiration4.Checked = true;
                        }

                        //if (string.IsNullOrEmpty(forth.RegulatoryBody))
                        //{
                        //    DisableEnableJurisdiction4Controls(false);
                        //}
                    }
                }
                
                if (history.PracticeOtherDetails!= null)
                {
                    var firstOP1 = history.PracticeOtherDetails.Find(I => I.OrderNum == 1);
                    if (firstOP1 != null)
                    {
                        hfPracticeSeqnOP1.Value = firstOP1.SEQN.ToString();
                        var listItem = ddlNameProfessionOP1.Items.FindByValue(firstOP1.OtherProfessionType);
                        if (listItem != null) ddlNameProfessionOP1.SelectedValue = listItem.Value;

                        if (ddlNameProfessionOP1.SelectedItem.Text.ToUpper() == "OTHER")
                        {
                            trNameProfessionOtherOP1.Visible = true;
                            txtNameProfessionOtherOP1.Text = firstOP1.OtherProfessionTypeOther;
                            ddlRegBodyOP1.Items.Clear();
                            ddlRegBodyOP1.DataBind();
                            ddlRegBodyOP1.Items.Add("Other");
                            trRegBodyOtherOP1.Visible = true;
                            txtRegBodyOtherOP1.Text = firstOP1.RegulatoryBodyOther;
                        }
                        else
                        {
                            trNameProfessionOtherOP1.Visible = false;
                            txtNameProfessionOtherOP1.Text = string.Empty;

                            trRegBodyOtherOP1.Visible = false;
                            txtRegBodyOtherOP1.Text = string.Empty;

                            ddlRegBodyOP1.Items.Clear();
                            //var repository = new Repository();
                            var list = repository.GetGeneralList("REG_OTHER_PROF_BODIES");
                            var subList = ddlNameProfessionOP1.SelectedValue + "_";
                            list = list.Where(I => I.Code.StartsWith(subList)).ToList();
                            ddlRegBodyOP1.DataSource = list;
                            ddlRegBodyOP1.DataValueField = "Code";
                            ddlRegBodyOP1.DataTextField = "Description";
                            ddlRegBodyOP1.DataBind();
                            ddlRegBodyOP1.Items.Insert(0, string.Empty);

                            var listItem2 = ddlRegBodyOP1.Items.FindByValue(firstOP1.RegulatoryBody);
                            if (listItem2 != null) ddlRegBodyOP1.SelectedValue = listItem2.Value;
                            //ddlRegBodyOP1.SelectedValue = firstOP1.RegulatoryBody;
                            if (ddlRegBodyOP1.SelectedItem.Text.ToUpper() == "OTHER")
                            {
                                trRegBodyOtherOP1.Visible = true;
                                txtRegBodyOtherOP1.Text = firstOP1.RegulatoryBodyOther;
                            }
                        }
                        
                        ddlProvinceOP1.SelectedValue = firstOP1.Province;
                        ddlCountryOP1.SelectedValue = firstOP1.Country;
                        txtLicenseOP1.Text = firstOP1.RegNumber;

                        txtRegStatusOP1.Text = firstOP1.RegStatus;

                        ddlInitMonthOP1.SelectedValue = firstOP1.InitMonth;
                        ddlInitYearOP1.SelectedValue = firstOP1.InitYear;

                        if (firstOP1.ExpiryDate != null)
                        {
                            txtExpireDateOP1.Text = ((DateTime)firstOP1.ExpiryDate).ToString("MM/dd/yyyy");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ddlRegBodyOP1.SelectedValue)) cbNoExpirationOP1.Checked = true;
                        }

                        if (!string.IsNullOrEmpty(firstOP1.RegulatoryBody))
                        {
                            DisableEnableProfession2Controls(true);
                        }
                    }

                    var secondOP1 = history.PracticeOtherDetails.Find(I => I.OrderNum == 2);
                    if (secondOP1 != null)
                    {
                        hfPracticeSeqnOP2.Value = secondOP1.SEQN.ToString();

                        ddlNameProfessionOP2.SelectedValue = secondOP1.OtherProfessionType;

                        if (ddlNameProfessionOP2.SelectedItem.Text.ToUpper() == "OTHER")
                        {
                            trNameProfessionOtherOP2.Visible = true;
                            txtNameProfessionOtherOP2.Text = secondOP1.OtherProfessionTypeOther;
                            ddlRegBodyOP2.Items.Clear();
                            ddlRegBodyOP2.DataBind();
                            ddlRegBodyOP2.Items.Add("Other");
                            trRegBodyOtherOP2.Visible = true;
                            txtRegBodyOtherOP2.Text = secondOP1.RegulatoryBodyOther;
                        }
                        else
                        {
                            trNameProfessionOtherOP2.Visible = false;
                            txtNameProfessionOtherOP2.Text = string.Empty;

                            trRegBodyOtherOP2.Visible = false;
                            txtRegBodyOtherOP2.Text = string.Empty;

                            ddlRegBodyOP2.Items.Clear();
                            //string newLookup = "REG_OTHER_PROF_" + ddlNameProfessionOP2.SelectedValue;
                            ////var repository = new Repository();
                            //var list = repository.GetGeneralList(newLookup);
                            //var repository = new Repository();
                            var list = repository.GetGeneralList("REG_OTHER_PROF_BODIES");
                            var subList = ddlNameProfessionOP2.SelectedValue + "_";
                            list = list.Where(I => I.Code.StartsWith(subList)).ToList();
                            ddlRegBodyOP2.DataSource = list;
                            ddlRegBodyOP2.DataValueField = "Code";
                            ddlRegBodyOP2.DataTextField = "Description";
                            ddlRegBodyOP2.DataBind();
                            ddlRegBodyOP2.Items.Insert(0, string.Empty);

                            var listItem2 = ddlRegBodyOP2.Items.FindByValue(secondOP1.RegulatoryBody);
                            if (listItem2 != null) ddlRegBodyOP2.SelectedValue = listItem2.Value;
                            //ddlRegBodyOP1.SelectedValue = firstOP1.RegulatoryBody;
                            if (ddlRegBodyOP2.SelectedItem.Text.ToUpper() == "OTHER")
                            {
                                trRegBodyOtherOP2.Visible = true;
                                txtRegBodyOtherOP2.Text = firstOP1.RegulatoryBodyOther;
                            }
                        }

                        ddlProvinceOP2.SelectedValue = secondOP1.Province;
                        ddlCountryOP2.SelectedValue = secondOP1.Country;
                        txtLicenseOP2.Text = secondOP1.RegNumber;

                        txtRegStatusOP2.Text = secondOP1.RegStatus;

                        ddlInitMonthOP2.SelectedValue = secondOP1.InitMonth;
                        ddlInitYearOP2.SelectedValue = secondOP1.InitYear;

                        if (secondOP1.ExpiryDate != null)
                        {
                            txtExpireDateOP2.Text = ((DateTime)secondOP1.ExpiryDate).ToString("MM/dd/yyyy");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ddlRegBodyOP2.SelectedValue)) cbNoExpirationOP2.Checked = true;
                        }
                    }
                }
            }

            var user2 = repository.GetApplicationUserInfo(CurrentUserId);
            if (user2 != null)
            {
                //CurrentUser = user2;
                lblPersonalEmploymentInformationTitle.Text = string.Format("Professional Registration for {0} #{1}", user2.FullName, CurrentUserId);
            }
        }

        protected string UpdateUserProfessionalHistory()
        {
            var repository = new Repository();

            var history = new ProfessionalHistory();
            history.PracticeDetails = new List<ProfessionalHistoryPracticeDetailNew>();
            history.PracticeOtherDetails = new List<ProfessionalHistoryPracticeDetailNew>();

            int SeqnProf1 = 0;
            int SeqnProf2 = 0;
            int SeqnProf3 = 0;
            int SeqnProf4 = 0;
            int SeqnProfOP1 = 0;
            int SeqnProfOP2 = 0;

            int.TryParse(hfPracticeSeqn1.Value, out SeqnProf1);
            int.TryParse(hfPracticeSeqn2.Value, out SeqnProf2);
            int.TryParse(hfPracticeSeqn3.Value, out SeqnProf3);
            int.TryParse(hfPracticeSeqn4.Value, out SeqnProf4);

            int.TryParse(hfPracticeSeqnOP1.Value, out SeqnProfOP1);
            int.TryParse(hfPracticeSeqnOP2.Value, out SeqnProfOP2);

            // if Seqn = 0 - new record, if seqn > 0 and ddlRegBody1.SelectedValue is blank - deleteing record from db
            if (SeqnProf1 > 0 || !string.IsNullOrEmpty(ddlRegBody1.SelectedValue))
            {
                var hItem = new ProfessionalHistoryPracticeDetailNew();
                hItem.SEQN = SeqnProf1;
                hItem.RegType = "OT";
                hItem.RegulatoryBody = ddlRegBody1.SelectedValue;
                if (!string.IsNullOrEmpty(ddlRegBody1.SelectedValue))
                {
                    if (ddlRegBody1.SelectedValue == "ZOTHER")
                    {
                        hItem.RegulatoryBodyOther = txtRegBodyOther1.Text;
                    }
                    hItem.Province = ddlProvince1.SelectedValue;
                    hItem.Country = ddlCountry1.SelectedValue;
                    hItem.RegNumber = txtLicense1.Text;
                    hItem.RegStatus = txtRegStatus1.Text;
                    hItem.InitMonth = ddlInitMonth1.SelectedValue;
                    hItem.InitYear = ddlInitYear1.SelectedValue;

                    var culture = new CultureInfo("en-CA");
                    CultureInfo provider = CultureInfo.InvariantCulture;

                    if (!string.IsNullOrEmpty(txtExpiryDate1.Text))
                    {
                        DateTime expDate = DateTime.ParseExact(txtExpiryDate1.Text, "MM/dd/yyyy", provider);
                        hItem.ExpiryDate = expDate;
                    }
                    else
                    {
                        hItem.ExpiryDate = null;
                    }
                }
                history.PracticeDetails.Add(hItem);
            }

            if (SeqnProf2 > 0 || !string.IsNullOrEmpty(ddlRegBody2.SelectedValue))
            {
                var hItem = new ProfessionalHistoryPracticeDetailNew();
                hItem.SEQN = SeqnProf2;
                hItem.RegType = "OT";
                hItem.RegulatoryBody = ddlRegBody2.SelectedValue;
                if (!string.IsNullOrEmpty(ddlRegBody2.SelectedValue))
                {
                    if (ddlRegBody2.SelectedValue == "ZOTHER")
                    {
                        hItem.RegulatoryBodyOther = txtRegBodyOther2.Text;
                    }
                    hItem.Province = ddlProvince2.SelectedValue;
                    hItem.Country = ddlCountry2.SelectedValue;
                    hItem.RegNumber = txtLicense2.Text;
                    hItem.RegStatus = txtRegStatus2.Text;
                    hItem.InitMonth = ddlInitMonth2.SelectedValue;
                    hItem.InitYear = ddlInitYear2.SelectedValue;

                    var culture = new CultureInfo("en-CA");
                    CultureInfo provider = CultureInfo.InvariantCulture;

                    if (!string.IsNullOrEmpty(txtExpiryDate2.Text))
                    {
                        DateTime expDate = DateTime.ParseExact(txtExpiryDate2.Text, "MM/dd/yyyy", provider);
                        hItem.ExpiryDate = expDate;
                    }
                    else
                    {
                        hItem.ExpiryDate = null;
                    }
                }
                history.PracticeDetails.Add(hItem);
            }

            if (SeqnProf3 > 0 || !string.IsNullOrEmpty(ddlRegBody3.SelectedValue))
            {
                var hItem = new ProfessionalHistoryPracticeDetailNew();
                hItem.SEQN = SeqnProf3;
                hItem.RegType = "OT";
                hItem.RegulatoryBody = ddlRegBody3.SelectedValue;
                if (!string.IsNullOrEmpty(ddlRegBody3.SelectedValue))
                {
                    if (ddlRegBody3.SelectedValue == "ZOTHER")
                    {
                        hItem.RegulatoryBodyOther = txtRegBodyOther3.Text;
                    }
                    hItem.Province = ddlProvince3.SelectedValue;
                    hItem.Country = ddlCountry3.SelectedValue;
                    hItem.RegNumber = txtLicense3.Text;
                    hItem.RegStatus = txtRegStatus3.Text;
                    hItem.InitMonth = ddlInitMonth3.SelectedValue;
                    hItem.InitYear = ddlInitYear3.SelectedValue;

                    var culture = new CultureInfo("en-CA");
                    CultureInfo provider = CultureInfo.InvariantCulture;

                    if (!string.IsNullOrEmpty(txtExpiryDate3.Text))
                    {
                        DateTime expDate = DateTime.ParseExact(txtExpiryDate3.Text, "MM/dd/yyyy", provider);
                        hItem.ExpiryDate = expDate;
                    }
                    else
                    {
                        hItem.ExpiryDate = null;
                    }
                }
                history.PracticeDetails.Add(hItem);
            }

            if (SeqnProf4 > 0 || !string.IsNullOrEmpty(ddlRegBody4.SelectedValue))
            {
                var hItem = new ProfessionalHistoryPracticeDetailNew();
                hItem.SEQN = SeqnProf4;
                hItem.RegType = "OT";
                hItem.RegulatoryBody = ddlRegBody4.SelectedValue;
                if (!string.IsNullOrEmpty(ddlRegBody4.SelectedValue))
                {
                    if (ddlRegBody4.SelectedValue == "ZOTHER")
                    {
                        hItem.RegulatoryBodyOther = txtRegBodyOther4.Text;
                    }
                    hItem.Province = ddlProvince4.SelectedValue;
                    hItem.Country = ddlCountry4.SelectedValue;
                    hItem.RegNumber = txtLicense4.Text;
                    hItem.RegStatus = txtRegStatus4.Text;
                    hItem.InitMonth = ddlInitMonth4.SelectedValue;
                    hItem.InitYear = ddlInitYear4.SelectedValue;

                    var culture = new CultureInfo("en-CA");
                    CultureInfo provider = CultureInfo.InvariantCulture;

                    if (!string.IsNullOrEmpty(txtExpiryDate4.Text))
                    {
                        DateTime expDate = DateTime.ParseExact(txtExpiryDate4.Text, "MM/dd/yyyy", provider);
                        hItem.ExpiryDate = expDate;
                    }
                    else
                    {
                        hItem.ExpiryDate = null;
                    }
                }
                history.PracticeDetails.Add(hItem);
            }

            if (SeqnProfOP1 > 0 || !string.IsNullOrEmpty(ddlNameProfessionOP1.SelectedValue))
            {
                var hItem = new ProfessionalHistoryPracticeDetailNew();
                hItem.SEQN = SeqnProfOP1;
                hItem.RegType = "OTHER";
                hItem.OtherProfessionType = ddlNameProfessionOP1.SelectedValue;
                hItem.OtherProfessionTypeOther = txtNameProfessionOtherOP1.Text;
                hItem.RegulatoryBody = ddlRegBodyOP1.SelectedValue;
                if (!string.IsNullOrEmpty(ddlRegBodyOP1.SelectedValue))
                {
                    if (trRegBodyOtherOP1.Visible)
                    {
                        hItem.RegulatoryBodyOther = txtRegBodyOtherOP1.Text;
                    }
                    hItem.Province = ddlProvinceOP1.SelectedValue;
                    hItem.Country = ddlCountryOP1.SelectedValue;
                    hItem.RegNumber = txtLicenseOP1.Text;
                    hItem.RegStatus = txtRegStatusOP1.Text;
                    hItem.InitMonth = ddlInitMonthOP1.SelectedValue;
                    hItem.InitYear = ddlInitYearOP1.SelectedValue;

                    var culture = new CultureInfo("en-CA");
                    CultureInfo provider = CultureInfo.InvariantCulture;

                    if (!string.IsNullOrEmpty(txtExpireDateOP1.Text))
                    {
                        DateTime expDate = DateTime.ParseExact(txtExpireDateOP1.Text, "MM/dd/yyyy", provider);
                        hItem.ExpiryDate = expDate;
                    }
                    else
                    {
                        hItem.ExpiryDate = null;
                    }
                }
                history.PracticeOtherDetails.Add(hItem);
            }

            if (SeqnProfOP2 > 0 || !string.IsNullOrEmpty(ddlNameProfessionOP2.SelectedValue))
            {
                var hItem = new ProfessionalHistoryPracticeDetailNew();
                hItem.SEQN = SeqnProfOP2;
                hItem.RegType = "OTHER";
                hItem.OtherProfessionType = ddlNameProfessionOP2.SelectedValue;
                hItem.OtherProfessionTypeOther = txtNameProfessionOtherOP2.Text;
                hItem.RegulatoryBody = ddlRegBodyOP2.SelectedValue;
                if (!string.IsNullOrEmpty(ddlRegBodyOP2.SelectedValue))
                {
                    if (trRegBodyOtherOP2.Visible)
                    {
                        hItem.RegulatoryBodyOther = txtRegBodyOtherOP2.Text;
                    }
                    hItem.Province = ddlProvinceOP2.SelectedValue;
                    hItem.Country = ddlCountryOP2.SelectedValue;
                    hItem.RegNumber = txtLicenseOP2.Text;
                    hItem.RegStatus = txtRegStatusOP2.Text;
                    hItem.InitMonth = ddlInitMonthOP2.SelectedValue;
                    hItem.InitYear = ddlInitYearOP2.SelectedValue;

                    var culture = new CultureInfo("en-CA");
                    CultureInfo provider = CultureInfo.InvariantCulture;

                    if (!string.IsNullOrEmpty(txtExpireDateOP2.Text))
                    {
                        DateTime expDate = DateTime.ParseExact(txtExpireDateOP2.Text, "MM/dd/yyyy", provider);
                        hItem.ExpiryDate = expDate;
                    }
                    else
                    {
                        hItem.ExpiryDate = null;
                    }
                }
                history.PracticeOtherDetails.Add(hItem);
            }

            string message = repository.UpdateApplicationUserProfessionalHistoryLoggedNew(CurrentUserId, history);
            return message;
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        /// <summary>
        ///  Messages collection. Contains all custom warning or errors which will be populated on the page.
        /// </summary>
        public PageMessages Messages
        {
            get
            {
                return (PageMessages)SessionParameters.PageMessages;
            }
            set
            {
                SessionParameters.PageMessages = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                if (SessionParameters.CurrentUser != null)
                {
                    return SessionParameters.CurrentUser;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUser = value;
            }
        }

        #endregion

        

        

        
    }
}