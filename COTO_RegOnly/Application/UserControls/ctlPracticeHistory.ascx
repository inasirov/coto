﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlPracticeHistory.ascx.cs" Inherits="COTO_RegOnly.Application.UserControls.ctlPracticeHistory" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="pnlUpdate" runat="server">
            <ContentTemplate>
                <table style="border: 0px; width: 600px; padding: 0px; border-spacing: 2px;">
                    <tr class="HeaderTitle">
                        <td class="HeaderTitle" style="text-align: right;">
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Registration Application Step 7 of 11" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr class="RowTitle">
                        <td>
                            <div>
                                <asp:Label ID="lblPracticeHistoryTitle" runat="server" CssClass="heading" Text="OT Practice History" />
                            </div>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    
                    <tr id="trMemberInfo" runat="server" >
                        <td>
                            <table class="memberInfo" style="width: 600px">
                                <tr>
                                    <td style="text-align: left; width: 60%">
                                        <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* - denotes required field" />
                                    </td>
                                    <td style="text-align: right; font-weight:bold; width: 40%">
                                        <p>
                                            <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/Glossary-OccupationalTherapyPractice.pdf" target="_blank">Glossary</a>
                                            <%--&nbsp;/&nbsp;<a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/Glossary-OccupationalTherapyPracticeFR.pdf" target="_blank">Glossaire</a>--%>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        
                                    </td>
                                </tr>
                                <tr id="trDescription" runat="server">
                                    <td colspan="2" style="text-align: left;">
                                        <p>
                                            If any of the information displayed is incorrect, please
                                            <asp:LinkButton ID="lbtnCorrectDetails" runat="server" Text="click here " OnClick="lbtnCorrectDetailsClick" />&nbsp;
                                            to provide the correct details to the College.
                                        </p>
                                    </td>
                                </tr>
                                <tr id="trEditConfirmation" runat="server" visible="false">
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblMessage" runat="server" Text="Your request for information change request has been received." ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr id="trDescriptionNew" runat="server">
                                    <td colspan="2" style="text-align: left;">
                                        <p>
                                            Please complete the required information
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <span style="font-size: Medium; font-weight: bold;">Initial Practice Information</span>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <span>What is the first year you began practising occupational therapy, in or outside of Ontario?</span>&nbsp;
                                        <span style="white-space:nowrap;">If you have never practised occupational therapy please select "Not Applicable". <sup>*</sup></span>
                                        
                                    </td>
                                    <td style="text-align: right; vertical-align: top;">
                                        <asp:DropDownList ID="ddlFirstYearOTPractice" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFirstYearOTPracticeSelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvFirstYearOTPractice" runat="server" ControlToValidate="ddlFirstYearOTPractice"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please provide your first year you began practising occupational therapy"
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblFirstCountryOTPractice" runat="server" Text='What is the first country in which you practised occupational therapy after completing your entry level occupational therapy education? If you have not practised outside of Canada, please select "Canada". <sup>*</sup>' />&nbsp;
                                    </td>
                                    <td style="text-align: right; vertical-align: top;">
                                        <asp:DropDownList ID="ddlFirstCountryOTPractice" runat="server" OnSelectedIndexChanged="ddlFirstCountryOTPracticeSelectedIndexChanged" AutoPostBack="true" Width="120px">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvFirstCountryOTPractice" runat="server" ControlToValidate="ddlFirstCountryOTPractice"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please provide your first country in which you practised occupational therapy after completing your entry level occupational therapy education"
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblFirstStateOTPractice" runat="server" Text='What is the first province or territory (if Canada), or state (if USA) in which you began practising occupational therapy after completing your entry level occupational therapy education? If you have not practised outside of Ontario, please select "Ontario".  If your first country of occupational therapy practice is not Canada or the USA, please select "Not Applicable" <sup>*</sup>' />&nbsp;&nbsp;
                                    </td>
                                    <td style="text-align: right;vertical-align: top;">
                                        <asp:DropDownList ID="ddlFirstStateOTPractice" runat="server" OnSelectedIndexChanged="ddlFirstStateOTPracticeSelectedIndexChanged" AutoPostBack="true" Width="120px">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvFirstStateOTPractice" runat="server" ControlToValidate="ddlFirstStateOTPractice"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please provide your first province or territory or state in which you began practising occupational therapy after completing your entry level occupational therapy education"
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <span style="font-size: Medium; font-weight: bold;">First Canadian Practice Information</span>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblFirstYearCanadianOTPractice" runat="server" Text='What is the first year you practised occupational therapy in Canada?  If you have never practised occupational therapy please select "Not Applicable". <sup>*</sup>' />&nbsp;&nbsp;
                                    </td>
                                    <td style="text-align: right;vertical-align: top;">
                                        <asp:DropDownList ID="ddlFirstYearCanadianOTPractice" runat="server" OnSelectedIndexChanged="ddlFirstYearCanadianOTPracticeSelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvFirstYearCanadianOTPractice" runat="server" ControlToValidate="ddlFirstYearCanadianOTPractice"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please provide your first year you practiced occupational therapy in Canada"
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red"  />
                                        <asp:Label ID="lblFirstYearCanadianPracticeErrorMessage" runat="server" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblFirstProvinceOTPractice" runat="server" Text='What is the first Canadian province or territory in which you practised occupational therapy? If you have not practised outside of Ontario, please select "Ontario". If your first country of occupational therapy practice is not Canada, please select "Not applicable".<sup>*</sup>' />&nbsp;&nbsp;
                                    </td>
                                    <td style="text-align: right;vertical-align: top;">
                                        <asp:DropDownList ID="ddlFirstProvinceOTPractice" runat="server" Width="120px">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvFirstProvinceOTPractice" runat="server" ControlToValidate="ddlFirstProvinceOTPractice"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please provide your first Canadian province or territory in which you practiced occupational therapy"
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <span style="font-size: Medium; font-weight: bold;">Most Recent Non-Ontario Practice
                                            Information</span>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="RightColumn">
                                        <span>If you have never practised occupational therapy outside of Ontario, please select “Not Applicable” from each of the dropdown menus and proceed to the next page.</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="RightColumn">
                                        <asp:Label ID="lblLastYearOutsideOntarioTitle" runat="server" Text='What is the most recent year in which you practised occupational therapy outside of Ontario?' />
                                    </td>
                                    <td style="text-align: right;vertical-align: top;">
                                        <asp:DropDownList ID="ddlLastYearOutsideOntario" runat="server" OnSelectedIndexChanged="ddlLastYearOutsideOntarioSelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList><br />
                                        <asp:Label ID="lblLastYearOutsideOntarioErrorMessage" runat="server" ForeColor="Red" />
                                        <%--<asp:RequiredFieldValidator ID="rfvLastYearOutsideOntario" runat="server" ControlToValidate="ddlLastYearOutsideOntario"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please provide your last year of practise in jurisdiction outside of Ontario"
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblLastCountryOTPractice" runat="server" Text='What is the most recent country in which you practised occupational therapy outside of Canada?' />
                                    </td>
                                    <td style="text-align: right;vertical-align: top;">
                                        <asp:DropDownList ID="ddlLastCountryOTPractice" runat="server" Width="120px">
                                        </asp:DropDownList>
                                        
                                        <%--<asp:RequiredFieldValidator ID="rfvLastCountryOTPractice" runat="server" ControlToValidate="ddlLastCountryOTPractice"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please provide your last country of occupational therapy Practice"
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <asp:Label ID="lblLastStateTitle" runat="server" Text="Last Canadian province/territory or USA state of occupational therapy practice (Canada or USA only)" />
                                    </td>
                                    <td style="text-align: right;vertical-align: top;">
                                        <asp:DropDownList ID="ddlLastStateOrCanadianProvince" runat="server" Width="120px">
                                        </asp:DropDownList>
                                         
                                        <%--<asp:RequiredFieldValidator ID="rfvLastStateOrCanadianProvince" runat="server" ControlToValidate="ddlLastStateOrCanadianProvince"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="<br />Please provide your last province/territory/state of occupational therapy Practice"
                                            Display="Dynamic" EnableClientScript="false" ForeColor="Red" />--%>
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>