﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlApplicationCheckList.ascx.cs"
    Inherits="COTO_RegOnly.Application.UserControls.ctlApplicationCheckList" %>
<style type="text/css">
.withBorder tr td
{
    border: 1px solid black;
    padding: 4px;
}
.withBorder
{
    border-collapse: collapse;
}
</style>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="border: 0px; width: 100%; padding: 0px; border-spacing: 0px;">
                    <tr class="HeaderTitle" style="text-align: right;">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Registration Application - Application Checklist" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <%--<asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" style="width: 100%; padding: 2px; border-spacing: 3px;">
                                <%--<tr>
                                    <td>
                                        <asp:Label ID="lblPageTitle" CssClass="heading" runat="server" Text="Application Checklist" />
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td style="text-align: left;">
                                        <p>For details on how to meet the requirement(s) and/or what you are required to submit, please see the appropriate section of our website:</p>
                                        <ul>
                                            <li>
                                                <a href="https://www.coto.org/memberservices/applicants/canadian-educated" target="_blank">Canadian Educated</a>
                                            </li>
                                            <li>
                                                <a href="https://www.coto.org/memberservices/applicants/internationally-educated" target="_blank">Internationally Educated </a>
                                            </li>
                                            <li>
                                                <a href="https://www.coto.org/memberservices/applicants/registered-in-another-province" target="_blank">Registered in Another Province</a>
                                            </li>
                                            <li>
                                                <a href="https://www.coto.org/memberservices/applicants/reapply-to-the-college" target="_blank">Previously Registered with the College</a>
                                            </li>

                                        </ul>
                                    </td>
                                </tr>
                                <tr id="trNoAccess" runat="server">
                                    <td>Your Application Checklist is not ready to view online </td>
                                </tr>
                                <tr id="trView" runat="server">
                                    <td>
                                        <table  class="memberInfo withBorder" style="width: 100%; padding: 0px; border-spacing: 0px;">
                                            <tr>
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblRequirementTitle" runat="server" Text="Requirement" Font-Bold="true" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblDateRequirement" runat="server" Text="Date Requirement Met" Font-Bold="true"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem1Title" runat="server" Text="Application" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem1Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem2Title" runat="server" Text="Application Fee" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem2Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem3Title" runat="server" Text="Currency Requirement" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem3Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus4" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem4Title" runat="server" Text="Additional Currency Data Sheet" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem4Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus5" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem5Title" runat="server" Text="Practice Supervisor Form" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem5Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus6" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem6Title" runat="server" Text="Initial Learning Contract" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem6Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus7" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem7Title" runat="server" Text="Final Learning Contract" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem7Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblVS_CheckTitle" runat="server" Text="Vulnerable Sector Check" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblVS_CheckStatus" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem8Title" runat="server" Text="Suitability to Practise" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem8Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus9" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem9Title" runat="server" Text="Suitability to Practise Review Fee" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem9Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus10" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem10Title" runat="server" Text="Language Fluency" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem10Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem11Title" runat="server" Text="Work Eligibility/Legal Work Authorization" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem11Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <%--<tr id="trStatus12" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem12Title" runat="server" Text="WES Report" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem12Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus13" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem13Title" runat="server" Text="Academic Equivalency Review" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem13Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus14" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem14Title" runat="server" Text="Academic Review Tool" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem14Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus15" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem15Title" runat="server" Text="Curriculum" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem15Status" runat="server"  />
                                                </td>
                                            </tr>--%>
                                            <tr id="trStatus16" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem16Title" runat="server" Text="Final Educational Transcript" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem16Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus17" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem17Title" runat="server" Text="SEAS Disposition Report " />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem17Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus17a" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem17aTitle" runat="server" Text="LMSA Form" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem17aStatus" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus18" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem18Title" runat="server" Text="OT Regulatory History 1" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem18Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus19" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem19Title" runat="server" Text="OT Regulatory History 2" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem19Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus20" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem20Title" runat="server" Text="OT Regulatory History 3" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem20Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus21" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem21Title" runat="server" Text="OT Regulatory History 4" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem21Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus22" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem22Title" runat="server" Text="OT Regulatory History 5" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem22Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus23" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem23Title" runat="server" Text="Other Professional Registration 1" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem23Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus24" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem24Title" runat="server" Text="Other Professional Registration 2" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem24Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus25" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem25Title" runat="server" Text="Other Professional Registration 3" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem25Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem26Title" runat="server" Text="Professional Liability Insurance" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem26Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus27" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem27Title" runat="server" Text="Affidavit/Confirmation of Graduation from University" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem27Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus28" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem28Title" runat="server" Text="Employer Acknowledgement Form" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem28Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr id="trStatus29" runat="server">
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem29Title" runat="server" Text="Confirmation of Registration for CAOT Exam" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem29Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem30Title" runat="server" Text="CAOT Exam Results" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem30Status" runat="server"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblItem31Title" runat="server" Text="Registration Fee" />
                                                </td>
                                                <td class="RightColumn">
                                                    <asp:Label ID="lblItem31Status" runat="server"  />
                                                </td>
                                            </tr>    
                                        </table>
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
