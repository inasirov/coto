﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlIntroduction.ascx.cs" Inherits="COTO_RegOnly.Application.UserControls.ctlIntroduction" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<%@ Import Namespace="COTO_RegOnly.Classes" %>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="HeaderTitle" align="left">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Registration Application - Online Form"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tr>
                                    <td style="width:15%; text-align:left">
                                        <asp:Label ID="lblUserIDTitle" runat="server" Text="User ID:" />
                                    </td>
                                    <td style="text-align:left;">
                                        <asp:Label ID="lblUserID" runat="server" Text="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:15%; text-align:left">
                                        <asp:Label ID="lblUserNameTitle" runat="server" Text="User Name:" />
                                    </td>
                                    <td style="text-align:left;">
                                        <asp:Label ID="lblUserName" runat="server" Text="<Guest>" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">
                            <asp:LinkButton ID="lbtnRegistrationPage" runat="server" Text="Registration Page" OnClick="lbtnRegistrationPageClick" Enabled="false" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">
                            <asp:LinkButton ID="lbtnLoginPage" runat="server" Text="Login" OnClick="lbtnLoginPageClick" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">
                            <asp:LinkButton ID="lbtnSignUpPage" runat="server" Text="Sign Up" OnClick="lbtnSignUpPageClick" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="errorsPanel" runat="server" Style="display: none; width: 750px;" CssClass="modalPopup"
                                DefaultButton="okBtn">
                                <table width="100%" cellspacing="2" cellpadding="2">
                                    <tr class="topHandleRed">
                                        <td align="left" runat="server" id="tdCaption">
                                            <asp:Label ID="lblCaption" runat="server" Text="Error Messages:"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="text-align: left">
                                                <asp:Label ID="lblErrorMessage" runat="server" />
                                                <asp:ValidationSummary ID="ValidationSummary" runat="server" DisplayMode="BulletList"
                                                    ShowSummary="true" ValidationGroup="GeneralValidation" />
                                                <div style="text-align: right">
                                                    <asp:Button ID="okBtn" runat="server" Text="Ok" /></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <ajaxToolkit:ModalPopupExtender ID="modalPopupEx" runat="server" PopupControlID="errorsPanel"
                                TargetControlID="invisibleTarget" CancelControlID="okBtn" BackgroundCssClass="modalBackground">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Label ID="invisibleTarget" runat="server" Style="display: none" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>