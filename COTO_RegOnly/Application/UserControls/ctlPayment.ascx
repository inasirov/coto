﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlPayment.ascx.cs" Inherits="COTO_RegOnly.Application.UserControls.ctlPayment" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<%@ Import Namespace="COTO_RegOnly.Classes" %>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="border: 0; border-spacing: 0; padding: 0; width: 100%">
                    <tr class="HeaderTitle" style="text-align: right;">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Registration Application Step 11 of 11" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <%--<asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                Visible="false" ValidationGroup="PersonalValidation" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" style="width: 100%; border-spacing: 2px; padding: 3px;">
                                <tr class="RowTitle">
                                    <td>
                                        <asp:Label ID="lblPersonalInformationSectionTitle" CssClass="heading" runat="server"
                                            Text="Payment" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <p>
                                            Instructions
                                        </p>
                                        <p>
                                            A non-sufficient funds (NSF) fee of $<%#  HardcodedValues.Application_NSF_Fee_Total%>($<%#  HardcodedValues.Application_NSF_Fee%>+ $<%#  HardcodedValues.Application_NSF_Fee_HST%>HST) 
                                            will be charged to all payments that are returned NSF.
                                        </p>
                                        <p>
                                            Please note that partial payments and post dated cheques are not permitted.
                                        </p>
                                        <p>
                                            Please select your payment method:
                                        </p>
                                        <ol>
                                            <li>Online credit card payment (Visa/MasterCard/Amex) - select add to basket </li>
                                            <li>Offline payment (Online banking/cheque/money order/credit card) -&nbsp;
                                                <asp:LinkButton ID="lbtnOfflinePayment" runat="server" Text="click here" OnClick="lbtnOfflinePaymentClick" />&nbsp;
                                                <asp:ImageButton ID="ibtnOfflinePaymentHelp" runat="server" OnClick="ibtnOfflinePaymentHelpClick"
                                                    ImageUrl="~/images/qmark.jpg" />
                                            </li>
                                        </ol>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ListView ID="lvSubscriptions" runat="server">
                                            <LayoutTemplate>
                                                <table style="width: 100%; text-align: center; border: 0; border-spacing:0; padding: 3px;">
                                                    <tr style="background-color: #EBEBEB; ">
                                                        <td style="width: 35px;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="text-align: left;">
                                                            <asp:Label ID="lblItemHeader" runat="server" Text="Item" />
                                                        </td>
                                                        <td style="width: 25px;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 100px; text-align: left; padding-right: 4px;">
                                                            <asp:Label ID="lblCostHeader" runat="server" Text="Cost" />
                                                        </td>
                                                    </tr>
                                                    <tr id="ItemPlaceHolder" runat="server">
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="lblTotalText" runat="server" Text="Total" Font-Bold="true" />&nbsp;
                                                        </td>
                                                        <td style='text-align: right; border-bottom: 1px solid black; border-left: 1px solid black;'>
                                                            $US
                                                        </td>
                                                        <td style='text-align: right; border-bottom: 1px solid black;'>
                                                            <asp:Label ID="lblTotalSummaryValue" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </LayoutTemplate>
                                            <EmptyDataTemplate>
                                                <table style="width: 100%; text-align: center; border: 0; border-spacing:0; padding: 3px;">
                                                    <tr style="background-color: #EBEBEB;">
                                                        <td style="width: 35px;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="text-align: left;">
                                                            <asp:Label ID="lblItemHeader" runat="server" Text="Item" />
                                                        </td>
                                                        <td style="width: 25px;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 100px; text-align: right; padding-right: 4px;">
                                                            <asp:Label ID="lblCostHeader" runat="server" Text="Cost" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td colspan="3">
                                                            <span>Empty List</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="lblTotalText" runat="server" Text="Total" Font-Bold="true" />&nbsp;
                                                        </td>
                                                        <td style="text-align: right;">
                                                            $US
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="lblFundChartSummaryValue" runat="server" Text="0.00" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EmptyDataTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="border-bottom: 1px solid black;">
                                                        <asp:Image ID="imgCheckBox" runat="server" ImageUrl="~/images/checkbox.gif" />
                                                    </td>
                                                    <td style="white-space: nowrap; text-align: left; border-bottom: 1px solid black;">
                                                        <asp:Label ID="lblSubscriptionItem" runat="server" Text='<%# Eval("DuesProduct.Title") %>' />
                                                    </td>
                                                    <td style="text-align: right; border-bottom: 1px solid black; border-left: 1px solid black;">
                                                        $US
                                                    </td>
                                                    <td style="white-space: nowrap; text-align: right; border-bottom: 1px solid black;">
                                                        <asp:Label ID="lblSubscriptionItemCost" runat="server" Text='<%# ((decimal)Eval("AmountBilled")).ToString("#,#0.00;") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <%--<asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                ValidationGroup="PersonalValidation" Visible="false" />--%>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>
