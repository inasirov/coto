﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using System.Globalization;
using System.Text;
using System.Security.Cryptography;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlInsurance : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;

        private string PrevStep = WebConfigItems.Application_Step8;
        private string NextStep = WebConfigItems.Application_Step10;
        private const int CurrentStep = 9;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.Application_Step0);
                return;
            }

            if (!IsPostBack) // first time loading 
            {

                SessionParameters.ApplicationStep = CurrentStep;
                BindLists();
                BindData();

                if (!string.IsNullOrEmpty(WebConfigItems.Application_Step9_LabourMobilitSupportAgreementConfirmationForm))
                {
                    var link = WebConfigItems.Application_Step9_LabourMobilitSupportAgreementConfirmationForm;
                    lbtnLaborMobilitySupportAgreementFormLink.Attributes.Add("onclick", string.Format("javascript:window.open('{0}', '_blank')", link));
                }

                if (!string.IsNullOrEmpty(WebConfigItems.Application_Step9_RegulatoryHistoryForm))
                {
                    var link2 = WebConfigItems.Application_Step9_RegulatoryHistoryForm;
                    lbtnRegulatoryHistoryFormLink.Attributes.Add("onclick", string.Format("javascript:window.open('{0}', '_blank')", link2));
                }
                //DataBind();
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            UpdateSteps(-1);
            //Response.Redirect("MemberDisplay.aspx");
            Response.Redirect(PrevStep);
        }
        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            if (ValidationCheck())  // Page.IsValid
            {
                UpdateUserInsuranceInfo();
                UpdateSteps(1);
                //Response.Redirect("MemberDisplay.aspx");
                Response.Redirect(NextStep);
            }
            else
            {
                if (Messages != null && Messages.Count > 0)
                {
                    string message = string.Empty;
                    foreach (var item in Messages)
                    {
                        message += ((PageMessage)item).Message + "<br />";
                    }
                    message = string.Format("<font color='Red'>{0}</font>", message);

                    Messages.Clear();
                    ShowErrorMessage(message);
                }
            }
        }

        protected void ibLanguageServiceHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("Please indicate all language(s) in which you can competently provide OT services.", "Help Languages");
        }

        protected void ddlPlanHeldWithSelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshInsuranceSection();
        }

        protected void ddlExaminationCategorySelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshExamSection();
        }

        #endregion

        #region Methods

        protected void RefreshExamSection()
        {
            trCAOTExamCompleted.Visible = false;
            trCAOTExamCompleted2.Visible = false;
            trCAOTExamRegistered.Visible = false;
            trCAOTExamRegistered2.Visible = false;
            trLMSA.Visible = false;

            if (ddlExaminationCategory.SelectedValue.ToUpper() == "EXAM_COMPLETE")
            {
                trCAOTExamCompleted.Visible = true;
                trCAOTExamCompleted2.Visible = true;
            }
            if (ddlExaminationCategory.SelectedValue.ToUpper() == "EXAM_REG")
            {
                trCAOTExamRegistered.Visible = true;
                trCAOTExamRegistered2.Visible = true;
            }

            if (ddlExaminationCategory.SelectedValue.ToUpper() == "LMSA")
            {
                trLMSA.Visible = true;
            }
        }

        public void AddMessage(string message, PageMessageType messageType)
        {
            this.Messages.Add(new PageMessage(message, messageType));
        }

        protected void ShowErrorMessage(string Message)
        {
            lblErrorMessages.Text = Message;
            update.Update();
        }

        protected bool ValidationCheck()
        {
            bool valid = true;
            
            // DateTime.TryParseExact(txt
            if (ddlExaminationCategory.SelectedValue.ToUpper() == "EXAM_COMPLETE")
            {
                DateTime complDate = DateTime.MinValue;
                CultureInfo provider = CultureInfo.InvariantCulture;
                string format = "MM/dd/yyyy";
                if (!string.IsNullOrEmpty(txtDateExamCompletion.Text) && DateTime.TryParseExact(txtDateExamCompletion.Text, format, provider, DateTimeStyles.None, out complDate))
                {
                    if (complDate >= DateTime.Now)
                    {
                        AddMessage("Date of completion must be less than current date", PageMessageType.UserError);
                        valid = false;
                    }
                }
                else
                {
                    AddMessage("Date of completion has invalid format", PageMessageType.UserError);
                    valid = false;
                }
            }

            //if (ddlExaminationCategory.SelectedValue.ToUpper() == "EXAM_REG")
            //{
            //    DateTime examDate = DateTime.MinValue;
            //    CultureInfo provider = CultureInfo.InvariantCulture;
            //    string format = "dd/MM/yyyy";
            //    if (!string.IsNullOrEmpty(txtDateExamRegistration.Text) && DateTime.TryParseExact(txtDateExamRegistration.Text, format, provider, DateTimeStyles.None, out examDate))
            //    {
            //        if (examDate <= DateTime.Now)
            //        {
            //            AddMessage("Date of exam must be greater than current date", PageMessageType.UserError);
            //            valid = false;
            //        }
            //    }
            //    else
            //    {
            //        AddMessage("Date of exam has invalid format", PageMessageType.UserError);
            //        valid = false;
            //    }
            //}

            //if (trExpiryDate.Visible)  // ddlPlanHeldWith.SelectedValue == "OT" 
            //{
                DateTime expiryDate = DateTime.MinValue;
                DateTime startDate = DateTime.MinValue;
                CultureInfo provider2 = CultureInfo.InvariantCulture;
                string format2 = "MM/dd/yyyy";
                if (trExpiryDate.Visible && !string.IsNullOrEmpty(txtExpiryDate.Text) && !DateTime.TryParseExact(txtExpiryDate.Text, format2, provider2, DateTimeStyles.None, out expiryDate))
                {
                    AddMessage("Expiry Date has invalid format", PageMessageType.UserError);
                    valid = false;
                }
                if (trStartDate.Visible && !string.IsNullOrEmpty(txtStartDate.Text) && !DateTime.TryParseExact(txtStartDate.Text, format2, provider2, DateTimeStyles.None, out startDate))
                {
                    AddMessage("Start Date has invalid format", PageMessageType.UserError);
                    valid = false;
                }
                if (trExpiryDate.Visible && expiryDate != DateTime.MinValue)
                {
                    if (expiryDate <= DateTime.Today)
                    {
                        AddMessage("Insurance Expiry date must be greater than todays date", PageMessageType.UserError);
                        valid = false;
                    }
                }

                if (trExpiryDate.Visible &&  expiryDate != DateTime.MinValue && trStartDate.Visible && startDate != DateTime.MinValue)
                {
                    if (expiryDate <= startDate)
                    {
                        AddMessage("Insurance Expiry date must be greater than Start Date", PageMessageType.UserError);
                        valid = false;
                    }
                }
            //}

            Page.Validate("PersonalValidation");

            if (!Page.IsValid)
            {
                lblErrorMessages.Text = string.Empty;
                valid = false;
            }
            return valid;
        }

        protected void RefreshInsuranceSection()
        {
            txtExpiryDate.ReadOnly = false;
            ceExpiryDate.Enabled = true;
            //rjsExpiryDate.Visible = true;
            //rjsExpiryDate.Enabled = true;
            if (ddlPlanHeldWith.SelectedValue.ToUpper() == "NONE")
            {
                trCertificate.Visible = false;
                trStartDate.Visible = false;
                trExpiryDate.Visible = false;
            }
            else
            {
                trCertificate.Visible = true;
                trStartDate.Visible = true;
                trExpiryDate.Visible = true;
            }

            if (ddlPlanHeldWith.SelectedValue.ToLower() == "ot")
            {
                trOtherInsurance.Visible = true;
                //txtCertificate.MaxLength = 10;
                //revCertificate.Enabled = false;
            }
            else
            {
                txtOther.Text = string.Empty;
                trOtherInsurance.Visible = false;
                //txtCertificate.MaxLength = 6;
                //revCertificate.Enabled = true;
                //Page.Validate("PersonalValidation");
                if (ddlPlanHeldWith.SelectedValue == "OSOT")
                {
                    //DateTime expiryDate = new DateTime(2014, 10, 1);
                    //txtExpiryDate.Text = expiryDate.ToString("MM/dd/yyyy");
                    //txtExpiryDate.ReadOnly = true;
                    //ceExpiryDate.Enabled = false;
                    //rjsExpiryDate.Visible = false;
                    //rjsExpiryDate.Enabled = false;
                    //rjsDateExamCompletion.Enabled = true;
                    //rjsDateExamRegistration.Enabled = true;
                }
                if (ddlPlanHeldWith.SelectedValue == "CAOT")
                {
                    //DateTime expiryDate = new DateTime(2014, 10, 1);
                    //txtExpiryDate.Text = expiryDate.ToString("MM/dd/yyyy");
                    //txtExpiryDate.ReadOnly = true;
                    //ceExpiryDate.Enabled = false;
                    //rjsExpiryDate.Visible = false;
                    //rjsExpiryDate.Enabled = false;
                    //rjsDateExamCompletion.Enabled = true;
                    //rjsDateExamRegistration.Enabled = true;
                }
            }
        }

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.ApplicationStep != 0)
            {
                SessionParameters.ApplicationStep += diff;
            }
        }

        protected void UpdateUserInsuranceInfo()
        {
            int currentYear = DateTime.Now.Year;
            if (DateTime.Now.Month <= 5)
            {
                currentYear = currentYear - 1;
            }
            User user = new User();

            user.Id = CurrentUserId;
            user.InsuranceInfo = new Classes.Insurance();
            int insurSEQN = 0;
            if (!string.IsNullOrEmpty(hfInsuranceSEQN.Value))
            {
                int.TryParse(hfInsuranceSEQN.Value, out insurSEQN);
                user.InsuranceInfo.SEQN = insurSEQN;
            }
            user.InsuranceInfo.PlanHeld = ddlPlanHeldWith.SelectedValue;
            user.InsuranceInfo.OtherInsuranceProvider = txtOther.Text;
            
            CultureInfo provider = CultureInfo.InvariantCulture;
            if (trStartDate.Visible && !string.IsNullOrEmpty(txtStartDate.Text))
            {
                var startDate = DateTime.ParseExact(txtStartDate.Text, "MM/dd/yyyy", provider);
                user.InsuranceInfo.StartDate = startDate;
            }
            else
            {
                user.InsuranceInfo.StartDate = DateTime.Now; // DateTime.MinValue;
            }
            if (trExpiryDate.Visible && !string.IsNullOrEmpty(txtExpiryDate.Text))
            {
                var expiryDate = DateTime.ParseExact(txtExpiryDate.Text, "MM/dd/yyyy", provider);
                user.InsuranceInfo.ExpiryDate = expiryDate;
            }
            else
            {
                user.InsuranceInfo.ExpiryDate = DateTime.MinValue;
            }
            if (trCertificate.Visible)
            {
                user.InsuranceInfo.CertificateNumber = txtCertificate.Text;
            }
            else
            {
                user.InsuranceInfo.CertificateNumber = string.Empty;
            }
            
            var repository = new Repository();
            repository.UpdateApplicationUserInsuranceInfoLoggedNew(CurrentUserId, user);

            // updating exam info
            user.ExaminationInfo = new Classes.Examination();
            user.ExaminationInfo.ExaminationStatus = ddlExaminationCategory.SelectedValue;
            if (trCAOTExamCompleted.Visible)
            {
                if (!string.IsNullOrEmpty(txtDateExamCompletion.Text))
                {
                    //var culture = new CultureInfo("en-CA");
                    //CultureInfo provider = CultureInfo.InvariantCulture;
                    var complDate = DateTime.ParseExact(txtDateExamCompletion.Text, "MM/dd/yyyy", provider);
                    //var complDate = Convert.ToDateTime(txtDateExamCompletion.Text, culture);
                    user.ExaminationInfo.CompletionDate = complDate;
                }
                else
                {
                    user.ExaminationInfo.CompletionDate = DateTime.MinValue;
                }
            }
            if (trCAOTExamRegistered.Visible)
            {
                //if (!string.IsNullOrEmpty(txtDateExamRegistration.Text))
                //{
                //    var culture = new CultureInfo("en-CA");
                //    var examDate = Convert.ToDateTime(txtDateExamRegistration.Text, culture);
                //    user.ExaminationInfo.ExaminationDate = examDate;
                //}
                //else
                //{
                //    user.ExaminationInfo.ExaminationDate = DateTime.MinValue;
                //}
                if (!string.IsNullOrEmpty(ddlDateExamRegistration.SelectedValue))
                {
                    //var culture = new CultureInfo("en-CA");
                    //CultureInfo provider = CultureInfo.InvariantCulture;
                    //DateTime examDate = DateTime.ParseExact(ddlDateExamRegistration.SelectedValue, "yyyy-MM-dd", provider);
                    ////var examDate = Convert.ToDateTime(txtDateExamRegistration.Text, culture);
                    //user.ExaminationInfo.ExaminationDate = examDate;
                    user.ExaminationInfo.ExaminationDateExt = ddlDateExamRegistration.SelectedValue;
                }
                else
                {
                    //user.ExaminationInfo.ExaminationDate = DateTime.MinValue;
                    user.ExaminationInfo.ExaminationDateExt = string.Empty;
                }
            }
            repository.UpdateApplicationUserExamInfoLogged(CurrentUserId, user);
            // end of updating exam info

            //if (ddlPlanHeldWith.SelectedValue.ToUpper() == "OT")
            //{
            //    string message = string.Format("Member ID: {0}; ", CurrentUserId); ;
            //    message += string.Format("Date: {0}<br />", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));


            //    message += "You have selected \"Other\" as your insurance provider. The insurance policy you have selected must meet the College’s requirements " +
            //    "for professional liability insurance and include a sexual abuse therapy and counseling fund endorsement.<br /><br />" +
            //    "You are required to submit a copy of the insurance policy content and certificate to the College for review and approval. " +
            //    "Please submit a copy to Brandi Park, Manager, Registration via email  bpark@coto.org or by fax  416-214-0851. <br /><br />" +
            //    "If you have any questions please contact Brandi Park via email  bpark@coto.org or by telephone  416-214-1177 (toll free 1-800-890-6570) x229 ";

            //    string emailSubject = "Registrant Requires Insurance Review " + WebConfigItems.GetTestLabel;
            //    string emailTo = WebConfigItems.RegistrationManagerContactEmail;
            //    if (!string.IsNullOrEmpty(CurrentUserEmail))
            //    {
            //        emailTo += "; " + CurrentUserEmail;
            //    }
            //    var tool = new Tools();
            //    tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);
            //}
        }

        protected void BindData()
        {
            var repository = new Repository();
            var user = repository.GetApplicationUserInsuranceInfoNew(CurrentUserId);
            if (user != null)
            {
                if (user.InsuranceInfo != null)
                {
                    hfInsuranceSEQN.Value = user.InsuranceInfo.SEQN.ToString();

                    var item = ddlPlanHeldWith.Items.FindByValue(user.InsuranceInfo.PlanHeld);
                    if (item != null)
                    {
                        ddlPlanHeldWith.SelectedValue = user.InsuranceInfo.PlanHeld;
                    }

                    if (user.InsuranceInfo.StartDate != DateTime.MinValue)
                    {
                        txtStartDate.Text = user.InsuranceInfo.StartDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                    }

                    if (user.InsuranceInfo.ExpiryDate != DateTime.MinValue)
                    {
                        txtExpiryDate.Text = user.InsuranceInfo.ExpiryDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                    }

                    if (ddlPlanHeldWith.SelectedValue.ToUpper() == "OT")
                    {
                        txtOther.Text = user.InsuranceInfo.OtherInsuranceProvider;
                        trOtherInsurance.Visible = true;
                    }
                    else
                    {
                        trOtherInsurance.Visible = false;
                    }
                    txtCertificate.Text = user.InsuranceInfo.CertificateNumber;
                }
                RefreshInsuranceSection();
            }
            else
            {
                hfInsuranceSEQN.Value = "0";
            }

            var user1 = repository.GetApplicationUserExaminationInfo(CurrentUserId);
            if (user1 != null)
            {
                if (user1.ExaminationInfo != null)
                {
                    if (!string.IsNullOrEmpty(user1.ExaminationInfo.ExaminationStatus))
                    {
                        ddlExaminationCategory.SelectedValue = user1.ExaminationInfo.ExaminationStatus;
                    }
                    
                    if (user1.ExaminationInfo.ExaminationStatus.ToUpper() == "EXAM_COMPLETE")
                    {
                        //trCAOTExamCompleted.Visible = true;
                        //trCAOTExamCompleted2.Visible = true;
                        txtDateExamCompletion.Text = user1.ExaminationInfo.CompletionDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                    }
                    if (user1.ExaminationInfo.ExaminationStatus.ToUpper() == "EXAM_REG")
                    {
                        //trCAOTExamRegistered.Visible = true;
                        //trCAOTExamRegistered2.Visible = true;
                        ddlDateExamRegistration.SelectedValue = user1.ExaminationInfo.ExaminationDateExt;
                        //txtDateExamRegistration.Text = user1.ExaminationInfo.ExaminationDate.ToString("dd/MM/yyyy");
                    }
                    if (user1.ExaminationInfo.ExaminationStatus.ToUpper() == "LMSA")
                    {
                        //trLMSA.Visible = true;
                    }
                    RefreshExamSection();
                }
            }
            var user2 = repository.GetApplicationUserInfo(CurrentUserId);
            if (user2 != null)
            {
                //CurrentUser = user2;
                CurrentUserEmail = user2.Email;
                lblInsuranceTitle.Text = string.Format("Insurance & Examination for {0} #{1}", user2.FullName, user2.Id);
            }
        }

        protected void BindLists()
        {
            var repository = new Repository();

            var list1 = repository.GetGeneralList("INSURANCE"); // get data for status field
            list1.RemoveAll(I => I.Code == "OSOT");
            ddlPlanHeldWith.DataSource = list1;
            ddlPlanHeldWith.DataValueField = "CODE";
            ddlPlanHeldWith.DataTextField = "DESCRIPTION";
            ddlPlanHeldWith.DataBind();
            ddlPlanHeldWith.Items.Insert(0, string.Empty);

            var list2 = repository.GetGeneralList("ENTRY_CATEGORY"); // get data for status field
            ddlExaminationCategory.DataSource = list2;
            ddlExaminationCategory.DataValueField = "CODE";
            ddlExaminationCategory.DataTextField = "DESCRIPTION";
            ddlExaminationCategory.DataBind();
            ddlExaminationCategory.Items.Insert(0, string.Empty);

            var list3 = repository.GetGeneralList("CAOT_EXAM_DATES"); // get data for status field
            DateTime complDate = DateTime.MinValue;
            CultureInfo provider = CultureInfo.InvariantCulture;
            string format = "yyyy-MM-dd";
            foreach (var item in list3)
            {
                if ( DateTime.TryParseExact(item.Substitute, format, provider, DateTimeStyles.None, out complDate))
                {
                    if (complDate < DateTime.Now)
                    {
                        item.Code = "delete";
                    }
                }
            }
            list3.RemoveAll(I => I.Code == "delete");

            ddlDateExamRegistration.DataSource = list3;
            ddlDateExamRegistration.DataValueField = "SUBSTITUTE";
            ddlDateExamRegistration.DataTextField = "DESCRIPTION";
            ddlDateExamRegistration.DataBind();
            ddlDateExamRegistration.Items.Insert(0, string.Empty);
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        public string CurrentUserEmail
        {
            get
            {
                if (ViewState["CURRENT_USER_EMAIL"] != null)
                {
                    return (string)ViewState["CURRENT_USER_EMAIL"];
                }
                else
                    return string.Empty; ;
            }
            set
            {
                ViewState["CURRENT_USER_EMAIL"] = value;
            }
        }

        /// <summary>
        ///  Messages collection. Contains all custom warning or errors which will be populated on the page.
        /// </summary>
        public PageMessages Messages
        {
            get
            {
                return (PageMessages)SessionParameters.PageMessages;
            }
            set
            {
                SessionParameters.PageMessages = value;
            }
        }

        #endregion
    }
}