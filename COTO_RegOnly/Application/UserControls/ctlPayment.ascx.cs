﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using System.Security.Cryptography;
using System.Text;
//using Asi;
//using Asi.Web;
//using Asi.iBO.ContactManagement;
//using Asi.iBO;
//using Asi.iBO.Commerce;
//using Asi.Soa.ClientServices;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlPayment : System.Web.UI.UserControl
    {
        #region Consts

        private const string VIEW_STATE_CURRENT_IMIS_ID = "GetCurrentImisId";
        private const string CHECKOUT_BASKET_PAGE = "~/BasketCheckout.aspx";

        private string _Key = "Yz7!~3";
        //private string _Key = string.Empty;

        private string PrevStep = WebConfigItems.Application_Step10;
        //private string NextStep = WebConfigItems.Application_Step12;
        private const int CurrentStep = 11;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.Application_Step0);
                return;
            }

            if (!string.IsNullOrEmpty(CurrentUserId))
            {
                //CurrentIMISID = newId;
                //BindDues();
                //CheckExistingItemsInBasket();
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(PrevStep);
        }

        protected void lbtnOfflinePaymentClick(object sender, EventArgs e)
        {
            Response.Redirect(WebConfigItems.Application_Step11_Offline_Payment);
        }

        protected void ibtnOfflinePaymentHelpClick(object sender, ImageClickEventArgs e)
        {
            ShowMessage("1. Send credit card information to the College, by mail or by fax, to be processed in-house by the College. <br />2. Make payment by mailing a cheque or money order to the College.", "Help - Offline Payment");
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        #endregion

        #region Methods

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");
                Session["ID"] = null;
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }

        }
        
        //public string CurrentIMISID
        //{
        //    get
        //    {
        //        if (ViewState[VIEW_STATE_CURRENT_IMIS_ID] != null)
        //        {
        //            return (string)(ViewState[VIEW_STATE_CURRENT_IMIS_ID]);
        //        }
        //        else
        //        {
        //            string newId = GetCurrentiMISID();
        //            ViewState[VIEW_STATE_CURRENT_IMIS_ID] = newId;
        //            return newId;
        //        }
        //    }
        //    set
        //    {
        //        ViewState[VIEW_STATE_CURRENT_IMIS_ID] = value;
        //    }
        //}

        //public string GetCurrentiMISID()
        //{
        //    string _id = "";
        //    try
        //    {
        //        if (Asi.Security.AppPrincipal.CurrentIdentity.IsAuthenticated && Asi.Security.Utility.SecurityHelper.LoggedInImisId != "")
        //        {
        //            _id = Asi.Security.Utility.SecurityHelper.LoggedInImisId;
        //        }
        //    }
        //    catch { }
        //    return _id;
        //}

        #endregion

        

    }
}