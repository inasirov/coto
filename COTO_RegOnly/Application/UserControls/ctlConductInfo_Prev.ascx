﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlConductInfo_Prev.ascx.cs" Inherits="COTO_RegOnly.Application.UserControls.ctlConductInfo_Prev" %>

<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion1" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion2" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion3" />

                <asp:AsyncPostBackTrigger ControlID="ddlQuestion4" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion5" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion11" />
                <asp:AsyncPostBackTrigger ControlID="ddlQuestion12" />
                
            </Triggers>
            <ContentTemplate>
                <table style="border: 0px; width: 100%; padding: 0px; border-spacing: 0px;">
                    <tr class="HeaderTitle" style="text-align: right;">
                        <td>
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Registration Application Step 8 of 11" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" style="width: 100%; padding: 2px; border-spacing: 3px;">
                                <tr>
                                    <td style="width: 15%">&nbsp; </td>
                                    <td style="width: 65%">&nbsp; </td>
                                    <td style="width: 20%"><a href="https://payments.coto.org/coto/Custom/Application/Docs/Glossary%20-%20Suitability%20to%20Practise.pdf" target="_blank">Glossary</a> </td>
                                </tr>
                                <tr class="RowTitle">
                                    <td colspan="3">
                                        <asp:Label ID="lblConductTitle" runat="server" CssClass="heading" Text="Professional Conduct" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align:left">
                                        <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">&nbsp; </td>
                                    <td style="width: 65%">&nbsp; </td>
                                    <td style="width: 20%"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="RightColumn">
                                        <p>
                                            These questions are about occupational therapy practice, the practice of other regulated professions, offences and bail conditions, conduct and your ability to practise occupational therapy safely and ethically. If you answer <strong>‘yes’</strong> to any of the questions, you will be required to provide further information. 
                                        </p>
                                        <p>For a more detailed description of an underlined term, use the glossary at the top of the page.</p>
                                        <p>
                                            Beginning April 1, 2017, applicants must submit a Vulnerable Sector (VS) Check as part of their application for registration with the College – this also applies to current applicants who have not been issued registration by April 1, 2017. The College uses the results of the VS Check to verify information pertaining to offences; bail conditions or restrictions; and any other relevant conduct.
                                            The College’s mandate is public protection and VS Checks are an important step in the process to support that mandate. Depending on where you live, it may take 1 to 12 weeks, or longer, to obtain the results of a VS Check. As registration will not be issued until results are received, we suggest applying for the VS Check well in advance of your required registration date.
                                        </p>
                                        <p>
                                            Please note, if you do not register within six months of the date the results were issued by the police, you will be required to submit an updated VS Check. Please see the following section of our website for full details: <a href='https://www.coto.org/memberservices/applicants/vulnerable-sector-checks' target="_blank">https://www.coto.org/memberservices/applicants/vulnerable-sector-checks</a>. 
                                        </p>
                                        <p>
                                            If there is a concern that you will not practice occupational therapy safely and ethically, your application will be referred to the Registration Committee for review. For more information on the review process, please refer to the <a href="https://www.coto.org/docs/default-source/registration-policies/8-72-determining-suitability-to-practise-at-registration.pdf?sfvrsn=2" target="_blank">Determining Suitability to Practice at Registration</a> policy.</p>
                                        <%--<p><br />
                                            <b><u>Professional Conduct</u></b></p>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="lblQuestion1Title" runat="server" Text="1. Have you been refused registration, membership or a license with a &lt;u&gt;regulatory body&lt;/u&gt; in Canada or elsewhere?  *" />
                                        <asp:HiddenField ID="hfReportedDate" runat="server" />
                                        <asp:HiddenField ID="hfConductSEQN" runat="server" Value="0" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion1" runat="server" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlQuestion1SelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion1" runat="server" ControlToValidate="ddlQuestion1" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please select 'Yes' or 'No'" ForeColor="Red" InitialValue="" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr id="trQuestion1Details" runat="server" visible="false">
                                    <td style="vertical-align: top;">
                                        <asp:Label ID="lblQuestion1DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td class="RightColumn" colspan="2">
                                        <asp:TextBox ID="txtQuestion1Details" runat="server" Columns="60" Rows="3" TextMode="MultiLine" />
                                        <asp:RequiredFieldValidator ID="rvfQuestion1Details" runat="server" ControlToValidate="txtQuestion1Details" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Details field is empty" ForeColor="Red" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="lblQuestion2Title" runat="server" Text="2. Have you had a &lt;u&gt;finding&lt;/u&gt; of &lt;u&gt;professional misconduct&lt;/u&gt;, &lt;u&gt;incompetence&lt;/u&gt;, &lt;u&gt;incapacity&lt;/u&gt;, or &lt;u&gt;similar issue&lt;/u&gt; in Canada or elsewhere? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion2" runat="server" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlQuestion2SelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion2" runat="server" ControlToValidate="ddlQuestion2" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please select 'Yes' or 'No'" ForeColor="Red" InitialValue="" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr id="trQuestion2Details" runat="server" visible="false">
                                    <td style="vertical-align: top;">
                                        <asp:Label ID="lblQuestion2DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td class="RightColumn" colspan="2">
                                        <asp:TextBox ID="txtQuestion2Details" runat="server" Columns="60" Rows="3" TextMode="MultiLine" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion2Details" runat="server" ControlToValidate="txtQuestion2Details" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Details field is empty" ForeColor="Red" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion3Title" runat="server" Text="3. Are you &lt;u&gt;currently facing a proceeding&lt;/u&gt; (such as a hearing) for &lt;u&gt;professional misconduct&lt;/u&gt;, &lt;u&gt;incompetence&lt;/u&gt;, &lt;u&gt;incapacity&lt;/u&gt; or a similar issue in Canada or elsewhere? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion3" runat="server" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlQuestion3SelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion3" runat="server" ControlToValidate="ddlQuestion3" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please select 'Yes' or 'No'" ForeColor="Red" InitialValue="" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr id="trQuestion3Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="lblQuestion3DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td class="RightColumn" colspan="2">
                                        <asp:TextBox ID="txtQuestion3Details" runat="server" Columns="60" Rows="3" TextMode="MultiLine" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion3Details" runat="server" ControlToValidate="txtQuestion3Details" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Details field is empty" ForeColor="Red" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion4Title" runat="server" Text="4. Have you ever had a &lt;u&gt;finding&lt;/u&gt; of &lt;u&gt;professional negligence&lt;/u&gt; or &lt;u&gt;malpractice&lt;/u&gt; in Canada or elsewhere? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion4" runat="server" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlQuestion4SelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion4" runat="server" ControlToValidate="ddlQuestion4" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please select 'Yes' or 'No'" ForeColor="Red" InitialValue="" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr id="trQuestion4Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="Label2" runat="server" Text="Details:" />
                                    </td>
                                    <td class="RightColumn" colspan="2">
                                        <asp:TextBox ID="txtQuestion4Details" runat="server" Columns="60" Rows="3" TextMode="MultiLine" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion4Details" runat="server" ControlToValidate="txtQuestion4Details" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Details field is empty" ForeColor="Red" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion5Title" runat="server" Text="5. Is there anything in your &lt;u&gt;previous conduct&lt;/u&gt; that would give the College a reason to believe that you lack the knowledge, skill or judgement to practise safely and ethically? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion5" runat="server" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlQuestion5SelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion5" runat="server" ControlToValidate="ddlQuestion5" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please select 'Yes' or 'No'" ForeColor="Red" InitialValue="" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr id="trQuestion5Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="Label4" runat="server" Text="Details:" />
                                    </td>
                                    <td class="RightColumn" colspan="2">
                                        <asp:TextBox ID="txtQuestion5Details" runat="server" Columns="60" Rows="3" TextMode="MultiLine" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion5Details" runat="server" ControlToValidate="txtQuestion5Details" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Details field is empty" ForeColor="Red" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td colspan="3" style="vertical-align: top; text-align: left">
                                        <p>
                                            <b><u>Offences and Bail Conditions</u></b></p>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="lblQuestion11Title" runat="server" Text="6. Have you been &lt;u&gt;found guilty&lt;/u&gt; by a court or other &lt;u&gt;lawful authority&lt;/u&gt; of any &lt;u&gt;offence&lt;/u&gt; in Canada or elsewhere that has not been previously reported to the College? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion11" runat="server" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlQuestion11SelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion11" runat="server" ControlToValidate="ddlQuestion11" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please select 'Yes' or 'No'" ForeColor="Red" InitialValue="" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr id="trQuestion11Details" runat="server" visible="false">
                                    <td style="vertical-align: top;">
                                        <asp:Label ID="lblQuestion11DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td class="RightColumn" colspan="2">
                                        <asp:TextBox ID="txtQuestion11Details" runat="server" Columns="60" Rows="3" TextMode="MultiLine" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion11Details" runat="server" ControlToValidate="txtQuestion11Details" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Details field is empty" ForeColor="Red" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left">
                                        <asp:Label ID="lblQuestion12Title" runat="server" Text="7. Are you currently subject to any &lt;u&gt;conditions or restrictions&lt;/u&gt; (such as bail conditions) by a court (or similar authority) in Canada or elsewhere? *" />
                                    </td>
                                    <td class="RightColumn">
                                        <asp:DropDownList ID="ddlQuestion12" runat="server" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlQuestion12SelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion12" runat="server" ControlToValidate="ddlQuestion12" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Please select 'Yes' or 'No'" ForeColor="Red" InitialValue="" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr id="trQuestion12Details" runat="server" visible="false">
                                    <td style="vertical-align: top; text-align: left">
                                        <asp:Label ID="lblQuestion12DetailsTitle" runat="server" Text="Details:" />
                                    </td>
                                    <td class="RightColumn" colspan="2">
                                        <asp:TextBox ID="txtQuestion12Details" runat="server" Columns="60" Rows="3" TextMode="MultiLine" />
                                        <asp:RequiredFieldValidator ID="rfvQuestion12Details" runat="server" ControlToValidate="txtQuestion12Details" Display="Dynamic" EnableClientScript="false" ErrorMessage="&lt;br /&gt;Details field is empty" ForeColor="Red" ValidationGroup="PersonalValidation" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <hr style="color: Gray" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg"
                                OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg"
                                OnClick="ibtnNextClick" ValidationGroup="PersonalValidation" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>

<style type="text/css">
    .RightColumn p{
        padding: 5px;
    }
</style>