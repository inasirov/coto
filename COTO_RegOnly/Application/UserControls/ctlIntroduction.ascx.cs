﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlIntroduction : System.Web.UI.UserControl
    {
        #region Consts

        private const string VIEW_STATE_CURRENT_IMIS_ID = "GetCurrentImisId";
        private const string Reset_Password_Url = "/IMIS15/COTO/AsiCommon/Controls/Shared/FormsAuthentication/RecoverPassword.aspx";
        private string ReturnPageUrl = WebConfigItems.Application_Step1;
        private string LoginPageUrl = "";
        private string SingUpPageUrl = "";
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (!string.IsNullOrEmpty(CurrentUserId))
                //{
                    //CurrentUserId = newId;
                    //BindExistingUserInfo();
                //}
            }
        }

        protected void lbtnRegistrationPageClick(object sender, EventArgs e)
        {
            Response.Redirect(WebConfigItems.Application_Step1);
        }

        protected void lbtnLoginPageClick(object sender, EventArgs e)
        {
            Response.Redirect(LoginPageUrl);
        }

        protected void lbtnSignUpPageClick(object sender, EventArgs e)
        {
            Response.Redirect(SingUpPageUrl);
        }

        #region Methods

        //public void BindExistingUserInfo()
        //{
        //    var repository = new Repository();
        //    var contact = repository.GetContactByID(CurrentUserId);
        //    if (contact != null)
        //    {
        //        lblUserID.Text = contact.ContactId;
        //        lblUserName.Text = contact.FullName;
        //        lbtnRegistrationPage.Enabled = true;
        //    }
        //}

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        #endregion
    }
}