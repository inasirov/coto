﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlEmploymentProfile.ascx.cs" Inherits="COTO_RegOnly.Application.UserControls.ctlEmploymentProfile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<script src="../Scripts/MaskedEditFix.js" type="text/javascript"></script>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Src="~/UserControls/DialogBox.ascx" TagName="DialogBox" TagPrefix="uc" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
    
    <style type="text/css">
        body
        {
            font-family: Arial, Helvetica, sans-serif;
            /*font-size: 13px;*/
        }
        .errMessage
        {
            width:300px;
            border: 1px solid;
            margin: 10px 0px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            position: relative;
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('images/info.png');
        }
        .modalBackground {
            z-index: 6000 !important;
        }
        .modalPopup
        {
            z-index: 6001 !important;
        }
        body{
            min-height: 600px;
        }
      
    </style>
    <script type="text/javascript">
        function showMessage() {
            $('.errMessage').show();
            setTimeout('$(".errMessage").fadeOut(800)', 2000);
        }

        function valPhone(source, clientside_arguments) {
            clientside_arguments.IsValid = checkPhoneNumber
            (clientside_arguments.Value);
        }

        function checkPhoneNumber(phoneNo) {
            var phoneRE = /^(1\s*[-\/\.]?)?(\((\d{3})\)|(\d{3}))\s*[-\/\.]?\s*(\d{3})\s*[-\/\.]?\s*(\d{4})\s*(([xX]|[eE][xX][tT])\.?\s*(\d+))*$/;
            if (phoneNo.match(phoneRE)) {
                return true;
            } else {
                return false;
            }
        }

        function UpdateField() {
            var hfield = document.getElementById('<%=txtTelephone3.ClientID %>');
            if (hfield) {
                var oldValue = hfield.value;
                var pattern = /\_/g;
                var pattern2 = /\(\)/g;
                var newValue1 = oldValue.replace(pattern, "");
                var newValue2 = newValue1.replace(pattern2, "");
                if (newValue2 == ' -') {
                    newValue2 = "";
                }

                hfield.value = newValue2;
            }
        }

        function UpdateTextField(hfield) {
            //var hfield = document.getElementById('<%=txtTelephone3.ClientID %>');
            // var hfield = document.getElementById(controlID);

            if (hfield) {
                var oldValue = hfield.value;
                var pattern = /\_/g;
                var pattern2 = /\(\)/g;
                var newValue1 = oldValue.replace(pattern, "");
                var newValue2 = newValue1.replace(pattern2, "");
                if (newValue2 == ' -') {
                    newValue2 = "";
                }

                hfield.value = newValue2;
            }
        }

    </script>

<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server">
            <Triggers>
                <asp:PostBackTrigger ControlID="ddlOffer" />
            </Triggers>
            <ContentTemplate>
                <table style="width: 100%; border-spacing: 2px; padding: 0; border: 0px;">
                    <tr class="HeaderTitle">
                        <td class="HeaderTitle" style="text-align: right;">
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Registration Application Step 5 of 11" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" UseSubmitBehavior="true" TabIndex="0"/>--%>
                        </td>
                    </tr>
                    <%--<tr>
                        <td align="right">
                            <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back" OnClick="btnBackClick" />&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Update" OnClick="btnUpdateClick"  />
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                            <uc:DialogBox ID="dmb" runat="server" />
                        </td>
                    </tr>
                    <tr class="RowTitle">
                        <td>
                            <div>
                                <asp:Label ID="lblPersonalEmploymentInformationTitle" runat="server" CssClass="heading"
                                Text="Employment Profile" />
                            </div>
                            
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 50%">
                                        <asp:Label ID="lblDenotesRequiredTitle" runat="server" Text="* denotes required field" /><br />
                                        <asp:Label ID="lblDebugMessage" runat="server" />
                                    </td>
                                    <td style="text-align: right; padding-right: 20px;">
                                        <p>
                                            <a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/Glossary-PracticeSiteInformation.pdf" target="_blank">Glossary</a>
                                            <%--&nbsp;/&nbsp;<a href="https://occupationaltherapist.coto.org/coto/Custom/Application/Docs/Glossary-PracticeSiteInformationFR.pdf" target="_blank">Glossaire</a>--%>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo">
                                <tr>
                                    <td style="text-align: left;">
                                        <asp:Label ID="lblHaveOffer" runat="server" Text="Do you have an offer of occupational therapy employment in Ontario? *"
                                            Font-Bold="true" />&nbsp;&nbsp;
                                        <asp:DropDownList ID="ddlOffer" runat="server" OnSelectedIndexChanged="ddlOfferSelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>&nbsp;
                                        <asp:RequiredFieldValidator ID="rfvOffer" runat="server" ControlToValidate="ddlOffer" SetFocusOnError="True"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="Offer of occupational therapy employment in Ontario: Please select 'YES' or 'NO'"
                                            Display="None" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                
                                <tr id="trQuestion1" runat="server" visible="false">
                                    <td style="text-align: left;">
                                        <asp:Label ID="lblQuestion1Text" runat="server" Text="Will you have more than three employers? *"
                                            Font-Bold="true" />&nbsp;&nbsp;
                                        <asp:RadioButtonList ID="rblQuestion1Answers" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Yes" Value="1" Selected="False" />
                                            <asp:ListItem Text="No" Value="0" Selected="True" />
                                        </asp:RadioButtonList>
                                        <br />
                                    </td>
                                </tr>
                                <tr id="trQuestion2" runat="server" visible="false">
                                    <td class="RightColumn">
                                        <asp:Label ID="lblCollegeMailings" runat="server" Text="College mailings are usually sent to the home address. Would you prefer the College to send mail to your primary employment address? *"
                                         Font-Bold="true" />&nbsp;&nbsp;
                                        <asp:DropDownList ID="ddlCollegeMailings" runat="server" >
                                        </asp:DropDownList>&nbsp;
                                        <asp:RequiredFieldValidator ID="rfvCollegeMailings" runat="server" ControlToValidate="ddlCollegeMailings" SetFocusOnError="True"
                                            InitialValue="" ValidationGroup="GeneralValidation" ErrorMessage="College Mailing: Please select 'YES' or 'NO'"
                                            Display="None" EnableClientScript="false" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr id="trEmployment" runat="server" visible="false">
                                    <td class="RightColumn">
                                        <ajaxToolkit:TabContainer ID="tcMainContainer" CssClass="NewsTab" runat="server" CssTheme="None"
                                            AutoPostBack="false" TabStripPlacement="Top" ScrollBars="Auto" ActiveTabIndex="0">
                                            <ajaxToolkit:TabPanel ID="tpEmpl1" runat="server" HeaderText="PRIMARY SITE" ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table style="width: 100%; border-spacing: 4px; padding: 2px;">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblStatus1Title" runat="server" Text="Status" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlStatus1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus1SelectedIndexChanged" />
                                                                <asp:Label ID="lblStatus1Message" runat="server" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblStartDate1Label" runat="server" Text="Start Date (MM/DD/YYYY)" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtStartDate1" runat="server" />
                                                                
                                                                <%--<rjs:PopCalendar ID="PopCalendar1" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtStartDate1" From-Date="1950-01-01" ValidationGroup="Employer1Validation"
                                                                    MessageAlignment="RightCalendarControl" Enabled="true"  />--%>
                                                                <asp:Image ID="imgStartDate1" runat="server" ImageUrl="~/Images/calen_25.png" style="vertical-align: middle;" />
                                                                <ajax:CalendarExtender ID="ceStartDate1" runat="server" TargetControlID="txtStartDate1" Format="MM/dd/yyyy" StartDate="01/01/1950" PopupButtonID="imgStartDate1" />
                                                                <asp:RequiredFieldValidator ID="rfvStartDate1" runat="server" ControlToValidate="txtStartDate1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 1: Start Date is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer1Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblEndDate1Label" runat="server" Text="End Date (MM/DD/YYYY)" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtEndDate1" runat="server" />
                                                                <asp:Image ID="imgEndDate1" runat="server" ImageUrl="~/Images/calen_25.png" style="vertical-align: middle;"/>
                                                                <ajax:CalendarExtender ID="ceEndDate1" runat="server" TargetControlID="txtEndDate1" Format="MM/dd/yyyy" StartDate="01/01/1950" PopupButtonID="imgEndDate1" />
                                                                <%--<rjs:PopCalendar ID="PopCalendar2" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtEndDate1" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: top;" class="LeftTitle">
                                                                <asp:Label ID="lblEmployerName1Label" runat="server" Text="Employer Name" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtEmployerName1" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                                <asp:RequiredFieldValidator ID="rfvEmployerName1" runat="server" ControlToValidate="txtEmployerName1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 1: Employer name is blank."
                                                                    Display="None" ValidationGroup="Employer1Validation" EnableClientScript="false"
                                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblAddress1_1Label" runat="server" Text="Address" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAddress1_1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvAddress1_1" runat="server" ControlToValidate="txtAddress1_1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 1: Address is blank."
                                                                    Display="None" EnableClientScript="false" ValidationGroup="Employer1Validation"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAddress1_2" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCity1Label" runat="server" Text="City" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtCity1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCity1" runat="server" ControlToValidate="txtCity1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 1: City is blank."
                                                                    Display="None" ValidationGroup="Employer1Validation" EnableClientScript="false"
                                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvince1Label" runat="server" Text="Province" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince1" runat="server" ControlToValidate="ddlProvince1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Province is blank."
                                                                    Display="None" EnableClientScript="false" Text="*" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPostalCode1Label" runat="server" Text="Postal Code" />&nbsp*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtPostalCode1" runat="server" Columns="10" />
                                                                <ajaxToolkit:MaskedEditExtender ID="meePostalCode1" TargetControlID="txtPostalCode1"
                                                                Mask="LNLNLN" ClearMaskOnLostFocus="false" MaskType="None" ErrorTooltipEnabled="True" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvPostalCode1" runat="server" ControlToValidate="txtPostalCode1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 1: Postal Code is blank."
                                                                    Display="None" ValidationGroup="Employer1Validation" EnableClientScript="false"
                                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revPostalCode1" runat="server" EnableClientScript="false"
                                                                    ControlToValidate="txtPostalCode1" SetFocusOnError="True" ErrorMessage="Employer 1: Please provide a valid postal code without any spaces"
                                                                    Display="None" ValidationExpression="^([abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][a-zA-Z][0-9][a-zA-Z][0-9])$"
                                                                    ValidationGroup="Employer1Validation"  Text="*"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPostalCodeReflectPractice1Label" runat="server" Text="Does the postal code reflect site of practice?" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn" style="vertical-align: top;">
                                                                <asp:DropDownList ID="ddlPostalCodeReflectPractice1" runat="server" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlPostalCodeReflectPractice1SelectedIndexChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvPostalCodeReflectPractice1" runat="server" ControlToValidate="ddlPostalCodeReflectPractice1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 1: Does the postal code reflect site of practice?"
                                                                    Display="None" ValidationGroup="Employer1Validation" EnableClientScript="false"
                                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry1Label" runat="server" Text="Country" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry1" runat="server" ControlToValidate="ddlCountry1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Country is blank."
                                                                    Display="None" ForeColor="Red" EnableClientScript="false" />
                                                                <div id="message" class="errMessage" runat="server" style="display: none">
                                                                    You answered Yes to the question 'Does the postal code reflect site of practice?'.  Please note that the country field must be Canada.
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblTelephone1Label" runat="server" Text="Telephone" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtTelephone1" runat="server" Columns="30" />
                                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtTelephone1"
                                                                    Mask="(999) 999-9999 x9999999999" MaskType="None" ClearMaskOnLostFocus="false" InputDirection="LeftToRight"
                                                                    ErrorTooltipEnabled="True" runat="server" Filtered="xX" />
                                                                <asp:RequiredFieldValidator ID="rfvTelephone1" runat="server" ControlToValidate="txtTelephone1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 1: Phone is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer1Validation" EnableClientScript="false" Text="*"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revTelephone1" runat="server" ControlToValidate="txtTelephone1" ErrorMessage="Please provide all information for Employer 1: Phone number is invalid." Display="None"
                                                                    EnableClientScript="false" Text="*" ValidationGroup="Employer1Validation" ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}( )?([x]?(\d+)?)?$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblFax1Label" runat="server" Text="Fax"  />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtFax1" runat="server" Columns="30" />
                                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtFax1"
                                                                    Mask="(999) 999-9999" ClearMaskOnLostFocus="false" Filtered="xX" MaskType="None"
                                                                    OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" InputDirection="LeftToRight"
                                                                    ErrorTooltipEnabled="True" runat="server" />
                                                                <asp:RegularExpressionValidator ID="revFax1" runat="server" ControlToValidate="txtFax1" ErrorMessage="Please provide all information for Employer 1: Fax number is invalid." Display="None"
                                                                    EnableClientScript="false" Text="*" ValidationGroup="Employer1Validation" ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="RightColumn">
                                                                <span>
                                                                    Please complete this information if it is known. If you do not currently know this information please proceed to the next page.  
                                                                    Once you are registered with the College and begin working, you will be required to log in to the member section of the College website 
                                                                    to provide complete employment details within 30 days.  
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblEmploymentRelationship1Label" runat="server" Text="Employment Relationship" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlEmploymentRelationship1" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvEmploymentRelationship1" runat="server" ControlToValidate="ddlEmploymentRelationship1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Employment Category is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCasualStatus1Label" runat="server" Text="Full / Part Time/ Casual Status" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCasualStatus1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCasualStatus1SelectedIndexChanged"/>
                                                                <%--<asp:RequiredFieldValidator ID="rfvCasualStatus1" runat="server" ControlToValidate="ddlCasualStatus1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Full / Part Time Status is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblAverageWeeklyHours1Label" runat="server" Text="Average Weekly Hours (no ranges)" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAverageWeeklyHours1" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvAverageWeeklyHours1" runat="server" ControlToValidate="txtAverageWeeklyHours1"
                                                                    ForeColor="Red" ErrorMessage="Please provide all information for Employer 1: Average Weekly Hours is blank."
                                                                    Display="None" ValidationGroup="Employer1Validation" Text="*" EnableClientScript="false" />--%>
                                                                <asp:RegularExpressionValidator ID="revAverageWeeklyHours1" runat="server" ControlToValidate="txtAverageWeeklyHours1"
                                                                    ForeColor="Red" ErrorMessage="Please enter a value greater than zero for Average Weekly Hours Employment 1. 
                                                        Please to be sure this is an integer number (i.e. '25', and not a range (i.e. '15 to 25')." Display="None"
                                                                    ValidationExpression="^([0-9]*)$" ValidationGroup="Employer1Validation"
                                                                    Text="*"></asp:RegularExpressionValidator>
                                                                    <asp:RangeValidator ID="rvAverageWeeklyHours1" runat="server" MaximumValue="168" MinimumValue="0"  ErrorMessage="Maximum number of hours in a week is 168" ValidationGroup="Employer1Validation"
                                                                    ControlToValidate="txtAverageWeeklyHours1" Display="None" ForeColor="Red" Type="Integer" Text="*" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPrimaryRole1Label" runat="server" Text="Primary Role" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPrimaryRole1" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvPrimaryRole1" runat="server" ControlToValidate="ddlPrimaryRole1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: Position is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPracticeSetting1Label" runat="server" Text="Practice Setting" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPracticeSetting1" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvPracticeSetting1" runat="server" ControlToValidate="ddlPracticeSetting1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: EMPLOYMENT TYPE is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblMajorServices1Label" runat="server" Text="Major Services" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlMajorServices1" runat="server"  AutoPostBack="true" OnSelectedIndexChanged="ddlMajorServices1SelectedIndexChanged" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvMajorServices1" runat="server" ControlToValidate="ddlMajorServices1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: MAJOR SERVICE is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr id="trHealthCondition1" runat="server" visible="false">
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblHealthCondition1Label" runat="server" Text="Health Condition" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlHealthCondition1" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvHealthCondition1" runat="server" ControlToValidate="ddlHealthCondition1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please select a Health Condition for Employer 1."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblClientAgeRange1Label" runat="server" Text="Client Age Range" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlClientAgeRange1" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvClientAgeRange1" runat="server" ControlToValidate="ddlClientAgeRange1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: CLIENT AGE RANGE is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblFundingSource1Label" runat="server" Text="Funding Source" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlFundingSource1" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvFundingSource1" runat="server" ControlToValidate="ddlFundingSource1"
                                                                    InitialValue="" ValidationGroup="Employer1Validation" ErrorMessage="Please provide all information for Employer 1: FUNDING SOURCE is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                            <ajaxToolkit:TabPanel ID="tpEmpl2" runat="server" HeaderText="SECONDARY SITE"
                                                ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table style="width: 100%; padding: 2px; border-spacing: 4px">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblStatus2Title" runat="server" Text="Status" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlStatus2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus2SelectedIndexChanged" />
                                                                <asp:Label ID="lblStatus2Message" runat="server" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblStartDate2Label" runat="server" Text="Start Date (MM/DD/YYYY)" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtStartDate2" runat="server" />
                                                                <%--<rjs:PopCalendar ID="PopCalendar3" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtStartDate2" From-Date="1950-01-01" 
                                                                    MessageAlignment="RightCalendarControl"  OnSelectionChanged="PopCalendar1_SelectionChanged" />--%>
                                                                <asp:Image ID="imgStartDate2" runat="server" ImageUrl="~/Images/calen_25.png" style="vertical-align: middle;" />
                                                                <ajax:CalendarExtender ID="ceStartDate2" runat="server" TargetControlID="txtStartDate2" Format="MM/dd/yyyy" StartDate="01/01/1950" PopupButtonID="imgStartDate2" />
                                                                <asp:RequiredFieldValidator ID="rfvStartDate2" runat="server" ControlToValidate="txtStartDate2"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 2: Start Date is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer2Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblEndDate2Label" runat="server" Text="End Date (MM/DD/YYYY)" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtEndDate2" runat="server" />
                                                                <%--<rjs:PopCalendar ID="PopCalendar4" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtEndDate2" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />--%>
                                                                <asp:Image ID="imgEndDate2" runat="server" ImageUrl="~/Images/calen_25.png" style="vertical-align: middle;"/>
                                                                <ajax:CalendarExtender ID="ceEndDate2" runat="server" TargetControlID="txtEndDate2" Format="MM/dd/yyyy" StartDate="01/01/1950" PopupButtonID="imgEndDate2" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: top" class="LeftTitle">
                                                                <asp:Label ID="lblEmployerName2Label" runat="server" Text="Employer Name" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtEmployerName2" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                                <asp:RequiredFieldValidator ID="rfvEmployerName2" runat="server" ControlToValidate="txtEmployerName2"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 2: Employer name is blank."
                                                                    Display="None" ValidationGroup="Employer2Validation" ForeColor="Red" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblAddress2_1Label" runat="server" Text="Address" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAddress2_1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvAddress2_1" runat="server" ControlToValidate="txtAddress2_1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 2: Address is blank."
                                                                    Display="None" ValidationGroup="Employer2Validation" ForeColor="Red" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAddress2_2" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCity2Label" runat="server" Text="City" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtCity2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCity2" runat="server" ControlToValidate="txtCity2"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 2: City is blank."
                                                                    Display="None" ValidationGroup="Employer2Validation" ForeColor="Red" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvince2Label" runat="server" Text="Province" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince2" runat="server" ControlToValidate="ddlProvince2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPostalCode2Label" runat="server" Text="Postal Code" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtPostalCode2" runat="server" Columns="10" />
                                                                <ajaxToolkit:MaskedEditExtender ID="mmePostalCode2" TargetControlID="txtPostalCode2"
                                                                Mask="LNLNLN" ClearMaskOnLostFocus="false" MaskType="None" ErrorTooltipEnabled="True" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvPostalCode2" runat="server" ControlToValidate="txtPostalCode2"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 2: Postal Code is blank."
                                                                    Display="None" ValidationGroup="Employer2Validation" ForeColor="Red" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revPostalCode2" runat="server" EnableClientScript="false"
                                                                    ControlToValidate="txtPostalCode2" SetFocusOnError="True" ErrorMessage="Employer 2: Please provide a valid postal code without any spaces"
                                                                    Display="None" ValidationExpression="^([abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][a-zA-Z][0-9][a-zA-Z][0-9])$"
                                                                    ValidationGroup="Employer2Validation" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPostalCodeReflectPractice2Label" runat="server" Text="Does the postal code reflect site of practice?" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn" style="vertical-align: top;">
                                                                <asp:DropDownList ID="ddlPostalCodeReflectPractice2" runat="server" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlPostalCodeReflectPractice2SelectedIndexChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvPostalCodeReflectPractice2" runat="server" ControlToValidate="ddlPostalCodeReflectPractice2"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 2: Does the postal code reflect site of practice?"
                                                                    Display="None" ValidationGroup="Employer2Validation" EnableClientScript="false"
                                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry2Label" runat="server" Text="Country" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry2" runat="server" ControlToValidate="ddlCountry2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Country is blank."
                                                                    Display="None" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblTelephone2Label" runat="server" Text="Telephone" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtTelephone2" runat="server" Columns="30" />
                                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3" TargetControlID="txtTelephone2"
                                                                    Mask="(999) 999-9999 x9999999999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                    OnInvalidCssClass="MaskedEditError" ClearMaskOnLostFocus="false" InputDirection="LeftToRight"
                                                                    ErrorTooltipEnabled="True" runat="server" Filtered="xX" MaskType="None"/>
                                                                <asp:RequiredFieldValidator ID="rfvTelephone2" runat="server" ControlToValidate="txtTelephone2"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 2: Phone is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer2Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revTelephone2" runat="server" ControlToValidate="txtTelephone2" ErrorMessage="Please provide all information for Employer 2: Phone number is invalid." Display="None"
                                                                EnableClientScript="false" Text="*" ValidationGroup="Employer2Validation" ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}( )?([x]?(\d+)?)?$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblFax2Label" runat="server" Text="Fax" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtFax2" runat="server" Columns="30" />
                                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender4" TargetControlID="txtFax2"
                                                                    Mask="(999) 999-9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                    OnInvalidCssClass="MaskedEditError" ClearMaskOnLostFocus="false" InputDirection="LeftToRight"
                                                                    ErrorTooltipEnabled="True" runat="server" Filtered="xX" MaskType="None"/>
                                                                <asp:RegularExpressionValidator ID="revFax2" runat="server" ControlToValidate="txtFax2" ErrorMessage="Please provide all information for Employer 2: Fax number is invalid." Display="None"
                                                                EnableClientScript="false" Text="*" ValidationGroup="Employer2Validation" ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="RightColumn">
                                                                <span>
                                                                    Please complete this information if it is known. If you do not currently know this information please proceed to the next page. 
                                                                    Once you are registered with the College and begin working, you will be required to log into the member section of the College 
                                                                    website to provide complete details.  
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblEmploymentRelationship2Label" runat="server" Text="Employment Relationship" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlEmploymentRelationship2" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvEmploymentRelationship2" runat="server" ControlToValidate="ddlEmploymentRelationship2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Employment Category is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCasualStatus2Label" runat="server" Text="Full / Part Time/ Casual Status" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCasualStatus2" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvCasualStatus2" runat="server" ControlToValidate="ddlCasualStatus2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Full / Part Time Status is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblAverageWeeklyHours2Label" runat="server" Text="Average Weekly Hours (no ranges)" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAverageWeeklyHours2" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvAverageWeeklyHours2" runat="server" ControlToValidate="txtAverageWeeklyHours2"
                                                                    ForeColor="Red" ErrorMessage="Please provide all information for Employer 2: Average Weekly Hours is blank."
                                                                    Display="None" ValidationGroup="Employer2Validation" Text="*" EnableClientScript="false" />--%>
                                                                <asp:RegularExpressionValidator ID="revAverageWeeklyHours2" runat="server" ControlToValidate="txtAverageWeeklyHours2"
                                                                    ForeColor="Red" ErrorMessage="Please enter a value greater than zero for Average Weekly Hours Employment 2. 
                                                                    Please to be sure this is an integer number (i.e. '25', and not a range (i.e. '15 to 25')." Display="None"
                                                                    ValidationExpression="^([0-9]*)$" ValidationGroup="Employer2Validation"
                                                                    Text="*" EnableClientScript="false"></asp:RegularExpressionValidator>
                                                                <asp:RangeValidator ID="rvAverageWeeklyHours2" runat="server" MaximumValue="168" MinimumValue="0"  ErrorMessage="Maximum number of hours in a week is 168" ValidationGroup="Employer2Validation"
                                                                    ControlToValidate="txtAverageWeeklyHours2" Display="None" ForeColor="Red" Type="Integer" Text="*" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPrimaryRole2Label" runat="server" Text="Primary Role" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPrimaryRole2" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvPrimaryRole2" runat="server" ControlToValidate="ddlPrimaryRole2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: Position is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPracticeSetting2Label" runat="server" Text="Practice Setting" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPracticeSetting2" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvPracticeSetting2" runat="server" ControlToValidate="ddlPracticeSetting2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: EMPLOYMENT TYPE is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblMajorServices2Label" runat="server" Text="Major Services" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlMajorServices2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMajorServices2SelectedIndexChanged"/>
                                                                <%--<asp:RequiredFieldValidator ID="rfvMajorServices2" runat="server" ControlToValidate="ddlMajorServices2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: MAJOR SERVICE is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr id="trHealthCondition2" runat="server" visible="false">
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblHealthCondition2Label" runat="server" Text="Health Condition" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlHealthCondition2" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvHealthCondition2" runat="server" ControlToValidate="ddlHealthCondition2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please select a Health Condition for Employer 2."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblClientAgeRange2Label" runat="server" Text="Client Age Range" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlClientAgeRange2" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvClientAgeRange2" runat="server" ControlToValidate="ddlClientAgeRange2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: CLIENT AGE RANGE is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblFundingSource2Label" runat="server" Text="Funding Source" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlFundingSource2" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvFundingSource2" runat="server" ControlToValidate="ddlFundingSource2"
                                                                    InitialValue="" ValidationGroup="Employer2Validation" ErrorMessage="Please provide all information for Employer 2: FUNDING SOURCE is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                            <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="TERTIARY SITE"
                                                ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table style="width: 100%; padding: 2px; border-spacing: 4px">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblStatus3Title" runat="server" Text="Status" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlStatus3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus3SelectedIndexChanged" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblStartDate3Label" runat="server" Text="Start Date (MM/DD/YYYY)" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtStartDate3" runat="server" />
                                                                <%--<rjs:PopCalendar ID="PopCalendar5" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtStartDate3" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" From-Date="1950-01-01" />--%>
                                                                <asp:Image ID="imgStartDate3" runat="server" ImageUrl="~/Images/calen_25.png" style="vertical-align: middle;" />
                                                                <ajax:CalendarExtender ID="ceStartDate3" runat="server" TargetControlID="txtStartDate3" Format="MM/dd/yyyy" StartDate="01/01/1950" PopupButtonID="imgStartDate3" />
                                                                <asp:RequiredFieldValidator ID="rfvStartDate3" runat="server" ControlToValidate="txtStartDate3"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 3: Start Date is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer3Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblEndDate3Label" runat="server" Text="End Date (MM/DD/YYYY)" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtEndDate3" runat="server" />
                                                                <%--<rjs:PopCalendar ID="PopCalendar6" runat="server" AutoPostBack="False" BlankFieldText="dd/mm/yyyy"
                                                                    Format="dd/mm/yyyy" Separator="/" Control="txtEndDate3" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />--%>
                                                                <asp:Image ID="imgEndDate3" runat="server" ImageUrl="~/Images/calen_25.png" style="vertical-align: middle;" />
                                                                <ajax:CalendarExtender ID="ceEndDate3" runat="server" TargetControlID="txtEndDate3" Format="MM/dd/yyyy" StartDate="01/01/1950" PopupButtonID="imgEndDate3" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: top;" class="LeftTitle">
                                                                <asp:Label ID="lblEmployerName3Label" runat="server" Text="Employer Name" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtEmployerName3" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                                <asp:RequiredFieldValidator ID="rfvEmployerName3" runat="server" ControlToValidate="txtEmployerName3"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 3: Employer name is blank."
                                                                    Display="None" ValidationGroup="Employer3Validation" ForeColor="Red" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblAddress3_1Label" runat="server" Text="Address" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAddress3_1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvAddress3_1" runat="server" ControlToValidate="txtAddress3_1"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 3: Address is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer3Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAddress3_2" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCity3Label" runat="server" Text="City" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtCity3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCity3" runat="server" ControlToValidate="txtCity3"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 3: City is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer3Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvince3Label" runat="server" Text="Province" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince3" runat="server" ControlToValidate="ddlProvince3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPostalCode3Label" runat="server" Text="Postal Code" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtPostalCode3" runat="server" Columns="10" />
                                                                <ajaxToolkit:MaskedEditExtender ID="meePostalCode3" TargetControlID="txtPostalCode3"
                                                                Mask="LNLNLN" ClearMaskOnLostFocus="false" MaskType="None" ErrorTooltipEnabled="True" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvPostalCode3" runat="server" ControlToValidate="txtPostalCode3"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 3: Postal Code is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer3Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revPostalCode3" runat="server" EnableClientScript="false"
                                                                    ControlToValidate="txtPostalCode3" SetFocusOnError="True" ErrorMessage="Employer 3: Please provide a valid postal code without any spaces"
                                                                    Display="None" ValidationExpression="^([abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][a-zA-Z][0-9][a-zA-Z][0-9])$"
                                                                    ValidationGroup="Employer3Validation" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPostalCodeReflectPractice3Label" runat="server" Text="Does the postal code reflect site of practice?" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn" style="vertical-align: top;">
                                                                <asp:DropDownList ID="ddlPostalCodeReflectPractice3" runat="server" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlPostalCodeReflectPractice3SelectedIndexChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvPostalCodeReflectPractice3" runat="server" ControlToValidate="ddlPostalCodeReflectPractice3"
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 3: Does the postal code reflect site of practice?"
                                                                    Display="None" ValidationGroup="Employer3Validation" EnableClientScript="false"
                                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry3Label" runat="server" Text="Country" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry3" runat="server" ControlToValidate="ddlCountry3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: Country is blank."
                                                                    Display="None" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblTelephone3Label" runat="server" Text="Telephone" />&nbsp;*
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtTelephone3" runat="server" Columns="30" />
                                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender5" TargetControlID="txtTelephone3"
                                                                    Mask="(999) 999-9999 x9999999999" MaskType="None" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                    OnInvalidCssClass="MaskedEditError" ClearMaskOnLostFocus="false" InputDirection="LeftToRight" 
                                                                    runat="server" Filtered="xX" />
                                                                <asp:RequiredFieldValidator ID="rfvTelephone3" runat="server" ControlToValidate="txtTelephone3"  
                                                                    SetFocusOnError="True" ErrorMessage="Please provide all information for Employer 3: Phone is blank."
                                                                    Display="None" ForeColor="Red" ValidationGroup="Employer3Validation" EnableClientScript="false"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revTelephone3" runat="server" ControlToValidate="txtTelephone3" ErrorMessage="Please provide all information for Employer 3: Phone number is invalid." Display="None"
                                                                EnableClientScript="false" Text="*" ValidationGroup="Employer3Validation" ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}( )?([x]?(\d+)?)?$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblFax3Label" runat="server" Text="Fax" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtFax3" runat="server" Columns="30" />
                                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender6" TargetControlID="txtFax3"
                                                                    Mask="(999) 999-9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                                    OnInvalidCssClass="MaskedEditError" ClearMaskOnLostFocus="false" InputDirection="LeftToRight"
                                                                    ErrorTooltipEnabled="True" runat="server" Filtered="xX" MaskType="None"/>
                                                                <asp:RegularExpressionValidator ID="revFax3" runat="server" ControlToValidate="txtFax3" ErrorMessage="Please provide all information for Employer 3: Fax number is invalid." Display="None"
                                                                EnableClientScript="false" Text="*" ValidationGroup="Employer3Validation" ValidationExpression="^(\(?[1-9]{1}\d\d\)?)?( |-|\.)?\d\d\d( |-|\.)?\d{4,4}$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="RightColumn">
                                                                <span>
                                                                    Please complete this information if it is known. If you do not currently know this information please proceed to the next page. 
                                                                    Once you are registered with the College and begin working, you will be required to log into the member section of the College 
                                                                    website to provide complete details.
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblEmploymentRelationship3Label" runat="server" Text="Employment Relationship" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlEmploymentRelationship3" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvEmploymentRelationship3" runat="server" ControlToValidate="ddlEmploymentRelationship3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: Employment Category is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCasualStatus3Label" runat="server" Text="Full / Part Time/ Casual Status" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCasualStatus3" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvCasualStatus3" runat="server" ControlToValidate="ddlCasualStatus3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 2: Full / Part Time Status is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblAverageWeeklyHours3Label" runat="server" Text="Average Weekly Hours (no ranges)" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtAverageWeeklyHours3" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvAverageWeeklyHours3" runat="server" ControlToValidate="txtAverageWeeklyHours3"
                                                                    ForeColor="Red" ErrorMessage="Please provide all information for Employer 3: Average Weekly Hours is blank."
                                                                    Display="None" ValidationGroup="Employer3Validation" Text="*" EnableClientScript="false" />--%>
                                                                <asp:RegularExpressionValidator ID="revAverageWeeklyHours3" runat="server" ControlToValidate="txtAverageWeeklyHours3"
                                                                    ForeColor="Red" ErrorMessage="Please enter a value greater than zero for Average Weekly Hours Employment 3. 
                                                        Please to be sure this is an integer number (i.e. '25', and not a range (i.e. '15 to 25')." Display="None"
                                                                    ValidationExpression="^([0-9]*)$" ValidationGroup="Employer3Validation"
                                                                    Text="*" EnableClientScript="false"></asp:RegularExpressionValidator>
                                                                <asp:RangeValidator ID="rvAverageWeeklyHours3" runat="server" MaximumValue="168" MinimumValue="0"  ErrorMessage="Maximum number of hours in a week is 168" ValidationGroup="Employer3Validation"
                                                                    ControlToValidate="txtAverageWeeklyHours3" Display="None" ForeColor="Red" Type="Integer" Text="*" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPrimaryRole3Label" runat="server" Text="Primary Role" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPrimaryRole3" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvPrimaryRole3" runat="server" ControlToValidate="ddlPrimaryRole3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: Position is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblPracticeSetting3Label" runat="server" Text="Practice Setting" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlPracticeSetting3" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvPracticeSetting3" runat="server" ControlToValidate="ddlPracticeSetting3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: EMPLOYMENT TYPE is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblMajorServices3Label" runat="server" Text="Major Services" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlMajorServices3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMajorServices3SelectedIndexChanged" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvMajorServices3" runat="server" ControlToValidate="ddlMajorServices3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: MAJOR SERVICE is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr id="trHealthCondition3" runat="server" visible="false">
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblHealthCondition3Label" runat="server" Text="Health Condition" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlHealthCondition3" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvHealthCondition3" runat="server" ControlToValidate="ddlHealthCondition3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please select a Health Condition for Employer 3."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblClientAgeRange3Label" runat="server" Text="Client Age Range" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlClientAgeRange3" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvClientAgeRange3" runat="server" ControlToValidate="ddlClientAgeRange3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: CLIENT AGE RANGE is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblFundingSource3Label" runat="server" Text="Funding Source" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlFundingSource3" runat="server" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvFundingSource3" runat="server" ControlToValidate="ddlFundingSource3"
                                                                    InitialValue="" ValidationGroup="Employer3Validation" ErrorMessage="Please provide all information for Employer 3: FUNDING SOURCE is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                        </ajaxToolkit:TabContainer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right;">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" UseSubmitBehavior="true" TabIndex="0"/>--%>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="errorsPanel" runat="server"  Style="display: none; width: 750px;" CssClass="modalPopup">
                    <table style="width: 100%; border-spacing: 2px!important; padding: 2px!important">
                        <tr class="topHandleRed">
                            <td style="text-align: left;" runat="server" id="tdCaption">
                                <asp:Label ID="lblCaption" runat="server" Text="Error Messages:" ></asp:Label>
                            </td>
                        </tr>
                        <tr class="midHandle">
                            <td>
                                <div style="text-align: left">
                                    <asp:Label ID="lblErrorMessage" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary4" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="GeneralValidation" />
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Employer1Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Employer2Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary3" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Employer3Validation" />
                                    <div style="text-align: right">
                                        <asp:Button ID="okBtn" runat="server" Text="Ok" OnClick="okBtn_Click" CssClass="errorButton" /></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="modalPopupEx" runat="server" PopupControlID="errorsPanel"
                    TargetControlID="hfInvisibleTarget" CancelControlID="okBtn" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>
                <asp:HiddenField ID="hfInvisibleTarget" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>