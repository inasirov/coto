﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlProfessionalHistory.ascx.cs" Inherits="COTO_RegOnly.Application.UserControls.ctlProfessionalHistory" %>

<%@ import Namespace="COTO_RegOnly.Classes" %>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
    
    <style type="text/css">
        body
        {
            font-family: Arial, Helvetica, sans-serif;
            /*font-size: 13px;*/
        }
        .errMessage
        {
            width:300px;
            border: 1px solid;
            margin: 10px 0px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            position: relative;
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('images/info.png');
        }
    </style>
    
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<div class="MainForm">
    <center>
        <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="border: 0px; width: 100%; padding: 0px; border-spacing: 2px; box-sizing: content-box;">
                    <tr class="HeaderTitle">
                        <td class="HeaderTitle" style="text-align: right;">
                            <asp:Label ID="lblPageTitleLabel" runat="server" Text="Registration Application Step 6 of 11" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <uc:MessageBox ID="omb" runat="server" />
                        </td>
                    </tr>
                    <tr class="RowTitle">
                        <td>
                            <div>
                                <asp:Label ID="lblPersonalEmploymentInformationTitle" runat="server" CssClass="heading"
                                Text="Professional Registration" />
                            </div>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" style="width: 100%;">
                                <tr>
                                    <td style="text-align: left;">
                                        <h3>Occupational Therapy Registration in Canada and Elsewhere</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;">
                                        <p>
                                            Please fill in the required fields if you’ve ever been registered to practise as an occupational therapist anywhere other than Ontario.&nbsp;
                                            <a href='<%= Classes.WebConfigItems.Application_Step6_RegulatoryForm %>' target="_blank">A Regulatory History Form</a>
                                            &nbsp;must be completed by each regulatory body and sent directly to the College.
                                        </p>
                                        <p>
                                            Information on professional memberships is not permitted (e.g. OSOT, CAOT, etc.).
                                        </p>
                                        <p>
                                            If you hold or have held registration with a regulatory body that is not linked to any one state or province indicate where the regulatory body holds an office or choose ‘not applicable’.
                                        </p>
                                        <p>
                                            If you do not see your regulatory body on the list, please send us an email at <a href='mailto:application@coto.org' target="_blank">application@coto.org</a>.  In the email, be sure to include your name, applicant number, the name of the regulatory body and the country where you are/were registered/licensed.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RightColumn">
                                        <ajaxToolkit:TabContainer ID="tcMainContainer" CssClass="NewsTab" runat="server" CssTheme="None"
                                            AutoPostBack="false" TabStripPlacement="Top" ScrollBars="Auto" ActiveTabIndex="0">
                                            <ajaxToolkit:TabPanel ID="tpJur1" runat="server" HeaderText="Jurisdiction 1" ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table style="width: 100%; padding: 2px; border-spacing: 4px;">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblRegBody1Title" runat="server" Text="Regulatory Body " />
                                                                <asp:HiddenField ID="hfPracticeSeqn1" runat="server" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlRegBody1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRegBody1_SelectedIndexChanged" style="max-width: 450px;" />
                                                            </td>
                                                        </tr>
                                                        <tr id="trRegBodyOther1" runat="server" visible="false">
                                                            <td style="vertical-align: top;" class="LeftTitle">
                                                                <asp:Label ID="lblRegBodyOther1Title" runat="server" Text="Regulatory Body Other " />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegBodyOther1" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvinceState1Label" runat="server" Text="Province/State" />&nbsp;
                                                                
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince1" runat="server" ControlToValidate="ddlProvince1"
                                                                    InitialValue="" ValidationGroup="Jurisdiction1Validation" ErrorMessage="Please provide all information for Jurisdiction 1: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry1Label" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry1" runat="server" ControlToValidate="ddlCountry1"
                                                                    InitialValue="" ValidationGroup="Jurisdiction1Validation" ErrorMessage="Please provide all information for Jurisdiction 1: Country is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblLicense1Label" runat="server" Text="Registration/License #" />&nbsp;
                                                            &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtLicense1" runat="server" Columns="12" MaxLength="12" />
                                                                <asp:RequiredFieldValidator ID="rfvLicense1" runat="server" ControlToValidate="txtLicense1" EnableClientScript="false"
                                                                   ValidationGroup="Jurisdiction1Validation" ErrorMessage="Please provide all information for Jurisdiction 1: Registration/License # is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblRegStatus1Title" runat="server" Text="Registration Status" />&nbsp;
                                                            &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegStatus1" runat="server" Columns="60" MaxLength="100" />
                                                                <asp:RequiredFieldValidator ID="rfvRegStatus1" runat="server" ControlToValidate="txtRegStatus1" EnableClientScript="false"
                                                                   ValidationGroup="Jurisdiction1Validation" ErrorMessage="Please provide all information for Jurisdiction 1: Registration status is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblInitMonth1Title" runat="server" Text="Initial Month of Registration" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlInitMonth1" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblInitYear1Title" runat="server" Text="Initial Year of Registration" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlInitYear1" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblExpiryDate1Label" runat="server" Text="Expiry Date (MM/DD/YYYY)" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtExpiryDate1" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar1" runat="server" AutoPostBack="False" Culture="en-US"
                                                                    Format="mm dd yyyy" Separator="/" Control="txtExpiryDate1" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvExpiryDate1" runat="server" ControlToValidate="txtExpiryDate1" EnableClientScript="false"
                                                                    ValidationGroup="Jurisdiction1Validation" ErrorMessage="Please provide all information for Jurisdiction 1: Expiry Date is blank." Display="None" ForeColor="Red" />
                                                                &nbsp;
                                                                <asp:CheckBox ID="cbNoExpiration1" runat="server" Text="No Expiration" OnCheckedChanged="cbNoExpiration1CheckedChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                            <ajaxToolkit:TabPanel ID="tpJur2" runat="server" HeaderText="Jurisdiction 2"
                                                ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table style="width: 100%; padding: 2px; border-spacing: 4px;">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblRegBody2Title" runat="server" Text="Regulatory Body " />
                                                                <asp:HiddenField ID="hfPracticeSeqn2" runat="server" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlRegBody2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRegBody2_SelectedIndexChanged" style="max-width: 450px;"/>
                                                            </td>
                                                        </tr>
                                                        <tr  id="trRegBodyOther2" runat="server" visible="false">
                                                            <td style="vertical-align: top;" class="LeftTitle">
                                                                <asp:Label ID="lblRegBodyOther2" runat="server" Text="Regulatory Body Other " />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegBodyOther2" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvinceState2" runat="server" Text="Province/State" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince2" runat="server" ControlToValidate="ddlProvince2"
                                                                    InitialValue="" ValidationGroup="Jurisdiction2Validation" ErrorMessage="Please provide all information for Jurisdiction 2: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry2" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry2" runat="server" ControlToValidate="ddlCountry2"
                                                                    InitialValue="" ValidationGroup="Jurisdiction2Validation" ErrorMessage="Please provide all information for Jurisdiction 2: Country is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblLicense2" runat="server" Text="Registration/License #" />&nbsp;
                                                            &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtLicense2" runat="server" Columns="12" MaxLength="12" />
                                                                <asp:RequiredFieldValidator ID="rfvLicense2" runat="server" ControlToValidate="txtLicense2" EnableClientScript="false"
                                                                   ValidationGroup="Jurisdiction2Validation" ErrorMessage="Please provide all information for Jurisdiction 2: Registration/License # is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblRegStatus2Title" runat="server" Text="Registration Status" />&nbsp;
                                                            &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegStatus2" runat="server" Columns="60" MaxLength="100" />
                                                                <asp:RequiredFieldValidator ID="rfvRegStatus2" runat="server" ControlToValidate="txtRegStatus2" EnableClientScript="false"
                                                                   ValidationGroup="Jurisdiction2Validation" ErrorMessage="Please provide all information for Jurisdiction 2: Registration status is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblInitMonth2Title" runat="server" Text="Initial Month of Registration" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlInitMonth2" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblInitYear2Title" runat="server" Text="Initial Year of Registration" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlInitYear2" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblExpiryDate2" runat="server" Text="Expiry Date (MM/DD/YYYY)" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtExpiryDate2" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar2" runat="server" AutoPostBack="False" Culture="en-US"
                                                                    Format="mm dd yyyy" Separator="/" Control="txtExpiryDate2" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvExpiryDate2" runat="server" ControlToValidate="txtExpiryDate2" EnableClientScript="false"
                                                                    ValidationGroup="Jurisdiction2Validation" ErrorMessage="Please provide all information for Jurisdiction 2: Expiry Date is blank." Display="None" ForeColor="Red" />
                                                                &nbsp;
                                                                <asp:CheckBox ID="cbNoExpiration2" runat="server" Text="No Expiration" OnCheckedChanged="cbNoExpiration2CheckedChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                            <ajaxToolkit:TabPanel ID="tpJur3" runat="server" HeaderText="Jurisdiction 3"
                                                ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table style="width: 100%; padding: 2px; border-spacing: 4px;">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblRegBody3Title" runat="server" Text="Regulatory Body " />
                                                                <asp:HiddenField ID="hfPracticeSeqn3" runat="server" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlRegBody3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRegBody3_SelectedIndexChanged" style="max-width: 450px;" />
                                                            </td>
                                                        </tr>
                                                        <tr id="trRegBodyOther3" runat="server"  visible="false">
                                                            <td style="vertical-align: top" class="LeftTitle">
                                                                <asp:Label ID="lbllblRegBodyOther3Title" runat="server" Text="Regulatory Body Other " />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegBodyOther3" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvince3" runat="server" Text="Province/State" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince3" runat="server" ControlToValidate="ddlProvince3"
                                                                    InitialValue="" ValidationGroup="Jurisdiction3Validation" ErrorMessage="Please provide all information for Jurisdiction 3: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry3" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry3" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry3" runat="server" ControlToValidate="ddlCountry3"
                                                                    InitialValue="" ValidationGroup="Jurisdiction3Validation" ErrorMessage="Please provide all information for Jurisdiction 3: Country is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblLicense3" runat="server" Text="Registration/License #" />&nbsp;
                                                            &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtLicense3" runat="server" Columns="12" MaxLength="12" />
                                                                <asp:RequiredFieldValidator ID="rfvLicense3" runat="server" ControlToValidate="txtLicense3" EnableClientScript="false"
                                                                   ValidationGroup="Jurisdiction3Validation" ErrorMessage="Please provide all information for Jurisdiction 3: Registration/License # is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblRegStatus3Title" runat="server" Text="Registration Status" />&nbsp;
                                                            &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegStatus3" runat="server" Columns="60" MaxLength="100" />
                                                                <asp:RequiredFieldValidator ID="rfvRegStatus3" runat="server" ControlToValidate="txtRegStatus3" EnableClientScript="false"
                                                                   ValidationGroup="Jurisdiction3Validation" ErrorMessage="Please provide all information for Jurisdiction 3: Registration status is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblInitMonth3Title" runat="server" Text="Initial Month of Registration" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlInitMonth3" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblInitYear3Title" runat="server" Text="Initial Year of Registration" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlInitYear3" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblExpiryDate3" runat="server" Text="Expiry Date (MM/DD/YYYY)" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtExpiryDate3" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar3" runat="server" AutoPostBack="False" Culture="en-US"
                                                                    Format="mm dd yyyy" Separator="/" Control="txtExpiryDate3" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvExpiryDate3" runat="server" ControlToValidate="txtExpiryDate3" EnableClientScript="false"
                                                                    ValidationGroup="Jurisdiction3Validation" ErrorMessage="Please provide all information for Jurisdiction 3: Expiry Date is blank." Display="None" ForeColor="Red" />
                                                                &nbsp;
                                                                <asp:CheckBox ID="cbNoExpiration3" runat="server" Text="No Expiration" OnCheckedChanged="cbNoExpiration3CheckedChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                            <ajaxToolkit:TabPanel ID="tpJur4" runat="server" HeaderText="Jurisdiction 4"
                                                ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table style="width: 100%; padding: 2px; border-spacing: 4px;">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblRegBody4" runat="server" Text="Regulatory Body " />
                                                                <asp:HiddenField ID="hfPracticeSeqn4" runat="server" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlRegBody4" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRegBody4_SelectedIndexChanged" style="max-width: 450px;"/>
                                                            </td>
                                                        </tr>
                                                        <tr id="trRegBodyOther4" runat="server"  visible="false">
                                                            <td  style="vertical-align: top" class="LeftTitle">
                                                                <asp:Label ID="lblRegBodyOther4" runat="server" Text="Regulatory Body Other " />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegBodyOther4" runat="server" TextMode="MultiLine" Rows="3" Columns="50" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvince4" runat="server" Text="Province/State" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvince4" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvince4" runat="server" ControlToValidate="ddlProvince4"
                                                                    InitialValue="" ValidationGroup="Jurisdiction4Validation" ErrorMessage="Please provide all information for Jurisdiction 4: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountry4" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountry4" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountry4" runat="server" ControlToValidate="ddlCountry4"
                                                                    InitialValue="" ValidationGroup="Jurisdiction4Validation" ErrorMessage="Please provide all information for Jurisdiction 4: Country is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblLicense4" runat="server" Text="Registration/License #" />&nbsp;
                                                            &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtLicense4" runat="server" Columns="12" MaxLength="12" />
                                                                <asp:RequiredFieldValidator ID="rfvLicense4" runat="server" ControlToValidate="txtLicense4" EnableClientScript="false"
                                                                   ValidationGroup="Jurisdiction4Validation" ErrorMessage="Please provide all information for Jurisdiction 4: Registration/License # is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblRegStatus4Title" runat="server" Text="Registration Status" />&nbsp;
                                                            &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegStatus4" runat="server" Columns="60" MaxLength="100" />
                                                                <asp:RequiredFieldValidator ID="rfvRegStatus4" runat="server" ControlToValidate="txtRegStatus4" EnableClientScript="false"
                                                                   ValidationGroup="Jurisdiction4Validation" ErrorMessage="Please provide all information for Jurisdiction 4: Registration status is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblInitMonth4" runat="server" Text="Initial Month of Registration" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlInitMonth4" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblInitYear4" runat="server" Text="Initial Year of Registration" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlInitYear4" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblExpiryDate4" runat="server" Text="Expiry Date (MM/DD/YYYY)" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtExpiryDate4" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar4" runat="server" AutoPostBack="False" Culture="en-US"
                                                                    Format="mm dd yyyy" Separator="/" Control="txtExpiryDate4" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvExpiryDate4" runat="server" ControlToValidate="txtExpiryDate4" EnableClientScript="false"
                                                                    ValidationGroup="Jurisdiction4Validation" ErrorMessage="Please provide all information for Jurisdiction 4: Expiry Date is blank." Display="None" ForeColor="Red" />
                                                                &nbsp;
                                                                <asp:CheckBox ID="cbNoExpiration4" runat="server" Text="No Expiration" OnCheckedChanged="cbNoExpiration4CheckedChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                        </ajaxToolkit:TabContainer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <%--<tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>--%>
                    <tr>
                        <td style="text-align: left;">
                            <h3>Practice of Other Professions </h3>
                        </td>
                    </tr>
                    <%--<tr>
                        <td>&nbsp;</td>
                    </tr>--%>
                    <tr>
                        <td class="RightColumn">
                            <p>
                                Please fill in the required fields if you’ve ever been registered to practise a regulated profession other than occupational therapy in Ontario or elsewhere.&nbsp;
                                <a href='<%= Classes.WebConfigItems.Application_Step6_RegulatoryForm %>' target="_blank">A Regulatory History Form</a>
                                &nbsp;must be completed by each regulatory body and sent directly to the College.
                            </p>
                            <p>
                                Examples of other regulated professions include lawyers, kinesiologists, social workers, and engineers. For more examples, please see the drop-down menu.
                            </p>
                            <p>
                                If you do not see your profession on the list, please send us an email at <a href="mailto:application@coto.org" target="_blank">application@coto.org</a>. 
                                In the email, be sure to include your name, applicant number, name of the profession, regulatory body and country where you are/were registered/licensed.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="memberInfo" style="width: 100%;">
                                <tr>
                                    <td class="RightColumn">
                                        <ajaxToolkit:TabContainer ID="TabContainer1" CssClass="NewsTab" runat="server" CssTheme="None"
                                            AutoPostBack="false" TabStripPlacement="Top" ScrollBars="Auto" ActiveTabIndex="0">
                                            <ajaxToolkit:TabPanel ID="tpProf1" runat="server" HeaderText="Profession 1" ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table style="width: 100%; padding: 2px; border-spacing: 4px;">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblNameProfessionOP1" runat="server" Text="Name of Profession" />
                                                                <asp:HiddenField ID="hfPracticeSeqnOP1" runat="server" Value="0" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlNameProfessionOP1" runat="server" OnSelectedIndexChanged="ddlNameProfessionOP1_SelectedIndexChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                        <tr id="trNameProfessionOtherOP1" runat="server" visible="false">
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblNameProfessionOtherOP1" runat="server" Text="Other" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtNameProfessionOtherOP1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvNameProfessionOtherOP1" runat="server" ControlToValidate="txtNameProfessionOtherOP1" EnableClientScript="false"
                                                                   ValidationGroup="Profession1Validation" ErrorMessage="Please provide all information for Profession 1: Name Profession Other is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: top;" class="LeftTitle">
                                                                <asp:Label ID="lblRegBodyOP1" runat="server" Text="Regulatory Body" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlRegBodyOP1" runat="server" OnSelectedIndexChanged="ddlRegBodyOP1_SelectedIndexChanged" AutoPostBack="true" style="max-width: 420px" />
                                                                <asp:RequiredFieldValidator ID="rfvRegBodyOP1" runat="server" ControlToValidate="ddlRegBodyOP1" EnableClientScript="false"
                                                                   ValidationGroup="Profession1Validation" ErrorMessage="Please provide all information for Profession 1: Regulatory Body is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr id="trRegBodyOtherOP1" runat="server" visible="false">
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblRegBodyOtherOP1Title" runat="server" Text="Other" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegBodyOtherOP1" runat="server" TextMode="MultiLine" Rows="3" Columns="50"/>
                                                                <asp:RequiredFieldValidator ID="rfvRegBodyOtherOP1" runat="server" ControlToValidate="txtRegBodyOtherOP1" EnableClientScript="false"
                                                                   ValidationGroup="Profession1Validation" ErrorMessage="Please provide all information for Profession 1: Regulatory Body Other is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvinceOP1" runat="server" Text="Province/State" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvinceOP1" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvinceOP1" runat="server" ControlToValidate="ddlProvinceOP1"
                                                                    InitialValue="" ValidationGroup="Profession1Validation" ErrorMessage="Please provide all information for Profession 1: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountryOP1" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountryOP1" runat="server" />
                                                                 <asp:RequiredFieldValidator ID="rfvCountryOP1" runat="server" ControlToValidate="ddlCountryOP1"
                                                                    InitialValue="" ValidationGroup="Profession1Validation" ErrorMessage="Please provide all information for Profession 1: Country is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblLicenseOP1" runat="server" Text="Registration/License #" />&nbsp;
                                                            &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtLicenseOP1" runat="server" Columns="12" MaxLength="12" />
                                                                <asp:RequiredFieldValidator ID="rfvLicenseOP1" runat="server" ControlToValidate="txtLicenseOP1" EnableClientScript="false"
                                                                   ValidationGroup="Profession1Validation" ErrorMessage="Please provide all information for Profession 1: Registration/License # is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblRegStatusOP1Title" runat="server" Text="Registration Status" />&nbsp;
                                                            &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegStatusOP1" runat="server" Columns="60" MaxLength="100" />
                                                                <asp:RequiredFieldValidator ID="rfvRegStatusOP1" runat="server" ControlToValidate="txtRegStatusOP1" EnableClientScript="false"
                                                                   ValidationGroup="Profession1Validation" ErrorMessage="Please provide all information for Profession 1: Registration status is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblInitMonthOP1Title" runat="server" Text="Initial Month of Registration" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlInitMonthOP1" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblInitYearOP1" runat="server" Text="Initial Year of Registration" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlInitYearOP1" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblExpireDateOP1" runat="server" Text="Expiry Date (MM/DD/YYYY)" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtExpireDateOP1" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar5" runat="server" AutoPostBack="False" Culture="en-US"
                                                                    Format="mm dd yyyy" Separator="/" Control="txtExpireDateOP1" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvExpireDateOP1" runat="server" ControlToValidate="txtExpireDateOP1" EnableClientScript="false"
                                                                    ValidationGroup="Profession1Validation" ErrorMessage="Please provide all information for Profession 1: Expiry Date is blank." Display="None" ForeColor="Red" />
                                                                &nbsp;
                                                                <asp:CheckBox ID="cbNoExpirationOP1" runat="server" Text="No Expiration" OnCheckedChanged="cbNoExpirationOP1CheckedChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                            <ajaxToolkit:TabPanel ID="tpProf2" runat="server" HeaderText="Profession 2"
                                                ScrollBars="Auto">
                                                <ContentTemplate>
                                                    <table style="width: 100%; padding: 2px; border-spacing: 4px;">
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblNameProfessionOP2" runat="server" Text="Name Of Profession" />
                                                                <asp:HiddenField ID="hfPracticeSeqnOP2" runat="server" Value="0" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlNameProfessionOP2" runat="server" OnSelectedIndexChanged="ddlNameProfessionOP2_SelectedIndexChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                        <tr id="trNameProfessionOtherOP2" runat="server" visible="false">
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblNameProfessionOtherOP2Title" runat="server" Text="Other" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtNameProfessionOtherOP2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvNameProfessionOtherOP2" runat="server" ControlToValidate="txtNameProfessionOtherOP2" EnableClientScript="false"
                                                                   ValidationGroup="Profession2Validation" ErrorMessage="Please provide all information for Profession 2: Name Profession Other is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: top;" class="LeftTitle">
                                                                <asp:Label ID="lblRegBodyOP2" runat="server" Text="Regulatory Body" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlRegBodyOP2" runat="server" OnSelectedIndexChanged="ddlRegBodyOP2_SelectedIndexChanged" AutoPostBack="true" style="max-width: 420px"/>
                                                                <asp:RequiredFieldValidator ID="rfvRegBodyOP2" runat="server" ControlToValidate="ddlRegBodyOP2" EnableClientScript="false"
                                                                   ValidationGroup="Profession2Validation" ErrorMessage="Please provide all information for Profession 2: Regulatory Body is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr id="trRegBodyOtherOP2" runat="server" visible="false">
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblRegBodyOtherOP2" runat="server" Text="Other" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegBodyOtherOP2" runat="server" TextMode="MultiLine" Rows="3" Columns="50"/>
                                                                <asp:RequiredFieldValidator ID="rfvRegBodyOtherOP2" runat="server" ControlToValidate="txtRegBodyOtherOP2" EnableClientScript="false"
                                                                   ValidationGroup="Profession2Validation" ErrorMessage="Please provide all information for Profession 2: Regulatory Body Other is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblProvinceOP2" runat="server" Text="Province/State" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlProvinceOP2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvProvinceOP2" runat="server" ControlToValidate="ddlProvinceOP2"
                                                                    InitialValue="" ValidationGroup="Profession2Validation" ErrorMessage="Please provide all information for Profession 2: Province is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblCountryOP2" runat="server" Text="Country" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlCountryOP2" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvCountryOP2" runat="server" ControlToValidate="ddlCountryOP2"
                                                                    InitialValue="" ValidationGroup="Profession2Validation" ErrorMessage="Please provide all information for Profession 2: Country is blank."
                                                                    Display="None" Text="*" ForeColor="Red" EnableClientScript="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblLicenseOP2" runat="server" Text="Registration/License #" />&nbsp;
                                                            &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtLicenseOP2" runat="server" Columns="12" MaxLength="12" />
                                                                <asp:RequiredFieldValidator ID="rfvLicenseOP2" runat="server" ControlToValidate="txtLicenseOP2" EnableClientScript="false"
                                                                   ValidationGroup="Profession2Validation" ErrorMessage="Please provide all information for Profession 2: Registration/License # is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblRegStatusOP2Title" runat="server" Text="Registration Status" />&nbsp;
                                                            &nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtRegStatusOP2" runat="server" Columns="60" MaxLength="100" />
                                                                <asp:RequiredFieldValidator ID="rfvRegStatusOP2" runat="server" ControlToValidate="txtRegStatusOP2" EnableClientScript="false"
                                                                   ValidationGroup="Profession2Validation" ErrorMessage="Please provide all information for Profession 2: Registration status is blank."
                                                                   Display="None" ForeColor="Red" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblInitMonthOP2" runat="server" Text="Initial Month of Registration" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlInitMonthOP2" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle" style="width: 35%">
                                                                <asp:Label ID="lblInitYearOP2" runat="server" Text="Initial Year of Registration" />&nbsp;
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:DropDownList ID="ddlInitYearOP2" runat="server"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LeftTitle">
                                                                <asp:Label ID="lblExpireDateOP2" runat="server" Text="Expiry Date (MM/DD/YYYY)" />
                                                            </td>
                                                            <td class="RightColumn">
                                                                <asp:TextBox ID="txtExpireDateOP2" runat="server" />
                                                                <rjs:PopCalendar ID="PopCalendar6" runat="server" AutoPostBack="False" Culture="en-US"
                                                                    Format="mm dd yyyy" Separator="/" Control="txtExpireDateOP2" MessageAlignment="RightCalendarControl"
                                                                    OnSelectionChanged="PopCalendar1_SelectionChanged" />
                                                                <asp:RequiredFieldValidator ID="rfvExpireDateOP2" runat="server" ControlToValidate="txtExpireDateOP2" EnableClientScript="false"
                                                                    ValidationGroup="Profession2Validation" ErrorMessage="Please provide all information for Profession 2: Expiry Date is blank." Display="None" ForeColor="Red" />
                                                                &nbsp;
                                                                <asp:CheckBox ID="cbNoExpirationOP2" runat="server" Text="No Expiration" OnCheckedChanged="cbNoExpirationOP2CheckedChanged" AutoPostBack="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                        </ajaxToolkit:TabContainer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="ibtnBack2" runat="server" ImageUrl="~/Images/back_eReg.jpg" OnClick="ibtnBackClick" />&nbsp;&nbsp;
                            <asp:ImageButton ID="ibtnNext2" runat="server" ImageUrl="~/Images/btn_next_eReg.jpg" OnClick="ibtnNextClick" />
                            <%--<asp:Button ID="btnBack" CssClass="button" runat="server" Text="< Back" OnClick="btnBackClick" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" CssClass="button"  runat="server" Text="Next >" OnClick="btnUpdateClick" UseSubmitBehavior="true" TabIndex="0"/>--%>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="errorsPanel" runat="server"  Style="display: none; width: 750px;" CssClass="modalPopup" DefaultButton="okBtn">
                    <table style="width: 100%; padding: 2px; border-spacing: 2px;">
                        <tr class="topHandleRed">
                            <td align="left" runat="server" id="tdCaption">
                                <asp:Label ID="lblCaption" runat="server" Text="Error Messages:" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="text-align: left">
                                    <asp:Label ID="lblErrorMessage" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="GeneralValidation" />
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Jurisdiction1Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary3" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Jurisdiction2Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary4" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Jurisdiction3Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary5" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Jurisdiction4Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary6" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Profession1Validation" />
                                    <asp:ValidationSummary ID="ValidationSummary7" runat="server" DisplayMode="BulletList" ShowSummary="true"
                                        ValidationGroup="Profession2Validation" />
                                    <div style="text-align: right">
                                        <asp:Button ID="okBtn" runat="server" Text="Ok" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="modalPopupEx" runat="server" PopupControlID="errorsPanel"
                    TargetControlID="invisibleTarget" CancelControlID="okBtn" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Label ID="invisibleTarget" runat="server" Style="display: none" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</div>