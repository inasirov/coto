﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using System.Security.Cryptography;
using System.Text;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlApplicationCheckList : System.Web.UI.UserControl
    {

        #region Consts
        private string _Key = "Yz7!~3";

        private string PrevStep = WebConfigItems.Application_Step0;
        
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 1)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                //Response.Redirect("LoginFalse.aspx");
                Response.Redirect(PrevStep);
                return;
            }

            if (!IsPostBack) // first time loading 
            {
                //BindLists();
                BindData();
            }
        }
        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(PrevStep); 
        }

        private void securityCheck()
        {
            //string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                //tool.AddMessage("COTO_ID = " + CotoId.ToLower());
                //tool.AddMessage("hash = " + hash.ToLower());
                //tool.AddMessage("timeValuePassed = " + timeValuePassed.ToString());
                //tool.AddMessage("timeValueLocal = " + timeValueLocal.ToString());
                //tool.AddMessage("timeDifference = " + timeDifference.ToString());
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");

                Session["ID"] = null;
            }
        }

        #endregion

        #region Methods

        protected void BindData()
        {
            var repository = new Repository();
            var status = repository.GetUserApplicationStatusInfo(CurrentUserId);
            if (status != null)
            {
                if (status.ReadyOnlineView)
                {
                    trNoAccess.Visible = false;
                    trView.Visible = true;

                    if (status.ApplicationReceivedDate!= DateTime.MinValue)
                    {
                        lblItem1Status.Text = status.ApplicationReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.ApplicationFeeReceivedDate != DateTime.MinValue)
                    {
                        lblItem2Status.Text = status.ApplicationFeeReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.CurrencyRequrementMetDate != DateTime.MinValue)
                    {
                        lblItem3Status.Text = status.CurrencyRequrementMetDate.ToString("MM/dd/yyyy");
                    }

                    if (status.AddCurrencyDataSheetReceivedReqired) { trStatus4.Visible = true; } else { trStatus4.Visible = false; }
                    if (status.AddCurrencyDataSheetReceivedDate != DateTime.MinValue)
                    {
                        lblItem4Status.Text = status.AddCurrencyDataSheetReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.PracticeSupervisorFormReceivedRequired) { trStatus5.Visible = true; } else { trStatus5.Visible = false; }
                    if (status.PracticeSupervisorFormReceivedDate != DateTime.MinValue)
                    {
                        lblItem5Status.Text = status.PracticeSupervisorFormReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.InitialLearningContactRequired) { trStatus6.Visible = true; } else { trStatus6.Visible = false; }
                    if (status.InitialLearningContactDate != DateTime.MinValue)
                    {
                        lblItem6Status.Text = status.InitialLearningContactDate.ToString("MM/dd/yyyy");
                    }

                    if (status.FinalLearningContactRequired) { trStatus7.Visible = true; } else { trStatus7.Visible = false; }
                    if (status.FinalLearningContactDate != DateTime.MinValue)
                    {
                        lblItem7Status.Text = status.FinalLearningContactDate.ToString("MM/dd/yyyy");
                    }

                    if (status.VulnerableCheckDate != DateTime.MinValue)
                    {
                        lblVS_CheckStatus.Text = status.VulnerableCheckDate.ToString("MM/dd/yyyy");
                    }

                    if (status.ConductRequirementMetDate != DateTime.MinValue)
                    {
                        lblItem8Status.Text = status.ConductRequirementMetDate .ToString("MM/dd/yyyy");
                    }

                    if (status.ConductReviewFeeReceivedRequired) { trStatus9.Visible = true; } else { trStatus9.Visible = false; }
                    if (status.ConductReviewFeeReceivedDate != DateTime.MinValue)
                    {
                        lblItem9Status.Text = status.ConductReviewFeeReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.LanguageFluencyReceivedRequired) { trStatus10.Visible = true; } else { trStatus10.Visible = false; }
                    if (status.LanguageFluencyReceivedDate != DateTime.MinValue)
                    {
                        lblItem10Status.Text = status.LanguageFluencyReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.WorkEligibilityLegalWorkDate != DateTime.MinValue)
                    {
                        lblItem11Status.Text = status.WorkEligibilityLegalWorkDate.ToString("MM/dd/yyyy");
                    }

                    //if (status.WESReportReceivedRequired) { trStatus12.Visible = true; } else { trStatus12.Visible = false; }
                    //if (status.WESReportReceivedDate != DateTime.MinValue)
                    //{
                    //    lblItem12Status.Text = status.WESReportReceivedDate.ToString("MM/dd/yyyy");
                    //}

                    //if (status.AcademicEquivalencyReviewFeeReceivedRequired) { trStatus13.Visible = true; } else { trStatus13.Visible = false; }
                    //if (status.AcademicEquivalencyReviewFeeReceivedDate != DateTime.MinValue)
                    //{
                    //    lblItem13Status.Text = status.AcademicEquivalencyReviewFeeReceivedDate.ToString("MM/dd/yyyy");
                    //}

                    //if (status.AcademicReviewToolReceivedRequired) { trStatus14.Visible = true; } else { trStatus14.Visible = false; }
                    //if (status.AcademicReviewToolReceivedDate != DateTime.MinValue)
                    //{
                    //    lblItem14Status.Text = status.AcademicReviewToolReceivedDate.ToString("MM/dd/yyyy");
                    //}

                    //if (status.CourseCirriculumReceivedRequired) { trStatus15.Visible = true; } else { trStatus15.Visible = false; }
                    //if (status.CirriculumReceivedDate != DateTime.MinValue)
                    //{
                    //    lblItem15Status.Text = status.CirriculumReceivedDate.ToString("MM/dd/yyyy");
                    //}
                    if (status.FinalEducationalTranscriptReceivedRequired) { trStatus16.Visible = true; } else { trStatus16.Visible = false; }
                    if (status.FinalEducationalTranscriptReceivedDate != DateTime.MinValue)
                    {
                        lblItem16Status.Text = status.FinalEducationalTranscriptReceivedDate.ToString("MM/dd/yyyy");
                    }

                    //if (status.ProgramApprovedCotoDate != DateTime.MinValue)
                    //{
                    //    lblItem17Status.Text = status.ProgramApprovedCotoDate.ToString("MM/dd/yyyy");
                    //}

                    if (status.SeasDispositionReportRequired) { trStatus17.Visible = true; } else { trStatus17.Visible = false; }
                    if (status.SeasDispositionReport != DateTime.MinValue) {
                        lblItem17Status.Text = status.SeasDispositionReport.ToString("MM/dd/yyyy");
                    }

                    if (status.LMSA_FormReceivedRequired) { trStatus17a.Visible = true; } else { trStatus17a.Visible = false; }
                    if (status.LMSA_FormReceived != DateTime.MinValue)
                    {
                        lblItem17aStatus.Text = status.LMSA_FormReceived.ToString("MM/dd/yyyy");
                    }

                    if (status.OTRegulatoryHistory1ReceivedRequired) { trStatus18.Visible = true; } else { trStatus18.Visible = false; }
                    if (status.OTRegulatoryHistory1ReceivedDate != DateTime.MinValue)
                    {
                        lblItem18Status.Text = status.OTRegulatoryHistory1ReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.OTRegulatoryHistory2ReceivedRequired) { trStatus19.Visible = true; } else { trStatus19.Visible = false; }
                    if (status.OTRegulatoryHistory2ReceivedDate != DateTime.MinValue)
                    {
                        lblItem19Status.Text = status.OTRegulatoryHistory2ReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.OTRegulatoryHistory3ReceivedRequired) { trStatus20.Visible = true; } else { trStatus20.Visible = false; }
                    if (status.OTRegulatoryHistory3ReceivedDate != DateTime.MinValue)
                    {
                        lblItem20Status.Text = status.OTRegulatoryHistory3ReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.OTRegulatoryHistory4ReceivedRequired) { trStatus21.Visible = true; } else { trStatus21.Visible = false; }
                    if (status.OTRegulatoryHistory4ReceivedDate != DateTime.MinValue)
                    {
                        lblItem21Status.Text = status.OTRegulatoryHistory4ReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.OTRegulatoryHistory5ReceivedRequired) { trStatus22.Visible = true; } else { trStatus22.Visible = false; }
                    if (status.OTRegulatoryHistory5ReceivedDate != DateTime.MinValue)
                    {
                        lblItem22Status.Text = status.OTRegulatoryHistory1ReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.OtherProfessionRegistration1Required) { trStatus23.Visible = true; } else { trStatus23.Visible = false; }
                    if (status.OtherProfessionRegistration1Date != DateTime.MinValue)
                    {
                        lblItem23Status.Text = status.OtherProfessionRegistration1Date.ToString("MM/dd/yyyy");
                    }

                    if (status.OtherProfessionRegistration2Required) { trStatus24.Visible = true; } else { trStatus24.Visible = false; }
                    if (status.OtherProfessionRegistration2Date != DateTime.MinValue)
                    {
                        lblItem24Status.Text = status.OtherProfessionRegistration2Date.ToString("MM/dd/yyyy");
                    }

                    if (status.OtherProfessionRegistration3Required) { trStatus25.Visible = true; } else { trStatus25.Visible = false; }
                    if (status.OtherProfessionRegistration3Date != DateTime.MinValue)
                    {
                        lblItem25Status.Text = status.OtherProfessionRegistration3Date.ToString("MM/dd/yyyy");
                    }

                    if (status.ProfessionLiabilityInsuranceReceivedDate != DateTime.MinValue)
                    {
                        lblItem26Status.Text = status.ProfessionLiabilityInsuranceReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.AffidavitReceivedRequired) { trStatus27.Visible = true; } else { trStatus27.Visible = false; }
                    if (status.AffidavitReceivedDate != DateTime.MinValue)
                    {
                        lblItem27Status.Text = status.AffidavitReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.EmployerAcknowledgeFormReceivedRequired) { trStatus28.Visible = true; } else { trStatus28.Visible = false; }
                    if (status.EmployerAcknowledgeFormReceivedDate != DateTime.MinValue)
                    {
                        lblItem28Status.Text = status.EmployerAcknowledgeFormReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.ConfirmationRegistrationCAOTExamReceivedRequired) { trStatus29.Visible = true; } else { trStatus29.Visible = false; }
                    if (status.ConfirmationRegistrationCAOTExamReceivedDate != DateTime.MinValue)
                    {
                        lblItem29Status.Text = status.ConfirmationRegistrationCAOTExamReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.CAOTExamResultsReceivedDate != DateTime.MinValue)
                    {
                        lblItem30Status.Text = status.CAOTExamResultsReceivedDate.ToString("MM/dd/yyyy");
                    }

                    if (status.RegistrationFeeReceivedDate != DateTime.MinValue)
                    {
                        lblItem31Status.Text = status.RegistrationFeeReceivedDate.ToString("MM/dd/yyyy");
                    }
                }
                else
                {
                    trNoAccess.Visible = true;
                    trView.Visible = false;
                }
                
            }
            else
            {
                trNoAccess.Visible = true;
                trView.Visible = false;
            }

            var user2 = repository.GetApplicationUserInfo(CurrentUserId);
            if (user2 != null)
            {
                //CurrentUser = user2;
                lblPageTitleLabel.Text = string.Format("Application Checklist for {0} {1} - Applicant ID {2}", user2.FirstName, user2.LastName, user2.Id);
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }

        }

        #endregion
    }
}