﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Classes;
using System.Text;
using System.Security.Cryptography;
using COTO_RegOnly.Classes;
using System.Globalization;

namespace COTO_RegOnly.Application.UserControls
{
    public partial class ctlConductInfo : System.Web.UI.UserControl
    {
        #region Consts

        private string _Key = "Yz7!~3";
        private string PrevStep = WebConfigItems.Application_Step7;
        private string NextStep = WebConfigItems.Application_Step9;
        private const int CurrentStep = 8;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
                securityCheck();

            if (string.IsNullOrEmpty((string)Session["ID"]))
            {
                Response.Redirect(WebConfigItems.Application_Step0);
                return;
            }
            if (!IsPostBack) // first time loading 
            {
                SessionParameters.ApplicationStep = CurrentStep;
                BindLists();
                BindData();
            }
        }

        protected void ibtnBackClick(object sender, ImageClickEventArgs e)
        {
            //Response.Redirect("MemberDisplay.aspx");
            UpdateSteps(-1);
            Session["CustomAction"] = "renewal";
            Response.Redirect(PrevStep); // + "&ID=" + CurrentUserId + "&CustomAction=renewal");
        }

        protected void ibtnNextClick(object sender, ImageClickEventArgs e)
        {
            Page.Validate("PersonalValidation");
            if (Page.IsValid)
            {
                UpdateUserInfo();
                UpdateSteps(1);
                Session["CustomAction"] = "renewal";
                Response.Redirect(NextStep);
            }
        }

        protected void ddlQuestion1SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion1.SelectedValue.ToUpper() == "YES")
            {
                trQuestion1Details.Visible = true;
                txtQuestion1Details.Focus();
            }
            else
            {
                txtQuestion1Details.Text = string.Empty;
                trQuestion1Details.Visible = false;
            }
        }

        protected void ddlQuestion2SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion2.SelectedValue.ToUpper() == "YES")
            {
                trQuestion2Details.Visible = true;
                txtQuestion2Details.Focus();
            }
            else
            {
                txtQuestion2Details.Text = string.Empty;
                trQuestion2Details.Visible = false;
            }
        }

        protected void ddlQuestion3SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion3.SelectedValue.ToUpper() == "YES")
            {
                trQuestion3Details.Visible = true;
                txtQuestion3Details.Focus();
            }
            else
            {
                txtQuestion3Details.Text = string.Empty;
                trQuestion3Details.Visible = false;
            }
        }

        protected void ddlQuestion4SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion4.SelectedValue.ToUpper() == "YES")
            {
                trQuestion4Details.Visible = true;
                txtQuestion4Details.Focus();
            }
            else
            {
                txtQuestion4Details.Text = string.Empty;
                trQuestion4Details.Visible = false;
            }
        }

        protected void ddlQuestion5SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion5.SelectedValue.ToUpper() == "YES")
            {
                trQuestion5Details.Visible = true;
                txtQuestion5Details.Focus();
            }
            else
            {
                txtQuestion5Details.Text = string.Empty;
                trQuestion5Details.Visible = false;
            }
        }

        protected void ddlQuestion6SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion6.SelectedValue.ToUpper() == "YES")
            {
                trQuestion6Details.Visible = true;
                txtQuestion6Details.Focus();
            }
            else
            {
                txtQuestion6Details.Text = string.Empty;
                trQuestion6Details.Visible = false;
            }
        }

        protected void ddlQuestion7SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion7.SelectedValue.ToUpper() == "YES")
            {
                trQuestion7Details.Visible = true;
                txtQuestion7Details.Focus();
            }
            else
            {
                txtQuestion7Details.Text = string.Empty;
                trQuestion7Details.Visible = false;
            }
        }

        protected void ddlQuestion8SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestion8.SelectedValue.ToUpper() == "YES")
            {
                trQuestion8Details.Visible = true;
                txtQuestion8Details.Focus();
            }
            else
            {
                txtQuestion8Details.Text = string.Empty;
                trQuestion8Details.Visible = false;
            }
        }
        #endregion

        #region Methods

        protected void UpdateSteps(int diff)
        {
            if (SessionParameters.ApplicationStep != 0)
            {
                SessionParameters.ApplicationStep += diff;
            }
        }

        protected void UpdateUserInfo()
        {

            User user = new User();

            user.Id = CurrentUserId;
            var conductInfo = new ConductNew();

            DateTime reportedDate = DateTime.Now;
            conductInfo.ReportedDate = reportedDate;

            if (!string.IsNullOrEmpty(hfConductSEQN.Value))
            {
                int seqn = 0;
                int.TryParse(hfConductSEQN.Value, out seqn);
                conductInfo.SEQN = seqn;
            }

            conductInfo.RefusedByRegBody = ddlQuestion1.SelectedValue;
            conductInfo.RefusedByRegBodyDetails = txtQuestion1Details.Text;

            conductInfo.FindMisconductIncomp = ddlQuestion2.SelectedValue;
            conductInfo.FindMisconductIncompDetails = txtQuestion2Details.Text;

            conductInfo.FacingMisconduct = ddlQuestion3.SelectedValue;
            conductInfo.FacingMisconductDetails = txtQuestion3Details.Text;

            conductInfo.FindNegMalpract = ddlQuestion4.SelectedValue;
            conductInfo.FindNegMalpractDetails = txtQuestion4Details.Text;

            conductInfo.ChargedOffence = ddlQuestion5.SelectedValue;
            conductInfo.ChargedOffenceDetails = txtQuestion5Details.Text;

            conductInfo.CondRestrict = ddlQuestion6.SelectedValue;
            conductInfo.CondRestrictDetails = txtQuestion6Details.Text;

            conductInfo.Guilty_Authority = ddlQuestion7.SelectedValue;
            conductInfo.Guilty_Authority_Details = txtQuestion7Details.Text;

            conductInfo.EventCircumstance = ddlQuestion8.SelectedValue;
            conductInfo.EventCircumstanceDetails = txtQuestion8Details.Text;


            var repository = new Repository();
            //repository.UpdateApplicationUserConductInfoLogged(CurrentUserId, user, currentYear);
            repository.UpdateUserConductInfoLoggedNew(CurrentUserId, conductInfo);

            if (ddlQuestion1.SelectedValue.ToUpper() == "YES" || ddlQuestion2.SelectedValue.ToUpper() == "YES" || ddlQuestion3.SelectedValue.ToUpper() == "YES" ||
                ddlQuestion4.SelectedValue.ToUpper() == "YES" || ddlQuestion5.SelectedValue.ToUpper() == "YES" || ddlQuestion6.SelectedValue.ToUpper() == "YES" || 
                ddlQuestion7.SelectedValue.ToUpper() == "YES" || ddlQuestion8.SelectedValue.ToUpper() == "YES")
            {
                string message = string.Format("Member ID: {0}; ", CurrentUserId); ;
                message += string.Format("Date: {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

                string emailSubject = "Applicant Requires Conduct Review";
                string emailTo = WebConfigItems.ApplicationSupportEmail;
                var tool = new Tools();
                tool.SendConfirmationEmail(CurrentUserId, message, emailSubject, emailTo, string.Empty);
            }
        }

        protected void BindLists()
        {
            var list1 = new List<GenClass>();
            list1.Add(new GenClass { Code = "", Description = "" });
            list1.Add(new GenClass { Code = "No", Description = "No" });
            list1.Add(new GenClass { Code = "Yes", Description = "Yes" });

            ddlQuestion1.DataSource = list1;
            ddlQuestion1.DataValueField = "Code";
            ddlQuestion1.DataTextField = "Description";
            ddlQuestion1.DataBind();

            ddlQuestion2.DataSource = list1;
            ddlQuestion2.DataValueField = "Code";
            ddlQuestion2.DataTextField = "Description";
            ddlQuestion2.DataBind();

            ddlQuestion3.DataSource = list1;
            ddlQuestion3.DataValueField = "Code";
            ddlQuestion3.DataTextField = "Description";
            ddlQuestion3.DataBind();

            ddlQuestion4.DataSource = list1;
            ddlQuestion4.DataValueField = "Code";
            ddlQuestion4.DataTextField = "Description";
            ddlQuestion4.DataBind();

            ddlQuestion5.DataSource = list1;
            ddlQuestion5.DataValueField = "Code";
            ddlQuestion5.DataTextField = "Description";
            ddlQuestion5.DataBind();

            ddlQuestion6.DataSource = list1;
            ddlQuestion6.DataValueField = "Code";
            ddlQuestion6.DataTextField = "Description";
            ddlQuestion6.DataBind();

            ddlQuestion7.DataSource = list1;
            ddlQuestion7.DataValueField = "Code";
            ddlQuestion7.DataTextField = "Description";
            ddlQuestion7.DataBind();

            ddlQuestion8.DataSource = list1;
            ddlQuestion8.DataValueField = "Code";
            ddlQuestion8.DataTextField = "Description";
            ddlQuestion8.DataBind();
        }

        protected void BindData()
        {
            var repository = new Repository();
            //int currentYear = DateTime.Now.Year;
            //if (DateTime.Now.Month <= 5)
            //{
            //    currentYear = currentYear - 1;
            //}

            //var user = repository.GetApplicationUserConductInfo(CurrentUserId, currentYear);
            var user = repository.GetUserApplicationConductInfoNew(CurrentUserId);
            if (user != null)
            {
                hfConductSEQN.Value = user.SEQN.ToString();
                hfReportedDate.Value = user.ReportedDate.ToString("yyyy-MM-dd");
                ddlQuestion1.SelectedValue = user.RefusedByRegBody;
                if (ddlQuestion1.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion1Details.Visible = true;
                    txtQuestion1Details.Text = user.RefusedByRegBodyDetails;
                }

                ddlQuestion2.SelectedValue = user.FindMisconductIncomp;
                if (ddlQuestion2.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion2Details.Visible = true;
                    txtQuestion2Details.Text = user.FindMisconductIncompDetails;
                }

                ddlQuestion3.SelectedValue = user.FacingMisconduct;
                if (ddlQuestion3.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion3Details.Visible = true;
                    txtQuestion3Details.Text = user.FacingMisconductDetails;
                }

                ddlQuestion4.SelectedValue = user.FindNegMalpract;
                if (ddlQuestion4.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion4Details.Visible = true;
                    txtQuestion4Details.Text = user.FindNegMalpractDetails;
                }

                ddlQuestion5.SelectedValue = user.ChargedOffence;
                if (ddlQuestion5.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion5Details.Visible = true;
                    txtQuestion5Details.Text = user.ChargedOffenceDetails;
                }

                ddlQuestion6.SelectedValue = user.CondRestrict;
                if (ddlQuestion6.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion6Details.Visible = true;
                    txtQuestion6Details.Text = user.CondRestrictDetails;
                }

                ddlQuestion7.SelectedValue = user.Guilty_Authority;
                if (ddlQuestion7.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion7Details.Visible = true;
                    txtQuestion7Details.Text = user.Guilty_Authority_Details;
                }

                ddlQuestion8.SelectedValue = user.EventCircumstance;
                if (ddlQuestion8.SelectedValue.ToUpper() == "YES")
                {
                    trQuestion8Details.Visible = true;
                    txtQuestion8Details.Text = user.EventCircumstanceDetails;
                }

            }
            var user2 = repository.GetApplicationUserInfo(CurrentUserId);
            if (user2 != null)
            {
                //CurrentUser = user2;
                lblConductTitle.Text = string.Format("Suitability to Practise for {0} #{1}", user2.FullName, user2.Id);
            }
        }

        protected void ShowMessage(string Message)
        {
            omb.ShowMessage(Message, "Message at: " + DateTime.Now.ToShortTimeString());
        }

        protected void ShowMessage(string Message, string Caption)
        {
            omb.ShowMessage(Message, Caption);
        }

        private void securityCheck()
        {
            string hostName = Request.UserHostName;
            string timestamp = Request.QueryString["timestamp"];
            string ID = Request.QueryString["ID"];
            string CotoId = Request.QueryString["COTO_ID"];

            string toHash = ID + _Key + timestamp;
            string hash = getMd5Hash(toHash);

            DateTime dateNow = DateTime.Now;
            string year = dateNow.ToString("yyyy");
            string month = dateNow.ToString("MM");
            string day = dateNow.ToString("dd");
            string hour = dateNow.ToString("HH");
            string minute = dateNow.ToString("mm");

            //  string minute 
            string timestamp2 = year + month + day + hour + minute;

            //   double  timeValueLocal = 
            double timeValueLocal = Convert.ToDouble(timestamp2);
            double timeValuePassed = Convert.ToDouble(timestamp);

            double timeDifference = timeValuePassed - timeValueLocal;
            timeDifference = Math.Abs(timeDifference);

            //if (AMO.ToLower() == hash.ToLower() && timeDifference < 120)
            if (((CotoId.ToLower() == hash.ToLower()) || WebConfigItems.DevMode) && timeDifference < 120)
            {
                Session["ID"] = ID;
            }
            else
            {
                var tool = new Tools();
                tool.AddMessage("Session Timeout.");
                Response.Redirect("ErrorPage.aspx");
            }
        }

        static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        #endregion

        #region Properties

        public string CurrentUserId
        {
            get
            {
                if (SessionParameters.CurrentUserId != null)
                {
                    return SessionParameters.CurrentUserId;
                }
                else
                    return null;
            }
            set
            {
                SessionParameters.CurrentUserId = value;
            }
        }

        #endregion
    }
}