﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="COTO_RegOnly.ErrorPage" MasterPageFile="~/COTOv1.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TemplateHead" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TemplateBody" runat="server">
    <%--<asp:ScriptManager ID="something" runat="server" />--%>
        
    <div>
        <center>
            <table border="0" width="750px" cellpadding="4" cellspacing="0" bgcolor="#ffffff">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="MainForm" style="padding: 10px; text-align:center;">
                            <asp:Label ID="lblErrorLabel" runat="server" Text="Error Message:" Font-Bold="true" ForeColor="Red" />
                            <br />
                            <asp:DataList ID="lsMessages" runat="server" OnItemDataBound="lsMessages_ItemDataBound"
                                Width="99.9%">
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr style="text-align: center">
                                            <td>
                                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                            <br />
                            <asp:Button ID="btnGoBack" runat="server" OnClick="BtnGoBack_Click"
                                Text="Go Back" />
                        </div>
                    </td>
                </tr>
            </table>
        </center>
    </div>        
</asp:Content>
