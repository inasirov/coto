﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Security.Cryptography;

using System.IO;
using System.Text;


public partial class OT_2013RenewDev_VA_dotNet_WCMPayment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string toHash = "*aAU~Aas2" + Session["ID"].ToString() + " 1cc#";
        string redirect = "";
        DateTime dateNow = DateTime.Now;

        lblDebug.Text += "<br>logged in as " + Session["ID"].ToString();
        lblDebug.Text += "<br>hash generated = " + getMd5Hash(toHash);
        redirect = System.Configuration.ConfigurationManager.AppSettings["WCMPaymentPage"] + "?id=" + Session["ID"].ToString() + "&key=" + getMd5Hash(toHash);// +"&timestamp=" + dateNow.ToString();
        lblDebug.Text += "<br>redirect them to: " + redirect;
        Response.Redirect(redirect);

    }


    static string getMd5Hash(string input)
    {
        // Create a new instance of the MD5CryptoServiceProvider object.
        MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

        // Convert the input string to a byte array and compute the hash.
        byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

        // Create a new Stringbuilder to collect the bytes
        // and create a string.
        StringBuilder sBuilder = new StringBuilder();

        // Loop through each byte of the hashed data 
        // and format each one as a hexadecimal string.
        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }

        // Return the hexadecimal string.
        return sBuilder.ToString();
    }





    


    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {

    }

    
    }

 