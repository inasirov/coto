﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace COTO_RegOnly
{
    public partial class Member : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            imgHome.Attributes.Add("onmouseover", "this.src= '" + this.Page.ResolveClientUrl("~/Images/toolbar-homeDwn.gif") + "'");
            imgHome.Attributes.Add("onmouseout", "this.src= '" + this.Page.ResolveClientUrl("~/Images/toolbar-homeUp.gif") + "'");
            imgContactUs.Attributes.Add("onmouseover", "this.src= '" + this.Page.ResolveClientUrl("~/Images/toolbar-contactDwn.gif") + "'");
            imgContactUs.Attributes.Add("onmouseout", "this.src= '" + this.Page.ResolveClientUrl("~/Images/toolbar-contactUp.gif") + "'");
        }
    }
}